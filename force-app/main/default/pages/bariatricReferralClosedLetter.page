<apex:page standardController="Bariatric__c" extensions="bariatricReferralClosedLetterExt" renderAs="pdf" standardStylesheets="false" title="{!cc.patientLastName} - Referral form">
 
<apex:messages />
<head>
 <apex:stylesheet value="{!$Resource.PDFDocumentStyleOncology}" />
 
 <c:pdfheaderfooter type="footer" position="center" showPageNumbers="false">©2019 Health Design Plus, Inc. All Rights Reserved.</c:pdfheaderfooter>
 <title>{!cc.patientLastName } - Referral form</title> 

 </head>
<apex:form style="font-family:Helvetica">
   <div style="padding:1em;padding-top:0;margin-top:-20px">
     <table width="100%">
       <tr>
         <td>
           <apex:image value="{!$Resource.HDP_Corporate_Logo}" style="width:120px;float:left;margin-left:-5px"/>
         </td>
         <td>
           <apex:image value="{!$Resource.ecenLogo}" style="width:120px;float:right;"/>
         </td>
       </tr>
     </table>
     <div style="text-align:center;display:none">
       Adverse Determination Notice
     </div>
     <div>
       <apex:outputText value="{0, date, MMMM d','  yyyy}">
         <apex:param value="{!TODAY()}" /> 
       </apex:outputText>
     </div>
     <div style="margin-top:2.5em">
       <apex:outputText value="{!cc.patientName} test" /><br/>
       <apex:outputText value="{!cc.patientStreet} test" /><br/>
       <!-- rendered="{!cc.patientStreet!=''}" rendered="{!cc.patientCSZ!=''}"-->
       <apex:outputText value="{!cc.patientCSZ } test" /><br/>
     </div>
     <div style="margin-top:1.75em;margin-bottom:1em">
       <p>RE:&nbsp;<apex:outputtext value="{!cc.clientName}"/>&nbsp; Surgery Benefit</p>
     </div>
     <div style="margin-top:1em;margin-bottom:1em">
       <p>Thank you for your interest in the&nbsp;<apex:outputtext value="{!cc.clientName}"/>&nbsp; Surgery Benefit.  This benefit offers <b>eligible medical plan members</b> access to nationally-recognized medical facilities for <b>select covered procedures.</b>  </p>
     </div>
     <div style="margin-top:1em;margin-bottom:1em">
       <p>The benefit is available for patients who meet <b><u>all</u></b> of the program criteria.  Below indicate area(s) that did <b>not</b> meet program requirements: </p>
     </div>
     <div style="margin-top:0;margin-bottom:1em">
       <table style="width:95%;margin-left:20px">
         <tr>
           <td style="width:25px;vertical-align:top">
             <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!Bariatric.Intake_Information_Complete__c!='No' && Bariatric.Status__c = 'Closed' && Bariatric.Date_Provider_Verification_Form_Received__c!=null}"/>
             <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!Bariatric.Intake_Information_Complete__c=='No' || (Bariatric.Status__c = 'Closed' && Bariatric.Date_Provider_Verification_Form_Received__c==null)}" />
           </td>
           <td>
             Provider Verification Form has not been received, is incomplete, or illegible.  
           </td>
         </tr>
         <tr>
           <td style="width:25px;vertical-align:top">
             <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!Bariatric.Date_Provider_Verification_Form_Signed__c==null || Bariatric.Date_Provider_Verification_Form_Signed__c> ADDMONTHS(TODAY(),-1)}"/>
             <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!Bariatric.Date_Provider_Verification_Form_Signed__c!=null&&Bariatric.Date_Provider_Verification_Form_Signed__c <= ADDMONTHS(TODAY(),-1)}"/>
           </td>
           <td>
             Primary Care Provider visit was not completed in the last 30 days.  
           </td>
         </tr>
         <tr>
           <td style="width:25px;vertical-align:top">
             <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px"/>
           </td>
           <td>
             Provider has not agreed to manage the patient’s at home care.
           </td>
         </tr>
         <tr>
           <td style="width:25px;vertical-align:top">
             <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!Bariatric.Does_patient_have_other_coverage__c!='No'}"/>
             <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!Bariatric.Does_patient_have_other_coverage__c=='No'}"/>
           </td>
           <td>
             Patient’s plan is not the primary medical coverage <apex:outputText rendered="{!cc.clientName=='Walmart'}">or the patient is enrolled in an HMO plan.</apex:outputText>
           </td>
         </tr>
         <tr>
           <td style="width:25px;vertical-align:top">
             <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!Bariatric.Eligible__c!='Yes' || (Bariatric.Patient_Age__c!=null && Bariatric.Relationship_to_the_Insured__c !='Child' && Bariatric.Patient_Age__c>=18)}"/>
             <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!Bariatric.Eligible__c=='Yes' && Bariatric.Patient_Age__c!=null && (Bariatric.Relationship_to_the_Insured__c=='Child' || Bariatric.Patient_Age__c<18)}"/>
           </td>
           <td>
             <apex:outputText rendered="{!cc.clientName=='Walmart'}">
               Patient is not a covered Walmart associate, spouse or domestic partner, is not 18 years or older. Coverage is not available to dependent children
             </apex:outputText>
             <apex:outputText rendered="{!cc.clientName!='Walmart'}">
               Patient is not a covered employee, spouse or dependent child 18 years or older. 
             </apex:outputText>
           </td>
         </tr>
         <apex:outputPanel rendered="{!cc.clientName=='Walmart'}">
         <tr>
           <td style="width:25px;vertical-align:top">
             <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px"/>
           </td>
           <td>
             Patient has not had a minimum of 12 months continuous medical plan coverage.
           </td>
         </tr>
         </apex:outputPanel>
         <tr>
           <td style="width:25px;vertical-align:top">
             <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!cc.patientBMI != null && cc.patientBMI>=35.0 && (Bariatric.Type_2_Diabtetic__c=='Yes' || Bariatric.Respiratory_Disorders__c=='Yes' || Bariatric.Hypertension__c=='Yes' || Bariatric.Heart_Disease__c=='Yes')}"/>
             <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!cc.patientBMI != null && cc.patientBMI>=35.0 && (Bariatric.Type_2_Diabtetic__c!='Yes' && Bariatric.Respiratory_Disorders__c!='Yes' && Bariatric.Hypertension__c!='Yes' && Bariatric.Heart_Disease__c!='Yes')}"/>
           </td>
           <td>
             Patient does not meet one of the following clinical criteria:<br/>
             <table style="width:100%;margin-left:auto;margin-right:auto;">
               <tr>
                 <td style="width:25px;vertical-align:top">
                   <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!cc.patientBMI != null && cc.patientBMI<40.0}"/>
                   <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!cc.patientBMI != null && cc.patientBMI>=40.0}"/>
                 </td>
                 <td>
                   Body Mass Index (BMI) of 40 or higher
                 </td>
               </tr>
               <tr>
                 <td style="width:25px;vertical-align:top">
                   <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!cc.patientBMI != null && cc.patientBMI>=35.0 && (Bariatric.Type_2_Diabtetic__c=='Yes' || Bariatric.Respiratory_Disorders__c=='Yes' || Bariatric.Hypertension__c=='Yes' || Bariatric.Heart_Disease__c=='Yes')}"/>
                   <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!cc.patientBMI != null && cc.patientBMI>=35.0 && (Bariatric.Type_2_Diabtetic__c!='Yes' && Bariatric.Respiratory_Disorders__c!='Yes' && Bariatric.Hypertension__c!='Yes' && Bariatric.Heart_Disease__c!='Yes')}"/>
                 </td>
                 <td>
                   Body Mass Index (BMI) of 35 or higher, and at least one obesity-related co-morbid condition, such as type 2 diabetes, high blood pressure, sleep apnea, other respiratory disorders, non-alcoholic fatty liver disease, osteoarthritis, high cholesterol, gastrointestinal disorders, or heart disease.
                 </td>
               </tr>
             </table>  
           
           </td>
         </tr>
         <tr>
           <td style="width:25px;vertical-align:top">
             <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px"/>
           </td>
           <td>
             
             A medication list has not been provided for one of the following conditions:<br/>
             
             <table style="width:80%;margin-left:auto;margin-right:auto;">
               <tr>
                 <td style="width:25px">
                   <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!Bariatric.Type_2_Diabtetic__c!='Yes' }" />
                   <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!Bariatric.Type_2_Diabtetic__c=='Yes' && Bariatric.Medication_List__c!='Included'}"/>
                 </td>
                 <td>
                   type 2 diabetes
                 </td>
                 <td style="width:25px">
                   <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!Bariatric.Respiratory_Disorders__c!='Yes' }"/>
                   <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!Bariatric.Respiratory_Disorders__c=='Yes' && Bariatric.Medication_List__c!='Included'}"/>
                 </td>
                 <td>
                   respiratory disorder
                 </td>
               </tr>
               <tr>
                 <td style="width:25px">
                   <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!Bariatric.Hypertension__c!='Yes'}"/>
                   <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!Bariatric.Hypertension__c=='Yes' && Bariatric.Medication_List__c!='Included'}"/>
                 </td>
                 <td>
                   hypertension
                 </td>
                 <td style="width:25px">
                   <apex:image value="{!URLFOR($Resource.unchecked)}" width="20px" rendered="{!Bariatric.Heart_Disease__c!='Yes'}" />
                   <apex:image value="{!URLFOR($Resource.checked)}" width="20px" rendered="{!Bariatric.Heart_Disease__c=='Yes' && Bariatric.Medication_List__c!='Included'}"/>
                 </td>
                 <td>
                   heart disease
                 </td>
               </tr>
             </table>
           </td>
         </tr>
       </table>
     </div>  
      
     <div style="margin-top:0;margin-bottom:1em">
       <p>During your recent intake process, one or more of the criteria were not met, resulting in your case being closed. If you have questions regarding this benefit, please contact your health care advisor at the number listed on the back of your plan ID card.  If you have updated information to meet the above criteria, please contact us at (877) 891-2689.</p>
     </div>  
     <!--
     <div style="margin-bottom:1em">
       <p>We appreciate the opportunity to serve you.</p>
     </div>
     
     <div style="margin-top:1em;margin-bottom:1em">
       <p>Regards,<br/><br/></p>
     </div>-->
     <div style="margin-top:1em;">
       <p>Regards,<br/><br/></p>
       Health Design Plus<br/>Weight Loss Surgery Benefit Program Administrator
     </div>      
   </div>  
   <div style="page-break-after:always;"/>
   <table width="100%">
       <tr>
         <td>
           <apex:image value="{!$Resource.HDP_Corporate_Logo}" style="width:140px;float:left;margin-left:-5px"/>
         </td>
         <td>
           <apex:image value="{!$Resource.ecenLogo}" style="width:140px;float:right;"/>
         </td>
       </tr>
     </table>
     <div style="margin-top:1em">
       <p>You or your authorized representative have the right to appeal any claim denied in whole or in part. Your appeal must be sent in writing along with any additional information, within 365 days of receipt of the denial to Health Design Plus, ATTENTION APPEALS COORDINATOR at 1755 Georgetown Road, Hudson, Ohio 44236. You may have the right to bring a civil action under ERISA 502(a) following the Plan's final internal appeal process. If your attending provider believes your situation is urgent, you may request an expedited appeal by contacting the Weight Loss Surgery Program at 877-891-2689. If your claim involves medical judgment, and you have exhausted all of the Plan's internal appeal processes (except where your claim is urgent), you may be able to request an external review of your claim by an independent third party who will review the denial and issue a final decision. Please refer your Plan Document for details on the Plan's claims and appeals procedures. For questions about your appeal rights, this notice, or for assistance, you can contact the Weight Loss Surgery Program at 877-891-2689. You also may contact the Employee Benefits Security Administration at 1-866-444-EBSA (3272).</p>
     </div>
     <div style="margin-top:1em">
       <p>PLEASE RETAIN FOR YOUR RECORDS</p>
     </div >
     <div style="margin-top:20em">
       <div style="border:1px solid #000;padding:5px">
         (SPANISH) ATENCIÓN: si habla español, tiene a su disposición servicios gratuitos de asistencia lingüística.  Llame al 1-330-656-1072. (TTY: 711)
         <br/><br/>
         (TAGALOG) PAUNAWA:  Kung nagsasalita ka ng Tagalog, maaari kang gumamit ng mga serbisyo ng tulong sa wika nang walang bayad.  Tumawag sa 1-330-656-1072. (TTY: 711)
         <br/><br/>
         (CHINESE) 注意：如果您使用繁體中文，您可以免費獲得語言援助服務。請致電 1-330-656-1072. (TTY: 711)
         <br/><br/>
         (NAVAJO) Díí baa akó nínízin: Díí saad bee yáníłti’go Diné Bizaad, saad bee áká’ánída’áwo’dę́ę́’, t’áá jiik’eh, éí ná hólq̨́, kojį́ hǫ́dį́į́lnih 1-330-656-1072. (TTY: 711)
       </div>
     </div>
</apex:form>  
</apex:page>