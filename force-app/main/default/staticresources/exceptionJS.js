    var uw;
    var lDate;
    var clientName;
    var fileName;
    
    function setVals(_uw,_lDate,_clientName, _fileName){
        uw = _uw;
        lDate= _lDate;
        clientName= _clientName;
        fileName= _fileName;
    }
    
    j$ = jQuery.noConflict();
    
    j$(window).resize(function() {
    
        j$('.tableWrapper').css('max-height',j$(window).height()*.75);
        j$('#container').css('min-height',j$(window).height()*.75); /*force footer to bottom of the page*/
    
    });
    
    j$(document).ready(function(){
        try{
		
		
		j$('.tdgo').click(function(){
            var i = j$(this).attr('id');
            j$('#init').hide();
			j$('#loading').show();
            setVals(j$('#tr'+i).attr('uw'),j$('#tr'+i).attr('lDate'),j$('#tr'+i).attr('clientName'),j$('#tr'+i).attr('fileName'));
            getex(j$('#tr'+i).attr('uw'),j$('#tr'+i).attr('lDate'));
        });
		
        j$('.trSum').click(function(){
            var i = j$(this).attr('row');
            j$('#trSum'+i).toggle();
            j$('#col'+i).toggle();
            j$('#exp'+i).toggle();
        });
        
		j$('.dlExceptions').click(function(){
		    var i = j$(this).attr('id');
			document.getElementsByClassName('downloading')[parseInt(i)].style.display = 'block';
			setVals(j$('#tr'+i).attr('uw'),j$('#tr'+i).attr('lDate'),j$('#tr'+i).attr('clientName'),j$('#tr'+i).attr('fileName'));
			downloadExceptions(j$('#tr'+i).attr('uw'),j$('#tr'+i).attr('lDate'));
        });
		
		j$('.dlChanges').click(function(){
			var i = j$(this).attr('id');
			document.getElementsByClassName('downloading')[parseInt(i)].style.display = 'block';
			setVals(j$('#tr'+i).attr('uw'),j$('#tr'+i).attr('lDate'),j$('#tr'+i).attr('clientName'),j$('#tr'+i).attr('fileName'));
            downloadChanges(j$('#tr'+i).attr('uw'),j$('#tr'+i).attr('lDate'));
        });
		
        //j$('#exceptionTable').tablesorter();
        j$('#container').css('min-height',j$(window).height()*.75); /*force footer to bottom of the page*/
        }catch(exception){
        //catch errors silently
        }finally{
            j$('.trSum')[0].click();
			j$('body').show(); 
			
        }
        
    });