	function hideShow(hideVal, theVal, theClass){
		if(hideVal.indexOf('||')>-1){
			
			var foo = hideVal.split('||');
			var found = false;
			var l = foo.length;
			for(i=0; i < l; i++){
				if(foo[i]==theVal){
					found = true;
					i=l+1;
				}
			}
			
			if(found){
				j$('.'+theClass).parent().show();
			}else{
				j$('.'+theClass).parent().hide();
			}
			
		}else{
		
			if(hideVal==theVal){
				j$('.'+theClass).parent().show();
			}else{
				j$('.'+theClass).parent().hide();
			}
		}
	}
	
    function prescribedDrug(foo){
        
        if(foo=='Yes'){
            j$('#p\\:f\\:pageBlockMain\\:ppPBS\\:prescribedDrugPBS\\:prescribedDrugPBSLeft\\:drugBlock').show();
        }else{
            j$('#p\\:f\\:pageBlockMain\\:ppPBS\\:prescribedDrugPBS\\:prescribedDrugPBSLeft\\:drugBlock').hide();
        }
        
    }
    
    function setFormatPhone(){  
        j$('.phone').each(function(){
            j$(this).keydown(function(event){
                formatPhoneOnEnter(this, event);
            });
            
            j$(this).blur(function(){
                formatPhone(this);
            });
        });
    }
    
	function m_Delivered(x){
        
        if(x=='Yes' || x ==''){
            j$('.m_nonDelivered').closest('.data2Col').hide();
            j$('.m_Delivered').closest('.data2Col').show();
			
			j$('.m_nonDelivered').closest('.labelCol').hide();
            j$('.m_Delivered').closest('.labelCol').show();
        }else if(x=='No'){
            j$('.m_nonDelivered').closest('.data2Col').show();
            j$('.m_Delivered').closest('.data2Col').hide();
			
			j$('.m_nonDelivered').closest('.labelCol').show();
            j$('.m_Delivered').closest('.labelCol').hide();
        }
        
    }
	
    function delivered(x){
        
        if(x=='Yes'){
            j$('.nonDelivered').parent().hide();
            j$('.Delivered').parent().show();
        }else if(x=='No'){
            j$('.nonDelivered').parent().show();
            j$('.Delivered').parent().hide();
        }
        
    }
       
    function gh(x){
        if(x=='Yes'){
            j$('.gh').parent().show();
        }else{
            j$('.gh').parent().hide();
        }
    }
    
    function expand(){
        
        j$('.expandL,.expandR').click(function(){
            var theid = this.id.substr(0,this.id.length-1);
            var column = this.className.substr(this.className.length-1);
            if(column=='L'){column='Left'}else{column='Right'}
            
            j$('#'+theid+'E').hide();
            j$('#'+theid+'C').show();
            
            var unselected = 'p:f:pageBlockMain:medicalHistoryPBS:mhTop:medicalHistory'+column+'PBS:'+theid+'PBSI:'+theid+'_unselected';
            var selected = 'p:f:pageBlockMain:medicalHistoryPBS:mhTop:medicalHistory'+column+'PBS:'+theid+'PBSI:'+theid+'_selected';
            var s = ((document.getElementById(unselected).length+document.getElementById(selected).length)+1);
            document.getElementById(unselected).size = s;
            document.getElementById(selected).size = s;
            
        });
        
    }
    
    function collapse(){
        
        j$('.collapseL,.collapseR').click(function(){
            var theid = this.id.substr(0,this.id.length-1);
            var column = this.className.substr(this.className.length-1);
            if(column=='L'){column='Left'}else{column='Right'}
            
            j$('#'+theid+'E').show();
            j$('#'+theid+'C').hide();
            
            var unselected = 'p:f:pageBlockMain:medicalHistoryPBS:mhTop:medicalHistory'+column+'PBS:'+theid+'PBSI:'+theid+'_unselected';
            var selected = 'p:f:pageBlockMain:medicalHistoryPBS:mhTop:medicalHistory'+column+'PBS:'+theid+'PBSI:'+theid+'_selected';
            
            document.getElementById(unselected).size = "4";
            document.getElementById(selected).size = "4";
            
        });
        
    }
	
	function nursing(x){
	
		if(x=='Stopped Nursing'){
			j$('.stoppedNursing').parent().show();
			j$('.nursing').parent().hide();
		}else if(x=='Nursing'){
			j$('.stoppedNursing').parent().hide();
			j$('.nursing').parent().show();
		}else{
			j$('.stoppedNursing').parent().hide();
			j$('.nursing').parent().hide();
		}
		return;
	}
	
	function feedingType(x){
		
		if(x=='Breast' || x=='Supplementing'){
		  j$('.breast').parent().show();
		  j$('.formula').parent().show();	
		}else if(x=='Formula'){
		  j$('.breast').parent().hide();
		  j$('.formula').parent().show();	
		}else{
		  j$('.breast').parent().hide();
		  j$('.formula').parent().hide();	
		}
		return;
	}