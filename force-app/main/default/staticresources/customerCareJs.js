function buttonComplete(el){
        try{
            el.disabled=false;
            el.style.cursor='pointer';
        }catch(e){console.log(e);}
        document.body.style.cursor='default';
        buttonHandler();
  }

  function buttonHandler(){
    j$('.btn').click(function(){
        this.disabled=true;
        this.style.cursor='progress';
        document.body.style.cursor='progress';
    });
  }
 
  function hideShow(hideVal, theVal, theClass){
		console.log(hideVal+ "" +theVal+ "" + theClass);
        if(hideVal.indexOf('||')>-1){
            
            var foo = hideVal.split('||');
            var found = false;
            var l = foo.length;
            for(i=0; i < l; i++){
                if(foo[i]==theVal){
                    found = true;
                    i=l+1;
                }
            }
            
            if(found){
                j$('.'+theClass).show();
            }else{
                j$('.'+theClass).hide();
            }
            
        }else{
        
            if(hideVal==theVal){
                j$('.'+theClass).show();
            }else{
                j$('.'+theClass).hide();
            }
        }
    }
    
    var attachment={};
    
    attachment.saveAttachment = function(){
        
        var input = document.getElementById('attachFile');
        var reader = new FileReader(); 
        reader.file = input.files[0];
        
        reader.onerror = function(e) 
        {
            switch(e.target.error.code) 
            {
                case e.target.error.NOT_FOUND_ERR:
                    alert('File Not Found!');
                    break;
                case e.target.error.NOT_READABLE_ERR:
                    alert('File is not readable');
                    break;
                case e.target.error.ABORT_ERR:
                    break; // noop
                default:
                    alert('An error occurred reading this file.');
            };
        };     

        reader.onabort = function(e) 
        {
            alert('File read cancelled');
        };

        reader.onload = function(e){ 
            saveAttach(input.files[0].name, input.files[0].type, (new sforce.Base64Binary(e.target.result)).toString());
        };
        
        reader.readAsBinaryString(input.files[0]);
        
    }   
    
     $.fn.scrollView = function () {
        return this.each(function () {
        
            if($(this).scrollTop!=0){
       
                $('html, body').animate({
                    scrollTop: $(this).offset().top - 60
                }, 100);
            }
        });
    }
    
    function SSNValidation(ssn) {
        if(ssn!=null && ssn != ''){
        var matchArr = ssn.match(/^\d{9}$/);
        
        if (matchArr == null){
            alert('Please enter or correct the SSN. The SSN should be 9 numeric characters, please do not enter any dashes');
            document.body.style.cursor='default';
            return false;
        }else{ 
            if (parseInt(matchArr[1],10)==0) {
                alert("Invalid SSN: SSN's can't start with 000.");
                document.body.style.cursor='default';
                return false;
            }
        }
        }
        
        return true;
    }   
    
    function validateSearch(){
    
        document.getElementById('p:f:searchPB:searchHealthpac').disabled=true;
        document.body.style.cursor='progress';
        document.getElementById('p:f:searchPB:searchHealthpac').style.cursor='progress';
        
    
        msg = '';
        var ssn  = document.getElementById('searchSSN').value;
        console.log('ssn '+ssn);
        cert = document.getElementById('searchCERT').value;
        client = document.getElementById('p:f:searchPB:searchClient').value;
        efn = document.getElementById('efn').value;
        eln = document.getElementById('eln').value
        
        if(ssn!='' && ssn!=null){
            if(!SSNValidation(document.getElementById('searchSSN').value)){
                return false;
            }
        }else{
            
            if(cert=='' && client=='' && efn=='' && eln==''){   
               msg = msg + 'No search criteria entered';
               return false;
            }
            
        }
        if(msg==''){
            searchHealthpac(client,ssn,cert,efn,eln);
            return true;
        }else{
            alert(msg);
            
        }
        
    }
    
    function ccdRelChange(rel){
        
        if(rel=='Employee'){
            document.getElementById('p:f:ccdPB:pfn').value = document.getElementById('p:f:ccdPB:efn').value;
            document.getElementById('p:f:ccdPB:pln').value = document.getElementById('p:f:ccdPB:eln').value;
            document.getElementById('pDOB').value = document.getElementById('eDOB').value;
        }
        
    }
    
    function callerRelationship(rel){
        
        if(rel=='Other'){
            document.getElementById('RelationshipOther').style.display ='block'
        }else{
            document.getElementById('RelationshipOther').style.display ='none'
        }
    }
    
    function getWorkTask(){
		if(document.getElementById('p:f:cciPB:inquiryWorkTaskInput')==null){
			return '';
		}	
        return document.getElementById('p:f:cciPB:inquiryWorkTaskInput').value;
    }
    
    function getWorkDepartment(){
        return document.getElementById('p:f:cciPB:inquiryWorkDepartmentInput').value;
    }
    
    function getInquiryReason(){
		if(document.getElementById('p:f:cciPB:inquiryReasonInput')==null){
			return '';
		}	
        return document.getElementById('p:f:cciPB:inquiryReasonInput').value;
    }
    
	function getInquiryReasonClosed(){
		return document.getElementById('p:f:cciPB:inquiryClosedReason').value;
		
    }
	
	function getInquiryReasonClosedNote(){
        return document.getElementById('p:f:cciPB:inquiryClosedNote').value;
        
    }
  

let dropArea = document.getElementById('coe-file-drop-area')
  let dropAreaModal = document.getElementById('coe-file-drop-area-modal')
  let coeFileDirectory=""
  let uploadProgress = []
  let uploadError = false;
  let url ='';
  
  function preventDefaults (e) {
	e.preventDefault()
	e.stopPropagation()
  }
  
  function initializeProgress(numFiles) {
	progressBar.value = 0
	uploadProgress = []

	for(let i = numFiles; i > 0; i--) {
		uploadProgress.push(0)
	}
  }
  
  function setDocumentStorageUrl(u,b,c){
	  coeFileDirectory = b+''+c;
	  url=u+'savefile/';
	  console.log('coeFileDirectory '+coeFileDirectory );
	  console.log('url '+url);
  }
  
  function fileUploadOk(){
	  document.getElementById('document_storage_error').style.display='none'; 
	  document.getElementById('document_storage_success').style.display='block'; 
	  document.getElementById('document_storage_success_msg').innerText='File Uploaded';
				
	  setTimeout(
		()=>{
		  document.getElementById('document_storage_success').style.display='none'; 		
		},3000
	  );
  }
  
  function fileUpload(){
	
    if(document.getElementById('attachFile').files[0]===undefined){
		alert('Please select a file');
		
	}else{
		uploadFile(document.getElementById('attachFile').files[0])
		.then(
			function n(){
				saveAttach(document.getElementById('attachFile').files[0].name);
			}
		)		
		.catch(
			e => {
				console.log(e);
				document.body.style.cursor='default';  
			}
		)
		
	}
  }
  function cancelFileUpload(){
	document.body.style.cursor='default';  
	document.getElementById('document_storage_error').style.display='none'; 
	document.getElementById('document_storage_success').style.display='none'; 
	
  }
  function handleFiles(files) {
	document.getElementById('attachFile').files = files;
	
  }
  
  function handleDrop(e) {
	let dt = e.dataTransfer
	let files = dt.files
	handleFiles(files)
  }
  	  
  function updateProgress(fileNumber, percent) {
	uploadProgress[fileNumber] = percent
	let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length
	progressBar.value = total
  }

  function uploadFile(file) {
	console.log(coeFileDirectory);
	return new Promise((resolve, reject) => {	
	let formData = new FormData()
	uploadError=true;
	
	let fileName = file.name;
	let newFileName = fileName.substring(0,fileName.lastIndexOf('.'));
	
	newFileName = newFileName.replaceAll('-', '_');
	newFileName = newFileName.replaceAll(',', '_');
	newFileName = newFileName.replaceAll('#', '_');
	newFileName = newFileName.replaceAll('&', '_');
	
	var blob = file.slice(0, file.size, file.type); 
	newFile = new File([blob], newFileName+''+fileName.substring(fileName.lastIndexOf('.')), {type: blob.type});
	
	formData.append('file', newFile)
	formData.append('directory', coeFileDirectory)
	
	fetch(url, {
		method: 'POST',
		body: formData
	}).
	then(response => response.json()).
	
	then(json => {
		uploadError=false;
		resolve('ok');/* Done. Inform the user */ 
		
	})
	.catch(e => {
		reject(e);
		uploadError=true;/* Error. Inform the user */ })
    
    });
  }