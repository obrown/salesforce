  let coeFileDirectory=""
  
  function setDocumentStorageUrl(u,b,c){
	  coeFileDirectory = b+''+c;
	  url=u+'savefile/';
  }
  function fileUpload(){
	
    if(document.getElementById('attachFile').files[0]===undefined){
		alert('Please select a file');
		
	}else{
		uploadFile(document.getElementById('attachFile').files[0])
		.then(
			function n(){
				saveAttach(document.getElementById('attachFile').files[0].name);
			}
		)		
		.catch(
			e => {
				console.log(e);
				document.body.style.cursor='default';  
			}
		)
		
	}
  }
  function uploadFile(file) {
	
	return new Promise((resolve, reject) => {	
	let formData = new FormData()
	uploadError=true;
	let fileName = file.name;
	let newFileName = fileName.substring(0,fileName.lastIndexOf('.'));
	//newFileName = newFileName.replaceAll(' ', '');
	newFileName = newFileName.replaceAll('-', '_');
	newFileName = newFileName.replaceAll(',', '_');
	newFileName = newFileName.replaceAll('#', '_');
	newFileName = newFileName.replaceAll('&', '_');
	
	var blob = file.slice(0, file.size, file.type); 
	newFile = new File([blob], newFileName+''+fileName.substring(fileName.lastIndexOf('.')), {type: blob.type});
	formData.append('file', newFile)
	formData.append('directory', coeFileDirectory)
	fetch(url, {
		method: 'POST',
		body: formData
	}).
	then(response => response.json()).
	
	then(json => {
		uploadError=false;
		resolve('ok');/* Done. Inform the user */ 
		
	})
	.catch(e => {
		reject(e);
		uploadError=true;/* Error. Inform the user */ })
    
    });
	
  }  