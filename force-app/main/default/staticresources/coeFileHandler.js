let dropArea = document.getElementById('coe-file-drop-area')
  let dropAreaModal = document.getElementById('coe-file-drop-area-modal')
  let coeFileDirectory=""
  let uploadProgress = []
  let uploadError = false;
  let url ='';
  
  function addDSListeners(){
	  dropArea = document.getElementById('coe-file-drop-area');
	  dropAreaModal = document.getElementById('coe-file-drop-area-modal');
      if(dropArea!==null){
		document.getElementById('document_storage_cancelBtn').addEventListener('click', function(){
		cancelFileUpload()
	  
		});
		document.getElementById('document_storage_uploadBtn').addEventListener('click', function(){
		fileUpload()
	  
		})
	}
  if(dropAreaModal!==null){
  document.getElementById('fileChooserCancelButtonModal').addEventListener('click', function(){
	  cancelFileUpload()
	  
  })
  }
  if(dropArea!==null){
	dropArea.addEventListener('drop', handleDrop, false)
  }
  if(dropAreaModal!==null){
	dropAreaModal.addEventListener('drop', handleDrop, false)
  }
  if(dropArea!==null){
  ;	['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
		dropArea.addEventListener(eventName, preventDefaults, false)
	})
  }
  if(dropAreaModal!==null){
	;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
		dropAreaModal.addEventListener(eventName, preventDefaults, false)
	})
  }
  if(dropArea!==null){
	;['dragenter', 'dragover'].forEach(eventName => {
		dropArea.addEventListener(eventName, highlight, false)
	})
  }
  if(dropAreaModal!==null){
	;['dragenter', 'dragover'].forEach(eventName => {
		dropAreaModal.addEventListener(eventName, highlight, false)
	})
  }
  if(dropArea!==null){
	;['dragleave', 'drop'].forEach(eventName => {
		dropArea.addEventListener(eventName, unhighlight, false)
	})
  }
  if(dropAreaModal!==null){
	;['dragleave', 'drop'].forEach(eventName => {
		dropAreaModal.addEventListener(eventName, unhighlight, false)
	})
  }
  }
  
  function highlight(e) {
	if(dropArea!==null){
		dropArea.classList.add('highlight')
	}
	if(dropAreaModal!==null){
		dropAreaModal.classList.add('highlight')
	}
  }

  function unhighlight(e) {
	if(dropArea!==null){
		dropArea.classList.remove('highlight')
	}
	if(dropAreaModal!==null){
		dropAreaModal.classList.remove('highlight')
	}
  }
  
  function preventDefaults (e) {
	e.preventDefault()
	e.stopPropagation()
  }
  
  function initializeProgress(numFiles) {
	progressBar.value = 0
	uploadProgress = []

	for(let i = numFiles; i > 0; i--) {
		uploadProgress.push(0)
	}
  }
  
  
  
  function setDocumentStorageUrl(u,b,c){
	  coeFileDirectory = b+''+c;
	  url=u+'savefile/';
  }
  
  function fileUploadOk(){
	  document.getElementById('document_storage_error').style.display='none'; 
	  document.getElementById('document_storage_success').style.display='block'; 
	  document.getElementById('document_storage_success_msg').innerText='File Uploaded';
				
	  setTimeout(
		()=>{
		  document.getElementById('document_storage_success').style.display='none'; 		
		},3000
	  );
  }
  
  function fileUpload(){
	document.getElementById('document_storage_error').style.display='none'; 
	document.getElementById('document_storage_success').style.display='none'; 
	
	
    if(document.getElementById('fileElem').files[0]===undefined){
		alert('Please select a file');
		
	}else{
		//coeFileDirectory='{!baseDir}{!coeFileDirectory}';
		uploadFile(document.getElementById('fileElem').files[0])
		.then(
			function n(){
				addAttachment(document.getElementById('fileElem').files[0].name);
			}
		)		
		.catch(
			e => {
				document.getElementById('document_storage_error').style.display='block'; 
				document.getElementById('document_storage_error_msg').innerText=e;
				document.getElementById('document_storage_uploadBtn').disabled=false; 
				document.getElementById('document_storage_uploadBtn').style.cursor='pointer'; 
				document.body.style.cursor='default';  
			}
		)
		
	}
  }
  function cancelFileUpload(){
	document.body.style.cursor='default';  
	document.getElementById('document_storage_error').style.display='none'; 
	document.getElementById('document_storage_success').style.display='none'; 
	
	if(dropArea!==null){
		document.getElementById('document_storage_cancelBtn').disabled=false; 
		document.getElementById('document_storage_cancelBtn').style.cursor='pointer';  
		document.getElementById('fileDescription').style.display='none'; 
		
		document.getElementById('fileElem').value=''  
		document.getElementById("fileChooserButtonLabel").innerHTML="<strong>Choose a file</strong><span style='font-weight:200'>&nbsp;or drag it here.</span>";
		document.getElementById('document_storage_buttons').style.display='none'; 
	}
	if(dropAreaModal!==null){
		document.getElementById('fileElemModal').value=''  
		document.getElementById("fileChooserButtonLabelModal").innerHTML="<strong>Choose a file</strong><span style='font-weight:200'>&nbsp;or drag it here.</span>";
		//document.getElementById('fileChooserCancelButtonModal').style.display='none'; 
		document.getElementById('attachmentModal').style.display='none';
		document.getElementById('modal-backdrop').style.display='none';
	}  
  }
  
  function listFiles(fileName) {
	
	if(dropArea!==null){
		document.getElementById('fileChooserButtonLabel').innerHTML=fileName;
		document.getElementById('document_storage_buttons').style.display='inline-block';
		document.getElementById('fileDescription').style.display='block'; 
    }
	if(dropAreaModal!==null){
		document.getElementById('fileChooserButtonLabelModal').innerHTML=fileName;
		document.getElementById('fileChooserCancelButtonModal').style.display='block';
	}
  }
  
  function handleFiles(files) {
	listFiles(files[0].name)
	if(dropArea!==null){
		document.getElementById('fileElem').files = files;
	}
	if(dropAreaModal!==null){
		document.getElementById('fileElemModal').files = files;
	}
  }
  
  function handleDrop(e) {
	let dt = e.dataTransfer
	let files = dt.files
	handleFiles(files)
  }
  	
  
  function updateProgress(fileNumber, percent) {
	uploadProgress[fileNumber] = percent
	let total = uploadProgress.reduce((tot, curr) => tot + curr, 0) / uploadProgress.length
	progressBar.value = total
  }

  function uploadFile(file) {
	
	return new Promise((resolve, reject) => {	
	let formData = new FormData()
	uploadError=true;
	let fileName = file.name;
	let newFileName = fileName.substring(0,fileName.lastIndexOf('.'));
	//newFileName = newFileName.replaceAll(' ', '');
	newFileName = newFileName.replaceAll('-', '_');
	newFileName = newFileName.replaceAll(',', '_');
	newFileName = newFileName.replaceAll('#', '_');
	newFileName = newFileName.replaceAll('&', '_');
	
	var blob = file.slice(0, file.size, file.type); 
	newFile = new File([blob], newFileName+''+fileName.substring(fileName.lastIndexOf('.')), {type: blob.type});
	formData.append('file', newFile)
	formData.append('directory', coeFileDirectory)
	fetch(url, {
		method: 'POST',
		body: formData
	}).
	then(response => response.json()).
	
	then(json => {
		uploadError=false;
		resolve('ok');/* Done. Inform the user */ 
		
	})
	.catch(e => {
		reject(e);
		uploadError=true;/* Error. Inform the user */ })
    
    });
	
  }