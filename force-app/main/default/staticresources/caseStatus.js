
    function setStatus(pageload){
        try{
        var status = getAvailableStatus().split(',');
        status[0] = status[0].substring(1,status[0].length)
        status[status.length-1] = status[status.length-1].substring(0,status[status.length-1].length-1)
        var objStatus = getStatus();
               
        var _select = j$('<select>');
            for(var i=0;i<status.length;i++){
                if(status[i].trim() == objStatus){
                _select.append(
                    j$('<option></option>').val(status[i].trim()).html(status[i].trim()).attr('selected','selected')
                );
                foo=false;
               }else{
                _select.append(
                   j$('<option></option>').val(status[i].trim()).html(status[i].trim())
                );
               }
            }
        j$('#status').append(_select.html());   
        setStatusReason(objStatus, pageload);
        }catch(e){}
    }

    function setStatusReason(s, pageload){
        try{
        j$('#statusReason')
        .find('option')
        .remove()
        .end()
        
        var objStatusReason = getStatusReason();
        var statusReason = [];
        statusReason = setstatusReasonforCompare(s);
		
        if(statusReason.length==1){ //User may not have access to the current status IE CSR Pended
            s=j$('#status').val();
            statusReason = setstatusReasonforCompare(s);
        }
        
        if(statusReason.length>0){
            statusReason[0] = statusReason[0].substring(1,statusReason[0].length)
            statusReason[statusReason.length-1] = statusReason[statusReason.length-1].substring(0,statusReason[statusReason.length-1].length-1)
            var foo=true;
            
            var _select = j$('<select>');
            for(var i=0;i<statusReason.length;i++){
                if(statusReason[i].trim() == objStatusReason){
                _select.append(
                    j$('<option></option>').val(statusReason[i].trim()).html(statusReason[i].trim()).attr('selected','selected')
                    
                );
                foo=false;
               }else{
                _select.append(
                   j$('<option></option>').val(statusReason[i].trim()).html(statusReason[i].trim())
                );
               }
            }        
            j$('#statusReason').append(_select.html());
            if(pageload && foo){
            _select.html('');
                _select.append(
                    j$('<option></option>').val(objStatusReason.trim()).html(objStatusReason.trim()).attr('selected','selected')
                );
                j$('#statusReason').append(_select.html());
            }
            
            if(s=='Closed'){
                j$('#closedReason').show();
            }else{
                j$('#closedReason').hide();
            }
            
            j$('#srSubList').hide();
        
        }
        
        setSubStatusReason(undefined, pageload);
        }catch(e){}
    }   
	
    function setSubStatusReason(sr, pageload){
       
        j$('#statusReasonSub')
        .find('option')
        .remove()
        .end()
        
        var status = j$('#status').val();
        if(sr===undefined){
            sr=j$('#statusReason').val();
        }
        
        var objSubStatusReason = getSubStatusReason();
        var SubStatusReason = setSubStatusReasonForCompare(sr);
        
        if(SubStatusReason.length==0 && objSubStatusReason !='-- None --' && objSubStatusReason !='' && sr == getStatusReason()){
            var foo = [];
            foo[0] = ',' + getSubStatusReason() + ',';
            SubStatusReason[0] = foo[0];
        }
        
        if(SubStatusReason.length>0){
            
            SubStatusReason[0] = SubStatusReason[0].substring(1,SubStatusReason[0].length)
            SubStatusReason[SubStatusReason.length-1] = SubStatusReason[SubStatusReason.length-1].substring(0,SubStatusReason[SubStatusReason.length-1].length-1)
            
            var _select = j$('<select>');
			var foo = false;
            for(var i=0;i<SubStatusReason.length;i++){
                if(SubStatusReason[i].trim() == objSubStatusReason){
                foo = true;
				_select.append(
					j$('<option></option>').val(SubStatusReason[i].trim()).html(SubStatusReason[i].trim()).attr('selected','selected')
                );
               }else{
                _select.append(
                   j$('<option></option>').val(SubStatusReason[i].trim()).html(SubStatusReason[i].trim())
                );
               }
            } 

			if(objSubStatusReason!='' && !foo && pageload){
				_select.append(
					j$('<option></option>').val(objSubStatusReason).html(objSubStatusReason).attr('selected','selected')
				);
			}
			
            j$('#statusReasonSub').append(_select.html());
            j$('#p\\:f\\:section1\\:section1\\:right\\:srSubList\\:statusReasonlbl').parent().show();
            j$('#statusReasonSub').parent().show();
            
        }else{
        
            j$('#p\\:f\\:section1\\:section1\\:right\\:srSubList\\:statusReasonlbl').parent().hide();
            j$('#statusReasonSub').parent().hide();
            j$('#p\\:f\\:gipb\\:gileftright\\:giright\\:hSubStatusReason').val('');
            j$('#p\\:f\\:hSubStatusReason').val('');
            
            
        }
    
    }