    $(document).ready(function(){
       
        $('.modal_opener').click(function(){
            var action = this.getAttribute('data-modal');
            if (action) {
                $('#' + action).modal('show');
            }
        });
      
       $('.datepicker').datepicker();
       $('#rxTo').datepicker('setDate', clientRxTo)
       $('#rxFrom').datepicker('setDate', clientRxFrom)
       
        
    });
    
    var searchAMS = {};
       
    searchAMS.verifySearchRX = function(){
        
        searchAMS.clearError();
        
        if($('#rxTransSearchEssn').val() == ''){
            searchAMS.addError('Please provide the eSSN to search'); 
            return;
        }
        try{
            
            if($('#rxFrom').val()!=''){
                var d = searchAMS.normalizeDate($('#rxFrom').val());
                if(d==null){
                    searchAMS.addError("Please check that the 'From' date is a valid date and formatted 'mm/dd/yyyy'");
                    return;
                }
                $('#rxFrom').datepicker('setDate', d); 
            }
            
            if($('#rxTo').val()!=''){
                var d = searchAMS.normalizeDate($('#rxTo').val());
                if(d==null){
                    searchAMS.addError("Please check that the 'To' date is a valid date and formatted 'mm/dd/yyyy'");
                    return;
                }
                $('#rxTo').datepicker('setDate', d);
            }    
        }catch(e){
            searchAMS.addError('An error has occured with one of the search date criteria');
            console.log(e);
            return;
        }
                
        var rxFrom = $('#rxFrom').datepicker('getDate');
        var today = new Date();
        
        if(today<rxFrom){
            searchAMS.addError('Please set the "From Date" to a date in the past or today'); 
            return;
        }
        
        var rxTo = $('#rxTo').datepicker('getDate');
        
        if(rxTo != null && rxTo<rxFrom){
            searchAMS.addError('Please set the "To Date" to a date later than or equal to the "From Date"'); 
            return;
        
        }
        
        $('#rxSearchActionPrevent').addClass('modal-backdrop in');
        $('#rxSearchActionPrevent').show();
        
        searchRx( $('#rxFrom').val(), $('#rxTo').val(), $('#rxTransSearchGrp').val(), $('#rxTransSearchUW').val(), $('#rxTransSearchEssn').val(), $('#rxSearchFamily').prop('checked'));
    }
    
    searchAMS.clearError = function(){
        $('#messageRow').hide();
        $('#message').html('');
    }
    
    searchAMS.addError = function(msg){
        $('#messageRow').show();
        $('#message').html(msg);
    }
    
           
    searchAMS.normalizeDate= function(x){
        
        var rxd = x.split('/');
        
        if(rxd[0].length==1){
            rxd[0]='0'+rxd[0];
        }
        
        if(rxd[1].length==1){
            rxd[1]='0'+rxd[1];
        }
        
        if(rxd[2].length==2){
            rxd[2] = '20'+rxd[2];
        }
        
        if(parseInt(rxd[0])>12 || parseInt(rxd[0])==00){
            return null;
        }
        
        if(parseInt(rxd[1])>31 || parseInt(rxd[0])==00){
            return null;
        }
        
        x= rxd[0] + '/' + rxd[1] + '/' + rxd[2];
        return x;
    }
    
    searchAMS.searchRxComplete = function(){
      $('#searchErrorsModal').modal('hide');
      $('#searchFamilyResults').prop('checked',$('#rxSearchFamily').prop('checked'));
      $('#errorsResultsModal').modal('show');
      $('#rxSearchActionPrevent').removeClass('modal-backdrop in');
      $('#rxSearchActionPrevent').hide();
    }