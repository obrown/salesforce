j$= jQuery.noConflict();

function overlay(section) {
	var isIE = false
	if(navigator.userAgent.indexOf('MSIE')>-1){isIE= true;}else{isIE= false};
	var el = document.getElementById("overlay");
	j$('.overlayinner').css('height',0);
	j$('.overlayinner').css('width',0);
	j$('.dateInput').hide();
	j$('.bPageBlock .detailList .labelCol').css('width', '32%');
	j$('.data2Col').css('width', '82%');
	if(el.style.visibility == "visible"){
		
		el.style.visibility =  "hidden"; 
		j$('#myoverlayBackground').hide();
		
	}else{
		if(section=='rc'){
			
			j$('.dateInput').show();
			el.style.visibility =  "visible";
			j$('#myoverlayBackground').show();
			j$('.overlayinner').hide();
			j$('#relatedCaseType').dialog({ closeOnEscape: true,
											position: { my: 'center', at: 'top+25%', of: window}, 
											width:'auto', 
											close: function(event, ui){el.style.visibility =  'hidden';}}).parent().appendTo(j$('#patientCase\\:f'));		
			
		}else if(section=='pro'){
			j$('.overlayinner').hide();
			el.style.visibility =  "visible";
			j$('#myoverlayBackground').show(); 
			j$('.bPageBlock .detailList .dataCol').css('width', '0%');
			j$('#addproviders').dialog({ closeOnEscape: true,
										 position: { my: 'center', at: 'top+25%', of: window},  
										 width:'auto', 
										 close: function(event, ui){j$('.bPageBlock .detailList .dataCol').css('width', '32%');el.style.visibility =  'hidden';}}).parent().appendTo(j$('#patientCase\\:f'));
	
		}else if(section=='addoncharge'){
			j$('.dateInput').show();
			el.style.visibility =  "visible";
			j$('#myoverlayBackground').show();
			j$('.overlayinner').hide();
			j$('#addoncharges').dialog({ closeOnEscape: true,
										 position: { my: 'center', at: 'top+25%', of: window}, 
										 width:'auto', 
										 close: function(event, ui){el.style.visibility =  'hidden';}}).parent().appendTo(j$('#patientCase\\:f'));		
			
		}else if(section=='statusModal'){
			
			j$('.overlayinner').hide();
			el.style.visibility =  "visible";
			j$('#myoverlayBackground').show(); 
			
			j$('.dateInput').show();
			
			setStatus();
			var status = getStatus();
			setStatusReason(status);
			setSubStatusReason(getStatusReason());

			j$('#statusModal').dialog({ closeOnEscape: true,
										position: { my: 'center', at: 'top+25%', of: window}, 
										width:'auto', 
										close: function(event, ui){el.style.visibility =  'hidden';}}).parent().appendTo(j$('#patientCase\\:f'));
			
		}else if(section=='cc'){
			j$('.dateInput').show();
			el.style.visibility =  "visible";
			j$('#myoverlayBackground').show();
			j$('.overlayinner').hide();
			j$('#clinicalCodeModal').dialog({ closeOnEscape: true,
											  position: { my: 'center', at: 'top+25%', of: window},  
											  width:'auto', 
											  close: function(event, ui){el.style.visibility =  'hidden';}}).parent().appendTo(j$('#patientCase\\:f'));
			
		}else if(section=='continine'){
			el.style.visibility =  "visible";
			j$('#myoverlayBackground').show();
			j$('#continine').show(); 
			j$('.overlayinner').draggable();
			j$('.overlayinner').css('min-width','300px');
			j$('.overlayinner').css('width','7%');
			j$('.overlayinner').css('height','7%');                
			j$('.overlayinner').css('margin','250px auto');
			j$('#patientCase\\:f\\:pgclinical\\:planofcare\\:leftplanofcare\\:tipbsileft\\:pocacceptedout').show();			
		}else if(section=='contininetravel'){
			el.style.visibility =  "visible";
			j$('#myoverlayBackground').show();
			j$('#contininetravel').show(); 
			j$('.overlayinner').draggable();
			var newwidth =100+document.getElementById('contininetravel').offsetWidth;
			j$('.overlayinner').css('width',newwidth);
			var newheight = j$('#contininetravel').height();
			j$('.overlayinner').css('height',newheight);
			j$('.overlayinner').css('margin','250px auto');   
			j$('#patientCase\\:f\\:contininetravelmodal\\:contininetravelbuttons\\:contininereasonok').css('margin-left',(newwidth-j$('#patientCase\\:f\\:contininetravelmodal\\:contininetravelbuttons\\:bottom').width())/4);
		}else if(section='proSearch'){
			j$('.dateInput').show();
			el.style.visibility =  "visible";
			j$('#myoverlayBackground').show();
			j$('.overlayinner').hide();
			j$('.bPageBlock .detailList .labelCol').css('width', '0%');
			j$('#addprovidersDB').dialog({ closeOnEscape: true,
										   position: { my: 'center', at: 'top+25%', of: window},
										   width:'auto', 
										   close: function(event, ui){j$('.bPageBlock .detailList .labelCol').css('width', '32%');el.style.visibility =  'hidden';}}).parent().appendTo(j$('#patientCase\\:f'));
			
		}
		
	}
}

function hidemodal(){

	j$('#relatedCaseType').hide();
	j$('#clinicalCodeModal').hide();
	j$('#continine').hide(); 
	j$('#contininetravel').hide(); 
              
}

function sc(){
	var ephn = getEphn();
	
	if(ephn == 'false'){
		try{
			j$('#patientCase\\:f\\:eandppb\\:iipbsncs\\:sclabel').hide();
		}catch (e){
			j$('#patientCase\\:f\\:eandppb\\:iipbscs\\:sclabel').hide();
		}
		
	}else{
		try{
			j$('#patientCase\\:f\\:eandppb\\:iipbsncs\\:sclabel').show();
		}catch (err){
			j$('#patientCase\\:f\\:eandppb\\:iipbscs\\:sclabel').show();
		}

	}
	
}

function dande(x){
	decryptandopen(x);
}

function getSelectedTabName() {
	if (RichFaces) {

		var tabs = RichFaces.panelTabs['patientCase:f:tp'];
		
		for (var i = 0; i < tabs.length; i++) {
			var tab = tabs[i];
			if (RichFaces.isTabActive(tab.id + '_lbl')) {
				
				return tab.name;

			}

		}

	}

	return null;
	
}



function testEvent(e){
	
	var test=false;
	
	if(e.keyCode==48){
		return true;
	}
	if(e.keyCode==49){
		return true;
	}
	if(e.keyCode==50){
		return true;
	}
	if(e.keyCode==51){
		return true;
	}
	if(e.keyCode==52){
		return true;
	}
	if(e.keyCode==53){
		return true;
	}
	if(e.keyCode==54){
		return true;
	}
	if(e.keyCode==55){
		return true;
	}
	if(e.keyCode==56){
		return true;
	}
	if(e.keyCode==57){
		return true;
	}
	if(e.keyCode==96){
		return true;
	}
	if(e.keyCode==97){
		return true;
	}
	if(e.keyCode==98){
		return true;
	}
	if(e.keyCode==99){
		return true;
	}
	if(e.keyCode==100){
		return true;
	}
	if(e.keyCode==101){
		return true;
	}
	if(e.keyCode==102){
		return true;
	}
	if(e.keyCode==103){
		return true;
	}
	if(e.keyCode==104){
		return true;
	}
	if(e.keyCode==105){
		return true;
	}
	if(e.keyCode==191){
		return true;
	}
	if(e.keyCode==111){
		return true;
	}
	
	return test;
}

function bdate(e,theID){
	
	if(testEvent(e)==true){
		var theDate = document.getElementById(theID).value;
		var n = theDate.split("/");
		if(n.length==3){
			var end = n[2];
			
			if(end.length==4){
				document.getElementById(theID).value = n[0] + "/" + n[1] + "/" + "19" + end.substr(2,2);
			}
			if(end.length==2){
				if(end!="19"){
					document.getElementById(theID).value = n[0] + "/" + n[1] + "/" + "19" + end.substr(0,2);
				}
			}
			
		}
	}
	
}

function addNote(){
	document.body.style.cursor='default';
	window.open('/apex/addNote?id={!Patient_Case__c.id}&tab=' + getSelectedTabName(),'_blank','width=800,height=690,left=300px,top=300px');
	
}

function createTask(){
	
	window.open('/00T/e?what_id=' + getRecordID() + '&retURL=%2Fapex%2FcaseDetail%3Fid%3D' + getRecordID() + '%26tab%3DTab10',null,'width=850,height=860');
	document.body.style.cursor='default';
}

function sendEmail(){
	
	window.open('/_ui/core/email/author/EmailAuthor?p3_lkid='+getRecordID()+'&retURL=%2F'+getRecordID(),'_parent');
	
}

function previewReferral(){
	document.body.style.cursor='default';
	var theURL = '';
	var isRelated = getIsRelated();
	var recordid = getRecordID();
	
	if(record == 'Walmart Cardiac' && isRelated=='true'){
		theURL = '/apex/wmcardiacrelatedreferral?id=' + recordid; 
	}else if(record == 'Walmart Cardiac' && isRelated=='false'){
		theURL = '/apex/wmcardiacreferral?id=' + recordid; 
	}else if(record == 'Walmart Spine' && isRelated=='false'){
		theURL = '/apex/wmspinereferral?id=' + recordid;
	}else if(record == 'Walmart Spine' && isRelated=='true'){
		theURL = '/apex/wmspinerelatedreferral?id=' + recordid;       
	}else if(record == 'Lowes Cardiac' && isRelated=='true'){
		theURL = '/apex/lowesCardiacRelatedReferral?id=' + recordid; 
	}else if(record == 'Lowes Cardiac' && isRelated=='false'){
		theURL = '/apex/lowesCardiacReferral?id=' + recordid; 
	}else if(record == 'Lowes Spine' && isRelated=='true'){
		theURL = '/apex/lowesSpineRelatedReferral?id='+ recordid; 
	}else if(record == 'Lowes Spine' && isRelated=='false'){
		theURL = '/apex/lowesSpineReferral?id='+ recordid; 
	}else if(record == 'Kohls Cardiac' && isRelated=='false'){
		theURL = '/apex/kohlsCardiacReferral?id=' + recordid; 
	}else if(record == 'Kohls Cardiac' && isRelated=='true'){
		theURL = '/apex/kohlsCardiacRelatedReferral?id='+ recordid;      
	}else if(record == 'PepsiCo Cardiac' && isRelated=='true'){
		var theURL = '/apex/pepsicoCardiacRelatedReferral?id='+ recordid; 
	}else if(record == 'PepsiCo Cardiac' && isRelated=='false'){
		var theURL = '/apex/pepsicoCardiacReferral?id='+ recordid; 
	}else if(record == 'PepsiCo Joint' && isRelated=='true'){
		var theURL = '/apex/pepsicoJointRelatedReferral?id='+ recordid; 
	}else if(record == 'PepsiCo Joint' && isRelated=='false'){
		var theURL = '/apex/pepsicoJointReferral?id=' + recordid; 
	}else if(record == 'HCR ManorCare Cardiac' && isRelated=='true'){
		var theURL = '/apex/hcrCardiacRelatedreferral?id=' + recordid; 
	}else if(record == 'HCR ManorCare Cardiac' && isRelated=='false'){
		var theURL = '/apex/hcrCardiacreferral?id=' + recordid; 
	}else if(record == 'jetBlue Spine' && isRelated=='false'){
		var theURL = '/apex/jbSpinereferral?id=' + recordid; 
	}else if(record == 'jetBlue Spine' && isRelated=='true'){
		var theURL = '/apex/jbSpinerelatedreferral?id=' + recordid; 
	}else if(record == 'Walmart Joint' || record == 'Lowes Joint' || record == 'McKesson Joint'){
		var theURL = '/apex/pbghreferral?id=' + recordid; 
	}
	
	window.open(theURL,"_blank","width=800,height=500,scrollbars=yes, resizable=yes"); 

}

function confirmtheDelete(){

	if(confirm('Are you sure you want to delete the case?')){
		document.body.style.cursor='progress';
		deletePC();
	}
	
}

function compcg(){
	
	if(record.indexOf("Walmart") !== -1 || record.indexOf("McKesson") !== -1){
		j$('.cg').show();   
		j$('.comp').hide();  
	}else{
		j$('.cg').hide();   
		j$('.comp').show(); 
	}
	
}

function openandencrypt(attachID){
	
	document.body.style.cursor = 'default';
	window.open('https://c.na7.content.force.com/servlet/servlet.FileDownload?file=' + attachID,'_blank');
	re_encrypt();
	return false;
}

function deduct(){
	var isError = getIsError();
	if(isError=='false'){
		alert('Something has gone wrong, please submit a ticket to report the issue.');
	}else{
		alert('Request successfully submitted');
		document.location.reload(true);
	}
}

function saveDisable(){
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:inlineEditSave').attr('disabled', 'disabled');
}	

function checkRelated(){
	
	if('{!Patient_Case__c.Client_Name__c}'=='HCR ManorCare'){
		document.body.style.cursor='progress';
		createaRelatedCase('HCR ManorCare');
	}else if('{!Patient_Case__c.Client_Name__c}'=='Kohls'){
		document.body.style.cursor='progress';
		createaRelatedCase('Kohls');
	}else{
		overlay('rc');
	}   
	
}

function createtherelatedcase(){
	var clientName = getClientName();
	if(j$('#patientCase\\:f\\:getrct').val()==''){
		document.body.style.cursor='default';
		alert('Please select a case type');
	}else{
		createaRelatedCase(clientName + ' ' + j$('#patientCase\\:f\\:getrct').val());
	}
	
}	

function showpocedit(){

	j$('#patientCase\\:f\\:pgclinical\\:planofcare\\:leftplanofcare\\:tipbsileft\\:pocacceptedout').css('display','none');
	j$('#patientCase\\:f\\:pgclinical\\:planofcare\\:leftplanofcare\\:tipbsileft\\:pocacceptedin').css('display','inline-block');
	setTimeout( function() { j$('#patientCase\\:f\\:pgclinical\\:planofcare\\:leftplanofcare\\:tipbsileft\\:pocacceptedin').focus(); }, 5 );
	j$('.dateInput').css('display','inline-block');
	j$('.dateFormat').css('display','none');
	myinlinebuttons();

}

function addeditclass(theid){

	j$(document.getElementById(theid)).addClass('inlineEditWriteOn');
}   

function removeeditclass(theid){
	j$(document.getElementById(theid)).removeClass('inlineEditWriteOn');
}
function myinlinebuttons(){
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:inlineEditSave').show();
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:inlineEditCancel').show();
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:inlineEditCancel').show();
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:edit').hide();
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:deleteButton').hide();
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:ltr').hide();
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:eButton').hide();
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:refButton').hide();
	j$('#patientCase\\:f\\:pagemode\\:buttons\\:convButton').hide();
	
}

function nicotravelreason(){
	
	j$('#patientCase_f_nicotabpb_nicotabresultspbs_nicotabresultspbsleft_clearedtotravel').change(function() {
		j$('#patientCase_f_nicotabpb_nicotabresultspbs_nicotabresultspbsleft_nicoreason').change(function() {
			
			if( j$("#patientCase_f_nicotabpb_nicotabresultspbs_nicotabresultspbsleft_clearedtotravel").val()=='Yes' && j$(this).val()=='No Test Required'){
				overlay('contininetravel');
				j$('#InlineEditDialog').hide();
				j$('.overlayBackground').hide();
				j$('#patientCase\\:f\\:contininetravelmodal\\:contininetravelbuttons\\:contininereasonok').addClass(btnDisabled);
				j$('#patientCase\\:f\\:contininetravelmodal\\:contininetravelbuttons\\:contininereasonok').attr('disabled','true');
				
			}           
		});
		
	});
	
	j$('#patientCase_f_nicotabpb_nicotabresultspbs_nicotabresultspbsleft_nicoreason').change(function () {
		
		if(j$("#patientCase_f_nicotabpb_nicotabresultspbs_nicotabresultspbsleft_clearedtotravel").val()=='Yes' && j$(this).val()=='No Test Required'){
			overlay('contininetravel');
			j$('#InlineEditDialog').hide();
			j$('.overlayBackground').hide();
			j$('#patientCase\\:f\\:contininetravelmodal\\:contininetravelbuttons\\:contininereasonok').addClass('btnDisabled');
			j$('#patientCase\\:f\\:contininetravelmodal\\:contininetravelbuttons\\:contininereasonok').attr('disabled','true');
		}
	}); 
	
}

function jointtype(_type){
	if(_type == 'Hip'){
		
		j$('#patientCase\\:f\\:pgclinical\\:pgbsclinical\\:pgbsclinicalright\\:rhpapbsi\\:rhpaop').parent().prev('th').css('display','table-cell');
		j$('#patientCase\\:f\\:pgclinical\\:pgbsclinical\\:pgbsclinicalright\\:rhpapbsi\\:rhpaop').parent().css('display','table-cell');
		j$('#patientCase\\:f\\:pgclinical\\:planofcare\\:leftplanofcare\\:chrapbsi\\:chralabel').css('display','block');
		j$('#patientCase\\:f\\:pgclinical\\:planofcare\\:leftplanofcare\\:chrapbsi\\:chraop').css('display','block');
		
	}else{
		j$('#patientCase\\:f\\:pgclinical\\:pgbsclinical\\:pgbsclinicalright\\:rhpapbsi\\:rhpaop').parent().prev('th').css('display','none');
		j$('#patientCase\\:f\\:pgclinical\\:pgbsclinical\\:pgbsclinicalright\\:rhpapbsi\\:rhpaop').parent().css('display','none');
		j$('#patientCase\\:f\\:pgclinical\\:planofcare\\:leftplanofcare\\:chrapbsi\\:chralabel').css('display','none');
		j$('#patientCase\\:f\\:pgclinical\\:planofcare\\:leftplanofcare\\:chrapbsi\\:chraop').css('display','none');
	}
	
}

function forcedNote(field) {
	var pcID  = getRecordID();
	var body = prompt('Please enter the reason the ' + field);
	
	while(body==null || body==''){
		
		body = prompt('Please enter the reason the ' + field);
		
	}
	
	Visualforce.remoting.Manager.invokeAction(
	'{!$RemoteAction.publicRemoteAction.addForceNote}',
	pcID,
	field,
	body,
	function(result, event){
		
		if (result == '200') {
			loadNotes();
		} else {
			alert(result);
			forcedNote(field);
		}
	}, 
	{escape: true}
	);        
	
}

function resizeinner(){
	setTimeout(function () {
		var newWidth = 120+j$('#patientCase\\:f\\:addCC\\:ccInfo\\:cListPbs\\:cList').outerWidth();
		j$('.overlayinner').css('width',newWidth );
	},5);
}

function nicoShow(){
	j$('#patientCase\\:f\\:pgclinical\\:pgbsclinical\\:pgbsclinicalleft\\:nicopbsi\\:nicoOut').hide();
	j$('#patientCase\\:f\\:pgclinical\\:pgbsclinical\\:pgbsclinicalleft\\:nicopbsi\\:nicoIn').show();
	myinlinebuttons();
}

function nicoretestShow(){ 
	j$('#patientCase\\:f\\:nicotabpb\\:nicotabpbs\\:nicotabpbsright\\:nicotabpbsi\\:testResultsOut').hide();
	j$('#patientCase\\:f\\:nicotabpb\\:nicotabpbs\\:nicotabpbsright\\:nicotabpbsi\\:testResultsIn').show();
	myinlinebuttons();
}   

function showNicoTab(theVal){
	
	//Sets the Date Range of Cotinine to inline
	j$('#patientCase_f_nicotabpb_nicotabpbs_nicotabpbsleft_nicotabpbsdaterange_nicotabpbsdaterangebegin_ilecell').css('display','inline-block');
	j$('#patientCase_f_nicotabpb_nicotabpbs_nicotabpbsleft_nicotabpbsdaterange_nicotabpbsdaterangeend_ilecell').css('display','inline-block');

	j$('#patientCase_f_nicotabpb_nicotabretestingpbs_nicotabretestingpbsleft_nicotabpbsdaterange_nicotabpbsdaterangebegin_ilecell').css('display','inline-block');
	j$('#patientCase_f_nicotabpb_nicotabretestingpbs_nicotabretestingpbsleft_nicotabpbsdaterange_nicotabpbsdaterangeend_ilecell').css('display','inline-block');
	// End
	
	if(theVal==='Yes'){
		j$('#patientCase\\:f\\:nicotineTab_shifted').show();
		j$('.rich-tabpanel-content').show();
	}else if(theVal===undefined && (j$('#patientCase\\:f\\:pgclinical\\:pgbsclinical\\:pgbsclinicalleft\\:nicopbsi\\:nicoOut').html()=='Yes')){
		j$('#patientCase\\:f\\:nicotineTab_shifted').show();
		j$('.rich-tabpanel-content').show();
	}else{
		j$('#patientCase\\:f\\:nicotineTab_shifted').hide();
	}
}

function nicoretest(theVal){
	try{
		if(theVal==='Not Completed'){
			j$('#patientCase\\:f\\:nicotabpb\\:nicoretest').show();
			
		}else if(theVal===undefined && (j$('#patientCase\\:f\\:nicotabpb\\:nicotabpbs\\:nicotabpbsright\\:nicotabpbsi\\:testResultsOut').html()==='Not Completed')){
			j$('#patientCase\\:f\\:nicotabpb\\:nicoretest').show();

		}else{
			j$('#patientCase\\:f\\:nicotabpb\\:nicoretest').hide();
		}
	}catch(e){} 
}