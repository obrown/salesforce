public with sharing class birthNotification{
    
    //public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician,string userName,string userTitle,string userPhone,string userEmail){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);

        string disclaimerPath;
        
        StaticResource logo;
        StaticResource[] srList = [SELECT Id,NamespacePrefix,SystemModstamp,Name FROM StaticResource WHERE Name = 'ohyDisclaimer' order by Name asc];
        logo=srList[0];
        
        String prefix = logo.NamespacePrefix;
        if( String.isEmpty(prefix) ) {
            prefix = '';
        }else{
            //If has NamespacePrefix
            prefix += '__';
        }        

        disclaimerPath='/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'ohyDisclaimer';//added 1-30-21 to set pull in the disclaimerPath    
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=cm.patient__r.Patient_First_Name__c+' '+cm.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Address__c: +'<br/>';
        letterText+='<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.City__c+', ' : ''+'&nbsp';
        letterText+=hasPatientAddress ? cm.patient__r.State__c+' ' : ''+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Zip__c : ''+'<br/><br/>';
        letterText+='<br/><br/>';

        letterText+='</div>';
        letterText+='<p>';

        letterText+='Congratulations on the birth of your baby! If you haven’t done so already, OhioHealthy wants to remind you to enroll your child onto the medical benefits within 31 days of the date of birth. ';
        letterText+='If you are adding to the OhioHealthy benefits, go into eSource > Workday > Change Benefits and select “Birth/Adoption of Child”. ';
        letterText+='You will be required to attach a copy of the proof of birth. ';
        letterText+='The document you received from the hospital is an appropriate document to use at this time. ';
        letterText+='</p>';
        letterText+='<p>';  
        letterText+='If you have questions with the enrollment process, please contact the HRRC at (614) 533.8888 prior to the end of the enrollment window—31 days from the birth of your baby. ';
        letterText+='Congratulations again to you and your family!<br/><br/>';
        letterText+='OhioHealthy<br/>';
        letterText+='</p>';

        letterText+='<p>';  
        letterText+='Sincerely,<br/><br/>';
        /*
        letterText+='Care Management Department<br/>';
        letterText+='Ohio Healthy<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';
        */
        letterText+=userName+'<br/>';
        letterText+=userTitle+'<br/>';
        letterText+=userPhone+'<br/>';
        letterText+=userEmail+'<br/>';        
        
        letterText+='</p>';
        
        letterText+='<div><img src="'+disclaimerPath+'" style="width:320px;margin-left:-3px"/></div><br/>';// pulls in the image of the surveyPath
        
        return letterText;
    }
    
}