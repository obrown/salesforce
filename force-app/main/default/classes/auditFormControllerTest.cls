@isTest(seealldata=false)
private class auditFormControllerTest {
    static testMethod void auditFormController() {
        
        Claims_Audit_Client__c client = new Claims_Audit_Client__c();
        client.Name = 'Cooper';
        client.Underwriter__c = '008';
        client.group_number__c = 'SC1';
        client.Percent_to_Pull__c = 2;
        insert client;

        caClaim__c claim = new caClaim__c();
        claim.Name = '12345678900';
        claim.Allowed_Amount__c = 1000.00;
        claim.Total_Charge__c = 1000.00;
        claim.Amount_to_be_paid__c = 500.00;
        claim.Claim_Processed_Date__c = date.today().addDays(-30);
        claim.Claim_Received_Date__c = date.today().addDays(-25);
        claim.FDOS__c = date.today().addDays(-30);
        claim.TDOS__c = date.today().addDays(-25);
        claim.Employee_First_Name__c = 'John';
        claim.Employee_Last_Name__c = 'Test';
        claim.Patient_First_Name__c = 'John';
        claim.Patient_Last_Name__c = 'Test';
        claim.Relationship_to_Member__c = '00';
        claim.Diagnosis__c ='Test';
        claim.Diagnosis_Description__c ='Test';
        claim.Product__c = 'IV';
        claim.Source_Type__c = 'Test';
        claim.Patient_Age__c = '30';
        claim.client__c = client.id;
        insert claim;
        
        Claims_Auditor__c[] caList = new Claims_Auditor__c[]{};
        caList.add(new Claims_Auditor__c (Audit_Type__c='Random Selection'));
        caList.add(new Claims_Auditor__c (Audit_Type__c='High Dollar'));
         
        Claims_Processor_Crosswalk__c[] cpcList = new Claims_Processor_Crosswalk__c[]{};
        cpcList.add(new Claims_Processor_Crosswalk__c(Processor_Alias_Name__c='yada'));
        cpcList.add(new Claims_Processor_Crosswalk__c(Email_Address__c='unitTest@hdplus.com', name='PlanBuild'));
        
        insert caList;
        insert cpcList;
       
        Claim_Audit__c audit = new Claim_Audit__c();
        audit.claim__c = claim.id;
        audit.Audit_Type__c = 'Random Selection';
        audit.Audit_Status__c = 'To Be Audited';
        audit.comments__c = 'Test';
        audit.Claim_Auditor__c = caList[0].id;
        audit.Claim_Processor__c = cpcList[0].id;
        insert audit;
        
        Claim_Audit_Error__c error = new Claim_Audit_Error__c();
        error.Claim_Audit__c = audit.id;
        error.Coding_Error__c = 123;
        error.Comments__c = 'Test';
        error.Error_Reason__c = 'A';
        error.Error_Source__c = 'Data Entry';
        error.Error_Type__c = 'FYI';
        insert error;
        
        ApexPages.currentPage().getParameters().put('Id',audit.id);
        auditformcontroller acc = new auditformcontroller(new ApexPages.StandardController(audit));
        
        acc.getWordPrintViewXML();
        acc.getCheckBoxXML();
        
        /* Test claims audit batch */
        
        claimsAuditBatch cab = new claimsAuditBatch();
        cab.run();
        
        sendClaimsAuditErrorForm.claimsAuditError(error.id);
        
        /* End test for batch */
        
        ApexPages.currentPage().getParameters().put('Id',audit.id);
        auditformcontroller acc1 = new auditformcontroller();
        
        /* Test highDollarClaimAuditReview */
        
        ApexPages.currentPage().getParameters().put('Id',audit.id);
        highDollarClaimAuditReview hcr = new highDollarClaimAuditReview();
        hcr.init();
        
        /* End */
        
        /* Test highDollarClaimAuditReview */
        
        highDollarController hdc = new highDollarController();
        hdc.save();
        
        //Unresolved errors
        hdc.submit();
        
        //Resolved errors
        error.resolved__c = true;
        update error;
        
        hdc = new highDollarController();
        hdc.submit();
        
        hdc.createPDF();
        hdc.obj.Adjusted_Claim__c = 'Yes';
        hdc.obj.In_Network__c = 'Yes';
        hdc.obj.DRG_Pricing__c = 'No';
        hdc.obj.Negotiation_Attempted__c = 'Yes';
        hdc.obj.Precert__c = 'Yes';
        hdc.obj.Does_COB_Apply__c = 'Yes';
        hdc.obj.Does_Subrogation_Apply__c = 'No';
        hdc.obj.Stop_Loss_Notification_Initiated__c = 'Yes';
        
        hdc.createPDF();
        
        hdc.obj.Pricing_Method_if_not_DRG__c = 'Test';
        hdc.obj.Negotiated_Amount__c = 100.00;
        hdc.obj.Network_Name__c = 'Yes';
        hdc.obj.Precert_Number_of_Days__c= 3;
        hdc.obj.Stop_Loss_Notification_Date__c = date.today();
        hdc.obj.Adjusted_Claim_Reason__c = 'Test';
        
        hdc.createPDF();
        
        highDollarController hdcc = new highDollarController(new ApexPages.StandardController(audit));
        
        /* End */
        
    }
}