public with sharing class UpdateEligibilityRecords {
    
    public set<ID> theIDs = new set<ID>();
    public patient_case__c[] objList = new patient_case__c[]{};
    public list<Eligibilty__c> eList = new list<Eligibilty__c>();
    
    public void UpdateRecordsForOpp(patient_case__c[] newList){
        objList = newList;
        eids();
        list<Eligibilty__c> eUpdateList = new list<Eligibilty__c>();
        set<id> pcId = new set<id>();
        for(patient_case__c pc: newList){
        
                
                for(Eligibilty__c e: elist){                
            
                  if((e.Eligible__c == null || e.Eligible__c =='Cobra') && (e.Patient_Case__c == pc.id)){
                    e.Eligible__c=pc.Eligible__c;  
                    e.Paid_Through_Date__c = pc.Paid_Thru_Date__c;
                    e.Date_Eligibility_Checked_with_Client__c = date.today();
                    e.Notes__c =pc.eNotes__c;
                    eUpdateList.add(e);     
                  }
         
                }
                
                if(userInfo.getLastName() != 'System' &&  userInfo.getLastName() != 'Grisez'){ //Mike and Tom need to test without shooting off emails 
                    pcId.add(pc.id);               
                
                }
            
        }
        
        futureElig.sendEligEmail(pcId);
        
        if(!eUpdateList.isEmpty()){
            update eUpdateList;
        }
        
    
    }
    
    private set<ID> setTheIDs(){
        
        //only run once     
        if(theIDs.isEmpty()){ 
        
            //populate a set of ids that 
            for(patient_case__c o: objList){
                
                theIDs.add(o.id);                
            }
        }   

    return theIDs;   
    }
    
    private void eids(){
        
    //Only run in the after update      
        if(eList.isEmpty()){
            //looking for Eligibilty records coming with intake IDs
            for(Eligibilty__c e: [select e.id,e.Date_Eligibility_Checked_with_Client__c, e.Patient_Case__c,e.Paid_Through_Date__c, e.notes__c,e.eligible__c from Eligibilty__c e where e.Patient_Case__c IN :setTheIDs()]){
                elist.add(e);
            }
     
        }
    }
    
    public static string hdpemail(string client){
        if(utilities.isRunningInSandbox()){
            return UserInfo.getUserEmail();
        }else{
            if(client=='Walmart'){
                return 'wmcoeclinical@hdplus.com'; //WM COE queue e-mail        
            }else{
                return 'hdpcoeclinical@hdplus.com'; //non-WM COE queue e-mail
            }
        }
        
        return null;
    }
    
    static testMethod void unitTest(){
    
        patient_case__c testLead = new patient_case__c();
        testLead.RecordTypeId = '012A0000000ViUe';
        testLead.Client_Name__c = 'Walmart';
        testLead.employee_Last_Name__c='Test1';
        testLead.employee_First_Name__c='Demo';
        //testLead.Company = 'Tester Company + i';
        testLead.Patient_Home_Phone__c='9587458452';
        testLead.Relationship_to_the_Insured__c = 'Patient is Insured';
        testLead.Date_of_Last_Primary_Care_Phys_Visit__c  = system.today();
        testLead.Medicare_As_Secondary_Coverage__c = 'Yes';
        testLead.Expedited_Referral__c  = 'No';
        testLead.Patient_last_Name__c = 'test Last';
        testLead.Patient_Preferred_Phone__c  = '4676464644';
        testLead.Patient_Emergency_Contact_Name__c  = 'test';
        testLead.Patient_Emergency_Contact_Phone__c  = '23123123';
        testLead.Patient_Emergency_Contact_Relationship__c ='Spouse';
        testLead.Patient_City__c = 'P city';
        testLead.Patient_Street__c = 'p street';
        testLead.Patient_State__c = 'OH';
        testLead.Patient_Country__c = 'USA';
        testLead.Patient_Symptoms__c = 'test symptoms'; 
        testLead.Patient_DOB__c = date.today();
        testLead.Patient_gender__c = 'Male';
        testLead.Patient_Symptoms__c = '2323';   
        testLead.Caregiver_Home_Phone__c  = '4850123';
        testLead.Caregiver_Name__c = 'Test CareGiver';    
        testLead.Other_Pertinent_Medical_Info_History__c = 'asdlfjasdjf';
        testLead.Recent_Testing__c ='eradfadsf';        
        testLead.Relationship_to_the_Insured__c = 'Patient is Insured';
        testLead.Diagnosis__c  ='testafas';
        testLead.Proposed_Procedure__c ='klhsaddklfhaskldfj';
        testLead.Procedure_Complexity__c = 'Non-Complex';
        testLead.Referred_Facility__c = 'test facility';
        testLead.Date_Eligibility_is_Checked_with_Client__c  = system.today();
        testLead.Eligible__c = 'Yes';
        testLead.Employee_Primary_Health_Plan_Name__c = 'Test';
        testLead.Employee_DOB__c =  date.today();
        testLead.Number_of_DRG_Codes__c  = 1.00;
        testLead.Employee_gender__c = 'Female';
        testLead.Employee_SSN__c  = '121123';
        testLead.Patient_SSN__c  = '122123';
        testLead.Employee_Street__c = 'E street';
        testLead.Employee_City__c = 'E City';
        testLead.Employee_State__c = 'AZ';
        testLead.Employee_Country__c = 'USA';
        testLead.Status__c = 'Open';
         
        Eligibilty__c testE = new Eligibilty__c();
        insert testE; 
        list<patient_case__c> testLeadlist = new list<patient_case__c>();
        testLeadlist.add(testLead);
        UpdateEligibilityRecords uer = new UpdateEligibilityRecords();
        uer.UpdateRecordsForOpp(testLeadlist);
    
    }
    


}