public with sharing class umDenialMedicalNecessity_ERISA {
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c uml, Utilization_Management_Clinician__c clinician, Utilization_Management_Clinical_Code__c[] diagnosisCodes, Utilization_Management_Clinical_Code__c[] clinicalCodes){
        boolean hasClinician = (Clinician.Street__c != null && Clinician.City__c != null && Clinician.State__c != null && Clinician.Zip_Code__c != null);
        boolean hasAddress = (um.patient__r.Address__c != null && um.patient__r.City__c != null && um.patient__r.State__c != null && um.patient__r.Zip__c != null);

        string letterText = '';

        letterText += um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c + '<br/>';
        letterText += hasAddress ? um.patient__r.Address__c + '<br/>': '' + '<br/>';
        letterText += hasAddress ? um.patient__r.City__c + ', ': ', ';
        letterText += hasAddress ? um.patient__r.State__c + ' ' : ' ';
        letterText += hasAddress ? um.patient__r.Zip__c + '<br/><br/>': '' + '<br/><br/>';
        letterText += 'RE:&nbsp;&nbsp;Patient Name:&nbsp;' + um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c;
        letterText += '<br/>';
        letterText += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth:&nbsp;' + um.patient__r.Patient_Date_of_Birth__c;
        letterText += '</div>';
        letterText += '<p>';
        letterText += 'Dear&nbsp;';
        if (um.patient__r.Gender__c == 'Female') {
            letterText += 'Ms. ';
        }
        else if (um.patient__r.Gender__c == 'Male') {
            letterText += 'Mr. ';
        }

        letterText += um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c + ',';
        letterText += '<p>';
        letterText += 'Contigo Health, LLC is a third-party administrator that performs Care Management services for ' + um.patient__r.Patient_Employer__r.name + '\'s, self-funded group health plan of (Plan), regulated by ERISA.<br/>';
        letterText += '</p>';

        letterText += 'The request referenced below has been denied:<br/><br/>';
        letterText += '<div style="margin-top:10px">';
        letterText += '<div style="display:inline-block;width:15%">';
        letterText += 'Case Number:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px;">';
        letterText += '&nbsp;' + um.HealthPac_Case_Number__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:15%">';
        letterText += 'Physician Name:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;' + Clinician.First_Name__c + ' ' + Clinician.Last_Name__c + ', ' + Clinician.Credentials__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:15%">';
        letterText += 'Facility Name:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;' + um.Facility_Name__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:15%">';
        letterText += 'Dates of Service:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;Beginning ' + um.Admission_Date__c.month() + '/' + um.Admission_Date__c.day() + '/' + um.Admission_Date__c.year();
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:15%;vertical-align:top">';
        letterText += 'Type of Request:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;' + uml.Type_of_Request__c;
        letterText += '</div>';
        letterText += '</div>';

        letterText += '<br/>';
        letterText += 'Diagnoses: ';
        if (diagnosisCodes.isEmpty()) {
            letterText += 'N/A';
        }
        else{
            for (Utilization_Management_Clinical_Code__c dc :diagnosisCodes) {
                letterText += dc.name + ' ' + dc.Description__c + ' ;';
            }
            letterText = letterText.removeEnd(';');
        }
        letterText += '<br/><br/>';
        letterText += 'Treatment: ';

        if (clinicalCodes.isEmpty()) {
            letterText += 'N/A';
        }
        else{
            for (Utilization_Management_Clinical_Code__c dc :clinicalCodes) {
                letterText += dc.name + ' ' + dc.Description__c + ' ;';
            }
            letterText = letterText.removeEnd(';');
        }
        letterText += '<br/><br/>';

        letterText += 'Denial Reason:&nbsp;';
        letterText += '<br/><br/>';

        letterText += 'Clinical Rationale Used to Make Determination: MCG®:';
        letterText += '<br/><br/>';

        letterText += 'Although additional information is not required to submit an appeal, all information provided will be considered when rendering an appeal decision.';
        letterText += '<br/><br/>';

        letterText += 'Additional Information Consideration:<br/>';
        letterText += '    •    Documentation representing material difference from information originally submitted.';
        letterText += '<br/><br/>';

        letterText += 'All choices regarding the care and treatment of the patient remain the responsibility of the attending physician. In no way does this letter attempt to dictate the care the patient ultimately receives.';

        letterText += '<br/><br/>';
        letterText += 'Sincerely,';
        letterText += '<div ></div>';
        letterText += '<div style="display:none" >signhere</div>';
        letterText += 'Eric M. Yasinow, M.D.<br/>';
        letterText += 'Medical Director';
        letterText += '<br/><br/>';

        letterText += '<div>';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += 'CC:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;">';
        letterText += Clinician.First_Name__c + ' ' + Clinician.Last_Name__c + ', ' + Clinician.Credentials__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;"/>';
        letterText += hasClinician ? Clinician.Street__c : '';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;">';
        letterText += hasClinician ? Clinician.City__c + ', ' : ', ';
        letterText += hasClinician ? Clinician.State__c + ' ' : ' ';
        letterText += hasClinician ? Clinician.Zip_Code__c : '';
        letterText += '</div>';
        letterText += '</div><br/>';

        letterText += '<p>';
        letterText += 'This decision has been processed consistent with benefit terms and conditions described in the Plan’s Summary Plan Description and Plan Document. Contacting Customer Service at the telephone number listed on your medical ID card may resolve your questions. You have a right to';
        letterText += ' request, free of charge, a copy of any internal rule, guideline, protocol, or similar criteria used in this determination, an explanation of the scientific or clinical basis of the determination if the denial involves a medical necessity or experimental treatment limitation';
        letterText += ' or exclusion, the diagnosis and treatment codes related to this claim and their meanings, and any documents, records or other information relevant to this claim.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'You or your authorized representative have the right to appeal this decision. Your appeal must be sent in writing, along with any additional information, within 180 days of receipt of the denial to Contigo Health, LLC.,';
        letterText += 'Attention: Appeals Coordinator, 1755 Georgetown Road, Hudson Ohio 44236.<br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'You may have the right to bring civil action under ERISA 502(a) following the Plans final internal appeal process. If your attending provider believes your situation is urgent, you may request an expedited appeal by contacting Customer Service.';
        letterText += ' If your claim involves medical judgment, and you have exhausted all of the Plan\'s internal appeal processes (except where your claim is urgent), you may be able to request an external review of your claim by an independent third party who';
        letterText += ' will review the denial and issue a final decision.  Please refer to your Summary Plan Description and Plan Document for details on the Plan\'s claims and appeals procedures.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'For questions about your appeal rights, this notice, or for assistance, you can contact Customer Service at the telephone number listed on your medical ID card.';
        letterText += 'You also may contact the Employee Benefits Security Administration at 1-866-444-EBSA(3272). <br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'Your employer’s health plan complies with applicable Federal civil rights laws and does not discriminate on the basis of race, color, national origin, age, disability or sex.<br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += '(SPANISH) ATENCIÓN: si habla español, tiene a su disposición servicios gratuitos de asistencia lingüística.  Llame al 1-330-656-1072. (TTY: 711)<br/><br/>';
        letterText += '(TAGALOG) PAUNAWA: Kung nagsasalita ka ng Tagalog, maaari kang gumamit ng mga serbisyo ng tulong sa wika nang walang bayad.  Tumawag sa 1-330-656-1072. (TTY: 711)<br/><br/>';
        letterText += '(CHINESE) <span style="font-family: Arial Unicode MS">(中文):  注意：如果您使用繁體中文，您可以免費獲得語言援助服務。請致電 </span> 1-330-656-1072 (TTY: 711)<br/><br/>';
        letterText += '(NAVAJO) Díí baa akó nínízin:Díí saad bee yáníłti’go Diné Bizaad, saad bee áká’ánída’áwo’dę́ę́’, t’áá jiik’eh, éí ná hólq̨́, kojį́ hǫ́dį́į́lnih   1-330-656-1072<br/>';
        letterText += '</p>';
        letterText += '<br/><br/>';

        return letterText;
    }
}