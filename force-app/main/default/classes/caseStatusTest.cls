@isTest
private class caseStatusTest {

    static testMethod void myUnitTest() {
        //Statuses
        //Non-CSR
        //Pre-Referral Statuses
        id profid = [select id from profile where name = 'HDP Care Management'].id;
        caseStatuses cs = new caseStatuses(profid);
        string[] status = cs.getStatus(false, false);
        set<string> statusSet = new set<string>();
        for(string s :status){
            statusSet.add(s);
        }
        
        system.assert(statusSet.contains('Open'));
        system.assert(statusSet.contains('Pended'));
        system.assert(statusSet.contains('Closed'));
        status.clear();
        statusSet.clear();
        
        //Post-Referral Statuses
        status = cs.getStatus(true, true);
        for(string s :status){
            statusSet.add(s);
        }
        
        system.assert(statusSet.contains('Open'));
        system.assert(statusSet.contains('Pended'));
        system.assert(statusSet.contains('Closed'));
        system.assert(statusSet.contains('Completed'));
        
        //CSR
        profid = [select id from profile where name = 'Customer Service'].id;
        cs = new caseStatuses(profid);
        status = cs.getStatus(false, true);
        
        for(string s :status){
            statusSet.add(s);
        }
        
        system.assert(statusSet.contains('Open'));
        system.assert(statusSet.contains('Closed'));
        status.clear();
        statusSet.clear();
        
        //Post-Referral Statuses
        status = cs.getStatus(true, true);
        for(string s :status){
            statusSet.add(s);
        }
        
        system.assert(statusSet.contains('Open'));
        //system.assert(!statusSet.contains('Closed'));
        //End Statuses
        
        //Status Reasons
        //Non-CSR
        //Pre-Referral Statuses
        cs = new caseStatuses(profid);
        map<string, string[]> statusReasons = cs.getStatusReason(false, true);
        
        //Post-Referral Statuses
        statusReasons = cs.getStatusReason(true, true);
        
        //CSR
        
        //End Status Reasons
        
        //Sub-Status Reasons
        //Non-CSR
        //Pre-Referral Sub-Status
        profid = [select id from profile where name = 'HDP Care Management'].id;
        cs = new caseStatuses(profid);
        map<string, string[]> subStatusReasons = cs.getSubStatusReason(false, true);
        subStatusReasons = cs.getSubStatusReason(true, true);
    }
}