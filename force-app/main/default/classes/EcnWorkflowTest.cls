/**
* This class contains unit tests for validating the behavior of the Appeal Log application
*/
@isTest()
private class EcnWorkflowTest{
    
  static testMethod void testRequiredToSave() {
      patient_case__c pc = new patient_case__c();
      map<string, string> labelMap = new map<string, string>();
      EcenWorkflow.result r = EcenWorkflow.checkRequiredToSaveFields(pc);
  }
  
  static testMethod void testRequiredToContinue() {
      patient_case__c pc = new patient_case__c();
      map<string, string> labelMap = new map<string, string>();
      EcenWorkflow.result r = EcenWorkflow.checkRequiredToContinueIntake(pc, labelMap);
  }
}