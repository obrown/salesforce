public with sharing class umDenialBenefitExclusion_nonERISA {
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c uml, Utilization_Management_Clinician__c clinician, Utilization_Management_Clinical_Code__c[] diagnosisCodes, Utilization_Management_Clinical_Code__c[] clinicalCodes, string logoPath){
        StaticResource sr = [SELECT Id, NamespacePrefix, SystemModstamp FROM StaticResource WHERE Name = 'medicalDirectorSignature' LIMIT 1];

        boolean hasAddress = (um.patient__r.Address__c != null && um.patient__r.City__c != null && um.patient__r.State__c != null && um.patient__r.Zip__c != null);

        string letterText = '';

        letterText += '<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText += um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c + '<br/>';
        letterText += hasAddress ? um.patient__r.Address__c + '<br/>': '' + '<br/>';
        letterText += hasAddress ? um.patient__r.City__c + ', ': ', ';
        letterText += hasAddress ? um.patient__r.State__c + ' ' : ' ';
        letterText += hasAddress ? um.patient__r.Zip__c + '<br/><br/>': '' + '<br/><br/>';
        letterText += 'RE:&nbsp;&nbsp;Patient Name:&nbsp;' + um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c;
        letterText += '<br/>';
        letterText += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth:&nbsp;' + um.patient__r.Patient_Date_of_Birth__c;
        letterText += '</div>';
        letterText += '<p>';
        letterText += 'Dear&nbsp;';
        if (um.patient__r.Gender__c == 'Female') {
            letterText += 'Ms. ';
        }
        else if (um.patient__r.Gender__c == 'Male') {
            letterText += 'Mr. ';
        }

        letterText += um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c + ',';

        letterText += '</p>';
        letterText += '<p>';
        letterText += 'Contigo Health, LLC is a third-party administrator that performs benefit administration services for ' + um.patient__r.Patient_Employer__r.name + '\'s, self-funded group health plan (Plan). This letter is';
        letterText += ' in response to the request for the *** [service] *** at ' + um.facility_name__c + ' for ' + um.patient__r.patient_first_name__c + ' ' + um.patient__r.Patient_Last_Name__c + ' beginning on ' + um.admission_date__c.format() + '. *** [services denied] ***.';
        letterText += '</p>';

          //letterText+='<br/>';
        letterText += 'Diagnoses: ';
        if (diagnosisCodes.isEmpty()) {
            letterText += 'N/A';
        }
        else{
            for (Utilization_Management_Clinical_Code__c dc :diagnosisCodes) {
                letterText += dc.name + ' ' + dc.Description__c + ' ;';
            }
            letterText = letterText.removeEnd(';');
        }
        letterText += '<br/><br/>';
        letterText += 'Treatment: ';

        letterText += '<br/><br/>';
        letterText += 'Denial Reason:<br/><br/>';

        letterText += 'As per ' + um.patient__r.Patient_Employer__r.name + '\'s Summary Plan Description and Plan Document:&nbsp;';
        letterText += '<br/><br/>';

        letterText += 'Medical Benefit your Plan covers:&nbsp;';
        letterText += 'refer to the section titled *** [section title] ***';
        letterText += '<br/><br/>';
        letterText += '*** [SPD language quote] ***';
        letterText += '<br/><br/>';

        letterText += '<br/>';
        letterText += 'Specific Plan Exclusions:&nbsp;';
        letterText += '<br/><br/>';
        letterText += '*** [exclusion language quote] ***';
        letterText += '<br/><br/>';

        letterText += '<div style="font-style:italic">';
        letterText += 'All choices regarding the care and treatment of the patient remain the responsibility of the patient and the patient’s provider. In no way does this letter attempt to dictate the care the patient receives.';
        letterText += '</div>';

        letterText += '<p>';
        letterText += 'Contigo Health<br/>';
        letterText += '1755 Georgetown Rd<br/>';
        letterText += 'Hudson, OH 44236<br/>';
        letterText += '</p>';

        boolean hasClinician = (Clinician.Street__c != null && Clinician.City__c != null && Clinician.State__c != null && Clinician.Zip_Code__c != null);

        letterText += '</div><br/>';
        letterText += '<div>';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += 'CC:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;">';
        letterText += Clinician.First_Name__c + ' ' + Clinician.Last_Name__c + ', ' + Clinician.Credentials__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;"/>';
        letterText += hasClinician ? Clinician.Street__c : '';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;">';
        letterText += hasClinician ? Clinician.City__c + ', ' : ', ';
        letterText += hasClinician ? Clinician.State__c + ' ' : ' ';
        letterText += hasClinician ? Clinician.Zip_Code__c : '';
        letterText += '</div>';
        letterText += '</div><br/>';

        letterText += '<p>';
        letterText += 'This decision has been processed consistent with benefit terms and conditions described in the Plan’s Summary Plan Description and Plan Document. Contacting Customer Service at the telephone number listed on your medical ID card may resolve your questions. You have a right to';
        letterText += ' request, free of charge, a copy of any internal rule, guideline, protocol, or similar criteria used in this determination, an explanation of the scientific or clinical basis of the determination if the denial involves a medical necessity or experimental treatment limitation';
        letterText += ' or exclusion, the diagnosis and treatment codes related to this claim and their meanings, and any documents, records or other information relevant to this claim.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'You or your authorized representative have the right to appeal this decision. Your appeal must be sent in writing, along with any additional information, within 180 days of receipt of the denial to Contigo Health, LLC.,';
        letterText += 'Attention: Appeals Coordinator, 1755 Georgetown Road, Hudson Ohio 44236.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'For questions about your appeal rights, this notice, or for assistance, you can contact Customer Service at the telephone number listed on your medical ID card.';
        letterText += 'You also may contact the Employee Benefits Security Administration at 1-866-444-EBSA(3272). In addition, you may also reach out to the Ohio Department of Insurance at:<br/>';
        letterText += '</p>';

        letterText += '<p style="text-align:center">';
        letterText += 'Ohio Department of Insurance<br/>';
        letterText += 'ATTN:  Consumer Affairs<br/>';
        letterText += '50 West Town Street, Suite 300, Columbus, OH  43215<br/>';
        letterText += '800-686-1526 / 614-644-2673<br/>';
        letterText += '614-644-3744 (fax)<br/>';
        letterText += '614-644-3745 (TDD)<br/>';
        letterText += '</p>';

        letterText += '<p style="text-align:center">';
        letterText += 'Contact ODI Consumer Affairs:<br/>';
        letterText += '<span style="color:blue;text-decoration:underline;">https://secured.insurance.ohio.gov/ConsumServ/ConServComments.asp</span><br/>';
        letterText += 'File a Consumer Complaint:<br/>';
        letterText += '<span style="color:blue;text-decoration:underline;">http://insurance.ohio.gov/Consumer/OCS/Pages/ConsCompl.aspx</span><br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'Your employer’s health plan complies with applicable Federal civil rights laws and does not discriminate on the basis of race, color, national origin, age, disability or sex.<br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += '(SPANISH) ATENCIÓN: si habla español, tiene a su disposición servicios gratuitos de asistencia lingüística.  Llame al 1-330-656-1072. (TTY: 711)<br/><br/>';
        letterText += '(TAGALOG) PAUNAWA: Kung nagsasalita ka ng Tagalog, maaari kang gumamit ng mga serbisyo ng tulong sa wika nang walang bayad.  Tumawag sa 1-330-656-1072. (TTY: 711)<br/><br/>';
        letterText += '(CHINESE) <span style="font-family: Arial Unicode MS">(中文):  注意：如果您使用繁體中文，您可以免費獲得語言援助服務。請致電 </span> 1-330-656-1072 (TTY: 711)<br/><br/>';
        letterText += '(NAVAJO) (Díí baa akó nínízin:Díí saad bee yáníłti’go Diné Bizaad, saad bee áká’ánída’áwo’dę́ę́’, t’áá jiik’eh, éí ná hólq̨́, kojį́ hǫ́dį́į́lnih   1-330-656-1072<br/>';
        letterText += '</p>';
        letterText += '<br/><br/>';

        return letterText;
    }
}