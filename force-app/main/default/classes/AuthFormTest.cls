/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers for the Auth form controller.
*/
@isTest(seealldata=true)
private class AuthFormTest {

    static testMethod void AuthForm() {
        
        map<string, id> rtMap = new map<string, id>();
        
        client_facility__c[] cfs = [select client__r.name, client__c, procedure__c, procedure__r.name from client_facility__c];
        
        map<string, string> cfIdMap = new map<string, string>();
        map<string, string> procedureIdMap = new map<string, string>();
        
        for(client_facility__c cf : cfs){
            cfIdMap.put(cf.client__r.name, cf.client__c);
            procedureIdMap.put(cf.procedure__r.name, cf.procedure__c);
        }
        
        for(RecordType rt : [select id, name from RecordType where isActive = true and sObjectType='Patient_Case__c']){
            
            rtMap.put(rt.name, rt.id);
        }
        
        Patient_Case__c pc = new Patient_Case__c();
        pc.Program_Authorization_Received__c = date.today();
        pc.Plan_of_Care_accepted_at_HDP__c = date.today();
        pc.Financial_Responsibility_Waiver__c = date.today();
        pc.bid__c = '123456';
        pc.Employee_First_Name__c = 'Johnny';
        pc.Employee_Last_Name__c = 'Tester';
        pc.Patient_DOBe__c = string.valueof(date.today().addYears(-20));
        pc.Patient_Email_Address__c = 'something@something.com';
        pc.Employee_DOBe__c = string.valueof(date.today().addYears(-20));
        pc.Employee_Primary_Health_Plan_ID__c = '12345';
        pc.Relationship_to_the_Insured__c = 'Patient is Insured';
        pc.Patient_Preferred_Phone__c = 'Home';
        pc.Patient_Gender__c= 'Male'; 
        pc.Patient_City__c ='Mesa'; 
        pc.Patient_State__c='AZ';
        pc.recent_testing__c='rt';
        pc.caregiver_name__c ='Johnny Tester';
        pc.RecordTypeid = rtMap.get('Walmart Cardiac');
        pc.Client_Name__c = 'Walmart';
        pc.client__c = cfIdMap.get(pc.Client_Name__c);
        pc.ecen_procedure__c= procedureIdMap.get('Joint');
        
        blob cryptoKey = Crypto.generateAesKey(128);
        pc.attachEncrypt__c = EncodingUtil.base64encode(cryptoKey);
        
        insert pc;
        
        
        Attachment theAttachment = new Attachment();
        theAttachment.ParentId = pc.id;
        theAttachment.Name = 'Test';
        theAttachment.Description = 'Test';
        theAttachment.Body = blob.valueof('test');
        insert theAttachment;
        
        Program_Notes__c pn = new Program_Notes__c();       
        pn.Subject__c = 'Test';
        pn.isAuthEmailAttach__c = true;
        pn.attachment__c = true;
        pn.attachID__c = theAttachment.id;
        pn.Patient_Case__c = pc.id;
        insert pn;   
        
        ApexPages.currentPage().getParameters().put('Id', pc.id);
        AuthFormController afc = new AuthFormController();
        afc.attachID = theAttachment.id;
        afc.runrenderpdf();
        afc.updateInfo();
        afc.getURL();
        afc.addtheAttachment();
        afc.geterrorMessage();
        afc.formSelected();
        afc.theCC = 'something@hdplus.com; something@hdplus.com';
        afc.updateInfo();
        afc.formSelected();
        afc.gettheOpp();
        afc.encryptAttach();

        pc.RecordTypeId = rtMap.get('Walmart Spine'); 
        pc.ecen_procedure__c = procedureIdMap.get('Spine');
        update pc;
        AuthFormController afcws = new AuthFormController();  

        pc.RecordTypeId = rtMap.get('Walmart Joint'); 
        pc.ecen_procedure__c = procedureIdMap.get('Joint');
        update pc;
        
        AuthFormController afcwj = new AuthFormController(pc.id); 
        pc.RecordTypeId = rtMap.get('Lowes Spine');
        pc.ecen_procedure__c = procedureIdMap.get('Spine');
        pc.Client_Name__c = 'Lowes';
        pc.client__c = cfIdMap.get(pc.Client_Name__c);
        
        update pc;
        
        AuthFormController afcls = new AuthFormController(pc.id);
        pc.RecordTypeId = rtMap.get('Lowes Cardiac');
        pc.Client_Name__c = 'Lowes';
        pc.ecen_procedure__c = procedureIdMap.get('Cardiac');
        pc.client__c = cfIdMap.get(pc.Client_Name__c);
        
        update pc;
        
        AuthFormController afclc = new AuthFormController(pc.id);
        pc.RecordTypeId = rtMap.get('Lowes Joint');
        pc.ecen_procedure__c = procedureIdMap.get('Joint');
        pc.client__c = cfIdMap.get(pc.Client_Name__c);
        
        update pc;
        
        AuthFormController afclj = new AuthFormController(pc.id);
        pc.Client_Name__c = 'HCR ManorCare';
        pc.RecordTypeId = rtMap.get('HCR ManorCare Cardiac');
        pc.client__c = cfIdMap.get(pc.Client_Name__c);
        pc.ecen_procedure__c = procedureIdMap.get('Cardiac');
        update pc;
        
        AuthFormController afchmc = new AuthFormController(pc.id);
        
        pc.RecordTypeId = rtMap.get('Mckesson Joint');
        pc.Client_Name__c = 'HCR ManorCare';
        pc.client__c = cfIdMap.get(pc.Client_Name__c);
        pc.ecen_procedure__c = procedureIdMap.get('Joint');
        
        update pc;
        AuthFormController afcmj = new AuthFormController(pc.id);  
    }
}