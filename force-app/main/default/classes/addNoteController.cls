public with sharing class addNoteController {
    public sObject objTPN {
        get; set;
    }
    public sObject objTrans {
        get; set;
    }

    public Program_Notes__c objPN {
        get; set;
    }
    public string theID {
        get; set;
    }
    public string retURL {
        get; set;
    }
    public string theURL {
        get; set;
    }
    public string theNotes {
        get; set;
    }
    public string error {
        get; set;
    }
    public string theSubject {
        get; set;
    }
    public boolean ccNote {
        get; set;
    }
    public boolean authNote {
        get; set;
    }
    public string cancel {
        get; set;
    }
    public string message {
        get; set;
    }
    public boolean isError {
        get; set;
    }
    public boolean isSuccess {
        get; set;
    }
    public string pname {
        get; set;
    }
    public Datetime customCreatedDate {
        get; set;
    }
    public Patient_Case__c objPC;
    public string attachmentname {
        get; set;
    }
    public string noteType {
        get; set;
    }
    public string theSessID {
        get; set;
    }
    public string doReload {
        get; set;
    }
    private boolean testCC = false;
    string testNote = '';
    string testSubject;
    string testType;
    string testAttachment;
    string testInitiated = '';
    string testConType = '';
    string testCommType = '';
    private boolean testAuth;
    public transient Attachment endAttachment = new Attachment();
    public string coeFileDirectory {
        get; private set;
    }
    public string coeFileNamePrefix {
        get; private set;
    }

    public Attachment Attachment {
        get {
            if (attachment == null) {
                attachment = new Attachment();
            }
            return attachment;
        }

        set;
    }

    public void setct(){
    }

    public addNoteController(){
        theurl = URL.getSalesforceBaseUrl().toExternalForm();
        theID = ApexPages.currentPage().getParameters().get('Id');
        string objtype = utilities.getObjType(theID.left(3));

        if (objtype == 'Patient_Case__c') { //Patient Case Note
            retURL = '/' + theID;
            objPN = new Program_Notes__c(patient_case__c = theId);
            objPC = [select name, owner.name, client_facility__r.Referral_Email_Contact__c, attachEncrypt__c, authReady__c, employee_first_name__c, employee_last_name__c, patient_first_name__c, patient_last_name__c, patient_dobe__c, employee_dobe__c from Patient_Case__c where id =:theID];

            if (objPC.patient_first_name__c == null || objPC.patient_last_name__c == null) {
                pname = 'Add note - ' + objPC.employee_first_name__c + ' ' + objPC.employee_last_name__c;
            }
            else{
                pname = 'Add note - ' + objPC.patient_first_name__c + ' ' + objPC.patient_last_name__c;
                coeFileNamePrefix = objPC.patient_last_name__c + ', ' + objPC.patient_first_name__c + '_' + objPC.patient_dobe__c.mid(5, 2) + '_' + objPC.patient_dobe__c.right(2) + '_' + objPC.patient_dobe__c.left(4) + '/' + objPC.name;
                coeFileNamePrefix = string.escapeSingleQuotes(coeFileNamePrefix);
            }
            coeFileNamePrefix = documentStorageComponentController.safePath(coeFileNamePrefix);
            coeFileDirectory = PatientCase__c.getInstance().fileDirectory__c + '/' + coeFileNamePrefix;
        }
        error = 'false';
        cancel = 'false';
    }

    public List<SelectOption> getTypeItems() {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('--None--', '--None--'));
        options.add(new SelectOption('Call', 'Call'));
        options.add(new SelectOption('E-Mail', 'E-Mail'));
        options.add(new SelectOption('Note', 'Note'));
        options.add(new SelectOption('Auth. Form', 'Auth. Form'));
        options.add(new SelectOption('Other', 'Other'));
        return options;
    }

    public List<SelectOption> getTypeItemsTrans() {
        List<SelectOption> options = new List<SelectOption>();

        options.add(new SelectOption('--None--', '--None--'));
        options.add(new SelectOption('Call', 'Call'));
        options.add(new SelectOption('E-Mail', 'E-Mail'));
        options.add(new SelectOption('Fax', 'Fax'));
        options.add(new SelectOption('Voicemail', 'Voicemail'));
        options.add(new SelectOption('Conference Call', 'Conference Call'));
        options.add(new SelectOption('Note', 'Note'));
        options.add(new SelectOption('Review', 'Review'));
        return options;
    }

    public string getCancel(){
        return cancel;
    }

    public void addAttachment(){
        isError = false;
        string file_id = ApexPages.CurrentPage().getParameters().get('fileId');
        string file_name = [select fileName__c from ftpAttachment__c where id = :file_id].fileName__c;

        objPN.ftpAttachment__c = file_id;
        objPN.attachment__c = true;

        if (objPN.Communication_Type__c == 'Auth. Form') {
            objPN.isAuthEmailAttach__c = true;
        }

        if (objPN.Notes__c == null || objPN.Notes__c == '') {
            objPN.Notes__c = 'Attachment ' + file_name + ' added';
        }
        else{
            objPN.Notes__c = objPN.Notes__c + '\n\nAttachment ' + file_name + ' added';
        }
        update objPN;
    }

    public void addtheAttachment(){
        isError = false;
        isSuccess = false;
        cancel = '';
        updateInfo();
        system.debug(theID);
        if (attachment.body != null) {
            endAttachment = attachment;
            endattachment.ParentId = theID;
            attachment = null;
            blob myBlob = endattachment.Body;

            if (testSubject == null) {
                isError = true;
                message = 'Please add a subject';
            }
            else{
                if (testNote == null) {
                    testNote = 'Attachment ' + endattachment.Name + ' added.';
                }
                else{
                    testNote = testNote + '\n\n' + 'Attachment ' + endattachment.Name + ' added.';
                }

                try{
                    if (objPN.isContinineResults__c) {
                        string email;
                        if (utilities.isRunningInSandbox()) {
                            email = userinfo.getuseremail();
                        }
                        else{
                            email = objPC.client_facility__r.Referral_Email_Contact__c;
                        }
                        Messaging.SingleEmailMessage mail = utilities.email(email, null, 'mmartin@hdplus.com', Userinfo.getFirstName() + ' ' + Userinfo.getLastName(), 'Cotinine Results for ' + objPC.Patient_Last_Name__c + ', ' + objPC.Patient_First_Name__c, 'Hello,\n\nThe Cotinine test results for ' + objPC.Patient_Last_Name__c + ', ' + objPC.Patient_First_Name__c + ' have been attached.\n\nThank You,\n\n' + Userinfo.getFirstName() + ' ' + Userinfo.getLastName());
                        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                        efa.setFileName(endAttachment.name);
                        efa.setBody(endAttachment.body);
                        mail.setFileAttachments(new Messaging.EmailFileAttachment[] { efa });
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                    }

                    isSuccess = true;
                    message = 'Upload Successful';
                    blob Key;
                    if (objPC.attachEncrypt__c == null) {
                        Key = Crypto.generateAesKey(128);
                        objPC.attachEncrypt__c = EncodingUtil.base64encode(Key);
                        key = EncodingUtil.base64Decode(objPC.attachEncrypt__c);
                        update objPC;
                    }
                    else{
                        key = EncodingUtil.base64Decode(objPC.attachEncrypt__c);
                    }

                    insert endattachment;
                    Program_Notes__c pn = new Program_Notes__c();
                    pn.patient_case__c = theID;
                    pn.Initiated__c = testInitiated;
                    pn.Communication_Type__c = testCommType;
                    pn.Contact_Name__c = testConType;
                    pn.subject__c = testSubject;
                    pn.notes__c = testNote;
                    pn.type__c = testType;
                    pn.attachment__c = true;
                    pn.attachID__c = endattachment.id;
                    pn.Clinical_Note__c = testCC;

                    if (testAuth) {
                        pn.isAuthEmailAttach__c = testAuth;
                    }

                    encryptAttach ea = new encryptAttach(EncodingUtil.base64Decode(objPC.attachEncrypt__c));
                    blob thebody = endattachment.body;
                    blob decryptedbody;
                    decryptedbody = ea.encryptAttachmentBlob(thebody);
                    endattachment.body = decryptedbody;
                    update endattachment;
                    attachment = null;
                    insert pn;
                    if (testAuth == true) {
                        objPC.authReady__c = true;
                        update objPC;
                    }
                    endattachment = null;
                    objPN.Subject__c = null;
                    objPN.notes__c = null;
                    objPN.Clinical_Note__c = null;
                    objPN.isAuthEmailAttach__c = null;
                    noteType = null;
                    testNote = theNotes;
                }catch (exception e) {
                    isError = true;
                    error = 'error1';
                    system.debug(e.getMessage() + ' ' + e.getLineNumber());
                    if (e.getMessage().contains('_EXCEPTION')) {
                        message = utilities.errorMessageText(e.getMessage());
                    }
                    else{
                        message = 'REQUIRED FIELD MISSING: ' + utilities.errorMessageTextRF(e.getMessage());
                    }
                }
            }
        }
        else{
            isError = true;
            message = 'No file chosen';
        }
    }

    public void updateInfo(){
        if (objPN == null) {
            Program_Notes__c temp = new Program_Notes__c();
            objPN = temp;
        }

        testInitiated = objPN.Initiated__c;
        testConType = objPN.Contact_Type__c;
        testCommType = objPN.Communication_Type__c;
        testSubject = objPN.Subject__c;
        testNote = objPN.notes__c;
        testCC = objPN.Clinical_Note__c;
        testAuth = objPN.isAuthEmailAttach__c;

        if (testCommType == 'Auth. Form') {
            testAuth = true;
        }
        testType = noteType;
    }

    public void testtheAttach(){
        testAttachment = attachment.name;
    }

    public void mySave(){
        isError = false;
        message = '<br>';

        if (objPN.Subject__c == null) {
            message += 'The subject field must be populated before saving.<br>';
        }

        Profile p = [select name from Profile where id = :userInfo.getProfileId()];
        if (!p.name.tolowercase().contains('customer service')) {
            if (objPN.Contact_Type__c == null) {
                message += 'The contact type field must be populated before saving.<br>';
            }

            if (objPN.Contact_Type__c != null && objPN.Contact_Type__c != 'Other') {
                if (objPN.communication_type__c == null) {
                    message += 'The communication type must be populated before saving.<br>';
                }
                if (objPN.initiated__c == null) {
                    message += 'The initiated field must be populated before saving.<br>';
                }
            }
        }

        if (message == '<br>') {
            try{
                objPN.Patient_Case__c = id.valueof(theID);
                isError = false;
                upsert objPN;
            }catch (exception e) {
                isError = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            }
        }
        else{
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, message));
        }
    }

    public void theCancel(){
        if (objPN.type__c == null || objPN.notes__c.length() <= 0) {
            cancel = 'true';
        }
    }
}