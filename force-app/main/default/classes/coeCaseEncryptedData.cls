public with sharing class coeCaseEncryptedData{
        
        public string clientName {get; set;}
        
        public string patientName {get; set;}
        
        public string patientFirstName {get; set;}
        public string patientLastName {get; set;}
        public string patientDOB {get; set;}
        
        public string employeeFirstName {get; set;}
        public string employeeLastName {get; set;}
        public string employeeDOB {get; set;}
        
        public string patientStreet {get; set;}
        public string patientCSZ {get; set;}
        public string patientCity {get; set;}
        public string patientState {get; set;}
        public string patientZip {get; set;}
        
        public string employeeStreet {get; set;}
        public string employeeCSZ {get; set;}
        public string employeeCity {get; set;}
        public string employeeState {get; set;}
        public string employeeZip {get; set;}
        
        public string employeeEmail {get; set;}
        public string patientEmail {get; set;}
        
        
        public string facility {get; set;}
        public string logoUrl {get; set;}
        public string BID {get; set;}
        public string Grp {get; set;}
        public string planType {get; set;}
        public string id {get; set;}
        public string effDate {get; set;}
        public string termDate {get; set;}
        public boolean isCNSA {get;set;}
        public double patientBMI {get;set;}
        
    }