@IsTest
public class googleMapsStructTest {
    
    static testMethod void testDirectionsParse() {
        String json = '{ \"destination_addresses\" : [ \"2401 S 31st St, Temple, TX 76508, USA\" ],\"origin_addresses\" : [ \"535 Live Oak Dr, Bertram, TX 78605, USA\" ],\"rows\" : [{\"elements\" : [{\"distance\":{\"text\" :\"57.6 mi\",\"value\":92714},               \"duration\" : {                  \"text\" : \"1 hour 7 mins\",                  \"value\" : 3995               },               \"status\" : \"OK\"            }        ]      }   ],   \"status\" : \"OK\"}';
        googleMapsStruct obj = googleMapsStruct.parse(json);
        System.assert(obj != null);
    }
    
    static testMethod void testGeoCodeParse() {
        String json = '{\"results\" : [{\"address_components\" : [{\"long_name\" : \"9888\",\"short_name\" : \"9888\",\"types\" : [ \"street_number\" ]},{\"long_name\" : \"Genesee Avenue\",\"short_name\" : \"Genesee Ave\",\"types\" : [ \"route\" ]},{\"long_name\" : \"La Jolla\",\"short_name\" : \"La Jolla\",\"types\" : [ \"neighborhood\", \"political\" ]},{\"long_name\" : \"San Diego\",\"short_name\" : \"San Diego\",\"types\" : [ \"locality\", \"political\" ]},{\"long_name\" : \"San Diego County\",\"short_name\" : \"San Diego County\",\"types\" : [ \"administrative_area_level_2\", \"political\" ]},{\"long_name\" : \"California\",\"short_name\" : \"CA\",\"types\" : [ \"administrative_area_level_1\", \"political\" ]},{\"long_name\" : \"United States\",\"short_name\" : \"US\",\"types\" : [ \"country\", \"political\" ]},{\"long_name\" : \"92037\",\"short_name\" : \"92037\",\"types\" : [ \"postal_code\" ]},{\"long_name\" : \"1205\",\"short_name\" : \"1205\",\"types\" : [ \"postal_code_suffix\" ]}],\"formatted_address\" : \"9888 Genesee Ave, La Jolla, CA 92037, USA\",\"geometry\" : {\"bounds\" : {\"northeast\" : {\"lat\" : 32.8858262,\"lng\" : -117.2245414},\"southwest\" : {\"lat\" : 32.8844893,\"lng\" : -117.2265437}},\"location\" : {\"lat\" : 32.8851544,\"lng\" : -117.2255383},\"location_type\" : \"ROOFTOP\",\"viewport\" : {\"northeast\" : {\"lat\" : 32.88650673029149,\"lng\" : -117.2241935697085},\"southwest\" : {\"lat\" : 32.88380876970849,\"lng\" : -117.2268915302915}}},\"place_id\" : \"ChIJYYgAY94G3IAR6ONDjnx6GAw\",\"types\" : [ \"premise\" ]}],\"status\" : \"OK\"}';
        googleMapsStruct obj = googleMapsStruct.parse(json);
        System.assert(obj != null);
    }
    
    static testMethod void testGoogleMaps() {
        googlemaps.returnLatLong('unittest');
        googlemaps.returnDistance('unittest', 'unittest');
    }
}