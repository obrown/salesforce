public class patientAgeSyncPopHealth {
    public void run(){
        set<id> idSet = new set<id>();
        integer i = 0;

        for (phm_Patient__c p : [select id, name from phm_Patient__c]) {
            idSet.add(p.id);

            if (i == 99) {
                populationHealthFuture.syncPopHealth(idSet);
                idSet.clear();
                i = 0;
            }
            else{
                i++;
            }
        }
        populationHealthFuture.syncPopHealth(idSet);
    }
}