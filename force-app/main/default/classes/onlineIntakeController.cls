public without sharing class onlineIntakeController {

    public string empName       {get;set;}
    public string pt            {get;set;}
    public string ptlbl         {get;set;}
    public string bid           {get; private set;}
    public string jsonString    {get;set;}
    public string projsonString {get;set;}
    public string noteString    {get;set;}
    public string caseType      {get;set;}
    public string pcName        {get;private set;}
    public string imageVar      {get;private set;}
    public string employeeLabel {get;private set;}
    public string pass          {get;set;}
    public string csNumber      {get;private set;}
    public boolean showDomesticPartner {get;set;}
    public selectOption[] facilities {get;set;}
    map<id, Client_Facility__c> facilityMap = new map<id, Client_Facility__c>();
    public Client_Facility__c facility {get; private set;}
    
    public string maQuestionnaireJSON {get; set;}
    Ecen_Mobile_App_Questionnaire__c maQuestionnaire;
    string selectedFacility;
    public void setFacility(){
        selectedFacility = ApexPages.CurrentPage().getParameters().get('selectedFacility');
        facility=facilityMap.get(selectedFacility);
    }
    
    public onlineIntakeController (){
        
        empName='';pt='';pcName='';pass='0';employeeLabel='Employee';
        showDomesticPartner=true;
        maQuestionnaire  = new Ecen_Mobile_App_Questionnaire__c();
        
        String BaseURL = site.getBaseUrl();
        
        if(BaseURL != null){
             string[] bulist = BaseURL.split('/');
             if(bulist!=null && !bulist.isempty()){
                 string test = bulist[bulist.size()-1];
                 test = test.tolowercase();
                 if(test=='jbs'){
                    empName='JBS';
                    checkEmpName();
                 }
             }
             
        }
        
    }
    
    public void checkEmpName(){
        
        showDomesticPartner=true;
        
        if((empName.tolowercase().contains('w') && empName.tolowercase().contains('a') && empName.tolowercase().contains('l')) || 
           (empName.tolowercase().contains('r') && empName.tolowercase().contains('r') && empName.tolowercase().contains('m')))
        {
            empName = ' Walmart';
            pt = 'Heart;Back/Neck;Knee/Hip';
            bid = 'Benefit ID Number';
            imageVar = '/resource/1430293870000/logos/logos/WalmartLogo.png';
            pass='1';
            csNumber ='(877) 230-7037';
            employeeLabel='Associate';
                    
        }else if(empName.tolowercase().contains('l') && empName.tolowercase().contains('o') && empName.tolowercase().contains('w') && empName.tolowercase().contains('e')){
            empName = ' Lowe';
            pt = 'Heart;Back/Neck;Knee/Hip;Substance Use Disorder';
            bid = 'Sales ID';
            imageVar = '/resource/1430293870000/logos/logos/LowesLogo.jpg';
            pass='1';
            csNumber ='(877) 230-0994';
            
                       
            facilities=coeSelfAdminPickList.selectOptionList(null, [select id from client__c where name='Lowes'].id, [select id from procedure__c where name='Substance Disorder'].id, null, coeSelfAdminPickList.referredFacility, null, true);
            system.debug('facilities '+facilities);
            for(selectOption so : facilities){
                if(so.getValue()!=''){
                    facilityMap.put(so.getValue(), null);
                }
            }
        
            for(Client_Facility__c f : [select Facility__r.Facility_Address__c,
                                           Facility__r.Facility_City__c,
                                           Facility__r.Facility_State__c,
                                           Facility__r.Facility_Zip__c,
                                           Facility__r.Latitude__c,
                                           Facility__r.Longitude__c from Client_Facility__c  where id in :facilityMap.keySet()]){
                facilityMap.put(f.id, f);
            }
            
        }else if(empName.tolowercase().contains('m') && empName.tolowercase().contains('c') && empName.tolowercase().contains('s') && empName.tolowercase().contains('k')){
            empName = ' McKesson';
            pt = 'Knee/Hip';
            bid = 'Employee ID';
            imageVar = '/resource/1430293870000/logos/logos/McKessonLogo.png';
            pass='1';
            csNumber ='(800) 656-1064';
        }else if(empName.tolowercase().contains('j') && empName.tolowercase().contains('e') && empName.tolowercase().contains('t') && empName.tolowercase().contains('b')){
            empName = ' jetBlue';
            pt = 'Spine';
            bid = 'Crew Member ID';
            imageVar = '/resource/1430293870000/logos/logos/jetBlue.jpg';
            pass='1';
            csNumber ='(844) 207-8125';
        
        }else if(empName.tolowercase().contains('n') && empName.tolowercase().contains('x') && empName.tolowercase().contains('t') && empName.tolowercase().contains('e') && empName.tolowercase().contains('r')&& empName.tolowercase().contains('a')){
            empName = ' NextEra Energy';
            pt = ';Bariatric;Back/Neck;Knee/Hip;Oncology';
            ptlbl = ';Bariatric;Spine Care;Hip and Knee Replacement;Oncology';        
            bid = 'Cigna Medical ID';
            imageVar = '/resource/1430293870000/logos/logos/nextEraLogo.jpg';
            pass='1';
            csNumber ='(877) 230-0988';
            showDomesticPartner=false;
        }else if(empName.tolowercase().contains('l') && empName.tolowercase().contains('e') && empName.tolowercase().contains('g') && empName.tolowercase().contains('e') && empName.tolowercase().contains('t')&& empName.tolowercase().contains('a') && empName.tolowercase().contains('p')){
            empName = ' Leggett & Platt';
            pt = 'Heart;Back/Neck;Knee/Hip';
            ptlbl = 'Cardiac;Spine Care;Hip or Knee Replacement';        
            bid = 'Medical Plan ID';
            imageVar = '/resource/1430293870000/logos/logos/leggetplat.jpg';
            pass='1';
            csNumber ='(877) 230-7041';
            showDomesticPartner=true;
            employeeLabel='Employee';
        }else if(empName.tolowercase().contains('j') && empName.tolowercase().contains('b') && empName.tolowercase().contains('s')){
            empName = ' JBS';
            pt = 'Back/Neck';
            ptlbl = 'Spine Care';        
            bid = 'Employee ID';
            imageVar = '/resource/1564668358000/jbsLogo';
            pass='1';
            csNumber ='(844) 315-7965';
            showDomesticPartner=true;
            employeeLabel='Employee';
        //}else if(empName.tolowercase().contains('d') && empName.tolowercase().contains('o') && empName.tolowercase().contains('l')){
        //    empName = ' Dollar General';
        //    pt = 'Heart;Back/Neck;Knee/Hip';
        //    ptlbl = 'Cardiac;Spine Care;Hip or Knee Replacement';
        //    bid = 'Medical Plan ID';
        //    imageVar = '/resource/1430293870000/logos/logos/DGLogo.jpg';
        //    pass='1';
        //    csNumber ='(833) 867-3699';
        }else{
            pass = '2';
            empName='';
        }    
        
    }
    
    public void mySubmit(){
        try{
        
        //Map<String, Object> fieldValueMap = new Map<String, Object>();
        //string caseType='';
        string phone='';
        
        Patient_case__c pc = new Patient_Case__c();
        
        Provider__c pro = new Provider__c();
        Program_Notes__c note = new Program_Notes__c();
        
        try{
            pro = (Provider__c)Json.deserialize(projsonString, Provider__c.class);
        }catch(exception e){
            Messaging.SingleEmailMessage theMessage = new Messaging.SingleEmailMessage();    
            list<String> finalTo = new list<String>();
            finalTo.add('mmartin@hdplus.com');
            theMessage.setSenderDisplayName('donotreply@hdplus.com');
            theMessage.setInReplyTo('donotreply@hdplus.com'); 
            theMessage.setToAddresses(finalTo);
            theMessage.setPlainTextBody(e.getLineNumber()+' '+e.getMessage()+'\n\n'+projsonString);
            theMessage.setSubject('projsonstring');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { theMessage});
        }
        
        try{
          jsonString = jsonString.replaceAll('[""]','""'); 
          jsonString = jsonString.replaceAll('""','"'); 
          pc = (Patient_Case__c)Json.deserialize(jsonString, Patient_Case__c.class);
        }catch(exception e){
            Messaging.SingleEmailMessage theMessage = new Messaging.SingleEmailMessage();    
            list<String> finalTo = new list<String>();
            finalTo.add('mmartin@hdplus.com');
            theMessage.setSenderDisplayName('donotreply@hdplus.com');
            theMessage.setInReplyTo('donotreply@hdplus.com'); 
            theMessage.setToAddresses(finalTo);
            theMessage.setPlainTextBody(e.getLineNumber()+' '+e.getMessage()+'\n\n'+jsonString);
            theMessage.setSubject('pc');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { theMessage});
        }
        
        try{
          note = (Program_Notes__c)Json.deserialize(noteString, Program_Notes__c.class);  
        }catch(exception e){
            Messaging.SingleEmailMessage theMessage = new Messaging.SingleEmailMessage();    
            list<String> finalTo = new list<String>();
            finalTo.add('mmartin@hdplus.com');
            theMessage.setSenderDisplayName('donotreply@hdplus.com');
            theMessage.setInReplyTo('donotreply@hdplus.com'); 
            theMessage.setToAddresses(finalTo);
            theMessage.setPlainTextBody(e.getLineNumber()+' '+e.getMessage()+'\n\n'+noteString);
            theMessage.setSubject('note');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { theMessage});
        }
        
        if(caseType=='Back/Neck'){
            caseType='Spine';
        }else if(caseType=='Knee/Hip'){
            caseType='Joint';
        }else  if(caseType=='Heart'){
            caseType='Cardiac';
                               
        }
    
        PatientCase__c rt = PatientCase__c.getInstance();
         
         try{
             client__c client = [select id from client__c where name = 'Walmart' limit 1];
             pc.client__c = client.id;
         
         }catch(exception e){}
         
         if(empName.trim()=='Walmart'){
             try{
             client__c client = [select id from client__c where name = 'Walmart' limit 1];
             pc.client__c = client.id;
         
             }catch(exception e){}
             pc.client_name__c=empName;
             if(caseType=='Cardiac'){
                 pc.recordtypeid=rt.wmCardiac__c;
                 
             }else if(caseType=='Joint'){
                 pc.recordtypeid=rt.wmJoint__c;
                 
             }else if(caseType=='Spine'){
                 pc.recordtypeid=rt.wmSpine__c;
            
             }
         }else if(empName.trim()=='Lowe'){
             try{
             client__c client = [select id from client__c where name = 'Lowes' limit 1];
             pc.client__c = client.id;
         
             }catch(exception e){}
             pc.client_name__c='Lowes';
             
             switch on caseType{
                 when 'Cardiac'{
                     pc.recordtypeid=rt.lowesCardiac__c;
                 }
                 when 'Joint'{
                     pc.recordtypeid=rt.lowesJoint__c;
                 }
                 when 'Spine'{
                     pc.recordtypeid=rt.lowesSpine__c;
                 }
                 when 'Substance'{
                     pc.recordtypeid=rt.lowesSubstance__c;
                 }
             }
             
             
         }else if(empName.trim()=='McKesson'){
             try{
             client__c client = [select id from client__c where name = 'McKesson' limit 1];
             pc.client__c = client.id;
         
             }catch(exception e){}
             pc.client_name__c='McKesson';
             
             if(caseType=='Cardiac'){
                 pc.recordtypeid=rt.mckessonJoint__c;
             }else if(caseType=='Joint'){
                 pc.recordtypeid=rt.mckessonJoint__c;
             }else if(caseType=='Spine'){
                 pc.recordtypeid=rt.mckessonJoint__c;
             } 
         }else if(empName.trim()=='jetBlue'){
             try{
             client__c client = [select id from client__c where name = 'jetBlue' limit 1];
             pc.client__c = client.id;
         
             }catch(exception e){}
             pc.client_name__c='jetBlue';
             pc.recordtypeid=rt.jetBlueSpine__c;
              
         }else if(empName.trim()=='NextEra Energy'){
             try{
             client__c client = [select id from client__c where name = 'NextEra Energy' limit 1];
             pc.client__c = client.id;
         
             }catch(exception e){}
             pc.client_name__c='NextEra Energy';
             if(caseType=='Bariatric'){
                 //pc.recordtypeid=rt.lowesCardiac__c;
             }else if(caseType=='Joint'){
                 pc.recordtypeid=rt.nextEraEnergyJoint__c;
             }else if(caseType=='Spine'){
                 pc.recordtypeid=rt.nextEraEnergySpine__c;
             }    
              
         }else if(empName.trim()=='Leggett & Platt'){
             try{
             client__c client = [select id from client__c where name = 'Leggett & Platt' limit 1];
             pc.client__c = client.id;
         
             }catch(exception e){}
             if(caseType=='Joint'){
                 pc.recordtypeid=rt.LeggettPlattJoint__c;
             }else if(caseType=='Spine'){
                 pc.recordtypeid=rt.LeggettPlattSpine__c;
             }
             
        }else if(empName.trim()=='JBS'){
             try{
                 client__c client = [select id, name from client__c where name = 'JBS' limit 1];
                 pc.client__c = client.id;
                 pc.client_name__c = 'JBS';
         
             }catch(exception e){}
             
             pc.recordtypeid=rt.jbsSpine__c;
         //11-12-20 added     
         }else if(empName.trim()=='Dollar General'){
             try{
                 client__c client = [select id, name from client__c where name = 'Dollar General' limit 1];
                 pc.client__c = client.id;
                 pc.client_name__c = 'Dollar General';
              }catch(exception e){}  
              
              if(caseType=='Joint'){
                 pc.recordtypeid=rt.DollarGeneralJoint__c;
             }else if(caseType=='Spine'){
                 pc.recordtypeid=rt.DollarGeneralSpine__c;
             }else if(caseType=='Cardiac'){
                 pc.recordtypeid=rt.DollarGeneralCardiac__c;
             }            
         }               
        
        if(caseType=='Cardiac'){
            pc.ecen_procedure__c= jcsSettings__c.getInstance().cardiac__c;
                 
        }else if(caseType=='Joint'){
            pc.ecen_procedure__c=jcsSettings__c.getInstance().joint__c;
            
        }else if(caseType=='Spine'){
            pc.ecen_procedure__c=jcsSettings__c.getInstance().spine__c;
        } 
          
        if(pc.Relationship_to_the_Insured__c=='Patient is Insured'){
              pc.patient_Mobile__c = pc.Employee_Mobile__c;
              pc.patient_Home_Phone__c= pc.Employee_Home_Phone__c;
              pc.patient_Work_Phone__c= pc.Employee_Work_Phone__c;
        }
        
        pc.status__c = 'Open';  
        pc.status_reason__c = 'New - Web Intake';
        pc.caller_name__c = 'Web Intake';   
        
        sObject[] submitCount = new sObject[]{};
        if(caseType=='Bariatric'){
            submitCount = [select id from Patient_case__c where createddate >= :datetime.now().addhours(-1)];    
        }else{
            submitCount = [select id from Bariatric__c where createddate >= :datetime.now().addhours(-1)];    
        }
        
        if(submitCount.size()<100){    
            SystemID__c sid = SystemID__c.getInstance();  
            pc.ownerid = sid.IntakeSpecialistQueue__c; 
            
            if(caseType=='Bariatric'){
                finishBariatric(pc);
                return;
            }
            
            try{
            /* Create or find employee contact */
             employeeContacts__c[] newIntakeSearchResults = new employeeContacts__c[]{};
             string emp_st = '';
             try{
                 emp_st =pc.employee_last_name__c.left(1)+''+pc.employee_last_name__c.right(1);
             }catch(exception e){}
             
             string pat_st = ''; 
             
             try{
                 pat_st =pc.patient_last_name__c.left(1)+''+pc.patient_last_name__c.right(1);
             }catch(exception e){}
             
             for(employeeContacts__c ec : [select firstname__c, lastname__c,DOB__c,EmailAddress__c,StreetAddress__c,City__c,State__c, ZipCode__c,EmployeeRelationShip__r.name from employeeContacts__c where (searchHelper__c=:pat_st or searchHelper__c=:emp_st)  and EmployeeRelationShip__r.name!='Caregiver']){
                 if(pc.employee_last_name__c==ec.lastname__c || pc.employee_last_name__c==ec.lastName__c){
                         newIntakeSearchResults.add(ec);
                 }
             }
             if(newIntakeSearchResults.isEmpty()){
                 //Create the new employee contact records
                 pc.employeeID__c = createEmployeeContact(pc);
                 if(pc.Relationship_to_the_Insured__c=='Patient is Insured'){
                     pc.patientID__c = pc.employeeID__c;
                 }else{
                     pc.patientID__c = createPatientContact(pc);
                 }
                 
             }else{
                 for(employeeContacts__c ec : newIntakeSearchResults){
                     
                     if(ec.firstName__c.toLowerCase()== pc.employee_first_name__c.toLowerCase() && ec.lastName__c.toLowerCase()== pc.employee_last_name__c.toLowerCase() && pc.employee_dobe__c==ec.dobe__c ){
                         pc.employeeID__c = ec.id;
                     }
                     
                     if(pc.Relationship_to_the_Insured__c=='Patient is Insured'){
                         pc.patientID__c = ec.id;
                         break;
                         
                     }else{
                         if(ec.firstName__c.toLowerCase()== pc.patient_first_name__c.toLowerCase() && ec.lastName__c.toLowerCase()== pc.patient_last_name__c.toLowerCase() && pc.patient_dobe__c==ec.dobe__c ){
                             pc.patientID__c = ec.id;
                             break;
                         }
                     }
                 }
                
                 if(pc.employeeID__c == null){
                    pc.employeeID__c = createEmployeeContact(pc);     
                    if(pc.Relationship_to_the_Insured__c=='Patient is Insured'){
                        pc.patientID__c = pc.employeeID__c;
                         
                    }else{
                        pc.patientID__c =createPatientContact(pc);
                    }
                 }
                
                 if(pc.patientID__c == null){
                     pc.patientID__c =createPatientContact(pc);
                 }
                                  
             }
            }catch(exception e){
                system.debug(e.getMessage());
            }
            try{
                
                if(selectedFacility!=null && selectedFacility !=''){
                    pc.client_facility__c=selectedFacility;
                }
                
                insert pc;
                system.debug(pc.id);
            }catch(dmlexception e){
                pcName = 'An error has occurred ';
                for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                pcName += '\n\n'+e.getDmlMessage(i);
                
                System.debug(e.getDmlMessage(i)); 
                }
            }
            
            if(pro.First_Name__c !='' && pro.First_Name__c !=null){
                pro.patient_case__c = pc.id;
                try{
                    insert pro;
                }catch(exception e){
                    system.debug(e.getMessage()+' '+ e.getLineNumber());
                }
            }
            
            if(note.Notes__c!= null && note.Notes__c!= ''){
                note.patient_case__c = pc.id;
                note.subject__c = 'Web Intake Notes';
                try{
                    insert note;
                }catch(exception e){
                    system.debug(e.getMessage()+' '+ e.getLineNumber());
                }
                
            }
            
            if(empName!=' JBS' && caseType!='Substance'){
            maQuestionnaire = (Ecen_Mobile_App_Questionnaire__c)Json.deserialize(maQuestionnaireJSON, Ecen_Mobile_App_Questionnaire__c.class);
            maQuestionnaire.Patient_Case__c = pc.id;
            
            if(maQuestionnaire.Phone_OS__c=='--'){
                maQuestionnaire.Phone_OS__c=null;
            }
                insert maQuestionnaire;
            }
            
            pcName = [select patient_case__c from patient_case__c where id = :pc.id].patient_case__c;
             system.debug(pcName);
            if((pc.Employee_Email_Address__c!=''&&pc.Employee_Email_Address__c!=null) || (pc.Patient_Email_Address__c!=null && pc.Patient_Email_Address__c!='')){
        
            Messaging.SingleEmailMessage theMessage = new Messaging.SingleEmailMessage();    
            list<String> finalTo = new list<String>();
            if(pc.Employee_Email_Address__c!=null && pc.Employee_Email_Address__c!=''){
                finalTo.add(pc.Employee_Email_Address__c);
            }
            
            if(pc.Patient_Email_Address__c!=null && pc.Patient_Email_Address__c!=''){
                finalTo.add(pc.Patient_Email_Address__c);
            }
            
            theMessage.setSenderDisplayName('donotreply@hdplus.com');
            theMessage.setInReplyTo('donotreply@hdplus.com'); 
            theMessage.setToAddresses(finalTo);
            string subject = 'Your online intake for case number ' + pcName;
            string body = 'Hello '+pc.Employee_First_name__c+' '+pc.Employee_Last_name__c+',\n\n';
            body += 'Your case number for your online intake is '+pcName+'. An intake specialist from Health Design Plus will contact you shortly at the phone number(s) provided. In the meantime, gathering the following information will help facilitate the process:\n\n';
            body += '- Caregiver Name\n- Caregiver Phone\n\n';
            body += '- Physician Name\n- Physician Address\n- Physician Phone and Fax number\n\n';
            body += 'If you have any questions, please contact us at (800) 656-1064\n\nThanks!';
            theMessage.setPlainTextBody(body);
            theMessage.setSubject(subject);
            theMessage.setSenderDisplayName('donotreply@hdplus.com');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { theMessage});
                        
            }
            
        }else{
            pcName = 'An error has occurred. You have submitted too many online intakes, please call our customer service line';
        }
        }catch(exception e){
           // pcName = 'An error has occurred ' + e.getMessage();
            system.debug(pcName);
            system.debug(e.getMessage()+' '+ e.getLineNumber());
        }
    }
    
    void finishBariatric(patient_case__c pc){
        bariatric__c b = new bariatric__c();
        b.client__c = [select id from client__c where name=:empName.trim() limit 1].id;
        b.bid__c = pc.bid__c;
        b.employee_first_name__c = pc.employee_first_name__c;
        b.employee_last_name__c = pc.employee_last_name__c;
        if(pc.employee_dob__c!=null){
            b.employee_dob__c = string.valueof(pc.employee_dob__c);
        }
        b.Employee_Email_Address__c  =pc.Employee_Email_Address__c ;
        b.Patient_Email_Address__c =pc.Patient_Email_Address__c ;
        b.Employee_Gender__c = pc.Employee_Gender__c;
        b.Employee_Mobile_Phone__c= pc.Employee_Mobile__c ;
        b.Employee_Home_Phone__c =pc.Employee_Home_Phone__c ;
        b.Relationship_to_the_Insured__c=pc.Relationship_to_the_Insured__c;
        b.Employee_Street__c =pc.Employee_Street__c;
        b.Employee_City__c =pc.Employee_City__c;
        b.Employee_State__c =pc.Employee_State__c;
        b.Employee_Zip_Code__c=pc.Employee_Zip_Postal_Code__c;
        
        if(b.Relationship_to_the_Insured__c !='Patient is Insured'){
            b.patient_first_name__c=pc.employee_first_name__c;
            b.patient_last_name__c=pc.employee_last_name__c;
            if(pc.patient_dob__c!=null){
                b.patient_dob__c=string.valueof(pc.patient_dob__c);
            }
            b.patient_Gender__c=pc.patient_Gender__c;
            b.patient_Email_Address__c =pc.patient_Email_Address__c ;
            b.Patient_Mobile_Phone__c =pc.Patient_Mobile__c ;
            b.Patient_Home_Phone__c =pc.Patient_Home_Phone__c ;
            b.Patient_Street__c =pc.Patient_Street__c ;
            b.Patient_City__c =pc.Patient_City__c ;
            b.Patient_State__c =pc.Patient_State__c ;
            b.Patient_Zip_Code__c=pc.Patient_Zip_Postal_Code__c ;
        }else{
            b.patient_first_name__c=pc.employee_first_name__c;
            if(pc.employee_dob__c!=null){
                b.patient_dob__c=string.valueof(pc.employee_dob__c);
            }
            b.patient_Gender__c=pc.employee_Gender__c;
            b.patient_Email_Address__c =pc.employee_Email_Address__c ;
            b.Patient_Mobile_Phone__c =pc.employee_Mobile__c ;
            b.Patient_Home_Phone__c =pc.employee_Home_Phone__c ;
            
        
        }
        
        b.Patient_Weight__c = pc.Patient_Weight__c; 
        b.Current_Nicotine_User__c = pc.Current_Nicotine_User__c;
        b.Patient_Height_FT__c = pc.Patient_Height_FT__c;
        b.Patient_Height_Inches__c = pc.Patient_Height_Inches__c;
        
        
        try{
            Provider__c pro = (Provider__c)Json.deserialize(projsonString, Provider__c.class);
            b.Provider_Name_Credentials__c = pro.first_name__c +' '+pro.last_name__c;
            b.Provider_Street__c =pro.street__c ;
            b.Provider_City__c =pro.city__c;
            b.Provider_State__c =pro.state__c;
            b.Provider_Zip_Code__c =pro.Zip_Postal_Code__c ;    
            b.Provider_Phone__c =pro.Phone_Number__c ;
            b.Provider_Fax__c =pro.Fax_Number__c ;
            
        }catch(exception e){
            //catch silently
        }
        
        insert b;
        
        pcName = [select name from bariatric__c where id = :b.id].name;
        
        /*
        if(pc.Initial_Call_Discussion__c!=null && pc.Initial_Call_Discussion__c !=''){
            Bariatric_Note__c bNote = new Bariatric_Note__c(bariatric__c=b.id);
            bNote.subject__c = 'Web Intake Note';
            bNote.Note__c = pc.Initial_Call_Discussion__c;
            insert bNote;
        }
        */
    }
    
    private boolean isBoolean(string foo){
            
            try{
                if(foo.tolowercase()=='true'||foo.tolowercase()=='false'){
                        return true;
                }
                return false;
            
            }catch(exception e){
                return false;
            }

     }
     
     id createEmployeeContact(patient_case__c pc){
         
         employeeContacts__c employeeContact = new employeeContacts__c();
         employeeContact.EmployerID__c = pc.client__c;
         
         getRelationshipTypesMap();
         employeeContact.EmployeeRelationShip__c=RelationshipTypesMap.get('Employee');
         employeeContact.firstname__c = pc.employee_first_name__c;
         employeeContact.lastname__c = pc.employee_last_name__c;
         
         if(pc.employee_dobe__c!=null){
             employeeContact.dobe__c=pc.employee_dobe__c;
         }
         
         employeeContact.ssn__c = pc.Employee_SSN__c;
         employeeContact.gender__c = pc.Employee_Gender__c;
         employeeContact.EmailAddress__c = pc.Employee_Email_Address__c;
            
         employeeContact.StreetAddress__c = pc.Employee_Street__c;
         employeeContact.City__c = pc.Employee_City__c;
         employeeContact.State__c = pc.Employee_State__c;
         employeeContact.ZipCode__c = pc.Employee_Zip_Postal_Code__c;
            
         employeeContact.MobilePhoneNumber__c = pc.Employee_Mobile__c;
         employeeContact.HomePhoneNumber__c = pc.Employee_Home_Phone__c;
         employeeContact.WorkPhoneNumber__c=pc.Employee_Work_Phone__c;
         
         insert employeeContact;

         //11-19-21 update o.brown--------start------------------------------------
         pc.employeeid__c = employeeContact.id;
         insert pc;
         //11-19-21 update o.brown--------end--------------------------------------		 
		 
         RelatedtoEmployee__c rte = new RelatedtoEmployee__c(EmployeeContactID__c=employeeContact.id,EmployeeRelationID__c=employeeContact.EmployeeRelationShip__c );
         insert rte;
         system.debug(employeeContact.id);
         return employeeContact.id;
     }
     
     id createPatientContact(patient_case__c pc){
         
         employeeContacts__c patientContact= new employeeContacts__c();
         getRelationshipTypesMap();
         string rel = RelationshipTypesMap.get(pc.Relationship_to_the_Insured__c);
         patientContact.EmployeeRelationShip__c =rel;
         if(rel==null){
                switch on pc.Relationship_to_the_Insured__c{
                    when 'Child'{
                        patientContact.EmployeeRelationShip__c=RelationshipTypesMap.get('Dependent');
                    }
                    when 'Domestic Partner'{
                        patientContact.EmployeeRelationShip__c=RelationshipTypesMap.get('Significant Other');
                    }
                
                }
         }
         
         patientContact.firstname__c = pc.patient_first_name__c;
         patientContact.lastname__c = pc.patient_last_name__c;
         
         if(pc.patient_dobe__c!=null){
             patientContact.dobe__c=pc.patient_dobe__c;
         }
         
         patientContact.ssn__c = pc.patient_SSN__c;
         patientContact.gender__c = pc.patient_Gender__c;
         patientContact.EmailAddress__c = pc.patient_Email_Address__c;
            
         patientContact.StreetAddress__c = pc.patient_Street__c;
         patientContact.City__c = pc.patient_City__c;
         patientContact.State__c = pc.patient_State__c;
         patientContact.ZipCode__c = pc.patient_Zip_Postal_Code__c;
            
         patientContact.MobilePhoneNumber__c = pc.patient_Mobile__c;
         patientContact.HomePhoneNumber__c = pc.patient_Home_Phone__c;
         patientContact.WorkPhoneNumber__c=pc.patient_Work_Phone__c;
         system.debug(patientContact.id);
         return patientContact.id;
     }
     
     map<string, id> RelationshipTypesMap;
     
     map<string, id> getRelationshipTypesMap(){
         if(RelationshipTypesMap==null){
             RelationshipTypesMap= new map<string, id>();
             for(EmployeeRelationShip__c er : [select id, name from EmployeeRelationShip__c]){
                RelationshipTypesMap.put(er.name, er.id);
             }
         }
         return RelationshipTypesMap;
    }
     
}