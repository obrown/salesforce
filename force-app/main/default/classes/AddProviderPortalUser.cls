@RestResource(urlMapping='/providerportalregisteruser')

global class AddProviderPortalUser{
    
    class userRegistration{
        string phone_number,given_name,family_name,email,facility;
        
    }
    
    @HttpPost
    global static String doPost() {
        RestRequest req = RestContext.request;
        system.debug(req.RequestBody);
        userRegistration user = (userRegistration)JSON.deserialize(req.RequestBody.toString(), userRegistration.class);
        system.debug(req.RequestBody.toString());
        system.debug(user);
        
        AWS_User__c awsUser = new AWS_User__c();
        awsUser.Email__c = user.email;
        awsUser.User_supplied_Facility__c = user.facility;
        awsUser.FirstName__c = user.given_name;
        awsUser.LastName__c = user.family_name;
        awsUser.name = user.email;
        try{
        insert awsUser;
        }catch(dmlException de){
            system.debug('error registering: '+de.getMessage());
        }
        return json.serialize(awsUser);
    }

}