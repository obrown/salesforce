public with sharing class ClaimsMatcherJointCardiacSpine {
    date min_joint_cardic_spine_actual_arrival = date.today();
    ClaimsReporting__c[] joint_cardic_spine_claims = new ClaimsReporting__c[] {};
    ClaimsReporting__c[] claims_to_update = new ClaimsReporting__c[] {};

    set<string> claims_newly_paid;
    map<string, patient_case__c[]> pcMap = new map<string, patient_case__c[]>();
    formatID fid = new formatID();

    public static void run(ClaimsReporting__c[] joint_cardic_spine_claims, set<string> claims_newly_paid, boolean updateRecords){
        ClaimsMatcherJointCardiacSpine cm = new ClaimsMatcherJointCardiacSpine();

        cm.joint_cardic_spine_claims = joint_cardic_spine_claims;
        cm.claims_newly_paid = claims_newly_paid;
        cm.set_min_joint_cardic_spine_actual_arrival(cm.joint_cardic_spine_claims);
        cm.set_casemap();
        cm.associateCaseClaims();
    }

    void set_min_joint_cardic_spine_actual_arrival(ClaimsReporting__c[] joint_cardic_spine_claims){
        for (claimsReporting__c c : joint_cardic_spine_claims) {
            if (c.fdos__c < min_joint_cardic_spine_actual_arrival) {
                min_joint_cardic_spine_actual_arrival = c.fdos__c;
            }
        }
    }

    void set_casemap(){
        min_joint_cardic_spine_actual_arrival = min_joint_cardic_spine_actual_arrival.addDays(-30);
        list<Patient_Case__c> pcList = [select name, Relationship_to_the_Insured__c, Employee_Last_Name__c, Patient_Last_Name__c, certID__c, Actual_Arrival__c, Actual_Departure__c, client_name__c from patient_case__c where Actual_Departure__c != null and Actual_Arrival__c != null and actual_arrival__c > :min_joint_cardic_spine_actual_arrival];
        string ccertid, field;

        for (patient_case__c pc : pcList) {
            ccertid = fid.formatID(pc.certID__c, pc.client_name__c);
            list<Patient_Case__c> thelist = pcMap.get(ccertid + '' + pc.Employee_Last_Name__c.tolowerCase());
            field = pc.Employee_Last_Name__c.tolowerCase();

            if (theList == null) {
                thelist = pcMap.get(ccertid + '' + pc.Patient_Last_Name__c.tolowerCase());
                field = pc.Patient_Last_Name__c.tolowerCase();

                if (theList == null) {
                    theList = new list<Patient_Case__c>();
                }
            }
            theList.add(pc);
            pcMap.put(ccertid + '' + field, theList);
        }
    }

    void associateCaseClaims(){
        string ccertid;
        map<string, ClaimsReporting__c > crMap = new map<string, ClaimsReporting__c>();
        set<string> caseNames = new set<string>();

        for (ClaimsReporting__c cr : joint_cardic_spine_claims) {
            crMap.put(cr.claim_number__c, cr);
            ccertid = fid.formatID(cr.certID__c, cr.client_name__c);
            list<Patient_case__c> cases = pcmap.get(ccertid + cr.Employee_Last_Name__c.tolowerCase());

            if (cases == null) {
                cases = pcmap.get(ccertid + cr.Patient_Last_Name__c.tolowerCase());
            }

            if (cases == null) {
                continue;
            }

            for (Patient_case__c p :cases) {
                if (p.Actual_Arrival__c.addDays(-4) <= cr.fdos__c && p.Actual_Departure__c.addDays(4) >= cr.TDOS__c) {
                    cr.Patient_Case__c = p.name;
                    claims_to_update.add(cr);
                }
            }
        }

        map<id, patient_case__c> pc_update_map = new map<id, patient_case__c>();
        Claim__c[] claimList = [select name, Claim_Number__c, Date_HP_Paid__c, Date_Received__c, Patient_Case__c from Claim__c where name IN : claims_newly_paid order by Date_HP_Paid__c asc];
        system.debug('claims_newly_paid ' + claims_newly_paid);
        for (Claim__c c: claimList) {
            c.Date_Received__c = crMap.get(c.name).Date_Received__c;
            c.Date_HP_Paid__c = crMap.get(c.name).Date_HP_Paid__c;

            Patient_Case__c pc = new Patient_Case__c(id = c.Patient_Case__c);

            if (crMap.get(c.name).Paid__c != null && crMap.get(c.name).Paid__c > 0.00) {
                pc.status__c = 'Completed';
                pc.status_reason__c = 'Claim Paid';
                pc_update_map.put(pc.id, pc);
            }

            if (!pc_update_map.keyset().contains(pc.id) && (crMap.get(c.name).Paid__c == null || crMap.get(c.name).Paid__c == 0.00)) {
                pc.status__c = 'Open';
                pc.status_reason__c = 'Claim Pending';
                pc_update_map.put(pc.id, pc);
            }
        }

        update claimList;
        update pc_update_map.values();
    }
}