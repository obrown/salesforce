public class ussscorpionFileSharing{
    
    public transient string  content { get; set; }
    public boolean found { get; set; }
    public string recordName {get; private set;}
    public string contentType {get; private set;}
    
    public static pageReference getter(string recordName){
        return new PageReference('/apex/displayUssscorpion?recordName='+recordName);
       
    }
    
    public ussscorpionFileSharing(){
        recordName = ApexPages.CurrentPage().getParameters().get('recordName');
    }
        
    public void loadContent(){
        
        Http endpoint = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(fileServer__c.getInstance().ussscorpion__c+ 'openfile/' +recordname+'?filepath=r:/salesforceattachments_sandbox/' + recordname + '&oid=' + UserInfo.getOrganizationId() + '&sid=' + userinfo.getSessionid() + '&uid=' + userinfo.getuserid());
        
        req.setMethod('GET');
        req.setTimeout(120000);
        HttpResponse res = new HttpResponse();
        
        if(Test.isRunningTest()){
            Dom.Document doc = new Dom.Document();
            res.setBody('unitTest');
            
        }else{
             res = endpoint.send(req); 
        
        } 
        
        if(res.getStatusCode()==404){
            found= false;
            return;
        }
        
        found=true;
        map<string, string> contentMap = loadContentMap();
        string ext = recordName.right((recordName.length()-recordName.lastIndexOf('.'))-1);
        contentType = contentMap.get(ext);
        
        if(contentType== null || contentType == ''){
        
        try{
           
           ftpAttachment__c ftpa = [select contentType__c from ftpAttachment__c where fileName__c = :recordName];
           if(ftpa.contenttype__c!='' && ftpa.contenttype__c!=null){
                    contentType =ftpa.contenttype__c;
           }
        }catch(queryException e){}
        
        }
        //contentType='';
        
        Attachment A = new Attachment();
        a.Body = res.getBodyAsBlob();
        a.name = recordName;
        a.parentid = recordName.left(18);
        insert a;
        
        
        content = a.id;// 'data:' +contentType+ ';base64,' + EncodingUtil.base64Encode(res.getBodyAsBlob());
        //directBillingFuture.deleteAttachment(a.id);
         
    }
    
    public void deleteTempAttachmentVF(){
        try{
            string deleteID = ApexPages.currentPage().getParameters().get('attachID');
            if(deleteID!=null && deleteID!=''){
                delete ([select id from Attachment where id = :deleteID]);
            }
        }catch(exception e){
        
        }
    }
    
    public void deleteAttachmentVF(){
        string deleteID = ApexPages.currentPage().getParameters().get('attachID');
        if(deleteID!=null && deleteID!=''){
            directBillingFuture.deleteAttachment(deleteID);
        }
    }
    
    public static boolean deleteAttachment(string recordName){
        
        try{
        
            http h = new http();
            HttpRequest req = new HttpRequest ();
            req.setEndPoint('https://secured.hdplus.com/' + EncodingUtil.urlEncode(recordName,'UTF-8'));
            req.setmethod('DELETE');
            req.setHeader('Authorization','Basic ' + EncodingUtil.base64Encode(blob.valueof('sfattachsftp:'+floridaKeys__c.getInstance().USSSCORPION__c)));
            HttpResponse res = h.send(req);
        }catch(exception e){
            if(Test.isRunningTest()){return true;}
        }
        
        return true;
    }
    
    public static HttpResponse uploader(string recordName, string contentType, blob body){
        
       HttpResponse res; 
       try{
           http h = new http();
           HttpRequest req = new HttpRequest ();
           req.setTimeout(120000);
           req.setEndPoint('https://secured.hdplus.com/' + recordName);
           req.setmethod('PUT');
           req.setHeader('Authorization','Basic ' + EncodingUtil.base64Encode(blob.valueof('sfattachsftp:'+floridaKeys__c.getInstance().USSSCORPION__c)));
           req.setHeader('Translate','f');
           req.setHeader('Content-Type', contentType); 
           req.setBodyAsBlob(body);
           res = h.send(req);
           
       }catch(exception e){
           system.debug(res);
           res = new HttpResponse();
           if(Test.isRunningTest()){res.setStatusCode(200);return res;}
           
           res.setStatusCode(400);
           res.setBody(e.getMessage());
           system.debug(e.getLineNumber());
           system.debug(e.getMessage());
       }
       
       return res;        
    }
    
    map<string, string> loadContentMap(){
        
        map<string, string> contentMap = new map<string, string>();
        
        contentMap.put('xlsx','application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        contentMap.put('xls','application/vnd.ms-excel');
        contentMap.put('xlt','application/vnd.ms-excel');
        contentMap.put('xla','application/vnd.ms-excel');
        contentMap.put('docx','application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        contentMap.put('dotx','application/vnd.openxmlformats-officedocument.wordprocessingml.template');
        contentMap.put('doc','application/msword');
        contentMap.put('dot','application/msword');
        contentMap.put('msg','application/vnd.ms-outlook');
        contentMap.put('pdf','application/pdf');
        contentMap.put('ppt','application/vnd.ms-powerpoint');
        contentMap.put('zip','application/zip');
        contentMap.put('xml','application/xml');
        contentMap.put('png','image/png');
        contentMap.put('jpeg','image/jpeg');
        contentMap.put('jpg','image/jpeg');
        contentMap.put('txt','text/plain');
               
        return contentMap;
    }
    
}