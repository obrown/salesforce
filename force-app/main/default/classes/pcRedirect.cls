public with sharing class pcRedirect {
    
    id theiD;
    
    patient_case__c pc;
    
    public pcRedirect(ApexPages.StandardController controller){
       theiD = controller.getID();
       
       try{
           controller.addFields(new string[]{'program_type__c','isConverted__c', 'case_type__c','Ecen_Procedure__r.Name'});
           pc = (patient_case__c)controller.getRecord(); 
          
       }catch(exception e){
          pc = [select id,program_type__c,isConverted__c,case_type__c,Ecen_Procedure__r.Name from patient_case__c where id = :controller.getid()];
          system.debug(e.getMessage());
       }
       
       
    }
    
    public pageReference redirector(){
        
        pageReference foo;
        string role = userinfo.getUserRoleId();
        SystemID__c sid = SystemID__c.getInstance();  
        id userProfileID = UserInfo.getProfileId();
        
        string params='';
        map<string, string> incomingParams = ApexPages.currentPage().getParameters();
        for(string s : incomingParams.keyset()){
           params += '&'+ s + '=' + incomingParams.get(s);    
        }
        
        if(role==sid.FacilityRoleID__c){
            foo = new pageReference('/apex/facility?id='+theiD);
        }else{
            string pt;
            if(incomingParams.get('RecordType') != null){
                pt = [select name from recordtype where id = :incomingParams.get('RecordType')].name.split(' ')[1];
                
            }else{
                if(pc.ecen_procedure__r.name!=null){
                    pt = pc.ecen_procedure__r.name;
                }else if(pc.program_type__c!=null){
                    pt = pc.program_type__c;
                }
                
            }
            
            system.debug('pt= '+pt);
            if(sid.CustomerCareProfile__c== userProfileID || sid.hdpEligibilityprofID__c== userProfileID || sid.claims__c == userProfileID){
                    foo = new pageReference('/apex/caseDetail?id='+theiD+params);
                    return foo;
            }
            
            if(pt =='Joint' && !pc.case_type__c.contains('PepsiCo')){
              if(sid.csrProfID__c != userProfileID && sid.csrSuperProfid__c != userProfileID && pc.isConverted__c){
                    foo = new pageReference('/apex/jointCaseReferred?id='+theiD+params);
                    return foo;
                    
                }else if(sid.csrProfID__c == userProfileID || sid.csrSuperProfid__c == userProfileID ){
                    foo = new pageReference('/apex/caseBeta?id='+theiD + params);
                
                }else{
                    foo = new pageReference('/apex/caseBetaNurse?id='+theiD+ params);
                
                }
            }else if(pt =='Spine'){
                
                if(sid.csrProfID__c != userProfileID && sid.csrSuperProfid__c != userProfileID && pc.isConverted__c){
                    foo = new pageReference('/apex/spineCaseReferred?id='+theiD+params);
                
                }else{
                    foo = new pageReference('/apex/spineIntake?id='+theiD+params);
                }
                return foo;
            
            }else if(pt=='Joint Digital Physical Therapy'){
                foo = new pageReference('/apex/jointDigitalPhysicalTherapy?id='+theiD+params);
                return foo;
            
            }else if(pt=='Spine Digital Physical Therapy'){
                foo = new pageReference('/apex/spineDigitalPhysicalTherapy?id='+theiD+params);
                return foo;
                
            }else if(pt =='Substance Disorder' || pt =='Disorder'){
                if(sid.csrProfID__c != userProfileID && sid.csrSuperProfid__c != userProfileID){
                    foo = new pageReference('/apex/SubstanceDisorder?id='+theiD+params);
                
                }               
                return foo;
                                                      
            }else {
            
                if(incomingParams.get('RecordType') != null){
                    foo = new pageReference('/apex/caseEdit?id='+theiD + params);
                }else{
                    foo = new pageReference('/apex/caseDetail?id='+theiD + params);
                }
            }
        }
        
        return foo;
    
    }

}