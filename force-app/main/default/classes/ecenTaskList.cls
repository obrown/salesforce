public class ecenTaskList{
    
    public Task[] Pending;
    public Task[] Completed;
        
    public ecenTaskList(){
        Pending= new Task[]{};
        Completed= new Task[]{};
    }
        
    public class Task{
        public string  label;
        public integer sortOrder;
        public string ResponsibleParty;
        
        public Task(string label, integer sortOrder, string ResponsibleParty){
            
            this.label = label;
            this.sortOrder= sortOrder;
            this.ResponsibleParty =ResponsibleParty ; 
        }
        
    }
    
}