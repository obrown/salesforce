public with sharing class airlineTrip{
    
    public selectOption[] contacts {get; set;}
    public selectOption[] flights {get; set;}
    
    public AirlineTrips__c airLineTrip {get; set;}
    public AirlineTrips__c[] airLineTrips {get; private set;}
    
    public Airline_Trip_Adjustment__c airLineAdjTrip {get; set;}
    public Airline_Trip_Adjustment__c[] airLineAdjTrips {get; private set;}
    
    id memberTripId, patientCaseId;
    set<id> eIdSet = new set<id>();
    
    public boolean isError {get; private set;}
    public string message {get; private set;}
    
    public airlineTrip(id memberTripId, id patientCaseId,set<id> eIdSet){
        this.memberTripId=memberTripId;
        this.patientCaseId=patientCaseId;
        this.eIdSet=eIdSet;
        loadAirlineTrips();
        loadAirlineAdjTrips();
        loadContactsOption();
    }
   
    void loadContactsOption(){
        contacts = new selectOption[]{};
        contacts.add(new selectOption('', '-- None --'));
        
        for(employeeContacts__c ec : [select id,FirstName__c,
                                                LastName__c,
                                                DOBe__c,
                                                Gender__c,
                                                EmployeeRelationShip__r.name,
                                                EmployeeRelationShip__c from employeeContacts__c where id in :eIdSet]){
        
            contacts.add(new selectOption(ec.id, ec.FirstName__c+' '+ec.LastName__c+' ('+ec.EmployeeRelationShip__r.name+')'));
                                                
        }
    }
    
    
    set<id> airLineTripSet = new set<id>();
    void loadAirlineTrips(){
        
        airLineTrips = new AirlineTrips__c[]{};
        flights = new selectOption[]{};
        flights.add(new selectOption('','-- None --'));
        
        try{
            for(airlineTrips__c  at : [select Adjustment_Reason_Notes__c,
                                             Airfare__c,
                                   Name,
                                   ConfirmationNumber__c,
                                   ArrivalFlightNumber__c,
                                   ArrivalFlightDate__c,
                                   ArrivalFlightDateTime__c,
                                   ArrivalFlightTime__c,
                                   BaggageFees__c,
                                   DepartureFlightNumber__c,
                                   DepartureFlightDate__c,
                                   DepartureFlightTime__c,
                                   DepartureFlightDateTime__c,
                                   Extra_space_for_large_tall_patient__c,
                                   employeeContact__c,
                                   Flight_Adjustment__c,
                                   Flying_One_way__c,
                                   Maximum_flight_length__c,
                                   MemberTripID__c,
                                   MemberTripID__r.MemberTripName__c,
                                   MemberTripID__r.patient_case__c,
                                   employeeContact__r.firstname__c,
                                   employeeContact__r.lastname__c,
                                   Reason_for_Adjustment__c,
                                   Tax__c,
                                   Wheelchair_Needed__c,
                                   CreatedBy.name,
                                   CreatedDate from AirlineTrips__c where MemberTripID__c = :memberTripid]){
                                   
                                   airLineTrips.add(at);
                                   airLineTripSet.add(at.id);
                                       if(at.ConfirmationNumber__c==null){
                                          if(at.employeeContact__c==null){ 
                                              flights.add(new selectOption(at.id, at.MemberTripID__r.MemberTripName__c));
                                          }else{
                                              flights.add(new selectOption(at.id, at.employeeContact__r.firstname__c+' '+ at.employeeContact__r.lastname__c));
                                          } 
                                       }else{
                                           flights.add(new selectOption(at.id, at.ConfirmationNumber__c));
                                       }    
                                   }
        }catch(queryException qe){
            //catch silently
            system.debug(qe.getMessage());
        }
    }

    void loadAirlineAdjTrips(){
        
        airLineAdjTrips = new Airline_Trip_Adjustment__c []{};
        try{
            airLineAdjTrips = [select airlineTrip__r.Name,
                                      airlineTrip__r.ConfirmationNumber__c,  
                                      airlineTrip__r.employeeContact__r.FirstName__c,
                                      airlineTrip__r.employeeContact__r.LastName__c,
                                      Adjustment_Reason_Notes__c,
                                      Flight_Type__c,
                                      Reason_for_Adjustment__c,
                                      Adjustment_Amount__c,
                                      CreatedBy.name,createdDate  from Airline_Trip_Adjustment__c where AirlineTrip__c in :airLineTripSet order by airLineTrip__c desc, createdDate desc];
        }catch(queryException qe){
            //catch silently
            system.debug(qe.getMessage());
        }
    }    
    public void setMemeberTripId(id theId){
        this.memberTripId=theId;
    }
    
    public void newFlight(){
        airlineTrip = new airlineTrips__c(MemberTripID__c=memberTripid);
        
        try{
            //pc_poc__c poc = 
        }catch(exception e){
        
        }
        
    }

    public void newbookedFlight(datetime arrival,datetime departure,string pWheelChairNeeded,string pExtraTall){
        airlineTrip = new airlineTrips__c(MemberTripID__c=memberTripid);
        airlineTrip.ArrivalFlightDateTime__c=arrival;
        airlineTrip.DepartureFlightDateTime__c=departure;
        airlineTrip.Wheelchair_Needed__c=pWheelChairNeeded;
        airlineTrip.Extra_space_for_large_tall_patient__c=pExtraTall;        
        
    }
    
    public void getFlightInformation(string flightId){
        for(airLineTrips__c at: airLineTrips){
            if(at.id == flightId){
                airLineTrip = at;
                break;
            }
        }
    }
    public void getFlightAdjInformation(string adjId, string flightId){
    
        for(Airline_Trip_Adjustment__c at: airLineAdjTrips){
            if(at.id == adjId){
                airLineAdjTrip = at;
                break;
            }
        }
        
        if(airLineAdjTrip==null){
            airLineAdjTrip = new Airline_Trip_Adjustment__c(AirlineTrip__c=flightid);
        }
       
    }    
    public void saveFlightandNew(){
        isError=false;
        message ='';
        try{
            try{
                saveFlight();
                airlineTrip = airlineTrip.clone();
                airlineTrip.employeeContact__c=null;
                airlineTrip.memberTripId__c=memberTripId;
                
            }catch(dmlexception e){
                system.debug(e.getDMLMessage(0));
                message = e.getDMLMessage(0);
            }
        
        }catch(exception e){
            system.debug(e.getMessage());
            message = e.getMessage();
        }
    }
    
    public void saveFlight(){
        isError=false;
        message ='';
        system.debug(airLineTrip.employeeContact__c);
        try{
            try{
                if(airLineTrip.memberTripId__c==null){
                    airLineTrip.memberTripId__c=memberTripId;
                }
                
                if(airLineTrip.Reason_for_Adjustment__c!=null){
                    airLineTrip.Flight_Adjustment__c=true;
                
                }
                //9-1-20 added per Nate set to null on 1way flight
                if(airLineTrip.Flying_One_way__c==true){
                   airlineTrip.DepartureFlightNumber__c='';
                   airlineTrip.DepartureFlightDateTime__c=null;
                
                }
                upsert airlineTrip;
                loadAirlineTrips();
                loadAirlineAdjTrips();
                
                patient_case__c pc = new patient_case__c(id=patientCaseId);
                pc.Arriving_Flight_Number__c=airlineTrip.ArrivalFlightNumber__c;
                pc.Arriving_Flight_Time__c=airlineTrip.ArrivalFlightDateTime__c;
                pc.Departing_Flight_Number__c=airlineTrip.DepartureFlightNumber__c;
                pc.Departing_Flight_Time__c=airlineTrip.DepartureFlightDateTime__c; 
                update pc;
                
                message = 'Record Saved';
            
            }catch(dmlexception e){
                isError=true;
                system.debug(e.getDmlMessage(0));
                message= e.getDmlMessage(0);
            }        

        }catch(exception e){
            isError=true;
            system.debug(e.getMessage());
            message= e.getMessage();
        }        
    }
    
    public void saveFlightAdjustment(){
        isError=false;
        message ='';
        try{
            try{
               
                upsert airLineAdjTrip;
                loadAirlineAdjTrips();
                loadAirlineTrips();
                message = 'Record Saved';
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, message));
                
            }catch(dmlexception e){
                isError=true;
                system.debug(e.getDmlMessage(0));
                message= e.getDmlMessage(0);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, message));
            }        

        }catch(exception e){
            isError=true;
            system.debug(e.getMessage());
            message= e.getMessage();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, message));
        }        
    }
    
    public void deleteAirlineTrip(string flightId){
        airLineTrips__c at = new airLineTrips__c (id=flightId);
        delete at;
        
        patient_case__c pc = new patient_case__c(id=patientCaseId);
        pc.Arriving_Flight_Number__c=null;
        pc.Arriving_Flight_Time__c=null;
        pc.Departing_Flight_Number__c=null;
        pc.Departing_Flight_Time__c=null; 
        update pc;
        
        loadAirlineTrips();
    }
    
    public void deleteAirlineAdjTrip(string flightId){
        Airline_Trip_Adjustment__c  at = new Airline_Trip_Adjustment__c(id=flightId);
        delete at;
        loadAirlineAdjTrips();
    }    
}