/**
 * The BulkUpdate class is a utility to more efficiently change field values, so
 * we don't need to use the more complicated Excel spreadsheet method or copy &
 * paste Apex code for every field update. It is meant to be used as a simple
 * function.
 *
 * It is particularly useful for the Provider Portal which will break when any
 * select field, checklist value, or radio control differs from what Salesforce
 * says the available field options are, but the data loaded has a different
 * value than that (validation discrepancy).
 */
public with sharing class BulkUpdate {
    /**
     * The API name of the sObject
     * i.e 'pc_POC__c'
     */
    public final string objType {
        get;
        private set;
    }

    /**
     * The name of the field on the sObject to change the value of
     * i.e 'Proposed_Service_Type__c'
     */
    public final string fieldName {
        get;
        private set;
    }

    // The field value to change from
    public final string oldValue {
        get;
        private set;
    }

    // The field value to change to
    public final string newValue {
        get;
        private set;
    }

    public BulkUpdate(
        string objType,
        string fieldName,
        string oldValue,
        string newValue
    ) {
        this.objType = objType;
        this.fieldName = fieldName;
        this.oldValue = oldValue;
        this.newValue = newValue;

        updateData();
    }

    /**
     * This method will first select any data of the given sObject type where
     * for the given field, if its value matches the "oldValue" provided, it
     * will accumulate into a list and then be updated with the "newValue"
     *
     * Then for debugging purposes, it will query the data again to verify what
     * the field value has been changed to.  Or if nothing changed, it'll say so
     */
    @future
    public static void updateData() {
        try {
            List<sObject> objects = new List<sObject>();

            // build string query
            string query = 'SELECT '
                            + fieldName
                            + ' FROM '
                            + objType
                            + ' WHERE '
                            + fieldName
                            + ' =: oldValue';
            // string query2 = 'SELECT '
            //                 + 'Name,'
            //                 + fieldName
            //                 + ' FROM '
            //                 + objType
            //                 + ' WHERE '
            //                 + 'Id IN :changedObjects AND '
            //                 + fieldName
            //                 + ' =: newValue';

            objects = database.query(query);

            if (objects.size() > 0) {
                for (sObject sObj : objects) {
                    sObj.put(fieldName, newValue);
                }

                update objects;

                System.debug('Success!');

                // List<sObject> updatedObjects = new List<sObject>();
                //
                // updatedObjects = database.query(query2);
                //
                // if (updatedObjects.size() > 0) {
                //     for (sObject upObj : updatedObjects) {
                //         System.debug(
                //             'Updated '
                //             + upObj.get('Name')
                //             + ' ('
                //             + upObj.get('Id')
                //             + ')'
                //             + ' field named: '
                //             + fieldName
                //             + '. Changed old value "'
                //             + oldValue
                //             + '" to "'
                //             + upObj.get(fieldName)
                //             + '"'
                //         );
                //     }
                // }
            } else {
                System.debug('No objects were changed.');
            }
        } catch (Exception e) {
            System.debug(e.getMessage());
        }
    }
}

/*
Usage:
---
string ov = 'Cervical fusion';
string nv = 'Cervical Fusion';

BulkUpdate up1 = new BulkUpdate(
    'EcenProviderPlanOfCare__c',
    'proposedServiceType__c',
    ov,
    nv
);

BulkUpdate up2 = new BulkUpdate(
    'pc_POC__c',
    'Proposed_Service_Type__c',
    ov,
    nv
);

*/
