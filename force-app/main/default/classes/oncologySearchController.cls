public with sharing class oncologySearchController {

    public PageReference searchVF() {
        
        if(st==''){
            return null;
        
        }else if(sf==''){
            return null;
        
        }
        
        pagereference pageRef = Page.oncologySearch;
        pageref.getParameters().put('st', st);
        pageref.getParameters().put('sf', sf);
        pageRef.setRedirect(true);
        return pageRef;
    }

    
    public oncology__c[] results {get; set;} 
    public String st { get; set; } //searchTerm
    public String sf { get; set; } //searchField
    
    public oncologySearchController (){
        st = ApexPages.CurrentPage().getParameters().get('st');
        sf = ApexPages.CurrentPage().getParameters().get('sf');
        searchOncologyRecords();
    }
    
    public void searchOncologyRecords(){
        oncologySearch oSearch = new oncologySearch();
        results = oSearch.searchOncology(st, sf);
    }
}