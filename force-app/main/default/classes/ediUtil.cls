public with sharing class ediUtil {
    
    public static string callRH(string body, string endpoint){
        
        string returnStr;
        http h = new http();
        
        HttpRequest req = new HttpRequest ();
        req.setEndPoint(endpoint);
        req.setBody(body);
        req.setmethod('POST');
        req.setTimeout(120000);
        
        try{
            HttpResponse res = h.send(req);   
            if(res.getStatusCode()!=200){ 
                returnStr = res.getStatusCode()+' error';
                insert new Api_error__c(Status_Code__c=string.valueof(res.getStatusCode()), Message__c=res.getBody().left(200));
               
            }else{ returnStr = res.getBody();}
        }catch(exception e){
            returnStr =  e.getMessage()+' error';    
        }
        
        return returnStr;
        
    
    }
    
}