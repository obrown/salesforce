public class coeClientFacilities{

    public static selectOption[] facilitySelectOptions(string clientId, string procedureId, string facility, boolean activeOnly){
        
        selectOption[] foo = new selectOption[]{};
        foo.add( new selectoption('','--None--'));
        set<string>valuesAdded = new set<string>();
        
        for(Client_Facility__c f : coeClientFacilities.cfList(clientId, procedureId, activeOnly)){
           valuesAdded.add(f.Facility__r.name);
           foo.add(new selectoption(f.id, f.Facility__r.name)); 
        }
        
        return foo;
    }
    
    public static string[] facilityStringList(string clientId, string procedureId, boolean activeOnly){
        
        string[] foo = new string[]{};
        for(Client_Facility__c f : coeClientFacilities.cfList(clientId, procedureId, activeOnly)){
           foo.add(f.Facility__r.name); 
        }
        
        if(foo.size()==0){
            foo.add('Client has no facilities setup');
            
        }
        
        foo.sort();
        return foo;
    }
    
    static Client_Facility__c[] cfList(string clientId, string procedureId, boolean activeOnly){
        
        Client_Facility__c[] cfList = new Client_Facility__c[]{};
        if(clientId==null||procedureId==null){
            return cfList;
        }
        
        if(activeOnly){
            cfList= [select id,Facility__r.name from Client_Facility__c where active__c = true and Client__c= :clientId and Procedure__c = :procedureId];
        }else{
            cfList= [select id,Facility__r.name from Client_Facility__c where Client__c= :clientId and Procedure__c = :procedureId];
        }
        return cfList;
     }   

}