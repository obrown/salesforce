public with sharing class cciRedirectExt {
 
    customer_care_inquiry__c foo;
    
    public CCIRedirectExt(ApexPages.StandardController controller) {
        foo= (customer_care_inquiry__c)controller.getRecord();
        
    }
   
    public pageReference fwdUser(){
        pagereference pageRef = Page.CustomerCare;
        if(foo.id!=null){
            pageref.getParameters().put('icciId',foo.id);
            pageref.getParameters().put('id',foo.Customer_Care__c);
        }     
        
        return pageRef;
    }

}