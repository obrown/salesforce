global class oncologyJobScheduler implements Schedulable, Database.AllowsCallouts{
    
    global void execute(SchedulableContext go){
        
        oncologyworkflow.sendOncologyEligibilitySpreadsheet();
    }
    
}