public with sharing class newCase{

    public string clientId {get;set;}
    public string procedureId {get;set;}
    public SelectOption[] clients {get;set;}
    map<string, set<selectOption>> clientProcedureMap = new map<string, set<selectOption>>();
    
    public newCase(){
        loadClients();
    }
    
     public List<SelectOption> getempnames() {
        
        List<SelectOption> options = new List<SelectOption>();
                if(utilities.isInternal()){
                        
                        options.add(new SelectOption('wxx','Walmart'));
                        options.add(new SelectOption('lxx','Lowes'));
                        options.add(new SelectOption('hxx','HCR ManorCare'));
                        options.add(new SelectOption('mxx','Mckesson'));
                        options.add(new SelectOption('jxx','jetBlue'));
                        
                }else{
                        options.add(new SelectOption('alx','Allon'));
                        
                }
                
                return options;
    }
    
    public SelectOption[] getProcedures() {
        selectoption[] temp = new selectoption[]{};
        temp.add(new selectOption('', '-'));
        set<selectoption> availableOptions = clientProcedureMap.get(clientId);
        if(availableOptions!=null){
            temp.addAll(availableOptions);
        }
        
        return temp;
        
    }
    
    public SelectOption[] loadClients() {
        
                if(utilities.isInternal()){
                    set<selectOption> clientSO = new set<selectOption>();
                    clientSO.add(new selectOption('', '--None--'));
                    for(client_facility__c cf : [select procedure__c, client__c, client__r.name, procedure__r.name from client_facility__c where active__c = true and facility__r.name != 'IMC' order by client__r.name desc]){
                            set<selectOption> procedureList = clientProcedureMap.get(cf.client__c);
                            clientSO.add(new selectOption(cf.client__c, cf.client__r.name));
                            
                            if(procedureList ==null){
                                procedureList = new set<selectOption>();
                            }
                            
                            procedureList.add(new selectOption(cf.procedure__c, cf.procedure__r.name));
                            clientProcedureMap.put(cf.client__c, procedureList );
                            
                            
                    }
                        
                    clients = new list<selectoption>(clientSO);
                   
                }
                
                return clients ;
    }

}