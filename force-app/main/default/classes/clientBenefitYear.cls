public with sharing class clientBenefitYear{


    public static selectoption[] BenefitYearSelectOption(id clientID){
            
        selectOption[] so = new selectOption[]{};
        so.add(new selectOption('',''));    
            
        try{
            
            for(Client_Benefit_Year__c cby : [select end__c,id from Client_Benefit_Year__c where client__c = :clientID order by end__c desc]){
                so.add(new selectOption(cby.id, string.valueof(cby.end__c.year()))); 
            }
            
        }catch(exception e){
            so.add(new selectOption('','Error'));
        }
        
        return so;
   }
}