public without sharing class oncologyRequiredToRefer {
    public static string[] checkRequirements(oncology__c o) {
        string[] requiredToRefer = new string[]{};

        if (o.Patient_Agrees_to_Referral__c == 'No') {
            requiredToRefer.add('Patient does not agree to Referral');
            return requiredToRefer;
        }

        if (o.Carrier__c == null) {
            requiredToRefer.add('Carrier');
        }

        if (
            o.Eligibility_Verification__c == null ||
            o.Eligibility_Verification__c == 'No'
        ) {
            requiredToRefer.add('Case Not Eligible');
        }

        if (o.COBRA_Status__c == null || o.COBRA_Status__c == '') {
            requiredToRefer.add('COBRA (Status)');
        }

        if (
            o.COBRA_Status__c == 'Yes' &&
            o.COBRA_Paid_Through_Date__c == null
        ) {
            requiredToRefer.add('COBRA (Paid Through Date)');
        }

        if (o.Date_Eligibility_Verified__c == null) {
            requiredToRefer.add('Date Eligibility Verified');
        }

        if (o.Delegate_Status__c == null || o.Delegate_Status__c == '') {
            requiredToRefer.add('Delegate Status');
        }

        if (
            o.Employee_First_Name__c == '' ||
            o.Employee_First_Name__c == null
        ) {
            requiredToRefer.add('Employee First Name');
        }

        if (o.Employee_Last_Name__c == null || o.Employee_Last_Name__c == '') {
            requiredToRefer.add('Employee Last Name');
        }

        if (o.Employee_DOB__c == null) {
            requiredToRefer.add('Employee DOB');
        }

        if (o.Employee_Gender__c == null || o.Employee_Gender__c == '') {
            requiredToRefer.add('Employee Gender');
        }

        if (o.Procedure__c == null) {
            requiredToRefer.add('Employer Program');
        }

        if (o.Gender__c == null || o.Gender__c == '') {
            requiredToRefer.add('Patient Gender');
        }

        if (o.Employee_Healthplan__c == null) {
            requiredToRefer.add('Health Plan');
        }

        if (o.LOA_Status__c == null || o.LOA_Status__c == '') {
            requiredToRefer.add('LOA (Status)');
        }

        if (o.LOA_Status__c == 'Yes' && o.LOA_Paid_Through_Date__c == null) {
            requiredToRefer.add('LOA (Paid Through Date)');
        }

        if (o.Patient_Agrees_to_Referral__c == '') {
            requiredToRefer.add('Patient Agrees to Referral');
        }

        if (o.Patient_First_Name__c == null || o.Patient_First_Name__c == '') {
            requiredToRefer.add('Patient First Name');
        }

        if (o.Patient_Last_Name__c == null || o.Patient_Last_Name__c == '') {
            requiredToRefer.add('Patient Last Name');
        }

        if (o.Patient_DOB__c == null) {
            requiredToRefer.add('Patient Dob');
        }

        if (
            o.Patient_Medical_Plan_Number__c == null ||
            o.Patient_Medical_Plan_Number__c == ''
        ) {
            requiredToRefer.add('Patient Medical Plan Number');
        }

        if (o.Patient_Address__c == null || o.Patient_Address__c == '') {
            requiredToRefer.add('Patient Street');
        }

        if (o.Patient_City__c == null || o.Patient_City__c == '') {
            requiredToRefer.add('Patient City');
        }

        if (o.Patient_State__c == null || o.Patient_State__c == '') {
            requiredToRefer.add('Patient State');
        }

        if (o.Patient_Zip_Code__c == null || o.Patient_Zip_Code__c == '') {
            requiredToRefer.add('Patient Zip');
        }

        if (o.Patient_Country__c == '' || o.Patient_Country__c == null) {
            requiredToRefer.add('Patient Country');
        }

        if (
            o.Patient_Preferred_Phone__c == null ||
            o.Patient_Preferred_Phone__c == ''
        ) {
            requiredToRefer.add('Patient Preferred Phone');
        }

        if (o.Preferred_Language__c == null || o.Preferred_Language__c == '') {
            requiredToRefer.add('Preferred Language');
        }

        if (
            o.Translation_Services_Required__c == null ||
            o.Translation_Services_Required__c == ''
        ) {
            requiredToRefer.add('Translation Services Required');
        }

        if (
            o.Relationship_to_Subscriber__c == null ||
            o.Relationship_to_Subscriber__c == ''
        ) {
            requiredToRefer.add('Relationship to the Employee');
        }


        if (
            o.Patient_Agrees_to_Referral__c == null ||
            o.Patient_Agrees_to_Referral__c == ''
        ) {
            requiredToRefer.add('Patient Agrees to Referral');
        }

        if (
            o.Client_Name__c != 'Lowes' &&
            o.Client_Name__c != 'NextEra Energy' &&
            o.Client_Name__c != 'SISC' && (
                o.Patient_Advocacy__c == null ||
                o.Patient_Advocacy__c == ''
            )
        ) {
            requiredToRefer.add('Patient Advocacy');
        }

        if (
            o.Patient_Phone__c == '' &&
            o.Patient_Phone2__c == '' &&
            o.Patient_Phone3__c == ''
        ) {
            requiredToRefer.add('Patient Phone Number');
        }

        if (o.Preferred_Language__c == '' || o.Preferred_Language__c == null) {
            requiredToRefer.add('Preferred Language');
        }

        if (o.Referral_Source__c == '' || o.Referral_Source__c == null) {
            requiredToRefer.add('Referral Source');
        }

        if (o.Client_Facility__c == null) {
            requiredToRefer.add('Referred Facility');
        }

        if (
            o.Translation_Services_Required__c == '' ||
            o.Translation_Services_Required__c == null
        ) {
            requiredToRefer.add('Translation Services Required');
        }

        if (
            o.Reporting_Cancer_Diagnosis__c == '' ||
            o.Reporting_Cancer_Diagnosis__c == null
        ) {
            requiredToRefer.add('Reporting Cancer Diagnosis Required');
        }

        if (o.hasLocalPCP__c == 0 || o.hasLocalPCP__c == null) {
            Oncology_Physician__c[] oList = new Oncology_Physician__c[]{};

            try {
                oList = [
                    select id from Oncology_Physician__c where Oncology__c = :o.id
                ];

                if (oList.isEmpty()) {
                    requiredToRefer.add('A physician marked as a local PCP');
                }
            } catch (exception e) {
                requiredToRefer.add('A physician marked as a local PCP');
            }
        }

        return requiredToRefer;
    }
}
