public with sharing class coeExpenses{

    public boolean isError {
        get;
        set;
    }

    public coeExceptionNetwork networkException {
        get;
        set;
    } 
    
    public String message {
        get;
        set;
    } 

    public Patient_Case__c obj {
        get;
        set;
    }  
    
    public transient dupRecord theDupRecord {
        get;
        set;
    } 
    
    public COE_Expenses__c[] expenses {
        get;
        private set;
    }
    public COE_Expenses__c[] allowances {
        get;
        private set;
    }
    public COE_Expenses__c[] paycards {
        get;
        private set;
    }
    public COE_Expenses__c[] hotelpaycards {
        get;
        private set;
    }

    public COE_Expenses__c expense {
        get;
        set;
    }
    public COE_Expenses__c allowance {
        get;
        set;
    }
    public COE_Expenses__c paycard {
        get;
        set;
    }
    public COE_Expenses__c hotelpaycard {
        get;
        set;
    }

    public COE_Finance__c [] financeList{
        get;
        private set;
    }    
    public COE_Finance__c finance{
        get;
        set;
    }                 
    
    public coeExpenses(){}
    
    public coeExpenses(patient_case__c obj){
        this.obj=obj;
    }
          
    public map<id, COE_Expenses__c[]> expensesMap;
    COE_Expenses__c[] coeList = new COE_Expenses__c[]{};
    map<id, COE_Expenses__c[]> coeMap = new map<id, COE_Expenses__c[]>();
     
    COE_Expenses__c addExpense(string type, decimal amount, id pcID, id recordTypeID){
        
        COE_Expenses__c c = new COE_Expenses__c(patient_case__c =pcID, payment_amount__c=amount, type__c = type, recordTypeID=recordTypeID);
        return c;
        
    }
    
    public void deleteExpense(id theID){
        if(theID!=null){
            COE_Expenses__c c = new COE_Expenses__c(id=theID);
            delete c;
        }
        
        
    }

    id hasType(COE_Expenses__c[] exp, string type){
        
        if(exp!=null){
        
            for(COE_Expenses__c e : exp){
                // && e.paid_date__c == null
                if(e.type__c == type){
                    return e.id;
                    break;            
                }
            }
        
        }
        
        return null;
    
    }
    
    decimal finalStipend(string tt, decimal x, decimal y, string hotel){
        
        if(tt == 'Driving greater than 60 miles' || tt == 'Flying'){
                
                return x;
                
            }else if(tt == 'Driving less than 60 miles'){
                
                if(hotel==''){
                    return y;
                }else{
                    return x;
                }
            }
            
        return 0;
    }
    
    decimal dailyStipendAmount(string cl, string tt, string hotel){
        string client=cl;
        client = client.toLowerCase();
        
        if(client=='walmart'){
            return 60;
            
        }else if(client=='lowes'){
            return finalStipend(tt, 65, 80, hotel);
                        
        }else if(client=='jetblue' || client=='mckesson'){
            return finalStipend(tt, 75, 85, hotel);
            
        }else if(client=='pepsico'){
             return 75;
             
        }else if(client=='kohls'){
             return finalStipend(tt, 65, 85, hotel);
        }
        
        return 0;
    }
    
    public void dailyStipendHandler(set<patient_case__c> pcList, decimal amount){
        
        map<id, decimal> dailyStipend = new map<id, decimal>();
        set<id> pIDs = new set<id>();
        for(patient_case__c p :pcList){
            pIDs.add(p.id);
        }
        
        expensesMap = eMapper(pIDs);
        
        for(patient_case__c p : pcList){
        
            if(p.length_of_episode__c != null && p.nClient_Name__c != 'HCR Manorcare'){
                decimal x = 0;
                decimal loe = p.length_of_episode__c;
                
                id i =  hasType(expensesMap.get(p.id),'Intial Daily Stipend');
                if(i==null){
                
                    if(p.case_type__c.contains('Joint')){
                        if(loe>3){
                            coeList.add(this.addExpense('Intial Daily Stipend', dailyStipendAmount(p.nClient_Name__c, p.travel_type__c, p.Hotel_Name__c) * 3, p.id, PatientCase__c.getInstance().allowance__c));
                            coeList.add(this.addExpense('Daily Stipend', (loe - 3) * dailyStipendAmount(p.nClient_Name__c, p.travel_type__c, p.Hotel_Name__c), p.id, PatientCase__c.getInstance().allowance__c));
                            
                        }else{
                            coeList.add(this.addExpense('Intial Daily Stipend', dailyStipendAmount(p.nClient_Name__c, p.travel_type__c, p.Hotel_Name__c) * loe, p.id, PatientCase__c.getInstance().allowance__c));
                            
                        }        
                    }else{
                        if(loe>4){
                            coeList.add(this.addExpense('Intial Daily Stipend', dailyStipendAmount(p.nClient_Name__c, p.travel_type__c, p.Hotel_Name__c) * 4, p.id, PatientCase__c.getInstance().allowance__c));
                            coeList.add(this.addExpense('Daily Stipend', (loe - 4)*dailyStipendAmount(p.nClient_Name__c, p.travel_type__c, p.Hotel_Name__c), p.id, PatientCase__c.getInstance().allowance__c));
                        }else{
                            coeList.add(this.addExpense('Intial Daily Stipend', dailyStipendAmount(p.nClient_Name__c, p.travel_type__c, p.Hotel_Name__c) * loe, p.id, PatientCase__c.getInstance().allowance__c));
                        }  
                    }
                
                }
            }
           
        }
        
    }

    public void addlDailyStipendHandler(set<patient_case__c> pcList, decimal amount){
        
        map<id, decimal> dailyStipend = new map<id, decimal>();
        set<id> pIDs = new set<id>();
        for(patient_case__c p :pcList){
            pIDs.add(p.id);
        }
        
        expensesMap = eMapper(pIDs);
        
        for(patient_case__c p : pcList){
        
            if(p.Length_of_Episode_Additional_Days__c != null){
                decimal x = integer.valueOf(p.Length_of_Episode_Additional_Days__c) * amount;
                id i =  hasType(expensesMap.get(p.id),'Additional Stipend');
                if(i==null){
                    coeList.add(this.addExpense('Additional Stipend', x, p.id, PatientCase__c.getInstance().allowance__c));
                }
            }
           
        }
        
    }
    
    public void travelHandler(patient_case__c[] pcList){
        
        set<id> pIDs = new set<id>();
        for(patient_case__c p :pcList){
            pIDs.add(p.id);
        }
        
        patient_case__c[] pcListToUpdate = new patient_case__c[]{};
        map<id, patient_case__c> pcMap = new map<id, patient_case__c>();
        
        expensesMap = eMapper(pIDs);
        string client;
        for(patient_case__c p : pcList){
            client = p.nClient_Name__c;
            
            if(p.travel_type__c=='Flying'){
            
                id i = hasType(expensesMap.get(p.id),'Baggage');
                if(i==null){
                    coeList.add(this.addExpense('Baggage', 100.00, p.id, PatientCase__c.getInstance().allowance__c));
                }
                this.deleteExpense(hasType(expensesMap.get(p.id),'Driving Stipend'));
                
                if(client == 'Walmart' &&  p.Roundtrip_Mileage_to_the_Airport_Station__c!=null){
                    decimal x = p.Roundtrip_Mileage_to_the_Airport_Station__c * 0.575;
                    i =  hasType(expensesMap.get(p.id),'Driving Stipend to the Airport/Station');
                    if(i==null){
                        coeList.add(this.addExpense('Driving Stipend to the Airport/Station', x, p.id, PatientCase__c.getInstance().allowance__c));
                    }
                    
                    if(p.Prepaid_Card_Initial_Load_Amount__c==null){
                        pcMap.put(p.id, new patient_case__c(id=p.id, Prepaid_Card_Initial_Load_Amount__c = x+(dailyStipendAmount(p.nClient_Name__c, p.travel_type__c, p.Hotel_Name__c) * 3)));
                    }
                    
                }else{
                    
                    if(client!='Walmart' && client!='HCRManorcare'){
                        i =  hasType(expensesMap.get(p.id),'Driving Stipend to the Airport/Station');
                        if(i==null){
                            coeList.add(this.addExpense('Driving Stipend to the Airport/Station', 15, p.id, PatientCase__c.getInstance().allowance__c));
                        }    
                    }
                    
                        
                }                    
                
                if(p.airfare_offset__c == null || p.airfare_offset__c == 0.00){
                    patient_case__c pc = pcMap.get(p.id);
                    if(pc==null){
                        pcMap.put(p.id, new patient_case__c(id=p.id, airfare_offset__c = calcAirfare(expensesMap.get(p.id))));
                    }else{
                        pc.airfare_offset__c = calcAirfare(expensesMap.get(p.id));
                        pcMap.put(p.id, pc);
                    }
                }
                
            }else if(p.travel_type__c=='Driving Greater than 60 miles'){
                
                
                decimal x = 0;
                if(client=='Walmart'){
                    if(p.Roundtrip_Mileage__c!=null){
                        x = integer.valueof(p.Roundtrip_Mileage__c) * 0.575;
                    }else{
                        x=0;
                    }
                }else if(client!='HCRManorcare'){
                    x=330;
                }
                
                id i = hasType(expensesMap.get(p.id),'Driving Stipend');
                if(i==null){
                    coeList.add(this.addExpense('Driving Stipend', x, p.id, PatientCase__c.getInstance().allowance__c));
                }
                this.deleteExpense(hasType(expensesMap.get(p.id),'Baggage'));
                this.deleteExpense(hasType(expensesMap.get(p.id),'Driving Stipend to the Airport/Station'));
                
                if(p.Prepaid_Card_Initial_Load_Amount__c==null){
                    pcMap.put(p.id, new patient_case__c(id=p.id, Prepaid_Card_Initial_Load_Amount__c=x+dailyStipendAmount(p.nClient_Name__c, p.travel_type__c, p.Hotel_Name__c) * 3));
                }    
                
            }  
            
        }
        
        
        
        if(pcMap.keySet().size()>0){
            
            for(id i : pcMap.keySet()){
                pcListToUpdate.add(pcMap.get(i));
            }
            
            update pcListToUpdate;
        }
        
        if(coeList.size()>0){
            upsert coeList;
        }       
        
    }
    
    decimal calcAirfare(coe_expenses__c[] coeList){
    
        decimal x =0;
        
        if(coeList==null){return x;}
        
        for(coe_expenses__c c: coeList){
            if(c.payment_amount__c != null && c.type__c.contains('Airfare')){
                x += c.payment_amount__c;
            }
        }
    
        return x;
    }
    
    public void updateCoeList(){
        if(coeList.size()>0){
            upsert coeList;
        }
    }
    
    public COE_Expenses__c[] getCoeList(){
        return coeList;
    }
    
    public static void offsetUpdate(patient_case__c[] pcList){
        
        
        set<id> pIds = new set<id>();
        for(patient_case__c p :pcList){
            pIds.add(p.id);
        }
        
        coeExpenses ce = new coeExpenses();
        ce.expensesMap = ce.eMapper(pIDs);
        
        for(patient_case__c p : pcList){
        
            if(p.travel_type__c=='Flying'){
            
                p.airfare_offset__c = ce.calcAirfare(ce.expensesMap.get(p.id));
                
                if(p.Roundtrip_Mileage_to_the_Airport_Station__c==null){
                    p.Roundtrip_Mileage_to_the_Airport_Station__c=0;
                }
                
                p.Driving_Stipend_Mileage_Offset__c = p.Roundtrip_Mileage_to_the_Airport_Station__c * 0.24;
                
            }else if(p.travel_type__c=='Driving Greater than 60 miles'){
                
                if(p.mileage__c != null && p.mileage__c != 'Calculating...'){
                       p.Driving_Stipend_Mileage_Offset__c = integer.valueof(p.mileage__c) * .24;
                   }
                
            }
               
               if(p.Number_of_Hotel_Days__c !=null && p.hotel_rate__c!=null){
               
               if(p.hotel_rate__c>100.00){
                   p.hotel_offset__c = p.Number_of_Hotel_Days__c *100.00; 
               }else{
                   p.hotel_offset__c = p.Number_of_Hotel_Days__c *p.hotel_rate__c; 
               }
                   
               if(p.Days_in_Hospital__c != null){
                   p.hotel_offset__c = p.hotel_offset__c - (p.Days_in_Hospital__c *50);
               }
               
               }
               
               if(p.hotel_offset__c==null){p.hotel_offset__c=0;}
               if(p.Driving_Stipend_Mileage_Offset__c==null){p.Driving_Stipend_Mileage_Offset__c=0;}
               if(p.Airfare_Offset__c==null){p.Airfare_Offset__c=0;}
               p.Total_Taxable_Offset__c= p.Airfare_Offset__c + p.hotel_offset__c + p.Driving_Stipend_Mileage_Offset__c;
               
               if(p.Total_Expenses_Paid__c==null){p.Total_Expenses_Paid__c=0;}
               p.Total_taxable_income__c = p.Total_Expenses_Paid__c - p.Total_Taxable_Offset__c;
               
        }
        
        update pcList;   
        
    }
    
    public static void expenseTotalUpdate(COE_Expenses__c[] coeList){
        
        map<id, decimal> totalMap = new map<id, decimal>();
        map<id, decimal> airFareMap = new map<id, decimal>();
        map<id, decimal> dailyStipendMap = new map<id, decimal>();
        map<id, decimal> mileageMap = new map<id, decimal>();
        
        patient_case__c[] pList = new patient_case__c[]{};
        
        for(COE_Expenses__c c : coeList){
            system.debug('COE Expense************** '+c.type__c+' '+c.id);
            if(c.paid_date__c != null && c.payment_amount__c != null){
                decimal t = totalMap.get(c.patient_case__c);
                if(t==null){t=0;}
                t += c.payment_amount__c;
                totalMap.put(c.patient_case__c, t);
            
            }
            
            if(c.type__c!=null){
            
                if(airFareMap.get(c.patient_case__c)==null){
                    airFareMap.put(c.patient_case__c,0);
                }
                
                if(mileageMap.get(c.patient_case__c)==null){
                    mileageMap.put(c.patient_case__c,0);
                }
                
                if(dailyStipendMap.get(c.patient_case__c)==null){
                    dailyStipendMap.put(c.patient_case__c,0);
                }
                
                if(c.type__c.toLowerCase().contains('airfare')){
                    decimal a = airFareMap.get(c.patient_case__c);
                    if(a==null){a=0;}
                    a += c.payment_amount__c;
                    airFareMap.put(c.patient_case__c, a);    
                }
                
                if(c.type__c.contains('Daily Stipend') || c.type__c.contains('Additional Stipend')){
                    decimal a = dailyStipendMap.get(c.patient_case__c);
                    if(a==null){a=0;}
                    a += c.payment_amount__c;
                    dailyStipendMap.put(c.patient_case__c, a);    
                }
                
                if(c.type__c.contains('Driving')){
                    system.debug('In Driving');
                    decimal a = mileageMap.get(c.patient_case__c);
                    if(a==null){a=0;}
                    a += c.payment_amount__c;
                    mileageMap.put(c.patient_case__c, a);    
                    
                }
                
           }    
            
        }          
        system.debug(mileageMap);
        for(id i : totalMap.keySet()){
            patient_case__c p = new patient_case__c(id=i, Total_Expenses_Paid__c = totalMap.get(i), Daily_Stipend_Total__c=dailyStipendMap.get(i),airfare_total__c=airFareMap.get(i),Mileage_Stipend_Total__c=mileageMap.get(i));
            pList.add(p);   
        }
        
        if(pList.size()>0){
            update pList;
        }
    
    }
    
    public map<id, COE_Expenses__c[]> eMapper(set<id> pcID){
        
        if(expensesMap!=null){return expensesMap;}
        
        map<id, COE_Expenses__c[]> eMap = new map<id, COE_Expenses__c[]>();
        
        for(COE_Expenses__c e : [select type__c,patient_case__c,Payment_Amount__c , paid_date__c, recordTypeID from COE_Expenses__c where patient_case__c in :pcID]){
            
            COE_Expenses__c[] cList;
            cList = eMap.get(e.patient_case__c);
            if(cList==null){cList = new COE_Expenses__c[]{};}
            cList.add(e);
            eMap.put(e.patient_case__c, cList);
            
        }
        
        return eMap;
    }
//10-14-19 o.brown methods from the case controller
    public void saveNetworkException() {
        isError = false;
        message = networkException.saveException();
        if (message != null) {
            isError = true;
        }
    }    
    
    public selectOption[] getphysicianOpts() {
        return coeClientProviders.providerSelectOptions(obj, obj.client_facility__c);
    }    

    map<string, id> carrierIdMap = new map<string, id>();

    public selectOption[] getcarrierOpts() {
        if (obj.client__c == null) {
            return null;
        }
        selectOption[] foo = coeSelfAdminPickList.selectOptionList(null, obj.client__c, null, null, coeSelfAdminPickList.carrierName, obj.Client_Carrier__c, true);

        return foo;
    }

    public selectOption[] getemployeeHealthPlanOpts() {

        if (obj.Client_Carrier__c == null) {
            return new selectoption[] {
                new selectoption('', 'No carrier set')
            };
        }
        return coeSelfAdminPickList.selectOptionList(obj, obj.client__c, null, null, coeSelfAdminPickList.employeeHealthplanName, obj.Employee_Primary_Health_Plan_Name__c, true);
    }
    
    public selectOption[] getreferredFacilityOpts() {
        return coeSelfAdminPickList.selectOptionList(obj, obj.client__c, obj.ecen_procedure__c, null, coeSelfAdminPickList.referredFacility, obj.nReferred_facility__c, true);
    }    

    public selectOption[] gethotelOpts() {
        return coeSelfAdminPickList.selectOptionList(null, obj.client__c, obj.ecen_procedure__c, obj.client_facility__c, coeSelfAdminPickList.hotelName, null, true);

    }
    
    public void checkdup() {
        if (obj.id == null && obj.Employee_First_Name__c != null && obj.Employee_Last_Name__c != null) {
            searchController sc = new searchController(false, obj.Employee_First_Name__c, obj.Employee_Last_Name__c);
            theDupRecord =null;
            try {
                patient_case__c so = (patient_case__c) sc.findDup();
                if (so != null) {
                    theDupRecord = new dupRecord(so.employee_first_name__c, so.employee_last_name__c, so.employee_city__c, so.employee_state__c, so.employee_dobe__c, so.id);
                }
            } catch(exception e) {
                /*catch null silently and continue*/
                system.debug(e.getMessage());
            }

        }

    }
    
    public class dupRecord {
        public string fName {
            get;
            set;
        }
        public string lname {
            get;
            set;
        }
        public string city {
            get;
            set;
        }
        public string state {
            get;
            set;
        }
        public string dob {
            get;
            set;
        }
        public id id {
            get;
            set;
        }

        public dupRecord(string fName, string lname, string city, string state, string dob, id id) {

            string fdob = dob;
            this.fname = fname;
            this.lname = lname;
            this.city = city;
            this.state = state;

            if (fdob != null) {
                date foo = date.valueof(dob);
                fdob = foo.day() + '/' + foo.month() + '/' + foo.year();
            }

            this.dob = fdob;
            this.id = id;
        }
    }        

        Public void loadExpenses() {
        expenses = new coe_expenses__c[] {};
        allowances = new coe_expenses__c[] {};
        paycards = new coe_expenses__c[] {};
        hotelpaycards = new coe_expenses__c[] {};

        for (COE_Expenses__c c: [select Daily_Allowance__c,load_date__c, load_amount__c,Number_of_days__c, RecordType.Name, Patient_Case__c, CreatedBy.name, createdDate, Type__c, Note__c, Payment_Type__c, Payment_Amount__c, Paid_Date__c,Total_load__c from COE_Expenses__c where Patient_Case__c = :obj.id order by paid_date__c desc, createdDate desc]) {
            if (c.recordType.Name == 'Expense') {
                expenses.add(c);
            } else if (c.recordType.Name == 'Allowance') {
                allowances.add(c);
            } else if(c.recordType.Name == 'HotelPayCard'){
                hotelpaycards.add(c);
            } else {
                paycards.add(c);
            }
        }

    }

    public void loadFinance() {
        
        financeList = new COE_Finance__c [] {};
        financeList = [select Airfare_Original_Patient__c, Airfare_Tax_Patient__c, Airfare_Original_Caregiver__c, Airfare_Tax_Caregiver__c, Airfare_Adjustment_Patient__c, Airfare_Adjustment_Caregiver__c, Baggage__c, Baggage_Tax__c, Change_Fee__c, Change_Reason__c,Rate_Per_Night__c,Total_Amount__c,
        Hotel_Tax__c,Additional_or_Cancellation_of_Nights__c,Currency_Change__c,Hotel_Hospital_Transport__c,Hotel_Transportation_Tax__c,Airport_Transport__c,Hotel_Parking__c,Mileage__c,Rate_Per_Mile__c,Mileage_to_Airport__c,Mileage_to_COE__c,Reason_for_Mileage_Change__c,Number_of_Days__c,Daily_Allowance__c,
        Total_Initial_Card_Load__c,Stipend_Tax__c,Card_Number__c,Card_Expiration__c,Initial_Load_Date__c,Airfare_Patient_Actual_Amount__c,Airfare_Caregiver_Actual_Amount__c,Total_Airfair__c,Hotel_Estimated_Amount__c,Hotel_Actual__c,Month_Billed_Client__c,Sent_to_Finance__c,Comments__c,
        Quarter_Taxable_Sent_to_Employer__C from COE_Finance__c where patient_case__c = :obj.id order by createdDate desc];
        system.debug(financeList);
        if (financeList.size() > 0) {
            finance = financeList[0];
            
        }else{
            finance =  new COE_Finance__c(patient_case__c=obj.id);
        }
        system.debug(finance);    
       

    }

    public void saveFinance(){
        
        isError = false;
        if(obj.isConverted__c == true){
        try{
            upsert finance;
            loadFinance();
            
        }catch(dmlexception e){
            isError=true;
            for (Integer i = 0; i < e.getNumDml(); i++){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)+' '+e.getLineNumber()); 
            }
        }        
        }
        
    }

        Public void updatePayCardTotal() {
        decimal x = obj.Prepaid_Card_Initial_Load_Amount__c;
        if (x == null) {
            x = 0;
        }

        for (coe_expenses__c c: paycards) {
            x += c.Load_Amount__c;
        }

        obj.Total_Paycard_load__c = x;
        try {
            update obj;
        } catch(exception e) {
            system.debug(e.getMessage());
        }
    }

    public void savePaycard() {

        paycard.patient_case__c = obj.id;
        upsert paycard;
        loadExpenses();
        updatePayCardTotal();
        //init();

    }
    
    public void deletePaycard() {
        string theID = ApexPages.currentPage().getParameters().get('paycardID');
       
        coe_expenses__c dela;

        for (coe_expenses__c a: paycards) {

            if (a.id == theID) {
                dela = a;
                break;
            }

        }

        if (dela != null) {
            delete dela;
            loadExpenses();
            updatePayCardTotal();
            //init();

        }
    }    

    public void editPaycard() {
        string theID = ApexPages.currentPage().getParameters().get('paycardID');
         
        if (theID == null) {
            paycard = new coe_expenses__c(patient_case__c = obj.id, recordTypeId = PatientCase__c.getInstance().paycard__c, load_amount__c = null, load_date__c = null, Paid_Date__c = null, payment_amount__c = null);
            return;
        }

        for (coe_expenses__c a: paycards) {
           system.debug('paycardid='+a.id);
            if (a.id == theID) {
                paycard = a;
                break;
            }

        }

    }

    public void editHotelPaycard() {
        string theID = ApexPages.currentPage().getParameters().get('hotelPaycardID');
        
        if (theID == null) {
            hotelpaycard = new coe_expenses__c(patient_case__c = obj.id, recordTypeId = PatientCase__c.getInstance().hotelpaycard__c, load_amount__c = null, load_date__c = null, Paid_Date__c = null, payment_amount__c = null);
            return;
        }

        for (coe_expenses__c a: hotelpaycards) {
           system.debug('hotelpaycardid='+a.id);
            if (a.id == theID) {
                hotelpaycard = a;
                break;
            }

        }

    }
    
    public void saveHotelPaycard() {

        hotelpaycard.patient_case__c = obj.id;
        upsert hotelpaycard;
        loadExpenses();
        updateHotelPayCardTotal();
        //init();

    }    

    public void deleteHotelPaycard() {
        string theID = ApexPages.currentPage().getParameters().get('hotelPaycardID');
       
        coe_expenses__c dela;

        for (coe_expenses__c a: hotelpaycards) {

            if (a.id == theID) {
                dela = a;
                break;
            }

        }

        if (dela != null) {
            delete dela;
            loadExpenses();
            updateHotelPayCardTotal();
            //init();

        }
    }

    void updateHotelPayCardTotal() {
        decimal x = 0;

        for (coe_expenses__c c: hotelpaycards) {
        if(c.Payment_Amount__c>0){
        x += c.Payment_Amount__c;
        }
            
        }

        finance.Hotel_Actual__c = X;
        try {
            update finance;
        } catch(exception e) {
            system.debug(e.getMessage());
        }
    }

    public void saveAllowance() {

        allowance.patient_case__c = obj.id;
        upsert allowance;
        loadExpenses();
        COE_Expenses__c[] clist = new COE_Expenses__c[] {};
        clist.addall(allowances);
        clist.addall(expenses);

        coeExpenses.expenseTotalUpdate(clist);
        //init();

    }
    
    public void deleteAllowance() {
        string theID = ApexPages.currentPage().getParameters().get('allowanceID');

        coe_expenses__c dela;

        for (coe_expenses__c a: allowances) {
            if (a.id == theID) {
                dela = a;
                break;
            }
        }

        if (dela != null) {
            delete dela;
            loadExpenses();
        }

    }
    
    public void editAllowance() {
        string theID = ApexPages.currentPage().getParameters().get('allowanceID');

        if (theID == null) {
            allowance = new coe_expenses__c(patient_case__c = obj.id, recordTypeId = PatientCase__c.getInstance().allowance__c, Paid_Date__c = null, payment_amount__c = null);
            return;
        }

        for (coe_expenses__c a: allowances) {

            if (a.id == theID) {
                allowance = a;
                break;
            }

        }

    }    
    
    //public void saveOncologyExpense() {
    //    expense.oncology__c = o.id;
    //    saveExpense();
   // }
    
    public void savePCExpense() {
        expense.patient_case__c = obj.id;
        saveExpense();
    }
    
    void saveExpense() {

        upsert expense;
        loadExpenses();

        coeExpenses ce = new coeExpenses();
        ce.travelHandler(new patient_case__c[] {
            obj
        });

        COE_Expenses__c[] clist = new COE_Expenses__c[] {};
        clist.addall(allowances);
        clist.addall(expenses);

        coeExpenses.expenseTotalUpdate(clist);

        try {

            //update obj;
            //init();
        } catch(exception e) {
            system.debug(e.getMessage());
        }
    }

    public void deleteExpense() {
        string theID = ApexPages.currentPage().getParameters().get('expenseID');

        coe_expenses__c dela;

        for (coe_expenses__c a: expenses) {

            if (a.id == theID) {
                dela = a;
                break;
            }

        }

        if (dela != null) {
            delete dela;
            loadExpenses();
            //init();

        }
    }    

    public void editExpense() {
        string theID = ApexPages.currentPage().getParameters().get('expenseID');

        if (theID == null) {
            expense = new coe_expenses__c(patient_case__c = obj.id, recordTypeId = PatientCase__c.getInstance().expense__c, Paid_Date__c = null, payment_amount__c = null);
            return;
        }

        for (coe_expenses__c a: expenses) {

            if (a.id == theID) {
                expense = a;
                break;
            }

        }

    }

}