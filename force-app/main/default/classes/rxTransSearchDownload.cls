public class rxTransSearchDownload {

    public rxAudit__c[] rxTns {get; set;}
    public string ptName {get; private set;}
    
    
    public rxTransSearchDownload(){
        ptName = 'rxSearch';
        rxTns = (rxAudit__c[])JSON.deserialize(EncodingUtil.urlDecode(ApexPages.CurrentPage().getParameters().get('data'), 'UTF-8'),rxAudit__c[].class);
       
        for(rxAudit__c r : rxTns){
            r.ClaimNbr__c = '\'' + r.ClaimNbr__c;    
            r.Patseq__c = '\'' + r.Patseq__c; 
            r.Essn__c = '\'' + r.Essn__c ;    
        }
    }

}