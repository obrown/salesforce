public with sharing class oncologyManualReferralExt {
    
    public Oncology_Physician__c[] physicians {get; private set;}
    
    public oncologyManualReferralExt(ApexPages.StandardController controller) {
        physicians = new Oncology_Physician__c[]{}; 
        physicians = [select Local_Physician__c,Associated_Facilities__c, City__c, Credentials__c,First_Name__c, Fax_Number__c,Last_Name__c,Phone_Number__c,Specialty__c,State__c,Street_Address__c,Zip_Code__c from Oncology_Physician__c where  oncology__c  = :controller.getId()];
    
    }

}