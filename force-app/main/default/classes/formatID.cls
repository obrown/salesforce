public with sharing class formatID {

    public string formatID(string myid,string clientName){
        
        string newMyid = scrapeNumeric(myid);
        newMyid = addZeros(newMyid,9);
            
        if(clientName == 'Walmart'){
            newMyid = 'WMT' + newMyid;                
        
        }

        if(clientName == 'Lowes'){
            newMyid = 'LWE' + newMyid;
        }

        if(clientName == 'HCR ManorCare'){
            newMyid = 'HCR' + newMyid;
        }

        if(clientName == 'PepsiCo'){
            newMyid = 'PEP' + newMyid;
        }   
        
        if(clientName == 'Kohls'){
            newMyid = 'KSS' + newMyid;
        }

        if(clientName == 'McKesson'){
            newMyid = 'MCK' + newMyid;
        }
        
        if(clientName == 'jetBlue'){
            newMyid = 'JBL' + newMyid;
        }
        
        if(clientName == 'NextEra Energy'){
            newMyid = 'NEE' + newMyid;
        }
        
        if(clientName == 'Leggett & Platt'){
            newMyid = 'LEG' + newMyid;
        }
        
        return newMyid;
    }
    
    public string scrapeNumeric(string id){
        
        string newId = id;
        
        string theReturnID='';
        
        if(newId.indexof('W') > 5){ //5 is somewhat arbitrary...
            newId = newId.left(id.indexof('W'));  
        }
        
        for(integer x=0; x<newId.length();x++){
            
            if(newId.substring(x, x+1).isNumeric()){
                theReturnID += newId.substring(x, x+1);
            }
        }
        return theReturnID;
    }
    
    public string addZeros(string id,integer x){
        string newId = id;
        integer z = x-id.length();
        
        if(z<0){
            newId = newId.substring(newId.length()-(newId.length()+z),newId.length()); 
        }else{
            for(integer i=0;i<z;i++){
                newId = '0' + newId;
            }
        }
        return newId;
    }
    
}