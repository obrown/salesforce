public with sharing class googleMapsStruct{

    public List<String> destination_addresses;
    public List<String> origin_addresses;
    public List<Rows> rows;
    public String status;

    public class Elements {
        public Distance distance;
        public Distance duration;
        public String status;
    }

    public class Distance {
        public String text;
        public Integer value;
    }

    public class Rows {
        public List<Elements> elements;
    }

    public static googleMapsStruct parse(String json) {
        return (googleMapsStruct) System.JSON.deserialize(json, googleMapsStruct.class);
    }
    
        public class Address_components {
        public String long_name;
        public String short_name;
        public List<String> types;
    }

    public class Geometry {
        public Bounds bounds;
        public Northeast location;
        public String location_type;
        public Bounds viewport;
    }

    public List<Results> results;
    
    public class Results {
        public List<Address_components> address_components;
        //public String formatted_address {get; set;}
        public Geometry geometry;
        public String place_id;
        public List<String> types;
    }

    public class Bounds {
        public Northeast northeast;
        public Northeast southwest;
    }

    public class Northeast {
        public Double lat;
        public Double lng;
    }

}