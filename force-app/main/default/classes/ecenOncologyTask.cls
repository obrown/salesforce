public without sharing class ecenOncologyTask{
    
    
     ecenPatientInformation pinfo = new ecenPatientInformation();
     ecenDashBoardStruct dashboard = new ecenDashBoardStruct();
     ecenTaskList TaskList = new ecenTaskList();
        
     public ecenPatientInformation getPInfo(oncology__c oncology){
         pinfo.dashboard = dashboard;
         pinfo.dashboard.WelcomeMessage = 'Thank you for signing into the Contigo Health application.  In order to use this application, you must have an open case with Contigo Health created/open through your employer.    If you feel you have received this message in error, please call 888-463-3737.';
         pinfo.dashboard.ToDoItemsComplete='0%';
         pinfo.TaskList = TaskList;   
         
         if(oncology.id==null){
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
           }
           return pinfo;
        }else{
           ecenDashBoardTimeline.timelineRibbonItem timelineRibbonItem= new ecenDashBoardTimeline.timelineRibbonItem();
           ecenDashBoardTimeline timelineRibbon= new ecenDashBoardTimeline();
           map<integer, ecenDashBoardTimeline.timelineRibbonItem> trMap = new map<integer, ecenDashBoardTimeline.timelineRibbonItem>();
            
            trMap.put(1, new ecenDashBoardTimeline.timelineRibbonItem(1, 0, 'Referral - 1st Step'));
            trMap.put(2, new ecenDashBoardTimeline.timelineRibbonItem(2, 0, 'Treatment Decision'));
            trMap.put(3, new ecenDashBoardTimeline.timelineRibbonItem(3, 0, 'Travel for Care'));
            
            ecenDashBoardStruct.milestone[] graphMilestones= new ecenDashBoardStruct.milestone[]{};
            
            //timelineRibbon && graphRibbon
            
            if(oncology.referral_date__c!=null){
                timelineRibbonItem = trMap.get(1);
                timelineRibbonItem.Completed=1;
                trMap.put(1, timelineRibbonItem);
            }
                        
            if(oncology.determination_date__c!=null && oncology.Determination_Outcome__c=='Program Eligibility Met'){
                timelineRibbonItem = trMap.get(2);
                timelineRibbonItem.Completed=1;
                trMap.put(2, timelineRibbonItem);
            }
            
            timelineRibbon.items.add(trMap.get(1));
            timelineRibbon.items.add(trMap.get(2));
            
            
            //Welcome Message
            dashboard.WelcomeMessage= 'Welcome to the HDP Oncology Application!';
            
            
            // Patient Tasks
            
            map<string, integer> tasksMap = new map<string, integer>();
            tasksMap.put('referral', 1);
            tasksMap.put('forms', 0);
            tasksMap.put('nicotine', 2);
            tasksMap.put('appointments', 0);
            tasksMap.put('booktravel', 0);
            integer isNicotine=0;
            
            if(oncology.HDP_Program_Forms_Completed__c && oncology.Date_HDP_Forms_Completed__c!=null){
                tasksMap.put('forms', 1); //forms completed
            
            }

            if(oncology.Send_POC_Auth__c){
                tasksMap.put('appointments', 1);
                
            }

            if(oncology.booking_reference_number__c!=null){
               tasksMap.put('booktravel', 1);
                
            }
            
            if(tasksMap.get('referral')==0){
                TaskList.Pending.add(new ecenTaskList.Task('Facility Referral', 1, 'HDP'));         
            }else{
                TaskList.Completed.add(new ecenTaskList.Task('Referred to ' + oncology.client_facility__r.facility__r.name, 1, 'HDP ('+utilities.formatUSdate(date.valueof(oncology.referral_date__c))+')'));
                
            }
            
            
            
            if(tasksMap.get('forms')==0){
                TaskList.Pending.add(new ecenTaskList.Task('Complete HDP Forms', 2, null)); 
            }else{
                
                TaskList.Completed.add(new ecenTaskList.Task('HDP Forms Completed', 2, oncology.patient_first_name__c+' '+oncology.patient_last_name__c + ' ('+utilities.formatUSdate(oncology.Date_HDP_Forms_Completed__c)+')'));
            }
            
            
            if(tasksMap.get('appointments')==0){
                TaskList.Pending.Add(new ecenTaskList.Task('Confirm appointment Dates', 3, 'HDP')); 
            }else{
                TaskList.Completed.Add(new ecenTaskList.Task('Appointments Confirmed', 3, 'HDP ('+utilities.formatUSdate(date.valueof(oncology.referral_date__c))+')'));
                
            }
            
            if(tasksMap.get('booktravel')==0){
                TaskList.Pending.Add(new ecenTaskList.Task('Book Travel with American Express', 4, oncology.patient_first_name__c+' '+oncology.patient_last_name__c));  
            }else{
                TaskList.Completed.Add(new ecenTaskList.Task('Travel Booked with American Express', 4, oncology.patient_first_name__c+' '+oncology.patient_last_name__c+ ' ('+utilities.formatUSdate(date.valueof(oncology.referral_date__c))+')'));
                
                
            }
            
            integer toDoItemsComplete;
            
            if(TaskList.Pending.isEmpty()){
                toDoItemsComplete = 100;
                
                timelineRibbonItem = trMap.get(3);
                timelineRibbonItem.Completed=1;
                trMap.put(3, timelineRibbonItem);
                
            
                
            }else{
                decimal pnd = TaskList.Pending.size();
                toDoItemsComplete =  100-integer.valueof(pnd.divide(TaskList.Pending.size()+TaskList.Completed.size(),1)*100);
            }
            
            timelineRibbon.items.add(trMap.get(3));
            
            dashboard.ToDoItemsComplete = ToDoItemsComplete+'%';
            
            //End setting Dashboard
            
            //Set todo sortOrder list 
            
            if(!TaskList.Pending.isEmpty()){
                for(integer i=0; i<TaskList.Pending.size(); i++){
                    TaskList.pending[i].sortOrder = i+1;
                
                }
            
            }
            
            for(integer i=0; i<TaskList.Completed.size(); i++){
                    TaskList.Completed[i].sortOrder = i+1;
                
            }
            
            pinfo.dashboard = dashboard; 
            pinfo.TaskList = TaskList;
            pinfo.timelineRibbon = timelineRibbon;
            if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
            } 
            
        }
            
         return pInfo;
     
     }
     
}