public with sharing class hp_eligSearch extends hp_callWebApp{
    
    public hpEligDetailStruct eligResult = new hpEligDetailStruct();
    
    public static hpEligDetailStruct searchHP(string ssn, string uw, string grp,  string sequence, string effectiveDate){
        
        hp_eligSearch searchHP = new hp_eligSearch();
        searchHP.searchHealthPac(ssn, uw, grp, sequence, effectiveDate);
        return searchHP.eligResult;
        
    }
    
    public hpEligDetailStruct searchHealthPac(string ssn, string uw, string grp,  string Sequence, string effectiveDate){
        
        endpoint='elig/getelig';
        
        search.add('SSN:'+ssn);
        search.add('EffectiveDate:'+effectiveDate);
        search.add('Underwriter:'+uw);
        search.add('Group:'+grp);
        search.add('Sequence:'+Sequence);
        
        if(Sequence=='00'){
            search.add('LookupType:ELG');
        }else{
            search.add('LookupType:DLG');
        }
        
        jsonBody = jb.eligSearch(search);
        if(test.isRunningtest()){
            result = '{"ResultCode":"0","EligibilityInformation":{"":{"Underwriter":"001","Group":"MO1","Ssn":"528206757","DepSsn":"","EligDate":"20190101","Sequence":"","Salary":"       .00","Status":"T","MedicalBenefitPlan":"001MO1F   ","MedicalCOBFlag":"S","Dept":"        ","MedicalFamilyCoverageCode":"S","KeepFlag":"delete","PrevStatus":"R"}}}';    
        }else{
            result = callWebApp(null);
        }
        eligResult = (hpEligDetailStruct)JSON.deserialize(result, hpEligDetailStruct.class); 
        
        return eligresult;
    
    }
    
}