@RestResource(urlMapping='/providerPortal/v1/providerportaltasks')
global with sharing class ProviderPortalTaskListener {
    
    @HttpPost
    global static void save_tasks() {
        
        RestResponse res = RestContext.response;
        string caseName = RestContext.request.headers.get('EcenCase');
        blob requestBodyBlob = RestContext.request.requestBody;
        
        string jsonBody = requestBodyBlob.toString();
        system.debug(jsonBody);
        system.debug('caseName : '+ caseName );
        providerPortalTaskWorkflow.result result = providerPortalTaskWorkflow.saveTasks(jsonBody, caseName);
        res.addHeader('Content-Type', 'application/json');
        system.debug(JSON.serialize(result));
        if(result == null || result.isError){
            res.responseBody = Blob.valueOf(JSON.serialize(result));
            res.statusCode = 500; 
        }else{
            res.responseBody = Blob.valueOf(JSON.serialize(result));
            result.isError=false;
            res.statusCode = 200; 
        }    
    }
    
    @HttpGet
    global static void get_tasks() {
        
        RestResponse res = RestContext.response;
        string caseName = RestContext.request.headers.get('EcenCase');
        blob requestBodyBlob = RestContext.request.requestBody;
        
        providerPortalTaskWorkflow.result result = providerPortalTaskWorkflow.getTasks(caseName);
        res.addHeader('Content-Type', 'application/json');
        system.debug(JSON.serialize(result));
        if(result == null || result.isError){
            res.responseBody = Blob.valueOf(JSON.serialize(result));
            res.statusCode = 500; 
        }else{
            
            res.responseBody = Blob.valueOf(JSON.serialize(result.tasks));
            res.statusCode = 200; 
        }    
    }
}