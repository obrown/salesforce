public without sharing class coeTestData{
    
    string clientName = 'Walmart';
    string procedureName = 'Joint';
    string facilityName = 'Johns Hopkins';
    string hotelName = 'Best Western';
    
    public string clientID;
    public string clientFacilityId;
    public string procedureId;
    public string hotelId;
    
    public map<string, id> relationshipMap = new map<string, id>();
    
    public id createClient(string clientName){
       if(clientName==null){
           clientName = this.clientName;
       }
       
       client__c client = new client__c(name=clientName);
       insert client;
       return client.id;
    }

    public id createProcedure(string procedureName){
       if(procedureName==null){
           procedureName= this.procedureName;
       }
       
       
       procedure__c procedure = new procedure__c(name=procedureName);
       insert procedure;
       return procedure.id;
    }

    public id createFacility(string facilityName){
       if(facilityName==null){
           facilityName= this.facilityName;
       }
       
       facility__c facility = new facility__c(name=facilityName);
       facility.Facility_Address__c = '';

       facility.Facility_City__c ='Baltimore';
       facility.Facility_State__c ='MD';
       facility.Facility_Zip__c ='21224';
       
       insert facility;
       return facility.id;
    }
    
    public void createRelationships(){
        EmployeeRelationShip__c[] records = new EmployeeRelationShip__c[]{};
        records.add(new EmployeeRelationShip__c(name='Employee'));
        records.add(new EmployeeRelationShip__c(name='Spouse'));
        records.add(new EmployeeRelationShip__c(name='Significant Other'));
        records.add(new EmployeeRelationShip__c(name='Caregiver'));
        records.add(new EmployeeRelationShip__c(name='Dependent'));
        records.add(new EmployeeRelationShip__c(name='Other'));
        
        for(EmployeeRelationShip__c r : records){
            relationshipMap.put(r.name, r.id);
        }
        
        insert records;
        
    }
    
    public id createClientFacility(id clientID, id facilityID, id procedureID){
       system.debug(clientID);
       system.debug(facilityID);
       system.debug(procedureID);
       client_facility__c cf = new client_facility__c(client__c=clientID, facility__c=facilityID, procedure__c=procedureID, active__c=true);
       cf.Authorization_Email_Contact__c = 'test@test.com';
       cf.Referral_Email_Contact__c = 'test@test.com';
       
       insert cf;
       
       return cf.id;
    }
    
    public EmployeeContacts__c addEmployee(){
        EmployeeContacts__c employee = new EmployeeContacts__c();
        employee.FirstName__c='John';
        employee.LastName__c='Doe';
        employee.Gender__c='Male';
        employee.SSN__c='111111111';
        employee.InsuranceID__c = '11223344';
        employee.Dobe__c='1980-01-01';
        employee.PrimaryLanguageSpoken__c = 'English';
        employee.Translation_Services_Required__c = 'No';
        
        employee.StreetAddress__c= '123 E Main St';
        employee.City__c= 'Mesa';
        employee.State__c= 'AZ';
        employee.ZipCode__c= '85212';       
        
        //employee.HomePhoneNumber__c;
        employee.MobilePhoneNumber__c='(480) 555-1212';
        //employee.WorkPhoneNumber__c;        
        insert employee;
        
        RelatedtoEmployee__c rte = new RelatedtoEmployee__c();
        rte.EmployeeContactID__c = employee.id;
        rte.EmployeeRelationID__c = relationshipMap.get('Employee');
        rte.RelatedEmployeeID__c = employee.id;
        
        insert rte;
        
        return employee;
        
    } 
    
    public id addPatient(){
        EmployeeContacts__c patient = new EmployeeContacts__c();
        return null;
        
    } 
}