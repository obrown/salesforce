public with sharing class umDenialMedicalDocumentation{
    
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c  uml, Utilization_Management_Clinician__c clinician){
        string letterText='';
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c+'<br/>';
        letterText+=Clinician.Street__c+'<br/>';
        letterText+=Clinician.City__c+'&nbsp';
        letterText+=Clinician.State__c+'&nbsp';
        letterText+=Clinician.Zip_Code__c+'<br/><br/>';
        letterText+='RE:&nbsp;&nbsp;Patient Name:&nbsp;'+um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c;
        letterText+='<br/>';
        letterText+='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth:&nbsp;'+um.patient__r.Patient_Date_of_Birth__c;
        letterText+='</div>';
        letterText+='<p>';
        letterText+='Dear&nbsp;'+Clinician.Salutation__c+'&nbsp;'+Clinician.Last_Name__c+',';
        letterText+='</p>';
        letterText+='<p>';  
        letterText+='Contigo Health is a third-party administrator (TPA) that performs Care Management Services for the self-funded group health plan of '+um.patient__r.Patient_Employer__r.name+', which is regulated by ERISA under the US Department of Labor.<br/>';
        letterText+='</p>';
        
                      
        letterText+='The request referenced below has been administratively denied:<br/><br/>';
        letterText+='<div style="margin-top:10px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Case Number:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px;">';
        letterText+='&nbsp;'+um.HealthPac_Case_Number__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Physician Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Facility Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+um.Facility_Name__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Dates of Service:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;Beginning '+um.Admission_Date__c.month()+'/'+um.Admission_Date__c.day()+'/'+um.Admission_Date__c.year();
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%;vertical-align:top">';
        letterText+='Type of Request:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+uml.Type_of_Request__c;
        letterText+='</div>';
        letterText+='</div>';
        
        letterText+='<br/>';
        letterText+='Denial Reason: Despite 3 requests on our part, no clinical information has been received to support medical necessity. Therefore, '+uml.Admission_of__c+' has been denied until such time as the information has been received and reviewed.<br/><br/>';

        letterText+='Sincerely,<br/><br/>';
        letterText+='Care Management Department<br/>';
        letterText+='Contigo Health<br/>';
        letterText+='1755 Georgetown Rd<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';
        
        letterText+='All choices regarding the care and treatment of the patient remain the responsibility of the attending physician. In no way does this letter attempt to dictate the care the patient ultimately receives.<br/><br/>';

        letterText+='<p>';
        letterText+='The plan complies with applicable Federal civil rights laws and does not discriminate on the basis of race, color, national origin, sex, age or disability above the appeal right section of the denial letters.';
        letterText+='</p>';
        
        letterText+='<div>';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='CC:'; 
        letterText+='</div>';
        letterText+='<div style="display:inline-block;">';
        letterText+=um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        boolean hasPatientAddress = (um.patient__r.Address__c!=null && um.patient__r.City__c!=null && um.patient__r.State__c!=null && um.patient__r.Zip__c!=null);
        
        letterText+='<div style="display:inline-block;"/>';
        letterText+=hasPatientAddress ? um.patient__r.Address__c : '';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasPatientAddress ? um.patient__r.City__c+', ' : '';
        letterText+=hasPatientAddress ? um.patient__r.State__c+' ' : '';
        letterText+=hasPatientAddress ? um.patient__r.Zip__c : '';
        letterText+='</div>';
        letterText+='</div><br/>';         
        
        letterText+='<div style="margin-top:10px">';
        
        letterText+='<div style="display:inline-block;">';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        boolean hasFacility = (um.Facility_Name__c!=null && um.Facility_Street__c != null && um.Facility_City__c  != null && um.Facility_State__c  != null && um.Facility_Zip_Code__c  != null);
        
        letterText+= hasFacility ? um.Facility_Name__c : '';
                      
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasFacility ? um.Facility_Street__c : '';
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasFacility ? um.Facility_City__c+', ' : '';
        letterText+=hasFacility ? um.Facility_State__c+' ' : '';
        letterText+=hasFacility ? um.Facility_Zip_Code__c : '' ;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='</div>';  
                                                                                                                                                                                                                                                                                      
        letterText+='</div>';
        return letterText;
    }
    
}