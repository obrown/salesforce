public with sharing class coeClientHotelOptions{
    
    
    public static string[] hotelList(id clientFacilityId){
        string[] foo = new string[]{};
        
        if(clientFacilityId==null){
            foo.add('No Facility Set Up');
            return foo;
        }
        
        for(ECEN_Hotel__c hotel : [select Hotel__r.name from ECEN_Hotel__c where client_facility__c = :clientFacilityId]){
           foo.add(hotel.Hotel__r.name); 
        }
        
        if(foo.size()==0){
            foo.add('No Hotels Set Up');
        }
        
        return foo;
    }

    public static selectOption[] hotelselectOptions(id clientFacilityId, string hotelName){
        selectOption[] foo = new selectOption[]{};
        set<string> hotelsAdded = new set<string>();
        
        if(clientFacilityId==null){
            foo.add( new selectoption('','No Facility Set'));
            return foo;
        }
        
        foo.add(new selectoption('','--None--'));
        system.debug(clientFacilityId);
        for(ECEN_Hotel__c hotel : [select Hotel__r.name from ECEN_Hotel__c where client_facility__c = :clientFacilityId]){
           foo.add( new selectoption(hotel.id, hotel.Hotel__r.name)); 
           //hotelsAdded.add(hotel.Hotel__r.name);
        }
        
        //if(hotelName!=null && !hotelsAdded.contains(hotelName)){
       //     foo.add(new selectoption(hotelName, hotelName));    
       // }
        
        return foo;
    }
}