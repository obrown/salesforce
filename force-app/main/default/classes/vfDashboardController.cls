public with sharing class vfDashboardController {
    
     // Return a list of data points for a chart
    public List<Data> getData() {
        return vfDashboardController.getChartData();
    }
    
    // The actual chart data; needs to be static to be
    // called by a @RemoteAction method
    public static List<Data> getChartData() {
        List<Data> data = new List<Data>();
        for(AggregateResult a : [Select count(id),patient_state__c FROM patient_case__c where patient_state__c !=null Group By patient_state__c order by count(id) desc limit 10]){
               data.add(new Data(string.valueOf(a.get('Patient_State__c')), integer.valueof(a.get('expr0'))));
            }
        return data;
    }
    
    // Wrapper class
    public class Data {
        public String name { get; set; }
        public Integer data1 { get; set; }

        public Data(String name, Integer data1) {
            this.name = name;
            this.data1 = data1;

        }
    }
    
    string year = string.valueOf(date.today().year());
    
    public integer getAvgRefTimeTotal(){
        return AvgRefTime(null, date.today());
    }

    //January
    public integer getAvgRefTime01(){
        return AvgRefTime(date.valueOf(year+ '-01-01'), date.valueOf(year+ '-01-31'));
    }
    
    //February
    public integer getAvgRefTime02(){
        return AvgRefTime(date.valueOf(year+ '-02-01'), date.valueOf(year+ '-02-28'));
    }    

    //March
    public integer getAvgRefTime03(){
        return AvgRefTime(date.valueOf(year+ '-03-01'), date.valueOf(year+ '-03-31'));
    } 
    
    //April
    public integer getAvgRefTime04(){
        return AvgRefTime(date.valueOf(year+ '-04-01'), date.valueOf(year+ '-04-30'));
    }

    //May
    public integer getAvgRefTime05(){
        return AvgRefTime(date.valueOf(year+ '-05-01'), date.valueOf(year+ '-05-31'));
    }
    
    //June
    public integer getAvgRefTime06(){
        return AvgRefTime(date.valueOf(year+ '-06-01'), date.valueOf(year+ '-06-30'));
    }    
    
    //July
    public integer getAvgRefTime07(){
        return AvgRefTime(date.valueOf(year+ '-07-01'), date.valueOf(year+ '-07-31'));
    }    
    
    //August
    public integer getAvgRefTime08(){
        return AvgRefTime(date.valueOf(year+ '-08-01'), date.valueOf(year+ '-08-31'));
    }    
    
    //September
    public integer getAvgRefTime09(){
        return AvgRefTime(date.valueOf(year+ '-09-01'), date.valueOf(year+ '-09-30'));
    }    
    
    //October
    public integer getAvgRefTime10(){
        return AvgRefTime(date.valueOf(year+ '-10-01'), date.valueOf(year+ '-10-31'));
    }    
    
    //November
    public integer getAvgRefTime11(){
        return AvgRefTime(date.valueOf(year+ '-11-01'), date.valueOf(year+ '-11-30'));
    }    
    
    //December
    public integer getAvgRefTime12(){
        return AvgRefTime(date.valueOf(year+ '-12-01'), date.valueOf(year+ '-12-31'));
    }    
    
    //Used to avg number of days from transfer to CM to referral
    integer AvgRefTime(date s, date e){ //start date, end date
        map<id, date> transferDateMap = new map<id, date>();
        
        if(s==null){
            s = date.valueof(string.valueof(date.today().year()) + '-01-01');
        }
        
        if(e==null){
            e = date.today();
        }
        
        for(Patient_Case__History p: [select parentid, oldvalue, newvalue, createdDate from Patient_Case__History where field = 'Owner' and createdDate >= :s and createdDate <= :e])
        {
            if(p.newvalue == 'HDP Care Management COE')
            {
                transferDateMap.put(p.parentid, date.valueof(p.createdDate));
            }
    
        }
    
        integer totalDays =0;
        integer totalRecords = 0;
        //and status__c != 'Closed' 
        if(transferDateMap.keySet().size()>0){
            for(Patient_Case__c p: [select Converted_Date__c from Patient_Case__c where isconverted__c = true and ID IN :transferDateMap.keyset() and converted_date__c !=null])
            {
                if(transferDateMap.get(p.id)<p.converted_date__c){
                    totalDays += transferDateMap.get(p.id).daysBetween(p.converted_date__c);
                    totalRecords++;
                }
            }
            if(totalRecords!=0 && totalDays!=0){
                return totalDays/totalRecords;
            }
        }
        return 0;
    }
    
    public integer getAvgRefTimefromCreatedTotal(){
        
        return AvgRefTimefromCreated(null, date.today());
    }
    
    //January
    public integer getAvgRefTimefromCreated01(){
        
        return AvgRefTimefromCreated(date.valueOf(year+'-01-01'), date.valueOf(year+'-01-31'));
    }
    
    //February
    public integer getAvgRefTimefromCreated02(){
        return AvgRefTimefromCreated(date.valueOf(year+'-02-01'), date.valueOf(year+'-02-28'));
    }    
 
    //March
    public integer getAvgRefTimefromCreated03(){
        return AvgRefTimefromCreated(date.valueOf(year+'-03-01'), date.valueOf(year+'-03-31'));
    }  

    //April
    public integer getAvgRefTimefromCreated04(){
        return AvgRefTimefromCreated(date.valueOf(year+'-04-01'), date.valueOf(year+'-04-30'));
    }  
    
    //May
    public integer getAvgRefTimefromCreated05(){
        return AvgRefTimefromCreated(date.valueOf(year+'-05-01'), date.valueOf(year+'-05-30'));
    }  

    //June
    public integer getAvgRefTimefromCreated06(){
        return AvgRefTimefromCreated(date.valueOf(year+'-06-01'), date.valueOf(year+'-06-30'));
    } 

    //July
    public integer getAvgRefTimefromCreated07(){
        return AvgRefTimefromCreated(date.valueOf(year+'-07-01'), date.valueOf(year+'-07-31'));
    } 
    
    //August
    public integer getAvgRefTimefromCreated08(){
        return AvgRefTimefromCreated(date.valueOf(year+'-08-01'), date.valueOf(year+'-08-31'));
    }     

    //September
    public integer getAvgRefTimefromCreated09(){
        return AvgRefTimefromCreated(date.valueOf(year+'-09-01'), date.valueOf(year+'-09-30'));
    }     
    
    //October
    public integer getAvgRefTimefromCreated10(){
        return AvgRefTimefromCreated(date.valueOf(year+'-10-01'), date.valueOf(year+'-10-31'));
    }     

    //November
    public integer getAvgRefTimefromCreated11(){
        return AvgRefTimefromCreated(date.valueOf(year+'-11-01'), date.valueOf(year+'-11-30'));
    } 

    //November
    public integer getAvgRefTimefromCreated12(){
        return AvgRefTimefromCreated(date.valueOf(year+'-12-01'), date.valueOf(year+'-12-31'));
    } 
        
    //Used to avg number of days from created date to referral
    integer AvgRefTimefromCreated(date s, date e){ //start, end
        
        if(s==null){
            s = date.valueof(string.valueof(date.today().year()) + '-01-01');
        }
        
        if(e==null){
            e = date.today();
        }
        
        integer totalDays =0;
        integer totalRecords = 0;
        // and status__c != 'Closed'
        for(Patient_Case__c p: [select Converted_Date__c,createdDate from Patient_Case__c where isconverted__c = true and createdDate >= :s and createdDate <= :e and converted_date__c !=null])
        {
            totalDays += date.valueOf(p.createdDate).daysBetween(p.converted_date__c);
            totalRecords++;
        }
        
        if(totalRecords!=0 && totalDays!=0){
            return totalDays/totalRecords;
        }
        
        return 0;
    }
    
    
}