public with sharing class directBillingWorkflow{

    public set<date> biWeeklyPayDates,semiMonthlyPayDates;
    
    public void setbiWeeklyPayDates(){
        
        if(biWeeklyPayDates!=null){
            return;
        }
        
        biWeeklyPayDates = new set<date>{};
        
        for(DB_Pay_Schedule__c dbPay : [select pay_end__c from DB_Pay_Schedule__c where recordType.Name='Bi-Weekly']){
            biWeeklyPayDates.add(dbPay.pay_end__c );
        }
        
    }    
    
    public void setsemiMonthlyPayDates(){
        
        if(semiMonthlyPayDates!=null){
            return;
        }
        
        semiMonthlyPayDates = new set<date>{};
        
        for(DB_Pay_Schedule__c dbPay : [select pay_end__c from DB_Pay_Schedule__c where recordType.Name='Semi-Monthly']){
            semiMonthlyPayDates.add(dbPay.pay_end__c );
        }
        
    } 
    
    public directBillingWorkflow(){
        
       setsemiMonthlyPayDates();
       setbiWeeklyPayDates();
        
    }
    
    public static Direct_Billing_Leave__c[] checkStartDate(map<id, Direct_Billing_Leave__c> leaves){
        
        Direct_Billing_Leave__c[] returnList = new Direct_Billing_Leave__c[]{};
        map<id, Direct_Billing_Leave__c[]> leaveMap = new map<id, Direct_Billing_Leave__c[]>{};
        map<id, date[]> datesMap = new map<id, date[]>{};
        
        for(id i : leaves.keySet()){
            leaveMap.put(i, new Direct_Billing_Leave__c[]{leaves.get(i)});
            
        }
        
        for(Direct_Billing_Leave__c dbs : [select name, start_date__c, end_date__c, DB_Member__c from Direct_Billing_Leave__c where DB_Member__c in :leaves.keySet()]){
            
            Direct_Billing_Leave__c[] leaveList = leaveMap.get(dbs.DB_Member__c);
            
            if(leaveList==null){
                leaveList = new Direct_Billing_Leave__c[]{};
            }
            
            if(leaves.get(dbs.db_member__c).id == dbs.id){
                leaveList.add(leaves.get(dbs.db_member__c));
            }else{
                leaveList.add(dbs);
            }
            leaveMap.put(dbs.DB_Member__c, leaveList);    
        }
        
        map<id, Date[]> dateMap = new map<id, Date[]>{};
        
        for(id i : leaveMap.keySet()){
            
            Direct_Billing_Leave__c[] leaveList = leaveMap.get(i);
            
            //if(leaveList.size()==1){
            //    returnList.add(leaveList[0]);
            //    continue;
            
            //}else{
                
                for(Direct_Billing_Leave__c dbl : leaveList){
                    
                    date[] fooDates = dateMap.get(dbl.id);
                    if(fooDates==null){
                        fooDates = new Date[]{null, null};
                    }
                    fooDates[0]= dbl.start_date__c;
                    fooDates[1]= dbl.end_date__c;
                    
                    dateMap.put(dbl.id, fooDates);
                    
                }
                
                for(Direct_Billing_Leave__c dbl : leaveList){
                    
                    date startDate = dbl.start_date__c;
                    date endDate = dbl.end_date__c;
                    
                    for(id x : dateMap.keySet()){
                        
                        if(x != dbl.id && x != null){
                            
                            if(dateMap.get(x)[1]==null){
                                returnList.add(dbl);
                                
                            }else if(startDate >= dateMap.get(x)[0] && startDate < dateMap.get(x)[1]){
                                returnList.add(dbl);
                            
                            }else if(endDate > dateMap.get(x)[0] && endDate <= dateMap.get(x)[1]){
                                returnList.add(dbl);
                                
                            }else if(endDate == dateMap.get(x)[1] && startDate == dateMap.get(x)[0]){
                                returnList.add(dbl);
                            }
                            
                        }
                        
                    }
                }
                    
                
            //}
            
        
        }
        
        return returnList;
    }
    

    
    Public static void pastDueLetters(map<id, date>  invoiceMap, date invoiceMonth ){
        
        
        Direct_Billing_Invoice_Pay_Date__c[] invoicePayDates = new Direct_Billing_Invoice_Pay_Date__c[]{};
        invoiceMonth = date.newInstance(invoiceMonth.year(),invoiceMonth.month(),1);
        set<id> inv = new set<id>();
        set<id> invPastDue = new set<id>();
        
        for(Direct_Billing_Invoice__c dbi : [select db_leave__r.DB_Member__r.Employee_Name__c,
                                                    db_leave__r.DB_Member__r.Address_1__c,
                                                    db_leave__r.DB_Member__r.Address_2__c,
                                                    Physical_Invoice_Created__c, 
                                                    pastDueLetterNeeded__c, 
                                                    db_leave__r.name from Direct_Billing_Invoice__c where Physical_Invoice_Created__c = null and  DB_Leave__c in :invoiceMap.keySet()]){
                                                    
            inv.add(dbi.id);
            
            if(dbi.pastDueLetterNeeded__c){
                invPastDue.add(dbi.id);
            }
            
        }
        
        //Find Leaves where the leave has ended, but still has a current balance
        //last_due_date__c is a roll-up summary field
        Direct_Billing_Leave__c[] pdLeaveEnded= [select Last_Invoiced_Amount__c, id,end_date__c,start_date__c, current_balance__c, Last_invoiced_Date__c from Direct_Billing_Leave__c where end_date__c <= :invoiceMonth and current_balance__c > 0.00 and last_due_date__c<:invoiceMonth and id not in :inv];
        map<id, Direct_Billing_Leave__c> pdLeaveInvoiceMap = new map<id, Direct_Billing_Leave__c>();
        
        for(Direct_Billing_Leave__c l : pdLeaveEnded){
            pdLeaveInvoiceMap.put(l.id, l);
        }
        
        directBillingInvoiceStruct pastDueInvoiceStruct = directBillingInvoiceWorkflow.pastDueInvoices(invoiceMonth, pdLeaveEnded);
        insert pastDueInvoiceStruct.invoices;
        update pastDueInvoiceStruct.leaves;
        
        
        set<id> newInvoiceSet = new set<id>();
        for(Direct_Billing_Invoice__c dbi : pastDueInvoiceStruct.invoices){
            
            Direct_Billing_Leave__c dbl = pdLeaveInvoiceMap.get(dbi.DB_Leave__c);
            
            Direct_Billing_Invoice_Pay_Date__c payDate = new Direct_Billing_Invoice_Pay_Date__c(DB_Invoice__c=dbi.id);
            
                  
            payDate.Identity_Theft_Premium__c= 0;
            payDate.Indemnity_Plan_Premium__c= 0;
            payDate.Legal_Plan_Premium__c= 0;
            payDate.Medical_Premium__c= 0;
            payDate.Dental_Premium__c = 0;
            payDate.Vision_Premium__c= 0;
            payDate.Personal_Accident_Amount_Per_Pay__c= 0;
            payDate.Spouse_Life_Premium__c = 0;
            payDate.Dependent_Life_Premium__c = 0;
            payDate.Critical_Illness_Amount_Per_Pay__c=0;
            payDate.FSA_Premium__c = 0;
            payDate.Long_Term_Disability_Premium__c =0;
            payDate.Life_and_AD_D_Premium__c = 0;
            payDate.pay_end__c=dbl.End_Date__c;
            payDate.pay_date__c=dbl.End_Date__c;
            
            invoicePayDates.add(payDate);
            invPastDue.add(dbi.id);
            if(!inv.contains(dbi.id)){
                newInvoiceSet.add(dbi.id);
            }
            
            
        }
        
        insert invoicePayDates;
        
        
        directBillingCreateAttachments dbca = new directBillingCreateAttachments();
        dbca.createInvoiceBatchCalls(newInvoiceSet);
        dbca.createPastDueLetterBatchCalls(invPastDue);
    }
    
//5-11-20 update
    Public static void terminationLetters(map<id, date>  invoiceMap, date invoiceMonth ){
        Direct_Billing_Invoice_Pay_Date__c[] invoicePayDates = new Direct_Billing_Invoice_Pay_Date__c[]{};
        invoiceMonth = date.newInstance(invoiceMonth.year(),invoiceMonth.month(),1);
        set<id> inv = new set<id>();
        set<id> invTermination = new set<id>();
        
        for(Direct_Billing_Invoice__c dbi : [select db_leave__r.DB_Member__r.Employee_Name__c,
                                                    db_leave__r.DB_Member__r.Address_1__c,
                                                    db_leave__r.DB_Member__r.Address_2__c,
                                                    Physical_Invoice_Created__c, 
                                                    terminationLetterNeeded__c, 
                                                    Days_since_Due__c,
                                                    //db_leave__r.name from Direct_Billing_Invoice__c where Physical_Invoice_Created__c = null and  DB_Leave__c in :invoiceMap.keySet()]){
                                                    db_leave__r.name from Direct_Billing_Invoice__c where DB_Leave__c in :invoiceMap.keySet()]){
                                                    
            inv.add(dbi.id);
            
            if(dbi.Days_since_Due__c>89){
                invTermination.add(dbi.id);
            }
            
        }
        
        //Find Leaves where the leave has ended, but still has a current balance
        //last_due_date__c is a roll-up summary field
        Direct_Billing_Leave__c[] pdLeaveEnded= [select Last_Invoiced_Amount__c, id,end_date__c,start_date__c, current_balance__c, Last_invoiced_Date__c from Direct_Billing_Leave__c where end_date__c <= :invoiceMonth and current_balance__c > 0.00 and last_due_date__c<:invoiceMonth and id not in :inv];
        map<id, Direct_Billing_Leave__c> pdLeaveInvoiceMap = new map<id, Direct_Billing_Leave__c>();
        
        for(Direct_Billing_Leave__c l : pdLeaveEnded){
            pdLeaveInvoiceMap.put(l.id, l);
        }
        
        directBillingTermInvoiceStruct termInvoiceStruct = directBillingInvoiceWorkflow.termInvoices(invoiceMonth, pdLeaveEnded);
        insert termInvoiceStruct.invoices;
        update termInvoiceStruct.leaves;
        set<id> newInvoiceSet = new set<id>();
        for(Direct_Billing_Invoice__c dbi : termInvoiceStruct.invoices){
            
            Direct_Billing_Leave__c dbl = pdLeaveInvoiceMap.get(dbi.DB_Leave__c);
            
            Direct_Billing_Invoice_Pay_Date__c payDate = new Direct_Billing_Invoice_Pay_Date__c(DB_Invoice__c=dbi.id);
            
            payDate.Identity_Theft_Premium__c= 0;
            payDate.Indemnity_Plan_Premium__c= 0;
            payDate.Legal_Plan_Premium__c= 0;
            payDate.Medical_Premium__c= 0;
            payDate.Dental_Premium__c = 0;
            payDate.Vision_Premium__c= 0;
            payDate.Personal_Accident_Amount_Per_Pay__c= 0;
            payDate.Spouse_Life_Premium__c = 0;
            payDate.Dependent_Life_Premium__c = 0;
            payDate.Critical_Illness_Amount_Per_Pay__c=0;
            payDate.FSA_Premium__c = 0;
            payDate.Long_Term_Disability_Premium__c =0;
            payDate.Life_and_AD_D_Premium__c = 0;
            
            payDate.pay_end__c=dbl.End_Date__c;
            payDate.pay_date__c=dbl.End_Date__c;
            
            invoicePayDates.add(payDate);
            invTermination.add(dbi.id);
            if(!inv.contains(dbi.id)){
                newInvoiceSet.add(dbi.id);
            }
        }
        
        insert invoicePayDates;
        
        
        directBillingCreateAttachments dbca = new directBillingCreateAttachments();
        dbca.createInvoiceBatchCalls(newInvoiceSet);//create for termination letter
        dbca.createTerminationLetterBatchCalls(invTermination);//create for termination letter
    }
    
}