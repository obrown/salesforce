public with sharing class ClaimsMatcherTransplant {
    formatID fid = new formatID();
    date min_transplant_eff_date = date.today();
    ClaimsReporting__c[] transplant_claims = new ClaimsReporting__c[] {};
    set<string> claims_newly_paid;
    map<string, Stage__c> transmap = new map<string, Stage__c>();

    public static void run(ClaimsReporting__c[] transplant_claims, set<string> claims_newly_paid, boolean updateRecords){
        ClaimsMatcherTransplant cmt = new ClaimsMatcherTransplant();

        cmt.transplant_claims = transplant_claims;
        cmt.claims_newly_paid = claims_newly_paid;
        cmt.setmin_transplant_eff_date(cmt.transplant_claims);
        cmt.set_transmap();
        cmt.associateTransplantClaims();

        if (updateRecords) {
            cmt.save_transplant_claims();
        }
    }

    void setmin_transplant_eff_date(ClaimsReporting__c[] transplant_claims){
        for (claimsReporting__c c : transplant_claims) {
            if (c.fdos__c < min_transplant_eff_date) {
                min_transplant_eff_date = c.fdos__c;
            }
        }
    }

    void save_transplant_claims(){
    }

    void associateTransplantClaims(){
        string numericCert = '';

        for (ClaimsReporting__c c: transplant_claims) {
            numericCert = fid.addzeros(fid.scrapeNumeric(c.certid__c), 9);  //Looking to return more claim matches
            if (transmap.containsKey(numericCert + c.Employee_Last_Name__c.tolowerCase())) {
                c.transplantID__c = transmap.get(numericCert + c.Employee_Last_Name__c.tolowerCase()).id;
            }
            else if (transmap.containsKey(numericCert + c.Patient_Last_Name__c.tolowerCase())) {
                c.transplantID__c = transmap.get(numericCert + c.Patient_Last_Name__c.tolowerCase()).id;
            }
        }
    }

    void set_transmap(){
        list<Stage__c> transList = new list<Stage__c>( [select Transplant__r.certid__c, Transplant__r.Relationship_to_the_Insured__c, Transplant__r.bid__c, Transplant__r.Employee_SSN__c, Transplant__r.createdDate, Transplant__r.maxTerm__c, Transplant__r.Employee_Last_Name__c, Transplant__r.Patient_Last_Name__c from Stage__c where stageeff__c > : min_transplant_eff_date]);

        for (Stage__c t : transList) {
            if (t.Transplant__r.certid__c != null) {
                transmap.put(fid.addzeros(fid.scrapeNumeric(t.Transplant__r.certid__c), 9) + t.Transplant__r.Patient_Last_Name__c.tolowerCase(), t);

                if (t.Transplant__r.Relationship_to_the_Insured__c != 'Patient is Insured') {
                    transmap.put(fid.addzeros(fid.scrapeNumeric(t.Transplant__r.certid__c), 9) + t.Transplant__r.Employee_Last_Name__c.tolowerCase(), t);
                }
            }
        }
    }
}