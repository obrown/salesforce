public with sharing class logSumType {

        string ResultCode;
        public eligibilityLogs[] EligibilityLogs {get;set;}        
        public boolean isError;
        
        public class EligibilityLogs{

            public string logRDATE  {get;set;}  //Date the file was processed
            public string frlogRDATE {get;set;} //friendly logr date
            string logEDATE {get;set;}     //Eff Date of the information in the file
            public string logEMPSREAD {get;set;}  //Number of Employee Demographic records read from the file
            public string logELGSREAD {get;set;}  //Number of Employee Eligibility records read from file
            string logDEPSREAD {get;set;}  //Number of Dependent Demographic records read from file
            public string logDLGSREAD {get;set;}  //Number of Dependent Eligibility records read from file
            public string logEMPSWRTN {get;set;}  //Number of Employee Demographic changes
            public string logELGSWRTN {get;set;}  //Number of Employee Eligibility changes 
            public string logDEPSWRTN {get;set;}  //Number of Dependent Demographic changes
            public string logDLGSWRTN {get;set;}  //Number of Dependent Eligibility changes
            public string logEMPSNEW {get;set;}  //Number of New Employees
            public string logDEPSNEW {get;set;}  //Number of New Dependents
            public string logTOTEXCS {get;set;}  //Total number of New eligibility exceptions (errors)
            public string LogClientname {get;set;} //Client Name
            public string LogUnderwriter {get;set;} //Client Number
            public string logSKPEXCS {get;set;}   //Nuumber of errors in new file that are still unresolved from previous files
            string logRECSREJ {get;set;}   //Number of records Rejected due to critical data errors
            string logPNDSRMVD {get;set;}  //Number of EEs/DEPs whose coverage was re-activated by a correction in this file
            public string logFILE {get;set;}       //Name of File loaded
            public string logLOADED {get;set;}     //Client Number
            string logEXCSRES {get;set;}   //Exceptions resolved in this run
            string logMODE {get;set;}      //(P)rod or (T)est mode run
            string logEXCSTATS {get;set;}  //Exceptions count
            string logFILL {get;set;}      //      
            boolean isError;
            
        }
        
        public void cleanUp_eLog(){
            if(eligibilityLogs!=null){
                for(eligibilityLogs e: this.EligibilityLogs){
                    if(e.logRDATE!=null){
                        e.logRDATE = e.logRDATE.trim();
                        e.frlogRDATE = e.logRDATE.mid(4,2) +'/'+ e.logRDATE.right(2) +'/'+e.logRDATE.left(4);
                    }
                    
                    if(e.LogClientname!=null){e.LogClientname = e.LogClientname.trim()+' ';}
                    
                    if(e.frlogRDATE !=null){e.frlogRDATE = e.frlogRDATE.trim();}
                    if(e.logTOTEXCS  !=null){e.logTOTEXCS  = e.logTOTEXCS.trim();}
                    if(e.logSKPEXCS !=null){e.logSKPEXCS = e.logSKPEXCS.trim();}
                    if(e.logFILE !=null){e.logFILE = e.logFILE.trim();}
                    
                 }
            }
        }
        
}