public without sharing class pocLogin{

    //set username/ password variables via page
    public boolean isLowes {get; private set;}
    public boolean isJetBlue {get; private set;}
    public string username {get;set;}
    public string password {get;set;}
    public string sessid {get; private set;}
    public string errorMessage {get; private set;} 
         
    public class jsonResponse {
    
        public string access_token;
        public string error;
        public string instance_url;
        public string id;
    }
    
    public pocLogin() {
        
        isLowes =false;
        isJetBlue =false;
        
        Apexpages.currentPage().getHeaders().put('X-UA-Compatible', 'IE=10'); 
        
        string userProfilename;
        userProfilename = ApexPages.currentPage().getParameters().get('client');
        if(userProfilename!=null){
        if(userProfileName.toLowerCase().contains('lowes')){
           isLowes = true;
        }else if(userProfileName.toLowerCase().contains('jetblue')){
           isJetBlue =true; 
        }
        }
        
    }
    public void doLogin(){
        
        string i;
        string reqBody;
        if(utilities.IsRunningInSandbox()){
            i='cs3';
            reqBody = 'grant_type=password&client_id=3MVG9pHRjzOBdkd.Q8MJIQySikqAu1FbYYQ.lFjFg8exiIhibBvoP8JARIZERwgFn5P44dBGxbw8ThnLfmAFv&client_secret=3538393939299842223&username=' + username + '&password=' + password;
        }else{
            i='na7';
            reqBody = 'grant_type=password&client_id=3MVG9yZ.WNe6byQC6e8IIKnjJODklAhl03TXccwtzaJTe4Szm_WR3jOJYkCfQSP_KlbV0us0om9PfcY9Mu48O&client_secret=6749489376166670084&username=' + username + '&password=' + password;
        }
        
        http h = new http();
        HttpRequest req = new HttpRequest ();
        req.setEndPoint('https://' + i + '.salesforce.com/services/oauth2/token');
        req.setBody(reqBody);
        req.setmethod('POST');
        
        string response;
        
        if(!test.isRunningTest()){
            HttpResponse res = h.send(req);
            response = res.getBody();
        
        }else{
            response = '{"access_token":"00DQ000000EeOC8!AR4AQOLGBgmP4snimQ0MlWLdaL6rCbOLHaLKCa5dM33sat_eQ14x43ot5w7wfda7uSFQHvNJQfEqT9BHba3cu3GWo5juNB6w","instance_url":"https://cs3.salesforce.com","id":"https://test.salesforce.com/id/00DQ000000EeOC8MAN/005A0000003M7cuIAC","token_type":"Bearer","issued_at":"1488991473932","signature":"Rm8me6UYbC8W0Vu5+ie2qkcmte/7bapfGj/C/FY9L6c="}';
        }
              
            jsonResponse jsonRes;
            jsonRes = (jsonResponse)JSON.deserialize(response, jsonResponse.class);
        
            if(jsonRes.access_token!='' && jsonRes.access_token!=null){
                sessid = 'https://' + i + '.salesforce.com/login.jsp?un='+username+'&pw='+password +'&startURL=/apex/facilityEligQueue';
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Logging On'));

            }else if(jsonRes.error!=''){
                errorMessage = jsonRes.error;
                ApexPages.addmessage(new ApexPages.Message(ApexPages.Severity.Error, errorMessage));
            }
         
        
    }
    
      
}