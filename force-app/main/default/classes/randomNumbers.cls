public without sharing class randomNumbers{

    
    public static set<Integer> randomWithLimit(Integer upperLimit, Integer numberOfRands){
        set<Integer>selected = new set<Integer>();        
        for (Integer i =0; selected.size()< numberOfRands; i++){
            Integer rand = math.round(Math.random()*upperLimit);
            selected.add(rand);
        }           
        return selected;
    }
    
}