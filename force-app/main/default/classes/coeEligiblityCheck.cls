public with sharing class coeEligiblityCheck{

    public string xml; //used for testing purposes

    public static coeEligibilityCheckStruct checkEligibility(patient_case__c obj){

        coeEligiblityCheck cec = new coeEligiblityCheck();
        return cec.checkForEligibility(obj);
    }

    public class coeEligibilityCheckStruct{

        public Eligibilty__c objE;
        public string message;
        public boolean error;
    }

    public coeEligibilityCheckStruct checkForEligibility(patient_case__c obj){
        coeEligibilityCheckStruct answer = new coeEligibilityCheckStruct();
        answer.error =false;
        ID queueGroupId;
                try{
                    queueGroupId= [Select Id from Group where type='Queue' and Name= 'HDP Eligibility'].Id;
                }catch (exception e){}

        AonHewittEligCheck aec = new AonHewittEligCheck();
        aec.searchID = obj.employee_ssn__c;
        aec.subjectType = 'PRSN';

        if(Test.isRunningTest()){
            aec.xml = xml;
        }

        aec.run();

        if(aec.response==null){
            answer.Message = 'The call to the eligibility service failed.';
            answer.error =true;
            obj.pendingEligCheck__c=true;
            obj.Date_Care_Mgmt_Transfers_to_Eligibility__c= date.today();
            obj.eligible__c='';
            update obj;

            answer.objE = new Eligibilty__c();
            answer.objE.Patient_case__c = obj.id;
            answer.objE.Date_Care_Mgmt_Transfered_to_Eligibility__c = date.today();
            if(queueGroupId != null){
                    answer.objE.ownerID = queueGroupId;
            }
            insert answer.objE;
            return answer;
        }

        parseAhEligibilityResponse pah = new parseAhEligibilityResponse(aec.response);
        pah.parse();

        string Message;

        if(pah.responseDescription !='SUCCESS'){
            answer.Message = 'The employee could not be found. Please verify the employee SSN and Sales Id. If these fields are incorrect, please re-enter them and submit for eligibility again.';
            answer.error =true;
            obj.pendingEligCheck__c=true;
            obj.Date_Care_Mgmt_Transfers_to_Eligibility__c= date.today();
            obj.eligible__c='';
            update obj;

            answer.objE = new Eligibilty__c();
            answer.objE.Patient_case__c = obj.id;
            answer.objE.Date_Care_Mgmt_Transfered_to_Eligibility__c = date.today();
            if(queueGroupId != null){
                    answer.objE.ownerID = queueGroupId;
            }
            insert answer.objE;
            return answer;

        }else{

            if(pah.getemployee() == null){

                //Message = pah.responseDescription;
                answer.Message = 'An error has occurred. The case will be submitted for a manual eligibilty check';
                //errorInEligibiltyCall(message, obj);
                return answer;
            }else{

                //Successful call
                answer.objE = new Eligibilty__c();
                answer.objE.Patient_case__c = obj.id;
                answer.objE.Date_Care_Mgmt_Transfered_to_Eligibility__c = date.today();
                answer.objE.Date_Eligibility_Checked_with_Client__c = date.today();
                answer.objE.Medical_Coverage_Status__c = pah.getemployee().employmentStatusCode;
                answer.objE.Plan__c = pah.getCoverage().optionLongDescriptionText +' - '+pah.getCoverage().planLongDescriptionText;
                obj.Date_Eligibility_is_Checked_with_Client__c = date.today();
                obj.Date_Care_Mgmt_Transfers_to_Eligibility__c= date.today();

                if(queueGroupId != null){
                    answer.objE.ownerID = queueGroupId;
                }

                if(pah.getCoverage().optionLongDescriptionText == 'No Coverage' || pah.getCoverage().optionLongDescriptionText == null){

                                obj.eligible__c = 'No';
                                obj.eNotes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Medical benifits returned as no coverage.';
                                answer.obje.eligible__c = 'No';
                                answer.obje.Notes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Medical benifits returned as no coverage.';
                                answer.message = 'ok';
                                return answer;
                }


                //employee dob could not be verified
                if(obj.employee_dobe__c != pah.getemployee().birthDate){
                    obj.eligible__c = 'No';
                    obj.eNotes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Employee DOB could not be verified, returned as ' + pah.getemployee().birthDate;
                    answer.obje.eligible__c = 'No';
                    answer.obje.Notes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Employee DOB could not be verified, returned as ' + pah.getemployee().birthDate;
                    answer.message = 'ok';
                    return answer;

                }

                //normalize returned salesId and verify it
                formatID nf = new formatID();
                string formattedEmployeeId = nf.formatID(pah.getemployee().employeeId, obj.Client_Name__c);
                if(formattedEmployeeId ==obj.certId__c){
                    obj.BID_verified__c = true;

                }else{
                   obj.BID_verified__c = false;
                   obj.eligible__c = 'No';
                   obj.eNotes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Employee SalesID could not be verified, returned as ' + formattedEmployeeId;
                   answer.obje.eligible__c = 'No';
                   answer.obje.Notes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Employee SalesID could not be verified, returned as ' + formattedEmployeeId;
                   answer.message = 'ok';
                   return answer;
                }

                 //Employee is the patient?
                 if(obj.employee_ssn__c == obj.patient_ssn__c){
                     answer.objE = eligibilityYes(obj, answer.objE, pah);
                     answer.message = 'ok';
                     return answer;

                 }else{
                      //Employee not the patient

                      boolean hasPatientAsDep=false;
                       for(parseAhEligibilityResponse.dependent dep: pah.getDependents()){
                                if(dep.firstName!=null){
                                    if((obj.patient_first_name__c.touppercase().contains(dep.firstName.touppercase()))){
                                        hasPatientAsDep=true;
                                        break;
                                    }
                                }
                       }

                       if(hasPatientAsDep){
                           answer.objE = eligibilityYes(obj, answer.objE, pah);
                           answer.message = 'ok';
                           return answer;
                       }else{
                           obj.eligible__c = 'No';
                           obj.eNotes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined because the patient was not matched as a dependent. Dependents listed as ' + pah.getDependents() ;
                           answer.obje.eligible__c = 'No';
                           answer.obje.Notes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined because the patient was not matched as a dependent. Dependents listed as ' + pah.getDependents();
                           obj.eNotes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined because the patient was not matched as a dependent. Dependents listed as ' + pah.getDependents();
                           answer.message = 'ok';
                           return answer;
                       }

                 }
            }

        }

    }
    /*
    void errorInEligibiltyCall(string errorMsg, patient_case__c obj){
        obj.Date_Eligibility_is_Checked_with_Client__c = null;
        obj.Patient_and_Employee_DOB_verified__c = false;
        obj.Patient_and_Employee_SSN_verified__c = false;
        obj.Carrier_and_Plan_Type_verified__c = false;
        obj.BID_verified__c = false;
        obj.eNotes__c = errorMsg;
        convertCase.addEligibity(obj);

    }
    */
    string planName(string theName){

        if(theName.contains('Option 2')){
            return 'Option 2 PPO';

        }else if(theName.contains('Option 1')){
            return 'Option 1 PPO';

        }else if(theName.contains('Choice Account Plus')){
            return 'Choice Account Plus (HSA)';

        }else if(theName.contains('Choice Account')){
            return 'Choice Account (HSA)';
        }

        return theName;
    }

    Eligibilty__c eligibilityYes(patient_case__c obj, Eligibilty__c objE, parseAhEligibilityResponse pah){
        obj.Employee_Primary_Health_Plan_Name__c = planName(pah.getCoverage().optionLongDescriptionText);
        string theNote = 'Eligibility checked via automated process\n\n';
        theNote = theNote + 'Plan name ' + pah.getCoverage().planLongDescriptionText + ' - ' + pah.getCoverage().optionLongDescriptionText+'.';

        if(pah.getEmployee().employmentStatusCode == 'Term'){
            obj.eligible__c = 'Cobra';
            obje.eligible__c = 'Cobra';
            obj.Date_Eligibility_is_Checked_with_Client__c =null;
            obje.Date_Eligibility_Checked_with_Client__c =null;
            theNote += ' \n\nThe patient is on Cobra. The paid thru date needs to be determined. Once this have been done the eligibility check will be complete.';
        }else{
            obj.eligible__c = 'Yes';
            obje.eligible__c = 'Yes';
        }

        obj.eNotes__c = theNote;
        obje.Notes__c = theNote;
        return objE;


    }

    //1-13-22 update -------------------------------------------start---------------------------------------------

    public static coeOncologyEligibilityCheckStruct checkEligibility(Oncology__c o){
        coeEligiblityCheck cec = new coeEligiblityCheck();
        return cec.checkForEligibility(o);
    }

    public class coeOncologyEligibilityCheckStruct{

        public Eligibilty__c objE;
        public string message;
        public boolean error;
    }


    public coeOncologyEligibilityCheckStruct checkForEligibility(Oncology__c o){
        coeOncologyEligibilityCheckStruct answer = new coeOncologyEligibilityCheckStruct();
        answer.error =false;
        ID queueGroupId;
                try{
                    queueGroupId= [Select Id from Group where type='Queue' and Name= 'HDP Eligibility'].Id;
                }catch (exception e){}

        AonHewittEligCheck aec = new AonHewittEligCheck();
        aec.searchID = o.Employee_SSN__c;
        aec.subjectType = 'PRSN';

        if(Test.isRunningTest()){
            aec.xml = xml;
        }

        aec.run();

        if(aec.response==null){
            answer.Message = 'The call to the eligibility service failed.';
            answer.error =true;
            o.pendingEligCheck__c=true;//new field added 1-13-22
            o.Eligibility_Verification__c='No';//new field added 1-13-22
            update o;

            answer.objE = new Eligibilty__c();
            answer.objE.Oncology__c = o.id;//new field added 1-13-22
            answer.objE.Date_Care_Mgmt_Transfered_to_Eligibility__c = date.today();
            if(queueGroupId != null){
                    answer.objE.ownerID = queueGroupId;
            }
            insert answer.objE;
            return answer;
        }

        parseAhEligibilityResponse pah = new parseAhEligibilityResponse(aec.response);
        pah.parse();
        //string request =pah.x270.build270();

        string Message;

        if(pah.responseDescription !='SUCCESS'){
            answer.Message = 'The employee could not be found. Please verify the employee SSN and Sales Id. If these fields are incorrect, please re-enter them and submit for eligibility again.';
            answer.error =true;
            o.pendingEligCheck__c=true;//new field added 1-13-22
            o.Eligibility_Verification__c='No';
            update o;//updated 1-13-22

            answer.objE = new Eligibilty__c();
            answer.objE.Oncology__c = o.id;//new field added 1-13-22
            answer.objE.Date_Care_Mgmt_Transfered_to_Eligibility__c = date.today();
            if(queueGroupId != null){
                    answer.objE.ownerID = queueGroupId;
            }
            insert answer.objE;
            return answer;

        }else{

            if(pah.getemployee() == null){

                //Message = pah.responseDescription;
                answer.Message = 'An error has occurred. The case will be submitted for a manual eligibilty check';
                //errorInEligibiltyCall(message, o);
                return answer;
            }else{

                //Successful call
                answer.objE = new Eligibilty__c();
                answer.objE.Oncology__c = o.id;//new field added 1-13-22
                answer.objE.Date_Care_Mgmt_Transfered_to_Eligibility__c = date.today();
                answer.objE.Date_Eligibility_Checked_with_Client__c= date.today();
                answer.objE.Medical_Coverage_Status__c = pah.getemployee().employmentStatusCode;
                answer.objE.Plan__c = pah.getCoverage().optionLongDescriptionText +' - '+pah.getCoverage().planLongDescriptionText;
                o.Date_Eligibility_Verified__c = date.today();
                //1-14-22 added -------------start--------------------

                //1-14-22 added -------------end--------------------
                if(queueGroupId != null){
                    answer.objE.ownerID = queueGroupId;
                }

                if(pah.getCoverage().optionLongDescriptionText == 'No Coverage' || pah.getCoverage().optionLongDescriptionText == null){

                                o.Eligibility_Verification__c = 'No';
                                o.eNotes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Medical benifits returned as no coverage.';//new field added Notes
                                answer.obje.eligible__c = 'No';
                                answer.obje.Notes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Medical benifits returned as no coverage.';
                                answer.message = 'ok';
                                return answer;
                }

                /*not needed for oncology
                //employee dob could not be verified
                if(o.Employee_DOBe__c != pah.getemployee().birthDate){//new field added Employee_DOBe TODO******************populate this field in all orgs
                    o.Eligibility_Verification__c = 'No';//updated 1-13-22
                    o.eNotes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Employee DOB could not be verified, returned as ' + pah.getemployee().birthDate;//new field added Notes
                    answer.obje.eligible__c = 'No';
                    answer.obje.Notes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Employee DOB could not be verified, returned as ' + pah.getemployee().birthDate;
                    answer.message = 'ok';
                    return answer;

                }


                //normalize returned salesId and verify it
                formatID nf = new formatID();
                string formattedEmployeeId = nf.formatID(pah.getemployee().employeeId, obj.Client_Name__c);//TODO find out how this is used and Re-Factor for Oncology************************
                if(formattedEmployeeId ==obj.certId__c){
                    obj.BID_verified__c = true;

                }else{
                   obj.BID_verified__c = false;
                   obj.eligible__c = 'No';
                   obj.eNotes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Employee SalesID could not be verified, returned as ' + formattedEmployeeId;
                   answer.obje.eligible__c = 'No';
                   answer.obje.Notes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined. Employee SalesID could not be verified, returned as ' + formattedEmployeeId;
                   answer.message = 'ok';
                   return answer;
                }
                 */

                 //Employee is the patient?
                 if(o.Employee_SSN__c == o.Patient_SSN__c){
                     answer.objE = eligibilityYes(o, answer.objE, pah);
                     answer.message = 'ok';
                     return answer;

                 }else{
                      //Employee not the patient

                      boolean hasPatientAsDep=false;
                       for(parseAhEligibilityResponse.dependent dep: pah.getDependents()){
                                if(dep.firstName!=null){
                                    if((o.patient_first_name__c.touppercase().contains(dep.firstName.touppercase()))){
                                        hasPatientAsDep=true;
                                        break;
                                    }
                                }
                       }

                       if(hasPatientAsDep){
                           answer.objE = eligibilityYes(o, answer.objE, pah);
                           answer.message = 'ok';
                           return answer;
                       }else{
                           o.Eligibility_Verification__c = 'No';
                           o.eNotes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined because the patient was not matched as a dependent. Dependents listed as ' + pah.getDependents();
                           answer.obje.eligible__c = 'No';
                           answer.obje.Notes__c = 'Eligibility checked via automated process\n\nEligbility could not be determined because the patient was not matched as a dependent. Dependents listed as ' + pah.getDependents();
                           answer.message = 'ok';
                           return answer;
                       }

                 }
            }

        }

    }

    Eligibilty__c eligibilityYes(Oncology__c o, Eligibilty__c objE, parseAhEligibilityResponse pah){

    //system.debug('');
        //o.Employee_HealthPlan__c = planName(pah.getCoverage().optionLongDescriptionText); removed will add back if needed
        string theNote = 'Eligibility checked via automated process\n\n';
        theNote = theNote + 'Plan name ' + pah.getCoverage().planLongDescriptionText + ' - ' + pah.getCoverage().optionLongDescriptionText+'.';

        if(pah.getEmployee().employmentStatusCode == 'Term'){
            o.Eligibility_Verification__c = 'Cobra';
            obje.eligible__c = 'Cobra';
            obje.Date_Eligibility_Checked_with_Client__c =null;
            theNote += ' \n\nThe patient is on Cobra. The paid thru date needs to be determined. Once this have been done the eligibility check will be complete.';
        }else{
            o.Eligibility_Verification__c = 'Yes';
            obje.eligible__c = 'Yes';
        }

        o.eNotes__c = theNote;
        obje.Notes__c = theNote;
        return objE;


    }

    //1-13-22 update -------------------------------------------end-----------------------------------------------

}
