public with sharing class bariatricReferralClosedLetterExt{
    public coeCaseEncryptedData cc {get; set;}
    public bariatric__c bariatric {get; set;}
     
    public bariatricReferralClosedLetterExt(ApexPages.StandardController controller) {
         string cases = ApexPages.CurrentPage().getParameters().get('cases');
         string bjson = ApexPages.CurrentPage().getParameters().get('bjson');
         
         ApexPages.CurrentPage().getParameters().put('cases',  null);
         if(cases==null){
             cc = new coeCaseEncryptedData();
             return;
         }
         cc = (coeCaseEncryptedData)JSON.deserialize(cases, coeCaseEncryptedData.class);
         if(bjson !=null){
             bariatric = (bariatric__c)JSON.deserialize(bjson, bariatric__c.class);
         }
    }

}