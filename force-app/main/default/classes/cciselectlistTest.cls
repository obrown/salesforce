@isTest
private class cciselectlistTest {

    static testMethod void cciselectlistTest() {
        
        /* begin setup */
        
        cciPicklistSetupController cpsc = new cciPicklistSetupController();
        cpsc.setInquiryReasonToUpdate();
        
        cpsc.workTaskName= 'Unit Test Work Task';
        cpsc.saveWorkTask();
        cpsc.workTaskId = cpsc.isl.workTask[1].getValue();
        system.assert(cpsc.workTaskId!=null);
        cpsc.isl.workTaskValue =cpsc.workTaskId;
        string workTaskId = cpsc.workTaskId;
        
        ApexPages.CurrentPage().getParameters().put('workTaskId',cpsc.workTaskId);
        cpsc.InquiryReasonName= 'Unit Test Inquiry Reason';
        cpsc.saveInquiryReason();
        cpsc.InquiryReasonId = cpsc.isl.InquiryReason[1].getValue();
        system.assert(cpsc.InquiryReasonId !=null);
        cpsc.isl.inquiryReasonValue=cpsc.inquiryReasonId ;
        string inquiryReasonId = cpsc.inquiryReasonId;
        
        ApexPages.CurrentPage().getParameters().put('inquiryReasonId',cpsc.inquiryReasonId);
        cpsc.workDepartmentName= 'Unit Test Work Department';
        cpsc.saveWorkDepartment();
        cpsc.workDepartmentId = cpsc.isl.workDepartment[1].getValue();
        system.assert(cpsc.workDepartmentId !=null);
        cpsc.isl.workDepartmentValue=cpsc.workDepartmentId;
        string workDepartmentId = cpsc.workDepartmentId;
        
        cpsc.closedReasonName= 'Unit Test Closed Reason';
        cpsc.saveClosedReason();
        cpsc.closedReasonId = cpsc.isl.closedReason[1].getValue();
        system.assert(cpsc.closedReasonId!=''); 
        string closedReasonId= cpsc.closedReasonId;
        system.debug('closedReasonId '+closedReasonId);
        /* End Setup */
        
        cciSelectlist isl = new cciSelectlist(InquiryReasonId , workDepartmentId, workTaskId);
        
        //isl.getTheworkTask();
        isl.getinquiryReasonName(inquiryReasonId);
       // isl.getWorkDepartmentName(workDepartmentId);
        isl.getClosedReasonName(closedReasonId);
         
        ApexPages.CurrentPage().getParameters().put('workTaskId', workTaskId); 
        isl.setWorkTask();
        
        ApexPages.CurrentPage().getParameters().put('inquiryReasonId', inquiryReasonId); 
        isl.setInquiryReason();  
        
        ApexPages.CurrentPage().getParameters().put('workDepartmentId', workDepartmentId); 
        isl.setWorkDepartment();   
        
        isl.reset();
        isl.clearClosedReason();
        isl.clearInquiryReason();
        
    }
    
}