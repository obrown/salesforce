public with sharing class caseManagamentLetterPDFext{
 
    public Case_Management_Letter__c[] lettersList {get; private set;}
    public Case_Management__c cm {get; private set;}
    
    set<string> lids = new set<string>();
    set<string> cmids = new set<string>();
    
    public caseManagamentLetterPDFext() {
        
        
        lettersList = new Case_Management_Letter__c[]{};
        string lid=Apexpages.currentpage().getParameters().get('lids');
        if(lid!=null){
            string[] lidsFoo = lid.split(':');
            
            lids.addAll(lidsFoo);
            for(Case_Management_Letter__c cmm : [select letterText__c,Case_Management__c from Case_Management_Letter__c  where id in :lids]){                
                cmids.add(cmm.Case_Management__c);
                lettersList.add(cmm);
            }
           
        }
    }

}