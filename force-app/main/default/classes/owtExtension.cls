public class owtExtension {
    string fileName;

    public string getfileName(){
        return fileName;
    }

    public void setfileName(string fileName){
        this.fileName = fileName;
    }

    integer total_count;

    id topLevelId;
    public Operations_Work_Task__c [] owtList {
        get; set;
    }                                                     //related tasks
    public Operations_Work_Task__c [] owtListDT {
        get; set;
    }                                                       //parent tasks
    public Electronic_Work_Task_Note__c note {
        get; set;
    }
    public Electronic_Work_Task_Note__c[] notes {
        get; set;
    }
    Public Operations_Work_Task__c theCase {
        get; set;
    }
    Public Operations_Work_Task__c pTask {
        get; set;
    }
    Public string owtid {
        get; set;
    }
    public string drive {
        get; set;
    }
    public boolean reloadpage {
        get; set;
    }
    public boolean hpSet {
        get; private set;
    }
    public OwtUnassignedFiles uFiles {
        get; set;
    }
    Boolean PBV;
    public boolean unassignedfileloaded {
        get; private set;
    }
    public boolean error {
        get; private set;
    }
    public integer loadhpsearch {
        get; private set;
    }
    public Work_Task_Attachment__c[] attachments {
        get; private set;
    }
    public Work_Task_Attachment__c[] parentAttachments {
        get; private set;
    }

    public Work_Task_Attachment__c helperAttachment {
        get; private set;
    }
    public String helperAttachmentLink {
        get; private set;
    }
    public ewtSelectlist esl {
        get; set;
    }
    public string documentTypeName {
        get; private set;
    }
    ApexPages.StandardController controller;
    public List<SelectOption> User {
        get; set;
    }
    EWT_Document_Type__c dt {
        get; set;
    }
    public List<SelectOption> client {
        get; set;
    }                                           //added 6-21-21
    public Double offset {
        get {
            TimeZone tz = UserInfo.getTimeZone();
              //Milliseconds to Day
            return tz.getOffset(DateTime.now()) / (1000 * 3600 * 24.0);
        }
    }

    final boolean delowt = Operations_Work_Task__c.sObjectType.getDescribe().isDeletable();

    public owtExtension(ApexPages.StandardController controller) {
        this.controller = controller;
        theCase = (Operations_Work_Task__c)controller.getRecord();
        if (theCase.id == null) {
            reloadPage = true;
            theCase.OwnerID = userinfo.getuserid();
        }
        init(theCase.id, ApexPages.Currentpage().getParameters().get('owtid'), ApexPages.Currentpage().getParameters().get('odept'), ApexPages.Currentpage().getParameters().get('dtask') == 'true');
    }

    public void setdocumentTypeVF(){
        try{
            esl.setdocumentType();
            loadunassignedfiles();
        }catch (exception e) {
            system.debug(e.getMessage() + ' ' + e.getLineNumber());

            esl.setdocumentType();
        }
    }

    public void loadunassignedfiles(){
        if (esl.documentTypeRecord.Folder_Location__c != null) {
            OwtUnassignedFiles.response response = ufiles.setFileList(esl.documentTypeRecord.Folder_Location__c);
            if (response.resultCode == '2') {
                error = true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, response.ErrorMsg));
            }
        }
        unassignedfileloaded = true;
    }

    void init(string owtid, string pid, string odept, boolean dtask){
        reloadpage = false;
        uFiles = new OwtUnassignedFiles();
        unassignedfileloaded = false;

          /* O.Brown code  */
        if (theCase.id == null) {
            reloadpage = true;
            theCase.Work_Task_Request_date__c = date.today();
            theCase.Due_Date__c = date.today().addDays(3);

            theCase.Receive_Date__c = date.today();
            string deptask = ApexPages.currentPage().getParameters().get('dtask');

            if (ApexPages.currentPage().getParameters().get('dtask') == 'true') { // Does it matter if the param value of null?
                thecase.Dependent_Task__c = true;//added 7-7-21
                try{
                    theCase.ewt_document_type__c = [select id from EWT_Document_Type__c where Name = 'No Document' limit 1].id;
                }catch (queryException e) {
                    system.debug(e.getMessage());
                }
                theCase.Document_Type__c = 'No Document';
            }
            else if (ApexPages.currentPage().getParameters().get('dtask') == 'false') {
                thecase.Dependent_Task__c = false;//added 7-7-21
            }
            //    hpSet=false;
        }
        else{
            loadNotes();
        }

        if (theCase.id == null) {
            reloadpage = true;

            theCase.Receive_Date__c = date.today();
            theCase.Work_Task_Request_date__c = date.today();
            theCase.Due_Date__c = date.today().addDays(3);
        }
        else{
            loadNotes();
        }

        this.owtid = owtid;
        if (pid != null) {
            theCase.OWT_Parent__c = pid;
        }
        else{
            theCase.OrigOwtParent__c = owtid;
        }
        if (odept != null) {
            theCase.Originating_Department__c = odept;
        }

          //6-24-21 update ******************************start**********************************
        if (theCase.Client_ID__c != null) {
            clientId = thecase.Client_ID__c;
        }

        if (theCase.Procedure_ID__c != null) {
            procedureId = thecase.Procedure_ID__c;
        }
        //6-24-21 update ******************************end**********************************

        loadowt();
        loadowtDT();
        loadUserMap();
        loadClients();//added6-22-21

        if (theCase.Owt_parent__c == null) {
            pTask = theCase;
            esl = new ewtSelectlist(false, theCase.EWT_Document_Type__c, theCase.EWT_Work_Task__c, theCase.EWT_Work_Task_Reason__c, theCase.EWT_Work_Department__c, theCase.EWT_Closed_Reason__c, theCase.EWT_Work_Task_Status__c);
            topLevelId = owtid;
        }
        else{
            pTask = [select Member_Number__c,
                     name,
                     Work_Task__c,
                     Document_Type__c,
                     underwriter__c,
                     EESSN__c,
                     patient_dob__c,
                     Patient_Fname__c,
                     Patient_Lname__c,
                     Group__c,
                     Related_Claim_Number__c,
                     Sequence__c,
                     HP_Document_Type__c,
                     Document_description__c,
                     Work_Task_Reason__c,
                     Owt_parent__c,
                     OWT_Parent__r.OrigOwtParent__c,
                     OrigOwtParent__c,
                     Relationship__c from Operations_Work_Task__c where id = :theCase.owt_parent__c];

            theCase.Owt_parent__r = pTask;
            theCase.Owt_parent__r.Work_Task__c = pTask.Work_Task__c;
            theCase.Owt_parent__r.Document_Type__c = pTask.Document_Type__c;
            theCase.Owt_parent__r.Document_description__c = pTask.Document_description__c;
            theCase.EESSN__c = pTask.EESSN__c;
            theCase.Group__c = pTask.Group__c;
            theCase.Sequence__c = pTask.Sequence__c;
            theCase.Patient_Fname__c = pTask.Patient_Fname__c;
            theCase.Patient_Lname__c = pTask.Patient_Lname__c;
            theCase.Patient_Dob__c = pTask.Patient_Dob__c;
            theCase.Relationship__c = pTask.Relationship__c;
            theCase.Underwriter__c = pTask.Underwriter__c;
            theCase.Member_Number__c = pTask.Member_Number__c;
            theCase.Related_Claim_Number__c = pTask.Related_Claim_Number__c;
            theCase.OrigOwtParent__c = pTask.OrigOwtParent__c;

            esl = new ewtSelectlist(false, theCase.EWT_Document_Type__c, theCase.EWT_Work_Task__c, theCase.EWT_Work_Task_Reason__c, theCase.EWT_Work_Department__c, theCase.EWT_Closed_Reason__c, theCase.EWT_Work_Task_Status__c);

            string hpset = ApexPages.CurrentPage().getParameters().get('hpset');

            if (pTask.Owt_parent__c == null) {
                topLevelId = pTask.id;
            }
            else{
                id fooId = pTask.Owt_parent__c;
                integer overFlow = 0;
                while (topLevelId == null && overFlow < 9) {
                    overFlow++;
                    Operations_Work_Task__c foo = [select Owt_parent__c from Operations_Work_Task__c where id = :fooId];
                    if (foo.Owt_parent__c == null) {
                        topLevelId = fooId;
                    }
                    else{
                        fooId = foo.Owt_parent__c;
                    }
                }
            }
        }

        /* Updated logic to get the document type
         * if(ApexPages.currentPage().getParameters().get('dtask')!=null){
         * theCase.Dependent_Task__c=dtask;
         * if(theCase.id==null){
         *     for(selectOption so : esl.documentType){
         *         if(so.getLabel()=='No Document'){
         *             theCase.EWT_Document_Type__c = so.getValue();
         *             esl.documentTypeValue=theCase.EWT_Document_Type__c;
         *             esl.getTheDocumentType();
         *             theCase.Document_Type__c ='No Document';
         *             break;
         *         }
         *     }
         *
         * }
         * }
         */
        loadattachments();
    }

    void loadowt(){
        owtList = new Operations_Work_Task__c [] {};
        if (owtid == null) {
            return;
        }

        integer count = 0;

        for (Operations_Work_Task__c c : [select Name,
                                          Work_Task__c,
                                          Work_department__c,
                                          Work_Task_Status__c, Closed_Date__c, Priority__c, Reason_for_Work_task__c, OWT_Parent__c, OWT_Parent__r.name, createdDate, Dependent_Task__c, Dependent_Task_Completed__c, DepTaskTotalTOC__c, Work_Task_TOC__c, PtaskTOC__c, HP_Document_Type__c, Work_Task_Reason__c, First_Name__c, Last_Name__c, Client__c, COE_Program__c, Related_Claim_Number__c, OWT_Parent__r.OrigOwtParent__c, OrigOwtParent__c from Operations_Work_Task__c where OWT_Parent__c = :owtid and Dependent_Task__c != true]) {
            count++;
            owtList.add(c);
        }
    }

    void loadowtDT(){
        owtListDT = new Operations_Work_Task__c [] {};
        if (owtid == null) {
            return;
        }

        integer count = 0;

        for (Operations_Work_Task__c c : [select Name,
                                          Work_Task__c,
                                          Work_department__c,
                                          Work_Task_Status__c, Closed_Date__c, Priority__c, Reason_for_Work_task__c, OWT_Parent__r.name, createdDate, Dependent_Task_Completed__c, DepTaskTotalTOC__c, Work_Task_TOC__c, PtaskTOC__c, Work_Task_Reason__c, First_Name__c, Last_Name__c, Client__c, COE_Program__c, Related_Claim_Number__c, OWT_Parent__r.Related_Claim_Number__c, OWT_Parent__r.OrigOwtParent__c, OrigOwtParent__c from Operations_Work_Task__c where OWT_Parent__c = :owtid and Dependent_Task__c = true]) {
            count++;
            owtListDT.add(c);
        }
    }

    public void loadattachments(){
        helperAttachment = new Work_Task_Attachment__c();
        attachments = [select SalesforceAttachmentLocation__c, name, createdDate, createdById from Work_Task_Attachment__c where Operations_Work_Task__c = :theCase.id order by createddate desc];
        if (theCase.Owt_parent__c != null) {
            parentAttachments = [select SalesforceAttachmentLocation__c, name, createdDate, createdById from Work_Task_Attachment__c where Operations_Work_Task__c != null and Operations_Work_Task__c = :topLevelId order by createddate desc];
        }
        //parentAttachments = [select SalesforceAttachmentLocation__c,name,createdDate, createdById from Work_Task_Attachment__c where Operations_Work_Task__c = :theCase.OWT_Parent__c or Operations_Work_Task__c =:theCase.OrigOwtParent__c  order by createddate desc];

        if (!attachments.isEmpty() && theCase.EESSN__c == null) {
            helperAttachment = attachments [0];
            helperAttachmentLink = ufiles.fileserverurl + 'openfile?filepath=' + attachments [0].SalesforceAttachmentLocation__c + '&oid=' + userinfo.getOrganizationId() + '&sid=' + ufiles.sid + '&uid=' + ufiles.userId;
        }
    }

    public void myDelete(){
        try{
            delete theCase;
            loadowtDT();
            loadowt();
        }catch (dmlexception e) {
        }
    }

    public void deleteTheOWT(){
        if (delowt) {
            string owtID = ApexPages.currentPage().getparameters().get('owtiID');
            delete(new Operations_Work_Task__c(id = owtId));
        }
    }

    public boolean getDelOWT(){
        return delowt;
    }

    public void updateMyAccounts(){
        theCase.assigned__c = UserInfo.getName();
        upsert theCase;
    }

    public void Save(){
        error = false;

          //6-24-21 update ***************************************start**************************
        if (clientId != null) {
            system.debug('clientId =' + clientId);
            thecase.Client_ID__c = clientId;
            system.debug('************** clientId: ' + clientId);
            for (selectoption so : clients) {
                string id_check = so.getValue();
                if (id_check != '' && id_check == clientId) {
                    thecase.client__c = so.getLabel();
                    break;
                }
            }
        }
        if (procedureId != null) {
            thecase.Procedure_ID__c = procedureId;
            for (selectoption so : getProcedures()) {
                string id_check = so.getValue();
                if (id_check != '' && id_check == procedureId) {
                    thecase.COE_Program__c = so.getLabel();
                    system.debug('********************* Procedure: ' + thecase.COE_Program__c);
                    break;
                }
            }
        }

        //6-24-21 update ***************************************end**************************

        theCase.ewt_document_type__c = esl.documentTypeValue;
        try{
            theCase.Document_Type__c = esl.getDocumentName(esl.documentTypeValue).Document_Type__c;
            theCase.Document_Description__c = esl.getDocumentName(esl.documentTypeValue).Description__c;
            theCase.HP_Document_Type__c = esl.getDocumentName(esl.documentTypeValue).HP_Document_Type__c;
        }catch (exception e) {}

        theCase.EWT_Work_Task__c = esl.workTaskValue;
        theCase.Work_Task__c = esl.getWorkTaskName(esl.workTaskValue);

        theCase.EWT_Work_Task_Reason__c = esl.worktaskReasonValue;
        theCase.Work_Task_Reason__c = esl.getWorkTaskReasonName(esl.worktaskReasonValue);

        theCase.EWT_Work_Department__c = esl.workDepartmentValue;
        theCase.Work_Department__c = esl.getWorkDepartmentName(esl.workDepartmentValue);

        theCase.EWT_Work_Task_Status__c = esl.workTaskStatusValue;
        theCase.Work_Task_Status__c = esl.getworkTaskStatusName(esl.workTaskStatusValue);
        if (theCase.EWT_Work_Task_Status__c == null) {
            theCase.EWT_Work_Task_Status__c = esl.workTaskStatusValueMap.get('Open');
            esl.workTaskStatusValue = theCase.EWT_Work_Task_Status__c;
        }

        try{
            if (theCase.id == null) {
                reloadPage = true;
            }
              //upsert thecase;
            controller.save();
            theCase = (Operations_Work_Task__c)controller.getRecord();
            loadUserMap();
        }catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, e.getDmlMessage(0)));
            error = true;
            system.debug(e.getLineNumber() + ' ' + e.getDmlMessage(0));
            reloadPage = false;
        }
    }

    public void movefile(){
        loadhpsearch = 0;
        if (theCase.id != null) {
            if (ufiles.filename != null) {
                reloadpage = true;
                loadhpsearch = 1;
                Operations_Work_Task__c caseFoo = [select name, HP_Document_Type__c from Operations_Work_Task__c where id = :theCase.id];
                string m = ufiles.moveFile(ufiles.srcfile_filepath + '' + ufiles.fname, caseFoo.name, theCase.id, caseFoo.HP_Document_Type__c);
            }
        }
    }

    void loadNotes(){
        notes = new Electronic_Work_Task_Note__c[] {};
        notes = [select subject__c, body__c, createdDate, createdBy.Name from Electronic_Work_Task_Note__c where Electronic_Work_Task__c = :theCase.id order by createdDate desc];
    }

    public void saveNote(){
        error = false;
        try{
            upsert note;
            loadNotes();
        }catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, e.getDmlMessage(0)));
            error = true;
            reloadPage = false;
        }
    }

    public void getNote(){
        string noteId = ApexPages.CurrentPage().getParameters().get('noteId');

        for (Electronic_Work_Task_Note__c n: notes) {
            if (n.id == noteId) {
                note = n;
                break;
            }
        }
    }

    public void newNote(){
        note = new Electronic_Work_Task_Note__c(Electronic_Work_Task__c = theCase.id);
    }

    public void deleteNote(){
        error = false;
        try{
            delete note;
            loadNotes();
        }catch (DMLException e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.Error, e.getDmlMessage(0)));
            error = true;
            reloadPage = false;
        }
    }

    void loadUserMap(){
        // if(User==null){

        User = new List<SelectOption>();
        User.add(new SelectOption('', '-- None --'));
        User.add(new SelectOption('TBD', 'TBD'));
        boolean assignedAdded = false;

        for (EWT_User__c c : [select name from EWT_User__c where Name != '' order by name asc]) {
            if (theCase.assigned__c == c.name) {
                assignedAdded = true;
            }
            User.add(new SelectOption(c.name, c.name));
        }

        if (!assignedAdded && theCase.assigned__c != null) {
            User.add(new SelectOption(theCase.assigned__c, theCase.assigned__c));
        }

        // }
    }

    public SelectOption[] clients {
        get; set;
    }
    map<string, set<selectOption> > clientProcedureMap = new map<string, set<selectOption> >();
    public id clientId {
        get; set;
    }
    public id procedureId {
        get; set;
    }

    public SelectOption[] loadClients() {
        set<selectOption> clientSO = new set<selectOption>();

        clientSO.add(new selectOption('', '--None--'));
        for (client_facility__c cf : [select procedure__c, client__c, client__r.name, procedure__r.name from client_facility__c where active__c = true and facility__r.name != 'IMC' and client__r.Network_Exceptions_Allowed__c = true order by client__r.name desc]) {
            set<selectOption> procedureList = clientProcedureMap.get(cf.client__c);
            clientSO.add(new selectOption(cf.client__c, cf.client__r.name));

            set<selectOption> procedureSO = clientProcedureMap.get(cf.client__c);
            if (procedureSO == null) {
                procedureSO = new set<selectOption>();
            }

            procedureSO.add(new selectOption(cf.procedure__c, cf.procedure__r.name));

            clientProcedureMap.put(cf.client__c, procedureSO);
        }

        clients = new list<selectoption>(clientSO);

        return clients;
    }

    public SelectOption[] getProcedures() {
        selectoption[] temp = new selectoption[] {};
        temp.add(new selectOption('', '-'));
        set<selectoption> availableOptions = clientProcedureMap.get(clientId);

        if (availableOptions != null) {
            temp.addAll(availableOptions);
        }

        return temp;
    }

    string formatDate(Integer i){
        string s = string.valueof(i);

        if (s.length() == 1) {
            return '0' + s;
        }

        return s;
    }
}