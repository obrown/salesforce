@isTest
private class ewtPicklistSetupControllerTest {

    static testMethod void SetupControllerTest() {
        
        ewtPicklistSetupController epsc = new ewtPicklistSetupController();
        epsc.setDocumentTypeToUpdate();
        epsc.documentType.document_type__c='Unit Test';
        epsc.documentType.Description__c='Unit Test';
        epsc.documentType.HP_Document_Type__c='UT';
        epsc.documentType.Folder_Location__c='unit test';
        
        epsc.saveDocumentType();
        system.assert(epsc.documentType.id!=null);
        
        ApexPages.CurrentPage().getParameters().put('documentTypeId',epsc.documentType.id);
        epsc.esl.documentTypeValue = epsc.documentType.id;
        string documentTypeId = epsc.documentType.id;
        
        epsc.workTaskName= 'Unit Test Work Task';
        epsc.saveWorkTask();
        epsc.workTaskId = epsc.esl.workTask[1].getValue();
        system.assert(epsc.workTaskId!=null);
        epsc.esl.workTaskValue =epsc.workTaskId;
        string workTaskId = epsc.workTaskId;
        
        ApexPages.CurrentPage().getParameters().put('workTaskId',epsc.workTaskId);
        epsc.workTaskReasonName= 'Unit Test Work Task Reason';
        epsc.saveWorkTaskReason();
        epsc.workTaskReasonId = epsc.esl.workTaskReason[1].getValue();
        system.assert(epsc.workTaskReasonId !=null);
        epsc.esl.workTaskReasonValue=epsc.workTaskReasonId ;
        string workTaskReasonId = epsc.workTaskReasonId;
        
        ApexPages.CurrentPage().getParameters().put('workTaskReasonId',epsc.workTaskReasonId);
        epsc.workDepartmentName= 'Unit Test Work Department';
        epsc.saveWorkDepartment();
        epsc.workDepartmentId = epsc.esl.workDepartment[1].getValue();
        system.assert(epsc.workDepartmentId !=null);
        epsc.esl.workDepartmentValue=epsc.workDepartmentId;
        string workDepartmentId = epsc.workDepartmentId;
        
        epsc.closedReasonName= 'Unit Test Closed Reason';
        epsc.saveClosedReason();
        epsc.closedReasonId = epsc.esl.closedReason[1].getValue();
        system.assert(epsc.closedReasonId!=null); 
        string closedReasonId= epsc.closedReasonId;
        
        ApexPages.CurrentPage().getParameters().put('documentTypeId',documentTypeId);
        epsc.setDocumentTypeToUpdate();
        epsc.saveDocumentType();
        
        ApexPages.CurrentPage().getParameters().put('documentTypeId',documentTypeId);
        epsc.esl.setdocumentType();
        
        ApexPages.CurrentPage().getParameters().put('workTaskId',null);
        epsc.setWorkTaskToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('workTaskId',workTaskId);
        ApexPages.CurrentPage().getParameters().put('workTaskName','Test');
        epsc.setWorkTaskToUpdate();
        epsc.saveWorkTask();
        
        ApexPages.CurrentPage().getParameters().put('workTaskReasonId',null);
        epsc.setWorkTaskReasonToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('workTaskReasonId',workTaskReasonId);
        ApexPages.CurrentPage().getParameters().put('workTaskReasonName','Test');
        epsc.setWorkTaskReasonToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('workDepartmentId',null);
        epsc.setWorkDepartmentToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('workDepartmentId',workDepartmentId);
        ApexPages.CurrentPage().getParameters().put('workDepartmentName','Test');
        epsc.setWorkDepartmentToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('closedReasonId',null);
        epsc.setClosedReasonToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('closedReasonId',closedReasonId);
        ApexPages.CurrentPage().getParameters().put('closedReasonName','Test');
        epsc.setClosedReasonToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('ewtSoId',workDepartmentId);
        epsc.removeEWTSO();
        
        ApexPages.CurrentPage().getParameters().put('ewtSoId',workTaskReasonId);
        epsc.removeEWTSO();
        
        ApexPages.CurrentPage().getParameters().put('ewtSoId',workTaskId);
        epsc.removeEWTSO();
        
        ApexPages.CurrentPage().getParameters().put('ewtSoId',documentTypeId);
        epsc.removeEWTSO();
        
        ApexPages.CurrentPage().getParameters().put('ewtSoId',null);
        epsc.removeEWTSO();
        
        epsc = new ewtPicklistSetupController();
    }
    
}