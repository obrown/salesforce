public without sharing class logExceptionType{

        string ResultCode;
        public eligibilityRecs[] EligExceptions {get;set;}        
        
        public class EligibilityRecs{

            public string ElgxStatus {get;set;}  
            public string ElgxPriority {get;set;}
            public string ElgxType {get;set;}    
            public string ElgxDepseq {get;set;}
            string ElgxSdate{get;set;} //"ElgxSdate":"20140909" 
            public string frElgxSdate{get;set;} //"ElgxSdate":"20140909" 
            public string ElgxMsg{get;set;}
            string ElgxEdate {get;set;}  
            string ElgxPunbr {get;set;}  
            public string ElgxGrnbr {get;set;}
            public string ElgxEssn {get;set;}
            
            string ElgxHpfield {get;set;}
            string ElgxNewfield {get;set;} 
            string ElgxFil1 {get;set;} 
            boolean isError;
            
        }
        /*
        public void cleanUp_eLog(){
            if(EligibilityRecs!=null){
                for(EligibilityRecs e: this.EligibilityRecs){
                    if(e.ElgxSdate!=null){
                        e.ElgxSdate= e.ElgxSdate.trim();
                        e.frElgxSdate= e.ElgxSdate.mid(4,2) +'/'+ e.ElgxSdate.right(2) +'/'+e.ElgxSdate.left(4);
                    }
                   
                    
                 }
            }
        }
        */
}