public with sharing class facilityWorkflow{
    
    public set<id> fetchLatLong;
    
    public facilityWorkflow(){
        fetchLatLong = new set<id>();
    }
    
    public void fetchLatLong(){
        set<id> calls = new set<id>();
        integer counter=0;
        integer maxCalls=10;
        
        for(id  i : fetchLatLong){
            calls.add(i);
            counter++;
            if(maxCalls<=(fetchLatLong.size()-counter)){
                maxCalls=fetchLatLong.size(); 
            }
            
            if(maxCalls>=counter){
                facilityFuture.fetchLatLong(calls);
                calls.clear();
                counter=0;
            }
        }
        
    }
    
}