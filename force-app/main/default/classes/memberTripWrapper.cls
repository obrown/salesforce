public class memberTripWrapper{
    
    public memberTrips__c trip {get; private set;}
    public airlineTrips__c airlineTrip {get; private set;}
    public hotelTrips__c hotelTrip {get; private set;}
    
    public memberTripWrapper(memberTrips__c trip, airlineTrips__c airlineTrip, hotelTrips__c hotelTrip){
        this.trip = trip;
        this.airlineTrip = airlineTrip;
        this.hotelTrip = hotelTrip;
        
    }
    
    public void setHotelTrip(hotelTrips__c hotelTrip){
        this.hotelTrip=hotelTrip;
    }
        
    public void setairLineTrip(airLineTrips__c airLineTrip){
        this.airLineTrip=airLineTrip;
    }
}