public with sharing class coeIDCardController {
    public coeCaseEncryptedData cc { get; set; }

    public String claimEmailAddress { get; set; }

    public String claimMailAddressLine1 { get; set; }

    public String claimMailAddressLine2 { get; set; }

    public String claimMailAddressLine3 { get; set; }

    public String financialResponsibilityLanguage { get; set; }

    public String phone1 { get; set; }

    public String phone2 { get; set; }

    public String phone3 { get; set; }

    public String planLanguage { get; set; }

    public String program { get; set; }

    public coeIDCardController() {
        String cases = ApexPages.CurrentPage().getParameters().get('cases');
        ApexPages.CurrentPage().getParameters().put('cases',  null);

        cc = (coeCaseEncryptedData) JSON.deserialize(
            cases,
            coeCaseEncryptedData.class
        );

        patient_case__c pc = [
            select
                id,
                client_facility__r.facility__r.name,
                Client__r.logoDirectory__c,
                Ecen_Procedure__r.Name,
                Employee_HealthPlan__r.name,
                Employee_Primary_Health_Plan_Name__c,
                estimated_arrival__c,
                estimated_departure__c,
                nClient_Name__c,
                recordtype.name,
                Referred_Facility__c
            from patient_case__c
            where id = :cc.id
        ];

        cc.clientName = pc.nClient_Name__c;
        cc.facility = pc.client_facility__r.facility__r.name;
        cc.planType = pc.Employee_HealthPlan__r.name;

        claimEmailAddress = 'newcoeclaim@contigohealth.com';
        claimMailAddressLine1 = 'Contigo Health-' + cc.clientName + ' COE';
        claimMailAddressLine2 = 'P.O. BOX 2582';
        claimMailAddressLine3 = 'Hudson, OH 44236';

        // Member financial responsibility disclaimer shown when planLanguage isn't empty
        financialResponsibilityLanguage = '';

        // Phone # for Eligibility, Benefits, Travel, and Expense Money
        phone1 = '+1 (888) 463-3737';

        // Phone # for Care Coordination
        phone2 = '+1 (877) 286-3551';

        // Phone # for 24/7 Travel Support (American Express)
        phone3 = '+1 (646) 817-9847, #3';

        // Text shown at the bottom of the ID card's front face
        planLanguage = '';

        program = pc.Ecen_Procedure__r.Name;

        /**
         * Different clients have different email and mailing addresses, logos,
         * and phone numbers that are swapped out in the letter and ID card.
         */
        switch on pc.nClient_Name__c {
            when 'JBS' {
                cc.grp = 'JBS';
                cc.logoUrl = PageReference.forResource('JBS_HDP_Logo').getUrl();
            }

            when 'Lowes' {
                cc.clientName = 'Lowe\'s';
                cc.grp = 'LWE';
                cc.logoUrl = PageReference.forResource('lowesCoeIdCard').getUrl();
            }

            when 'McKesson' {
                cc.clientName = 'McKesson\'s';
                cc.grp = 'MCK';
                cc.logoUrl = PageReference.forResource('mcKessonCoeIdCard').getUrl();
            }

            when 'New England Baptist' {
                claimEmailAddress = 'esaulnie@nebh.org';
                claimMailAddressLine1 = '125 Parker Hill Avenue, Attn: Elizabeth Saulnier';
                claimMailAddressLine2 = 'Main 2, Ste 220';
                claimMailAddressLine3 = 'Boston, MA 02120';
            }

            when 'Scripps' {
                claimMailAddressLine1 = 'Scripps Health Plan Services-' + cc.clientName + ' COE';
                claimMailAddressLine2 = 'P.O. Box 2529, Mail Drop 4S-300,';
                claimMailAddressLine3 = 'LaJolla, CA 92038';
            }

            when 'Walmart' {
                cc.grp = 'WAL';
                cc.logoUrl = PageReference.forResource('walmartCoeIdCard').getUrl();
            }

            when else {
                String foo = PageReference.forResource('logos').getUrl();
                foo = foo.subString(0, foo.indexOf('?'));
                cc.logoUrl = foo + '/logos/' + pc.Client__r.logoDirectory__c;
            }
        }

        // Should stay below the switch statement since CNSA overrides some info
        if (pc.client_facility__r.facility__r.name != null && pc.client_facility__r.facility__r.name.tolowercase() == 'cnsa/chs') {
            cc.isCNSA = true;
            claimEmailAddress = 'hdpclaims@cnsa.com';
            claimMailAddressLine1 = 'CNSA, Attn: COE Travel';
            claimMailAddressLine2 = '225 Baldwin Avenue';
            claimMailAddressLine3 = 'Charlotte, NC 28204';
            phone2 = '+1 (877) 885-0654';
        }

        if (pc.Employee_HealthPlan__r.name == '' || pc.Employee_HealthPlan__r.name == null) {
            cc.planType = pc.Employee_Primary_Health_Plan_Name__c;
        }

        if (cc.planType != null) {
            // Performance optimization - circuit breaker
            Boolean shouldSkip = false;

            Set<String> plansWithHsaOrPpo = new Set<String>{
                'ACP (Banner)',
                'ACP (Mercy)',
                'ACP (Presbyterian)',
                'Advantage HSA',
                'Basic EPO 60',
                'Choice Account (HSA)',
                'Choice Account Plus (HSA)',
                'EPO',
                'Flex PPO',
                'HSA A Family Coverage (SISOFECN)',
                'HSA A Single Coverage (SISOAECN)',
                'HSA B (SISOBECN)',
                'HSA Family',
                'HSA Individual',
                'Local Plan',
                'Mercy St. Louis Local Plan (Mercy St. Louis ACP)',
                'Option 1 PPO',
                'Option 2 PPO',
                'PPO',
                'PPO (SISOPECN)',
                'Premier 80 HRA',
                'St. Luke\'s Local Plan',
                'Walmart Contribution Plan',
                'Walmart Premier Plan',
                'Walmart Saver Plan'
            };

            for (String plan : plansWithHsaOrPpo) {
                if (cc.planType == plan) {
                    financialResponsibilityLanguage = 'You will not be charged for any covered services under the program unless you are enrolled in the HSA plan. If you are enrolled in the HSA plan, you will be responsible for any outstanding deductible balance and/or out-of-pocket maximum balance. Contact the administrator listed on your medical plan ID card to find your deductibles and out-of-pocket maximums.';
                    planLanguage = 'For non-HSA plan enrollees, there is no member cost-share (e.g. co-pay) under this program. For HSA plan enrollees, refer to your medical plan ID card for deductibles and out-of-pocket maximums.';
                    shouldSkip = true;
                }
            }

            if (shouldSkip == false) {
                Set<String> plansWithDeductible = new Set<String>{
                    'FPL Bargaining',
                    'HRA - Family/Employee & Spouse',
                    'HRA - Individual/EE & Child',
                    'HSA - Family/Employee & Spouse',
                    'HSA - Individual/EE & Child',
                    'HSA (EE and EE + 1)',
                    'HSA (Family)',
                    'HSA Plus (EE and EE + 1)',
                    'Jupiter Tequesta AC Plumbing Electric',
                    'PPO',
                    'Retiree Consumer 4000 - No Travel Benefits',
                    'Retiree Pre65 2000 Plan - No Travel Benefits',
                    'Retiree Pre65 Base - No Travel Benefits',
                    'Retiree Pre65 Coinsurance - No Travel Benefits',
                    'Retiree Pre65 Copay Plan - No Travel Benefits',
                    'Retiree Pre65 Enhanced - No Travel Benefits',
                    'Retiree Pre65 HDHP - No Travel Benefits',
                    'Retiree Pre65 II - No Travel Benefits',
                    'Retiree Pre65 Network Only - No Travel Benefits'
                };

                for (String plan : plansWithDeductible) {
                    if (cc.planType == plan) {
                        financialResponsibilityLanguage = 'You will be responsible for any outstanding deductible balance and/or out-of-pocket maximum balance for covered services under this program. Contact the administrator listed on your medical plan ID card to find your deductibles and out-of-pocket maximums.';
                        planLanguage = 'There is a member cost-share (e.g. deductible only, or deductible and coinsurance) under this program. Refer to your medical plan ID card for deductibles and out-of-pocket maximums.';
                    }
                }
            }
        }

        if (pc.estimated_arrival__c != null) {
            cc.effDate = pc.estimated_arrival__c.month() + '/' + pc.estimated_arrival__c.day() + '/' + pc.estimated_arrival__c.year();
        }

        if (pc.estimated_departure__c != null) {
            if (cc.clientName == 'JBS' && cc.isCNSA == true) {
                cc.termDate = utilities.formatUSdate(pc.estimated_departure__c.adddays(1));
            } else {
                cc.termDate = pc.estimated_departure__c.month() + '/' + pc.estimated_departure__c.day() + '/' + pc.estimated_departure__c.year();
            }
        }
    }
}
