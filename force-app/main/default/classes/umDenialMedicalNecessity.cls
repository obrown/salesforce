public with sharing class umDenialMedicalNecessity {
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c uml, Utilization_Management_Clinician__c clinician, Utilization_Management_Clinical_Code__c[] diagnosisCodes, Utilization_Management_Clinical_Code__c[] clinicalCodes){
        boolean hasClinician = (Clinician.Street__c != null && Clinician.City__c != null && Clinician.State__c != null && Clinician.Zip_Code__c != null);
        boolean hasAddress = (um.patient__r.Address__c != null && um.patient__r.City__c != null && um.patient__r.State__c != null && um.patient__r.Zip__c != null);

        string letterText = '';

        letterText += '<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        /*
         * letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c+'<br/>';
         * letterText+=hasClinician ? Clinician.Street__c+'<br/>' : '<br/>';
         * letterText+=hasClinician ? Clinician.City__c+'&nbsp' : '&nbsp';
         * letterText+=hasClinician ? Clinician.State__c+'&nbsp' : '&nbsp';
         * letterText+=hasClinician ? Clinician.Zip_Code__c+'<br/><br/>' : '<br/><br/>';
         */
        letterText += um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c + '<br/>';
        letterText += hasAddress ? um.patient__r.Address__c + '<br/>': '' + '<br/>';
        letterText += hasAddress ? um.patient__r.City__c + ', ': ', ';
        letterText += hasAddress ? um.patient__r.State__c + ' ' : ' ';
        letterText += hasAddress ? um.patient__r.Zip__c + '<br/><br/>': '' + '<br/><br/>';
        letterText += 'RE:&nbsp;&nbsp;Patient Name:&nbsp;' + um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c;
        letterText += '<br/>';
        letterText += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth:&nbsp;' + um.patient__r.Patient_Date_of_Birth__c;
        letterText += '</div>';
        letterText += '<p>';
          //letterText+='Dear&nbsp;'+Clinician.Salutation__c+'&nbsp;'+Clinician.Last_Name__c+',';
        letterText += 'Dear&nbsp;' + um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c + ',';
        letterText += '</p>';
        letterText += '<p>';
        letterText += 'Contigo Health is a third-party administrator (TPA) that performs Care Management Services for the self-funded group health plan of ' + um.patient__r.Patient_Employer__r.name + ', which is regulated by ERISA under the US Department of Labor.<br/>';
        letterText += '</p>';

        letterText += 'The request referenced below has been denied:<br/><br/>';
        letterText += '<div style="margin-top:10px">';
        letterText += '<div style="display:inline-block;width:15%">';
        letterText += 'Case Number:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px;">';
        letterText += '&nbsp;' + um.HealthPac_Case_Number__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:15%">';
        letterText += 'Physician Name:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;' + Clinician.First_Name__c + ' ' + Clinician.Last_Name__c + ', ' + Clinician.Credentials__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:15%">';
        letterText += 'Facility Name:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;' + um.Facility_Name__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:15%">';
        letterText += 'Dates of Service:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;Beginning ' + um.Admission_Date__c.month() + '/' + um.Admission_Date__c.day() + '/' + um.Admission_Date__c.year();
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:15%;vertical-align:top">';
        letterText += 'Type of Request:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;' + uml.Type_of_Request__c;
        letterText += '</div>';
        letterText += '</div>';

        letterText += '<br/>';
        letterText += 'Diagnoses: ';
        if (diagnosisCodes.isEmpty()) {
            letterText += 'N/A';
        }
        else{
            for (Utilization_Management_Clinical_Code__c dc :diagnosisCodes) {
                letterText += dc.name + ' ' + dc.Description__c + ' ;';
            }
            letterText = letterText.removeEnd(';');
        }
        letterText += '<br/><br/>';
        letterText += 'Treatment: ';

        if (clinicalCodes.isEmpty()) {
            letterText += 'N/A';
        }
        else{
            for (Utilization_Management_Clinical_Code__c dc :clinicalCodes) {
                letterText += dc.name + ' ' + dc.Description__c + ' ;';
            }
            letterText = letterText.removeEnd(';');
        }
        letterText += '<br/><br/>';

        letterText += 'Denial Reason:&nbsp;';
        letterText += '<br/><br/>';

        letterText += 'Clinical Rationale Used to Make Determination: MCG®:';
        letterText += '<br/><br/>';

        letterText += 'Although additional information is not required to submit an appeal, all information provided will be considered when rendering an Appeal Decision.';
        letterText += '<br/><br/>';

        letterText += 'Additional Information Consideration:<br/>';
        letterText += '    •    Documentation representing material difference from information originally submitted.';
        letterText += '<br/><br/>';

        letterText += 'All choices regarding the care and treatment of the patient remain the responsibility of the attending physician. In no way does this letter attempt to dictate the care the patient ultimately receives.';

        letterText += '<br/><br/>';
        letterText += 'Sincerely,';
        letterText += '<div ></div>';
        letterText += '<div style="display:none" >signhere</div>';
        letterText += 'Eric M. Yasinow, M.D.<br/>';
        letterText += 'Medical Director';

        letterText += '<p>';
        letterText += 'SPANISH (Español): Para obtener asistencia en Español, llame al 330-656-1072<br/>';
        letterText += 'TAGALOG (Tagalog):  Kung kailangan ninyo ang tulong sa Tagalog tumawag sa 330-656-1072<br/>';
        letterText += 'CHINESE <span style="font-family: Arial Unicode MS">(中文):  如果需要中文的帮助，请拨打这个号码</span> 330-656-1072<br/>';
        letterText += 'NAVAJO (Dinek\'ehgo  shika  at\'ohwol  ninisingo, kwiijigo  holne\' 330-656-1072<br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'The plan complies with applicable Federal civil rights laws and does not discriminate on the basis of race, color, national origin, sex, age or disability above the appeal right section of the denial letters.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'The claimant has the right to request, verbally or in writing and free of charge, any internal rule, guideline, protocol, scientific or clinical rationale, other similar criteria, or plan provision used to make the original determination. Submit requests to Contigo Health at the address on this letter or call Customer Service at the number on your Medical Card.';
        letterText += '</p>';

        letterText += '<p style="text-align:center;text-decoration:underline">';
        letterText += 'RIGHT TO APPEAL';
        letterText += '</p>';
        letterText += '<p>';
        letterText += 'First appeal level:  The claimant has the right to initiate a formal appeal concerning any denied or partially denied claim verbally or in writing. The appeal should include the reason(s) for the request, any additional facts or documentation and/or a copy of the Explanation of Benefits that supports the appeal. To submit a first level appeal, send to Contigo Health within 180 days of receipt of this letter or call Customer Service at the number on your Medical Card. If the claimant does not submit an appeal on time, he or she will lose the right to the appeal and the right to file suit in court.';
        letterText += '</p>';
        letterText += '<p>';
        letterText += 'An Expedited Appeal is available when the adverse determination relates to a claim involving urgent care, where the application of standard appeal time periods could seriously jeopardize the life or health of the claimant or the ability to regain maximum function; or where in the opinion of a physician with knowledge of the claimant’s medical condition, would subject them to severe pain that cannot be adequately managed without the care or treatment that is the subject of the appeal.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'To file an Expedited Appeal, please call the Appeals Department at 1-877-230-0996 or fax the request to 1-877-891-2691 Attention: Appeals Coordinator.  Only Expedited Appeals will be accepted at these numbers.';
        letterText += '</p>';

        letterText += '<div>';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += 'CC:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;">';
        letterText += Clinician.First_Name__c + ' ' + Clinician.Last_Name__c + ', ' + Clinician.Credentials__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;"/>';
        letterText += hasClinician ? Clinician.Street__c : '';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;">';
        letterText += hasClinician ? Clinician.City__c + ', ' : ', ';
        letterText += hasClinician ? Clinician.State__c + ' ' : ' ';
        letterText += hasClinician ? Clinician.Zip_Code__c : '';
        letterText += '</div>';
        letterText += '</div><br/>';

        letterText += '<div style="margin-top:10px">';

        letterText += '<div style="display:inline-block;">';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        boolean hasFacility = (um.Facility_Name__c != null && um.Facility_Street__c != null && um.Facility_City__c != null && um.Facility_State__c != null && um.Facility_Zip_Code__c != null);

        letterText += hasFacility ? um.Facility_Name__c : '';

        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;">';
        letterText += hasFacility ? um.Facility_Street__c : '';
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;">';
        letterText += hasFacility ? um.Facility_City__c + ', ' : '';
        letterText += hasFacility ? um.Facility_State__c + ' ' : '';
        letterText += hasFacility ? um.Facility_Zip_Code__c : '';
        letterText += '</div>';
        letterText += '</div>';
        letterText += '</div>';

        letterText += '</div>';
        return letterText;
    }
}