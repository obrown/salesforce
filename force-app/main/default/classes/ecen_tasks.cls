public with sharing class ecen_tasks{
    
    boolean isFacility;
    
    public ecen_tasks(boolean isFacility){
        this.isFacility = isFacility;
    }
    
    public class result{
        public boolean isError;
        public string errorMsg;
        public Patient_Case_Task__c[] tasks;
    }
    
    public result getPatientCaseTasks(patient_case__c pc){
        Patient_Case_Task__c[] tasks;
        try{
            if(isFacility){
                tasks = [select name,completedDate__c,isCustom__c,status__c,description__c,organization__c,dueDate__c,dueDateDate__c,Case_Task__r.Status_Options__c,Patient_Case__c, Patient_Case__r.client_facility__r.client__r.name,Patient_Case__r.client_facility__r.procedure__r.name,visible_to_facility__c from Patient_Case_Task__c where patient_case__c = :pc.id and visible_to_facility__c = true order by name asc];    
            }else{
                tasks = [select name,completedDate__c,isCustom__c,status__c,description__c,organization__c,dueDate__c,dueDateDate__c,Case_Task__r.Status_Options__c,Patient_Case__c, Patient_Case__r.client_facility__r.client__r.name,Patient_Case__r.client_facility__r.procedure__r.name,visible_to_facility__c from Patient_Case_Task__c where patient_case__c = :pc.id order by name asc];    
            }
            
            if(tasks.isEmpty()){
               return createPatientCaseTaskList(pc);
            }
            
            tasks.addAll(checkForNewProgramTasks(tasks));
            
        }catch(exception e){
            system.debug(e.getMessage());
            
        }
        
        if(tasks==null){
           return createPatientCaseTaskList(pc);
        
        }
        
        result result = new result();
        result.isError = false;
        result.tasks = tasks ;
        return result;
    }
    
    public result createPatientCaseTaskList(patient_case__c pc){
        result result = new result();
        result.isError = false;
        
        Patient_Case_Task__c[] tasks = new Patient_Case_Task__c[]{};
        string queryFields='select ';
        string program = pc.client_facility__r.Client__r.name + ' '+ pc.client_facility__r.Procedure__r.name;
        
        if(isFacility){
            tasks = syncTasks(createTasks([select field__c,Status_Options__c,name,description__c,visible_to_facility__c,organization__c from Case_Task__c where Program__c = :program and visible_to_facility__c = true order by name asc], pc.id), pc.id);
        }else{
            tasks = syncTasks(createTasks([select field__c,Status_Options__c,name,description__c,visible_to_facility__c,organization__c from Case_Task__c where Program__c = :program order by name asc], pc.id), pc.id);
        }
        
        try{
            insert tasks;
        }catch(dmlexception e){
            result.isError=true;
            result.errorMsg=e.getDMLMessage(0);
            result.tasks = new Patient_Case_Task__c[]{};
            return result ;
        }
        
        result.tasks = tasks;
        return result;
    }
    
    public static Patient_Case_Task__c[] syncTasks(Patient_Case_Task__c[] iTasks, id pc_id){
        
        if(iTasks==null || iTasks.isEmpty()){
            return iTasks;
        }
        
        Patient_Case_Task__c[] tasks = iTasks;
        string queryFields='';
        
        for(Patient_Case_Task__c task : tasks){
            if(task.field__c!=null){
                queryFields += task.field__c +',';
            }  
        
        }
        
        queryFields = 'select '+ queryFields.removeEnd(',') + ' from Patient_Case__c where id = ' + '\'' + pc_id + '\''; 
        Patient_Case__c pc = database.query(queryFields);
        
        for(Patient_Case_Task__c task : tasks){
            
            if(task.field__c!=''){
                try{
                    if(pc.get(task.field__c)!=null){
                        if((task.status__c == '' || task.status__c== 'Not Started') ){
                            task.status__c = 'Complete';
                        }
                        task.completedDate__c = date.valueof(pc.get(task.field__c));
                        
                    }    
                    
                }catch(exception e){}
                
            }
            
        }
        
        return tasks;
    }
    
    
    Patient_Case_Task__c[] createTasks(Case_Task__c[] caseTasks, string pc_id){
        Patient_Case_Task__c[] tasks = new Patient_Case_Task__c[]{};
        
        for(Case_Task__c t : caseTasks){
            Patient_Case_Task__c task = new Patient_Case_Task__c(Patient_Case__c=pc_id);
            task.Case_Task__c = t.id;
            task.name = t.name;
            task.description__c = t.description__c;
            task.organization__c = t.organization__c;
            task.Status_Options__c = 'N/A;Not Started;In Progress;Complete';
            task.visible_to_facility__c = t.visible_to_facility__c ;
            task.status__c= 'Not Started';
            task.field__c= t.field__c;
            tasks.add(task);
        }
        
        return tasks;
    }
    
    public Patient_Case_Task__c[] checkForNewProgramTasks(Patient_Case_Task__c[] pTasks){
        
        Patient_Case_Task__c[] tasks = pTasks;
        
        set<string> existingTasks = new set<string>();
        string program = tasks[0].Patient_Case__r.client_facility__r.client__r.name+' '+tasks[0].Patient_Case__r.client_facility__r.procedure__r.name;
        
        for(Patient_Case_Task__c t :  tasks){
            existingTasks.add(t.name);
        }
        
        if(isFacility){
            tasks = syncTasks(createTasks([select field__c,Status_Options__c,name,description__c,visible_to_facility__c,organization__c from Case_Task__c where Program__c = :program and visible_to_facility__c= true and name not in :existingTasks order by name asc], tasks[0].Patient_Case__c), tasks[0].Patient_Case__c);
        }else{
            tasks = syncTasks(createTasks([select field__c,Status_Options__c,name,description__c,visible_to_facility__c,organization__c from Case_Task__c where Program__c = :program and name not in :existingTasks order by name asc], tasks[0].Patient_Case__c), tasks[0].Patient_Case__c);
        }
        
        if(!tasks.isEmpty()){
            insert tasks;
        }
        return tasks;
    }
}