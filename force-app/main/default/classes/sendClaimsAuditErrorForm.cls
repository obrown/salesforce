global with sharing class sendClaimsAuditErrorForm{

    webservice static string claimsAuditError(string errorId){
        
        set<id> foo = new set<id>();
        Claim_Audit_Error__c[]  ceList = new Claim_Audit_Error__c[]{};
        
        for(Claim_Audit_Error__c  ce : [select Claim_Audit__c,emailSent__c,Error_Source__c from Claim_Audit_Error__c where id = :errorId]){
            
            foo.add(ce.Claim_Audit__c);
            ce.emailSent__c = date.today();
            ceList.add(ce);
        }
        
        claimAuditFuture.sendClaimAuditErrors(foo, false);
        update ceList;
        {return 'success';}
    }
    
}