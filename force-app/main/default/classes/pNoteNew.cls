public with sharing class pNoteNew {
    
    public Patient_Case__c objPC {get;set;}
    public Program_Notes__c objNote {get;set;}
    public Transplant__c objTrans {get;set;}
    public Transplant_Notes__c objtransNote {get;set;}
    
    public string saveError {get;set;}
    public string message {get;set;}
    public boolean isError {get;set;}
    public string theSubject {get;set;}
    public string theNotes {get;set;}
    public boolean isCCnote {get;set;}
    public string auth {get; private set;}
    
    list<Program_Notes__c> pnc;
    list<Transplant_Notes__c> tnc;
    ApexPages.StandardController theController;
    id thePCID;
    public String Type;
    String sortfield;
    string sortOrder;
    
    public string sf {get;set;}
    public string value {get;set;}
    
    public ftpAttachment__c attach {get; set;}
    public ftpAttachment__c[] attachments {get; set;}
    
    public transient String fileName {get;set;}
    public transient Blob fileBody {get;set;}
    public transient string fileContent {get; set;}
    
    
    public string theID {get;set;}
    public string attachID {get;set;}
    public string url {get;set;}
    string objtype;
    
     public pNoteNew() {
       theid = ApexPages.currentPage().getParameters().get('id');
       objtype = utilities.getObjType(theid.left(3));
       
       
      if(objtype=='Patient_Case__c'){ 
        objPC = [select id, name,Patient_First_Name__c,Patient_Last_Name__c,Patient_Case__c,attachEncrypt__c,Employee_First_Name__c, Employee_Last_Name__c from Patient_Case__c where id =:theID];
        thePCID = id.valueof(theId);
      }else if(objtype=='Transplant__c'){
        thePCID = theID;
        objTrans = [select id, name,Patient_First_Name__c,Patient_Last_Name__c,Transplant__c,attachEncrypt__c,Employee_First_Name__c, Employee_Last_Name__c from Transplant__c where id =:theID];
        
      }  
        
    }
    
    public void myCancel(){
       message='';
       saveError='false';
       isError=false;
    }
    
    public void mydelete(){
    
      //pageReference returnPage = new pageReference('');
      
      if(objtype=='Program_Notes__c'){
            url = '/' + objPC.id;
        try{
            if(objNote.CreatedById == userinfo.getUserId()){
                delete objNote; 
                saveError='false';
            }else{
              saveError='true';
              isError=true;
              message = 'You cannot delete a note that was created by another user.';
            }
        }catch (exception e){
              saveError='true';
              isError=true;
              if(e.getMessage().contains('INSUFFICIENT_ACCESS')){message = 'You cannot delete a note that was created by another user';}else{message = 'REQUIRED FIELD MISSING: ' + utilities.errorMessageTextRF(e.getMessage());}
        
        } 

       }else if(objtype=='Transplant_Notes__c'){
            url = '/apex/transDetail?id=' + objTrans.id;
        try{
        delete objTransNote; 
        saveError='false';
        }catch (exception e){
            system.debug(e);
              saveError='true';
              isError=true;
              if(e.getMessage().contains('INSUFFICIENT_ACCESS')){message = 'You cannot delete a note that was created by another user';}else{message = 'REQUIRED FIELD MISSING: ' + utilities.errorMessageTextRF(e.getMessage());}
        
        } 
        }else{
            url = '/apex/TransTab?save_new=1&sfdc.override=1';
         }
         
  
        
    }
    
    public void decryptAttach(){
        
        if(attachID == null || attachID.left(3)=='a0M'){
            return;
        }else{
            try{
            Attachment a = [select body from attachment where ID = :attachID];
           
            blob key = EncodingUtil.base64Decode(objPC.attachEncrypt__c);
            encryptAttach ea = new encryptAttach(key);
            blob thebody= a.body;
            blob decryptedbody;
            decryptedbody= ea.decryptAttachmentBlob(thebody);
            a.body=decryptedbody;
            update a;
            }catch(exception e){
                isError=true;
                message = 'Something went wrong trying to decrypt the file. Please notify your system administrator.';
            }
        } 
    }
    
    public void encryptAttach(){
        Attachment a = [select body from attachment where ID = :attachID];
        blob key = EncodingUtil.base64Decode(objPC.attachEncrypt__c);
        encryptAttach ea = new encryptAttach(key);
        blob thebody= a.body;
        blob encryptedbody;
        encryptedbody=ea.encryptAttachmentBlob(thebody);
        a.body=encryptedbody;
        update a;
                
    }
    /*
    public void saveAttachment(){
        isError=false;
        
        if(attach.name==''){
            isError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'No File Selected, or an error has occured'));
            return;
        }else{
        
            try{
                attach.fileName__c = objtransNote.Transplant__c+'_'+attach.name;
                upsert attach;
                loadAttachments();
                
                auth='bW1hcnRpbjpDaTNycmExNDMh';
                attach = new ftpAttachment__c(Transplant_Case__c= objtransNote.Transplant__c);
            }catch(dmlexception e){
                isError=true;    
                for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)); 
                }
            }
        
        }
        
    }
    */
    
    void loadAttachments(){
        
        attachments = new ftpAttachment__c[]{};
        attachments = [select Transplant_Note__c, comments__c, fileName__c, name, createdDate, createdby.name from ftpAttachment__c where Transplant_Note__c = :objtransNote.id and Transplant_Note__c != ''];
        system.debug('attachments '+attachments);
    }
    
    public void uploader(){
         system.debug('attach ' +attach);
         if(attach==null){
             
             this.fileName = ApexPages.CurrentPage().getParameters().get('fileName');
             this.fileContent = ApexPages.CurrentPage().getParameters().get('fileContent');
             this.fileBody = EncodingUtil.Base64Decode(ApexPages.CurrentPage().getParameters().get('fileBody'));
             
             this.fileName= this.fileName.replaceAll(' ', '');
             this.fileName= this.fileName.replaceAll('-', '_');
             this.fileName= this.fileName.replaceAll(',', '_');
             
             HttpResponse res = ussscorpionFileSharing.uploader(theID+'_'+ this.fileName, this.fileContent , this.fileBody);
             
             if(res.getStatusCode() < 250){
                 attach = new ftpAttachment__c();
                 attach.fileName__c = theID+'_'+this.fileName;
                 attach.name=this.fileName;
                 attach.Transplant_Case__c = objtransNote.Transplant__c;
                 attach.Transplant_Note__c = objtransNote.id;
                 
                 system.debug('attach ' +attach);
                 
                 upsert attach;
                 
                 objTransNote.ftpAttachment__c = attach.id;
                 update objTransNote;
                 
             }else{
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, res.getBody()));
                 attach = new ftpAttachment__c();
                 return;
             }
             
         }else{
             upsert attach;
             
         }
         attach=null;
         loadAttachments();
         return;
         
    }
    
    public PageReference ussscorpion(){ 
         
        string recordName = ApexPages.CurrentPage().getParameters().get('recordName');
        ApexPages.CurrentPage().getParameters().put('recordName', null);
        return ussscorpionFileSharing.getter(recordName);
     
    }
    
    public pageReference deleteAttachment(){
        String deleteID = ApexPages.currentPage().getparameters().get('attachmentID');
        String recordName = ApexPages.currentPage().getparameters().get('recordName');
        
        ApexPages.currentPage().getparameters().put('attachmentID', null);
        ApexPages.currentPage().getparameters().put('recordName', null);
        
        ussscorpionFileSharing.deleteAttachment(recordName);
        
        for(ftpAttachment__c f : attachments){
                if(f.id==deleteID){
                    delete f;
                    loadAttachments();
                    attach=null;
                    break;
                }
        }
        
        return null;
    }
    
    public pNoteNew(ApexPages.StandardController controller) {

        theId = controller.getID();
        objtype = utilities.getObjType(theid.left(3));
        system.debug(theid);
       system.debug(objtype);
        if(objtype =='Patient_Case__c'){
            objPC = [select id, Patient_First_Name__c,Patient_Last_Name__c,patient_case__c,attachEncrypt__c, Employee_First_Name__c, Employee_Last_Name__c from Patient_Case__c where id =:theID];  
            
        }else if(objtype =='Program_Notes__c'){
            objNote = [select CreatedDate,Contact_Type__c,Communication_Type__c,Initiated__c,Contact_Name__c,Patient_Case__c,Notes__c,attachID__c,Subject__c,legacyCreatedDate__c, createdby.name, createdbyid, lastmodifieddate, lastmodifiedbyid,lastmodifiedby.name, type__c, Clinical_Note__c ,attachment__c from Program_Notes__c where id = :theID];
            theSubject = objNote.Subject__c;
            theNotes = objNote.Notes__c;
            Type = objNote.Communication_Type__c;
            isCCnote = objNote.Clinical_Note__c;
            objPC = [select id, name, patient_case__c,Patient_First_Name__c,Patient_Last_Name__c,attachEncrypt__c from Patient_Case__c where id =:objNote.Patient_Case__c];
            
        }else if(objtype=='Transplant_Notes__c'){
            objtransNote = [select ftpAttachment__c,Transplant__c,Contact_Type__c,Contact_Name__c,initiated__c,Notes__c,attachID__c,Subject__c,legacyCreatedDate__c, createdby.name, createdbyid, lastmodifieddate, lastmodifiedbyid,lastmodifiedby.name, type__c, Clinical_Note__c ,attachment__c from Transplant_Notes__c where id = :theID];
            theSubject = objtransNote.Subject__c;
            theNotes = objtransNote.Notes__c;
            Type = objtransNote.Type__c;
            isCCnote = objtransNote.Clinical_Note__c;
            objTrans = [select id, name,Transplant__c,Patient_First_Name__c,Patient_Last_Name__c,attachEncrypt__c from Transplant__c where id =:objtransNote.Transplant__c];   
            loadAttachments();   
        }else if(objtype =='Transplant__c'){
            objTrans = [select id, Patient_First_Name__c,Patient_Last_Name__c,transplant__c,attachEncrypt__c, Employee_First_Name__c, Employee_Last_Name__c from Transplant__c where id =:theID];
        }   
        
    }
    
    
    public void filtertnc(){
        string theQuery;
        string subject='';
        
        if(sf=='none'){
            theQuery = 'select contact_type__c,Subject__c,type__c,Initiated__c,Contact_Name__c,Clinical_Note__c,Notes__c,attachment__c,attachID__c,CreatedBy.Name,CreatedDate,Legacy_Created_By__c,legacyCreatedDate__c from transplant_Notes__c where transplant__c = :thePCID order by createdDate asc';
        }else{
            
            if(sf=='System'){
                sf='initiated__c';
                if(value=='Patient Info Change'){
                    subject = 'and (subject__c = \'Patient Address Change\' or subject__c = \'Patient DOB Change\' or subject__c = \'Patient SSN Change\' or subject__c = \'Patient Phone Change\'' + ')';
                }else if(value=='Employee Info Change'){
                    subject = 'and (subject__c = \'Employee Address Change\' or subject__c = \'Employee DOB Change\' or subject__c = \'Employee SSN Change\' or subject__c = \'Employee Phone Change\'' + ')';
                }else if(value=='Transplant Type Change'){
                    subject = 'and (subject__c = \'Transplant Type Change\'' + ')';
                }else if(value=='Status Change'){
                    subject = 'and (subject__c = \'Status Change\'' + ')';
                }else if(value=='Carrier Info Change'){
                    subject = 'and (subject__c = \'Status Change\'' + ')';
                }
                
                value = 'System';
            }
            theQuery = 'select contact_type__c,Subject__c,type__c,Initiated__c,Contact_Name__c,Clinical_Note__c,Notes__c,attachment__c,attachID__c,CreatedBy.Name,CreatedDate,Legacy_Created_By__c,legacyCreatedDate__c from transplant_Notes__c where transplant__c = :thePCID and '  + sf + '=' + '\'' + value + '\'' + subject +'order by createdDate asc';
        }
        tnc = database.query(theQuery);
    }
    
    public list<Transplant_Notes__c> getTNC(){
        
        if(tnc==null){
                
            tnc = [select contact_type__c,Subject__c,type__c,Initiated__c,Contact_Name__c,Clinical_Note__c,Notes__c,attachment__c,attachID__c,CreatedBy.Name,CreatedDate from Transplant_Notes__c where Transplant__c =:thePCID order by CreatedDate desc];
        } 

        return tnc;
        
    }
    
    public list<Program_Notes__c> getPNC(){
        if(sortfield==null){
        sortfield = 'legacyCreatedDate__c';
        }
        
        if(sortOrder==null){
        sortOrder = ' desc';
        }
        String theQuery;
        
        if(thePCID != null){
            theQuery = 'select id, notes__c, communication_type__c,type__c, subject__c,Clinical_Note__c,CreatedBy.Name, CreatedDate,attachID__c,attachment__c,Legacy_Created_By__c,legacyCreatedDate__c from Program_Notes__c where patient_case__c = :thePCID order by ' + sortField + sortOrder;
        }else{
            thePCID = id.valueof(ApexPages.currentPage().getParameters().get('id'));
            theQuery = 'select id, notes__c, communication_type__c,type__c, subject__c,Clinical_Note__c,CreatedBy.Name, CreatedDate,attachID__c,attachment__c,Legacy_Created_By__c,legacyCreatedDate__c from Program_Notes__c where patient_case__c = :thePCID order by ' + sortField + sortOrder;
        }

        pnc = database.query(theQuery);
      
        return pnc;
    }
   
    public String getType() {
       return Type;
    }
    
     public List<SelectOption> getTypeItems() {
        List<SelectOption> options = new List<SelectOption>();
                        options.add(new SelectOption('Call','Call'));
                        options.add(new SelectOption('E-Mail','E-Mail'));
                        options.add(new SelectOption('Note','Note'));
                        options.add(new SelectOption('Other','Other'));
        return options;
    }
    
    public void setType(String Type){
        this.type = type;
    }

 
    
    public void mySave(){
        saveError='false';
        isError=false;
       // string objtype = utilities.getObjType(theid.left(3));
        
        if(objtype == 'Program_Notes__c'){
            url = '/' + objPC.id;

            try{ 

                objNote.Subject__c = theSubject;
                objNote.Notes__c = theNotes;
                objNote.Type__c = Type;
                objNote.Clinical_Note__c = isCCnote;
                upsert objNote;
    
            }catch(exception e){
              saveError='true';
              isError=true;
              if(e.getMessage().contains('_EXCEPTION')){message = utilities.errorMessageText(e.getMessage());}else{message = 'REQUIRED FIELD MISSING: ' + utilities.errorMessageTextRF(e.getMessage());}
             }  

        }else if(objtype == 'Transplant_Notes__c'){
            url = '/apex/transDetail?id=' + objTrans.id;
            try{ 

                objtransNote.Subject__c = theSubject;
                objtransNote.Notes__c = theNotes;
                objtransNote.Type__c = Type;
                objtransNote.Clinical_Note__c = isCCnote;
                upsert objtransNote;
    
            }catch(exception e){
              saveError='true';
              isError=true;
              if(e.getMessage().contains('_EXCEPTION')){message = utilities.errorMessageText(e.getMessage());}else{message = 'REQUIRED FIELD MISSING: ' + utilities.errorMessageTextRF(e.getMessage());}
             }  

        }

    }
    
    public pageReference sortmyListbyDate(){
    
       if(sortOrder==' asc'){
           sortOrder=' desc';
       }else{
           sortOrder=' asc';
       }
       
   
    return null;
    }
    
    public pageReference sortmyListbyName(){
       if(sortOrder==' asc'){
           sortOrder=' desc';
       }else{
           sortOrder=' asc';
       }
       sortField='Legacy_Created_By__c';

    return null;
    }
    
    public pageReference sortbySubject(){
       if(sortOrder==' asc'){
           sortOrder=' desc';
       }else{
           sortOrder=' asc';
       }
       sortField='Subject__c';
    return null;
    }
    
    public pageReference sortmyListbyType(){
       if(sortOrder==' asc'){
           sortOrder=' desc';
       }else{
           sortOrder=' asc';
       }
       sortField='Type__c';
    return null;
    }
    
    public void clearAttachment(){
    
    }
    
    public boolean getisEdit(){
        return objNote.CreatedDate > datetime.now().addHours(-24);
    }
    
    public boolean getisDelete(){
        
    Schema.DescribeSObjectResult pn = Schema.SObjectType.Program_Notes__c;
    
    return pn.isDeletable();
    
    }

}