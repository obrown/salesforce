public class tamuGeoServicesStruct {
    
     public String TransactionId;
       // public String Version;
        public String QueryStatusCodeValue;
        public String FeatureMatchingResultType;
        public String FeatureMatchingResultCount;
        public String TimeTaken;
        public InputAddress InputAddress;
        public List<OutputGeocodes> OutputGeocodes;
    /*
    public class TamuGeoservices {
        public String TransactionId;
        public String Version;
        public String QueryStatusCodeValue;
        public String FeatureMatchingResultType;
        public String FeatureMatchingResultCount;
        public String TimeTaken;
        public InputAddress InputAddress;
        public List<OutputGeocodes> OutputGeocodes;
    }
*/
    public class OutputGeocode {
        public String Latitude;
        public String Longitude;
        public String NAACCRGISCoordinateQualityCode;
        public String NAACCRGISCoordinateQualityType;
        public String MatchScore;
        public String MatchType;
        public String FeatureMatchingResultType;
        public String FeatureMatchingResultCount;
        public String FeatureMatchingGeographyType;
        public String RegionSize;
        public String RegionSizeUnits;
        public String MatchedLocationType;
        public String ErrorMessage;
    }

    public class InputAddress {
        public String StreetAddress;
        public String City;
        public String State;
        public String Zip;
    }

    public class OutputGeocodes {
        public OutputGeocode OutputGeocode;
    }

    
    public static tamuGeoServicesStruct parse(String json) {
        return (tamuGeoServicesStruct) System.JSON.deserialize(json, tamuGeoServicesStruct.class);
    }
}