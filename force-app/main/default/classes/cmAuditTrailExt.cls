public with sharing class cmAuditTrailExt{

    
    public Case_Management_History__c[] cmAuditTrailRecords {
        get; private set;
    } 

    public Case_Management_History__c[] cmInitialAuditTrailRecords {
        get; private set;
   }     
    string cmAuditTrailID;
    public string at;
    Public boolean showHistory;
    
    public cmAuditTrailExt(ApexPages.StandardController controller){
    showHistory=false;
   
 
     
        cmAuditTrailID=  Apexpages.currentpage().getparameters().get('cmID') ; 
        at=Apexpages.currentpage().getparameters().get('at') ; 
        system.debug('at= '+at);  
    
        if(at=='initial'){
        loadcmInitialAuditTrail();
        showHistory=false;
      
        }else if(at!='initial'){
        loadcmAuditTrail();
        showHistory=true;
        }  

      
    }

    public cmAuditTrailExt(){
 
    }
  
    public void loadcmAuditTrail(){
    cmAuditTrailID=  Apexpages.currentpage().getparameters().get('cmID') ; 
    
        cmAuditTrailRecords = new Case_Management_History__c[] {};

        for (Case_Management_History__c atRecords : [
                 SELECT id, name, Changed_By__c, Changed_On__c,
                 New_Value__c,Old_Value__c,LastModifiedDate,LastModifiedBy.Name,
                 createdDate,createdBy.name
                 FROM Case_Management_History__c
                 where Case_Management__c =:cmAuditTrailID order by createdDate desc
             ]) {
             if(atRecords.New_Value__c!=null){
                  cmAuditTrailRecords.add(atRecords);
                  system.debug('cmAuditTrailRecords='+cmAuditTrailRecords);
                 }
            
        }


    }

    public void loadcmInitialAuditTrail(){
    cmAuditTrailID=  Apexpages.currentpage().getparameters().get('cmID') ; 
        cmInitialAuditTrailRecords = new Case_Management_History__c[] {};
    
         for (Case_Management_History__c atRecords : [
                 SELECT id, name, Benefit_Year_Plan_Year__c, Clinical_Update__c,
                 Clinical_Update_note__c,Case_Manager__c,Case_Status__c,
                 Case_Sub_status__c,Cost_Containment__c,Cost_Driver__c,
                 CM_Notes__c,CM_Casepac_ID__c,CM_Referral_Denied__c,
                 CM_Risk_Rating__c,Diagnosis__c,Discharge_Date__c,
                 Disease_Condition_TX_Plan_Education__c,Cost_Containment_Activity_Completion__c,
                 Coordination_of_Provider_Services__c,Education_on_Benefits_Network__c,
                 Engagement__c,Enrollment__c,Expected_Cost__c,HCC__c,Identification_Date__c,
                 MDC__c,MDC_Sub_Category__c,Referral_Source__c,Newborn__c,Next_Review_Date__c,
                 Opened__c,Patient_Coaching_Support__c,Referral_for_OON_Negotiations__c,
                 Ref_to_Other_Programs_Benefit_Services__c,softDelete__c,Steerage_to_In_Network_Provider__c,
                 LastModifiedDate,LastModifiedBy.Name,
                 createdDate,createdBy.name
                 FROM Case_Management_History__c
                 where Initial_History__c=true and Case_Management__c =:cmAuditTrailID order by createdDate desc
             ]) {
             if(atRecords.LastModifiedBy.Name!=''){
                  cmInitialAuditTrailRecords.add(atRecords);
                  system.debug('cmInitialAuditTrailRecords ='+cmInitialAuditTrailRecords );
                 }
            
        }   
   
    }
      
}