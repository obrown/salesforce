/**
* This class contains unit tests for validating the behavior of the Apex facilityEligController class
*/

@isTest
private class facilityEligControllerTest {

    static testMethod void facilityEligControllerTest() {
        
        recordType rt = [select id, name, developerName from recordType where name = 'Lowes Joint'];
        
        patient_case__c pc = new patient_case__c();
        pc.Employee_Last_Name__c='Test';
        pc.Employee_First_Name__c='Unit';
        pc.Employee_Gender__c= 'Male';
        pc.Employee_DOBe__c= '1950-01-01';
        pc.Employee_Street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c= 'AZ';
        pc.Employee_Zip_Postal_Code__c= '85212';
        pc.Employee_Mobile__c= '(480) 555-1212';
        pc.Employee_Home_Phone__c= '(480) 555-1212';
        pc.Employee_Work_Phone__c= '(480) 555-1212';
        
        pc.Patient_Last_Name__c='Test';
        pc.Patient_First_Name__c='Unit';
        pc.Patient_Gender__c= 'Male';
        pc.Patient_DOBe__c= '1950-01-01';
        pc.Patient_Street__c = '123 E Main St';
        pc.Patient_City__c = 'Mesa';
        pc.Patient_State__c= 'AZ';
        pc.Patient_Zip_Postal_Code__c= '85212';
        pc.Patient_Mobile__c= '(480) 555-1212';
        pc.Patient_Home_Phone__c= '(480) 555-1212';
        pc.Patient_Work_Phone__c= '(480) 555-1212';
        pc.Relationship_to_the_Insured__c = 'Patient is Insured';
        pc.recordtypeid = rt.id;
        pc.recordtype =rt;
        pc.client_name__c ='Lowes';
        
        
        insert pc;
        
        facilityEligController fec = new facilityEligController(pc.id);
        
        fec.mySave();
        fec.mySubmit();
        
        fec.empVerified=true;
        fec.patientVerified=true;
        fec.eligVerified=true;
        fec.mySubmit();
        
        fec.objPC.eligible__c ='Yes';
        
        fec.mySubmit();
        
        fec.elig.status__c = 'Active';
        fec.elig.Status_Effective_Date__c = date.today().addDays(1);
        fec.objPC.Effective_Date_of_Medical_Coverage__c = date.today().addDays(1);
        
        fec.mySubmit();
        
        pc.client_name__c = 'jetblue';
        update pc;
        
        fec = new facilityEligController(pc.id);
        
    }
    
}