public with sharing class cognitoTesting {

    public string mSubject { get; set; }
    public string mBody { get; set; }

    public string dashBoardjson {get; set;}
    public string contactsjson {get; set;}
    public string messagesjson {get; set;}
    public string traveljson {get; set;}
    string endPoint= 'https://connect.hdplus.com/mapi';
    public class response{
        string ResultCode;
        string Message;
    }
    string idToken;
    
    public cognitoTesting(){
        //dashBoardjson='a';
        //contactsjson='a';
        //messagesjson='a';
        //traveljson='a';
        
    }
    
    public void getJsonString(){
        
        idToken = ApexPages.CurrentPage().getParameters().get('tok');
        http h = new http();
        
        HttpRequest req = new HttpRequest ();
        req.setEndPoint(endPoint+''+'/patientstatus/');
        req.setheader('token', idToken);
        req.setmethod('POST');
        req.setTimeout(120000);
        
        try{
            HttpResponse res;
            if(test.isRunningTest()){
                res = new HttpResponse();
                res.setStatusCode(200);
            }else{
                res = h.send(req);   
            }
            dashBoardjson='';
            
            if(res.getStatusCode()==200){
                
                if(test.isRunningTest()){
                    res.setBody('{"TimelineRibbon":{"items":[{"Position":1,"Label":"Referral - 1st Step","Completed":1},{"Position":2,"Label":"Treatment Decision","Completed":1},{"Position":3,"Label":"Travel for Care","Completed":0}]},"TaskList":{"Pending":[{"sortOrder":1,"ResponsibleParty":"John Smith","label":"Complete Nicotine Testing"}],"Completed":[{"sortOrder":1,"ResponsibleParty":"HDP (3/29/2019)","label":"Referred to null"},{"sortOrder":2,"ResponsibleParty":"John Smith (3/29/2019)","label":"HDP Forms Completed"},{"sortOrder":3,"ResponsibleParty":"HDP (3/29/2019)","label":"Appointments Confirmed"},{"sortOrder":4,"ResponsibleParty":"John Smith (3/29/2019)","label":"Travel Booked with American Express"}]},"dashboard":{"WelcomeMessage":"Welcome to the Walmart Travel Surgical Program!","ToDoItemsComplete":"80%"}}');
                }
                system.debug(res.getBody());
                response response = (response)JSON.deserialize(res.getBody(), response.class);
               
                if(response.ResultCode=='0'){
                    ecenPatientInformation epi = (ecenPatientInformation)JSON.deserialize(response.Message, ecenPatientInformation.class);
                    dashBoardjson = json.serializePretty(epi);
                }else if(response.ResultCode=='1'){
                    dashBoardjson = json.serializePretty(response);
                }else{
                    dashBoardjson = response.Message;
                    
                }
                
                
            }
            
        }catch(exception e){
            system.debug(e.getMessage());
            dashBoardjson='';
        }
        
        req.setEndPoint(endPoint+''+'/contacts/');
        
        try{
            HttpResponse res;
            if(test.isRunningTest()){
                res = new HttpResponse();
                res.setStatusCode(200);
            }else{
                res = h.send(req);   
            }
            
            contactsjson='';  
            if(res.getStatusCode()==200){ 
                if(test.isRunningTest()){
                    res.setBody('{"contacts":[{"name":"Scripps","contactNumber":null}]}');
                }
                
                response response = (response)JSON.deserialize(res.getBody(), response.class);
                if(response.ResultCode=='0'){
                    ecenContactExchange epi = (ecenContactExchange)JSON.deserialize(response.Message, ecenContactExchange.class);
                    contactsjson= json.serializePretty(epi);
                }else{
                    contactsjson= 'Error '+response.Message;
                }
               
            }
            
        }catch(exception e){
            system.debug(e.getMessage());
            contactsjson='';
        }
        
        req.setEndPoint(endPoint+''+'/messages/');
        
        try{
            HttpResponse res;
            if(test.isRunningTest()){
                res = new HttpResponse();
                res.setStatusCode(200);
            }else{
                res = h.send(req);   
            }  
            
            
            if(res.getStatusCode()==200){ 
                if(test.isRunningTest()){
                    res.setBody('{"pageInfo":{"totalResults":1,"resultsPerPage":20},"messagesSent":[{"user":{"name":"Unit.Test","email":"testig@test.com"},"subject":"Unit.Test","mid":"a2VQ0000001VdZaMAK","dateSent":"3/29/2019 5:45 pm","body":"Unit.Test"}],"messages":[{"user":{"name":"Unit.Test","email":"testig@test.com"},"subject":"Unit.Test","mid":"a2VQ0000001VdZZMA0","dateReceived":"3/29/2019 5:45 pm","body":"Unit.Test"}],"message":null,"mainInfo":{"mainContact":{"phone":"330-555-1212","email":"memberadvocate@hdplus.com"},"announcement":"If this is a medical emergency please dial 911.\n\nMessages are meant as a non urgent way to contact your member advocate. If this is a matter that requires a quick response please use the \"My Contacts\" page.\n\nOtherwise your message will be answered within 2 business days."},"contactOptions":[{"name":"Member Advocate","email":"memberadvocate@hdplus.com"}]}');
                }
                
                response response = (response)JSON.deserialize(res.getBody(), response.class);
                
                if(response.ResultCode=='0'){
                    coeMobileMessaging epi = (coeMobileMessaging)JSON.deserialize(response.Message, coeMobileMessaging.class);
                    messagesjson= json.serializePretty(epi);
                    messagesjson = messagesjson.replace('|n','\\n');
                }else{
                    messagesjson= 'Error '+response.Message;
                }
                
            }
            
        }catch(exception e){
            messagesjson='';    
            system.debug(e.getMessage());
        }
        
        req.setEndPoint(endPoint+''+'/travel/');
        
        try{
            HttpResponse res;
            if(test.isRunningTest()){
                res = new HttpResponse();
                res.setStatusCode(200);
            }else{
                res = h.send(req);   
            }
            
            
            if(res.getStatusCode()==200){ 
                if(test.isRunningTest()){
                    res.setBody('{"travelType":"Flying","hotel":{"label":null,"checkout":"3/29/2019","checkin":"3/29/2019"},"facility":{"label":"Scripps","Appointments":[{"label":"Admission","displayTime":"","displayDate":"3/29/2019"},{"label":"First Appointment","displayTime":"10:30 am","displayDate":"3/29/2019"},{"label":"Surgical Procedure","displayTime":"","displayDate":"3/29/2019"},{"label":"Physician Follow-up Appointment","displayTime":"10:30 am","displayDate":"3/29/2019"},{"label":"Last Appointment","displayTime":"NA","displayDate":"3/29/2019"},{"label":"Hospital Discharge","displayTime":"","displayDate":"3/29/2019"}]},"departure":{"recordNumber":"Pending","displayTime":" 6:03 pm","displayDate":"Fri, Mar 29 2019"},"arrival":{"recordNumber":"#87489","displayTime":" 6:03 pm","displayDate":"Fri, Mar 29 2019"}}');
                }

                response response = (response)JSON.deserialize(res.getBody(), response.class);
                if(response.ResultCode=='0'){
                    ecenPatientTravelStruct epi = (ecenPatientTravelStruct)JSON.deserialize(response.Message, ecenPatientTravelStruct.class);
                    travelJson= json.serializePretty(epi);
                    
                }else{
                   travelJson= 'Error '+response.Message;
                }
                
            }else{
                
                travelJson = res.getBody();
            }
            
        }catch(exception e){
            travelJson ='';    
            system.debug(e.getLineNUmber());
            system.debug(e.getMessage());
        }
        
       
    }
    
    public void sendMessage(){
        if(idToken==null){return;}
        try{
        http h = new http();
        HttpRequest req = new HttpRequest ();
        req.setEndPoint(Url.getSalesforceBaseURL().toExternalForm()+ '/services/apexrest/ecen/v1/messages');
        
        //
        coeMobileMessaging mw= new coeMobileMessaging();
        mw.message = new coeMobileMessaging.Message(); 
        
        mw.message.User.name = userinfo.getfirstname()+' '+userinfo.getlastname();
        mw.message.User.email= userinfo.getUserEmail();
        mw.message.subject = mSubject;
        mw.message.body = mBody;
        
        string jsonString = JSON.serialize(mw);
        req.setBody(jsonstring);
        req.setEndPoint(endPoint+''+'/sendmessage/');
        req.setheader('token', idToken);
        req.setmethod('POST');
        req.setHeader('tok',idToken);
       
        req.setTimeout(120000);
        HttpResponse res = h.send(req);   
        
        if(res.getStatusCode()!=200 && res.getStatusCode()!=201){
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.warning,res.getBody()));
            return;
        }
        
        mSubject =null;
        mBody =null;
                
        apexpages.addMessage(new ApexPages.message(Apexpages.Severity.confirm,'Message Sent'));
        }catch(exception e){
            apexpages.addMessage(new ApexPages.message(Apexpages.Severity.fatal, e.getMessage()+' '+e.getLineNUmber()));    
        }
        
    }

}