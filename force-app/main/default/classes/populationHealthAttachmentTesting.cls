@isTest
private class populationHealthAttachmentTesting {

    static testMethod void populationHealthAttachmentTest() {
            
        PopHealthTestData t = new PopHealthTestData();
        t.loadData('Facebook','City of Hope');
        phm_Patient__c patient  = t.patient;
         patient = t.completePHIntake(patient); //inserts the record, populating all the required fields
         system.assert(patient.id!=null);
         ApexPages.CurrentPage().getparameters().put('patientRecordId', patient.id);
         phPatientTabsController phController = new phPatientTabsController();
         phController.addressNumber='1';
         phController.setAddress();
         
         phController.addressNumber='2';
         phController.setAddress();
         
         phController.addressNumber='3';
         phController.setAddress();
         
         phController.addressNumber='4';
         phController.setAddress();
         phController.quickSave();
         
         phController.addressNumber='1';
         phController.quickSave();
         
         phController.addressNumber='2';
         phController.quickSave();
         
         phController.addressNumber='3';
         phController.quickSave();
         phController.closeAddress();
         
         //phController.newUM();
         system.debug('umRecordTypes ' + phController.umRecordTypes[1].getValue());
         ApexPages.CurrentPage().getParameters().put('umRt', phController.umRecordTypes[1].getValue());
         phController.setUmRecordType();
         phController.um = t.completeUMInpatient(phController.um);
         phController.saveUM();    
         
         patientSearchVFController psv = new patientSearchVFController();
         psv.searchHealthpac();
         Apexpages.CurrentPage().getParameters().put('memberKey', 'MICHAELNEWBECK04/17/1949034001ABCD123456');
         psv.eligDetail();
         
         ApexPages.CurrentPage().getParameters().put('selectedFileName', 'selectedFileName.pdf');
         ApexPages.CurrentPage().getParameters().put('fileSrcBaseDir', 'Test');
         psv.setSelectedFileName();
         
         ApexPages.CurrentPage().getParameters().put('filename', 'Test.pdf');
         psv.moveFile();
         
        
    }
    
    static testMethod void utilizationManagmentUnassignedFiles(){
         utilizationManagmentUnassignedFiles umuaf = new utilizationManagmentUnassignedFiles();
         umuaf.setFileList();
         umuaf.setFileName();
         
         ApexPages.CurrentPage().getParameters().put('fn','');
         umuaf.setFileName();
         
         ApexPages.CurrentPage().getParameters().put('fn','unit test');
         umuaf.setFileName();
     }
     
     static testMethod void phPatientAttachmentsController(){
         
         PopHealthTestData t = new PopHealthTestData();
         t.loadData('Facebook','City of Hope');
         phm_Patient__c patient  = t.patient;
         patient = t.completePHIntake(patient); //inserts the record, populating all the required fields
         system.assert(patient.id!=null);
            
         
         phPatientAttachmentsController pac = new phPatientAttachmentsController();
         pac.pid=patient.id;
         pac.pageLoad();
         
         Population_Health_Attachment__c pha = new Population_Health_Attachment__c();
         pha.Patient__c = patient.id;
         pha.File_Directory__c = 'unitTest';
         pha.File_Name__c = 'unitTest';
         
         insert pha;
         
         Utilization_Management__c um = new Utilization_Management__c(Patient__c=patient.id);
         insert um;
         
         ApexPages.CurrentPage().getParameters().put('fileList', pha.id);
         pac.selectedUmforFileAssoc = um.id;
         
         pac.pageLoad();
         pac.createUMAttachments();
         
         
         
     }
    
}