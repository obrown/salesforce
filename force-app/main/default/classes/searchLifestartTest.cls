/* 

    This class contains unit tests for validating the behavior of  the Apex searchLifestart class

*/

@isTest
private class searchLifestartTest {

    static testMethod void searchLifestart() {
        
        searchLifestart ls = new searchLifestart();
        ApexPages.currentPage().getParameters().put('st', 'Sally Smith');
        ls = new searchLifestart();
        
        lifestart__c foo = new lifestart__c(patient_first_name__c='Sally', patient_last_name__c='Smith');
        insert foo;
        ApexPages.currentPage().getParameters().put('st', 'Smith');
        ls = new searchLifestart();
        
        ApexPages.currentPage().getParameters().put('uw', '008');
        ApexPages.currentPage().getParameters().put('essn', '555746607');
        
        ls.searchHealthPac();
        
        ApexPages.currentPage().getParameters().put('puw', '008');
        ApexPages.currentPage().getParameters().put('pEssn', '555746607');
        ApexPages.currentPage().getParameters().put('pSeq', '00');
        ApexPages.currentPage().getParameters().put('pgrp','');
        ApexPages.currentPage().getParameters().put('pefn',''); 
        ApexPages.currentPage().getParameters().put('peln',''); 
        
        ls.selectEmployeeAF();
        
    }
    
}