public without sharing class claimsProcessed{
    public id clientId;
    public date startDate, endDate;
    public caClaims_Processed__c cpRecord;
}