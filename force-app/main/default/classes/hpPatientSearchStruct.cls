public with sharing class hpPatientSearchStruct{

    public PatSearchResultLogs[] PatSearchResultLogs {get; set;} 
    public string ResultCode {get; set;} 
    public string ErrorReason {get; set;}
    public string ErrorMsg {get; set;}
    
    boolean getIsHDPGrpMember(){
        try{
            groupMember gm = [select group.DeveloperName from GroupMember where UserOrGroupId = :UserInfo.getUserId() and group.DeveloperName ='Group_HDP'];
            if(gm.id!=null){
                return true;
            }
        }catch(queryException e){
            return false;
        }
        return false;
    }
       
    public void cleanUpLog(){
        
        boolean isHDPGrpMember = getIsHDPGrpMember();
        
        if(this.PatSearchResultLogs !=null){
            
            PatSearchResultLogs[] finalList = new PatSearchResultLogs[]{};
            
            for(PatSearchResultLogs p : this.PatSearchResultLogs){
                
                if(p.EmpDateOfBirth != null){
                    p.EmpDateOfBirth = p.EmpDateOfBirth.trim();
                    p.EmpDateOfBirth = p.EmpDateOfBirth.mid(4,2) +'/'+ p.EmpDateOfBirth.right(2) +'/'+ p.EmpDateOfBirth.left(4);
                }
                
                if(p.PatDateOfBirth != null){
                    p.PatDateOfBirth = p.PatDateOfBirth.trim();
                    p.PatDateOfBirth = p.PatDateOfBirth.mid(4,2) +'/'+ p.PatDateOfBirth.right(2) +'/'+ p.PatDateOfBirth.left(4);
                }
                
                if(p.Grp != 'HDP' && p.Grp != '000'){
                    finalList.add(p);
                    
                }else if(isHDPGrpMember){ //add hdp employees for members for the group hdp public group
                    finalList.add(p);
                    
                }
                
                
            }
            
            this.PatSearchResultLogs = finalList.clone();
        
        }
    
    }
   
    public class PatSearchResultLogs{
    
        public string Underwriter {get; private set;}
        public string Grp {get; private set;}
        public string SSN {get; private set;}
        public string Sequence {get; private set;}
        
        public string EmpFirstName {get; private set;}
        public string EmpLastName {get; private set;}
        public string EmpDateOfBirth {get; private set;}
        
        public string EmpAddress1    {get; set;}
        public string EmpAddress2    {get; set;}
        public string EmpCity  {get; set;}
        public string EmpState {get; set;}
        public string EmpZip {get; set;}
        
        public string PatFirstName {get; private set;}
        public string PatLastName {get; private set;}
        public string PatDateOfBirth {get; private set;}
        public string Relationship {get; private set;}
        public string Gender {get; private set;}
        public string Status {get; private set;}
        public string Dept {get; private set;}
        public string MemberNumber {get; private set;}
        
        public string EligDate {get; set;}
        
        public string ClientName {get; private set;}
        public boolean Enrolled {get; set;}
        public id PatientRecordId {get; set;}
        
        /* Do not modify */
        public PatSearchResultLogs(string Underwriter,
                       string Grp,
                       string SSN,
                       string EmpFirstName,
                       string EmpLastName,
                       string EmpDateOfBirth,
                       string Sequence,
                       string PatFirstName,
                       string PatLastName,
                       string PatDateOfBirth,
                       string Relationship,
                       string gender,
                       string Status,
                       string ClientName){
                                             
            this.Underwriter = Underwriter;
            this.Grp = Grp;
            this.SSN = SSN;      
            this.EmpFirstName= EmpFirstName;
            this.EmpLastName= EmpLastName;
            this.EmpDateOfBirth= EmpDateOfBirth;
            this.Sequence= Sequence;
            this.PatFirstName= PatFirstName;
            this.PatLastName= PatLastName;
            this.PatDateOfBirth= PatDateOfBirth;  
            this.Relationship= Relationship;
            this.Gender= gender;
            this.Status= Status;
            this.ClientName = ClientName;
        }
        /* End DNM */
        
        public PatSearchResultLogs(string Underwriter,
                       string Grp,
                       string SSN,
                       string EmpFirstName,
                       string EmpLastName,
                       string EmpDateOfBirth,
                       string EmpAddress1,
                       string EmpAddress2,
                       string EmpCity,
                       string EmpState,
                       string EmpZip,
                       string Sequence,
                       string PatFirstName,
                       string PatLastName,
                       string PatDateOfBirth,
                       string Relationship,
                       string gender,
                       string Status,
                       string Dept,
                       //string EligDate,
                       string MemberNumber,
                       string ClientName){
                                             
            this.Underwriter = Underwriter;
            this.Grp = Grp;
            this.SSN = SSN;      
            this.EmpFirstName= EmpFirstName;
            this.EmpLastName= EmpLastName;
            this.EmpDateOfBirth= EmpDateOfBirth;
            this.EmpAddress1= EmpAddress1;
            this.EmpAddress2= EmpAddress2;
            this.EmpCity= EmpCity;
            this.EmpState= EmpState;
            this.EmpZip= EmpZip;
            this.Sequence= Sequence;
            this.PatFirstName= PatFirstName;
            this.PatLastName= PatLastName;
            this.PatDateOfBirth= PatDateOfBirth;  
            this.Relationship= Relationship;
            this.Gender= gender;
            this.Status= Status;
            this.MemberNumber= MemberNumber;
            this.Dept= Dept;
            //this.EligDate=EligDate;
            this.ClientName = ClientName;
        }
        
        public PatSearchResultLogs(){}
    
    }
    

}