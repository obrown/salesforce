/**
* This class contains unit tests for validating the behavior of the Appeal Log application
*/
@isTest()
private class fixNullClientsTest {
    
  static testMethod void TestfixNullClients() {
      fixNullClients.fixNullClients();
      
      Test.startTest();
      fixNullClientsSchedule sh1 = new fixNullClientsSchedule();
      String sch = '0 0 23 * * ?'; 
      system.schedule('Test null clients', sch, sh1); 
      Test.stopTest();
  }
  

}