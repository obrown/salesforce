public with sharing class AonHewittEligCheck{
    
    public string searchID;
    public string subjectType;
    string clientID = '03979';
    public dom.Document response;
    public string xml; //used for testing
    
    public void run(){
        string endpoint = 'https://services.alight.com/AHServiceGatewayPU/services/alight/HealthDesignPlus';
        //string endpoint = 'https://services.qc.alight.com/AHServiceGatewayQC/services/alight/HealthDesignPlus';
        //string endpoint = 'https://two.sse.hewitt.com/AHServiceGatewayPU/services/HealthDesignPlus';
        
        string guid = string.valueof(date.today());
        guid = guid.replaceAll('-','');
         
        string x509id = 'A' + string.valueof(math.abs(crypto.getRandomLong()));
        string bodyID = string.valueof(math.abs(crypto.getRandomLong()));
        
        guid = 'hdp' + guid + '-' + string.valueOf(bodyId);
        ExternalServicesCallLog__c esc = new ExternalServicesCallLog__c(name = guid);
        
        string xmlRequest = ''; //'<?xml version="1.0" encoding="UTF-8"?>\n';
        xmlRequest += '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v2="http://alight.com/hro/benefits/cm/xsd/v2_0" xmlns:xsd="http://alight.com/hro/benefits/healthDesignPlus/xsd">\n   ';
        
        //This body of the request
        string docBody = '\n      <xsd:getMedicalCoverage>\n         <xsd:brokerHeader>\n            <v2:brokerUserId>HealthDesignPlus</v2:brokerUserId>\n            <v2:clientId>' + clientID + '</v2:clientId>\n            \n            <v2:locale>en_US</v2:locale>\n            <v2:callerRefId>' + guid + '</v2:callerRefId>\n            \n            <v2:subject>\n               <v2:subjectId>';
        docBody += searchID + '</v2:subjectId>\n               <v2:subjectType>' + subjectType + '</v2:subjectType>\n            </v2:subject>\n         </xsd:brokerHeader>\n      </xsd:getMedicalCoverage>\n   </soapenv:Body>';
        
        xmlRequest += '<soapenv:Header>';
        xmlRequest += '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        xmlRequest += '<wsse:BinarySecurityToken EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary" ValueType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3" wsu:Id="X509-' + x509Id + '">';
        
        //This is the public cert that matches the private key were are using to sign this request, ASGWebservices
        xmlRequest += 'MIIGdTCCBF2gAwIBAgIOAWyqkQFTAAAAAFr8LHkwDQYJKoZIhvcNAQELBQAwfzEXMBUGA1UEAwwOQVNHV2Vic2VydmljZXMxGDAWBgNVBAsMDzAwREEwMDAwMDAwZ0VFbzEXMBUGA1UECgwOU2FsZXNmb3JjZS5jb20xFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xCzAJBgNVBAgMAkNBMQwwCgYDVQQGEwNVU0EwHhcNMTkwODE5MTU0ODQ1WhcNMjEwODE5MTIwMDAwWjB/MRcwFQYDVQQDDA5BU0dXZWJzZXJ2aWNlczEYMBYGA1UECwwPMDBEQTAwMDAwMDBnRUVvMRcwFQYDVQQKDA5TYWxlc2ZvcmNlLmNvbTEWMBQGA1UEBwwNU2FuIEZyYW5jaXNjbzELMAkGA1UECAwCQ0ExDDAKBgNVBAYTA1VTQTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAIdk08TEUjlQhM3hBam6OQguQw4yoPX5WsXupYwy87NiI4mvz6lGsR6ovPDRVoKyQdak9sgcMQ226F2oNaFyTZR91qA43V1ReuaNd4yfo9Ls+BNylIXrZXaJY6rI7BeXSQxzHYzsFrNdNHWIPcS8VtxUlWUGhRzNpV57EfPVWlw6Cews4mFOqmF1Wt/eeKLtvfMHfMo3NJ4kVrtHx+QxX9FN1E7mecmi9uYm3S4t9OVvFnNdsnB1MEOorZcLdjQksT7N554gJoZ+ntbG7URo11wd3sxoOrTmnaVR/tsvCvj+ddRHeernREGu2C9PO1exQRSNRQUslhOb9gHBBrYTMDfmI66JcATEAkjx1E+C6xIeSyadq0DrYZ1mfILjED6X3eQAqMjUKYNCU/WHA3F67QTdGtW0LouGEYq+2V8pQ6IoqfTnknt5DtBd++uFZPz3FpSpriDrpODiuV+dxpIDDeYWOpc1z+36Bc2U4s3WV9tEorcHGx3aNvFR3mCD2l7zFCchh9RYRJcqLB7HSb95z6QUIp5vyTqmadp98Jez0izZgNqRGoI5Gs+ZzFQVYieFM3obtaC/5b2pMXmtzuHN5aj6VSlUWVPT+d9BpKptgwq5aG9WbVNvriSzOZvmAHwsKQ4YI7+1yaONyVYEnCStuZKQVRz6ZjuxMB6TWrtop19DAgMBAAGjge4wgeswHQYDVR0OBBYEFNQvauWRsObaC6OqeKV5/2gLVqeRMA8GA1UdEwEB/wQFMAMBAf8wgbgGA1UdIwSBsDCBrYAU1C9q5ZGw5toLo6p4pXn/aAtWp5GhgYSkgYEwfzEXMBUGA1UEAwwOQVNHV2Vic2VydmljZXMxGDAWBgNVBAsMDzAwREEwMDAwMDAwZ0VFbzEXMBUGA1UECgwOU2FsZXNmb3JjZS5jb20xFjAUBgNVBAcMDVNhbiBGcmFuY2lzY28xCzAJBgNVBAgMAkNBMQwwCgYDVQQGEwNVU0GCDgFsqpEBUwAAAABa/Cx5MA0GCSqGSIb3DQEBCwUAA4ICAQBH41eozg04KsOe8tuCz4pPgvY5U08aTkbBfQoN9SRHIRlScP1C7SSeNEeInnri/tOa1e3nWvz3DL2Jqu5NwGH/CeUrIiMEu1i4iSHhlMEYj7rrzYtaHpQ5oXeSCIh0ho+5jpHCWpUJSrM2eTAdcBYGBvXVNImlMH6BRy9x87VXY1AGD07LQmbYyI8EJEh4LPWsODdZjirCEqNpPE5Yg+GyiySq1aKSfUFd11tWw0Tn5BXhnwVUqOIW1uKAbdI41VQ/HjFJBRNfAnOE4wP1+sw8nFjT+4ZVyy9G7UfGQ91OoVJwK2z0ymcQPUBvlXtMSbRqOiV57TwqLLJKucqFqUtaJoNyZmXvij5qmZZeFM6TJwRUcVinz24881vso05+F5ZVcBWmUKNq4gsYFlj4XQg/NWr0B0ZKKxee6shVxQRXELcQXQEj2rtnW3c2V8UwMw7XJNQHUDU47pTi+LcWEsYf4nRHnkV9++UVDCXvcKGSI4ES4Pu+4s0PLIwdZujBxVu/wIVWGY/CUgRFES8fwooCHYH47B/mXALZNwu2e5kZJ1O0S7UqUE8k7RZdl2Gwa1OkjCwG71oAcRttMNwNc6CZ4syzw+GctvOD8AEv9PBViQW5cyqXp55+JmugAi47rlBZO0L6CuxWwPbKaeASsLsQaR600L/XoFYUa9XO3oDFWA==';        
        xmlRequest += '</wsse:BinarySecurityToken>';                
        
        //Enter signature        
        xmlRequest += XMLSignature.generateXMLSignature(docBody, x509id, bodyId);
        
        xmlRequest += '</wsse:Security>';
        xmlRequest += '</soapenv:Header>\n';
        
        xmlRequest += '   <soapenv:Body wsu:Id="id-' + bodyID + '" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">';
        xmlRequest += docBody;
        
        xmlRequest += '\n</soapenv:Envelope>';
        
        HttpRequest req = new HttpRequest();
        req.setEndpoint(endpoint);

        req.setMethod('POST');
        system.debug(xmlRequest);
        req.setBody(xmlRequest);
        req.setClientCertificateName('ASGWebservices');
        req.setHeader('Content-Type', 'text/xml');
        req.setCompressed(true); 
        Http http = new Http();
        
        HttpResponse res = new HttpResponse();
        
        if(Test.isRunningTest()){
            Dom.Document doc = new Dom.Document();
            xml='<?xml version=\'1.0\' encoding=\'utf-8\'?><soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"><soapenv:Header><wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" soapenv:mustUnderstand="1"><wsu:Timestamp wsu:Id="TS-539983"><wsu:Created>2018-03-22T03:35:50.065Z</wsu:Created><wsu:Expires>2018-03-22T03:40:50.065Z</wsu:Expires></wsu:Timestamp><wsse:BinarySecurityToken EncodingType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-soap-message-security-1.0#Base64Binary" ValueType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3" wsu:Id="X509-A264187DD966D047501521689750066807528">MIIG9DCCBdygAwIBAgIQL64VmuxCvQbmRddmZ0+L9jANBgkqhkiG9w0BAQsFADCBhDELMAkGA1UEBhMCVVMxHTAbBgNVBAoTFFN5bWFudGVjIENvcnBvcmF0aW9uMR8wHQYDVQQLExZTeW1hbnRlYyBUcnVzdCBOZXR3b3JrMTUwMwYDVQQDEyxTeW1hbnRlYyBDbGFzcyAzIFNlY3VyZSBTZXJ2ZXIgU0hBMjU2IFNTTCBDQTAeFw0xNjAzMDQwMDAwMDBaFw0xOTAzMDQyMzU5NTlaMIGCMQswCQYDVQQGEwJVUzERMA8GA1UECAwISWxsaW5vaXMxFTATBgNVBAcMDExpbmNvbG5zaGlyZTEYMBYGA1UECgwPQW9uIENvcnBvcmF0aW9uMRQwEgYDVQQLDAtCZW5lZml0cyBJVDEZMBcGA1UEAwwQdGJhd3MuaGV3aXR0LmNvbTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAL1XvdYMGVHRlkzr2qqSqXnCE3x0LGqE4KrPmwVMP3HLcFuq3GpMZZvpZJdhGNuCMrEZz5Mt2BVt0fpbn8TI1JOoaamFiRImtfBiy5eOoag5z2mjYOsr0GxA0Aon9Lb4zktaPwQuz4F8rJ3Whi/BwQkGS0XGNCoij3Y/WOOWszD2EBRwR0RR0RduZK0+GH6Jwtu8RPRhaJj2nkAMEu3VtXMLMHLriPBPpyg1k1TRVr0eTT7Ybtye6cHzt71yrKQFQEnFHgQxnGFUXGDXlymu707XxWyxuaf3tst5nJ6uCBLhScGMxd2hRrGU+6bU/U3TXGCKKV+Z3lA4cPKoGLMEF9MCAwEAAaOCA2AwggNcMBsGA1UdEQQUMBKCEHRiYXdzLmhld2l0dC5jb20wCQYDVR0TBAIwADAOBgNVHQ8BAf8EBAMCBaAwHQYDVR0lBBYwFAYIKwYBBQUHAwEGCCsGAQUFBwMCMGEGA1UdIARaMFgwVgYGZ4EMAQICMEwwIwYIKwYBBQUHAgEWF2h0dHBzOi8vZC5zeW1jYi5jb20vY3BzMCUGCCsGAQUFBwICMBkaF2h0dHBzOi8vZC5zeW1jYi5jb20vcnBhMB8GA1UdIwQYMBaAFNtiIPt9Aol80jtvx+QybAVSHa2xMCsGA1UdHwQkMCIwIKAeoByGGmh0dHA6Ly9zZy5zeW1jYi5jb20vc2cuY3JsMFcGCCsGAQUFBwEBBEswSTAfBggrBgEFBQcwAYYTaHR0cDovL3NnLnN5bWNkLmNvbTAmBggrBgEFBQcwAoYaaHR0cDovL3NnLnN5bWNiLmNvbS9zZy5jcnQwggH3BgorBgEEAdZ5AgQCBIIB5wSCAeMB4QB2AN3rHSt6DU+mIIuBrYFocH4ujp0B1VyIjT0RxM227L7MAAABU0A/lFYAAAQDAEcwRQIgaR5WpfuHRO5w12gva9cdR4RoTeyk28zdNETOOVPOINcCIQD8og0J8da9qt6GlfTXaUiAhKBeqH7gEr+wVYSvrFVrJAB2AKS5CZC0GFgUh7sTosxncAo8NZgE+RvfuON3zQ7IDdwQAAABU0A/lHIAAAQDAEcwRQIhANZUMU7edzYlUGAlU5r/pFqebJCwoAlb56c/bBf92SK+AiBY58J5R/U2zaoBPsLTbBUY7BqL50AjZB1rrUUjQViCkQB2AGj2mPgfZIK+OozuuSgdTPxxUV1nk9RE0QpnrLtPT/vEAAABU0A/lHMAAAQDAEcwRQIgMu4PDUryX8n3fZ6CnhIGyOLHDRNWcJSQZUhtNzeLYaUCIQD7uqWDD1YMPpjHxwYAcgGjGfcVl+m8Gd8HETM8aj64KgB3AO5Lvbd1zmC64UJpH6vhnmajD35fsHLYgwDEe4l6qP3LAAABU0A/llEAAAQDAEgwRgIhAKJD2kpqEf4auS01l5XghZiMStf6aOnQK1BGlHx/0O7HAiEAiguS19mU+ZoAvhJhnK8O7y9geLj7nq1DOoAf92T1IBUwDQYJKoZIhvcNAQELBQADggEBAEuGCZW9Bq29C5GKq2ZHBsmM5LKvyGPxbSmN0RMKXVYnDkSQK1jE2XhAJJCNDIbB4gY0KrOQ8J+Ucbo9AAnOHoMtubQFcj9zrvQYx71vvRj0qVvJ7HWYzw3iyLEJKiepngWOxOC8x9UcGyyn4pJx428cnUd+VXlhzSSb/i0qILKXuVHZkiDrhXXYPlt/zAF1PdXKb+HtrlvWvtuMEZPIm8cg4CtS7H97diAs/z8Bw/bbPT2rfJJ6hbVhKBNPvUMGR+VwJD8Zo1LMJgdaJaaHEbk2ve65zuJZWJrpnf+PU++vezz9tt0nJAm65zr9eWrAaelGjCTwErC2rXojmTDTjAw=</wsse:BinarySecurityToken><ds:Signature xmlns:ds="http://www.w3.org/2000/09/xmldsig#" Id="SIG-539984"><ds:SignedInfo><ds:CanonicalizationMethod Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" /><ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256" /><ds:Reference URI="#Id-1624989915"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" /></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256" /><ds:DigestValue>wzlxSLCzuigJRpJNDQREjcmJAorqGEt4vseqF2mjsOw=</ds:DigestValue></ds:Reference><ds:Reference URI="#TS-539983"><ds:Transforms><ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#" /></ds:Transforms><ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256" /><ds:DigestValue>dUXWK6HKtJWRFFxi0K8ei7KA62A8HxSYirqwij6gamY=</ds:DigestValue></ds:Reference></ds:SignedInfo><ds:SignatureValue>oZibANQjKkUg71c5eyXqcjtm4f+rSpHXw75SfJcijWozq6W7xsDqzEPRbdCCRz9fqZkSlP9R44bmTEI58FFduDyxEHIou8h1GozLpB6oP4tINr2tJb2do8H5NFxPNRxRNGQMGEqI2tYVjhy/A8m07G3c4+MGBstY9Oc9Q2no93feNNkp2GFkPNVXIvhIdzOxBHYu6aYxK+pn9U/uUvAGCIK7R/lH7ysWGYKpoCpPyHP1B7huJm3tHM352xNCFbZbayB3ynR3AFRghvD/N6oZ1XaKhCg/a8IrunRoeacK8mk0fL/RSf9GKBH+nyk1El9jKwTuSF58ER36++YInU0LIg==</ds:SignatureValue><ds:KeyInfo Id="KI-A264187DD966D047501521689750066807529"><wsse:SecurityTokenReference wsu:Id="STR-A264187DD966D047501521689750066807530"><wsse:Reference URI="#X509-A264187DD966D047501521689750066807528" ValueType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3" /></wsse:SecurityTokenReference></ds:KeyInfo></ds:Signature></wsse:Security></soapenv:Header><soapenv:Body xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" wsu:Id="Id-1624989915"><ns2:getMedicalCoverageResponse xmlns:ns2="http://alight.com/hro/benefits/healthDesignPlus/xsd"><ns2:responseHeader><ns1:systemTicketArray xmlns:ns1="http://alight.com/hro/benefits/cm/xsd/v2_0"><ns1:systemTicket><ns1:key>UDP</ns1:key><ns1:value>594e92c411a4b930dbc9b324</ns1:value></ns1:systemTicket></ns1:systemTicketArray><ns1:responseCode xmlns:ns1="http://alight.com/hro/benefits/cm/xsd/v2_0">0</ns1:responseCode><ns1:responseDescription xmlns:ns1="http://alight.com/hro/benefits/cm/xsd/v2_0">SUCCESS</ns1:responseDescription></ns2:responseHeader><ns2:responseData><ns2:personId>213154174</ns2:personId><ns2:personTypeCode>rkp</ns2:personTypeCode><ns2:personBasicData><ns2:firstName>Douglas</ns2:firstName><ns2:middleName>E.</ns2:middleName><ns2:lastName>Dean</ns2:lastName><ns2:suffix /><ns2:birthDate>1976-06-01</ns2:birthDate><ns2:sex>M</ns2:sex><ns2:employeeId>000035683</ns2:employeeId><ns2:employmentStatusCode>Active</ns2:employmentStatusCode><ns2:employmentStatusDescription>Active</ns2:employmentStatusDescription></ns2:personBasicData><ns2:healthBenefitsCurrentCoverage><ns2:planId>3500</ns2:planId><ns2:planBrandCode>MDCL</ns2:planBrandCode><ns2:planLongDescriptionText>Medical</ns2:planLongDescriptionText><ns2:optionId>13501</ns2:optionId><ns2:optionLongDescriptionText>BCBS Choice Account Plus</ns2:optionLongDescriptionText><ns2:coverageCategoryId>240</ns2:coverageCategoryId><ns2:coverageCategoryLongDescriptionText>You + Family</ns2:coverageCategoryLongDescriptionText><ns2:participantCoverageCode>true</ns2:participantCoverageCode><ns2:coveredDependentArray><ns2:coveredDependent><ns2:firstName>NICOLE</ns2:firstName><ns2:middleName /><ns2:lastName /><ns2:suffix /><ns2:globalId /></ns2:coveredDependent><ns2:coveredDependent><ns2:firstName>REBECCA</ns2:firstName><ns2:middleName /><ns2:lastName /><ns2:suffix /><ns2:globalId /></ns2:coveredDependent><ns2:coveredDependent><ns2:firstName>CALEB</ns2:firstName><ns2:middleName /><ns2:lastName /><ns2:suffix /><ns2:globalId /></ns2:coveredDependent></ns2:coveredDependentArray></ns2:healthBenefitsCurrentCoverage></ns2:responseData></ns2:getMedicalCoverageResponse></soapenv:Body></soapenv:Envelope>';
            res.setBody(xml);
            doc.load(xml);
            
        }else{
            res = http.send(req);
        
        } 
        
        
        string resBody = res.getBody();
        system.debug(' resBody  '+resBody);
        esc.ResponseStatus__c = res.getStatus();
        esc.ResponseStatusCode__c = string.valueof(res.getStatusCode());
        esc.endpoint__c = endpoint;
        esc.ResponseBody__c = 'Successful call, body not recorded';
        
        if(esc.ResponseStatusCode__c=='500' || esc.ResponseStatusCode__c=='404'){
            esc.ResponseBody__c = resBody;
            response = null;
            return;
        }
        
        
        response = res.getBodyDocument();
        
       
        
       // insert esc;
        
        
    }

}