public with sharing class directBillingInvoiceStruct{

    public Direct_billing_leave__c[]   leaves {get; private set;}
    public Direct_Billing_Invoice__c[] invoices {get; private set;}
    
    public directBillingInvoiceStruct(Direct_Billing_Invoice__c[] invoices, Direct_billing_leave__c[] leaves){
        this.leaves = leaves ;
        this.invoices = invoices;
    }
    
}