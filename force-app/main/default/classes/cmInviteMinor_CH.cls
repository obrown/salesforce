public with sharing class cmInviteMinor_CH{
    
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);

        string enrollmentEnclosurePath;
        string inviteEnclosureAPath;
        string inviteEnclosureBPath;

        //7-13-22 updates ----------------start---------------------------
        string inviteEnclosureCHletsGoPath;
        string inviteERVisitsPath;
        string inviteUCVisitsPath;
       //7-13-22 updates ----------------end---------------------------

        StaticResource logo;
        //7-13-22 added new static resources-chLetsGoLogo,erVisits
        StaticResource[] srList = [SELECT Id,NamespacePrefix,SystemModstamp,Name FROM StaticResource WHERE Name ='inviteEnclosureA' or name ='inviteEnclosureB' or name='enrollmentAppEnclosure_rev' or name='chLetsGoLogo' or name='erVisits' or name='ucVisits' order by Name asc];
        logo=srList[0];
        
        String prefix = logo.NamespacePrefix;
        if( String.isEmpty(prefix) ) {
            prefix = '';
        }else{
            //If has NamespacePrefix
            prefix += '__';
        } 
        
        enrollmentEnclosurePath='/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'enrollmentAppEnclosure_rev';//added 2-17-22 to set pull in the EnclosureA
        inviteEnclosureAPath='/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'inviteEnclosureA';//added 1-25-21 to set pull in the EnclosureA
        inviteEnclosureBPath='/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'inviteEnclosureB';//added 1-25-21 to set pull in the EnclosureB        
        //2-28-22 added end--------------------------------------------

        //7-13-22 update-------------start----------------------------------------------------------------------------------------------------
        inviteEnclosureCHletsGoPath='/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'chLetsGoLogo';//added 7-12-22 to set pull in the chLetsGoLogo
        inviteERVisitsPath='/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'erVisits';//added 7-12-22 to set pull in the erVisits image
        inviteUCVisitsPath='/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'ucVisits';//added 7-12-22 to set pull in the ucVisits image
        //7-13-22 update-------------end------------------------------------------------------------------------------------------------------
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=cm.patient__r.Patient_First_Name__c+' '+cm.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Address__c: +'<br/>';
        letterText+='<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.City__c+', ' : ''+'&nbsp';
        letterText+=hasPatientAddress ? cm.patient__r.State__c+' ' : ''+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Zip__c : ''+'<br/>';
        letterText+='<br/>';

        letterText+='</div>';
        letterText+='<p>';
        letterText+='RE:&nbsp;'+cm.patient__r.Patient_First_Name__c+'&nbsp;'+cm.patient__r.Patient_Last_Name__c+'<br/><br/>';
        letterText+='Dear Parent/Guardian of&nbsp;'+cm.patient__r.Patient_First_Name__c+',';//'&nbsp;'+cm.patient__r.Patient_Last_Name__c+',';
        letterText+='</p>';
        letterText+='<p>';  
        letterText+='Contigo Health, LLC and '+cm.patient__r.Patient_Employer__r.Legal_Name__c+ ' are committed to supporting members with their health management by providing a variety of health and wellness programs. Our Case Management program is';
        letterText+=' a telephonic education and support program that is voluntary and available at no additional cost to the member or the member’s eligible dependents.';
        letterText+='</p>';

        letterText+='<p>';  
        letterText+='I am available to assist you and your covered dependents by providing ongoing support and education to help you make informed health care decisions. I do not take the place of your healthcare provider(s) ';
        letterText+=' but am available to work with your healthcare provider(s) to coordinate your healthcare services and help you make the most of your healthcare benefits.';
        letterText+='</p>';

        letterText+='<p>';  
        letterText+='I look forward to talking with you soon. Please feel free to call me at 1-877-891-2690, Monday-Friday from 8:30AM – 5:00PM Eastern Time. If I am not able to take your call,';
        letterText+=' please leave a message on my confidential voicemail and I will return your call as soon as possible.';
        letterText+='</p>';         

        letterText+='<p>';  
        letterText+='Sincerely,<br/><br/>';
        letterText+='Care Management Department<br/>';
        letterText+='Contigo Health<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';
        letterText+='</p>'; 

        letterText+='<p>';
        letterText+='Enclosures';
        letterText+='</p>';
        
        //push enclosure to the next page -----------------------------------start---------------------------------------
        letterText+='<p>';
        letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>'; 
        //push enclosure to the next page -----------------------------------end---------------------------------------
    

        //7-13-22 update ------------------------------------------start------------------------------------------------------------------
        letterText+='<div><img src="'+inviteEnclosureCHletsGoPath+'" style="width:290px;margin-left:2px"/></div><br/>';// pulls in the image of the chLetsGo logo
        letterText+='<p>'; 
        letterText+='<b><u>Instructions to downloading the Contigo Health™ App</u></b><br/><br/>';
        letterText+='<b><u>Download the App</u></b><br/>';
        letterText+='1. To Install the app, go to the App Store on your mobile device, and search Contigo Health (be sure to NOT select Contigo Health FSA.)<br/>';
        letterText+='2. Install the app.<br/>';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='<b><u>App Registration</u></b><br/>';
        letterText+='1. Once installed, you will need to register for the first time. Select the "Register" button on the log in screen.<br/>';
        letterText+='2. You will be re-directed to the registration screen.<br/>';
        letterText+='3. Enter the information requested.<br/>';
        letterText+='4. Once you have entered all the required information, click the "Register" button.<br/>';
        letterText+='5. Once you click the "Register" button and all of your information is accepted, you will be re-directed to a confirmation page.<br/>';
        letterText+='6. An email will be sent to you containing the confirmation code to complete registration.<br/>';
        letterText+='7. Enter the confirmation code and click the "Confirm" button.<br/><br/>';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='<b><u>Logging In</u></b><br/>';
        letterText+='1) Once you are confirmed, you can now log into the app.<br/>';
        letterText+='2) Enter the email address you registered with as well as the password you created during the registration process.<br/>';
        letterText+='3) Click "Log-in".<br/><br/>';
        letterText+='<b>You are now ready to use the Contigo Health™ App-</b><br/>';

        letterText+='<div style="display:inline-block;margin-left:155px;">';
        letterText+='<b>Let\'s Go!</b><br/>';
        letterText+='</div>'; 
        letterText+='<br/><br/>';
        
        letterText+='The Contigo Health App provides an interactive experience! Your dashboard provides up to date information,<br/> ';
        letterText+='reminders or outstanding task, and the app provides you the opportunity to securely message your<br/> ';
        letterText+='Contigo Health™ Case Manager or Disease Manager direct!<br/><br/>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';   

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        
        letterText+='<p>';
        letterText+='<div style="font-size: 10px;">';
        letterText+='<b>@2020, ALL RIGHTS RESERVED, CONTIGO HEALTH LLC PUBLISHED:DECEMBER 2020.<br/>';
        letterText+='To access online or mobile app services, terms and conditions apply.</b>';
        letterText+='</div>';
        letterText+='</p>';
        
        //7-13-22 added to push enclosure to the next page -----------------------------------start---------------------------------------
        //letterText+='<p>';
        //letterText+=' *</br></br>';
        //letterText+='</p>';

        //letterText+='<p>';
        //letterText+=' *</br></br>';
        //letterText+='</p>';
        
        //letterText+='<p>';
        //letterText+=' *</br></br>';
        //letterText+='</p>';
                             
        //7-13-22 added to push enclosure to the next page -----------------------------------end--------------------------------------- 
        //letterText+='<div><img src="'+inviteERVisitsPath+'" style="width:375px;margin-left:2px"/></div><br/>';// pulls in the image of the chLetsGo logo---ok to remove per Trista 7-19-22
        /*
        letterText+='<p>';
        letterText+='<b><h1>Important Information to Know About Emergency Room Visits </h1></b> ';
        letterText+='<b><u>If you feel you are experiencing a life-threatening emergency, call 911 immediately</u></b>';
        letterText+='</p>';
        */
        letterText+='<div>';
        letterText+='<b><h3>Important Information to Know About Emergency Room Visits </h3></b>';
        letterText+='<b><u>If you feel you are experiencing a life-threatening emergency, call 911 immediately</u></b></br>';        
        letterText+='A number of medical conditions can be considered medical emergencies because they</br>';
        letterText+='require rapid diagnosis and treatment that are only available in a hospital setting.</br>';
        letterText+='Symptoms evaluated in an emergency room include but are not limited to:';
        letterText+='</div>';

        letterText+='<p>';
        letterText+='• Persistent chest pain, especially if it radiates to your arm or jaw or is accompanied by</br> ';
        letterText+='&nbsp&nbspsweating,vomiting or shortness of breath</br>';
        letterText+='• Difficulty breathing</br> ';
        letterText+='• Vaginal bleeding during pregnancy</br>';
        letterText+='• Severe and persistent vomiting or diarrhea</br>';
        letterText+='• Serious burns</br>';
        letterText+='• Seizures without a previous diagnosis of epilepsy</br>';
        letterText+='• Severe pain, particularly in the abdomen or </br>';
        letterText+='&nbsp&nbspstarting halfway down the back</br>';
        letterText+='• Sudden clumsiness, loss of balance or fainting</br>';
        letterText+='• Sudden difficulty speaking, or trouble understanding </br>';
        letterText+=' &nbsp&nbspspeech</br>';
        letterText+='• Altered mental status or confusion, including suicidal</br>';
        letterText+='&nbsp&nbspthoughts</br>';        
        letterText+='• Sudden weakness or paralysis, especially on one side</br>';
        letterText+='&nbsp&nbspof the face or body</br>'; 
        letterText+='• Severe heart palpitations </br>';
        letterText+='• Sudden, severe headache</br>';
        letterText+='• Sudden testicular pain and swelling</br>';
        letterText+='• Newborn baby with a fever (a baby less than three</br>';
        letterText+='&nbsp&nbspmonths old with a temperature of 100.4 degrees or </br>';
        letterText+='&nbsp&nbsphigher needs to be seen right away)</br>'; 
        letterText+='• Falls that cause injury or occur while taking blood</br>';
        letterText+='&nbsp&nbspthinning medications</br>';
        letterText+='• Sudden vision changes, including blurred or double</br>';
        letterText+='&nbsp&nbspvision and full or partial vision loss</br>';
        letterText+='• Broken bones or dislocated joints</br>';
        letterText+='• Deep cuts that require stitches, especially on the face,</br>';
        letterText+='&nbsp&nbspor a large open wound that won’t stop bleeding</br>';
        letterText+='• Head or eye injuries</br>';
        letterText+='• Severe flu or cold symptoms</br>';
        letterText+='• High fevers or fevers with rash </br>';    
        letterText+='</p>';

        letterText+='<p>';
        letterText+='<b><u>What resources are available to me? </u></b></br> ';
        letterText+='When contacting 911, a representative will ask specific questions to</br>';
        letterText+='help determine the medical intensity or needs of your situation. A</br>';
        letterText+='medical transport team may be dispatched to take you to the emergency</br>';
        letterText+='room or hospital, or you may be given further instructions to follow.</br>';
        letterText+='Another resource is your primary care physician (PCP). Your PCP is</br>';
        letterText+='your health advocate and has your medical history to help determine</br>';
        letterText+='your need for emergent care. It’s a good idea to keep your PCP’s</br>';
        letterText+='phone number in your wallet or at home for convenience. Many PCPs</br>';
        letterText+=' offer same-day appointments and telephone advice as well.';
        letterText+='</p>'; 

        letterText+='<p>';
        letterText+='<div style="font-size: 10px;">';
        letterText+='<b>@2020, ALL RIGHTS RESERVED, CONTIGO HEALTH LLC PUBLISHED:DECEMBER 2020.<br/>';
        letterText+='To access online or mobile app services, terms and conditions apply.</b><br/>';
        letterText+='</div>';
        letterText+='</p>';

        //letterText+='<p>';
        //letterText+=' *</br></br>';
        //letterText+='</p>';
        
        //letterText+='<p>';
        //letterText+=' *</br></br>';
        //letterText+='</p>';
        
        letterText+='<div>';
        letterText+='<b><h3>Important Information to Know About Emergency Room Visits-Continued </h3></b>';
        //letterText+='<b><u>If you feel you are experiencing a life-threatening emergency, call 911 immediately</u></b> ';
        letterText+='</div>';

        letterText+='<p>';
        //letterText+='</br>';
        letterText+='<b><u>What to Expect Using an Emergency Room for Emergent Needs</u></b></br> ';
        letterText+='If you use an emergency room for emergent needs, designated medical</br>';
        letterText+='professionals will evaluate, diagnose, and stabilize your condition.</br>';
        letterText+='These professionals develop a plan of care to treat your symptoms. You</br>';
        letterText+='may be hospitalized or provided instructions for further follow-up</br>';
        letterText+='treatment with your PCP or Specialist.</br>';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='<b><u>What to Expect Using an Emergency Room for Non-Emergent Needs</u></b></br> ';
        letterText+='If you choose to use an emergency room for non-emergent needs, expect</br>';
        letterText+='designated medical professionals to assess and determine your level of</br>';
        letterText+='urgency. Patients with the most urgent needs will be cared for first. Also,</br>';
        letterText+='while an emergency care team has expertise in providing emergency care,</br>';
        letterText+='they don’t have experience with your personal health history. There may</br>';
        letterText+='be disruption in your current plan of care provided by your PCP.</br>';
        letterText+='</p>';        

        letterText+='<p>';
        letterText+='<b><u>Important Considerations When Using an Emergency Room </u></b></br> ';
        letterText+='When choosing an emergency room versus an urgent care facility, there</br>';
        letterText+='are important items to evaluate. Consider the cost involved. Your</br>';
        letterText+='out-of- pocket expenses to use an emergency room will be higher than</br>';
        letterText+='using an urgent care facility. Also, to the extent possible, be prepared</br>';
        letterText+='and bring your medical ID card, share current diagnosed medical conditions,</br>';
        letterText+='a list of your current medications, current immunization history, and a list</br>';
        letterText+='of your allergies. Your primary care doctor may have this information, however,</br>';
        letterText+=' when using an emergency room or urgent care facility, these items may not</br>';
        letterText+='be available.</br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';        

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';  

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';  

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';  
        
        letterText+='<p>';
        letterText+='<div style="font-size: 10px;">';
        letterText+='<b>@2020, ALL RIGHTS RESERVED, CONTIGO HEALTH LLC PUBLISHED:DECEMBER 2020.<br/>';
        letterText+='To access online or mobile app services, terms and conditions apply.</b><br/>';
        letterText+='</div><br/>';
        letterText+='</p>';         
        //7-14-22 added to push enclosure to the next page -----------------------------------start---------------------------------------
        /*
        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';     
        */                             
        //7-14-22 added to push enclosure to the next page -----------------------------------end---------------------------------------
        //letterText+='<div><img src="'+inviteUCVisitsPath+'" style="width:375px;margin-left:2px"/></div><br/>';// pulls in the image of the ucVisits image

        letterText+='<div>';
        letterText+='<b><h3>Important Information to Know About Urgent Care Visits</h3></b>';
        letterText+='<b><u>If you feel you are experiencing a life-threatening emergency, call 911 immediately.</u></b>';
        letterText+='</div>';  
        
        letterText+='<p>';
        letterText+='</br>';
        letterText+='Urgent Care Centers are same-day clinics that can handle a variety of medical problems</br>';
        letterText+='that need to treated right away, but are not considered true emergencies. Symptoms that</br>';
        letterText+='can be evaluated and treated in an urgent care clinic include, but are not limited to:</br>';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='• Fever without rash</br>';
        letterText+='• Ear pain</br>';
        letterText+='• Painful urination</br>';
        letterText+='• Persistent diarrhea</br>';
        letterText+='• Sore throat</br>';
        letterText+='• Vomiting</br>';
        letterText+='• Minor trauma such as a common sprain or shallow cut</br>';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='If your symptoms come on gradually or you have an idea of your ailment—for example,</br>';
        letterText+='you have repeat urinary tract infections, or you recognize when your child has come down</br>';
        letterText+='with an ear infection—it’s worth calling your primary care doctor’s office to see if you can</br>';
        letterText+='get a same day appointment. After all, your primary care doctor knows your health history,</br>';
        letterText+='including what treatments have worked best in the past and whether other medical conditions</br>';
        letterText+='need to be taken into consideration. However, while urgent care clinics are not a substitute</br>';
        letterText+='for your primary care doctor, they are a great resource to utilize.</br>';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='<b><u>Important Considerations when using an Emergency Room versus Urgent Care</u></b></br> ';
        letterText+='When choosing to use an emergency room versus an urgent care facility, there are</br>';
        letterText+='important items to evaluate. Consider the cost involved. Your out-of-pocket expenses</br>';
        letterText+='to use an emergency room will be higher than using an urgent care facility. Also, to the</br>';
        letterText+='extent possible, be prepared and bring your medical ID card, share current diagnosed</br>';
        letterText+='medical conditions, a list of your current medications, current immunization history, and</br>';
        letterText+='primary care doctor may have this information, however, when using an emergency room</br>';
        letterText+='or urgent care facility, these items may not be available.</br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';


        letterText+='<p>';
        letterText+='<div style="font-size: 10px;">';
        letterText+='<b>@2020, ALL RIGHTS RESERVED, CONTIGO HEALTH LLC PUBLISHED:DECEMBER 2020.<br/>';
        letterText+='To access online or mobile app services, terms and conditions apply.</b>';
        letterText+='</div>';
        letterText+='</p>';
                             
        return letterText;
    }
    
}