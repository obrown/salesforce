/**
 * This class contains unit tests for validating the behavior of the Appeal Log application
 */
@isTest()
private class aclTest {
    string a, b, c = '';

    static testMethod void TestInsert() {
          //initialize new object record as alias tAcc
        Appeal_case__c tAcc = new Appeal_case__c();

          //Pass new tAcc record to the standard controller aliased as as sc
        ApexPages.StandardController sc = new ApexPages.StandardController(tAcc);

          //call CurrentRecordIdMemCount class aliased as crmc pass in standard controller sc
        CurrentRecordIdMemCount crmc = new CurrentRecordIdMemCount(sc);

          //add values to tAcc record and insert record
        tAcc.DateReceived__c = tAcc.Orig_Date_Received__c = system.today();
        tAcc.Name = 'test';
        tAcc.Member_ID__c = 'Test Member';
        test.startTest();
        insert tAcc;

          //use the system.assert to not pass the test class if the id is null
        system.assert (tAcc.id != null);
        system.debug(tAcc.id);

          //get the current page parameters in a map using the .get parameters
          //gets the pcid value and puts it into the tAcc.id
        ApexPages.Currentpage().getParameters().put('pcid', tAcc.id);

          //call new instance of CurrentRecordIdMemCount and pass in the standard controller??
        crmc = new CurrentRecordIdMemCount(sc);

          //call the get total count method to pass the test class??
        crmc.gettotal_count();

          //call the get total previousCase method to pass the test class??
        Appeal_Case__c foo = crmc.previousCase;

          //Test the searchACL class
        new searchAcl('Test Member');

        ApexPages.currentPage().getParameters().put('st', 'Test Member');
        new searchAcl();
        test.stopTest();
    }
}