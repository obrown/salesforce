@isTest(seeAllData=true)
private class referralFormTest {

    static testMethod void referralFormPatientCase() {
        
        Patient_Case__c pc = new Patient_Case__c();
        
        pc.client_name__c= 'Walmart';
        pc.recordtypeid = [select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name = 'Walmart Joint' limit 1].id;
        
        pc.Status__c = 'Open';
        pc.BID__c = '123456';
        pc.Language_Spoken__c = 'English';
        pc.Initial_Call_Discussion__c = 'The ICD';
        pc.Relationship_to_the_Insured__c = 'Spouse';
        pc.referral_source__c = 'Company Video';
        pc.Employee_Primary_Health_Plan_ID__c ='123456';
        pc.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        pc.Carrier_Name__c = 'Aetna';
        pc.Carrier_Nurse_Name__c = 'John Smith';
        pc.Carrier_Nurse_Phone__c = '(480) 555-1212';
        
        
        pc.Employee_first_name__c = 'John';
        pc.Employee_last_name__c = 'Smith';
        pc.employee_dobe__c = '1980-01-01';
        pc.Employee_Gender__c = 'Male';
        pc.employee_ssn__c = '123456789';
        
        pc.Patient_First_Name__c = 'Jane';
        pc.Patient_last_name__c = 'Smith';
        pc.Patient_dobe__c = '1981-01-01';
        pc.Patient_Gender__c = 'Female';
        pc.Patient_ssn__c = '987654321';
        pc.Patient_Email_Address__c = 'something@hdplus.com';
        
        pc.Same_as_Employee_Address__c = true;
        
        pc.employee_street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85297';
        
        pc.Employee_Home_Phone__c = '(480) 555-1212';
        pc.Employee_Mobile__c = '(480) 555-1212';
        pc.Employee_Work_Phone__c = '(480) 555-1212';
        
        pc.Employee_Preferred_Phone__c = 'Home';
        
        pc.Patient_Home_Phone__c = '(480) 555-1212';
        pc.Patient_Mobile__c = '(480) 555-1212';
        pc.Patient_Work_Phone__c = '(480) 555-1212';
        
        pc.Patient_Preferred_Phone__c = 'Home';
        
        pc.Callback_Number__c = '(480) 555-1212';
        
        pc.Caregiver_Home_Phone__c = '(480) 555-1212';
        pc.Caregiver_Mobile__c = '(480) 555-1212';
        
        pc.Caregiver_Name__c = 'John Smith';
        pc.Caregiver_DOBe__c = '1982-01-01';
        
        insert pc;
        
        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('id',pc.id);
        caseController cc = new caseController();
        
        cc.obj.Provider_Type__c='Family';
        cc.profname='Jane';
        cc.prolname='Smith';
        cc.proemail = 'something@hdplus.com';
        cc.prostreet = '123 E Main St';
        cc.procity = 'Mesa';
        cc.prozip = '85212';
        
        cc.AddProvider();
        
        cc.obj.Expedited_Referral__c = 'Yes';
        cc.obj.Medicare_As_Secondary_Coverage__c = 'No';
        cc.obj.Other_Pertinent_Medical_Info_History__c = 'Test';
        cc.obj.Patient_Symptoms__c = 'Testing';
        cc.obj.Proposed_Procedure__c = 'Testing';
        cc.obj.Recent_Testing__c = 'Testing';
    //    cc.obj.Referred_facility__c = 'John Hopkins';
        cc.obj.Diagnosis__c = 'Testing';
        
        cc.inlineSave();
        
        cc.getcodeOptions();
        cc.getcTypeItems();
        
        cc.obj.Clinical_Code_Status__c = 'Proposed';
        cc.codeType = 'DRG';
        cc.codeList =  [select id from Code__c where type__c = 'DRG' limit 1].id;
        cc.addCC();
        
        cc.obj.Eligible__c = 'Yes';
        cc.obj.isConverted__c = true;
        update cc.obj;
        
        referralFormController rfc = new referralFormController(new ApexPages.StandardController(cc.obj));
        
        ApexPages.currentPage().getParameters().put('pfName',cc.obj.Patient_First_Name__c);
        ApexPages.currentPage().getParameters().put('plName',cc.obj.Patient_Last_Name__c);
        
        ApexPages.currentPage().getParameters().put('eFname',cc.obj.Employee_First_Name__c);
        ApexPages.currentPage().getParameters().put('eLname',cc.obj.Employee_Last_Name__c);
        
        ApexPages.currentPage().getParameters().put('pDOB','1980-01-01');
        
        ApexPages.currentPage().getParameters().put('cDOB','1980-01-01');
        
        ApexPages.currentPage().getParameters().put('pSSN',cc.obj.Patient_SSN__c);
        ApexPages.currentPage().getParameters().put('eSSN',cc.obj.Employee_SSN__c);
        
        ApexPages.currentPage().getParameters().put('pStreet',cc.obj.Patient_Street__c);
        ApexPages.currentPage().getParameters().put('pCity',cc.obj.Patient_City__c);
        ApexPages.currentPage().getParameters().put('pZip',cc.obj.Patient_Zip_Postal_Code__c);
        
        ApexPages.currentPage().getParameters().put('eStreet',cc.obj.Employee_Street__c);
        ApexPages.currentPage().getParameters().put('eCity',cc.obj.Employee_City__c);
        ApexPages.currentPage().getParameters().put('eZip',cc.obj.Employee_Zip_Postal_Code__c);
        
        ApexPages.currentPage().getParameters().put('pHomePhone',cc.obj.Patient_Home_Phone__c);
        ApexPages.currentPage().getParameters().put('pMobilePhone',cc.obj.Patient_Mobile__c);
        
        ApexPages.currentPage().getParameters().put('pEmail',cc.obj.Patient_Email_Address__c);
        
        ApexPages.currentPage().getParameters().put('pBID',cc.obj.certid__c);
        
        ApexPages.currentPage().getParameters().put('cn',cc.obj.Caregiver_Name__c);
        ApexPages.currentPage().getParameters().put('cnHP',cc.obj.Caregiver_Home_Phone__c);
        ApexPages.currentPage().getParameters().put('cnMP',cc.obj.Caregiver_Mobile__c);
        
        rfc = new referralFormController(new ApexPages.StandardController(cc.obj));
        
        rfc = new referralFormController(new ApexPages.StandardController(cc.obj), 'John');
        
        Test.stopTest();
        
    }
    
}