@isTest(SeeAllData=true)
private class emailToNoteTest{

    static testMethod void emailToNoteTest() {
    
    string pcName = [select name from patient_case__c limit 1].name; 
        
       /* Send Email with no pc number in the subject */ 
        
       // Create a new email, envelope object and Attachment
       Messaging.InboundEmail email = new Messaging.InboundEmail();
       Messaging.InboundEnvelope env = new Messaging.InboundEnvelope();
       
       email.subject = 'test'; //will not attach
       email.plainTextBody = 'Test'; //will be labeled as inbound
       env.fromAddress = 'user@hdplus.com';
       
       // call the class and test it with the data in the testMethod
       emailToNote emailServiceObj = new emailToNote();
       emailServiceObj.handleInboundEmail(email, env ); 
       
       // Create another email, envelope object and Attachment
       email = new Messaging.InboundEmail();
       env = new Messaging.InboundEnvelope();
       
       /******* Send Email with pc number in the subject, outbound *******/
       
       email.subject = 'test ' + pcname; //will not attach
       email.plainTextBody = '__________________________                  Test'; //will be labeled as outbound
       env.fromAddress = 'user@hdplus.com';
       
       // call the class and test it with the data in the testMethod
       emailServiceObj = new emailToNote();
       emailServiceObj.handleInboundEmail(email, env );
       
       /* ------------------ */


       /******* Send Email with pc number in the subject, inbound *******/
       
       email.subject = 'test ' + pcname; //will not attach
       email.plainTextBody = 'Test'; //will be labeled as outbound
       env.fromAddress = 'user@hdplus.com';
       
       // call the class and test it with the data in the testMethod
       emailServiceObj = new emailToNote();
       emailServiceObj.handleInboundEmail(email, env );
       
       /* ------------------ */
                            
    }

}