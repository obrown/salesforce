public without sharing class bariatricFuture{
    
    
    @future() 
    public static void setDemoBariatricStages(set<id> ids) {
        
        Bariatric_Stage__c[] blist = new Bariatric_Stage__c[]{}; 
        
        for(Bariatric_Stage__c b: [select Employee_First_Name__c,
                                          Bariatric__r.Employee_First_Name__c,
                                          Employee_Last_Name__c,
                                          Bariatric__r.Employee_Last_Name__c,
                                          Employee_City__c,
                                          Bariatric__r.Employee_City__c,
                                          Employee_DOB__c,
                                          Bariatric__r.Employee_DOB__c,
                                          Patient_DOB__c,
                                          Bariatric__r.Patient_DOB__c,
                                          Patient_First_Name__c,
                                          Bariatric__r.Patient_First_Name__c,
                                          Patient_Last_Name__c,
                                          Bariatric__r.Patient_Last_Name__c,
                                          Patient_City__c,
                                          Bariatric__r.Patient_City__c from Bariatric_Stage__c where Bariatric__c IN :ids]){
        
        
             if(b.Employee_First_Name__c != b.Bariatric__r.Employee_First_Name__c ||
                b.Employee_Last_Name__c != b.Bariatric__r.Employee_Last_Name__c || 
                b.Employee_City__c != b.Bariatric__r.Employee_City__c ||
                b.Employee_DOB__c != b.Bariatric__r.Employee_DOB__c ||
                b.Patient_DOB__c != b.Bariatric__r.Patient_DOB__c ||
                b.Patient_First_Name__c != b.Bariatric__r.Patient_First_Name__c ||
                b.Patient_Last_Name__c != b.Bariatric__r.Patient_Last_Name__c ||
                b.Patient_City__c != b.Bariatric__r.Patient_City__c){
             
                    b.Employee_First_Name__c = b.Bariatric__r.Employee_First_Name__c;    
                    b.Employee_Last_Name__c = b.Bariatric__r.Employee_Last_Name__c;    
                    b.Employee_City__c = b.Bariatric__r.Employee_City__c;    
                    b.Employee_DOB__c = b.Bariatric__r.Employee_DOB__c;    
                
                    b.Patient_DOB__c = b.Bariatric__r.Patient_DOB__c;    
                    b.Patient_First_Name__c = b.Bariatric__r.Patient_First_Name__c;    
                    b.Patient_Last_Name__c = b.Bariatric__r.Patient_Last_Name__c;    
                    b.Patient_City__c = b.Bariatric__r.Patient_City__c;
                
                    blist.add(b);    
             
             }
            
        }
        
        update blist;
                
    }
    
    @future
    public static void submitForEligibility(set<id> theIds){
        
        map<id, string> result = bariatricWorkflow.submitForEligibility([select Eligible__c, Date_Eligibility_Check_with_Client__c,Eligibility_Verified_By__c ,Eligibility_Notes__c , BID_Verified__c,Verified_Employee_DOB__c,Verified_Patient_DOB__c,Verified_Patient_SSN__c from bariatric__c where id in:theids]);
        
    }
    
    @future
    public static void syncPostProcedureDate(set<id> theIds){
        
        map<id, bariatric_stage__c> postMap = new map<id, bariatric_stage__c>();
        boolean hasPostRecords =false;
        
        for(bariatric_stage__c bs : [select bariatric__c, Procedure__c from bariatric_stage__c where bariatric__c in: theIds and recordtype.name = 'Post']){
            postMap.put(bs.bariatric__c, bs);
            hasPostRecords = true;
        }
        
        if(hasPostRecords){
        
            bariatric_stage__c[] updateList = new bariatric_stage__c[]{};
        
            for(bariatric_stage__c bs : [select bariatric__c, Procedure__c from bariatric_stage__c where bariatric__c in: theIds and recordtype.name = 'Global']){
                postMap.get(bs.bariatric__c).Procedure__c = bs.Procedure__c;
                updateList.add(postMap.get(bs.bariatric__c));
            }
        
            update updateList;
        
        }
    
    }

    @future
    public static void syncReferredFacilty(set<id> theIds){
        
        bariatric_stage__c[] updateList = new bariatric_stage__c[]{};
        
        for(bariatric_stage__c bs : [select referred_facility__c,bariatric__r.referred_facility__c  from bariatric_stage__c where bariatric__c in: theIds]){
            bs.referred_facility__c = bs.bariatric__r.referred_facility__c;
            updateList.add(bs);
        }
        
        update updateList;
        
    }
    
    @future
    public static void sendCarrierDischargeNotice(set<id> theIds){
        bariatricWorkflow.sendCarrierDischargeNotice(theIds);
    
    }

    @future
    public static void setStatusRollUp(set<id> theIds){
        bariatricWorkflow.setStatusRollup([select id,Stage__c,Status__c,Status_Reason__c,Substatus_Reason__c,Status_RollUp__c from bariatric__c where id in :theIds]);
    
    }
    
    @future(callout=true)
    public static void sendReferralform(id theId) {
        bariatric__c b = [select Annual_Deductible__c,
        Annual_Out_of_Pocket_Max_Individual__c,
        Annual_Out_of_Pocket_Max_Family__c,
        bid__c,
        Carrier_Name__c,
        certID__c,
        Client__c,
        Client_Name__c,
        Client__r.logoDirectory__c,
        client__r.client_paid_travel__c,
        client_facility__c,
        client_facility__r.Referral_Email_Contact__c,
        client_facility__r.Authorization_Email_Contact__c,
        COB_Received__c,
        coverage_level__c,
        createdDate, 
        createdBy.name,
        Criteria_Met__c,
        Current_Nicotine_User__c,
        Date_Eligibility_Submitted__c,
        Date_Eligibility_Check_with_Client__c,
        Date_Intake_Information_is_Complete__c,
        Date_of_Lipid_Panel__c,
        Date_Provider_Verification_Form_Received__c,
        Date_Provider_Verification_Form_Signed__c,
        Deductible_Met__c,
        Deductible_Remaining__c,
        Distance_to_Facility__c,
        Does_patient_have_other_coverage__c,
        Intake_Information_Complete__c,
        encyptionKey__c,
        Eligible__c,
        Eligibility_Notes__c,
        Eligibility_Verified_By__r.name,
        Employee_DOB__c,
        Employee_Gender__c,
        Employee_Street__c,
        Employee_City__c,
        Employee_State__c,
        Employee_Zip_Code__c,
        employee_first_name__c,
        employee_last_name__c, 
        Employee_SSN__c,
        employee_home_phone__c,
        employee_mobile_phone__c,
        Employee_Email_Address__c,
        Employee_Primary_Health_Plan_Name__c,
        Facility_Clinic_Name__c,
        Gastrointestinal_Disorders__c,
        GastrointestinalDisordersDateOfDiagnosis__c ,
        Gastrointestinal_List_Current_Treatment__c,
        Hardstop__c,
        Hardstop_Reason__c,
        hardStopLetterSent__c,
        HDL__c,
        Heart_Disease__c,
        HeartDiseaseDateOfDiagnosis__c,
        Heart_Disease_List_Current_Treatment__c,
        HP_Plan_Name__c,
        Home_Provider__c,
        Hypertension__c,
        HypertensionDateOfDiagnosis__c,
        Is_Patient_on_Oxygen__c,
        Medication_List__c,
        last_patient_contact__c,
        lastmodifiedDate,
        lastmodifiedBy.Name,
        LDL__c ,
        Lipid_Abnormalities__c,
        LipidAbnormalitiesDateOfDiagnosis__c,
        name,
        Non_alcoholic_Fatty_Liver_Disease__c,
        NAFattyLiverDiseaseDateOfDiagnosis__c,
        Nicotine_Type__c,
        Out_of_Pocket_Met_Family__c,
        Out_of_Pocket_Met_Individual__c,
        OOP_Remaining__c,
        OOP_Remaining_Family__c,
        OOP_Remaining_Individual__c,
        Other_Obesity_Related_Comorbidities__c,
        ObesityRelatedDateofDiagnosis__c,
        Related_Comorbidities_Treatment__c,
        OsteoarthritisDateOfDiagnosis__c,
        Osteoarthritis__c,
        owner.name,
        Patient_Age__c,
        Patient_BMI__c,
        Patient_DOB__c,
        Patient_Gender__c,
        Patient_Street__c,
        Patient_City__c,
        Patient_State__c,
        Patient_Zip_Code__c,
        Patient_first_name__c,
        Patient_last_name__c,
        Patient_SSN__c,
        Patient_home_phone__c,
        Patient_mobile_phone__c,
        Patient_Email_Address__c,
        Patient_Height_FT__c,
        Patient_Height_Inches__c,
        Patient_Weight__c,
        Patient_Responsibility__c,
        Primary_Insurance__c,
        Provider_Email_Address__c,
        Provider_Name_Credentials__c,
        Provider_State__c,
        Provider_Street__c,
        Provider_City__c,
        Provider_Zip_Code__c,
        Provider_Phone__c,
        Provider_Fax__c, 
        pfv_valid__c,
        Recommended_Facility__c,
        Relationship_to_the_Insured__c,
        Referral_Date__c,
        Referred_Facility__c,
        Respiratory_Disorders__c,
        RespiratoryDisordersDateOfDiagnosis__c ,
        Respiratory_Disorders_Current_Treatment__c,
        Secondary_Insurance__c,
        Sleep_Apnea__c,
        Sleep_Apnea_List_Current_Treament__c,
        SleepApneaDateOfDiagnosis__c,
        status__c,
        status_reason__c,
        Stage__c,
        substatus_reason__c,
        Total_Cholesterol__c,
        Type_2_Diabtetic__c,
        Type2DiabetesDateOfDiagnosis__c,
        Verified_Patient_DOB__c,
        Verified_Employee_DOB__c,
        Verified_Patient_SSN__c,
        Verified_Employee_SSN__c,
        BID_Verified__c from bariatric__c where id = :theID limit 1];
        bariatricWorkflow.sendReferralEmail(b);
        
    }

}