@RestResource(urlMapping='/goRandomAudits/*')

global with sharing class randomAuditApi{

    @HttpPost
    global static string randomAuditClaims(){
        string msg = 'Ok';
        RestRequest req = RestContext.request;
        //string strTotalClaims = req.headers.get('Totalclaims');
        
        string jsonString = req.requestBody.toString();
        string[] incomingLines;
        incomingLines = (string[])JSON.deserialize(jsonString, string[].class);
        caClaim__c[] clmsToUpload = new caClaim__c[]{};
        
        Claims_Audit_Client__c client;
        
        map<string, Claims_Processor_Crosswalk__c> processorMap = new map<string,Claims_Processor_Crosswalk__c>();
           
        for(Claims_Processor_Crosswalk__c cpc : [select id, Name ,Processor_Alias_Name__c from Claims_Processor_Crosswalk__c]){
            if(cpc.Processor_Alias_Name__c!=null){
              
                if(cpc.Processor_Alias_Name__c.contains(',')){
                    for(string s : cpc.Processor_Alias_Name__c.split(',')){
                        processorMap.put(s.toLowerCase().trim(), cpc);
                    }                
                }else{
                    processorMap.put(cpc.Processor_Alias_Name__c.toLowerCase().trim(), cpc);
                  
                }
            }
        }
        
        map<string, Claims_Audit_Client__c> clientMap = new map<string, Claims_Audit_Client__c>();
        
        for(Claims_Audit_Client__c c : [select name, percent_to_pull__c,Underwriter__c,Group_Number__c from Claims_Audit_Client__c]){
            
            if(c.Group_Number__c.indexof(',')>0){
                for(string g : c.Group_Number__c.split(',')){
                    clientMap.put(c.Underwriter__c+''+g.trim(), c);
                }
            }else{
                clientMap.put(c.Underwriter__c+''+c.Group_Number__c, c);
            }
            
            
        }
        
        for(string il : incomingLines){
            string[] values = EncodingUtil.urlDecode(il,'UTF-8').replaceAll('"','').split(',');
            if(values[0]=='PUNBR'){
                return 'Line Ignored';
            } 
            
                    client = clientMap.get(values[0].trim()+values[1].trim());
                    if(client==null){
                        client = new Claims_Audit_Client__c(name=values[0], Underwriter__c=values[0].trim(), Group_Number__c=values[1].trim(), percent_to_pull__c=2.0);
                        insert client;
                        clientMap.put(client.Underwriter__c+''+client.Group_Number__c, client);
                    }
            
            caClaim__c clm = new caClaim__c();
            clm.name=values[2];
            clm.Client__c=client.id;
            
            string[] foo;
            if(values[6]!=''){
                foo = values[6].split('/');
                clm.Claim_Received_Date__c= date.valueof(foo[2]+'-'+foo[0]+'-'+foo[1]);
                
            }else{
                clm.Claim_Received_Date__c= null;
            }
            
            if(values[5]!=''){
                foo = values[5].split('/');
                clm.Date_Paid__c = date.valueof(foo[2]+'-'+foo[0]+'-'+foo[1]);
                
            }else{
                clm.Date_Paid__c =null;
            }
            
            if(values[4]!=''){
                foo = values[4].split('/');
                clm.Claim_Processed_Date__c= date.valueof(foo[2]+'-'+foo[0]+'-'+foo[1]);
                
            }else{
                clm.Claim_Processed_Date__c= null;
            }
            
            clm.Total_Charge__c = decimal.valueof(values[7]);
            clm.Product__c      = values[8].trim();
            clm.Form_Type__c    = values[9].trim();
            clm.Source_Type__c  = values[10].trim();
            clm.group_number__c = values[1].trim();
            clm.Underwriter__c  = values[0].trim();
            
            if(processorMap.get(values[3].toLowerCase().trim()) != null){
                clm.Claims_Processor_Crosswalk__r = processorMap.get(values[3].toLowerCase().trim());
                clm.Claims_Processor_Crosswalk__c = processorMap.get(values[3].toLowerCase().trim()).id;
            }else{
                clm.Claims_Processor_Crosswalk__r = processorMap.get('auditmanager');
                clm.Claims_Processor_Crosswalk__c = processorMap.get('auditmanager').id;
            }
            
            clmsToUpload.add(clm);
        }
        
        string linesRcvd = string.valueof(clmsToUpload.size());
        
        //AggregateResult[] claimsLoaded = [select count(id) from caClaim__c where Underwriter__c =:clmsToUpload[0].Underwriter__c and createdDate > :date.today() and createdBy.Name='System'];        //system.debug(strTotalClaims+' '+claimsLoaded[0].get('expr0'));
        //system.debug(clmsToUpload[1].Underwriter__c+' '+claimsLoaded[0]);
        //system.debug('strTotalClaims '+strTotalClaims);
        //system.debug('NUmber of randown claims to load: '+ math.round(integer.valueof(integer.valueof(strTotalClaims)*(client.percent_to_pull__c/100))) +1);
        //string clmSize='0';
        //if(integer.valueof(claimsLoaded[0].get('expr0'))==0 || math.round(integer.valueof(integer.valueof(strTotalClaims)*(client.percent_to_pull__c/100))) +1>integer.valueof(claimsLoaded[0].get('expr0'))){
            claimAuditRandomSelection cars = new claimAuditRandomSelection();
            string clmSize = cars.RandomSelection(clmsToUpload, client);
        //}
        
        return string.valueof('Lines received '+ linesRcvd + ' || Audits records created ' + clmSize);
    }
    
    @HttpPut
    global static string randomAuditClaimsProcessed(){
        string msg = 'Ok';
        RestRequest req = RestContext.request;
        map<string, string> headers = req.headers;
        
        string uw = req.headers.get('Uw');
        string processed = req.headers.get('Processed');
        string month = req.headers.get('Month');
        string resp='';
        
        if(uw=='PUNBR'){
                return 'Line Ignored';
        } 
        
        if(uw != null && month != null && processed != null){
            uw = uw.replaceAll('\"','');
            string grp;
            if(uw.left(3)=='502'){
                grp = uw.right(3);
            }
            
            uw = uw.left(3);
            id clientID;
            if(uw=='502'){
                clientID = [select id from Claims_Audit_Client__c where Underwriter__c = :uw and Group_Number__c = :grp limit 1].id;
            }else{
                clientID = [select id from Claims_Audit_Client__c where Underwriter__c = :uw limit 1].id;
            }
            
            string[] foo = month.split('/');
            date dfoo = date.valueof(foo[2]+'-'+foo[0]+'-01');
            caClaims_Processed__c cp = new caClaims_Processed__c(Client__c=clientId,Claims_Processed__c=integer.valueof(processed),Month_Paid__c=dFoo);
            insert cp;
            resp = cp.id;
        }
       
        return resp;
    }
}