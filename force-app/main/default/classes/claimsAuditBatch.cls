public class claimsAuditBatch{
    
    public void run(){
        sendErrors();
    }
    
    void sendErrors(){
        
        set<id> foo = new set<id>();
        set<id> planFoo = new set<id>();
        integer batchSize = 100;
        integer count=0;
        
        Claim_Audit_Error__c[]  ceList = new Claim_Audit_Error__c[]{};
        
        for(Claim_Audit_Error__c  ce : [select Claim_Audit__c,emailSent__c,Error_Source__c from Claim_Audit_Error__c where emailSent__c = null and resolved__c = false]){
        
            if(ce.Error_Source__c == 'Plan Build'){
                planFoo.add(ce.Claim_Audit__c);
            }else{
                foo.add(ce.Claim_Audit__c);
            }
            
            ce.emailSent__c = date.today();
            ceList.add(ce);
            count++;
            
            if(batchSize==count){
                
                if(foo.size()>0){
                    claimAuditFuture.sendClaimAuditErrors(foo, false);
                    foo.clear();
                }
                
                if(planFoo.size()>0){
                    claimAuditFuture.sendClaimAuditErrors(planFoo, true);
                    planFoo.clear();
                }
                count=0;
                
            } 
            
               
        }
        
        if(count<batchSize){
            if(foo.size()>0){
                claimAuditFuture.sendClaimAuditErrors(foo, false);
                foo.clear();
            }
                
            if(planFoo.size()>0){
                claimAuditFuture.sendClaimAuditErrors(planFoo, true);
                planFoo.clear();
            }
        
        }
        
        if(ceList.size()>0){
            update ceList;                
        }
        
    }

}