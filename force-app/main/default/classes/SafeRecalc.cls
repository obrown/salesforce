public without sharing class SafeRecalc{

// Expensive, but typesafe 
public static sObject SafeRecalc(sObject record) {
    // New Instance with id 
    sObject clone = record.getSObjectType().newSObject(record.Id); 

    // Keep describe handy 
    Map<String, Schema.SObjectField> fields = record.getSObjectType().getDescribe().fields.getMap();  
    system.debug('fields '+fields);
    
    // Get fields with values 
    Map<String, Object> populatedFields = record.getPopulatedFieldsAsMap();   
    system.debug('populatedFields '+populatedFields );

    // Keep system fields in own list - merge into cloned record after recalc 
    Map<String, Object> nonEditableFields = new Map<String, Object>(); 
    
    // Set field values for non-standard fields 
    for (String key:populatedFields.keySet()) {
        // Any values we can safely pass along are "cloned" into the new record 
        system.debug('key '+key);
        
        if(key.right(3)=='__r' || key=='CreatedBy' || key =='LastModifiedBy' || key=='Owner'){
            continue;
        }if(fields.get(key).getDescribe().isUpdateable()) {
            clone.put(key, populatedFields.get(key));
        }else if (!fields.get(key).getDescribe().isCalculated()) {
            // Any system fields, non-editable fields, go here 
            nonEditableFields.put(key, populatedFields.get(key)); 
        }
    }

    clone.reCalculateFormulas(); // safe recalc 

    // Need to update read-only fields
    // So we use a json map to do that
    // We create a map based on the fields, then merge the non-editable fields into the map 
    Map<String, Object> untypedClone = (Map<String, Object>)JSON.deserializeUntyped(JSON.serialize(clone));

    for (String key: nonEditableFields.keySet()) {
        untypedClone.put(key, nonEditableFields.get(key));
    }

    // Then we re-seralize the value, then de-seralize it as a instance of the given type, then cast it back to an sObject to match our record 
    record = (sObject)JSON.deserialize(JSON.serialize(untypedClone), Type.forName(record.getSObjectType().getDescribe().getName())); 

    return record; 
}

}