@isTest()
private class oncologyQueuesControllerTest{
    
      static testMethod void QueuesTest(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncologyQueuesController qc = new oncologyQueuesController();
         ApexPages.CurrentPage().getParameters().put('lv', 'programDetermination');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('lv', 'all');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('lv', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('lv', 'amazon');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('lv', 'facebook');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('lv', 'nextera energy');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('lv', 'sisc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('lv', 'awaitingreferral');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('infopackettobesent', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('missingforms', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('weeklytouchbase', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('stipendcardtosend', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('travelrequest', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('traveltoenter', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('memberidcardtosend', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('travelitinerarysenttofacility', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('travelitinerarysenttomember', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('actualarrival', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('patientatcoe', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('actualdeparture', 'poc');
         qc.changeListView();
         
         ApexPages.CurrentPage().getParameters().put('thisMonth', 'poc');
         qc.changeListView();
         
         
      }
      
}