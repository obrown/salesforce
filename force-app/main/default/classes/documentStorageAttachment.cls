public with sharing class documentStorageAttachment{

    final string sId = userinfo.getSessionid();
    final string userId = userinfo.getuserid();
    final string orgId = userinfo.getOrganizationId();
    final string filepath;
    
    public class response{
        public boolean isError;
        public string errorMsg;
        public blob file;
        
    }
    
    public documentStorageAttachment(){
    }
    
    public response getAttachment(string filepath){
        
        response response = new response();
        response.isError = false;
        response.errorMsg='';
        //string sessionid, string userid, string Orgid, string filepath){
        fileServer fileServer = new fileServer();
        blob file = fileServer.getFile(sId, userId, Orgid, filepath);
        
        if(file ==null){
            response.isError=true;
            response.errorMsg='Error getting file';    
            
        }else{
            response.file = file;
        }
        
        return response;
    }
    
}