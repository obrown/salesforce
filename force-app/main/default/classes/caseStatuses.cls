public with sharing class caseStatuses {

    final string profID;
    SystemID__c sid = SystemID__c.getInstance(); 
    string[] status;
    map<string,string[]> statusReason;
    map<string,string[]> SubStatusReason;
        
    public caseStatuses(string profid){
        this.profid = profid;
            
    }
    
    public map<string,string[]> getStatusReasonDGT(boolean isConverted){
        setStatusReasonDigitalPhysicalTherapy(isConverted);
        return statusReason;
    }
    
    public string[] getStatus(boolean isConverted, boolean reload){
        if(status==null||reload){setStatus(isConverted);}
        return status;
    }
    
    public map<string,string[]> getStatusReason(boolean isConverted,boolean reload){
        if(statusReason==null||reload){setStatusReason(isConverted);}
        return statusReason;
    }

    public map<string,string[]> getSubStatusReason(boolean isConverted,boolean reload){
        if(SubStatusReason==null||reload){setSubStatusReason(isConverted);}
        return SubStatusReason;
    }
    
    void setStatus(boolean isConverted){
        status = new string[]{};
        status.add('Open');
        
        if(isConverted){
          
          if(profID !=sid.csrProfID__c){
            //set Statuses
            status.add('Pended'); 
            status.add('Closed');
            status.add('Completed');
                                
          }
              
        }else{
          if(profID !=sid.csrProfID__c){
            //set Statuses
            status.add('Pended'); 
          }
          status.add('Closed');
        }
        
    }
    void setStatusReasonDigitalPhysicalTherapy(boolean isConverted){
        statusReason = new map<string,string[]>();
        /* All keys must be available in the status reason map, or the page will error */
        string[] sr = new string[]{};
       
        sr.add('-- None --');
        
                sr.add('Awaiting Appointment Date Confirmations');
                sr.add('Claim Pending');
                sr.add('Claim Received');
                sr.add('Claim Paid/Appt in Progress');
               
                //statusReason.put('Open',sr);9-21-20 replaced with code below
                statusReason.put('Open',sr.clone());
                sr.clear();
                
                sr.add('-- None --');                
                sr.add('Awaiting Appointment Date Confirmations');
                sr.add('Claim Pending');
                sr.add('Claim Received');
                sr.add('Claim Paid/Appt in Progress');
                
                //statusReason.put('Pended',sr);9-21-20 replaced with code below
                statusReason.put('Pended',sr.clone());
                sr.clear();                

                sr.add('-- None --');                
                sr.add('Benefit Eligibility');  
                sr.add('Disengaged');
                sr.add('Local Vendor');
                sr.add('Technology');  
                
                //statusReason.put('Closed',sr);9-21-20 replaced with code below
                statusReason.put('Closed',sr.clone());
                sr.clear(); 
                        
                sr.add('-- None --');                
                sr.add('Awaiting Appointment Date Confirmations');
                sr.add('Claim Pending');
                sr.add('Claim Received');
                sr.add('Claim Paid/Appt in Progress');
                                        
                //statusReason.put('Completed',sr);9-21-20 replaced with code below
                statusReason.put('Completed',sr.clone());
                sr.clear();         


       
    }
    void setStatusReason(boolean isConverted){
        statusReason = new map<string,string[]>();
        /* All keys must be available in the status reason map, or the page will error */
        string[] sr = new string[]{};
        statusReason.put('Open',sr);
        statusReason.put('Pended',sr);
        statusReason.put('Closed',sr);
        statusReason.put('Completed',sr);
        
        
        //Post-referral Statuses
        if(isConverted){
            if(profID !=sid.csrProfID__c){
                //Set statusReason
                sr.add('-- None --');
                sr.add('Awaiting Appointment Date Confirmations');
                sr.add('Awaiting Forms');
                sr.add('Awaiting Plan of Care');
                sr.add('Awaiting Travel Confirmation');
                sr.add('Claim Pending');
                sr.add('Claim Received');
                sr.add('Patient at COE');
                sr.add('Ready for Arrival');
                
                statusReason.put('Open',sr.clone());
                sr.clear();
                
                sr.add('-- None --');
                sr.add('Appointment Date');
                sr.add('BMI');
                sr.add('Benefit Eligibility');
                sr.add('Caregiver');
                sr.add('COE Availability');
                sr.add('Dental');
                sr.add('Diagnostics');
                sr.add('Infectious Disease');                
                sr.add('Medical Condition');
                sr.add('Medical Records');
                sr.add('Nicotine');
                sr.add('No Home Physician');
                
                statusReason.put('Pended',sr.clone());
                sr.clear();
        
                sr.add('-- None --');
                sr.add('Benefit Eligibility');
                sr.add('Cancelled by Facility');
                sr.add('Cancelled by Patient');
                sr.add('Infectious Disease');
                //sr.add('Other');
                
                statusReason.put('Closed',sr.clone());
                sr.clear();
                
                sr.add('-- None --');
                sr.add('Claim Paid');
                sr.add('No Claim');
                //sr.add('Claim Received');
                                
                statusReason.put('Completed',sr.clone());
                sr.clear();             
            }else{
                sr.add('-- None --');
                sr.add('In Process');
               statusReason.put('Open',sr);
            }   
        }else{
        
        //Pre-referral Statuses
           if(profID !=sid.csrProfID__c){ //non-CSR
                
                sr.add('-- None --');
                sr.add('New');
                sr.add('In Process');
                sr.add('Invalid Information');
                sr.add('Clinical Protocols');
                
                if(profID ==sid.IntakeSpecialist__c){
                    sr.add('Call One');
                    sr.add('Call Two');
                    sr.add('Call Three');
                }
                
                statusReason.put('Open',sr.clone());
                sr.clear();
                
                sr.add('-- None --');
                sr.add('Patient Choice');
                sr.add('Clinical Protocols');
                sr.add('Infectious Disease');
                 
                statusReason.put('Pended',sr.clone());
                sr.clear();
                
                sr.add('-- None --');
                sr.add('Patient Declined');
                sr.add('Cancelled by Patient');
                sr.add('Clinical Protocols');
                //sr.add('Other');
                sr.add('Benefit Eligibility');
                sr.add('Infectious Disease');
                statusReason.put('Closed',sr.clone());
                sr.clear();
                
                statusReason.put('Completed',sr);
                
            }else{//CSR
              
                sr.add('-- None --');
                sr.add('Patient Declined');
                sr.add('Infectious Disease');
                statusReason.put('Closed',sr.clone());
                sr.clear();
                
                sr.add('-- None --');
                sr.add('New');
                sr.add('In Process');
                sr.add('Invalid Information');
                statusReason.put('Open',sr.clone());
                sr.clear();
            }       
        }
        
    }
    
    void setSubStatusReason(boolean isConverted){
        SubStatusReason = new map<string, string[]>();
        string[] sr = new string[]{};
        
        if(isConverted){
            
            sr.add('-- None --');
            sr.add('Medical Complications/Conditions');
            sr.add('BMI');
            sr.add('Medical Records');
            sr.add('Home Provider');
            sr.add('Recommended Procedure');
            sr.add('No Procedure Indicated Pre-travel');
            sr.add('No Evaluation Indicated');
            
            SubStatusReason.put('Cancelled by Facility',sr.clone());
            sr.clear();   
            
            sr.add('-- None --');
            sr.add('Program Compliance');
            sr.add('Local Hospital');
            sr.add('Disengaged');
            sr.add('Patient Declined');
            sr.add('Home Provider');
            sr.add('Caregiver');
            
            SubStatusReason.put('Cancelled by Patient',sr.clone());
            sr.clear();
            
            sr.add('-- None --');
            sr.add('No Eligibility');
            sr.add('Patient Expired');
            
            SubStatusReason.put('Benefit Eligibility',sr.clone());
            sr.clear();
            
            sr.add('-- None --');
            sr.add('Billed Carrier');
            sr.add('Bundle');
            sr.add('Cancel After Arrival');
            sr.add('Timely filing');
            sr.add('Warranty');
            
            SubStatusReason.put('No Claim',sr.clone());
            sr.clear();

            //6-30-20 updated with new values  
            sr.add('Infectious Disease');
            sr.add('Disengaged');
            sr.add('Local Hospital');
            sr.add('Patient Declined');
            sr.add('Reduced Symptoms');            
            SubStatusReason.put('Infectious Disease',sr.clone());
            sr.clear();            
                        
        }else{
            
            if(profID ==sid.IntakeSpecialist__c){
                sr.add('-- None --');
                sr.add('No Home Physician');
                sr.add('No Caregiver');
                sr.add('No Home Physician or Caregiver');
                SubStatusReason.put('Call One',sr.clone());
                SubStatusReason.put('Call Two',sr.clone());
                SubStatusReason.put('Call Three',sr.clone());
                sr.clear();
            }
            
            sr.add('-- None --');
            sr.add('Not Eligible - Procedure');
            sr.add('No Home Physician');
            sr.add('Caregiver');
            sr.add('No Eligibility');
            sr.add('Patient Expired');
            sr.add('Workers Compensation');
            sr.add('Subrogation');
            SubStatusReason.put('Benefit Eligibility',sr.clone());
            sr.clear(); 
            
            sr.add('-- None --');
            sr.add('Diagnostics');
            sr.add('Last 12 mos provider visit');
            sr.add('Recommended Procedure');
            SubStatusReason.put('Clinical ProtocolsOpen',sr.clone());
            sr.clear();
           
            sr.add('-- None --');
            sr.add('BMI');
            sr.add('Dental');
            subStatusReason.put('Clinical ProtocolsPended',sr.clone());
            sr.clear();     
           
            sr.add('-- None --');
            sr.add('Diagnostics');
            sr.add('Last 12 mos provider visit');
            sr.add('Recommended Procedure');
            sr.add('Dental');
            sr.add('Nicotine');
            SubStatusReason.put('Clinical ProtocolsClosed',sr.clone());
            sr.clear();
            
            sr.add('-- None --');
            sr.add('COE Assignment');
            sr.add('Local Hospital');
            sr.add('Incomplete');
            sr.add('Inquiry');
            if(profID != sid.csrProfID__c){
                sr.add('Disengaged');
            }           
            
            SubStatusReason.put('Patient Declined',sr.clone());
            sr.clear(); 
            
            sr.add('-- None --');
            sr.add('No Eligibility');
            sr.add('Not Eligible - Procedure');
            sr.add('Subrogation');
            sr.add('Workers Compensation');
            SubStatusReason.put('Benefit Eligibility',sr.clone());  
            sr.clear(); 

            //6-30-20 updated with new values  
            sr.add('Infectious Disease');
            sr.add('Disengaged');
            sr.add('Local Hospital');
            sr.add('Patient Declined');
            sr.add('Reduced Symptoms');
            
              
            SubStatusReason.put('Infectious Disease',sr.clone());
            sr.clear();            
            
        }
    }
    public static patient_case__c autoStatus(patient_case__c ipc){
        
        if(ipc.Ecen_Procedure_Name__c!=null && ipc.Ecen_Procedure_Name__c.contains('Digital Physical Therapy')){
            return autoStatusDigitalPT(ipc);
            
        }
        if(ipc.Ecen_Procedure_Name__c =='Substance Disorder'){
            return autoStatusSubstanceDisorder(ipc);
            
        }
        
        patient_case__c pc =ipc;
        
        if(pc.Status__c=='Open' && pc.isConverted__c && pc.actual_departure__c == null){
            if(pc.Received_Determination_at_HDP__c==null){
                pc.status_reason__c = 'Awaiting Plan of Care';
            }else{
                    
                       
                    if(pc.OP_Pre_op_Evaluation__c==null || pc.Hospital_Admit__c==null){
                        pc.status_reason__c = 'Awaiting Appointment Date Confirmations';
                    }
                    
                    if(pc.OP_Pre_op_Evaluation__c!=null && pc.Hospital_Admit__c!=null || (pc.Hospital_Admit__c==null && pc.OP_Pre_op_Evaluation__c!=null && pc.Last_Appointment_Date__c!=null)){
                        
                        if(pc.Travel_Request__c==null || pc.Estimated_Arrival__c==null || pc.Estimated_Departure__c==null){
                            pc.status_reason__c = 'Awaiting Travel Confirmation';
                        
                        }else if(pc.Travel_Request__c!=null){
                            
                            if(pc.Nicotine_Quit_Date__c != null && (pc.Patient_Cleared_to_Travel__c ==null || pc.Patient_Cleared_to_Travel__c =='')){
                                pc.status_reason__c = 'Awaiting Travel Confirmation';
                            
                            }else{
                                    
                                if(pc.Info_Packet_Sent__c==null || pc.Program_Authorization_Received__c==null || (pc.Financial_Responsibility_Waiver__c==null && pc.Caregiver_Responsibility_Received__c==null)){
                                        pc.status_reason__c = 'Awaiting Forms';
                                    
                                }else{
                                    
                                        if(pc.Estimated_Arrival__c>date.today()){
                                            pc.status_reason__c = 'Ready for Arrival';
                                        }
                                    
                                        if((pc.Estimated_Arrival__c<=date.today())&&pc.Actual_Departure__c==null){
                                            pc.status_reason__c = 'Patient at COE';
                                        }
                                }
                                 
                            }
                                   
                        }
                        
                    }
                }    
                
            }
            
        pc.Status_Roll_Up__c='';
        pc.Status_Roll_Up__c = setExternalStatusRollUp.theStatus(pc.status__c,pc.status_reason__c, pc.Status_Reason_Sub_List__c, pc.caregiver_name__c, pc.isConverted__c, pc.id);
        pc.Stage__c =          setExternalStatusRollUp.theStage(pc.status__c, pc.status_reason__c, pc.Status_Reason_Sub_List__c, pc.Status_Roll_Up__c, pc.isConverted__c);
        
        if(pc.Status_Roll_Up__c =='In Process' && pc.status_reason__c=='Nurse-Missing Info'){
            pc.status_reason__c='Nurse';
        }
        
        if(pc.Nicotine_Quit_Date__c != null && pc.Plan_of_Care_accepted_at_HDP__c==null && pc.Nicotine_Test_Scheduled__c!=null && pc.Nicotine_Test_Results__c==null){
            pc.Status_Reason__c = 'Awaiting Nicotine Clearance';
        }
        //End set Status Reasons
        return pc;
        
    }
    
    static patient_case__c autoStatusSubstanceDisorder(patient_case__c pc){
        if(pc.id == null){
            pc.status_reason__c = 'New';
            return pc;
        }
        
        if(pc.Estimated_Arrival__c == null || pc.Estimated_Departure__c == null){
            pc.status_reason__c = 'In Process';
            return pc;
        }
        
        if(pc.Caregiver_Name__c != null && (pc.Caregiver_Responsibility_Received__c == null || pc.Financial_Responsibility_Waiver__c == null)){
            pc.status_reason__c = 'Awaiting Forms';
            return pc;
        }
        
        if(pc.status__c=='Open' &&
           pc.Caregiver_Name__c != null && 
           pc.Caregiver_Responsibility_Received__c != null && 
           pc.Financial_Responsibility_Waiver__c != null && 
           pc.Estimated_Arrival__c != null && 
           pc.Estimated_Departure__c != null){
           
           
           if(pc.Travel_Itinerary_Notes__c>0){
               pc.status_reason__c = 'Ready for Arrival';
               return pc;
        
           }else{
               pc.status_reason__c = 'Awaiting Travel Confirmation';
               return pc;
           
           }
        
        }
        
        return pc;
    }
    
    static patient_case__c autoStatusDigitalPT(patient_case__c pc){
        
        if(pc.id == null){
            pc.status_reason__c = 'New';
            return pc;
        }
        if(pc.id == null && pc.Program_Completion_Date__c==null && pc.Last_Claim_Received__c==null && pc.Evaluation_Date__c==null){
            pc.status_reason__c = 'Awaiting Appointment Date Confirmations';
            return pc;
        }
        
        if(pc.Last_Claim_Paid__c!=null && pc.Program_Completion_Date__c!=null){
            pc.status__c='Completed';
            pc.status_reason__c='Claim Paid';
            return pc;
        }
        
        if(pc.Last_Claim_Received__c!=null){
            pc.status__c='Open';
            pc.status_reason__c='Claim Received/Appts in Progress';
            return pc;
        }
        
        if(pc.Evaluation_Date__c!=null){
            pc.status__c='Open';
            pc.status_reason__c='Claim Pending/Appts in Progress';
            return pc;
        }
        
        return pc;
    }
    
}