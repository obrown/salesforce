public with sharing class populationHealthBatchCMEligibility {
    public populationHealthBatchCMEligibility() {
    }

    public void checkEligibility(){
        set<id> idset = new set<id>();
        integer i = 0;

        for (Case_Management__c cm : [select Patient__c from Case_Management__c where Patient__r.Patient_Employer__r.Underwriter__c != null and softDelete__c = false and Case_Status__c != 'Closed']) {
            idset.add(cm.Patient__c);
            if (i == 99) {
                populationHealthFuture.eligbility(idset);
                idset.clear();
                i = 0;
            }
            else{
                i++;
            }
        }

        populationHealthFuture.eligbility(idset);
    }
}