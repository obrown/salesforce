/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class PaginatedSelectListTest {

    static testMethod void myUnitTest() {
        PaginatedSelectList codeOptions=new PaginatedSelectList();
                
        for(Code__c c: [select code__c, Code_Description__c,name,type__c from Code__c where type__c ='DRG' order by code__c asc]){
                    codeOptions.add(new SelectOption(c.id,c.code__c + ' ' + c.code_description__c));
                          
        }
        
        codeOptions.getSize();
        codeOptions.get(1);
        codeOptions.get0();
        codeOptions.get1();
        codeOptions.get2();
        codeOptions.get3();
        codeOptions.get4();
        codeOptions.get6();
        codeOptions.get7();
        codeOptions.get8();
        codeOptions.get9();
        codeOptions.get10();
        codeOptions.get11();
        codeOptions.get12();
        codeOptions.get13();
        codeOptions.get14();
        codeOptions.get15();
        codeOptions.get16();
        codeOptions.get17();
        codeOptions.get18();
        codeOptions.get19();
        
        codeOptions.getSelectlist();
        codeOptions.remove(1);
        codeOptions.clear();
        
        
    }
}