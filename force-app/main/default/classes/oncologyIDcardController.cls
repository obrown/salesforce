public with sharing class oncologyIDcardController {
    public oncologyCaseEncryptedData oc { get; set; }

    public Oncology__c obj { get; set; }

    public String objJSON;

    public String clientLogo { get; private set; }

    public String claimEmailAddress { get; set; }

    public String claimMailAddressLine1 { get; set; }

    public String claimMailAddressLine2 { get; set; }

    public String claimMailAddressLine3 { get; set; }

    public String effectiveDate { get; set; }

    public String expirationDate { get; set; }

    public String financialResponsibilityLanguage { get; set; }

    public String phone1 { get; set; }

    public String phone2 { get; set; }

    public String phone3 { get; set; }

    public String planLanguage { get; set; }

    public String program { get; set; }

    public oncologyIDCardController() {
        String cases = ApexPages.CurrentPage().getParameters().get('cases');
        ApexPages.CurrentPage().getParameters().put('cases',  null);

        oc = (oncologyCaseEncryptedData) JSON.deserialize(
            cases,
            oncologyCaseEncryptedData.class
        );

        obj = [
            select
                id,
                client_facility__r.client__r.Group_Number__c,
                client_facility__r.facility__r.name,
                Client_Name__c,
                client__r.logoDirectory__c,
                Employee_HealthPlan__r.Name,
                Estimated_Arrival_Date__c,
                Estimated_Departure_Date__c,
                Patient_Medical_Plan_Number__c,
                Plan_Type__c
            from Oncology__c
            where id = :oc.id
        ];

        oc.PatientIDNumber = obj.Patient_Medical_Plan_Number__c;
        clientLogo = '/logos/' + obj.client__r.logoDirectory__c;

        claimEmailAddress = 'newcoeclaim@contigohealth.com';
        claimMailAddressLine1 = 'Contigo Health-' + obj.Client_Name__c + ' COE';
        claimMailAddressLine2 = 'P.O. BOX 2582';
        claimMailAddressLine3 = 'Hudson, OH 44236';

        effectiveDate = 'N/A';
        expirationDate = 'N/A';

        // Member financial responsibility disclaimer shown when planLanguage isn't empty
        financialResponsibilityLanguage = '';

        // Phone # for Eligibility, Benefits, Travel, and Expense Money
        phone1 = '+1 (888) 463-3737';

        // Phone # for Care Coordination
        phone2 = '+1 (877) 286-3551';

        // Phone # for 24/7 Travel Support (American Express)
        phone3 = '+1 (646) 817-9847, #3';

        // Text shown at the bottom of the ID card's front face
        planLanguage = '';

        program = 'Oncology';

        if (obj.Employee_HealthPlan__r.Name != null) {
            // Performance optimization - circuit breaker
            Boolean shouldSkip = false;

            Set<String> plansWithHsaOrPpo = new Set<String>{
                'ACP (Banner)',
                'ACP (Mercy)',
                'ACP (Presbyterian)',
                'Advantage HSA',
                'Basic EPO 60',
                'Choice Account (HSA)',
                'Choice Account Plus (HSA)',
                'EPO',
                'Flex PPO',
                'HSA A Family Coverage (SISOFECN)',
                'HSA A Single Coverage (SISOAECN)',
                'HSA B (SISOBECN)',
                'HSA Family',
                'HSA Individual',
                'Local Plan',
                'Mercy St. Louis Local Plan (Mercy St. Louis ACP)',
                'Option 1 PPO',
                'Option 2 PPO',
                'PPO',
                'PPO (SISOPECN)',
                'Premier 80 HRA',
                'St. Luke\'s Local Plan',
                'Walmart Contribution Plan',
                'Walmart Premier Plan',
                'Walmart Saver Plan'
            };

            for (String plan : plansWithHsaOrPpo) {
                if (obj.Employee_HealthPlan__r.Name == plan) {
                    financialResponsibilityLanguage = 'You will not be charged for any covered services under the program unless you are enrolled in the HSA plan. If you are enrolled in the HSA plan, you will be responsible for any outstanding deductible balance and/or out-of-pocket maximum balance. Contact the administrator listed on your medical plan ID card to find your deductibles and out-of-pocket maximums.';
                    planLanguage = 'For non-HSA plan enrollees, there is no member cost-share (e.g. co-pay) under this program. For HSA plan enrollees, refer to your medical plan ID card for deductibles and out-of-pocket maximums.';
                    shouldSkip = true;
                }
            }

            if (shouldSkip == false) {
                Set<String> plansWithDeductible = new Set<String>{
                    'FPL Bargaining',
                    'HRA - Family/Employee & Spouse',
                    'HRA - Individual/EE & Child',
                    'HSA - Family/Employee & Spouse',
                    'HSA - Individual/EE & Child',
                    'HSA (EE and EE + 1)',
                    'HSA (Family)',
                    'HSA Plus (EE and EE + 1)',
                    'Jupiter Tequesta AC Plumbing Electric',
                    'PPO',
                    'Retiree Consumer 4000 - No Travel Benefits',
                    'Retiree Pre65 2000 Plan - No Travel Benefits',
                    'Retiree Pre65 Base - No Travel Benefits',
                    'Retiree Pre65 Coinsurance - No Travel Benefits',
                    'Retiree Pre65 Copay Plan - No Travel Benefits',
                    'Retiree Pre65 Enhanced - No Travel Benefits',
                    'Retiree Pre65 HDHP - No Travel Benefits',
                    'Retiree Pre65 II - No Travel Benefits',
                    'Retiree Pre65 Network Only - No Travel Benefits'
                };

                for (String plan : plansWithDeductible) {
                    if (obj.Employee_HealthPlan__r.Name == plan) {
                        financialResponsibilityLanguage = 'You will be responsible for any outstanding deductible balance and/or out-of-pocket maximum balance for covered services under this program. Contact the administrator listed on your medical plan ID card to find your deductibles and out-of-pocket maximums.';
                        planLanguage = 'There is a member cost-share (e.g. deductible only, or deductible and coinsurance) under this program. Refer to your medical plan ID card for deductibles and out-of-pocket maximums.';
                    }
                }
            }
        }

        if (obj.Estimated_Arrival_Date__c != null) {
            effectiveDate = obj.Estimated_Arrival_Date__c.month()
                                + '/'
                                + obj.Estimated_Arrival_Date__c.day()
                                + '/'
                                + obj.Estimated_Arrival_Date__c.year();
        }

        if (obj.Estimated_Departure_Date__c != null) {
            expirationDate = obj.Estimated_Departure_Date__c.month()
                                + '/'
                                + obj.Estimated_Departure_Date__c.day()
                                + '/'
                                + obj.Estimated_Departure_Date__c.year();
        }
    }
}
