@RestResource(urlMapping='/coe/v1/claimsReportingUpload')
global inherited sharing class claimsReportingUpload {
  
    class Response{
            string error;
            string errorMsg;
           
        }
        /*
        @HttpGet
        global static void getplanofcare() {
        }
        */
        
        @HttpPost
        global static void upsertClaims() {
            
            response result = new response();
            result.error='false';
            RestResponse res = RestContext.response;
            
            blob requestBodyBlob = RestContext.request.requestBody;
            string jsonString = requestBodyBlob.toString();
            jsonString = jsonString.replace('\"FDOS__c\":\"\"', '\"FDOS__c\":null');
            jsonString = jsonString.replace('\"Date_HP_Loaded__c\":\"\"', '\"Date_HP_Loaded__c\":null');
            jsonString = jsonString.replace('\"Date_HP_Paid__c\":\"\"', '\"Date_HP_Paid__c\":null');
            jsonString = jsonString.replace('\"TDOS__c\":\"\"', '\"TDOS__c\":null');
            jsonString = jsonString.replace('\"Date_Received__c\":\"\"', '\"Date_Received__c\":null');
            
            try{
              list<claimsReporting__c> claims = (list<claimsReporting__c>)json.deserialize(jsonString, list<claimsReporting__c>.class);
              
              upsert claims Claim_Number__c;
              res.statusCode = 200;
              
            }catch(exception e){
                result.error  = 'true';
                result.errorMsg  = e.getMessage();
                res.responseBody = Blob.valueOf(JSON.serialize(result));
                res.statusCode = 500;

            }               
        }
   
}