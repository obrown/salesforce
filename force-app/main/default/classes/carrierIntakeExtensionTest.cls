@isTest()
private class carrierIntakeExtensionTest{
    
     static testMethod void intakeTest(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         oncology__c oncology = t.oncology;
         
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); //pass in a new oncology record with no id
         
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         
         system.assert(oncology.id!=null);
         ApexPages.StandardController controller = new ApexPages.StandardController(oncology);
         carrierIntakeExtension intakeExt = new carrierIntakeExtension(controller);
         intakeExt.getrelOpts();
         intakeExt.gettimezones();
         intakeExt.getbtc();
         intakeExt.getphoneType();
         
         
         intakeExt.empFirstName = oncology.Employee_First_Name__c;
         intakeExt.empLastName = oncology.Employee_Last_Name__c;
         intakeExt.empDob = '01/01/'+date.today().addyears(-30);
         intakeExt.empAddress = oncology.Employee_Address__c;
         intakeExt.empEmail = oncology.Employee_Email_Address__c;
         intakeExt.empEmailConfirm = oncology.Employee_Email_Address__c;
         intakeExt.empAdditionalContact = 'Test';
         intakeExt.additionalInformation = 'Test';
         intakeExt.patFirstName = oncology.Patient_First_Name__c;
         intakeExt.patLastName = oncology.Patient_Last_Name__c;
         intakeExt.patDob = '01/01/'+date.today().addyears(-30);
         intakeExt.patAddress = oncology.Patient_Address__c;
         intakeExt.patEmail = oncology.Patient_Email_Address__c;
         intakeExt.patEmailConfirm = oncology.Patient_Email_Address__c;
         
         intakeExt.Pro.Associated_Facilities__c='na';
         intakeExt.Pro.City__c='Mesa';
         intakeExt.Pro.Credentials__c='MD';
         intakeExt.Pro.Facility_PCP__c=false;
         intakeExt.Pro.Facility_Reported_Physician__c=false;
         intakeExt.Pro.Fax_Number__c='480-555-5555';
         intakeExt.Pro.First_Name__c='James';
         intakeExt.Pro.Last_Name__c='Jones';
         intakeExt.Pro.Local_PCP__c=true;
         intakeExt.Pro.Network_Status__c='In Network';
         intakeExt.Pro.Network_Status_Verification_Date__c=system.today()+5;
         intakeExt.Pro.Phone_Number__c='480-555-5555';
         intakeExt.Pro.Provider_Type__c='COE Managing Physician';
         intakeExt.Pro.Specialty__c='Medical Oncologist';
         intakeExt.Pro.State__c='AZ';
         intakeExt.Pro.Street_Address__c='2500 S Power Rd';
         intakeExt.Pro.Zip_Code__c='85205';

         intakeExt.ProA.Associated_Facilities__c='na';
         intakeExt.ProA.City__c='Mesa';
         intakeExt.ProA.Credentials__c='MD';
         intakeExt.ProA.Facility_PCP__c=false;
         intakeExt.ProA.Facility_Reported_Physician__c=false;
         intakeExt.ProA.Fax_Number__c='480-555-5555';
         intakeExt.ProA.First_Name__c='James';
         intakeExt.ProA.Last_Name__c='Jones';
         intakeExt.ProA.Local_PCP__c=true;
         intakeExt.ProA.Network_Status__c='In Network';
         intakeExt.ProA.Network_Status_Verification_Date__c=system.today()+5;
         intakeExt.ProA.Phone_Number__c='480-555-5555';
         intakeExt.ProA.Provider_Type__c='COE Managing Physician';
         intakeExt.ProA.Specialty__c='Medical Oncologist';
         intakeExt.ProA.State__c='AZ';
         intakeExt.ProA.Street_Address__c='2500 S Power Rd';
         intakeExt.ProA.Zip_Code__c='85205';    
         
         intakeExt.isMemberPatient ='Yes';
         intakeExt.empAddySame ='Yes';
         
         intakeExt.validateCallBackTimeVF();
         
         intakeExt.mySubmit();     
         
        }
     
}