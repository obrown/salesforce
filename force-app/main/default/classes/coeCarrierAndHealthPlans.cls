public class coeCarrierAndHealthPlans{


    selectOption[] carriers;
    public map<string, string[]> healthPlanMap = new map<string, string[]>();
    id clientId;
    
    public map<string, string[]> gethealthPlanMap(){return healthPlanMap;}
    
    public static selectOption[] employeeHealthPlanPickList(id clientId, string clientCarrierId){
        
        selectOption[] soList = new selectOption[]{};
        soList.add(new selectOption('','--None--'));
        
        if(clientId==null){
            return soList;
        }
        
        //set<string> planNames = new set<string>();
        coeCarrierAndHealthPlans cchp = new coeCarrierAndHealthPlans(clientId);
        
        for(Client_Employee_Healthplan__c ehp : [select Name,Client_Carrier__r.name from Client_Employee_Healthplan__c where Client_Carrier__c = :clientCarrierId]){
            soList.add(new selectOption(ehp.id, ehp.name));
        }
        
        SelectOptionSorter.doSort(soList, SelectOptionSorter.FieldToSort.Label); 
        return soList;
    }
    
    //public static selectOption[] carriersPickList(id clientId, string carrierName){
    public static selectOption[] carriersPickList(id clientId, string carrierName,string state){    
    
        selectOption[] soList = new selectOption[]{};
        coeCarrierAndHealthPlans cchp = new coeCarrierAndHealthPlans(clientId);
        soList = cchp.getCarriers(carrierName,state);
        selectOptionSorter.doSort(soList, SelectOptionSorter.FieldToSort.Label); 
        return soList;
    }
    
    public coeCarrierAndHealthPlans(id clientId){
        this.clientId=clientId;
    } 
    
    public void setClientId(id clientId){
        this.clientId=clientId;
    }
    
    public selectOption[] getCarriers(string carrierName,string state){
    
        //map<id, string> carrierNameMap = new map<id, string>();
        if(clientId != null){
            carriers = new selectOption[]{};
            carriers.add(new selectOption('','--None--'));
            
            for(Client_Carrier__c carrier : [select Carrier__r.name,State__c,active__c  from Client_Carrier__c  where client__c =:clientId order by carrier__r.name asc]){

                if(carrier.id==carrierName || (carrier.active__c && carrier.State__c !=null &&carrier.State__c.contains(state))){
                    carriers.add(new selectoption(carrier.id, carrier.Carrier__r.name));
                }  
                
                
            }    
            
        }
        
        return carriers;
        
    }
    
    public string[] sethealthPlans(string carrierName){
        if(carriers==null || carriers.size()==0){return new string[]{'NA'};}
        return healthPlanMap.get(carrierName);
        
        
    }    

}