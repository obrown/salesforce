@isTest
private class coeDashboardTest {

    static testMethod void coeDashboardTest () {
    
        map<string, id> rtMap = new map<string, id>();
        
        for(RecordType rt : [select id, name from RecordType where isActive = true and sObjectType='Patient_Case__c']){
            rtMap.put(rt.name, rt.id);
        }
        
        patient_case__c[] pcList = new Patient_Case__c[]{}; 
        
        Patient_Case__c JointPC = new Patient_Case__c();
        JointPC.bid__c = '123456';
        JointPC.Employee_First_Name__c = 'Johnny';
        JointPC.Employee_Last_Name__c = 'Tester';
        JointPC.Patient_DOBe__c = string.valueof(date.today().addYears(-20));
        JointPC.Patient_Email_Address__c = 'something@something.com';
        JointPC.Employee_DOBe__c = string.valueof(date.today().addYears(-20));
        JointPC.Employee_Primary_Health_Plan_ID__c = '12345';
        JointPC.Relationship_to_the_Insured__c = 'Patient is Insured';
        JointPC.Patient_Preferred_Phone__c = 'Home';
        JointPC.Patient_Gender__c= 'Male'; 
        JointPC.Patient_City__c ='Mesa'; 
        JointPC.Patient_State__c='AZ';
        JointPC.recent_testing__c='rt';
        JointPC.caregiver_name__c ='Johnny Tester';
        JointPC.status__c='Open';
        JointPC.RecordTypeid = rtMap.get('Walmart Joint');
        JointPC.Ownerid = userInfo.getUserID();
        JointPC.Client_Name__c = 'Walmart';
        
        pcList.add(JointPC);
        Patient_Case__c SpinePC = JointPC.clone();
        SpinePC.RecordTypeid = rtMap.get('Walmart Spine');
        
        pcList.add(SpinePC);
        
        Patient_Case__c CardiacPC = JointPC.clone();
        Cardiacpc.RecordTypeid = rtMap.get('Walmart Cardiac');
        pcList.add(CardiacPC);
        
        Patient_Case__c patientatCOEPC = JointPC.clone();
        patientatCOEPC.isConverted__c = true;
        patientatCOEPC.status_reason__c = 'Patient at COE';
        pcList.add(patientatCOEPC);
        
        Patient_Case__c readyForArrivalPC = JointPC.clone();
        patientatCOEPC.isConverted__c = true;
        patientatCOEPC.status_reason__c = 'Ready for Arrival';
        pcList.add(readyForArrivalPC);
        
        Patient_Case__c claimProcessingPC = JointPC.clone();
        patientatCOEPC.isConverted__c = true;
        patientatCOEPC.status_reason__c = 'Claim Pending';
        pcList.add(claimProcessingPC);
        
        Patient_Case__c MemberAdvocateCollectionPC = JointPC.clone();
        patientatCOEPC.isConverted__c = true;
        patientatCOEPC.status_reason__c = 'Awaiting Appointment Date Confirmations';
        pcList.add(MemberAdvocateCollectionPC);
        
        insert pclist;

        
        coeDashboard dash = new coeDashboard();
        Apexpages.CurrentPage().getParameters().put('sojDate',string.valueof(date.today()-1));
        Apexpages.CurrentPage().getParameters().put('eojDate',string.valueof(date.today()-1));
        dash.setOpenSpineDates();
        dash.setOpenJointDates();
        dash.setOpenCardiacDates();

        Apexpages.CurrentPage().getParameters().put('scType','Joint');
        Apexpages.CurrentPage().getParameters().put('rType','CM');
        
        dash.setSC();
        
    }
}