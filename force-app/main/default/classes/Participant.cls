public class Participant{
    
        public string Name {get; set;} 
        public string eMail {get; set;}
        public string phone {get; set;}
        public string mobile {get; set;}
        public dateTime createdDate {get; set;}
        public string contactType {get; set;}
        public string recordID {get; set;}
        public string parentID {get; set;}
        public boolean isSelected {get; set;}
        
        public Participant(){     }
        
        public Participant(string Name, string eMail, string phone, string mobile, dateTime createdDate, string contactType, string recordID){
        
            this.Name = Name;
            this.eMail = eMail;
            this.phone= phone;
            this.mobile= mobile;
            this.createdDate= createdDate;
            this.contactType= contactType;
            this.recordID= recordID;
        }
        
        public Participant(string Name, string eMail, string phone, string mobile, dateTime createdDate, string contactType, string recordID, string parentID){
        
            this.Name = Name;
            this.eMail = eMail;
            this.phone= phone;
            this.mobile= mobile;
            this.createdDate= createdDate;
            this.contactType= contactType;
            this.recordID= recordID;
            this.parentID= parentID;
        }
        
    }