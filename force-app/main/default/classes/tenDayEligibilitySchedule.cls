global class tenDayEligibilitySchedule implements Schedulable, Database.AllowsCallouts{
    
    global void execute(SchedulableContext go){
        tenDayEligibilityCheck tdec = new tenDayEligibilityCheck();
        tdec.run();
        
    }
    
}