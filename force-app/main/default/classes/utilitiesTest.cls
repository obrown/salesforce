@isTest(seealldata=true)
private class utilitiesTest 
{

    static testMethod void utilitiesALL() 
    {
        
        id theID;
        for(GroupMember g : [select id from GroupMember])
        {
            if(string.valueof(g.id).left(3)=='00G')
            {
                theid = g.id;
            }
        }
        if(theid==null)theid=id.valueof('00GA0000000ViuU');
                
        utilities.emailRec(theid);
        utilities.emailRec(userInfo.getProfileId());
        
        utilities.convertCRtoBR('test\ntest\n');    
        utilities.errorMessageText('test_EXCEPTIONtestestestestestestest.[testestestest');          
        utilities.errormessageTextRF(' missing:testestestestetsesttestest]:');
        utilities.authcontactEmail('Lowes', 'cardiac', 'N/A');  
        utilities.authcontactEmail('Lowes', 'cardiac', 'HDP-00000'); 
        utilities.authcontactEmail('Lowes', 'spine', 'HDP-00000'); 
        utilities.authcontactEmail('Kohls', 'spine', 'HDP-00000'); 
        utilities.authcontactEmail('Walmart', 'spine', 'HDP-00000');  
        utilities.authcontactEmail('PepsiCo', 'spine', 'HDP-00000');
        
        utilities.contactEmail('Walmart', 'Cleveland Clinic');
        utilities.contactEmail('Walmart', 'Geisinger');
        utilities.contactEmail('Walmart', 'Mercy Springfield');
        utilities.contactEmail('Walmart', 'Scott & White');
        utilities.contactEmail('Walmart', 'Virginia Mason');
        utilities.contactEmail('Lowes', 'cardiac');
        utilities.contactEmail('Lowes', 'spine');
        utilities.contactEmail('PepsiCo', 'spine');
        utilities.contactEmail('Kohls', 'spine');
        
        utilities.RecordTypeName([select id from Recordtype where isactive = true and sObjectType='Patient_Case__c' limit 1].id, 'Patient_Case__c');
        /*
        utilities.RecordTypeIDbyName('Walmart Cardiac');
        utilities.RecordTypeIDbyName('Walmart Spine');
        utilities.RecordTypeIDbyName('Walmart Joint');
        utilities.RecordTypeIDbyName('Lowes Cardiac');
        utilities.RecordTypeIDbyName('Lowes Spine');
        utilities.RecordTypeIDbyName('Lowes Joint');
        utilities.RecordTypeIDbyName('PepsiCo Cardiac');
        utilities.RecordTypeIDbyName('PepsiCo Joint');
        utilities.RecordTypeIDbyName('HCR ManorCare');
        utilities.RecordTypeIDbyName('McKesson Joint');
        utilities.RecordTypeIDbyName('McKet');
        */
        Messaging.SingleEmailMessage mail = utilities.email('something@hdplus.com','something@hdplus.com','something@hdplus.com','test','test','test');
        mail = utilities.email('something@hdplus.com,something@hdplus.com','something@hdplus.com,something@hdplus.com','something@hdplus.com,something@hdplus.com','test','test','test');
        
        utilities.formatUSdate(date.today());
        utilities.formatUSdate(string.valueof(date.today()));
        utilities.isInternal();
        
        claimsReporting__c cr = new claimsReporting__c();
        cr.Claim_Number__c = '12345789';
        insert cr;
        
        
    }
}