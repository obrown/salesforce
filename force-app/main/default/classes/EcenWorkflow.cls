public without sharing class EcenWorkflow{
    
    public class result {
        public boolean result {get; set;}
        public string message {get; set;}
    }
    
    public static result checkRequiredToContinueIntake(patient_case__c pc, map<string, string> labelMap){
    
        result r = new result();
        r.Result=true;
        string message='';
    
    if(pc.Employee_Gender__c==null || pc.Employee_Gender__c==''){
        message += 'Employee Gender<BR>';
    }
    
    
    if(pc.Callback_Type__c==null || pc.Callback_Type__c==''){
        message += 'Callback Type<BR>';
    }
        
    if(pc.Language_Spoken__c==null || pc.Language_Spoken__c==''){
        message += 'Language Spoken<BR>';
    }
    
    if(pc.bid__c==null || pc.bid__c==''){
        message += labelMap.get('BIDtext') + '<BR>';
    }
        
    if(pc.Referral_Source__c==null || pc.Referral_Source__c==''){
        message += 'Referral Source<BR>';
    }
        
        if(pc.Employee_First_Name__c==null || pc.Employee_First_Name__c==''){
            message += 'Employee First Name<BR>';
        }
        
        if(pc.Employee_Last_Name__c==null || pc.Employee_Last_Name__c==''){
            message += 'Employee Last Name<BR>';
        }
        
        if(pc.Employee_DOBe__c==null || pc.Employee_DOBe__c==''){
            message += 'Employee DOB<BR>';
        }
        
       
    if(pc.Patient_Gender__c==null || pc.Patient_Gender__c==''){
            message += 'Patient Gender<BR>';
        }
    
    if(pc.Employee_Street__c==null || pc.Employee_Street__c==''){
            message += 'Employee Street<BR>';
        }
        
        if(pc.Employee_City__c==null || pc.Employee_City__c==''){
            message += 'Employee City<BR>';
        }
        
        if(pc.Employee_State__c==null || pc.Employee_State__c==''){
            message += 'Employee State<BR>';
        }
        
        if(pc.Employee_Zip_Postal_Code__c==null || pc.Employee_Zip_Postal_Code__c==''){
            message += 'Employee Zip/Postal Code<BR>';
        }
        
        if(pc.Patient_Street__c==null || pc.Patient_Street__c==''){
            message += 'Patient Street<BR>';
        }
        
        if(pc.Patient_City__c==null || pc.Patient_City__c==''){
            message += 'Patient City<BR>';
        }
        
        if(pc.Patient_State__c==null || pc.Patient_State__c==''){
            message += 'Patient State<BR>';
        }
        
        if(pc.Patient_Zip_Postal_Code__c==null || pc.Patient_Zip_Postal_Code__c==''){
            message += 'Patient Zip/Postal Code<BR>';
        }
        
        if(message.length()>0){
            r.Result=false;
            r.Message='Fields still required to continue<BR><BR>'+message;
        }
        
        return r;
    }
            
    public static result checkRequiredToSaveFields (patient_case__c pc){
        
        result r = new result();
        r.Result=false;
        string message='';
        
        if(pc.caller_name__c==null || pc.caller_name__c==''){
            message = 'Caller Name<BR>';
        }
        
        if(pc.Caller_relationship_to_the_patient__c==null || pc.Caller_relationship_to_the_patient__c==''){
            message += 'Caller relationship to the patient<BR>';
        }
        
        if(pc.Callback_Number__c==null || pc.Callback_Number__c==''){
            message += 'Callback Number<BR>';
        }
        
        if(pc.Initial_Call_Discussion__c==null || pc.Initial_Call_Discussion__c==''){
            message += 'Initial Call Discussion<BR>';
        }
        
        if(pc.Patient_First_Name__c==null || pc.Patient_First_Name__c==''){
            message += 'Patient First Name<BR>';
        }
        
        if(pc.Patient_Last_Name__c==null || pc.Patient_Last_Name__c==''){
            message += 'Patient Last Name<BR>';
        }
        
        if(pc.Patient_DOBe__c==null || pc.Patient_DOBe__c==''){
            message += 'Patient DOB<BR>';
        }
        
        if((pc.Patient_Home_Phone__c==null || pc.Patient_Home_Phone__c=='') && 
           (pc.Patient_Mobile__c==null || pc.Patient_Mobile__c=='') && 
           (pc.Patient_Work_Phone__c == null || pc.Patient_Work_Phone__c==''))
        {
        
            message += 'At least one patient contact number<BR>';
        
        }
        
        if(message.length()>0){
            r.Result=true;
            r.Message=message;
        }
        
        return r;
        
    }
    
}