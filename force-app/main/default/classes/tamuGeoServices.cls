public class tamuGeoServices{


    public static string lookupAddress(string a, string c, string s, string z){
        string address = EncodingUtil.UrlEncode(a,'UTF-8');
        string city = EncodingUtil.UrlEncode(c,'UTF-8');
        string state = EncodingUtil.UrlEncode(s,'UTF-8');
        string zip = EncodingUtil.UrlEncode(z,'UTF-8');
        
        string myParams = 'streetAddress='+address+'&city='+city+'&state='+state+'&zip='+zip;
        string requiredParams = '&apikey='+ floridaKeys__c.getinstance().TAMU_ApiKey__c +'&format=json&version=4.01';   
        
        string urlparams = myParams+requiredParams;
        
        Http endpoint = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://geoservices.tamu.edu/Services/Geocode/WebService/GeocoderWebServiceHttpNonParsed_V04_01.aspx?'+urlparams);
        req.setMethod('GET');
        string returnValue;
        try{
            HttpResponse res = new HttpResponse();
            
            if(!Test.IsRunningTest()){
                res = endpoint.send(req); 
        
                if(res.getStatusCode()!=200){
                    return 'error';    
                }
                
                returnValue=res.getbody();
            }else{
                return '';
            }
            
            
        }catch(System.CalloutException e){
            return null;
        }
               
        
        return returnValue;
    }
}