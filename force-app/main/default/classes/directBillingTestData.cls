public with sharing class directBillingTestData{

    public static void loadData(){
        
        /* Set Up records for test case */
        
        Client__c client = new Client__c(Corporate_Name__c='University Hospitals',name='University Hospitals', Underwriter__c='034', Group_Number__c='UHH');
        insert client;
        
        string year = string.valueof(date.today().year());
        Client_Benefit_Year__c cby = new Client_Benefit_Year__c(client__c=client.id, name=year, start__c=date.valueof(year+'-1-1'), end__c=date.valueof(year+'-12-31')  );    
        insert cby;
        
        id biWeekly = Schema.SObjectType.DB_Pay_Schedule__c.getRecordTypeInfosByName().get('Bi-Weekly').getRecordTypeId();
        
        DB_Pay_Schedule__c[] payDates = new DB_Pay_Schedule__c[]{};
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='1/6/'+year,pay_end__c=date.valueof(year+'-'+'01-06'),pay_date__c=date.valueof(year+'-'+'01-08')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='1/20/'+year,pay_end__c=date.valueof(year+'-'+'01-20'),pay_date__c=date.valueof(year+'-'+'01-22')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='2/3/'+year,pay_end__c=date.valueof(year+'-'+'02-03'),pay_date__c=date.valueof(year+'-'+'02-05')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='2/17/'+year,pay_end__c=date.valueof(year+'-'+'02-17'),pay_date__c=date.valueof(year+'-'+'02-19')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='3/3/'+year,pay_end__c=date.valueof(year+'-'+'03-03'),pay_date__c=date.valueof(year+'-'+'03-05')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='3/17/'+year,pay_end__c=date.valueof(year+'-'+'03-17'),pay_date__c=date.valueof(year+'-'+'03-19')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='3/31/'+year,pay_end__c=date.valueof(year+'-'+'03-31'),pay_date__c=date.valueof(year+'-'+'03-31'),FSA_Only__c=true));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='4/14/'+year,pay_end__c=date.valueof(year+'-'+'04-14'),pay_date__c=date.valueof(year+'-'+'04-16')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='4/28/'+year,pay_end__c=date.valueof(year+'-'+'04-28'),pay_date__c=date.valueof(year+'-'+'04-30')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='5/12/'+year,pay_end__c=date.valueof(year+'-'+'05-12'),pay_date__c=date.valueof(year+'-'+'05-14')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='5/26/'+year,pay_end__c=date.valueof(year+'-'+'05-26'),pay_date__c=date.valueof(year+'-'+'05-28')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='6/9/'+year,pay_end__c=date.valueof(year+'-'+'06-09'),pay_date__c=date.valueof(year+'-'+'06-11')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='6/23/'+year,pay_end__c=date.valueof(year+'-'+'06-23'),pay_date__c=date.valueof(year+'-'+'06-25')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='7/7/'+year,pay_end__c=date.valueof(year+'-'+'07-07'),pay_date__c=date.valueof(year+'-'+'07-09')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='7/21/'+year,pay_end__c=date.valueof(year+'-'+'07-21'),pay_date__c=date.valueof(year+'-'+'07-23')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='8/4/'+year,pay_end__c=date.valueof(year+'-'+'08-04'),pay_date__c=date.valueof(year+'-'+'08-06')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='8/18/'+year,pay_end__c=date.valueof(year+'-'+'08-18'),pay_date__c=date.valueof(year+'-'+'08-20')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='9/1/'+year,pay_end__c=date.valueof(year+'-'+'09-01'),pay_date__c=date.valueof(year+'-'+'09-03')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='9/15/'+year,pay_end__c=date.valueof(year+'-'+'09-15'),pay_date__c=date.valueof(year+'-'+'09-17')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='9/29/'+year,pay_end__c=date.valueof(year+'-'+'09-29'),pay_date__c=date.valueof(year+'-'+'09-29'),FSA_Only__c=true));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='10/13'+year,pay_end__c=date.valueof(year+'-'+'10-13'),pay_date__c=date.valueof(year+'-'+'10-15')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='10/27/'+year,pay_end__c=date.valueof(year+'-'+'10-27'),pay_date__c=date.valueof(year+'-'+'10-29')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='11/10/'+year,pay_end__c=date.valueof(year+'-'+'11-10'),pay_date__c=date.valueof(year+'-'+'11-12')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='11/24/'+year,pay_end__c=date.valueof(year+'-'+'11-24'),pay_date__c=date.valueof(year+'-'+'11-26')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='12/8/'+year,pay_end__c=date.valueof(year+'-'+'12-8'),pay_date__c=date.valueof(year+'-'+'12-10')));
        payDates.add(new DB_Pay_Schedule__c(recordtypeid=biweekly,client__c=client.id, name='12/22/'+year,pay_end__c=date.valueof(year+'-'+'12-22'),pay_date__c=date.valueof(year+'-'+'12-24')));
        
        insert payDates;
        
        DB_Rate__c[] dbRates = new DB_Rate__c[]{};
        id medicalRT = Schema.SObjectType.DB_Rate__c.getRecordTypeInfosByName().get('Medical').getRecordTypeId();
        id medicalParmaRT = Schema.SObjectType.DB_Rate__c.getRecordTypeInfosByName().get('Medical - Parma').getRecordTypeId();
        id dentalRT = Schema.SObjectType.DB_Rate__c.getRecordTypeInfosByName().get('Dental').getRecordTypeId();
        id visionRT = Schema.SObjectType.DB_Rate__c.getRecordTypeInfosByName().get('Vision').getRecordTypeId();
        id spouseLifeRT = Schema.SObjectType.DB_Rate__c.getRecordTypeInfosByName().get('Spouse Life').getRecordTypeId();
        id dependentLifeRT = Schema.SObjectType.DB_Rate__c.getRecordTypeInfosByName().get('Dependent Life').getRecordTypeId();
        id personalAccidentRT = Schema.SObjectType.DB_Rate__c.getRecordTypeInfosByName().get('Voluntary Personal Accident').getRecordTypeId();
        
        dbRates.add(new DB_Rate__c(name='Consumer Select PPO Full Non-Tobacco',Client_Benefit_Year__c=cby.id, recordtypeid=medicalRT));
        dbRates.add(new DB_Rate__c(name='Consumer Select PPO Full Tobacco',Client_Benefit_Year__c=cby.id, recordtypeid=medicalRT));
        dbRates.add(new DB_Rate__c(name='Traditional PPO Full Non-Tobacco',Client_Benefit_Year__c=cby.id, recordtypeid=medicalRT));
        dbRates.add(new DB_Rate__c(name='Consumer Select PPO Part Non-Tobacco',Client_Benefit_Year__c=cby.id, recordtypeid=medicalRT));
        dbRates.add(new DB_Rate__c(name='Consumer Select PPO Part Tobacco',Client_Benefit_Year__c=cby.id, recordtypeid=medicalRT));
        dbRates.add(new DB_Rate__c(name='Traditional PPO Part Non-Tobacco',Client_Benefit_Year__c=cby.id, recordtypeid=medicalRT));
        dbRates.add(new DB_Rate__c(name='Traditional PPO Part Tobacco',Client_Benefit_Year__c=cby.id, recordtypeid=medicalRT));
        dbRates.add(new DB_Rate__c(name='Waive',Client_Benefit_Year__c=cby.id, recordtypeid=medicalRT));
        
        dbRates.add(new DB_Rate__c(name='Post-65 Retiree',Client_Benefit_Year__c=cby.id, recordtypeid=medicalParmaRT));
        dbRates.add(new DB_Rate__c(name='Pre-65 Retiree',Client_Benefit_Year__c=cby.id, recordtypeid=medicalParmaRT));
        
        dbRates.add(new DB_Rate__c(name='Dental PPO Full',Client_Benefit_Year__c=cby.id, recordtypeid=dentalRT));
        dbRates.add(new DB_Rate__c(name='Dental PPO Part',Client_Benefit_Year__c=cby.id, recordtypeid=dentalRT));
        dbRates.add(new DB_Rate__c(name='DHMO Full',Client_Benefit_Year__c=cby.id, recordtypeid=dentalRT));
        dbRates.add(new DB_Rate__c(name='DHMO Part',Client_Benefit_Year__c=cby.id, recordtypeid=dentalRT));
        dbRates.add(new DB_Rate__c(name='Waive',Client_Benefit_Year__c=cby.id, recordtypeid=dentalRT));
        
        dbRates.add(new DB_Rate__c(name='Vision',Client_Benefit_Year__c=cby.id, recordtypeid=visionRT));
        
        dbRates.add(new DB_Rate__c(name='Spouse Life',Client_Benefit_Year__c=cby.id, recordtypeid=spouseLifeRT));
        dbRates.add(new DB_Rate__c(name='Dependent Life',Client_Benefit_Year__c=cby.id, recordtypeid=dependentLifeRT));
        dbRates.add(new DB_Rate__c(name='Personal Accident',Client_Benefit_Year__c=cby.id, recordtypeid=personalAccidentRT));
        
        insert dbRates;
        
        DB_Rate_Tier__c[] rateTiers = new DB_Rate_Tier__c[]{};
        
        for(DB_Rate__c rate : dbRates){
            
            if(rate.recordtypeid==spouseLifeRT){
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Waive', Rate__c=0.00));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='$5,000', Rate__c=0.63));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='$10,000', Rate__c=1.37));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='$30,000', Rate__c=2.32));
            
            }else if(rate.recordtypeid==dependentLifeRT){
                
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Waive', Rate__c=0.00));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='$5,000 Child(ren) Only', Rate__c=0.32));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='$10,000 Child(ren) Only', Rate__c=0.64));
            
            }else if(rate.recordtypeid==personalAccidentRT){
            
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Waive', Rate__c=0.00));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=8.13));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=12.31));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=15.84));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=19.96));
        
            }else if(rate.recordtypeid==visionRT){
            
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Waive', Rate__c=0.00));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=4.42));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + 1', Rate__c=8.10));
                rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + 2 or more', Rate__c=11.79));
        
            }else if(rate.recordtypeid==dentalRT){
                if(rate.name=='Dental PPO Full'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=5.23));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=8.32));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=10.36));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=15.67));
                
                }else if(rate.name=='Dental PPO Part'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=9.95));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=15.80));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=19.67));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=29.77));
                
                }else if(rate.name=='DHMO Full'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=2.36));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=3.75));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=4.66));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=7.08));
                
                }else if(rate.name=='DHMO Part'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=4.49));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=7.13));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=8.86));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=13.45));
                
                }else if(rate.name=='Waive'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Waive', Rate__c=0.00));
                    
                }
                
            }else if(rate.recordtypeid==medicalRT){
                if(rate.name=='Consumer Select PPO Full Non-Tobacco'){
                
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=50.77));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=96.34));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=112.94));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=157.66));
                
                }else if(rate.name=='Consumer Select PPO Full Tobacco'){
                
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=75.77));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=121.34));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=137.94));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=182.66));
                
                }else if(rate.name=='Traditional PPO Full Non-Tobacco'){
                
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=78.88));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=163.99));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=180.61));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=265.71));
                
                }else if(rate.name=='Consumer Select PPO Part Non-Tobacco'){
                
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=83.92));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=166.78));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=186.50));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=279.54));
                
                }else if(rate.name=='Consumer Select PPO Part Tobacco'){
                
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=108.92));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=191.78));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=211.50));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=304.54));
                
                }else if(rate.name=='Traditional PPO Part Non-Tobacco'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=144.27));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=304.11));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=329.03));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=500.28));
                
                }else if(rate.name=='Traditional PPO Part Tobacco'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee Only', Rate__c=169.27 ));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Spouse', Rate__c=329.11));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Child(ren)', Rate__c=354.03));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Employee + Family', Rate__c=525.28));
                
                }else if(rate.name=='Waive'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Waive', Rate__c=0.00));
                
                }
                
            }else if(rate.recordtypeid==medicalParmaRT){
                if(rate.Name=='Pre-65 Retiree'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Retiree Only', Rate__c=910.80));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Retiree + Spouse', Rate__c=910.80));
                }else if(rate.Name=='Post-65 Retiree'){
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Retiree Only', Rate__c=162.56));
                    rateTiers.add(new DB_Rate_Tier__c(DB_Rates__c=rate.id, name='Retiree + Spouse', Rate__c=325.13));
                    
                }
            }               
                        
        }
        
        insert rateTiers;
        
        /* End of setup */
    }
    
}