public without sharing class utilizationManagementRedirect{

    public utilizationManagementRedirect(ApexPages.StandardController controller) {

    }




public pagereference redirect(){
    
    string umID = ApexPages.CurrentPage().getParameters().get('id');
    string patientID = [select Patient__c from Utilization_Management__c where id = :umId].Patient__c;
    pagereference pageref = new pagereference('/apex/populationHealthPatient?patientRecordId='+patientID+'&loadum='+umId);
    return pageref;
}

}