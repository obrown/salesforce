public with sharing class providerPortalTaskWorkflow{

    public result result {get; private set;}
    
    public class result{
        public boolean isError;
        public string errorMsg;
        public Patient_Case_Task__c[] tasks;
    }
    
    public providerPortalTaskWorkflow(){
        result = new result();
    }
    
    public static result saveTasks(string jsonBody, string caseName){ 
        providerPortalTaskWorkflow taskWorkflow = new providerPortalTaskWorkflow();
        return taskWorkflow.savePatientCaseTasks(jsonBody, caseName);
    }
    
    public result savePatientCaseTasks(string jsonBody, string caseName){
        
        id patientCase_id = [select id from patient_case__c where name = :caseName].id;
        result.isError = false;
        patient_case_task__c[] pct_tasks = new patient_case_task__c[]{};
        patient_case_task__c[] existing_pct_tasks = new patient_case_task__c[]{};
        existing_pct_tasks = [select id from patient_case_task__c where patient_case__c = :patientCase_id ];
        
        try{
            jsonBody = jsonBody.replace('"completedDate__c":"",','').replace('"dueDate__c":"",','');
            pct_tasks= (patient_case_task__c[])JSON.deserialize(jsonBody, List<patient_case_task__c>.class);
            
            //New tasks that come in from the portal may not/will not have a patient case id associated with them. 
            
            for(patient_case_task__c pct: pct_tasks){
                if(pct.patient_case__c==null  && pct.id==null){
                    pct.patient_case__c= patientCase_id;
                }
                if(pct.dueDate__c!=''){
                    pct.dueDateDate__c =stringToDate(pct.dueDate__c);
                }
                pct.Visible_to_Facility__c= true;
            }
            
        }catch(exception e){
            system.debug(e.getMessage());
            if(pct_tasks == null || pct_tasks.isEmpty()){
                result.isError = true;
                result.errorMsg = e.getMessage();
                result.tasks = new Patient_Case_Task__c[]{};
                return result;
            }
        }    
        
        map<string, task> taskMap = new map<string, task>();
        
        for(patient_case_task__c pct : pct_tasks){
            if(pct.status__c=='Complete' && pct.completedDate__c==null){
                pct.completedDate__c = date.today();
            }
        }
        
        try{
           upsert pct_tasks; 
           set<id> newTasksId = new set<id>();
           
           for(patient_case_task__c pct : pct_tasks){
               newTasksId.add(pct.id);
           }
           
           patient_case_task__c[] tobedeleted = new patient_case_task__c[]{};
           
           for(patient_case_task__c pct : existing_pct_tasks){
               if(!newTasksId.contains(pct.id)){
                   tobedeleted.add(new patient_case_task__c(id=pct.id));
               }
           }
           
           delete tobedeleted;
           
        }catch(dmlException e){
           system.debug(e.getDMLMessage(0));
           result.isError = true;
           result.errorMsg = e.getMessage();
            result.tasks = new Patient_Case_Task__c[]{};
                return result; 
        }
        result.tasks=    pct_tasks;
        return result;
    }
        
    public static result getTasks(string caseName){   
        
     // List<List<SObject>> sCase = [FIND :caseName IN Name Fields Returning Patient_Case__c(client_facility__r.procedure__r.name, client_facility__r.client__r.name), Oncology__c];
        providerPortalTaskWorkflow taskWorkflow = new providerPortalTaskWorkflow();
        patient_case__c patient_case = new patient_case__c();
        try{
            patient_case = [select client_facility__r.procedure__r.name, client_facility__r.client__r.name from patient_case__c where name = :caseName];
        
        }catch(exception e){
            system.debug(e.getMessage());
            taskWorkflow.result.isError=true;
            taskWorkflow.result.errorMsg='No case found';
            taskWorkflow.result.tasks = new Patient_Case_Task__c[]{};
            return taskWorkflow.result;
                
        }
        
            //string sObjectType;
            //TODO: This may need to be updated. We are searching on a unique record name, so if there is a match there should only be 1 record
            //sObjectType = string.valueof(sCase[0][0].getsObjectType());
            //if(sObjectType=='Patient_Case__c'){
                //return taskWorkflow.getPatientCaseTasks(sCase[0][0]);
                return taskWorkflow.getPatientCaseTasks(patient_case);
                
            //}
       
    }

    public result getPatientCaseTasks(SObject pc){
        
        task[] tasks = new task[]{};
        ecen_tasks ecen_tasks = new ecen_tasks(true);
        ecen_tasks.result create_tasks_result = ecen_tasks.getPatientCaseTasks((patient_case__c)pc);
        
        if(create_tasks_result.isError){
            result.isError=true;
            result.errorMsg=create_tasks_result.errorMsg;
            result.tasks = null;
            return result;
        }
        
        for(Patient_Case_Task__c  task : create_tasks_result.tasks){
            if(task.dueDateDate__c!=null){
                task.dueDate__c = task.dueDateDate__c.month()+'/'+task.dueDateDate__c.day()+'/'+task.dueDateDate__c.year();
            }
        }
        
        result.isError=false;
        result.tasks=create_tasks_result.tasks;
        return result;
    }
    
    date stringToDate(string val){
        string str_d = val;
        system.debug(str_d);
        try{
            string[] arr = str_d.split('/');
            date d = date.valueof(arr[2]+'-'+arr[0]+'-'+arr[1]);
            system.debug(d);
            
            return d;
        }catch(exception e){
            system.debug(e.getMessage());
            return null;
        }
    }

}