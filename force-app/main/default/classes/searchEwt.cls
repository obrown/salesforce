public with sharing class searchEwt{
    
    class search{
        string intakeNumber;
        string memberId;
        string ssn;
        string firstName;
        string lastName;
    }
    search search;
    public Operations_Work_Task__c[] lstEwt {get; private set;}
    public string searchDesc {get; private set;}
    
    
    public searchEwt(){
        string searchBy = ApexPages.currentPage().getParameters().get('st');
        searchDesc ='';
        string[] searchArr = new string[]{};
        if(searchBy !=null){
            search = new search();
            search = (search)json.deserialize(searchBy, search.class);
            
            if(search.intakeNumber!=''){
                searchArr.add('Intake Number = ' + search.intakeNumber);
            }
            
            if(search.ssn!=''){
                searchArr.add('SSN = ' + search.ssn);
            }
            
            if(search.memberId!=''){
                searchArr.add('Member Id = ' + search.memberId);
            }
           
            if(search.firstName!=''){
                searchArr.add('First Name = ' + search.firstName);
            }
            
            if(search.lastName!=''){
                searchArr.add('Last Name = ' + search.lastName);
            }
            
        }
        
        for(string s : searchArr){
            searchDesc += s + ' and ';
        }
        
        searchDesc = searchDesc.removeEnd(' and ');
        search();
    }
    
    void search(){
        boolean nameSearch=false;
        set<id> ids = new set<id>();
        string[] searchFields = new string[]{};
        lstEWT = new Operations_Work_Task__c[]{};
        system.debug('search: '+search);
        if(search.ssn==''){
            search.ssn='##';
        }
        
        if(search.memberId==''){
            search.memberId='##';
        }
        
        if(search.lastName==''){
            search.lastName='##';
        }
        
        if(search.firstName==''){
            search.firstName='##';
        }
        
        if(search.intakeNumber==''){
            string searchBy =search.ssn+'~~'+search.memberId+'~~'+search.firstName+'~~'+search.lastName;
            for(Operations_Work_Task__c owt: [select Work_Task_Reason__c,Patient_Fname__c,Patient_Lname__c,Member_Number__c,Name,Group__c,EESSN__c,createdDate from Operations_Work_Task__c order by createdDate desc limit 50000]){
                string record = '';
                
                if(search.memberId=='##' || owt.Member_Number__c==''||owt.Member_Number__c==null){
                    record+= '##~~';
                }else{
                    record+= owt.Member_Number__c.toLowerCase()+'~~';
                }
                
                if(search.firstName=='##' || owt.Patient_Fname__c==''||owt.Patient_Fname__c==null){
                    record+= '##~~';
                }else{
                    record+= owt.Patient_Fname__c.toLowerCase()+'~~';
                }
                
                if(search.lastName=='##' || owt.Patient_Lname__c==''||owt.Patient_Lname__c==null){
                     record+= '##~~';
                }else{
                    record+= owt.Patient_Lname__c.toLowerCase()+'~~';
                }
                
                if(search.ssn=='##' || owt.EESSN__c==''||owt.EESSN__c==null){
                    record+= '##~~';
                }else{
                    record+= owt.EESSN__c+'~~';
                }
                system.debug(record);
                if(record.contains(search.ssn) && record.contains(search.memberId.toLowerCase()) && record.contains(search.lastName.toLowerCase()) && record.contains(search.firstName.toLowerCase())){
                    lstEWT.add(owt);
                }
            }
            
            
        }else{
            lstEwt = new Operations_Work_Task__c[]{};
            lstEWT = [select Work_Task_Reason__c,Patient_Fname__c,Patient_Lname__c,Member_Number__c,Name,Group__c,EESSN__c,createdDate from Operations_Work_Task__c where name like :'%'+search.intakeNumber+'%'];
        }
       
    
    }
    
}