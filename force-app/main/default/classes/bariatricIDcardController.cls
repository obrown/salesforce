public with sharing class bariatricIDcardController {
    public bariatricCaseEncryptedData bs { get; set; }

    public Bariatric_Stage__c obj { get; set; }

    public String objJSON;

    public String clientName { get; set; }

    public String claimEmailAddress { get; set; }

    public String claimMailAddressLine1 { get; set; }

    public String claimMailAddressLine2 { get; set; }

    public String claimMailAddressLine3 { get; set; }

    public String effectiveDate { get; set; }

    public String expirationDate { get; set; }

    public String financialResponsibilityLanguage { get; set; }

    public String phone1 { get; set; }

    public String phone2 { get; set; }

    public String phone3 { get; set; }

    public String planLanguage { get; set; }

    public String program { get; set; }

    public bariatricIDcardController() {
        String cases = ApexPages.CurrentPage().getParameters().get('cases');
        ApexPages.CurrentPage().getParameters().put('cases',  null);

        bs = (bariatricCaseEncryptedData) JSON.deserialize(
            cases,
            bariatricCaseEncryptedData.class
        );

        obj = [
            select
                id,
                bariatric__r.certID__c,
                bariatric__r.Client__r.name,
                bariatric__r.Referred_Facility__c,
                Employee_Primary_Health_Plan_Name__c,
                Estimated_Arrival__c,
                Estimated_Departure__c,
                ID_Card_Requested__c
            from Bariatric_Stage__c
            where id = :bs.id
        ];

        clientName = obj.bariatric__r.Client__r.name;

        claimEmailAddress = 'newcoeclaim@contigohealth.com';
        claimMailAddressLine1 = 'Contigo Health-' + obj.bariatric__r.Client__r.name + ' COE';
        claimMailAddressLine2 = 'P.O. BOX 2582';
        claimMailAddressLine3 = 'Hudson, OH 44236';

        effectiveDate = 'N/A';
        expirationDate = 'N/A';

        // Member financial responsibility disclaimer shown when planLanguage isn't empty
        financialResponsibilityLanguage = 'You will be responsible for any outstanding deductible balance and/or out-of-pocket maximum balance for covered services under this program. Contact the administrator listed on your medical plan ID card to find your deductibles and out-of-pocket maximums.';

        // Phone # for Eligibility, Benefits, Travel, and Expense Money
        phone1 = '+1 (888) 463-3737';

        // Phone # for Care Coordination
        phone2 = '+1 (877) 286-3551';

        // Phone # for 24/7 Travel Support (American Express)
        phone3 = '+1 (646) 817-9847, #3';

        // Except for two plan types, planLanguage does not change for Bariatric
        planLanguage = 'There is a member cost-share (e.g. deductible only, or deductible and coinsurance) under this program. Refer to your medical plan ID card for deductibles and out-of-pocket maximums.';

        program = 'Weight Loss Surgery';

        switch on (obj.bariatric__r.Referred_Facility__c) {
            when 'Scripps Mercy' {
                claimMailAddressLine1 = 'Scripps Health Plan Services';
                claimMailAddressLine2 = 'P.O. Box 2529, Mail Drop 4S-300';
                claimMailAddressLine3 = 'La Jolla, CA 92038';
            }
        }

        if (
            obj.Employee_Primary_Health_Plan_Name__c != null && (
                obj.Employee_Primary_Health_Plan_Name__c.startsWith('eComm')
            )
        ) {
            financialResponsibilityLanguage = '';
            planLanguage = '';
        }

        if (obj.Estimated_Arrival__c != null) {
            effectiveDate = obj.Estimated_Arrival__c.month()
                                + '/'
                                + obj.Estimated_Arrival__c.day()
                                + '/'
                                + obj.Estimated_Arrival__c.year();
        }

        if (obj.Estimated_Departure__c != null) {
            expirationDate = obj.Estimated_Departure__c.month()
                                + '/'
                                + obj.Estimated_Departure__c.day()
                                + '/'
                                + obj.Estimated_Departure__c.year();
        }
    }
}
