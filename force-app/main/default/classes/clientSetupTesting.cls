public with sharing class clientSetupTesting {

    public newCase newCase {get; set;}
    
    Patient_Case__c patientCase;
    public Patient_Case__c  getpatientCase(){return patientCase; }
    
    public string procedureName {get; set;}
    public string procedure {get; set;}
    public string clientName {get; set;}
    public string client {get; set;}
    public string[] referralEmails {get; set;}
    public string[] authEmails {get; set;}
    
    public string facility {get; set;}
    
    public string[] hotels {get; private set;}
    public string[] providers {get; private set;}
    public selectOption[] carriers {get; private set;}
    public selectOption[] employeeHealthPlanNames {get; private set;}
    public selectOption procedures {get; set;}
    public map<string, string[]> carrierMap {get; private set;}
    public set<string> carrierSet {get; private set;}
    
    //public map<string, selectOption[]> facilityMap;
    
    selectOption[] facilities;
    public string healthPacPlanCode {get; private set;}
    
    public boolean isError {get; private set;}
    public boolean activeOnly {get; set;}
    string noReferredFacility = 'No Referred Facility Set';
    public client_facility__c cf {get; private set;}
    
    Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Patient_Case__c.fields.getMap();
    
    public clientSetUpTesting(){init();}
    
    void init(){
        newCase = new newCase();
        patientCase = new patient_Case__c();
        string clientFacilityId = ApexPages.CurrentPage().getParameters().get('cf');
        activeOnly =false;
        if(clientFacilityId!=null && clientFacilityId!=''){
            
            cf = [select Contact_Number__c,Authorization_Email_Contact__c, client__r.name, procedure__r.name,procedure__c, facility__r.name,client__c,Referral_Email_Contact__c, name from Client_facility__c where id=:clientFacilityId];
            
            clientName = cf.client__r.name;
            procedureName= cf.procedure__r.name;
            newCase.clientId = cf.client__c;
            newCase.procedureId= cf.procedure__c;
            facility=cf.id;
        
            runScenario();
        }
    }
    
    public map<string, string> healthPacPlanCodeMap {get; set;}
    public set<string> hpcSet {get; set;}
    public void runScenario(){
        healthPacPlanCodeMap = new map<string, string>();
        isError=false;
        carrierSet = new set<string>();
        carrierMap = new map<string, string[]>();
        hpcSet = new set<string>();
        
        referralEmails = new string[]{};
        authEmails = new string[]{};
        
        if(facility==null||facility==''){
            hotels = new string[]{noReferredFacility};
            providers = new string[]{noReferredFacility};
            hpcSet.add(noReferredFacility);
            healthPacPlanCodeMap.put(noReferredFacility, 'N/A');
            
            authEmails.add(noReferredFacility);
            referralEmails.add(noReferredFacility);
            cf.Contact_Number__c =noReferredFacility;
        }else{
            
            hotels = coeClientHotelOptions.hotelList(facility);
            providers = coeClientProviders.providerStringList(new patient_Case__c(client_facility__c=facility), facility);
            hpcSet = new set<string>();
            
            for(HealthPac_Plan_Code__c hpc : [select Case_Plan_Name__c, HealthPac_Plan_Code__c from HealthPac_Plan_Code__c where Client_Facility__c = :Facility]){
                healthPacPlanCodeMap.put(hpc.Case_Plan_Name__c, hpc.HealthPac_Plan_Code__c);
                
            }
            hpcSet = healthPacPlanCodeMap.keyset();
            
            if(cf.id!=facility){
                cf = [select Contact_Number__c,Authorization_Email_Contact__c, client__r.name, procedure__r.name,procedure__c, facility__r.name,client__c,Referral_Email_Contact__c, name from Client_facility__c where id=:facility];
            }
            
            if(cf.Authorization_Email_Contact__c!=null){
                authEmails = cf.Authorization_Email_Contact__c.removeEnd(',').split(',');
            }
            
            if(cf.Referral_Email_Contact__c!=null){
                referralEmails = cf.Referral_Email_Contact__c.removeEnd(',').split(',');
            }
            
        }
        
        if(newcase.clientId!=null){
            set<id> cset = new set<id>();
            system.debug('activeOnly '+activeOnly); 
            for(selectoption so : coeSelfAdminPickList.selectOptionList(getpatientCase(), newcase.clientId, null, null, coeSelfAdminPickList.carrierName, null, activeOnly)){
                if(so.getLabel()!='--None--'){
                    carrierSet.add(so.getLabel());
                    cset.add(so.getValue());
                    
                    carrierMap.put(so.getLabel(), new string[]{});
                }
                 
            }
            
            for(Client_Employee_Healthplan__c ehp : [select Name,Client_Carrier__c,Client_Carrier__r.name from Client_Employee_Healthplan__c where Client_Carrier__c in :cset ]){
                
                string[] foo = carrierMap.get(ehp.Client_Carrier__r.name);
                if(foo==null){
                    foo = new string[]{};
                }
                foo.add(ehp.name);
                carrierMap.put(ehp.Client_Carrier__r.name, foo);
            }
            
        }else{
            carrierSet.add('No client set');
            carrierMap.put('No client set', new string[]{'N/A'});
        }
        
        //employeeHealthPlanNames = new selectOption[]{new selectOption('','--None--')};
    
    }
    
    public selectOption[] getfacilities(){
        system.debug('activeOnly '+activeOnly); 
        if(newcase==null || newcase.clientid==null || newcase.procedureId==null){
            return new selectoption[]{new selectOption('','-')}; 
        }else{
            return coeSelfAdminPickList.selectOptionList(null, newcase.clientid, newcase.procedureId, null, coeSelfAdminPickList.referredFacility, null, activeOnly);
        }
    }

}