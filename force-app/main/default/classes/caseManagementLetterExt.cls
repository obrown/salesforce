public with sharing class caseManagementLetterExt{


    public Case_Management_Clinician__c clinician {get; private set;}
    public map<id, Case_Management_Clinician__c> clinicianMap {get; set;}
    public string letter {get; set;}

    id cmRecordId;

    public string letterType {get; set;}
    public selectOption[] letterOptions {get; set;}
    public Case_Management_Letter__c cml {get; set;}
    public selectOption[] cmClinicians {get;  set;}
    
    public boolean isError {get; private set;}
    //added 12-01-21 ------------------------------------------start------------------------------
    SystemID__c sid = SystemID__c.getInstance();
    id profID; 
    StaticResource logo;
    String logoPath;
    string letterText;
    
    //added 12-01-21 ------------------------------------------end------------------------------  
    //1-31-21 added o.brown -----------------------------------------start-------------------------------------------------------------
    string fName {get; set;}
    string lName {get; set;}
    public string userName {get; set;}
    public string userTitle {get; set;}
    public string userPhone {get; set;}
    public string userEmail {get; set;} 
    //1-31-21 added o.brown -----------------------------------------end-------------------------------------------------------------      
        
    public caseManagementLetterExt(ApexPages.StandardController controller){
    profID = userInfo.getProfileID(); //12-01-21 added
    
    userName= userInfo.getFirstName()+' '+userInfo.getLastName();//1-31-22 set to use switch statement to set OHy signatures
        
        //12-02-21 update to set the logo based on profile
        if (profid != sid.Ohio_Healthy__c) {
            StaticResource[] srList = [SELECT Id,NamespacePrefix,SystemModstamp,Name FROM StaticResource WHERE Name = 'HDP_Corporate_Logo' order by Name asc];
            logo=srList[0];
        }else{
            StaticResource[] srList = [SELECT Id,NamespacePrefix,SystemModstamp,Name FROM StaticResource WHERE Name = 'OhioHealthy_LogoRevised' order by Name asc];//new update for OHY  OhioHealthy_LogoRevised
            logo=srList[0];
        }
         
        String prefix = logo.NamespacePrefix;
        if( String.isEmpty(prefix) ) {
            prefix = '';
        }else{
            //If has NamespacePrefix
            prefix += '__';
        }
        
        //12-02-21 update to set the logo based on profile
        if (profid != sid.Ohio_Healthy__c) {
            logoPath = '/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'HDP_Corporate_Logo';
            letterText='<div><img src="'+logoPath+'" style="width:290px;margin-left:-5px"/></div><br/>';
            
        }else{
            logoPath = '/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'OhioHealthy_LogoRevised';//12-2-21 new update for OHY  OhioHealthy_LogoRevised
            letterText='<div><img src="'+logoPath+'" style="width:550px;margin-left:-5px"/></div><br/>';//added 12-7-21
            
        }

                  //1-31-22 set signature for OHy users -------------------start------------------------------------
  
                if (profid == sid.Ohio_Healthy__c||Test.isRunningTest()) {
                 
                  
                 switch on userName{

                      when 'Jessica Malone'{
                          userName='Jessica Malone, LPN';
                          userTitle='Case Manager';  
                          userPhone='380-444-5369';  
                          userEmail='Jessica.malone@ohiohealthyplans.com';        
                          
                      }     
                      when 'Melissa Snoots'{
                          userName='Melissa Snoots, LPN';
                          userTitle='Case Manager';  
                          userPhone='380-800-4192';  
                          userEmail='Melissa.snoots@ohiohealthyplans.com';        
                      }     
                      when 'Elizabeth Ireland'{
                          userName='Elizabeth Ireland RN, MSN, CCM';
                          userTitle='Case Management Manager';  
                          userPhone='380-444-5007';  
                          userEmail ='Elizabeth.ireland@ohiohealthyplans.com';         
                          
                      }     
                      when 'Sherri Pea'{
                          userName='Sherri Pea, LISW';
                          userTitle='Behavior Health Specialist';  
                          userPhone='380-444-5365';  
                          userEmail='Sherri.pea@ohiohealthyplans.com';        
                          
                      }         
                      when 'Jabe Stafford'{
                          userName='Jabe Stafford';
                          userTitle='Supervisor, Operations';  
                          userPhone='380-220-1128';  
                          userEmail='Jabe.stafford@ohiohealthyplans.com';         
                          
                      }     
                      when 'Darren Wade'{
                          userName='Darren Wade';
                          userTitle='Case Manager Extender';  
                          userPhone='380-444-5367';  
                          userEmail='Darren.wade@ohiohealthyplans.com';       
                          
                      }     
                      when 'Toni Goble'{
                          userName='Toni Goble';
                          userTitle='Case Manager Extender';  
                          userPhone='380-800-4186';  
                          userEmail='Toni.goble@ohiohealthyplans.com';        
                          
                      }      
                      when 'TroyLynn Smith'{
                          userName='TroyLynn Smith';
                          userTitle='Case Manager Extender';  
                          userPhone='380-444-5371';  
                          userEmail='Troylynn.smith@ohiohealthyplans.com';        
                          
                      }      
                      when 'Tina Blackmon'{
                          userName='Tina Blackmon';
                          userTitle='Case Manager Extender';  
                          userPhone='380-210-1005';  
                          userEmail='Tina.blackmon@ohiohealthplans.com';      
                          
                      }
                      when 'Test User OHY'{
                          userName='Ohio Healthy Test User';
                          userTitle='Test User';  
                          userPhone='602-222-2400';  
                          userEmail='oliver.brown@contigohealth.com';      
                          
                      }                           
                      when else{
                          userName='Care Management Department';
                          userTitle='Ohio Healthy';  
                          userPhone='P.O. Box 2584';  
                          userEmail='Hudson, OH 44236';                       
                      } 
                   } 
                   }
                 
                  //1-31-22 set signature for OHy users -------------------end------------------------------------
        
        string letterID = ApexPages.CurrentPage().getParameters().get('letter');
        string clinicianID = ApexPages.CurrentPage().getParameters().get('clinician');
        string letterType = ApexPages.CurrentPage().getParameters().get('lt');
        
        string[] fieldList = new string[]{};
        fieldList.add('patient__r.Patient_Date_of_Birth__c');
        
        if(!Test.isRunningTest()){ controller.addFields(fieldList); }
       
        
        //new cm code
        Case_Management__c cm = (Case_Management__c)controller.getRecord();
        cmRecordId = cm.id;
        cml = new Case_Management_Letter__c(Case_Management__c=cmRecordId);
        
        
        
        loadCmClinicians();
        letterOptions = new selectOption[]{};
        letterOptions.add(new selectOption('','-- Choose a letter --'));
        if (profid != sid.Ohio_Healthy__c) {
            letterOptions.add(new selectOption('Penalty Letter','Penalty Letter'));
            letterOptions.add(new selectOption('Unable to Reach after Contact','Unable to Reach after Contact'));
            letterOptions.add(new selectOption('UTR Minor Template','UTR Minor Template'));
            letterOptions.add(new selectOption('UTR Template','UTR Template'));

        }else{
        //12-01-21 new ohy letter options
            letterOptions.add(new selectOption('OHY Birth Notification','OHY Birth Notification'));
            letterOptions.add(new selectOption('OHY General Talked Once','OHY General Talked Once'));
            letterOptions.add(new selectOption('OHY Unable to Contact','OHY Unable to Contact'));
            letterOptions.add(new selectOption('OHY Welcome','OHY Welcome'));        
        }
        
        if(clinicianID!=null){
            setClinician(clinicianID);
            
        }else{

            clinician =new Case_Management_Clinician__c();
        }
        
        if(letterID !=null){
            try{
                letterID = letterID.escapehtml3();
                id.valueof(letterID);
                cml = [select LetterText__c,Salutation__c,Case_Management_Clinician__r.First_Name__c,Case_Management_Clinician__r.Last_Name__c,
                                Case_Management_Clinician__r.Street__c, 
                                Case_Management_Clinician__r.City__c,
                                Case_Management_Clinician__r.State__c,
                                Case_Management_Clinician__r.Zip_Code__c,
                                Case_Management_Clinician__r.Salutation__c,
                                Case_Management_Clinician__r.Credentials__c,
                                Case_Management__r.Patient__r.Patient_First_Name__c,
                                Case_Management__r.Patient__r.Patient_Last_Name__c,
                                Case_Management__r.Patient__r.Subscriber_First_Name__c,
                                Case_Management__r.Patient__r.Subscriber_Last_Name__c,
                                Case_Management__r.Patient__r.Patient_Date_of_Birth__c,
                                Case_Management__r.Patient__r.Gender__c,
                                Case_Management__r.patient__r.Address__c,
                                Case_Management__r.patient__r.City__c,
                                Case_Management__r.patient__r.State__c,
                                Case_Management__r.patient__r.Zip__c,
                                Case_Management__r.Patient__r.Patient_Employer__r.name,Provider_Name__c,Provider_Street__c,Provider_City__c,Provider_State__c,Provider_Zip_Code__c,Network_Variance_Provider_Clinician_Name__c,Network_Variance_Provider_Clinician__c,Description__c,case_Management_Clinician__c,Admission_of__c,Clinical_Rationale__c,HealthCare_Benefits_Plan_Document__c, Medical_Benefit_Plan_Covers__c, Section_Title__c ,Specific_Plan_Exclusions__c, 
                                Exclusion_language_quote__c,Type_of_Letter__c,Type_of_Request__c,Traditional_and_Consumer_Select_Medical__c,signed_by__c, signed_date__c from Case_Management_Letter__c where id =:letterID];
                  

              }catch(exception e){
                  system.debug(e.getMessage()+' '+e.getlinenumber());
              }
              
              }else{
                  cml = new Case_Management_Letter__c(Case_Management__c=cmRecordId);
              }
              system.debug('letterType '+letterType);
              if(cml.lettertext__c==null){
                  if(cml.Type_of_Letter__c==null){
                      cml.Type_of_Letter__c=letterType;
                  }
                  
                  if(cml.Case_Management_Clinician__c==null){
                      cml.Case_Management_Clinician__c=clinicianID;
                  
                  }else{
                      setClinician(cml.Case_Management_Clinician__c);
                  }
                  
                  //string letterText='<div><img src="'+logoPath+'" style="width:290px;margin-left:-5px"/></div><br/>';12-07-21 moved to display based new OHY logo for OHY only
                  letterText+='<div style="font-size: 15px;">';
                  letterText+='<div style="text-align:left;"><br/>';
                  letterText+=date.today().format();
                  letterText+='</div><br/>';
                  
                  switch on letterType{

                      when 'Penalty Letter'{
                          cml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                          letterText+=cmPenalty.letterText(cm, cml, clinician);  
                          cml.letterText__c=letterText;                      
                          
                      }
                      when 'Unable to Reach after Contact'{
                          cml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                          letterText+=cmUnabletoReach.letterText(cm, cml, clinician);  
                          cml.letterText__c=letterText;                      
                          
                      }
                      when 'UTR Minor Template'{
                          cml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                          letterText+=cmUTRMinorTemplate.letterText(cm, cml, clinician);  
                          cml.letterText__c=letterText;                      
                          
                      }
                      when 'UTR Template'{
                          cml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                          letterText+=cmUTRTemplate.letterText(cm, cml, clinician);  
                          cml.letterText__c=letterText;                      
                          
                      }

                      //12-01-21 OHY letters added  
             
                       when 'OHY Birth Notification'{
                          cml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                          letterText+=birthNotification.letterText(cm, cml, clinician,userName,userTitle,userPhone,userEmail);  
                          cml.letterText__c=letterText;                      
                          
                      } 
                       when 'OHY General Talked Once'{
                          cml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                          letterText+=initialFollowUp.letterText(cm, cml, clinician,userName,userTitle,userPhone,userEmail);
                          cml.letterText__c=letterText;                      
                          
                      }  
                       when 'OHY Unable to Contact'{
                          cml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                          letterText+=unableToContact.letterText(cm, cml, clinician,userName,userTitle,userPhone,userEmail);
                          cml.letterText__c=letterText;                      
                          
                      } 
                       when 'OHY Welcome'{
                          cml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                          letterText+=welcome.letterText(cm, cml, clinician,userName,userTitle,userPhone,userEmail);
                          cml.letterText__c=letterText;                      
                          
                      }                                                                        
                      
                  }
                     
             }                           
        
    }

    
    public void saveLetter(){
        isError=false;
        
        try{
            upsert cml;
        }catch(dmlexception e){
            isError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDMLMessage(0)));
        }
    }
    
    
    public void setLetterText(){
        
    }
    
    public void setLetterType(){
        
    }
    public void setClinician(){
        clinician = clinicianMap.get(cml.Case_Management_Clinician__c);
        
    }
    public void setClinician(string clincianID){
        clinician = clinicianMap.get(clincianID);
        
        
    }
    
    void loadCmClinicians(){
        
        cmClinicians = new selectOption[]{};
        if(cmRecordId !=null){
            clinicianMap = new map<id, Case_Management_Clinician__c>();
            cmClinicians.add(new selectOption('','-- Choose a clinician --'));
            for(Case_Management_Clinician__c  cm : [select Salutation__c,
                              City__c,
                              Contact_Name__c, 
                              Contact_Phone_Number__c, 
                              Credentials__c,
                              Clinician_Fax__c,
                              LastModifiedDate, 
                              LastModifiedBy.Name, 
                              CreatedBy.Name,
                              CreatedDate,
                              Name,
                              Network_Status__c,
                              First_Name__c,
                              Last_Name__c,
                              State__c,
                              Street__c,
                              Zip_Code__c from Case_Management_Clinician__c where Case_Management__c = :cmRecordId ]){
                              
                cmClinicians.add(new selectOption(cm.id,cm.First_Name__c+' '+cm.Last_Name__c));
                clinicianMap.put(cm.id, cm);
            }
        
        }                      
    }
    
}