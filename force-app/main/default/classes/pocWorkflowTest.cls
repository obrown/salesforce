/**
 * This class contains unit tests for validating the behavior of  the Apex pocWorkflowclass
 */

@isTest(seealldata = true)
private class pocWorkflowTest {
    static testMethod void patientCasePOCSpine(){
        //code coverage at 54% for this class...76% code coverage

        client_facility__c cf = [select client__c, procedure__c from client_facility__c where Client__r.name = 'Walmart' and Procedure__r.name = 'Spine' limit 1];

        patient_case__c pc = new patient_case__c();

        pc.client__c = cf.client__c;
        pc.ecen_procedure__c = cf.procedure__c;
        pc.client_name__c = 'Walmart';
        pc.Employee_First_Name__c = 'John';
        pc.Employee_Last_Name__c = 'Smith';
        pc.Patient_First_Name__c = 'John';
        pc.Patient_Last_Name__c = 'Smith';
        pc.client_facility__c = cf.id;
        pc.recordtypeid = [select id from RecordType where sObjectType = 'Patient_Case__c' and isActive = true and name = 'Walmart Spine' limit 1].id;
        insert pc;

        ApexPages.currentPage().getParameters().put('id', pc.id);

        caseController cc = new caseController();

        cc.obj.status__c = 'Open';
        cc.obj.status_reason__c = 'In Process';

        cc.obj.bid__c = '12456';
        cc.obj.initial_call_discussion__c = 'test';
        cc.obj.caller_name__c = 'Unit.test';
        cc.obj.Caller_relationship_to_the_patient__c = 'Self';
        cc.obj.Employee_dob__c = date.valueof('1981-01-01');
        cc.obj.Employee_Gender__c = 'Male';
        cc.obj.Employee_ssn__c = '987654321';

        cc.obj.Patient_dob__c = date.valueof('1981-01-01');
        cc.obj.Patient_Gender__c = 'Male';
        cc.obj.Patient_ssn__c = '987654321';

        cc.obj.Relationship_to_the_Insured__c = 'Patient is Insured';
        cc.obj.Same_as_Employee_Address__c = true;

        cc.obj.Employee_street__c = '123 E Main St';
        cc.obj.Employee_City__c = 'Mesa';
        cc.obj.Employee_State__c = 'AZ';
        cc.obj.Employee_Zip_Postal_Code__c = '85297';

        cc.obj.callback_number__c = '(480) 555-1212';
        cc.obj.patient_home_phone__c = '(480) 555-1212';
        cc.obj.Language_Spoken__c = 'English';
        cc.obj.referred_facility__c = 'Mayo Jacksonville';

        Clinical_Codes__c ccode = new Clinical_Codes__c(patient_case__c = cc.obj.id);
        insert ccode;

        cc.obj.Program_Authorization_Received__c = date.today().addDays(-1);
        cc.obj.Caregiver_Responsibility_Received__c = date.today().addDays(-1);
        attachment a = new attachment(name = 'test', parentid = pc.id, body = blob.valueof('1234'));
        insert a;
        Program_Notes__c pn = new Program_Notes__c(patient_case__c = pc.id, isAuthEmailAttach__c = true, attachID__c = a.id);
        insert pn;

        cc.inlineSave();
        system.debug('Message: ' + cc.message + ' ************************************************************************************* ');
        system.assert (cc.message == null || cc.message == '' || cc.message == 'The following fields are still required to save<BR><BR>null');

        cc.convertLead();
        system.debug('Message: ' + cc.message + ' ************************************************************************************* ');
          //system.assert(cc.message==null||cc.message=='The following fields are still required to save<BR><BR>null');
        cc.obj.isConverted__c = true;

        cc.inlineSave();
        system.debug('cc.message: ' + cc.message);
        //system.assert(cc.message==null||cc.message=='The following fields are still required to save<BR><BR>null');

        pc_POC__c poc = new pc_POC__c(patient_case__c = pc.id, recordtypeid = [select id from recordtype where sObjectType = 'pc_POC__c' and name = 'Initial'].id);
        insert poc;

        poc.Plan_of_Care_accepted__c = date.today();
        poc.Surgeon_Name__c = 'Luke, Kano';
        poc.Proposed_Service_Trip_Reason__c = 'New';
        poc.Proposed_Service_Trip__c = 'Evaluation';
        poc.Diagnosis_of_Scoliosis__c = 'No';
        poc.Current_Nicotine_User__c = 'No';
        poc.Proposed_Service_Type__c = 'Spine Evaluation';
        poc.Proposed_Service_Trip_Note__c = 'test';
        poc.Proposed_Add_on_Services__c = 'Additional Diagnostics of Ultrasound (Peripheral Muscle or Joint)';
        poc.Conservative_Treatment__c = 'Physical Therapy';
        poc.Requested_Billable_Additional_Services__c = 'test';
        poc.Clinically_Allowable_Mode_of_Transportat__c = 'Fly';
        poc.Patient_Preferred_Mode_of_Transportation__c = 'Fly';
        poc.Travel_Notes__c = 'test';
        poc.Late_Checkout_Needed__c = 'Yes';
        poc.Wheelchair_needed_at_airport__c = 'No';
        poc.Extra_space_for_large_tall_patient__c = 'Yes';
        poc.Maximum_flight_legnth__c = 4.0;
        poc.Patient_Arrival_Date__c = date.today();
        poc.First_Appointment_Date__c = date.today();
        poc.Last_Appointment_Date__c = date.today();
        poc.Return_Home_Travel_Date__c = date.today();
        poc.Date_Clinical_Received__c = date.today();
        poc.Date_Clinical_Saved_to_Folder__c = date.today();
        poc.Date_Transitioned_to_the_Carrier__c = date.today();
        poc.Conservative_Treatment__c = 'Bone SPECT Scans;Injection Paravertebral C/T;Diagnostic Radiology w/ Evaluation X-Ray;CT Scans;EMG Muscle Test One Limb;Physical Therapy;Walker Folding Wheeled Adjustable';

        update poc;

        system.debug('poc =' + poc);
        system.debug('pc =' + pc);
        system.debug('case type =' + cc.obj.case_type__c);

        cc.obj.Received_Determination_at_HDP__c = date.today().addDays(-7);
        cc.obj.eligible__c = 'Yes';

        pocWorkflow.planofcareRulesSpine(poc);
        Clinical_Codes__c[] fooCC = pocWorkflow.checkClinicalCodeSpine(poc);
        Clinical_Codes__c[] fooCC2 = pocWorkflow.checkClinicalCodeSpineConservativeTreatment(poc);

        //10-6-21 added------------------------------start-------------------------------------------------
        poc.Proposed_Service_Trip__c='Program Surgery - Inpatient';
        update poc;
        Clinical_Codes__c[] fooCCj = pocWorkflow.checkClinicalCodeJointSurgery(poc,true);

        poc.Proposed_Service_Trip__c='Program Surgery - Outpatient';
        poc.Proposed_Service_Type__c='Right Knee';
        update poc;
        Clinical_Codes__c[] fooCCj2 = pocWorkflow.checkClinicalCodeJointSurgery(poc,true);

        poc.Proposed_Service_Trip__c='Program Surgery - Outpatient';
        poc.Proposed_Service_Type__c='Right Hip';
        update poc;
        Clinical_Codes__c[] fooCCj3 = pocWorkflow.checkClinicalCodeJointSurgery(poc,true);

        //10-6-21 added------------------------------end-------------------------------------------------
        pocWorkflow pocWF = new pocWorkflow();

        pocWF.followUpOnPOC(date.today());
        //pocWF.checkVisitType(poc); not visable need to make public

        //static methods
        //10-6-21 added--------------------------start---------------------------
        poc.Nicotine_Quit_Date__c=date.today();
        poc.Patient_Arrival_Date__c=date.today();
        pocWorkflow.planofcareSetUpRules(poc,'No');

        //poc.Nicotine_Quit_Date__c=date.today();
        poc.Patient_Arrival_Date__c=date.today().addDays(30);
        pocWorkflow.planofcareSetUpRules(poc,'No');

        poc.First_Appointment_Date__c=date.today().addDays(33);
        poc.Patient_Arrival_Date__c=date.today().addDays(30);
        pocWorkflow.planofcareSetUpRules(poc,'No');

        poc.First_Appointment_Date__c=date.today().addDays(33);
        poc.Patient_Arrival_Date__c=date.today().addDays(30);
        pocWorkflow.planofcareSetUpRules(poc,'Yes');

        poc.Hospital_Admission_Date__c=date.today().addDays(33);
        poc.Hospital_Discharge_Date__c=date.today();
        poc.Surgical_Procedure_Date__c=date.today();
        poc.Physician_Follow_up_Appointment_Date__c=date.today().addDays(-33);
        poc.Last_Appointment_Date__c=date.today().addDays(-40);
        poc.First_Appointment_Date__c=date.today().addDays(40);
        pocWorkflow.planofcareSetUpRules(poc,'No');

        poc.Surgical_Procedure_Date__c=date.today().addDays(2);
        poc.Last_Appointment_Date__c=date.today().addDays(30);
        pocWorkflow.planofcareSetUpRules(poc,'No');

        poc.Proposed_Service_Trip_Reason__c=null;
        poc.Proposed_Service_Trip__c=null;
        poc.Proposed_Service_Type__c=null;
        poc.Diagnosis_of_scoliosis__c=null;
        poc.Current_Nicotine_User__c=null;
        poc.Clinically_Allowable_Mode_of_Transportat__c=null;
        poc.Patient_Arrival_Date__c=null;
        poc.First_Appointment_Date__c=null;
        poc.Hospital_Discharge_Date__c=null;
        poc.Last_Appointment_Date__c=null;
        poc.Return_Home_Travel_Date__c=null;
        pocWorkflow.planofcareRulesSpine(poc);

        poc.Proposed_Service_Trip__c='Evaluation with Planned Inpatient Surgery';
        poc.Hospital_Admission_Date__c=null;
        poc.Surgical_Procedure_Date__c=null;
        poc.Physician_Follow_up_Appointment_Date__c=null;
        poc.current_nicotine_user__c='Yes';
        poc.Nicotine_Quit_Date__c=null;
        pocWorkflow.planofcareRulesSpine(poc);

        poc.Plan_of_Care_Accepted__c=null;
        poc.Proposed_Service_Trip__c=null;
        poc.Proposed_Service_Type__c=null;
        //10-6-21 added--------------------------end---------------------------


        pocWorkflow.planofcareRulesDigitalPhysicalTherapy(poc, pc);
        pocWorkflow.AccumEmail('slim.brown@premierinc.com,jim.brown@premierinc.com');
        //8-17-21 o.brown update -------------------start----------------------------------------------

        cc.obj.current_nicotine_user__c = 'Yes';
        cc.obj.continineCompleted__c = true;
        cc.inlineSave();
        cc.sendAccumEmail();

        poc.Plan_of_Care_accepted__c = date.today().addDays(-1);
        update poc;

          //catch exceptions
        poc.Proposed_Service_Trip__c = 'Evaluation with Planned Inpatient Surgery';
        poc.Diagnosis_of_scoliosis__c = 'Yes';
        Clinical_Codes__c[] fooCC3 = pocWorkflow.checkClinicalCodeSpine(poc);
    }
    /*only works if the record type check on line 343 in pocWorkflow if commented out..Did not want to update the case controller and wherever else to pass in the record type.
     * was 51% percent code coverage with this in and 76% overall
     * static testMethod void patientCasePOCJoint(){
     *
     *  client_facility__c cf = [select client__c, procedure__c from client_facility__c where Client__r.name = 'Walmart' and Procedure__r.name = 'Joint' limit 1];
     *
     *  patient_case__c pc = new patient_case__c();
     *  pc.client__c = cf.client__c;
     *  pc.ecen_procedure__c = cf.procedure__c ;
     *  pc.client_name__c= 'Walmart';
     *  pc.Employee_First_Name__c  ='John';
     *  pc.Employee_Last_Name__c  ='Smith';
     *  pc.Patient_First_Name__c  ='John';
     *  pc.Patient_Last_Name__c  ='Smith';
     *  pc.client_facility__c = cf.id;
     *  pc.recordtypeid = [select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name = 'Walmart Joint' limit 1].id;
     *  insert pc;
     *
     *  ApexPages.currentPage().getParameters().put('id',pc.id);
     *
     *  caseController cc = new caseController();
     *
     *  cc.obj.status__c= 'Open';
     *  cc.obj.status_reason__c= 'In Process';
     *
     *  cc.obj.bid__c = '12456';
     *  cc.obj.initial_call_discussion__c= 'test';
     *  cc.obj.caller_name__c= 'Unit.test';
     *  cc.obj.Caller_relationship_to_the_patient__c='Self';
     *  cc.obj.Employee_dob__c = date.valueof('1981-01-01');
     *  cc.obj.Employee_Gender__c = 'Male';
     *  cc.obj.Employee_ssn__c = '987654321';
     *
     *  cc.obj.Patient_dob__c = date.valueof('1981-01-01');
     *  cc.obj.Patient_Gender__c = 'Male';
     *  cc.obj.Patient_ssn__c = '987654321';
     *
     *
     *  cc.obj.Relationship_to_the_Insured__c = 'Patient is Insured';
     *  cc.obj.Same_as_Employee_Address__c = true;
     *
     *  cc.obj.Employee_street__c = '123 E Main St';
     *  cc.obj.Employee_City__c = 'Mesa';
     *  cc.obj.Employee_State__c = 'AZ';
     *  cc.obj.Employee_Zip_Postal_Code__c = '85297';
     *
     *  cc.obj.callback_number__c = '(480) 555-1212';
     *  cc.obj.patient_home_phone__c = '(480) 555-1212';
     *  cc.obj.Language_Spoken__c = 'English';
     *
     *  cc.obj.referred_facility__c ='Johns Hopkins';
     *
     *  Clinical_Codes__c ccode = new Clinical_Codes__c(patient_case__c = cc.obj.id);
     *  insert ccode;
     *
     *  cc.obj.Program_Authorization_Received__c = date.today().addDays(-1);
     *  cc.obj.Caregiver_Responsibility_Received__c = date.today().addDays(-1);
     *  attachment a = new attachment(name='test',parentid=pc.id,body=blob.valueof('1234'));
     *  insert a;
     *  Program_Notes__c pn = new Program_Notes__c(patient_case__c=pc.id,isAuthEmailAttach__c=true,attachID__c=a.id);
     *  insert pn;
     *
     *  cc.inlineSave();
     *  system.debug('Message: ' + cc.message + ' ************************************************************************************* ');
     *  system.assert(cc.message==null||cc.message==''||cc.message=='The following fields are still required to save<BR><BR>null');
     *
     *  cc.convertLead();
     *  system.debug('Message: ' + cc.message + ' ************************************************************************************* ');
     *  //system.assert(cc.message==null||cc.message=='The following fields are still required to save<BR><BR>null');
     *  cc.obj.isConverted__c = true;
     *
     *  cc.inlineSave();
     *  system.debug('cc.message: '+cc.message);
     *  //system.assert(cc.message==null||cc.message=='The following fields are still required to save<BR><BR>null');
     *
     *  pc_POC__c poc = new pc_POC__c(patient_case__c=pc.id, recordtypeid=[select id from recordtype where sObjectType = 'pc_POC__c' and name= 'Initial'].id);
     *  insert poc;
     *
     *  poc.Plan_of_Care_accepted__c = date.today();
     *  //8-17-21 o.brown update added to test pocworkflow-------------------start----------------------------------------------
     *  poc.Surgeon_Name__c='Luke, Kano';
     *  poc.Proposed_Service_Trip__c='Evaluation with Planned Inpatient Program Surgery';
     *  poc.Proposed_Service_Type__c='Right Knee';
     *  poc.Current_Nicotine_User__c='No';
     *  poc.Hospital_Admission_Date__c =date.today();
     *  poc.Surgical_Procedure_Date__c =date.today();
     *  poc.Hospital_Discharge_Date__c =date.today();
     *  poc.Patient_Preferred_Mode_of_Transportation__c='Fly';
     *  poc.Patient_Arrival_Date__c=date.today();
     *  poc.First_Appointment_Date__c=date.today();
     *  poc.Physician_Follow_up_Appointment_Date__c=date.today();
     *  poc.Last_Appointment_Date__c=date.today();
     *  poc.Return_Home_Travel_Date__c=date.today();
     *  poc.Complication_Service_Trip__c='Inpatient';
     *  //poc.recordtype.name=poc.name ;
     *
     *
     *  pocWorkflow.planofcareRulesJoint(poc);
     *  update poc;
     * // cc.appovePlanOfCare();
     *  system.debug('poc ='+poc);
     *  system.debug('pc ='+pc);
     *  //system.debug('case type ='+cc.obj.case_type__c);
     *
     *  cc.obj.Received_Determination_at_HDP__c=date.today().addDays(-7);
     *  cc.obj.eligible__c= 'Yes';
     *
     *  //pocWorkflow.planofcareRulesJoint(poc);
     *  //8-17-21 o.brown update -------------------start----------------------------------------------
     *
     *  cc.obj.current_nicotine_user__c = 'Yes';
     *  cc.obj.continineCompleted__c= true;
     *  cc.inlineSave();
     *  cc.sendAccumEmail();
     *
     *  poc.Plan_of_Care_accepted__c = date.today().addDays(-1);
     *  update poc;
     *
     *
     *
     * }
     */
}
