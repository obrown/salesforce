public with sharing class facilityFuture{
    
    @future(callout=true)
    public static void fetchLatLong(set<id> theIds){
        
        Facility__c[] fList = new Facility__c[]{};
        for(Facility__c f : [select latitude__c,
                                    longitude__c,
                                    facility_address__c, 
                                    facility_city__c,
                                    facility_state__c,
                                    facility_zip__c from Facility__c where id in :theIds]){
            
            string returnValue = geocodioGeoCoding.lookupAddress(f.facility_address__c, f.facility_city__c, f.facility_state__c, f.facility_zip__c);
            try {
                    geocodio gc = (geocodio) System.JSON.deserialize(returnValue, geocodio.class);
                    f.latitude__c=string.valueof(gc.results[0].location.lat).left(15);
                    f.longitude__c=string.valueof(gc.Results[0].location.lng).left(15);
                } catch(exception e) {
                    system.debug(e.getMessage());
                }
            
            fList.add(f);
        }
        
        update flist;
    } 
    
}