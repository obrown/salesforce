public with sharing class oncologyCaseNote{


    public Oncology_Case_Note__c[] caseNotes {get; set;}
    public Oncology_Case_Note__c   caseNote  {get; set;}
    id oncologyId;
    public boolean isError {get; set;}
    
    public class response{
        public response(){
            error=false;
        }
        
        public boolean error;
        public string message;
    }
    
    public oncologyCaseNote(string oncologyId){
        this.oncologyId = oncologyId;
        loadNotes();
    }
    
    public void getNoteVF(){
        string noteId = ApexPages.CurrentPage().getParameters().get('casenoteid');
        ApexPages.CurrentPage().getParameters().put('casenoteid', null);
        getNote(noteId);
        
    }
    
    void getNote(string noteId){
    
        for(Oncology_Case_Note__c ocn : caseNotes){
            if(ocn.id==noteId){
                caseNote=ocn;
                break;
            }
        }
    }
    
    void loadNotes(){
        
        caseNotes = new Oncology_Case_Note__c[]{};
        
        if(this.oncologyId == null){
            return;
        }
        
        caseNotes = [select Body__c, 
                            Clinical_Note__c, Communication_Type__c,Contact_Type__c,
                            Initiated__c, 
                            Locked__c,
                            Subject__c,
                            createdby.name,
                            createddate,
                            lastModifiedDate, 
                            lastModifiedBy.Name from Oncology_Case_Note__c where oncology__c = :oncologyId order by createdDate desc];
        
    }
    
    public response saveNote(){
        oncologyCaseNote.response response = new oncologyCaseNote.response();
        
        try{
            upsert caseNote;
            loadNotes();
            response.message = 'Note Saved';
            
        }catch(dmlException e){
            response.error=true;
            response.message = e.getDMLMessage(0);
        }
        
        return response;
    }
    
    public void newCaseNote(){
        caseNote= new Oncology_Case_Note__c(oncology__c=oncologyId);
    }

}