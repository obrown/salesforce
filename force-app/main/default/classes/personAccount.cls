public without sharing class personAccount{

    public string FirstName, LastName, Patient_Email_Address_xx, Gender_xxpc, PersonEmail;
    public string Patient_Street_xx,Patient_City_xx,Patient_State_xx,Patient_Zip_xx;
    public string PersonHomePhone , PersonMobilePhone , PersonOtherPhone; 
    public string PersonMailingStreet,PersonMailingCity,PersonMailingState,PersonMailingPostalCode,PersonMailingCountry;
    //phyisician street information
    public string MailingStreet,MailingCity,MailingState,MailingPostalCode,MailingCountry,phone,Fax;
    public date PersonBirthdate;
    public string Work_Phone_Number_xx;
    public string Employer_Program_xx;
    public id recordtypeid;
    public string HDP_Parent_ID_xx;
    public date Date_of_Intake_Initiated_xx;
    public datetime Referral_Date_and_Time_xx;
    public string Eligibility_Verification_xx ;
    public date Date_Eligibility_Verified_xx;
    public string Patient_Preferred_Name_xx;
    public string Referral_Facility_xx;
    public string Patient_Gender_xx;
    public string Patient_COE_Benefit_Identification_Numbe_xx;
    public string Patient_Preferred_Phone_xx, Preferred_Phone_xx;
    
    public string Best_Time_to_Contact_xx;
    public string Translation_Services_Required_xx;
    public string Preferred_Language_xx;
    public string Relationship_to_Employee_xx;
    public string PHI_Permission_Granted_To_xx;
    public string Employee_ID_xx;
    public string Employee_First_Name_xx;
    public string Employee_Last_Name_xx;
    public date Employee_Date_of_Birth_xx;
    public string Employee_Gender_xx;
    public string Employee_Email_Address_xx;
    public boolean Same_as_Patient_Address_xx;
    //public string Employee_Address_Street_xx;
    //public string Employee_Address_City_xx;
    //public string Employee_Address_State_xx;
    //public string Employee_Address_Zip_xx;
    //public string Employee_Country_xx ='US';
    public string Employee_Home_Phone_xx;
    public string Employee_Mobile_Phone_xx;
    public string Employee_Work_Phone_xx;
    public string Employee_Other_Phone_xx;
    public string Employee_Preferred_Phone_xx;
    public string Best_Time_to_Contact_Employee_xx;
    public string Treatment_Patient_Reported_to_HDP_xx;
    public string Other_Pertinent_Medical_Information_xx;
    public string Primary_Diagnosis_PatientReported_to_HDP_xx;
    public string Patient_Country_xx;
    public date Date_of_Diagnosis_PatientReportedto_HDP_xx;
    
    public string Local_Physician_First_Name_Last_Name_xx;
    public string Local_Physician_Credentials_xx;
    public string Local_Physician_Specialty_xx;
    public string Local_Physician_Street_Address_xx;
    public string Local_Physician_City_xx;
    public string Local_Physician_State_xx;
    public string Local_Physician_Zip_xx;
    public string Local_Physician_Country_xx;
    public string Local_Physician_Phone_Number_xx;
    public string Local_Physician_Fax_Number_xx;
    
    public string Associated_Facilities_xx;
    
    public string COH_Nurse_Name_xx;
    public string COH_Nurse_Phone_Number_xx;
    public string COH_Nurse_Email_xx;
    public string HDP_Nurse_Name_xx;
    public string HDP_Nurse_Phone_Number_xx;
    public string HDP_Nurse_Email_xx;

}