public with sharing class Providers {

	public String[] fields;
	public providers(string[] fields){
		this.fields = fields;
	}
	
	public string createEditProvider(Provider__c thePro){
		
		String error;
		 sObject Pro = thePro;
			
		for(String s: fields){
			s = s.trim();
				system.debug(s.right(s.length() - (s.indexof(':')+1)));
	
			try{//Try catch block for date fields
				Pro.put(s.left(s.indexOf(':')),s.right(s.length() - (s.indexof(':')+1)));
			}catch(exception e){
				try{//date field catch
					Pro.put(s.left(s.indexOf(':')),date.valueof(s.right(s.length() - (s.indexof(':')+1))));
				}catch(exception exp){
					error = exp.getMessage();
				break;
				}
			}
	
		}
	
		if(error==null){
			upsert Pro;
			error = Pro.id;
		}
	
	return error;	
	
	}

}