/**
* This class contains unit tests for validating the behavior of  the Apex addNoteController class
*/

@isTest(seealldata=true)
private class patientCasePlanOfCareTest {

    static testMethod void patientCasePOC(){
        
        client_facility__c cf = [select client__c, procedure__c from client_facility__c where Client__r.name = 'Walmart' and Procedure__r.name = 'Joint' limit 1];
        
        patient_case__c pc = new patient_case__c();
        pc.client__c = cf.client__c;
        pc.ecen_procedure__c = cf.procedure__c ;
        pc.client_name__c= 'Walmart';
        pc.Employee_First_Name__c  ='John';
        pc.Employee_Last_Name__c  ='Smith';
        pc.Patient_First_Name__c  ='John';
        pc.Patient_Last_Name__c  ='Smith';
        pc.client_facility__c = cf.id;
        pc.recordtypeid = [select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name = 'Walmart Joint' limit 1].id;
        insert pc;
        
        //ApexPages.currentPage().getParameters().put('RecordType',[select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name = 'Walmart Joint' limit 1].id); 
        ApexPages.currentPage().getParameters().put('id',pc.id); 
        
        caseController cc = new caseController();
        
        cc.obj.status__c= 'Open';
        cc.obj.status_reason__c= 'In Process';
        
        cc.obj.bid__c = '12456';
        cc.obj.initial_call_discussion__c= 'test';
        cc.obj.caller_name__c= 'Unit.test';
        cc.obj.Caller_relationship_to_the_patient__c='Self';
        cc.obj.Employee_dob__c = date.valueof('1981-01-01');
        cc.obj.Employee_Gender__c = 'Male';
        cc.obj.Employee_ssn__c = '987654321';
        
        cc.obj.Patient_dob__c = date.valueof('1981-01-01');
        cc.obj.Patient_Gender__c = 'Male';
        cc.obj.Patient_ssn__c = '987654321';
        
        
        cc.obj.Relationship_to_the_Insured__c = 'Patient is Insured';
        cc.obj.Same_as_Employee_Address__c = true;
        
        cc.obj.Employee_street__c = '123 E Main St';
        cc.obj.Employee_City__c = 'Mesa';
        cc.obj.Employee_State__c = 'AZ';
        cc.obj.Employee_Zip_Postal_Code__c = '85297';
        
        cc.obj.callback_number__c = '(480) 555-1212';
        cc.obj.patient_home_phone__c = '(480) 555-1212';
        cc.obj.Language_Spoken__c = 'English';
        
        cc.obj.referred_facility__c ='Johns Hopkins';
        
        Clinical_Codes__c ccode = new Clinical_Codes__c(patient_case__c = cc.obj.id);
        insert ccode;
        
        cc.obj.Program_Authorization_Received__c = date.today().addDays(-1);
        cc.obj.Caregiver_Responsibility_Received__c = date.today().addDays(-1);
        attachment a = new attachment(name='test',parentid=pc.id,body=blob.valueof('1234'));
        insert a;
        Program_Notes__c pn = new Program_Notes__c(patient_case__c=pc.id,isAuthEmailAttach__c=true,attachID__c=a.id);
        insert pn;
        
        cc.inlineSave();
        system.debug('Message: ' + cc.message + ' ************************************************************************************* ');
        system.assert(cc.message==null||cc.message=='The following fields are still required to save<BR><BR>null');
        
        cc.convertLead();
        system.debug('Message: ' + cc.message + ' ************************************************************************************* ');
        //system.assert(cc.message==null||cc.message=='The following fields are still required to save<BR><BR>null');
        cc.obj.isConverted__c = true;
        
        cc.inlineSave();
        system.debug('cc.message: '+cc.message);
        //system.assert(cc.message==null||cc.message=='The following fields are still required to save<BR><BR>null');
        
        pc_POC__c poc = new pc_POC__c(patient_case__c=pc.id, recordtypeid=[select id from recordtype where sObjectType = 'pc_POC__c' and name= 'Initial'].id);
        insert poc;
        
        poc.Plan_of_Care_accepted__c = date.today();
        update poc;
        
        cc.obj.current_nicotine_user__c = 'Yes';
        cc.obj.continineCompleted__c= true;
        cc.inlineSave();
        
        poc.Plan_of_Care_accepted__c = date.today().addDays(-1);
        update poc;
        
        
    }

}