public with sharing class searchAcl extends searchEncrypted{
    
    public Appeal_Case__c[] lstAcl {get; private set;}
    
    public searchAcl(string searchBy ){
        
        if(searchBy != null){
            search(searchBy );
        }        
    }
    
    public searchAcl(){

        
        string searchBy = ApexPages.currentPage().getParameters().get('st');
        
        if(searchBy !=null){
            search(searchBy );
        }
        
    }
    
    void search(string searchBy){
        boolean nameSearch=false;
        string tsearchBy = searchBy;
        string[] foo = new string[]{};
        
        if(searchBy!=null&&searchBy.contains(',')){
            searchBy=searchBy.replaceAll(',',' ');
        }
        
        if(searchBy!=null&&searchBy.contains(' ')){
            //string foo = searchBy.split(' ');
            nameSearch = true;            
            searchBy=searchBy.replaceAll('  ',' ');
            searchBy=searchBy.replaceAll(' ',';');
            
        }
        setSearchTerm(searchBy);
        
        string[] searchFields = new string[]{};
        searchFields.add('Member_First_Name__c');
        searchFields.add('Member_Last_Name__c');
        searchFields.add('Case_Number__c');
        searchFields.add('Member_ID__c');
        searchFields.add('Client__c');
        searchFields.add('Appeal_Type__c');
        searchFields.add('createdDate');
        string searchString = '';
        
       // for(String s : searchFields){
       //     searchString = searchString+','+s;
       // }
        
        searchString = setSearchString(searchFields,'Appeal_Case__c');
        lstAcl = search(searchString, searchFields);
        
        if(lstAcl.isEmpty() && nameSearch){
            searchFields.clear();
            searchFields.add('Member_Last_Name__c');
            searchFields.add('Member_First_Name__c');
            searchFields.add('Case_Number__c');
            searchFields.add('Member_ID__c');
            searchFields.add('Client__c');
            searchFields.add('Appeal_Type__c');
            searchFields.add('createdDate');
            
            
            searchString = setSearchString(searchFields,'Appeal_Case__c');
            searchString += ' order by createdDate asc';
            system.debug(searchString);
            
            lstAcl = search(searchString, searchFields);
        }
        
        searchBy=searchBy.replaceAll(';',' ');
        setSearchTerm(tsearchBy);
    
    }
    
}