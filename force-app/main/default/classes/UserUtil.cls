public class UserUtil {
    //Protected Members
    private static final UserUtil instance = new UserUtil();
    private Map<Id, User> mapUsers;

    //Properties
    public static User CurrentUser {
        get { return getUser(UserInfo.getUserId()); }
    }

    //Constructor
    private UserUtil() {
        //mapUsers = new Map<Id, User>(queryUsers());
    }

    //Public Methods
    public static User getUser(Id userId) {
        if (instance.mapUsers.containsKey(userId)) {
            return instance.mapUsers.get(userId);
        }
        else {
            throw new InvalidUserIdException('Unable to locate user id: ' + userId);
        }
    }
    
    public static boolean getIsGrpMember(string group_name){
        try{
            groupMember gm = [select group.DeveloperName from GroupMember where UserOrGroupId = :UserInfo.getUserId() and group.DeveloperName =:group_name];
            if(gm.id!=null){
                return true;
            }
        }catch(queryException e){
            return false;
        }
        return false;
    }
    
    public static boolean getIsHDPGrpMember(){
        try{
            groupMember gm = [select group.DeveloperName from GroupMember where UserOrGroupId = :UserInfo.getUserId() and group.DeveloperName ='Group_HDP'];
            if(gm.id!=null){
                return true;
            }
        }catch(queryException e){
            return false;
        }
        return false;
    }
    
    //Private Methods
    private List<User> queryUsers() {
        return [SELECT UserName,
                       facility__c,
                       Alias FROM User where isactive = true];
    }
    
    //Internal Classes
    public class InvalidUserIdException extends Exception {}

}