public with sharing class documentStorageComponentController{
    
    public string coeFileDirectory {get; set;}
    public string s_object {get; set;}
    public string recordId {get; set;}
    public string baseDir {get; set;}
    public string fileServerUrl {get; set;}
    public string file_description {get; set;}
    public boolean overrideSave {get; set;}
    
    public string file_id {get; private set;}
    public boolean isError {get; private set;}
    
    public documentStorageComponentController(){
        fileServerUrl = fileServer__c.getInstance().fileserverURL__c;
    }
    
    public void deleteAttachment(){
        isError=false;
        string fileId = ApexPages.CurrentPage().getParameters().get('fileId');
        ftpAttachment__c ftpAttachment = new ftpAttachment__c(id=fileId);
        delete ftpAttachment;
        loadAttachments();
    }
    
    public static string safePath(string p){
        string path = p;
        path= path.replaceAll('"', '');
        //path= path.replaceAll('-', '_'); 11-19-21 removed causing issues on bariatric attachments
        path = String.escapeSingleQuotes(path);
        return path;
    }
    
    public void addAttachment(){
        
        string fileName = ApexPages.CurrentPage().getParameters().get('fileName');
        
        try{
            //fileName= fileName.replace('\\', '\'');
            fileName= fileName.replaceAll('-', '_');
            fileName= fileName.replaceAll(',', '_');
            fileName= fileName.replaceAll('#', '_');
            fileName= fileName.replaceAll('&', '_');
            fileName= fileName.replaceAll('\'', '');
            coeFileDirectory = safePath(coeFileDirectory);
            
        }catch(exception e){
            system.debug(e.getMessage()+' '+e.getLineNumber());
            return;
        }
        
        switch on s_object.tolowercase(){
            when 'utilization_management__c' {
                utilizationManagementDocumentStorage ds = new utilizationManagementDocumentStorage();
                file_id =ds.saveDocument(recordId, coeFileDirectory, fileName, file_description).resultMessage;
                
            }
            when 'patient_case__c' {
                coeDocumentStorage ds = new coeDocumentStorage();
                file_id = ds.saveDocument(recordId, coeFileDirectory, filename, file_description);
            }
            when 'oncology__c' {
                coeDocumentStorage  ds = new coeDocumentStorage();
                file_id = ds.saveOncologyDocument(recordId, coeFileDirectory, filename, file_description);
            }
            when 'bariatric__c' {
                coeDocumentStorage ds = new coeDocumentStorage();
                file_id = ds.saveBariatricDocument(recordId, coeFileDirectory, filename, file_description);
            }
            when else {
               
            }
        }
        
        loadAttachments();
    }
    
    void loadAttachments(){
        //attachments = new ftpAttachment__c[]{};
        //attachments = [select subDirectory__c, fileName__c,createdby.name, createdDate from ftpAttachment__c where oncology__c =:o.id order by createdDate desc];
        
    }
}