public with sharing class rxTransSearchController {

    public string rxFrom {get; set;}
    public string rxTo {get; set;}
    
    public string memberSeq {get; set;}
    
    public string clientRxFrom {get; set;}
    public string clientRxTo {get; set;}
    
    public static boolean searchRun = false;
    
    public decimal totalDed {get; private set;}
    public decimal totalOOP {get; private set;}

    public hpRxSearchStruct rxSearchResults {get; private set;}
    public rxAudit__c[] rxerrors;
    
    public PaginatedsObjectList pgrxErrorList {get; private set;}
    
    public string grp {get; set;}
    public string eSSN {get; set;}
    public string uw {get; set;}
    public string seq {get; set;}
    public List<SelectOption> client {get; set;}
     
    public string rxSearchResultsJSON {get; set;}
    
    public rxTransSearchController(){
        rxSearchResultsJSON = '';
        loadClientMap();
        rxSearchResults = new hpRxSearchStruct();
        rxSearchResults.RxTxns = new rxAudit__c[]{};
        pgrxErrorList = new PaginatedsObjectList();
        searchRun= false;
        rxFrom = clientRxFrom;
        rxTo = clientRxTo;
         
    }
    
    public void rxSearch(){
    
        if(seq=='true'){
            seq='';
        }else{
            if(memberSeq!=null){
                seq = memberSeq;
            }else{
                seq='00';
            }
        }
        
              
        try{
            if(rxFrom != null && rxFrom != ''){
                 rxFrom = rxFrom.right(4)+rxFrom.left(2)+rxFrom.mid(3,2);
        
            }
        }catch(exception e){}
        
        try{
            if(rxTo != null && rxTo != ''){
                rxTo = rxTo.right(4)+rxTo.left(2)+rxTo.mid(3,2);
        
            }
        }catch(exception e){}
        
        
        rxSearchResults = hp_rxSearch.searchHP(uw,eSSN,grp,seq, rxFrom, rxTo);
        
        totalDed = 0;
        totalOOP = 0;
        
        if(rxSearchResults.ErrorReason != null || (rxSearchResults.ResultCode==null || rxSearchResults.ResultCode=='2')){
            searchRun = true;
            setRxErrors(new sobject[]{});
            return;
        }
        
        for(rxAudit__c s: rxSearchResults.RxTxns){
        
            if(s.Oopamount__c!=null){
                totalOop += decimal.valueof(s.Oopamount__c.trim());
            }
            
            if(s.Dedamount__c!=null){
                totalDed += decimal.valueof(s.Dedamount__c.trim());
            }
        }
        
        searchRun=true;
        setRxErrors(rxSearchResults.RxTxns);
        fieldName ='DOS__c';
        rxLastFieldSort = 'DOS__c';
        sortDir=0;
    }
    
    public rxAudit__c[] getRxErrors(){
        
        rxErrors = (rxAudit__c[])pgrxErrorList.getviewList();
        return rxErrors;
    }
    
    map<string, client__c> clientMap;
    
    void loadClientMap(){
        
        if(client==null){
            client = new List<SelectOption>();
            client.add(new SelectOption('', '--'));
            clientMap = utilities.clientMap();
            for(string c : clientMap.keySet()){
                client.add(new SelectOption(c, clientMap.get(c).name));
            }
                    
        }
    
    }
    
    /* Excel Download  */
    
    public PageReference excelDownload(){
        
        PageReference rxDownload = new PageReference('/apex/rxTransSearchDownload?data='+ EncodingUtil.urlEncode(JSON.serialize(rxSearchResults.RxTxns), 'UTF-8'));
        return rxDownload.setRedirect(True);
    }
    
    /* Paginate  */
 
    void setRxErrors(sObject[] soList){
        pgrxErrorList.setSoList(soList);
        
    }
    
    /* Sort */
    
    public string fieldName {get; private set;}
    public integer sortDir {get; private set;} //1 desc, 0 asc
    string rxLastFieldSort;
    
    public void sortByFieldVF(){
    
        fieldName = ApexPages.CurrentPage().getParameters().get('rxFieldSort');
        ApexPages.CurrentPage().getParameters().put('rxFieldSort',null);
        
        if(rxLastFieldSort==fieldName){
            sSorter.reverse(pgrxErrorList.getSoList());
            if(sortDir==0){sortDir=1;}else{sortDir=0;}
        
        }else{
            rxLastFieldSort=fieldName;
            sSorter.sortByField((rxAudit__c[])pgrxErrorList.getSoList(), fieldName);
            sortDir=0;
        }    
    
    }
    
}