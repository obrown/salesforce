public with sharing class cmProviderMinor{
    
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        /*
        letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c+'<br/>';
        letterText+=Clinician.Street__c+'<br/>';
        letterText+=Clinician.City__c+', '+'<nbsp/>';
        letterText+=Clinician.State__c+' ';
        letterText+=Clinician.Zip_Code__c+'<br/>';
        letterText+='<br/>';
        */

        //not pass null clinician values on the letter
        if(Clinician.Credentials__c!=null){
           letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c+'<br/>';
        }else{
           letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+'<br/>';
        }        
    
        //not pass null values on the letter
        if(Clinician.Street__c!=null){
           letterText+=Clinician.Street__c+', '+'<nbsp/>';
        }

        if(Clinician.State__c!=null){
           letterText+=Clinician.State__c+', '+'<nbsp/>';
        }

        if(Clinician.Zip_Code__c!=null){
           letterText+=Clinician.Zip_Code__c+'<br/>';
        }

        letterText+='</div>';
        letterText+='<p>';
        //letterText+='Dear&nbsp;'+cm.patient__r.Patient_First_Name__c+'&nbsp;'+cm.patient__r.Patient_Last_Name__c+',';
        letterText+='Dear&nbsp;'+Clinician.First_Name__c+' '+Clinician.Last_Name__c+',';
        letterText+='</p>';
        letterText+='<p>';  
        //letterText+='Your patient’s parent(s)/guardian(s) have enrolled your patient in the Contigo Health™ Case Management program. This is a voluntary benefit provided at no additional cost. Case Management is a program';
        letterText+='Your patient’s parent(s)/guardian(s) have enrolled your patient '+cm.patient__r.Patient_First_Name__c+' '+cm.patient__r.Patient_Last_Name__c+' in the Contigo Health™ Case Management program.';
        letterText+='This is a voluntary benefit provided at no additional cost. Case Management is a program to coordinate care and support your patient’s treatment plan within their medical benefits.<br/>';
        //letterText+=' is a program to coordinate care and support your patient’s treatment plan within their medical benefits.<br/>';
        letterText+='</p>';

        letterText+='My role as their Case Manager is to work collaboratively with you and your patient to: <br/>';
        letterText+='<ul style="list-style-type:none;">';
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='Promote compliance with the treatment plan'+'</br>';
        letterText+='</li>';
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='Facilitate your patient’s ability to function at the highest level of independence'+'</br>';
        letterText+='</li>';
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='Maximize medical plan benefits'+'</br>';
        letterText+='</li>';                        
        letterText+='</ul>';

        letterText+='<p>';  
        letterText+='Please feel free to contact me at 1-877-891-2690, should you have any questions regarding the Case Management program,';
        letterText+=' Monday-Friday 8:30 – 5:00, EST.<br/>';
        letterText+='</p>';         

        letterText+='Sincerely,<br/><br/>';
        letterText+='Care Management Department<br/>';
        letterText+='Contigo Health<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';

        return letterText;
    }
    
}