public with sharing class recommendedFacilityRuleExt {
    
    public Recommended_Facility_Rule__c rfr {get; set;}
    ApexPages.StandardController c;
    
    public map<string, Recommendation_Logic__c[]> logicListMap {get; private set;}
    public Recommendation_Logic__c logic {get; set;}
    selectOption[] PatientCaseFields;
    
    public string[] levelSet {get; private set;}
    
    public string clientLogo {get; private set;}
    public boolean isError {get; private set;}
    string fieldType;
    public string logicString {get; private set;}
    
    selectOption[] fieldValues;
    
    public recommendedFacilityRuleExt(ApexPages.StandardController controller) {
        
        this.c = controller;
        rfr = (Recommended_Facility_Rule__c)c.getRecord();
        clientLogo = '/phmLogos/'+rfr.Client_Facility__r.client__r.logoDirectory__c;
        loadLogicMap();
        
    }
    
    /* rule logic */
    
    public void loadLogicMap(){
        
      logicListMap = new map<string, Recommendation_Logic__c[]>();
      integer prev_level;
      logicString = 'No Logic';
      Recommendation_Logic__c[] logicList = [select Field_Label__c,
                                                    Field_Logic__c,
                                                    Field_name__c,
                                                    Field_Value__c,
                                                    Level__c,
                                                    L1L2Rule_Logic__c,
                                                    L2L3Rule_Logic__c,
                                                    Recommended_Facility_Rule__r.Criteria__c,
                                                    Recommended_Facility_Rule__r.Client_Facility__c,
                                                    Recommended_Facility_Rule__r.L1Order__c,
                                                    Recommended_Facility_Rule__r.L2Order__c,
                                                    Recommended_Facility_Rule__r.L3Order__c,
                                                    Recommended_Facility_Rule__r.L2Rule_Order__c,
                                                    Recommended_Facility_Rule__r.L1Rule_Order__c,
                                                    Name,
                                                    Recommended_Facility_Rule__c from Recommendation_Logic__c where Recommended_Facility_Rule__c = :rfr.id order by level__c asc, createdDate asc];
       
      for(Recommendation_Logic__c rl : logicList){
          
          if(rl.Level__c==null){rl.Level__c=1;}                                     
          Recommendation_Logic__c[] foo = logicListMap.get(string.valueof(rl.Level__c));
          
          if(foo==null){foo = new Recommendation_Logic__c[]{};}
          foo.add(rl);
          logicListMap.put(string.valueof(rl.Level__c), foo);
          
          if(logicString =='No Logic'){logicString ='';}
          integer level = integer.valueof(rl.Level__c);
          
          if(level!=null){
            
            if(prev_level!=null &&
              (prev_level != integer.valueof(rl.level__c))){
                  logicString = logicString.left(logicString.length()-8)+'<br>';
                  logicString = logicString+ rl.Recommended_Facility_Rule__r.get('L'+ prev_level +'Rule_Order__c')+ '<br>';
                  if(rl.Field_Label__c.contains('Distance to')){
                      logicString = logicString+''+rl.name;
                  }else{
                      logicString = logicString+''+rl.Field_Label__c +' '+ rl.Field_Logic__c +' '+ rl.field_value__c;
                  }
            }else{
                if(rl.Field_Label__c.contains('Distance to')){
                    logicString = logicString+''+rl.name;
                }else{
                    logicString = logicString+''+rl.Field_Label__c +' '+ rl.Field_Logic__c +' '+ rl.field_value__c;
                }
            }
            
            if(logicList[logicList.size()-1].id != rl.id){
                logicString = logicString +' '+rl.Recommended_Facility_Rule__r.get('L'+level+'Order__c')+'<br>';
            }
              
          
          }
            
          prev_level= integer.valueof(rl.level__c);
          
      }
      
      levelSet = new list<string>(logicListMap.keyset());  
      logicString = logicString.removeEndIgnoreCase('OR');
      rfr.Criteria__c =logicString.replaceAll('<br>','\n');
    
    }
    
    Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Patient_Case__c.fields.getMap();
    
    public string getfieldType(){
        
        return fieldType;
    }
     
    public void setfieldType(){
        
        if(logic==null){
            fieldType ='STRING';
            return;
        }
        
        string fieldName = logic.Field_name__c; 
        
        if(fieldName==null){
            fieldType ='STRING';
            return;
        }
        
        if(fieldName=='Mileage__c'){
            fieldType ='MILEAGEDISTANCE';
            return;
        }
          
        try{
            Schema.SObjectField field;
            field = fieldMap.get(fieldName);
            schema.describefieldresult dfield = field.getDescribe();
            fieldType = string.valueof(dfield.getType());
            
        }catch(exception e){
            //catch silently
            fieldType='PICKLIST';
        }
        
    } 
     
    public selectOption[] getfieldValues(){
        
        fieldValues = new selectOption[]{};
        string fieldName = logic.Field_name__c; 
        
        if(fieldName==null){
            return fieldValues;
        }
        
        fieldValues.add(new SelectOption('', ''));
        
        Schema.SObjectField field;
        
        try{
            field = fieldMap.get(fieldName);
        }catch(exception e){
            //catch silently
        }
        
        if(field!=null && fieldName!='CaseLatitudeLongitude__c'){
        
            schema.describefieldresult dfield = field.getDescribe();
            List<Schema.PicklistEntry> ple = dfield.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple){
              fieldValues.add(new SelectOption(f.getLabel(), f.getValue()));
            }
            
            if('Mileage__c'==fieldName){
                fieldType='MILEAGEDISTANCE';
            }
            
        }else if(fieldName=='CaseLatitudeLongitude__c'){
            for(Client_Facility__c cf :[select facility__c, 
                                               facility__r.name,
                                               facility__r.longitude__c,
                                               facility__r.latitude__c
                                               from Client_Facility__c where client__c = :rfr.Client_Facility__r.client__c and procedure__c = :rfr.Client_Facility__r.procedure__c ]){
                if(rfr.Client_Facility__r.facility__r.name!= cf.facility__r.name){
                    fieldValues.add(new selectOption(cf.facility__r.latitude__c+','+cf.facility__r.longitude__c, cf.facility__r.name));
                }
            }
            fieldType='FACILITYDISTANCE';
        }
        
        SelectOptionSorter.doSort(fieldValues, SelectOptionSorter.FieldToSort.Label); 
        return fieldValues;
    }
    /*
    public selectOption[] getPatientCaseFields(){
        
        if(PatientCaseFields!=null){return PatientCaseFields;}
        PatientCaseFields = new selectOption[]{};
        for(Schema.SObjectField field : fieldMap.Values()){
            
            schema.describefieldresult dfield = field.getDescribe();
            string type = string.valueof(dfield.getType());
            
            if(type=='PICKLIST' || type =='STRING'|| type =='DOUBLE'){
                if(dfield.getname()=='CaseLatitudeLongitude__c'){
                    PatientCaseFields.add(new selectOption(dfield.getname(), 'Distance to ' + rfr.client_facility__r.facility__r.name));
                }else{
                    PatientCaseFields.add(new selectOption(dfield.getname(), dfield.getLabel()));
                }                
            }
        
        }
        
        SelectOptionSorter.doSort(PatientCaseFields, SelectOptionSorter.FieldToSort.Label);
        
        return PatientCaseFields;
        
    }
    */
    public selectOption[] getPatientCaseFields(){
    
        string procedure = [select Client_Facility__r.procedure__r.name from Recommended_Facility_Rule__c where id = :rfr.id].Client_Facility__r.procedure__r.name;
        coeLogicFields clf = new coeLogicFields(procedure);
        
        if(PatientCaseFields!=null){return PatientCaseFields;}
        PatientCaseFields = new selectOption[]{};
        PatientCaseFields.add(new selectOption('','--None--'));
        for(string fieldName : clf.clientFacilitySetupDisplayFields){
            
            Schema.SObjectField field = fieldMap.get(fieldName);
            schema.describefieldresult dfield = field.getDescribe();
            string type = string.valueof(dfield.getType());
            string fn = dfield.getname();
            // && (type== 'ENCRYPTEDSTRING' || type=='PICKLIST' || type =='STRING'|| type =='DOUBLE');
            PatientCaseFields.add(new selectOption(dfield.getname(), dfield.getLabel()));
            
        }
        
        PatientCaseFields.add(new selectOption('CaseLatitudeLongitude__c', 'Distance to ' + rfr.client_facility__r.facility__r.name));
            
        SelectOptionSorter.doSort(PatientCaseFields, SelectOptionSorter.FieldToSort.Label);
        return PatientCaseFields;
        
    }
    
    public void newLogicRule(){
        logic = new Recommendation_Logic__c(Recommended_Facility_Rule__c=rfr.id);
    }
    
    public pageReference deleteLogic(){
        delete logic;
        return c.view();
    }
    
    public void saveRfr(){
        try{
            update rfr;
        }catch(dmlexception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getDMLmessage(0)));
        }
    }
    
    public void saveLogic(){
        
        string prefix = Facility__c.sobjecttype.getDescribe().getKeyPrefix();
        if(logic.Field_name__c=='CaseLatitudeLongitude__c'){
            for(selectOption so: PatientCaseFields){
                if(so.getValue()==logic.field_name__c){
                    logic.field_label__c = so.getLabel();
                    break;
                }
            }
            
            for(selectOption so: fieldValues){
                if(so.getValue()==logic.Field_Value__c){
                    logic.Field_Value__c=so.getValue();
                    logic.Display_Value__c=so.getLabel();
                    break;
                }
            }
            
        }else{
        
            for(selectOption so: PatientCaseFields){
                if(so.getValue()==logic.field_name__c){
                    logic.field_label__c = so.getLabel();
                    logic.Display_Value__c=logic.field_value__c;
                    break;
                }
            }
        
        }
        
        logic.field_type__c=fieldType;
        
        if(levelSet!=null && !levelSet.isEmpty()){
            logic.level__c=integer.valueof(levelSet[levelSet.size()-1]);
        }else{
            logic.level__c=1;
        }
        
        try{
            isError=false;
            upsert logic;
            update rfr;
            loadLogicMap();
            
        }catch(dmlexception e){
             isError=true;       
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getDMLmessage(0)));
             return;  
        }
        
    }
    
    public void loadLogic(){
        
        string logicId = string.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('logicId'));
        
        for(string s : logicListMap.keyset()){
            for(Recommendation_Logic__c rl : logicListMap.get(s)){
                if(rl.id == logicId){
                    logic=rl;
                    break;
                }
            }
        }
        
        setfieldType();
        
        /*
        if(logic.field_value__c != null && logic.field_value__c.contains('Distance to')){
            logic.field_type__c='PICKLIST';
        }else{
            setfieldType();
        }
        */
    }
    
    public void updateLogicLevel(){
        
        string newLevel = String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('newLevel'));
        string logicId = String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('logicId'));
        
        if(newLevel==null || logicId==null){
            return;
        }
        integer upper=0;
        if(newLevel=='z'){
            
            string previousLevel = String.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('previousLevel'));
            
            integer dbPreviousLevel = integer.valueOf(previousLevel);
            dbPreviousLevel = dbPreviousLevel+1;
            
            for(string p: logicListMap.keyset()){
                if(integer.valueof(p)>upper){
                    upper = integer.valueof(p);
                }
            }
            
            if((upper+1)<=3){
                newLevel = string.valueof(upper+1);
            }else{
                newLevel = '3';
            }
        }
         
        Recommendation_Logic__c tempLogicHolder;
        
        for(string p: logicListMap.keyset()){
            
            if(tempLogicHolder==null){
           
                Recommendation_Logic__c[] logics = logicListMap.get(p);
            
                for(Recommendation_Logic__c logic : logics ){
                    
                    if(logic.id.equals(logicId)){
                        tempLogicHolder = logic;
                        break;
                    }
                    
                }
            
                if(tempLogicHolder!=null && logics.size()==1 && integer.valueof(p)<integer.valueof(newLevel)){
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'One rule must exist in each level before a new level is set. '));
                    return;
                
                }
            
            }
            
        }
        
        if(tempLogicHolder!=null){
        
            tempLogicHolder.level__c= integer.valueof(newLevel);
            update tempLogicHolder;
            loadLogicMap();
        }
        
    }
    
    /* end rule logic */
    
    public pageReference cloneOverride(){
        Recommended_Facility_Rule__c newRule = rfr.clone();
        newRule.name = 'New Cloned Rule of ' + rfr.name;
        insert newRule;
        
        Recommendation_Logic__c[] newLogicList = new Recommendation_Logic__c[]{};
        Recommendation_Logic__c newLogic;
        for(string i : logicListMap.keyset()){
            for(Recommendation_Logic__c rl : logicListMap.get(i)){
                newLogic = rl.clone(false, true, false, false);
                newLogic.Recommended_Facility_Rule__c= newRule.id;
                newLogicList.add(newLogic);
           
            }
        }
        insert newLogicList;
        return new pageReference('/'+newRule.id);
    }
    
   
    
}