@RestResource(urlMapping='/ecen/v1/mobileUserType')
global class ECENMobileUserType{
    
     @HttpGet   
     global static string doGet() {
        String ins_id = RestContext.request.headers.get('Ins_id');
        
        if(ins_id==null){
            return 'Not found';
        }
         
        try{
            oncology__c oncology = [select id from oncology__c where bid_id__c = :ins_id];
            return 'coeUser';         
        }catch(queryException e){}
        
        try{
            patient_case__c pc = [select id from patient_case__c  where bid_id__c = :ins_id];
            return 'coeUser';         
        }catch(queryException e){}
        
        try{
            bariatric__c b = [select id from bariatric__c where bid_id__c = :ins_id];
            return 'coeUser';         
        }catch(queryException e){}
         
        try{
           
           string fooCert = ins_id.left(5);  
           boolean pFound=false;
           phm_patient__c[] pList = [select id,cert__c from phm_patient__c where certSearch__c = :fooCert];
             
           for(phm_patient__c pat : pList){
               if(pat.cert__c==ins_id){
                   pFound=true;
                   break;
                      
               }
                  
           }
           
           if(pFound){
               return 'nonCOEUser';
           }
                 
        }catch(queryException e){}
          
         
        return 'Not found';
     }
    
}