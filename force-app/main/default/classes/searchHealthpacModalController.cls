public class searchHealthpacModalController{
    
    public set<string> eSSNKeys {get; private set;}
    public map<string, hpPatientSearchStruct.PatSearchResultLogs[]> resultsMap {get; set;}
    public map<string, searchResult[]> resultsUMap {get; set;}
    
    public searchResult claimSearchResult {get; private set;}
    
    public hpClaimSearchStruct.EligibilityInfo[] claimsFound {get; set;}
    
    string searchESSN;
    string searchCERT;
    public string iRecordId {get; set;}
    public string hpset {get; set;}
    public boolean hpSearchfound {get; private set;}
    public boolean hpSearchRun {get; private set;}
    public boolean claimSearch {get; private set;}
    public string essn {get; set;}
    
    public map<string, string> clientMap {get; private set;}
    public List<SelectOption> client {get; set;}
    
    public string claimNumber {get; set;}
    
    public boolean error {get; private set;}
    
    public searchHealthpacModalController(){
        loadClientMap();
    }
    
    public void selectResult(){
        
        if(iRecordId==null){return; }
        Operations_Work_Task__c foo = new Operations_Work_Task__c(id=iRecordId);
        
        string eSSN = ApexPages.currentPage().getParameters().get('pEssn');
        string pSeq  = ApexPages.currentPage().getParameters().get('pSeq');
        string status = ApexPages.currentPage().getParameters().get('status');
        string uw = ApexPages.currentPage().getParameters().get('uw');
        
        for(hpPatientSearchStruct.PatSearchResultLogs r: resultsMap.get(eSSN)){
            system.debug(r.sequence);
            system.debug(r.Status);
            
            if(r.sequence==pSeq && r.Status == status && r.Underwriter == uw){
                foo.EESSN__c =r.SSN;
                foo.Sequence__c= r.Sequence;
                foo.Group__c= r.grp;
                foo.Patient_Fname__c= r.PatFirstName;
                foo.Patient_Lname__c= r.PatLastName;
                foo.Patient_Dob__c= r.PatDateOfBirth;
                foo.Underwriter__c= r.Underwriter;
                foo.Member_Number__c = r.memberNumber;
                //foo.attachToClaim__c = false;
                //foo.Claim_Number__c = null;
                update foo; 
                break;
            }
        }
      
    }
    
    public void searchHealthpac(){
        
        string uw   = ApexPages.currentPage().getParameters().get('uw');
        string eSSN = ApexPages.currentPage().getParameters().get('essn');
        string cert = ApexPages.currentPage().getParameters().get('cert');  
        string efn  = ApexPages.currentPage().getParameters().get('efn'); 
        string eln  = ApexPages.currentPage().getParameters().get('eln'); 
        claimNumber = ApexPages.currentPage().getParameters().get('claimNumber'); 
        
        hpSearchRun = true;
        hpSearchfound = false;
        
        if(claimNumber!='' && claimNumber!=null){
            searchClaims(claimNumber);
            return;
        }
        
        claimsFound=null;
        claimSearch=false;
        searchESSN = eSSN;
        searchCERT = cert;
        
        resultsMap = new map<string, hpPatientSearchStruct.PatSearchResultLogs[]>();
        hpPatientSearchStruct.PatSearchResultLogs[] searchResults;
        
        eSSNKeys = new set<string>();
        hpPatientSearchStruct foo = hp_patientSearch.searchHP(uw, essn, cert, efn, eln);
        error=false;
        
        if(foo.ResultCode=='1'){
            return;
        }
        
        if(foo == null || foo.ResultCode != '2' || foo.ErrorMsg != ''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error has occured'));
            error=true;
            return;
        }else if(foo.PatSearchResultLogs == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'No results found'));
            return;
        }
        
        for(hpPatientSearchStruct.PatSearchResultLogs s : foo.PatSearchResultLogs ){
            
            eSSNKeys.add(s.SSN);
            
            if(resultsMap.keyset().contains(s.SSN)){
                searchResults = resultsMap.get(s.SSN);
            }else{
                searchResults = new hpPatientSearchStruct.PatSearchResultLogs[]{};
            }
            
            searchResults.add(new hpPatientSearchStruct.PatSearchResultLogs(
                                                          s.Underwriter,
                                                         s.Grp,
                                                         s.SSN,
                                                         s.EmpFirstName ,
                                                         s.EmpLastName ,
                                                         s.EmpDateOfBirth ,
                                                         s.EmpAddress1,
                                                         s.EmpAddress2, 
                                                         s.EmpCity,
                                                         s.EmpState,
                                                         s.EmpZip,
                                                         s.Sequence,
                                                         s.PatFirstName,
                                                         s.PatLastName,
                                                         s.PatDateOfBirth,
                                                         decodeRelationship(s.Relationship),
                                                         s.Gender,
                                                         decodeStatus(s.Status),
                                                         s.Dept,
                                                         //s.EligDate,
                                                         safeTrim(s.MemberNumber),
                                                         setClient(s.Underwriter)));
            
             
            resultsMap.put(s.SSN, searchResults);
        
        }
        
        if(resultsMap.keyset().size()>0){
            hpSearchfound=true;
        }else{
            hpSearchfound=false;
        }
        
    }
    
    string safeTrim(string xx){string x=xx;if(x==null){x='';}else{x=x.trim();}return x;}
    
    public void selectClaimResult(){
        Operations_Work_Task__c foo = new Operations_Work_Task__c(id=iRecordId);
        
        string claimNumber= ApexPages.currentPage().getParameters().get('pclaimNumber');
        string eSSN = ApexPages.currentPage().getParameters().get('pEssn');
        string pSeq = ApexPages.currentPage().getParameters().get('pSeq');
        string status = ApexPages.currentPage().getParameters().get('status');
        
        for(hpPatientSearchStruct.PatSearchResultLogs r: resultsMap.get(eSSN)){
            
            if(r.sequence==pSeq && r.Status == status){
                foo.EESSN__c =r.SSN;
                foo.Sequence__c= r.Sequence;
                foo.Group__c= r.grp;
                foo.Patient_Fname__c= r.PatFirstName;
                foo.Patient_Lname__c= r.PatLastName;
                foo.Patient_Dob__c= r.PatDateOfBirth;
                foo.Underwriter__c= r.Underwriter;
                foo.Member_Number__c = r.memberNumber;
                //foo.attachToClaim__c = true;
                //foo.Claim_Number__c = ClaimNumber;
                update foo; 
                break;
            }
        }
        
    }
    
    public void searchClaims(string claimNumber){
        claimSearch=true;
        hpClaimSearchStruct clmSearchResult = hp_ClaimSearch.search(claimNumber);
        claimSearchResult = new searchResult();
        
        if(clmSearchResult.ResultCode=='1'){
            hpSearchfound=false;
            return;
        }
        hpSearchfound=true;
        hpPatientSearchStruct.PatSearchResultLogs[] searchResults = new hpPatientSearchStruct.PatSearchResultLogs[]{};
        
        claimsFound = new hpClaimSearchStruct.EligibilityInfo[]{};
        claimsFound =clmSearchResult.EligibilityInfo;
        
        if(claimsFound == null || claimsFound.isEmpty()){
            hpSearchfound=false;
            return;
        
        }
        
        claimSearchResult.PatFirstName = claimsFound[0].FirstName;
        claimSearchResult.PatLastName = claimsFound[0].LastName;
        claimSearchResult.ClientName = setClient(claimsFound[0].Underwriter);
        claimSearchResult.Underwriter= claimsFound[0].Underwriter;
        claimSearchResult.Grp = claimsFound[0].Grp;
        claimSearchResult.ClaimNumber =clmSearchResult.claimNumber;
        claimSearchResult.PatDateOfBirth = claimsFound[0].DateOfBirth;
        claimSearchResult.ssn = clmSearchResult.HPSsnSeq.left(9);
        claimSearchResult.sequence= clmSearchResult.HPSsnSeq.right(2);
        
        claimSearchResult.Relationship = decodeRelationship(clmSearchResult.Relationship);
        claimSearchResult.Status = decodeStatus(clmSearchResult.status);
        string dob = claimSearchResult.PatDateOfBirth;
        string edob='';
        if(dob != null){
            dob = dob.trim();
            dob = dob.mid(4,2) +'/'+ dob.right(2) +'/'+ dob.left(4);
        }
        
        if(claimSearchResult.sequence=='00'){
            claimSearchResult.EmpFirstName = claimSearchResult.PatFirstName;
            claimSearchResult.EmpLastName = claimSearchResult.PatLastName;
            claimSearchResult.EmpDateOfBirth = dob;
            edob=dob;
        }
        
        claimSearchResult.PatDateOfBirth= dob;
        eSSNKeys = new set<string>();
        eSSNKeys.add(clmSearchResult.HPSsnSeq.left(9));
        resultsMap = new map<string, hpPatientSearchStruct.PatSearchResultLogs[]>();
        searchResults.add(new hpPatientSearchStruct.PatSearchResultLogs(
                                                         claimsFound[0].Underwriter,
                                                         claimsFound[0].Grp,
                                                         clmSearchResult.HPSsnSeq.left(9),
                                                         claimSearchResult.EmpFirstName, 
                                                         claimSearchResult.EmpLastName, 
                                                         edob, //employee dob
                                                         '', //s.EmpAddress1,
                                                         '', //s.EmpAddress2, 
                                                         '', //s.EmpCity,
                                                         '', //s.EmpState,
                                                         '', //s.EmpZip,
                                                         clmSearchResult.HPSsnSeq.right(2),
                                                         claimsFound[0].FirstName, //s.PatFirstName,
                                                         claimsFound[0].LastName, //s.PatLastName,
                                                         dob, //s.PatDateOfBirth,
                                                         decodeRelationship(clmSearchResult.Relationship),
                                                         '', //gender
                                                         decodeStatus(clmSearchResult.status),
                                                         '', //Dept,
                                                         //s.EligDate,
                                                         '', //safeTrim(s.MemberNumber),
                                                         setClient(claimsFound[0].Underwriter)));
            
             
        resultsMap.put(clmSearchResult.HPSsnSeq.left(9), searchResults);
        hpSearchfound=true;
        return;
    }
        
    public void resetSearch(){
        hpSearchRun = false;
        hpSearchfound=false;
        
    }
    
    string setClient(string uw){
        loadClientMap();
        string foo = '';
        foo= clientMap.get(uw);
        if(foo==null){
            foo = uw;
        }
        return foo;
        
    }
    
    void loadClientMap(){
    
        if(clientMap==null){
            clientMap = new map<string, string>();
            client = new List<SelectOption>();
            client.add(new SelectOption('', ''));
            for(Client__c  c : [select underwriter__c,active__c, name from Client__c where underwriter__c != '' order by name asc]){
                clientMap.put(c.underwriter__c, c.name);
                if(c.underwriter__c != '502' && c.active__c){
                    client.add(new SelectOption(c.underwriter__c, c.name));
                }
            }
           
        }
    
    }
    
    string decodeStatus(string status){
    
        if(status=='A'){
            return 'Active';
        }else if(status=='T'){
            return 'Termed';
        }
    
        return 'NA';
    
    }
    
    string decodeRelationship(string rel){
        
        if(rel=='1' || rel=='01'){
            return 'Employee';
        }else if(rel=='2' || rel=='02'){
            return 'Spouse';
        }else if(rel=='3' || rel=='03'){
            return 'Son';
        }else if(rel=='4' || rel=='04'){
            return 'Daughter';
        }else if(rel=='5' || rel=='05'){
            return 'Other';
        }
        
        return 'NA';
        
    }
    
    class searchResult{
        public string ClientName {get; set;}
        public string ClaimNumber {get; set;}
        public string SSN {get; set;}
        public string PatFirstName {get; set;}
        public string PatLastName {get; set;}
        public string EmpFirstName {get; set;}
        public string EmpLastName {get; set;}
        public string EmpDateOfBirth {get; set;}
        public string Underwriter {get; set;}
        public string Grp {get; set;}
        public string PatDateOfBirth {get; set;}
        public string Sequence {get; set;}
        public string MemberNumber {get; set;}
        public string Relationship {get; set;}
        public string Status {get; set;}
        
    }
}