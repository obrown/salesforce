public with sharing class PaginatedsObjectList {

    sObject[] soList = new sObject[]{};
    public sObject[] viewList; 
    
    integer pageSize = 10;
    integer page = 1;
    
    public integer notesPerPage {get; set;}
    public boolean seeAll {get; set;}
    
    public PaginatedsObjectList(){
        this.seeAll =false;
    }
    
    public sObject[] getSoList(){
        return this.soList;
    }
    
    public void setSoList(sObject[] soList){
        this.soList = soList;
    }
    
    public void setpageSize(integer pageSize){
        this.pageSize = pageSize;
    
    }
    
    public void setpageSizeVF(){
        this.setPageSize(integer.valueof(ApexPages.CurrentPage().getParameters().get('pageSize')));
    }
    
    public integer getlistSize(){
        return soList.size();
        
    }
    
    public integer getPage(){
        return this.page;
        
    }
    
    public boolean gethasNext(){
        return getlistSize() > page*pageSize;
        
    }

    public boolean gethasPrevious(){
        return page>1;
        
    }
    
    public integer[] getNumOfPages(){
        
        integer[] ret = new integer[]{};
        
        if(seeAll){
            ret.add(1);
            return ret; 
            
        }
        
        for(integer i=1; i < getlistSize() / pageSize; i++){
            ret.add(i);
        }
        
        if(ret.size()==0){
            ret.add(1);
            
        }else{
            ret.add(ret[ret.size()-1]+1);
        }
        
        if(Math.mod(getlistSize(), pageSize)!=0 && pageSize < getlistSize()){
            ret.add(ret[ret.size()-1]+1);
        }
        
        return ret;
    }
    
    public void setPageVF(){
        
        string pgNum = ApexPages.CurrentPage().getParameters().get('pageNum');
        ApexPages.CurrentPage().getParameters().put('pageNum', null);
        
        if(pgNum==null){ pgNum='1';}
        
        this.setPageParam(integer.valueof(pgNum));
    }
    
    public sObject[] getviewList(){
        
        viewList = new sObject[]{};
        
        if(this.SoList==null){
            return viewList;
        }
        
        if(this.page==null){
            this.page=1;
        }
        
        if(this.seeAll){
            return this.soList;
        }
        
        integer pageUpper = page*pageSize;
        integer pageLower = pageUpper - pageSize; 
        
        if(pageUpper > getlistSize()){
            pageUpper = getlistSize();
        }
        
        for(integer i = pageLower; i < pageUpper; i++){
            viewList.add(soList[i]);
        }
        
        return viewList;
        
    }
    
    public void setPageParam(integer page){
        system.debug('set page:' +page);
        this.page = page;
        
    }
   
    public void seeAll(){
        seeAll=true;
        this.page = 1;
        this.viewList = this.soList;
        
    }
    
    public void seeLess(){
        seeAll=false;
        
    }    
    
}