public class hp_Sendnote extends hp_callWebApp{

    public hpRxSearchStruct returnObj = new hpRxSearchStruct();
    
    public static hpRxSearchStruct sendNote(string uw, string essn, string grp, string seq, string noteType, string username, string noteBody){
        
        hp_Sendnote sendNote= new hp_Sendnote ();
        sendNote.sendtheNote(uw, essn, grp, seq, noteType, username, noteBody);
        return sendNote.returnObj;
        
    }
    
    public hpRxSearchStruct sendtheNote(string uw, string essn, string grp, string seq, string noteType, string username, string noteBody){
        endpoint = 'putnote';
        
        if(uw==null){uw='';}
        if(essn==null){essn='';}
        if(seq==null){seq='';}
        if(grp==null){grp='';}
        
        search.add('Essn:'+essn);
        search.add('Underwriter:'+uw);
        search.add('Group:'+grp);
        search.add('NoteType:'+noteType);
        search.add('Noteusername:'+username);
        search.add('Notebody:'+noteBody);
        search.add('Seq:'+seq);
        
        jsonBody = jb.eligSearch(search);
        
        result = callWebApp(null);
        result = result.replaceAll('":"','__c":"');
        result = result.replace('ResultCode__c','ResultCode');
        result = result.replace('ErrorReason__c','ErrorReason');
        
         try{
            returnObj = (hpRxSearchStruct)JSON.deserialize(result, hpRxSearchStruct.class);
            
        }catch(exception e){
            system.debug(e.getMessage());
        }
        
        
        return returnObj;
    }
    
    
    
}