public with sharing class recentItems{




map<id, integer> tIDs = new map<id, integer>();
map<id, integer> pIDs = new map<id, integer>();

public ri[] riList {get; private set;}

public recentItems(){
    run();
}

public recentItems(facilityQueueController controller) {
    run();
    
}

void run(){
riList = new ri[]{};
integer count=0;
integer fcount=0;
for(RecentlyViewed x : [SELECT Id, Type FROM RecentlyViewed WHERE LastViewedDate != NULL and Type IN ('Transplant__c', 'Patient_Case__c') ORDER BY LastReferencedDate DESC limit 20]){
                     
                     string foo = string.valueof(x.get('Id'));
                     if(x.get('Type')=='Transplant__c'){
                         if(!tIDS.keySet().contains(foo)){
                             tIDs.put(foo, count); 
                             fCount++;
                         }
                         
                     }else{
                         if(!pIDS.keySet().contains(foo)){
                             pIDs.put(foo, count);
                             fCount++;
                         }
                         
                     }
                     count++;
                     if(fCount==9){break;}
}

if(tIDs.size()>0){
    
    for(Transplant__c t :[select patient_first_name__c,patient_last_name__c from Transplant__c where id IN :tIDs.keyset()]){
        riList.add(new ri(t.id,t.patient_last_name__c+', '+ t.patient_first_name__c,tIDs.get(t.id),0));
    }
}

if(pIDs.size()>0){
    
    for(Patient_case__c p :[select patient_first_name__c,patient_last_name__c from Patient_case__c where id IN :pIDs.keyset()]){
        riList.add(new ri(p.id, p.patient_last_name__c+', '+ p.patient_first_name__c,pIDs.get(p.id),1));
    }
}

ri[] tempRI = riList.clone();

for(ri r :riList){
    tempRI[r.count] = r;
}

riList = tempRI;

}

public class ri{
    
    public id theID {get; private set;}
    public string Name {get; private set;}
    public string logoURL {get; private set;}
    integer count;
    
    ri(id i, string d, integer c, integer t){
        this.name = d;
        this.theID = i;
        this.count = c;
        if(t==0){
          this.logoURL = '/img/icon/custom51_100/caduceus16.png';
        }else{
          this.logoURL = '/img/icon/custom51_100/redcross16.png';
        }
    }
    
}

}