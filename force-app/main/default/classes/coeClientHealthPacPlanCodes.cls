public class coeClientHealthPacPlanCodes{

    public static string planCode(string clientFacilityId, string planName){
        
        string hp='';
        if(clientFacilityId==null||clientFacilityId==''){
            hp = 'No HealthPac Plan Code Found';
            return hp;
        }
        
        try{
            hp = [select HealthPac_Plan_Code__c from HealthPac_Plan_Code__c where Client_Facility__c = :clientFacilityId and Case_Plan_Name__c=:planName].HealthPac_Plan_Code__c;
        }catch(exception e){
            hp = 'No HealthPac Plan Code Found';
        }
        
        return hp;
    }

}