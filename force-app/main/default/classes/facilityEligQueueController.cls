public with sharing class facilityEligQueueController{
    
    public patient_case__c[] newCases {get; private set;}
    public string logo {get; private set;}
    public string redirectClient {get; private set;}
    
    public facilityEligibility[] records {get; private set;}
    
    final string facility;
    set<id> loaded = new set<id>();
    
    
    public facilityEligQueueController(){
        
        //facility = [select facility__c from user where id =:userInfo.getUserID()].facility__c;
        loadNewCases();
    }
    
    public void loadNewCases(){
        
       newCases = new patient_case__c[]{};
       records = new facilityEligibility[]{};
       
       string userProfileName;
       
       try{
           userProfileName = [select name from profile where id = :userInfo.getProfileID()].name;
       }catch(exception e){
           userProfileName ='';
       }
       string caseType='';
       
       if(userProfileName.contains('Lowes')){
           caseType = ' \'%Lowes%\'';
           logo = '/logos/LowesLogo.jpg';
           redirectClient = 'Lowes'; 
       }else if(userProfileName.toLowerCase().contains('jetblue')){
           caseType = ' \'%Jetblue Spine%\'';
           logo = '/logos/jetBlue.jpg'; 
           redirectClient = 'jetBlue'; 
       }if(userProfileName.contains('NextEra')){
           caseType = ' \'%NextEra%\'';
           logo = '/logos/NextEra.jpg';
           redirectClient = 'Lowes'; 
       
       }else{
           caseType = ' \'%NextEra%\'';
           logo = '/logos/NextEra.jpg';
           redirectClient = 'Lowes'; 
       }
       
       string query = 'select ecen_procedure__r.name,case_type__c,Single_Deductible__c ,Relationship_to_the_Insured__c ,Effective_Date_of_Medical_Coverage__c ,pendingEligCheck__c,Patient_Zip_Postal_Code__c ,Patient_City__c ,Patient_State__c ,Patient_Street__c, Patient_Email_Address__c ,Patient_Mobile__c ,Patient_Home_Phone__c ,Patient_SSN__c ,Patient_DOBe__c ,Patient_DOB__c ,Name,Family_Deductible__c ,Eligible__c ,eNotes__c ,Employee_Primary_Health_Plan_Name__c ,Employee_Zip_Postal_Code__c ,Employee_City__c ,Employee_State__c,Employee_Street__c ,Employee_Email_Address__c ,Employee_Mobile__c ,Employee_Home_Phone__c ,Employee_SSN__c ,Employee_DOB__c ,Employee_DOBe__c ,RecordType.name, Client_Name__c ,Client__c ,Client_Carrier__c ,Carrier_Name__c ,Patient_and_Employee_SSN_verified__c ,Patient_and_Employee_DOB_verified__c ,BID__c , BID_Verified__c ,Carrier_and_Plan_Type_verified__c ,patient_first_name__c, patient_last_name__c, employee_first_name__c,employee_last_name__c, Date_Care_Mgmt_Transfers_to_Eligibility__c, Program_Type__c from patient_case__c where pendingEligCheck__c= true and case_type__c like';
       facilityEligibility fe = new facilityEligibility(); 
       for(patient_case__c p : Database.query(query + caseType)){
           
           records.add(fe.setPC(p));
           loaded.add(p.id);
           newCases.add(p);
       }
       
       
       for(bariatric__c b :[select Single_Deductible__c ,Relationship_to_the_Insured__c , Effective_Date_of_Medical_Coverage__c ,Patient_Zip_Code__c ,Patient_State__c ,Patient_Street__c ,Patient_City__c ,Patient_Email_Address__c ,Patient_Mobile_Phone__c ,Patient_Home_Phone__c ,Patient_SSN__c ,Patient_DOB__c ,Patient_First_Name__c ,Patient_Last_Name__c ,Name,Eligible__c ,Employee_Primary_Health_Plan_Name__c ,Employee_Zip_Code__c,Employee_City__c ,Employee_State__c ,Employee_Street__c ,Employee_Email_Address__c ,Employee_Mobile_Phone__c ,Employee_Home_Phone__c ,Employee_SSN__c ,Employee_DOB__c ,Employee_First_Name__c ,Employee_Last_Name__c ,Client_Name__c ,Client__c ,Client_Carrier__c ,Carrier_Name__c ,Verified_Employee_SSN__c ,Verified_Employee_DOB__c ,BID__c , BID_Verified__c, Date_Eligibility_Submitted__c from bariatric__c where status__c ='Open' and Date_Eligibility_Submitted__c !=null and Date_Eligibility_Check_with_Client__c = null order by Date_Eligibility_Submitted__c desc]){
           records.add(fe.setBariatric(b));
       }
                                   
    
    }

}