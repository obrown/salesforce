public with sharing class cloneCase {
	static string theLetter;

	public string cloneCase(id caseId, string relatedrecordtype) {
		Patient_Case__c clonedCase = clonedCase(caseID, relatedrecordtype);
		return clonedCase.id;
	}

	public string cloneCase(id caseId) {
		Patient_Case__c clonedCase = clonedCase(
			caseID,
			[SELECT recordtype.name FROM Patient_Case__c WHERE id = :caseId]
			.recordtype.name
		);
		return clonedCase.id;
	}

	private Patient_Case__c clonedCase(id caseID, string relatedrecordtype) {
		Patient_Case__c originalCase = new Patient_Case__c();
		//Referred_Facility__c,
		originalCase = [
			SELECT
				attachEncrypt__c,
				employeeId__c,
				patientId__c,
				relatedCreated__c,
				Current_Nicotine_User__c,
				Information_Packet_Follow_Up_Call__c,
				Info_Packet_Sent__c,
				Listed_as_Program_Covered_Procedure__c,
				What_procedure_was_recommended__c,
				Referral_Source__c,
				bid__c,
				Caller_Name__c,
				client_Name__c,
				client__c,
				Relationship_to_the_Insured__c,
				Callback_Number__c,
				Callback_Type__c,
				Language_Spoken__c,
				Translation_Services_Required__c,
				Employee_First_Name__c,
				Employee_Last_Name__c,
				Employee_DOBe__c,
				Employee_Gender__c,
				Employee_SSN__c,
				Employee_Primary_Health_Plan_ID__c,
				Employee_Primary_Health_Plan_Name__c,
				Secondary_Coverage__c,
				Medicare_As_Secondary_Coverage__c,
				Member__c,
				Patient_First_Name__c,
				Patient_Last_Name__c,
				Patient_DOBe__c,
				Patient_Gender__c,
				Patient_SSN__c,
				Same_as_Employee_Address__c,
				Employee_Street__c,
				Employee_City__c,
				Employee_State__c,
				Employee_Zip_Postal_Code__c,
				Employee_Country__c,
				Patient_Street__c,
				Patient_City__c,
				Patient_State__c,
				Patient_Zip_Postal_Code__c,
				Patient_Country__c,
				Employee_Home_Phone__c,
				Employee_Mobile__c,
				Employee_Work_Phone__c,
				Employee_Preferred_Phone__c,
				Employee_Best_Time_To_Contact__c,
				Employee_Email_Address__c,
				Patient_Home_Phone__c,
				Patient_Mobile__c,
				Patient_Work_Phone__c,
				Patient_Preferred_Phone__c,
				Patient_Best_Time_To_Contact__c,
				Patient_Email_Address__c,
				Caregiver_Name__c,
				Caregiver_DOB__c,
				Caregiver_Home_Phone__c,
				Caregiver_Mobile__c,
				Carrier_Name__c,
				Carrier_Nurse_Name__c,
				Carrier_Nurse_Phone__c,
				Carrier_E_mail__c,
				Diagnosis__c,
				Proposed_Procedure__c,
				ecen_procedure__c,
				Patient_Symptoms__c,
				Recent_Testing__c,
				Other_Pertinent_Medical_Info_History__c,
				Date_of_Last_Primary_Care_Phys_Visit__c,
				Date_of_Last_Cardiac_Visit__c,
				Date_of_Last_Specialist_Visit__c,
				Date_of_Last_Spine_Related_Visit__c,
				Procedure_Complexity__c,
				Expedited_Referral__c,
				Recent_visit_with_home_phys_prior_to_ref__c,
				Home_Phys_to_manage_care_post_program__c,
				Home_Phys_phone_access_to_program_phys__c,
				Advise_on_potential_dental_clearance__c,
				Explain_trans_to_home_medical_carrier__c,
				Overview_of_clinical_components_timeline__c,
				Review_pre_post_op_medication_coverage__c,
				Provide_options_to_obtain_waiver__c,
				RecordTypeid
			FROM Patient_Case__c
			WHERE id = :caseID
		];
		Patient_Case__c cC = new Patient_Case__c();

		cc.creating_case_id__c = originalCase.id;
		cc.client__c = originalCase.client__c;
		cc.employeeId__c = originalCase.employeeId__c;
		cc.patientId__c = originalCase.patientId__c;
		cc.ecen_procedure__c = originalCase.ecen_procedure__c;
		cC.bid__c = originalCase.bid__c;
		cC.Referral_Source__c = originalCase.Referral_Source__c;
		cC.Info_Packet_Sent__c = originalCase.Info_Packet_Sent__c;
		cC.Information_Packet_Follow_Up_Call__c = originalCase.Information_Packet_Follow_Up_Call__c;
		cC.client_Name__c = originalCase.client_Name__c;
		cC.Relationship_to_the_Insured__c = originalCase.Relationship_to_the_Insured__c;
		cC.Language_Spoken__c = originalCase.Language_Spoken__c;
		cC.Translation_Services_Required__c = originalCase.Translation_Services_Required__c;
		cC.Employee_First_Name__c = originalCase.Employee_First_Name__c;
		cC.Employee_Last_Name__c = originalCase.Employee_Last_Name__c;
		cC.Employee_DOBe__c = originalCase.Employee_DOBe__c;
		cC.Employee_Gender__c = originalCase.Employee_Gender__c;
		cC.Employee_SSN__c = originalCase.Employee_SSN__c;
		cC.Employee_Primary_Health_Plan_ID__c = originalCase.Employee_Primary_Health_Plan_ID__c;
		cC.Employee_Primary_Health_Plan_Name__c = originalCase.Employee_Primary_Health_Plan_Name__c;
		cC.Secondary_Coverage__c = originalCase.Secondary_Coverage__c;
		cC.Medicare_As_Secondary_Coverage__c = originalCase.Medicare_As_Secondary_Coverage__c;
		system.debug(originalCase.Member__c);

		if (originalCase.Member__c == null) {
			Contact objCon = new Contact();
			objCon.FirstName = originalCase.Employee_First_Name__c;
			objCon.LastName = originalCase.Employee_Last_Name__c;
			objCon.Relationship_to_the_Insured__c = originalCase.Relationship_to_the_Insured__c;
			objCon.SSN__c = originalCase.Employee_SSN__c;
			objCon.DOB__c = originalCase.Employee_DOBe__c;
			insert objCon;
			system.debug(objCon.id);
			cC.Member__c = objCon.id;
			originalCase.Member__c = objCon.id;
		} else {
			cC.Member__c = originalCase.Member__c;
		}
		cC.Patient_First_Name__c = originalCase.Patient_First_Name__c;
		cC.Patient_Last_Name__c = originalCase.Patient_Last_Name__c;
		cC.Patient_DOBe__c = originalCase.Patient_DOBe__c;
		cC.Patient_Gender__c = originalCase.Patient_Gender__c;
		cC.Patient_SSN__c = originalCase.Patient_SSN__c;
		cC.Same_as_Employee_Address__c = originalCase.Same_as_Employee_Address__c;
		cC.Employee_Street__c = originalCase.Employee_Street__c;
		cC.Employee_City__c = originalCase.Employee_City__c;
		cC.Employee_State__c = originalCase.Employee_State__c;
		cC.Employee_Zip_Postal_Code__c = originalCase.Employee_Zip_Postal_Code__c;
		cC.Employee_Country__c = originalCase.Employee_Country__c;
		cC.Patient_Street__c = originalCase.Patient_Street__c;
		cC.Patient_City__c = originalCase.Patient_City__c;
		cC.Patient_State__c = originalCase.Patient_State__c;
		cC.Patient_Zip_Postal_Code__c = originalCase.Patient_Zip_Postal_Code__c;
		cC.Patient_Country__c = originalCase.Patient_Country__c;
		cC.Employee_Home_Phone__c = originalCase.Employee_Home_Phone__c;
		cC.Employee_Mobile__c = originalCase.Employee_Mobile__c;
		cC.Employee_Work_Phone__c = originalCase.Employee_Work_Phone__c;
		cC.Employee_Preferred_Phone__c = originalCase.Employee_Preferred_Phone__c;
		cC.Employee_Best_Time_To_Contact__c = originalCase.Employee_Best_Time_To_Contact__c;
		cC.Employee_Email_Address__c = originalCase.Employee_Email_Address__c;
		cC.Patient_Home_Phone__c = originalCase.Patient_Home_Phone__c;
		cC.Patient_Mobile__c = originalCase.Patient_Mobile__c;
		cC.Patient_Work_Phone__c = originalCase.Patient_Work_Phone__c;
		cC.Patient_Preferred_Phone__c = originalCase.Patient_Preferred_Phone__c;
		cC.Patient_Best_Time_To_Contact__c = originalCase.Patient_Best_Time_To_Contact__c;
		cC.Patient_Email_Address__c = originalCase.Patient_Email_Address__c;
		cC.Carrier_Name__c = originalCase.Carrier_Name__c;
		cC.Carrier_Nurse_Name__c = originalCase.Carrier_Nurse_Name__c;
		cC.Carrier_Nurse_Phone__c = originalCase.Carrier_Nurse_Phone__c;
		cC.Carrier_E_mail__c = originalCase.Carrier_E_mail__c;
		cC.Procedure_Complexity__c = originalCase.Procedure_Complexity__c;
		cC.isRelated__c = true;
		cC.attachEncrypt__c = EncodingUtil.base64encode(
			Crypto.generateAesKey(128)
		);

		if (
			(utilities.RecordTypeName(
				originalCase.recordtypeid,
				'Patient_Case__c'
			) == relatedrecordtype) ||
			relatedrecordtype == 'HCR ManorCare' ||
			relatedrecordtype == 'Kohls'
		) {
			cC.Diagnosis__c = originalCase.Diagnosis__c;
			cC.Proposed_Procedure__c = originalCase.Proposed_Procedure__c;
			cC.Patient_Symptoms__c = originalCase.Patient_Symptoms__c;
			cC.Recent_Testing__c = originalCase.Recent_Testing__c;
			cC.Other_Pertinent_Medical_Info_History__c = originalCase.Other_Pertinent_Medical_Info_History__c;
			cC.Current_Nicotine_User__c = originalCase.Current_Nicotine_User__c;
			cC.Recent_visit_with_home_phys_prior_to_ref__c = originalCase.Recent_visit_with_home_phys_prior_to_ref__c;
			cC.Home_Phys_to_manage_care_post_program__c = originalCase.Home_Phys_to_manage_care_post_program__c;
			cC.Home_Phys_phone_access_to_program_phys__c = originalCase.Home_Phys_phone_access_to_program_phys__c;
			cC.Advise_on_potential_dental_clearance__c = originalCase.Advise_on_potential_dental_clearance__c;
			cC.Explain_trans_to_home_medical_carrier__c = originalCase.Explain_trans_to_home_medical_carrier__c;
			cC.Overview_of_clinical_components_timeline__c = originalCase.Overview_of_clinical_components_timeline__c;
			cC.Review_pre_post_op_medication_coverage__c = originalCase.Review_pre_post_op_medication_coverage__c;
			cC.Provide_options_to_obtain_waiver__c = originalCase.Provide_options_to_obtain_waiver__c;
		}
		cC.recordtypeID = [
			SELECT r.name, r.SobjectType
			FROM RecordType r
			WHERE
				r.isActive = TRUE
				AND r.name = :relatedrecordtype
				AND r.SobjectType = 'Patient_Case__c'
		]
		.id;

		cC.initial_call_discussion__c = 'Related Case created';
		cc.Status__c = 'Open';
		try {
			insert cC;
			system.debug(cC.id);
			originalCase.relatedCreated__c = true;
			originalCase.isRelated__c = true;
			update originalCase;
			list<Provider__c> lList = new List<Provider__c>(
				[
					SELECT
						City__c,
						Credentials__c,
						Email_Address__c,
						Fax_Number__c,
						First_Name__c,
						Home_Transition_Care__c,
						Last_Name__c,
						Patient_Case__c,
						Phone_Number__c,
						State__c,
						Street__c,
						Type__c,
						Zip_Postal_Code__c
					FROM Provider__c
					WHERE Patient_Case__c = :originalCase.id
				]
			);
			list<Provider__c> newPlist = new List<Provider__c>();

			for (Provider__c p : lList) {
				Provider__c tempPro = new Provider__c(
					City__c = p.City__c,
					Credentials__c = p.Credentials__c,
					Email_Address__c = p.Email_Address__c,
					Fax_Number__c = p.Fax_Number__c,
					First_Name__c = p.First_Name__c,
					Home_Transition_Care__c = p.Home_Transition_Care__c,
					Last_Name__c = p.Last_Name__c,
					Patient_Case__c = cC.id,
					Phone_Number__c = p.Phone_Number__c,
					State__c = p.State__c,
					Street__c = p.Street__c,
					Type__c = p.Type__c,
					Zip_Postal_Code__c = p.Zip_Postal_Code__c
				);

				newPlist.add(tempPro);
			}

			insert newPlist;
		} catch (exception e) {
			system.debug(e);
			return null;
		}
		Program_Notes__c pn = new Program_Notes__c();
		pn.type__c = 'Other';
		pn.subject__c = 'Case created';
		pn.Notes__c =
			'Related Case created. The prior case can be found here: https://' +
			Environment__c.getInstance().instance__c +
			'.salesforce.com/' +
			originalCase.id;
		pn.patient_case__c = cC.id;
		insert pn;
		id attachID, ftpattachID;

		for (Program_Notes__c p : [
			SELECT patient_case__c, attachID__c, ftpAttachment__c
			FROM Program_Notes__c
			WHERE
				patient_case__c = :originalCase.id
				AND isAuthEmailAttach__c = TRUE
		]) {
			attachID = p.attachID__c;
			ftpattachID = p.ftpAttachment__c;
		}

		if (attachID != null) {
			try {
				decryptAttach(originalCase.attachEncrypt__c, attachID);

				attachment oAttach = [
					SELECT body, name
					FROM attachment
					WHERE id = :attachID
				];

				attachment copyAttach = new attachment();
				copyAttach.body = oAttach.body;
				copyAttach.name = oAttach.name;
				copyAttach.parentid = cC.id;

				insert copyAttach;

				encryptAttach(cC.attachEncrypt__c, copyAttach.id);
				encryptAttach(originalCase.attachEncrypt__c, attachID);

				cC.authReady__c = true;
				update cc;

				Program_Notes__c pnattach = new Program_Notes__c(
					Patient_Case__c = cC.id,
					Subject__c = 'Auth Form Attached',
                    Notes__c = 'Auth Form Attached',
					attachID__c = attachID,
					isAuthEmailAttach__c = true,
					attachment__c = true
				);
				insert pnattach;
			} catch (exception e) {
			}
		}

		if (ftpattachID != null) {
            ftpAttachment__c oAttach = [
					SELECT attachmentID__c, name, Comments__c, fileName__c, subDirectory__c, File_Description__c, External__c 
					FROM ftpAttachment__c
					WHERE id = :ftpattachID
				];

				ftpAttachment__c copyAttach = new ftpAttachment__c();
				copyAttach.attachmentID__c = oAttach.attachmentID__c;
				copyAttach.name = oAttach.name;
                copyAttach.subDirectory__c = oAttach.subDirectory__c;
                copyAttach.Comments__c = oAttach.Comments__c;
                copyAttach.fileName__c = oAttach.fileName__c;
                copyAttach.File_Description__c = oAttach.File_Description__c;
                copyAttach.External__c = oAttach.External__c;
                copyAttach.Patient_Case__c = cC.id;

				insert copyAttach;

                cC.authReady__c = true;
				update cc;

				Program_Notes__c pnattach = new Program_Notes__c(
					Patient_Case__c = cC.id,
					Subject__c = 'Auth Form Attached',
                    Notes__c = 'Auth Form Attached',
					isAuthEmailAttach__c = true,
                    ftpAttachment__c = copyAttach.id,
					attachment__c = true
				);
				insert pnattach;
		}

		return cC;
	}

	public void decryptAttach(string eID, string attachID) {
		if (attachID == null || attachID.left(3) == 'a0M') {
			return;
		} else {
			Attachment a = [
				SELECT body
				FROM attachment
				WHERE ID = :id.valueof(attachID)
			];
			blob key = EncodingUtil.base64Decode(eID);
			encryptAttach ea = new encryptAttach(key);
			blob thebody = a.body;
			blob decryptedbody;
			decryptedbody = ea.decryptAttachmentBlob(thebody);
			a.body = decryptedbody;
			update a;
		}
	}

	public void encryptAttach(string eID, string attachID) {
		Attachment a = [SELECT body FROM attachment WHERE ID = :attachID];
		blob key;
		//EncodingUtil.base64encode(Crypto.generateAesKey(128));
		key = EncodingUtil.base64Decode(eID);

		encryptAttach ea = new encryptAttach(key);
		blob thebody = a.body;
		blob encryptedbody;
		encryptedbody = ea.encryptAttachmentBlob(thebody);
		a.body = encryptedbody;
		update a;
	}

	public id cloneIntake(id caseID) {
		patient_case__c originalCase = [
			SELECT
				attachEncrypt__c,
				Referral_Source__c,
				bid__c,
				client__c,
				Caller_Name__c,
				client_Name__c,
				Relationship_to_the_Insured__c,
				Callback_Number__c,
				Callback_Type__c,
				Language_Spoken__c,
				Translation_Services_Required__c,
				ecen_procedure__c,
				employeeId__c,
				Employee_First_Name__c,
				Employee_Last_Name__c,
				Employee_DOBe__c,
				Employee_Gender__c,
				Employee_SSN__c,
				Employee_Primary_Health_Plan_ID__c,
				Employee_Primary_Health_Plan_Name__c,
				Secondary_Coverage__c,
				Medicare_As_Secondary_Coverage__c,
				Member__c,
				Patient_First_Name__c,
				Patient_Last_Name__c,
				Patient_DOBe__c,
				Patient_Gender__c,
				Patient_SSN__c,
				Same_as_Employee_Address__c,
				Employee_Street__c,
				Employee_City__c,
				Employee_State__c,
				Employee_Zip_Postal_Code__c,
				Employee_Country__c,
				patientID__c,
				Patient_Street__c,
				Patient_City__c,
				Patient_State__c,
				Patient_Zip_Postal_Code__c,
				Patient_Country__c,
				Employee_Home_Phone__c,
				Employee_Mobile__c,
				Employee_Work_Phone__c,
				Employee_Preferred_Phone__c,
				Employee_Best_Time_To_Contact__c,
				Employee_Email_Address__c,
				Patient_Home_Phone__c,
				Patient_Mobile__c,
				Patient_Work_Phone__c,
				Patient_Preferred_Phone__c,
				Patient_Best_Time_To_Contact__c,
				Patient_Email_Address__c,
				Caregiver_Name__c,
				Caregiver_DOB__c,
				Caregiver_Home_Phone__c,
				Caregiver_Mobile__c,
				Carrier_Name__c,
				Carrier_Nurse_Name__c,
				Carrier_Nurse_Phone__c,
				Carrier_E_mail__c,
				RecordTypeid
			FROM Patient_Case__c
			WHERE id = :caseID
		];

		Patient_case__c newCase = originalCase.clone();
		newCase.Status_Reason__c = 'New';

		try {
			insert newCase;
			return newCase.id;
		} catch (exception e) {
		}

		return null;
	}
}