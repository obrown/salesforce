public with sharing class oncologyDocumentStorage{
    public string saveDocument(string recordId, string coeFileDirectory, string filename, string file_description){
        
        ftpAttachment__c ftpAttachment = new ftpAttachment__c(oncology__c=recordId);
        ftpAttachment.subDirectory__c= coeFileDirectory;
        ftpAttachment.fileName__c= fileName;
        ftpAttachment.file_Description__c= file_description;
        
        try{
            insert ftpAttachment;
        }catch(exception e){
           // isError=true;
           // message=e.getMessage();
           return null;
        }
        return ftpAttachment.id;            
    }
}