@isTest()
private class oncologyTest{
    
    
     static testMethod void AutoStatusTest(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         oncology__c oncology = t.oncology;
         
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); //pass in a new oncology record with no id
         
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         
         system.assert(oncology.id!=null);
         
         oncology.Patient_Agrees_to_Referral__c='No';
         oncology.status__c='Open';
         
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); //Test line 22 **Cancelled by Patient/'N/A
         
         oncology.Referral_Date__c=system.today();
         oncology.Date_contacted_speciality_provider_POC__c=system.today();
         oncology.Date_informed_patient_of_the_referralPOC__c=system.today();
         oncology.Transitioned_as_Closed_Plan_of_Care__c=system.today();
         oncology.Patient_Agree_to_POC__c='No';
         oncology.status__c='Open';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); //Test line 31 **Cancelled by Patient at Plan of Care/N/A
         
         oncology = t.setFormulafield(oncology, 'hasLocalPCP__c', 0);
         oncology.Referral_Date__c=null;
         oncology.Patient_Agrees_to_Referral__c='Yes';
         oncology.status__c='Open';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); //Test line40 **Awaiting Referral/Local PCP
         
         t.addPcp(oncology);
         oncology = t.setFormulafield(oncology, 'hasLocalPCP__c', 1);
         
         oncology.Referral_Date__c=null;
         oncology.Patient_Agrees_to_Referral__c='Yes';
         oncology.status__c='Open';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); //Test line 47**Awaiting Referral/Benefit Eligibility
         
         //**Awaiting Referral/Intake Complete          
         oncology = t.setFormulafield(oncology, 'hasLocalPCP__c', 1);
         oncology.Date_Eligibility_Verified__c=system.today();
         oncology.Referral_Date__c=null;
         oncology.Patient_Agrees_to_Referral__c='Yes';
         oncology.status__c='Open';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); 
         
         //**Awaiting Plan of Care/N/A
         oncology.Referral_Date__c=datetime.now();
         oncology.Eligibility_Verification__c ='Yes';
         oncology.Patient_Agrees_to_Referral__c='Yes';
         oncology.status__c='Open';
         oncology.Determination_Outcome__c='Program Eligibility Met';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); 
         
         //**Awaiting Acceptance of Plan of Care/N/A
         oncology.Facility_Plan_of_Care_Submission_Date__c =date.today();
         oncology.Determination_Outcome__c = 'Program Eligibility Met';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); 
         
         //**Awaiting Travel / InPerson Eval Confirmation/N/A
         oncology.Facility_Plan_of_Care_Submission_Date__c =date.today();
         oncology.Evaluation_Travel_Type__c=null;
         oncology.Determination_Outcome__c = 'Program Eligibility Met';
         oncology.Proposed_Service_Type__c='In-Person Evaluation';
         oncology.Plan_of_Care_Accepted_Date__c=date.today();
         oncology.Patient_Agree_to_POC__c='Yes';
         oncology.Patient_FAT_Hours__c ='2';
         oncology.Evaluation_Actual_Departure__c=null;
         oncology.Evaluation_Actual_Arrival__c = null;
         oncology.Evaluation_Estimated_Departure__c = null;
         oncology.Referral_Date__c=date.today();
         oncology.Date_HDP_Forms_Completed__c=date.today();
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); 
         system.debug(oncology.status_reason__c);
         system.assert(oncology.status_reason__c=='Awaiting Travel / InPerson Eval Confirmation');
         
         //**Ready for Virtual Eval/N/A
         oncology.Facility_Plan_of_Care_Submission_Date__c =date.today();
         oncology.Determination_Outcome__c = 'Program Eligibility Met';
         oncology.Proposed_Service_Type__c='Virtual Evaluation';
         oncology.Evaluation_Date_1__c=date.today();
         oncology.Patient_Agree_to_POC__c='Yes';
         oncology.Patient_FAT_Hours__c ='2';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); 
         
         //**Awaiting Virtual Evaluation Confirmation/N/A
         oncology.Facility_Plan_of_Care_Submission_Date__c =date.today();
         oncology.Proposed_Service_Type__c='Virtual Evaluation';
         oncology.Evaluation_Date_1__c=date.today();
         oncology.Patient_Agree_to_POC__c='';
         oncology.Patient_FAT_Hours__c ='2';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); 
         
         //Closed statuses
         
         //**Program Eligibility NOT Met - Clinical/Retinoblastoma
         oncology.Proposed_Service_Type__c=null;
         oncology.Determination_Outcome__c='Program Eligibility NOT Met - Clinical';
         oncology.Determination_Outcome_Reason__c = 'Clinical - Retinoblastoma';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); 
     
         //**Program Eligibility NOT Met - Clinical/Ocular
         oncology.Determination_Outcome__c='Program Eligibility NOT Met - Clinical';
         oncology.Determination_Outcome_Reason__c = 'Clinical - Ocular';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); 
         //system.assert(oncology.sub_status_reason__c=='Ocular');
          
         //**Program Eligibility NOT Met - Clinical/Base of skull tumors
         oncology.Determination_Outcome__c='Program Eligibility NOT Met - Clinical';
         oncology.Determination_Outcome_Reason__c = 'Clinical – Base of the skull tumors';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology});
         //system.assert(oncology.sub_status_reason__c=='Base of the skull tumors');
         
         //**Program Eligibility NOT Met - Clinical/Intracardiac
         oncology.Determination_Outcome__c='Program Eligibility NOT Met - Clinical';
         oncology.Determination_Outcome_Reason__c = 'Clinical - Intracardiac';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology});
         //system.assert(oncology.sub_status_reason__c=='Intracardiac');
         
         //**Program Eligibility NOT Met - NonClinical/Age less than 2 years
         oncology.Determination_Outcome__c='Program Eligibility NOT Met - NonClinical';
         oncology.Determination_Outcome_Reason__c = 'Nonclinical - Age less than 2 years';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology});
         //system.assert(oncology.sub_status_reason__c=='Age less than 2 years');
         
         //**Program Eligibility NOT Met - NonClinical/No local physician
         oncology.Determination_Outcome__c='Program Eligibility NOT Met - NonClinical';
         oncology.Determination_Outcome_Reason__c = 'Nonclinical - No local physician';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology});
         //system.assert(oncology.sub_status_reason__c=='No local physician');
         
         //**Program Eligibility NOT Met - NonClinical/No local physician visit
         oncology.Determination_Outcome__c='Program Eligibility NOT Met - NonClinical';
         oncology.Determination_Outcome_Reason__c = 'Nonclinical - No local physician visit';
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology});
         //system.assert(oncology.sub_status_reason__c=='No local physician visit');
         
         //**Cancelled by Patient at Plan of Care/NA
         oncology.status__c='Open';
         oncology.Determination_Outcome__c=null;
         oncology.Patient_Agree_to_POC__c='No';
         oncology.Date_contacted_speciality_provider_POC__c=date.today();
         oncology.Date_informed_patient_of_the_referralPOC__c=date.today();
         oncology.Transitioned_as_Closed_Plan_of_Care__c=date.today();
         oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology});
         
     }

     static testMethod void AppointmentTest(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncologyAppointment appointment = new oncologyAppointment(oncology.id);
         appointment.setparentId(oncology.id);
         
         appointment.newAppointment();
         appointment.appointment.Actual_Start__c = datetime.now();
         appointment.appointment.Actual_End__c= datetime.now().adddays(3);
         appointment.appointment.Subject__c= 'Unit.Test';
         appointment.appointment.parentid__c=null;
         appointment.saveAppointment();
         system.assert(appointment.appointment.id==null); //confirm the save failed...there are no validation rules....but we removed the parentid
         
         appointment.appointment.parentid__c=oncology.id;
         appointment.saveAppointment();
         system.assert(appointment.appointment.id!=null); //confirm the save succeeded..
         
         appointment.cancelAppointment();
         system.assert(appointment.appointment==null); //confirm the cancel succeeded..
         
         ApexPages.CurrentPage().getParameters().put('apptId', appointment.appointments[0].id);
         appointment.openAppointment();
         system.assert(appointment.appointment.id!=null); //confirm the open succeeded..
         
         ApexPages.CurrentPage().getParameters().put('apptId', appointment.appointment.id);
         appointment.deleteAppointment();
         system.assert(appointment.appointment.id!=null); //confirm the open succeeded..
         
         
     }  
     
     static testMethod void AllowanceTest(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncologyAllowance allowance = new oncologyAllowance(oncology.id);
         allowance.setparentId(oncology.id);
         
         allowance.newallowance();
         system.assert(allowance.allowance.id==null); //confirm the save failed...there are no validation rules....but we removed the parentid
         
         allowance.allowance.oncology__c=oncology.id;
         allowance.saveallowance();
         system.assert(allowance.allowance.id!=null); //confirm the save succeeded..
         
         allowance.cancelallowance();
         system.assert(allowance.allowance==null); //confirm the cancel succeeded..
         
         ApexPages.CurrentPage().getParameters().put('allowanceId', allowance.allowances[0].id);
         allowance.openallowance();
         system.assert(allowance.allowance.id!=null); //confirm the open succeeded..
         
         ApexPages.CurrentPage().getParameters().put('allowanceId', allowance.allowance.id);
         allowance.deleteallowance();
         system.assert(allowance.allowance.id!=null); //confirm the open succeeded..
         
     }  
     
     static testMethod void RequiredToReferTest(){
         
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncology.Patient_Agrees_to_Referral__c='No';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_Agrees_to_Referral__c='';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_Agrees_to_Referral__c='Yes';
         
         oncology.procedure__c=null;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.procedure__c=t.oncology.procedure__c;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Referral_Source__c='unit.test';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Date_Eligibility_Verified__c = date.today();
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_First_Name__c='';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_Last_Name__c='';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_First_Name__c='Unit';
         oncology.Patient_Last_Name__c='Test';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_DOB__c=null;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         date pdob = date.today().addYears(-30);
         oncology.Patient_DOB__c = pdob.year()+'-'+pdob.month()+'-'+pdob.day();
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Gender__c=null;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Gender__c='Male';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_Medical_Plan_Number__c = '';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_Medical_Plan_Number__c = 'TXN1234567';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Carrier__c = null;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         carrier__c carrier= t.addCarrier();
         
         oncology.Carrier__c = carrier.id;
         oncology.Plan_Type__c = null;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Plan_Type__c = 'Unit Test';
         oncology.COBRA_Status__c = null;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.COBRA_Status__c = 'Yes';
         oncology.COBRA_Paid_Through_Date__c= null;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.COBRA_Paid_Through_Date__c = date.today().addMonths(6);
         oncology.LOA_Status__c =  '';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.LOA_Status__c =  'Yes';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.LOA_Paid_Through_Date__c = date.today();
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_Address__c=null;
         oncology.Patient_City__c=null;
         oncology.Patient_State__c=null;
         oncology.Patient_Zip_Code__c=null;
         oncology.Patient_Country__c=null;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Patient_Address__c='Unit test';
         oncology.Patient_City__c='Unit';
         oncology.Patient_State__c='AZ';
         oncology.Patient_Zip_Code__c='85205';
         oncology.Patient_Country__c='US';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Preferred_Language__c=null;
         oncology.Translation_Services_Required__c='';
         oncology.Relationship_to_Subscriber__c='';
         oncology.Delegate_Status__c = '';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Employee_First_Name__c='';
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology.Employee_First_Name__c=null;
         oncology.Employee_Last_Name__c=null;
         oncology.Employee_DOB__c= null;
         oncology.Employee_Gender__c = null;
         oncologyRequiredToRefer.checkRequirements(oncology);
         
         oncology = new oncology__c();
         oncologyRequiredToRefer.checkRequirements(oncology);
         
     }

     static testMethod void caseNoteTest(){
         
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncologyCaseNote caseNote = new oncologyCaseNote(oncology.id);
         caseNote.newCaseNote();
         
         caseNote.caseNote.Initiated__c ='Inbound';
         caseNote.caseNote.Communication_Type__c='Note';
         caseNote.caseNote.Subject__c='Other';
         caseNote.caseNote.Contact_Type__c='Email';
         caseNote.caseNote.Body__c='Email';
         
         caseNote.saveNote();
         
         ApexPages.CurrentPage().getParameters().put('casenoteid', caseNote.caseNotes[0].id);
         
         caseNote.getNoteVF(); 
             
     }
     
     static testMethod void noteTest(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncologyNote oNote = new oncologyNote(oncology.id);
         oNote.newNote();
         oNote.note.parentid__c=null;
         oNote.savenote();
         
         //'Intake Status Note'
         oNote.newNote();
         oNote.note.Activity_Type__c  = 'Intake Status Note';
         oNote.note.Subject__c = 'unit.test';
         oNote.savenote();
         
         ApexPages.CurrentPage().getParameters().put('notesId', oNote.inotes[0].id);
         ApexPages.CurrentPage().getParameters().put('notesType', 'iNote');
         oNote.openNote();
         
         //'Referral Status Note'
         oNote.newNote();
         oNote.note.Activity_Type__c  = 'Referral Status Note';
         oNote.note.Subject__c = 'unit.test';
         oNote.savenote();
         
         ApexPages.CurrentPage().getParameters().put('notesId', oNote.rnotes[0].id);
         ApexPages.CurrentPage().getParameters().put('notesType', 'rNote');
         oNote.openNote();
         
         //'Program Determination Activity Note'
         oNote.newNote();
         oNote.note.Activity_Type__c  = 'Program Determination Activity Note';
         oNote.note.Subject__c = 'unit.test';
         oNote.savenote();
         
         ApexPages.CurrentPage().getParameters().put('notesId', oNote.pdNotes[0].id);
         ApexPages.CurrentPage().getParameters().put('notesType', 'pdNote');
         oNote.openNote();

         //'Plan of Care Status Notes - Open'
         oNote.newNote();
         oNote.note.Activity_Type__c  = 'Plan of Care Status Notes - Open';
         oNote.note.Subject__c = 'unit.test';
         oNote.savenote();
         
         ApexPages.CurrentPage().getParameters().put('notesId', oNote.pocoNotes[0].id);
         ApexPages.CurrentPage().getParameters().put('notesType', 'pocoNote');
         oNote.openNote();         

         //'Plan of Care Status Notes - Closed'
         oNote.newNote();
         oNote.note.Activity_Type__c  = 'Plan of Care Status Notes - Closed';
         oNote.note.Subject__c = 'unit.test';
         oNote.savenote();
         
         ApexPages.CurrentPage().getParameters().put('notesId', oNote.poccNotes[0].id);
         ApexPages.CurrentPage().getParameters().put('notesType', 'poccNote');
         oNote.openNote();         

         //'Medical Record Collection Status Note'
         oNote.newNote();
         oNote.note.Activity_Type__c  = 'Medical Record Collection Status Note';
         oNote.note.Subject__c = 'unit.test';
         oNote.savenote();
         
         ApexPages.CurrentPage().getParameters().put('notesId', oNote.mrNotes[0].id);
         ApexPages.CurrentPage().getParameters().put('notesType', 'mrNote');
         oNote.openNote();         

         //'One Year Support Status Notes'
         oNote.newNote();
         oNote.note.Activity_Type__c  = 'One Year Support Status Notes';
         oNote.note.Subject__c = 'unit.test';
         oNote.savenote();
         
         ApexPages.CurrentPage().getParameters().put('notesId', oNote.oyssNotes[0].id);
         ApexPages.CurrentPage().getParameters().put('notesType', 'oyssNote');
         oNote.openNote();  
         
         //'One Year Support End'
         oNote.newNote();
         oNote.note.Activity_Type__c  = 'One Year Support End';
         oNote.note.Subject__c = 'unit.test';
         oNote.savenote();
         
         ApexPages.CurrentPage().getParameters().put('notesId', oNote.oyseNotes[0].id);
         ApexPages.CurrentPage().getParameters().put('notesType', 'oyseNote');
         oNote.openNote();   

         //'Clinical Facility Note'
         oNote.newNote();
         oNote.note.Activity_Type__c  = 'Clinical Facility Note';
         oNote.note.Subject__c = 'unit.test';
         oNote.savenote();
         
         ApexPages.CurrentPage().getParameters().put('notesId', oNote.cNotes[0].id);
         ApexPages.CurrentPage().getParameters().put('notesType', 'cNote');
         onote.openNote();
         oNote.cancelinote();                      
         
     } 
     
     
     static testMethod void manualReferralExtTest(){
         
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         t.addPcp(oncology);
         
         ApexPages.StandardController controller = new ApexPages.StandardController(oncology);
         oncologyManualReferralExt referralExt = new oncologyManualReferralExt(controller);
         
     }
     
     static testMethod void searchTest(){
         
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         string n = [select name from Oncology__c where id = :oncology.id].name;
         
         ApexPages.CurrentPage().getParameters().put('sf', 'caseNumber');
         ApexPages.CurrentPage().getParameters().put('st', n);
         oncologySearchController   osc = new oncologySearchController();
         
         ApexPages.CurrentPage().getParameters().put('sf', 'phone');
         ApexPages.CurrentPage().getParameters().put('st', oncology.Patient_Phone__c);
         osc = new oncologySearchController();
         
         ApexPages.CurrentPage().getParameters().put('sf', '');
         ApexPages.CurrentPage().getParameters().put('st', oncology.Patient_Last_Name__c);
         osc = new oncologySearchController();
         
         osc.sf= 'caseNumber';
         osc.st= n;
         osc.searchVF();
         
         osc.sf= null;
         osc.st= null;
         osc.searchVF();
     }
     
     static testMethod void needsAssessmentTest(){
     
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncologyNeedsAssessment need= new oncologyNeedsAssessment(oncology.id);
         need.setparentId(oncology.id);
         
         need.need.Oncology__c =null;
         need.saveNeed();
         system.assert(need.need.id==null); //confirm the save failed...there are no validation rules....but we removed the parentid
         
         need.need.Oncology__c =oncology.id;
         need.saveNeed();
         system.assert(need.need.id!=null); //confirm the save succeeded..
         
     }    
     
     static testMethod void workflowTest(){
         
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncology.Facility_Plan_of_Care_Submission_Date__c = date.today();
         oncology.Proposed_Service_Reason__c ='';
         oncology.Proposed_Service_Type__c='';
         oncology.Consultation_Types__c='';
         oncology.Facility_Managing_Physician__c='';
         oncology.Plan_of_Care_Accepted_Date__c = date.today();
         oncology.POC_HDP_Medical_Director_Review_Date__c = date.today();
         oncology.POC_HDP_Medical_Review_Determination__c='';
         oncology.Evaluation_Date_1__c = date.today();
         oncology.Patient_FAT_Hours__c='';
         oncology.PatientFAT_Minutes__c='';
         oncology.PatientFAT_AMPM__c='PM';
         oncology.Patient_FAT_Timezone__c='';
         oncology.patient_LAT_Hours__c=''; 
         oncology.PatientLAT_Minutes__c='';
         oncology.PatientLAT_AMPM__c='PM';
         oncology.Patient_LAT_Timezone__c='';
         oncology.Last_Appointment_Date__c = date.today();
         oncology.Estimated_Arrival_Date__c = date.today();
         oncology.Estimated_Departure_Date__c = date.today();
         oncology.Recommended_Mode_of_Transportation__c='';
         oncology.Allowable_Mode_of_Transportation__c='';
         oncology.Date_HDP_Forms_Completed__c= date.today();
         oncology.HDP_Program_Forms_Completed__c=true;
         oncology.Patient_Agree_to_POC__c='Yes';
         oncology.Addl_diagnostics_outside_bundle_None__c=true;
         
         set<oncology__c> oSet = new set<oncology__c>();
         oSet.add(oncology);
         oncologyWorkflow.selfSubcriber(oSet);
         oncologyWorkflow.sameAddress(oSet);
         oncologyWorkflow.pocAuth(oSet);
     }
 
     static testMethod void changeHealthEligibilityTest(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         ApexPages.StandardController controller = new ApexPages.StandardController(oncology);
         oncologyExtension oExt= new oncologyExtension(controller);
         
         oExt.submitForEligibility();
         
     }   
     
     static testMethod void requestIDCard(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncologyWorkflow.requestIDCard(oncology);
         
     }     

     static testMethod void requestEmailReferral(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncology.client_facility__c = t.addClientFacility(oncology.client__c, oncology.procedure__c, t.facility.id).id;
         
         oncology.Patient_Agrees_to_Referral__c='Yes';
        // oncology.procedure__c=t.oncology.procedure__c;
         oncology.Date_Eligibility_Verified__c = date.today();
         oncology.Eligibility_Verification__c= 'Yes';
         oncology.Patient_First_Name__c='Unit';
         oncology.Patient_Last_Name__c='Test';
         date pdob = date.today().addYears(-30);
         oncology.Patient_DOB__c = pdob.year()+'-'+pdob.month()+'-'+pdob.day();
         oncology.Gender__c='Male';
         oncology.Patient_Medical_Plan_Number__c = 'TXN1234567';
         carrier__c carrier= t.addCarrier();
         oncology.Carrier__c = carrier.id;
         oncology.Plan_Type__c = 'Unit Test';
         oncology.COBRA_Status__c = 'Yes';
         oncology.COBRA_Paid_Through_Date__c = date.today().addMonths(6);
         oncology.LOA_Status__c =  'Yes';
         oncology.LOA_Paid_Through_Date__c = date.today();
         oncology.Patient_Address__c='Unit test';
         oncology.Patient_City__c='Unit';
         oncology.Patient_State__c='AZ';
         oncology.Patient_Zip_Code__c='85205';
         oncology.Patient_Country__c='US';         
         oncology.Patient_Preferred_Phone__c = 'Mobile';
         oncology.Preferred_Language__c = 'English';
         oncology.Translation_Services_Required__c = 'No';
         oncology.Patient_Advocacy__c ='Unit.Test';
         oncology.Accolade_Nurse_Name__c='Unit.Test';
         oncology.Accolade_Nurse_Number__c ='(480) 555-1212';
         oncology.Advocacy_Nurse_Email__c='unit@test.com';
         
         oncology.Facility_Nurse_Name__c='Unit.Test';
         oncology.Facility_Nurse_Phone_Number__c='(480) 555-1212';
         oncology.Facility_Nurse_Email__c='unit@test.com';
         
         oncology.Client_HR__c='Unit.Test';
         
         oncology.HR_Name__c='Unit.Test';
         oncology.HR_Phone_Number__c='(480) 555-1212';
         oncology.HR_Email__c='unit@test.com';
         
         oncology.HDP_Nurse_Phone_Number__c='(480) 555-1212';
         oncology.HDP_Nurse_Email__c='unit@test.com';
         oncology.HDP_Nurse__c =userinfo.getuserid();
         oncology.Carrier_Nurse_Phone_Number__c='(480) 555-1212';
         oncology.Carrier_Nurse_Email__c='unit@test.com';
         oncology.Carrier_Nurse__c='Unit.Test';
         oncology.Carrier_UM_Nurse__c='Unit.Test';
         
         t.addPcp(oncology);
         oncology = t.setFormulafield(oncology, 'hasLocalPCP__c', 1);
         oncologyWorkflow.requestEmailReferral(oncology);
         
         
         
     }
     
     static testMethod void receiveRecordsFromCoHTest(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncology.client_facility__c = t.addClientFacility(oncology.client__c, oncology.procedure__c, t.facility.id).id;
         
         oncology.Patient_Agrees_to_Referral__c='Yes';
        // oncology.procedure__c=t.oncology.procedure__c;
         oncology.Date_Eligibility_Verified__c = date.today();
         oncology.Eligibility_Verification__c= 'Yes';
         oncology.Patient_First_Name__c='Unit';
         oncology.Patient_Last_Name__c='Test';
         date pdob = date.today().addYears(-30);
         oncology.Patient_DOB__c = pdob.year()+'-'+pdob.month()+'-'+pdob.day();
         oncology.Gender__c='Male';
         oncology.Patient_Medical_Plan_Number__c = 'TXN1234567';
         carrier__c carrier= t.addCarrier();
         oncology.Carrier__c = carrier.id;
         oncology.Plan_Type__c = 'Unit Test';
         oncology.COBRA_Status__c = 'Yes';
         oncology.COBRA_Paid_Through_Date__c = date.today().addMonths(6);
         oncology.LOA_Status__c =  'Yes';
         oncology.LOA_Paid_Through_Date__c = date.today();
         oncology.Patient_Address__c='Unit test';
         oncology.Patient_City__c='Unit';
         oncology.Patient_State__c='AZ';
         oncology.Patient_Zip_Code__c='85205';
         oncology.Patient_Country__c='US';         
         oncology.Patient_Preferred_Phone__c = 'Mobile';
         oncology.Preferred_Language__c = 'English';
         oncology.Translation_Services_Required__c = 'No';
         oncology.Patient_Advocacy__c ='Unit.Test';
         oncology.Accolade_Nurse_Name__c='Unit.Test';
         oncology.Accolade_Nurse_Number__c ='(480) 555-1212';
         oncology.Advocacy_Nurse_Email__c='unit@test.com';
         
         oncology.Facility_Nurse_Name__c='Unit.Test';
         oncology.Facility_Nurse_Phone_Number__c='(480) 555-1212';
         oncology.Facility_Nurse_Email__c='unit@test.com';
         
         oncology.Client_HR__c='Unit.Test';
         
         oncology.HR_Name__c='Unit.Test';
         oncology.HR_Phone_Number__c='(480) 555-1212';
         oncology.HR_Email__c='unit@test.com';
         
         oncology.HDP_Nurse_Phone_Number__c='(480) 555-1212';
         oncology.HDP_Nurse_Email__c='unit@test.com';
         oncology.HDP_Nurse__c =userinfo.getuserid();
         oncology.Carrier_Nurse_Phone_Number__c='(480) 555-1212';
         oncology.Carrier_Nurse_Email__c='unit@test.com';
         oncology.Carrier_Nurse__c='Unit.Test';
         oncology.Carrier_UM_Nurse__c='Unit.Test';
         
         t.addPcp(oncology);
         oncology = t.setFormulafield(oncology, 'hasLocalPCP__c', 1);
         oncologyWorkflow.requestEmailReferral(oncology);
         
         Oncology_Clinical_Facility__c ocf = new Oncology_Clinical_Facility__c(parentid__c=oncology.id);
         ocf.CoH_Primary__c='test';
         ocf.Plan_of_Care_Submission_Date__c=date.today();
         ocf.Consultation_Types__c='Test';
         ocf.Proposed_Service_Type__c='Test';
         ocf.External_reference_id__c='Test';
         ocf.Evaluation_Date_1__c=date.today();
         ocf.Evaluation_Date_2__c=date.today();
         ocf.First_AppointmentTimeOn_First_Evaluation__c='2019-08-02 12:00:00';
         ocf.Last_Appointment_Date_and_Time__c='2019-08-04 2:00:00';
         ocf.Additional_diagnostics_outside_bundle__c='Outpatient Psychiatric;Diagnostic Radiology Review;Additional Pathology Review;All other necessary evaluation and diagnostics not included in the In-Person Evaluation – Outpatient Services Bundle';
         
         ocf.Determination_Date__c=date.today();
         ocf.Determination_Outcome__c='test';
         ocf.Determination_Outcome_Reason__c='test';
         ocf.Treatment__c ='test';
         ocf.Clinical_Notes__c = 'Test';
         ocf.Date_of_Diagnosis__c = date.today();
         ocf.Primary_Diagnosis__c= 'Test';
         ocf.Physician_First_Name_Last_Name__c='John Smith';
         ocf.Physician_Specialty__c='Test';
         ocf.Physician_Credentials__c='MD';
         ocf.Physician_Phone_Number__c='(480) 555-1212';
         ocf.Physician_Fax_Number__c='(480) 555-1212';
         ocf.Physician_Street_Address__c='123 E Main st';
         ocf.Physician_City__c='Hudson';
         ocf.Physician_State__c='Arizona';
         ocf.Physician_Zip_Code__c='99999';
         ocf.Associated_Facilities__c= 'Test';
         
         map<id, Oncology_Clinical_Facility__c> planOfCareMap = new map<id, Oncology_Clinical_Facility__c>();
         map<id, Oncology_Clinical_Facility__c> determinationMap = new map<id, Oncology_Clinical_Facility__c>();
         planOfCareMap.put(oncology.id, ocf);
         determinationMap.put(oncology.id, ocf);
         
         oncologyWorkflow.receiveRecordsFromCoH(determinationMap, planOfCareMap, true);
         
     }
     
     static testMethod void requestAuthorizationNumber(){
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         

         
         oncologyWorkflow.requestAuthorizationNumber(oncology);
         
     }      
       
}