public without sharing class coeMarkMessageRead {
    final string recordId;
    public coeMarkMessageRead(ApexPages.StandardController controller) {
        recordid=controller.getid();
    }


    public PageReference redirector() {
        
        ECEN_Message__c message = new ECEN_Message__c(id=recordId);
        message.read__c=true;
        update message;
        
        return null;
    }

}