public class hpClaimSearchStruct{
        
   public string ResultCode {get; private set;}
   public string ErrorMsg {get; private set;}
    
   public string Relationship {get; private set;}
   public string HPSsnSeq {get; private set;}
   public string ClaimNumber {get; private set;}
   public string Status {get; private set;}
   
   public EligibilityInfo[] EligibilityInfo {get; private set;}
   
   public class EligibilityInfo{
            public string Underwriter {get; private set;}
            public string Grp {get; private set;}
            public string LastName {get; private set;}
            public string FirstName {get; private set;}
            public string DateOfBirth{get; private set;}
            public string Essn {get; private set;}
            public string Seq {get; private set;}
            
   }
   
   public void setEligibilityInfo(EligibilityInfo[] EligibilityInfo){
       this.EligibilityInfo = EligibilityInfo;
   
   }
   
   public void setResultCode(string ResultCode){
       this.ResultCode = ResultCode;
   
   }
   
   public void setErrorMsg(string ErrorMsg){
       this.ErrorMsg= ErrorMsg;
   
   }
   
   public void setClaimNumber(string ClaimNumber){
       this.ClaimNumber = ClaimNumber;
   
   }
}