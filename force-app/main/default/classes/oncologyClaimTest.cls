@isTest()
private class oncologyClaimTest{
    
      static testMethod void ClaimTest(){
         
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         oncology.client_facility__c = t.addClientFacility(oncology.client__c, oncology.procedure__c, t.facility.id).id;
         
         oncology.Patient_Agrees_to_Referral__c='Yes';
        // oncology.procedure__c=t.oncology.procedure__c;
         oncology.Date_Eligibility_Verified__c = date.today();
         oncology.Patient_First_Name__c='Unit';
         oncology.Patient_Last_Name__c='Test';
         date pdob = date.today().addYears(-30);
         oncology.Patient_DOB__c = pdob.year()+'-'+pdob.month()+'-'+pdob.day();
         oncology.Gender__c='Male';
         oncology.Patient_Medical_Plan_Number__c = 'TXN1234567';
         carrier__c carrier= t.addCarrier();
         oncology.Carrier__c = carrier.id;
         oncology.Plan_Type__c = 'Unit Test';
         oncology.COBRA_Status__c = 'Yes';
         oncology.COBRA_Paid_Through_Date__c = date.today().addMonths(6);
         oncology.LOA_Status__c =  'Yes';
         oncology.LOA_Paid_Through_Date__c = date.today();
         oncology.Patient_Address__c='Unit test';
         oncology.Patient_City__c='Unit';
         oncology.Patient_State__c='AZ';
         oncology.Patient_Zip_Code__c='85205';
         oncology.Patient_Country__c='US';         
         oncology.Patient_Preferred_Phone__c = 'Mobile';
         oncology.Preferred_Language__c = 'English';
         oncology.Translation_Services_Required__c = 'No';
         oncology.Patient_Advocacy__c ='Unit.Test';
         oncology.Accolade_Nurse_Name__c='Unit.Test';
         oncology.Accolade_Nurse_Number__c ='(480) 555-1212';
         oncology.Advocacy_Nurse_Email__c='unit@test.com';
         
         oncology.Facility_Nurse_Name__c='Unit.Test';
         oncology.Facility_Nurse_Phone_Number__c='(480) 555-1212';
         oncology.Facility_Nurse_Email__c='unit@test.com';
         
         oncology.Client_HR__c='Unit.Test';
         
         oncology.HR_Name__c='Unit.Test';
         oncology.HR_Phone_Number__c='(480) 555-1212';
         oncology.HR_Email__c='unit@test.com';
         
         oncology.HDP_Nurse_Phone_Number__c='(480) 555-1212';
         oncology.HDP_Nurse_Email__c='unit@test.com';
         oncology.HDP_Nurse__c =userinfo.getuserid();
         oncology.Carrier_Nurse_Phone_Number__c='(480) 555-1212';
         oncology.Carrier_Nurse_Email__c='unit@test.com';
         oncology.Carrier_Nurse__c='Unit.Test';
         oncology.Carrier_UM_Nurse__c='Unit.Test';
         
         t.addPcp(oncology);
         oncology = t.setFormulafield(oncology, 'hasLocalPCP__c', 1);
         
         oncologyClaim oClaim = new oncologyClaim(oncology.id);
         
         oClaim.newclaim();
         oClaim.claim .Claim_number__c = '11111111111';
         oclaim.saveClaim();
         ApexPages.CurrentPage().getParameters().put('cid', oClaim.claims[0].id);
         oClaim.openClaim();
         
         oClaim.getcTypeItems();
         oClaim.refreshCodeList();
         oClaim.getcodeOptions();
         oClaim.codeType = 'DRG';
         
         Clinical_Codes__c fooCC = oClaim.theCCPOC;
         
         
      }
      
}