public with sharing class cmUnabletoReach{
    
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=cm.patient__r.Patient_First_Name__c+' '+cm.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Address__c: +'<br/>';
        letterText+='<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.City__c+', ' : ''+'&nbsp';
        letterText+=hasPatientAddress ? cm.patient__r.State__c+' ' : ''+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Zip__c : ''+'<br/>';
        letterText+='<br/>';

        letterText+='</div>';
        letterText+='<p>';
        letterText+='Dear&nbsp;'+cm.patient__r.Patient_First_Name__c+'&nbsp;'+cm.patient__r.Patient_Last_Name__c+',';
        letterText+='</p>';
        letterText+='<p>';  
        letterText+='Thank you for talking with me about your health status. I am sorry I have been unable to reach you by phone to see how you are doing. I understand this ';
        letterText+=' may be a busy time for you.<br/>';
        letterText+='</p>';
 
        letterText+='<p>';  
        letterText+='Please feel free to call me at 1-877-891-2690, Monday-Friday from 8:30AM – 5:00PM  Eastern Time. If I am not able to take your call,';
        letterText+= ' please leave a message on my confidential voicemail and I will return your call as soon as possible.<br/>';
        letterText+='</p>';         

        letterText+='I encourage you to continue to see your physician as necessary and I look forward to hearing <br/>';
        letterText+='from you.<br/><br/>';
        
        letterText+='Sincerely,<br/><br/>';
        letterText+='Care Management Department<br/>';
        letterText+='Contigo Health<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';

        return letterText;
    }
    
}