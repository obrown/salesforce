@isTest()
private class coeTaskListControllerTest{

    private static testMethod void testController(){
        coeTestData testData = new coeTestData();
        id clientId = testData.createClient(null);
        id procedureId = testData.createProcedure(null);
        id facilityId = testData.createFacility(null);
        id clientFacilityId = testData.createClientFacility(clientId, facilityId, procedureId );
        
        patient_case__c pc = new patient_case__c();
        pc.client__c = clientID;
        pc.ecen_procedure__c = procedureId;
        pc.client_Facility__c = clientFacilityId;
        
        pc.BID__c = '123456';
        pc.Language_Spoken__c = 'English';
        pc.Initial_Call_Discussion__c = 'The ICD';
        pc.Relationship_to_the_Insured__c = 'Spouse';
        pc.Caller_relationship_to_the_patient__c='Spouse';
        pc.caller_name__c='John Doe';
        
        pc.Employee_first_name__c = 'John';
        pc.Employee_last_name__c = 'Smith';
        pc.employee_dob__c = date.valueof('1980-01-01');
        pc.Employee_Gender__c = 'Male';
        pc.employee_ssn__c = '123456789';
        pc.employee_street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85297';
        pc.Employee_Country__c = 'USA';
        
        pc.Patient_First_Name__c = 'Jane';
        pc.Patient_last_name__c = 'Smith';
        pc.Patient_dob__c = date.valueof('1981-01-01');
        pc.Patient_Gender__c = 'Female';
        pc.Patient_ssn__c = '987654321';
        pc.Patient_Email_Address__c = 'none';
        pc.Patient_street__c = '123 E Main St';
        pc.Patient_City__c = 'Mesa';
        pc.Patient_State__c = 'AZ';
        pc.Patient_Zip_Postal_Code__c = '85297';
        pc.Patient_Country__c = 'USA';
        
        pc.Same_as_Employee_Address__c = true;
        
        
        pc.Employee_Home_Phone__c = '(480) 555-1212';
        pc.Employee_Mobile__c = '(480) 555-1212';
        pc.Employee_Work_Phone__c = '(480) 555-1212';
        
        pc.Patient_Home_Phone__c = '(480) 555-1212';
        pc.Patient_Mobile__c = '(480) 555-1212';
        pc.Patient_Work_Phone__c = '(480) 555-1212';
        
        pc.CallBack_Number__c = '(480) 555-1212';
        pc.Caregiver_DOBe__c = '1982-01-01';
        
        pc.Listed_as_Program_Covered_Procedure__c = 'Yes';
        pc.What_procedure_was_recommended__c = 'Testing';
        pc.Carrier_Name__c = 'Aetna';
        pc.attachEncrypt__c = EncodingUtil.base64encode(Crypto.generateAesKey(128));
        
        insert pc;
        
        pc = [select client_facility__r.Client__r.name, client_facility__r.Procedure__r.name from patient_case__c where id = :pc.id];
        
        Case_Task__c caseTask = new Case_Task__c();
        caseTask.Program__c ='Walmart Joint';
        caseTask.Name = 'TaskA';
        caseTask.Status_Options__c='Completed,Open,In Progress';
        caseTask.field__c = 'Actual_Departure__c';
        insert caseTask ;
        
        ApexPages.CurrentPage().getParameters().put('id',pc.id);
        
        coeTaskListController tlc = new coeTaskListController();
        tlc.set_case_id();
        tlc.load_Tasks();
        tlc.load_Tasks(); //run this twice
        
        tlc.saveTask();
        tlc.save();
        tlc.clearError();
        
        
    }

}