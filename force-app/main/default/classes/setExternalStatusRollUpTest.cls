@isTest
private class setExternalStatusRollUpTest {

    static testMethod void unitTest() {
        
        //After Referral
        setExternalStatusRollUp.theStatus('Open', 'Awaiting Forms', 'subList', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', 'Claim Paid', 'subList', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', 'Claim Received', 'subList', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', 'Patient at COE', 'subList', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', 'Appointment Date', 'subList', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', 'No Eligibility', 'subList', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', '', 'No Eligibility', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', '', 'Patient Expired', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', 'Medical Records', '', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', 'Awaiting Plan of Care', '', 'cgName', true, null);
        setExternalStatusRollUp.theStatus('Open', 'No Home Physician', '', 'cgName', true, null);
    
        //Before Referral
        setExternalStatusRollUp.theStatus('Open', 'New', 'subList', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', 'Call Three', 'subList', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', 'No Home Physician', 'Diagnostics', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', 'Invalid Information', '', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', '', 'Incomplete', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', 'Not Eligible - Procedure', '', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', '', 'No Home Physician', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', '', 'No Eligibility', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', '', 'Patient Choice', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', 'Nurse- Ready', '', 'cgName', false, null);
        setExternalStatusRollUp.theStatus('Open', 'Nurse-Missing Info', '', null, false, null);
        setExternalStatusRollUp.theStatus('Open', 'Nurse-Missing Info', '', 'cgName', false, null);
        
        //Determine the Stage
        //setExternalStatusRollUp.theStage(string status, string statusReason, string sublist, string externalStatus, boolean isConverted);
        setExternalStatusRollUp.theStage('Open', '', '', 'In process', false);
        setExternalStatusRollUp.theStage('', '', 'Awaiting Appointment Date Confirmations', 'Accepted', false);
        setExternalStatusRollUp.theStage('', 'Claim Pending', '', '', false);
        setExternalStatusRollUp.theStage('', 'Patient at COE', '', '', false);
        setExternalStatusRollUp.theStage('', '', 'Medical Complications/Conditions', '', true);
        setExternalStatusRollUp.theStage('', 'Appointment Date', '', '', true);
        setExternalStatusRollUp.theStage('', 'No Eligibility', '', '', true);
        setExternalStatusRollUp.theStage('', 'Pended', '', '', true);
        setExternalStatusRollUp.theStage('', 'Claim Paid', '', '', true);
    }
    
}