@RestResource(urlMapping='/ecen/v1/devicetoken')
global with sharing class ECENPatientDevice {

    @HttpPost
    global static string doPost(){
        
        //blob body = RestContext.request.requestBody;
        String ins_id = RestContext.request.headers.get('Ins_id');
        String devicetoken = RestContext.request.headers.get('Token');
        String channelType= RestContext.request.headers.get('ChannelType');
        
        if(devicetoken !=null){
            boolean tokenAdded=false;
            for(ecen_device__c d : [select address__c  from ecen_device__c where ins_id__c = :ins_id]){
                if(d.address__c==devicetoken){
                    tokenAdded=true;
                    break;
                }
            }
            
            if(!tokenAdded){
                if(channelType==null||channelType==''){
                    if(devicetoken.length()>70){
                        channelType='GCM';
                    }else{
                        channelType='APNS';
                    }
                }
                ecen_device__c endpoint = new ecen_device__c(name='device token', ins_id__c=ins_id, channel__c=channelType, address__c = devicetoken );
                upsert endpoint;
            }
        }
        
        return 'ok';
        
    }
}