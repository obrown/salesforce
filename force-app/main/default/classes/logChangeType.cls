public class logChangeType{
    
    string ResultCode;
    public EligChanges[] EligChanges {get;set;}        
        
    public class EligChanges{

        public string EhsGrnbr {get;set;}  
        public string EhsSsn {get;set;}
        public string EhsLname {get;set;}    
        public string EhsFtname {get;set;}
        public string EhsMinit {get;set;}
        string EhsTstamp; //20140909165131
        public string EhsField {get;set;}
        public string EhsBefore{get;set;}  
        public string EhsAfter{get;set;}  
        boolean isError;
            
    }
    
}