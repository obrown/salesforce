@RestResource(urlMapping='/ecen/v1/messages')
global with sharing class ECENMessageExchange {
    
    @HttpGet
    global static coeMobileMessaging doGet() {
        String ins_id = RestContext.request.headers.get('Ins_id');
        coeMobileMessaging mClass = new coeMobileMessaging();
        coeMobileMessaging.MainInfo mainInfo = new coeMobileMessaging.MainInfo();
        mainInfo.announcement='If this is a medical emergency please dial 911.\n\nMessages are meant as a non urgent way to contact your member advocate. If this is a matter that requires a quick response please use the "My Contacts" page.\n\nOtherwise your message will be answered within 2 business days.';
        boolean isCoeUser=true;
        try{
           
           string fooCert = ins_id.left(5);  
           boolean pFound=false;
           phm_patient__c[] pList = [select id,cert__c from phm_patient__c where certSearch__c = :fooCert];
             
           for(phm_patient__c pat : pList){
               if(pat.cert__c==ins_id){
                   pFound=true;
                   break;
                      
               }
                  
           }
           
           if(pFound){
               isCoeUser=true;
               mainInfo.announcement='If this is a medical emergency please dial 911.\n\nMessages are meant as a non urgent way to contact your case manager. Your message will be answered within 2 business days.';
           }
                 
        }catch(queryException e){}
        
        coeMobileMessaging.mainContact mainContact = new coeMobileMessaging.mainContact();
        if(isCoeUser){
            mainContact.phone = '(888) 463-3737';
            mainContact.email = 'memberadvocate@contigohealth.com';
        }else{
            mainContact.phone = '(888) 463-3737';
            mainContact.email = 'memberadvocate@contigohealth.com';
        }
        mainInfo.MainContact  = mainContact;
        mClass.Maininfo = mainInfo;

        coeMobileMessaging.Message[] inbox = new coeMobileMessaging.Message[]{};
        coeMobileMessaging.MessagesSent[] outbox = new coeMobileMessaging.MessagesSent[]{};
        integer read=0;
        for(ECEN_Message__c m : [select user_read__c,body__c,dateReceived__c,dateSent__c,Sender_Email__c,Sender_Name__c,sentMessage__c,subject__c from ECEN_Message__c where Insurance_Id__c =:ins_id order by createdDate desc]){
            if(m.sentMessage__c){
                coeMobileMessaging.User u = new coeMobileMessaging.User();
                u.name=m.Sender_Name__c;
                u.email=m.Sender_Email__c;
        
                coeMobileMessaging.MessagesSent o = new coeMobileMessaging.MessagesSent();
                o.mid = m.id;       
                o.subject = m.subject__c;
                o.body = m.body__c.unescapeHtml4();
                system.debug('o.body '+o.body);
                o.dateReceived = utilities.formatDateTime(m.dateSent__c);
                o.user = u;
                o.isRead = true;
                
                outbox.add(o);
            }else{
                coeMobileMessaging.User u = new coeMobileMessaging.User();
                u.name=m.Sender_Name__c;
                u.email=m.Sender_Email__c;
                coeMobileMessaging.Message o = new coeMobileMessaging.Message();
                o.mid = m.id;   
                o.subject = m.subject__c;
                if(m.body__c!=null){
                    o.body = m.body__c.unescapeHtml4();
                }
                
                o.isRead = m.user_read__c;
                if(o.isRead){read++;}

                if(m.dateReceived__c!=null){
                    o.dateReceived = utilities.formatDateTime(m.dateReceived__c); //m.dateReceived__c.month()+'/'+m.dateReceived__c.day()+'/'+m.dateReceived__c.year();
                }
                o.user = u;
                
                inbox.add(o);   

            }        

        }
        
        mClass.messages = inbox;
        mClass.MessagesSent = outbox;

        coeMobileMessaging.PageInfo pageInfo = new coeMobileMessaging.PageInfo();
        pageInfo.totalResults =inbox.size();
        pageInfo.unRead = inbox.size()-read;
        mClass.PageInfo = pageInfo;

        coeMobileMessaging.User[] contactOptions = new coeMobileMessaging.User[]{};
        coeMobileMessaging.User u = new coeMobileMessaging.User();
        if(isCoeUser){
            u.name = 'Case Manager';
            u.email= 'memberadvocate@contigohealth.com';
        }else{
            u.name = 'Member Advocate';
            u.email= 'memberadvocate@contigohealth.com';
        }
        contactOptions.add(u);
        mClass.contactOptions = contactOptions;
        if(test.isRunningTest()){
            RestContext.response.responseBody= blob.valueof(JSON.serialize(mClass));
        }
        return mClass;
    }
    
    @HttpPost
    global static string doPost() {
        string programType; 
        string facilityId, clientId;
        String route = RestContext.request.headers.get('route');
        blob body = RestContext.request.requestBody;
        String ins_id = RestContext.request.headers.get('Ins_id');
        String sEmail = RestContext.request.headers.get('Semail');
        String sName = RestContext.request.headers.get('Sname');
        
        if(ins_id==null){
            return 'No search term found';
        }
        
        string lookUpId, sobjecttype;
        
        string mobileAlertEmail;
        try{
            //patient_case__c pc = [select id,patient_first_name__c,patient_last_name__c,Patient_Email_Address__c from patient_case__c where bid_id__c = :ins_id order by createdDate desc limit 1];
            patient_case__c pc = [select client_facility__r.facility__r.id, case_type__c, client__c,Client_Facility__c,patient_first_name__c,patient_last_name__c,Patient_Email_Address__c from patient_case__c where bid_id__c = :ins_id order by createdDate desc limit 1];
            if(sName==null){
                sName=pc.patient_first_name__c+' '+pc.patient_last_name__c;
                sEmail=pc.patient_email_address__c;
            }
            lookUpId=pc.id;
            sobjecttype='patient_case__c';
            facilityId= pc.client_facility__r.facility__r.id;
            programType= pc.case_type__c;
        }catch(exception e){}    
        
        if(lookUpId==null){
            try{
           
            //oncology__c oncology = [select id,patient_first_name__c,patient_last_name__c,Patient_Email_Address__c from oncology__c where bid_id__c = :ins_id order by createdDate desc limit 1];
            oncology__c oncology = [select client__c,Client_Facility__c,client_facility__r.facility__r.id,patient_first_name__c,patient_last_name__c,Patient_Email_Address__c from oncology__c where bid_id__c = :ins_id order by createdDate desc limit 1];
            if(sName==null){
                sName=oncology.patient_first_name__c+' '+oncology.patient_last_name__c;
                sEmail=oncology.patient_email_address__c;
            }
                lookUpId=oncology.id;
                sobjecttype='oncology__c';
                facilityId= oncology.client_facility__r.facility__r.id;
                programType= 'Oncology';
            }catch(exception e){}    
        
        }
        
        if(lookUpId==null){
            try{
           
            //bariatric_stage__c bs = [select id,bariatric__c, bariatric__r.patient_first_name__c,bariatric__r.patient_last_name__c,bariatric__r.Patient_Email_Address__c from bariatric_stage__c where bariatric__r.bid_id__c = :ins_id order by createdDate desc limit 1];
            bariatric_stage__c bs = [select bariatric__r.client__c,bariatric__c, bariatric__r.patient_first_name__c,bariatric__r.patient_last_name__c,bariatric__r.Patient_Email_Address__c from bariatric_stage__c where bariatric__r.bid_id__c = :ins_id order by createdDate desc limit 1];
            if(sName==null){
                sName=bs.bariatric__r.patient_first_name__c+' '+bs.bariatric__r.patient_last_name__c;
                sEmail=bs.bariatric__r.patient_email_address__c;
            }
                lookUpId=bs.bariatric__c;
                //clientId=bs.bariatric__r.client__c;
                sobjecttype='bariatric__c';
                programType= 'Bariatric';
            }catch(exception e){}    
        
        }
        
        if(lookUpId==null){
            try{
              
              string fooCert = ins_id.left(5);  
              phm_patient__c p = new phm_patient__c();
              //phm_patient__c[] pList = [select id, cert__c, patient_first_name__c, patient_last_name__c, Patient_Email_Address__c from phm_patient__c where certSearch__c = :fooCert];
              phm_patient__c[] pList = [select Patient_Employer__c,id, cert__c, patient_first_name__c, patient_last_name__c, Patient_Email_Address__c from phm_patient__c where certSearch__c = :fooCert];
              
              for(phm_patient__c pat : pList){
                  if(pat.cert__c==ins_id){
                      p=pat;
                      clientId=pat.Patient_Employer__c;
                      lookUpId=pat.id;
                      sobjecttype='phm_Patient__c';
                      programType= 'Population Health';
                      break;
                      
                  }
                  
              }
              
              if(sName==null){
                sName= p.patient_first_name__c+' '+p.patient_last_name__c;
                sEmail= p.patient_email_address__c;
              }
              
              
            }catch(exception e){}    
        
        }
        
        if(sName==null){
            sName='Mobile User';
        
        }
        
        if(sEmail==null){
            sEmail='NA@noemail.com';
        }
        
        coeMobileMessaging cMessage = (coeMobileMessaging)json.deserialize(body.tostring(), coeMobileMessaging.class);
        ECEN_Message__c sfMessage = new ECEN_Message__c(Insurance_Id__c=ins_id);
        sfMessage.subject__c = cMessage.message.subject;        
        sfMessage.body__c = cMessage.message.body;  
        sfMessage.dateSent__c = datetime.now();
        sfMessage.dateReceived__c = datetime.now();
        sfMessage.Sender_Name__c = sName;
        sfMessage.Sender_Email__c = sEmail;
        sfMessage.sentMessage__c=true;
        sfMessage.put(sobjecttype, lookUpId); // lookup field for the record assiociated with the message 
        sfMessage.Recipient_Email__c='MemberAdvocate@contigohealth.com';
        sfMessage.Recipient_Name__c='Member Advocate';
        insert sfMessage;
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        string[] sendemailto = new string[]{};
        HDP_App_Message__c[] messageContacts = new HDP_App_Message__c[]{};
        
        try{
            if(clientId ==null && facilityId ==null){
                sendemailto.add('MemberAdvocate@contigohealth.com'); 
            }
            messageContacts = [select  Mobile_App_Alert_Email_Contact__c,Program_Type__c from HDP_App_Message__c where client__c= :clientId or facility__c = :facilityId];
            
        }catch(exception e){}
        
        if(messageContacts.isEmpty()){
                if(utilities.isRunninginSandbox()){
                    sendemailto.add(userInfo.getUserEmail());   
                }else{
                    sendemailto.add('MemberAdvocate@contigohealth.com');   
                }
        }else{    
            try{
                for(HDP_App_Message__c am : messageContacts){
                    if(am.Mobile_App_Alert_Email_Contact__c.contains(',')){
                        for(string email : am.Mobile_App_Alert_Email_Contact__c.split(',')){
                            sendemailto.add(email);   
                        }    
                    }else{
                        sendemailto.add(am.Mobile_App_Alert_Email_Contact__c);   
                    }
                }
                  
            }catch(exception e){
                sendemailto.add('MemberAdvocate@contigohealth.com');   
            }
            
        }       
        
        mail.setToAddresses(sendemailto);
        mail.setSubject('New patient message');
        string htmlBody = '<html>';
        htmlBody += '<body>';
        htmlBody += 'A new message has been received from the patient via the HDP mobile app.<br/><br/><a href="' + Url.getSalesforceBaseURL().toExternalForm() + '/' + sfMessage.id+ '">Click here</a> to view and reply to the message<br/><br/>';
        htmlBody += '<table width="90%" style="margin-left:auto:margin-right:auto;border:1px solid black"><tr><th style="border-bottom:1px solid black">';
        
        htmlBody += '<div style="text-align:center;margin-top:.5em;">Message Contents</div><br/>';
        htmlBody += '<div style="text-align:center;margin-top:.5em;">* DO NOT REPLY TO THIS EMAIL *</div><br/>';
        htmlBody += '</tr></th>';
        htmlBody += '<tr><td>';
        
        htmlBody += '<div style="text-decoration:underline;font-size:80%">Subject</div>';
        htmlBody += '<div style="margin-left:5px">'+sfMessage.subject__c+'</div><br/>';
        htmlBody += '<div style="text-decoration:underline;font-size:80%">Body</div>';
        htmlBody += '<div style="margin-left:5px">'+string.escapeSingleQuotes(sfMessage.body__c)+'</div><br/>';
        htmlBody +=  '</fieldset >';
        htmlBody += '</td></tr>';
        htmlBody += '</table>';
        htmlBody += '</body>';
        htmlBody += '</html>';
        mail.setHTMLBody(htmlBody); 
        
        mail.setReplyTo('noreply@contigohealth.com');
        mail.setSenderDisplayName('noreply@contigohealth.com');
        try{    
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});   
        }catch(exception e){}
        return 'Message was sent successfully';
        
    }
}