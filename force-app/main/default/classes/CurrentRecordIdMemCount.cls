public class CurrentRecordIdMemCount{

   integer total_count;
   public Appeal_Case__c previousCase {get; private set;}
   public string appealTypeValue {get; set;}
   public boolean showIdate {get; set;}
   public string recordTypeName {get; private set;}
   public selectOption[] clientList  {get; private set;}  // 9-24-20 added 
   public CurrentRecordIdMemCount(ApexPages.StandardController controller) {
       
       init((Appeal_Case__c)controller.getRecord(), ApexPages.Currentpage().getParameters().get('pcid'));
       clients();//9-24-20 added
       
   }
    
   void init(appeal_case__c  ac, string prevCase){
        try{
           recordTypeName = [select name from recordtype where id=:apexpages.currentPage().getParameters().get('RecordType')].name;
       }catch(exception e){}
        string pCase = prevCase;
        if(pCase!=null){
            pCase = string.escapeSingleQuotes(prevCase);
            previousCase = [select case_Number__c from Appeal_Case__c where id = :prevCase limit 1];
            
        }
    
        total_count=0;
        
        if(ac.member_id__c==null){
            return;
        }
        
        for(appeal_case__c a :[select Member_ID__c from appeal_case__c]){
            if(ac.member_id__c==a.Member_ID__c){total_count++;}
        }
        
        
    }
    
    Public Integer gettotal_count() {
       return (total_count>0) ? total_count-1 : total_count;
    }
    
    // added 9-24-20 must pull all clients so the client list will populate the historical data
    public void clients(){
       clientList = new selectOption[]{};
       clientList.add(new selectOption('', '-- None --'));
            
       for(Client__c cL : [select id, name,Corporate_Name__c,Active__c from Client__c where Corporate_Name__c!=null and name!='Test Client' order by Corporate_Name__c asc]){
            clientList.add(new selectOption(cL.Corporate_Name__c, cL.Corporate_Name__c));
            
            }    
    system.debug('clientlist ='+clientList);
    }    
    
    public void setAppealType(){
       string appealType= ApexPages.CurrentPage().getParameters().get('appealType');
       if(appealType=='HIPAA Incident'||appealType=='HIPAA Breach'){
           showIdate  =true;        
        
       }
    
    }
}