@isTest()
private class oncolcogyStateFormatTest{
        
    static testMethod void StatesTest(){
        oncolcogyStateFormat.stateAbbrv(null);
        oncolcogyStateFormat.stateFormal(null);
        
        system.assert(oncolcogyStateFormat.stateFormal('AL')=='Alabama');
        system.assert(oncolcogyStateFormat.stateFormal('AK ')=='Alaska');
        system.assert(oncolcogyStateFormat.stateFormal('AZ')=='Arizona');
        system.assert(oncolcogyStateFormat.stateFormal('AR')=='Arkansas');
        system.assert(oncolcogyStateFormat.stateFormal('CA')=='California');
        system.assert(oncolcogyStateFormat.stateFormal('CO')=='Colorado');
        system.assert(oncolcogyStateFormat.stateFormal('CT')=='Connecticut');
        system.assert(oncolcogyStateFormat.stateFormal('DE')=='Delaware');
        system.assert(oncolcogyStateFormat.stateFormal('FL')=='Florida');
        system.assert(oncolcogyStateFormat.stateFormal('GA')=='Georgia');
        system.assert(oncolcogyStateFormat.stateFormal('HI')=='Hawaii');
        system.assert(oncolcogyStateFormat.stateFormal('ID')=='Idaho');
        system.assert(oncolcogyStateFormat.stateFormal('IL')=='Illinois');
        system.assert(oncolcogyStateFormat.stateFormal('IN')=='Indiana');
        system.assert(oncolcogyStateFormat.stateFormal('IA')=='Iowa');
        system.assert(oncolcogyStateFormat.stateFormal('KS')=='Kansas');
        system.assert(oncolcogyStateFormat.stateFormal('KY')=='Kentucky');
        system.assert(oncolcogyStateFormat.stateFormal('LA')=='Louisiana');
        system.assert(oncolcogyStateFormat.stateFormal('ME')=='Maine');
        system.assert(oncolcogyStateFormat.stateFormal('MD')=='Maryland');
        system.assert(oncolcogyStateFormat.stateFormal('MA')=='Massachusetts');
        system.assert(oncolcogyStateFormat.stateFormal('MI')=='Michigan');
        system.assert(oncolcogyStateFormat.stateFormal('MN')=='Minnesota');
        system.assert(oncolcogyStateFormat.stateFormal('MS')=='Mississippi');
        system.assert(oncolcogyStateFormat.stateFormal('MO')=='Missouri');
        system.assert(oncolcogyStateFormat.stateFormal('MT')=='Montana');
        system.assert(oncolcogyStateFormat.stateFormal('NE')=='Nebraska');
        system.assert(oncolcogyStateFormat.stateFormal('NV')=='Nevada');
        system.assert(oncolcogyStateFormat.stateFormal('NH')=='New Hampshire');
        system.assert(oncolcogyStateFormat.stateFormal('NJ')=='New Jersey');
        system.assert(oncolcogyStateFormat.stateFormal('NM')=='New Mexico');
        system.assert(oncolcogyStateFormat.stateFormal('NY')=='New York');
        system.assert(oncolcogyStateFormat.stateFormal('NC')=='North Carolina');
        system.assert(oncolcogyStateFormat.stateFormal('ND')=='North Dakota');
        system.assert(oncolcogyStateFormat.stateFormal('OH')=='Ohio');
        system.assert(oncolcogyStateFormat.stateFormal('OK')=='Oklahoma');
        system.assert(oncolcogyStateFormat.stateFormal('OR')=='Oregon');
        system.assert(oncolcogyStateFormat.stateFormal('PA')=='Pennsylvania');
        system.assert(oncolcogyStateFormat.stateFormal('RI')=='Rhode Island');
        system.assert(oncolcogyStateFormat.stateFormal('SC')=='South Carolina');
        system.assert(oncolcogyStateFormat.stateFormal('SD')=='South Dakota');
        system.assert(oncolcogyStateFormat.stateFormal('TN')=='Tennessee');        
        system.assert(oncolcogyStateFormat.stateFormal('TX')=='Texas');        
        system.assert(oncolcogyStateFormat.stateFormal('UT')=='Utah');
        system.assert(oncolcogyStateFormat.stateFormal('VT')=='Vermont');
        system.assert(oncolcogyStateFormat.stateFormal('VA')=='Virginia');
        system.assert(oncolcogyStateFormat.stateFormal('WA')=='Washington');
        system.assert(oncolcogyStateFormat.stateFormal('WV')=='West Virginia');
        system.assert(oncolcogyStateFormat.stateFormal('WI')=='Wisconsin');
        system.assert(oncolcogyStateFormat.stateFormal('WY')=='Wyoming'); 
        system.assert(oncolcogyStateFormat.stateFormal('')=='');
        
        system.assert(oncolcogyStateFormat.stateAbbrv('Alabama')=='AL');
        system.assert(oncolcogyStateFormat.stateAbbrv('Alaska')=='AK'); 
        system.assert(oncolcogyStateFormat.stateAbbrv('Arizona')=='AZ');
        system.assert(oncolcogyStateFormat.stateAbbrv('Arkansas')=='AR');
        system.assert(oncolcogyStateFormat.stateAbbrv('California')=='CA');
        system.assert(oncolcogyStateFormat.stateAbbrv('Colorado')=='CO');
        system.assert(oncolcogyStateFormat.stateAbbrv('Connecticut')=='CT');
        system.assert(oncolcogyStateFormat.stateAbbrv('Delaware')=='DE');
        system.assert(oncolcogyStateFormat.stateAbbrv('Florida')=='FL');
        system.assert(oncolcogyStateFormat.stateAbbrv('Georgia')=='GA');
        system.assert(oncolcogyStateFormat.stateAbbrv('Hawaii')=='HI');
        system.assert(oncolcogyStateFormat.stateAbbrv('Idaho')=='ID');
        system.assert(oncolcogyStateFormat.stateAbbrv('Illinois')=='IL');
        system.assert(oncolcogyStateFormat.stateAbbrv('Indiana')=='IN');
        system.assert(oncolcogyStateFormat.stateAbbrv('Iowa')=='IA');
        system.assert(oncolcogyStateFormat.stateAbbrv('Kansas')=='KS');
        system.assert(oncolcogyStateFormat.stateAbbrv('Kentucky')=='KY');
        system.assert(oncolcogyStateFormat.stateAbbrv('Louisiana')=='LA');
        system.assert(oncolcogyStateFormat.stateAbbrv('Maine')=='ME');
        system.assert(oncolcogyStateFormat.stateAbbrv('Maryland')=='MD');
        system.assert(oncolcogyStateFormat.stateAbbrv('Massachusetts')=='MA');
        system.assert(oncolcogyStateFormat.stateAbbrv('Michigan')=='MI');
        system.assert(oncolcogyStateFormat.stateAbbrv('Minnesota')=='MN');
        system.assert(oncolcogyStateFormat.stateAbbrv('Mississippi')=='MS');
        system.assert(oncolcogyStateFormat.stateAbbrv('Missouri')=='MO');
        system.assert(oncolcogyStateFormat.stateAbbrv('Montana')=='MT');
        system.assert(oncolcogyStateFormat.stateAbbrv('Nebraska')=='NE');
        system.assert(oncolcogyStateFormat.stateAbbrv('Nevada')=='NV');
        system.assert(oncolcogyStateFormat.stateAbbrv('New Hampshire')=='NH');
        system.assert(oncolcogyStateFormat.stateAbbrv('New Jersey')=='NJ');
        system.assert(oncolcogyStateFormat.stateAbbrv('New Mexico')=='NM');
        system.assert(oncolcogyStateFormat.stateAbbrv('New York')=='NY');
        system.assert(oncolcogyStateFormat.stateAbbrv('North Carolina')=='NC');
        system.assert(oncolcogyStateFormat.stateAbbrv('North Dakota')=='ND');
        system.assert(oncolcogyStateFormat.stateAbbrv('Ohio')=='OH');
        system.assert(oncolcogyStateFormat.stateAbbrv('Oklahoma')=='Ok');
        system.assert(oncolcogyStateFormat.stateAbbrv('Oregon')=='OR');
        system.assert(oncolcogyStateFormat.stateAbbrv('Pennsylvania')=='PA');
        system.assert(oncolcogyStateFormat.stateAbbrv('Rhode Island')=='RI');
        system.assert(oncolcogyStateFormat.stateAbbrv('South Carolina')=='SC');
        system.assert(oncolcogyStateFormat.stateAbbrv('South Dakota')=='SD');
        system.assert(oncolcogyStateFormat.stateAbbrv('Tennessee')=='TN');
        system.assert(oncolcogyStateFormat.stateAbbrv('Texas')=='TX');
        system.assert(oncolcogyStateFormat.stateAbbrv('Utah')=='UT');
        system.assert(oncolcogyStateFormat.stateAbbrv('Vermont')=='VT');
        system.assert(oncolcogyStateFormat.stateAbbrv('Virginia')=='VA');
        system.assert(oncolcogyStateFormat.stateAbbrv('Washington')=='WA');
        system.assert(oncolcogyStateFormat.stateAbbrv('West Virginia')=='WV');
        system.assert(oncolcogyStateFormat.stateAbbrv('Wisconsin')=='WI');
        system.assert(oncolcogyStateFormat.stateAbbrv('Wyoming')=='WY');
        system.assert(oncolcogyStateFormat.stateAbbrv('')==''); 
    }
        
        
    
    
}