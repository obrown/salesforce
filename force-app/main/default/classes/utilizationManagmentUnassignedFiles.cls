public with sharing class utilizationManagmentUnassignedFiles{
    public string fileName {get; set;}
    final string sId = userinfo.getSessionid();
    final string userId = userinfo.getuserid();
    final string orgId = userinfo.getOrganizationId();
    public FileChangeRequestResponseStruct.Files[] fileList {get; set;}
    public FileChangeRequestResponseStruct.Files[] reassigned_fileList {get; set;}
    public boolean isApiError {get; set;}
    final string sourceDir = fileServer__c.getInstance().populationHealthSourceDirectory__c; //z:/FaxCM/FaxInboundCM/
    final string reassigned_sourceDir = fileServer__c.getInstance().baseDirectory__c+fileServer__c.getInstance().populationHealthDestinationDirectory__c+'unassigned/'; //z:/FaxCM/FaxInboundCM/
    //final string sourceDir = 'z:/FaxCM/';
    final string sourceEmpDir = fileServer__c.getInstance().populationHealthEmpSourceDirectory__c;
    final string fileserverurl = fileServer__c.getInstance().fileserverurl__c;
    final static boolean hdpGrpMember = utilities.getIsHDPGrpMember();
    
    public string getsId(){
        return sid;
    }

    public string getreassigned_sourceDir(){
        return reassigned_sourceDir;
    }
    
    public string getbaseDir(){
        return sourceDir;
    }
    
    public boolean pageLoaded {get; set;}
    
    public string getuserId(){
        return userId;
    }
    public void setApiError(){
        isApiError=true;
    }
    
    public void setFileList(){
        fileName='';
        pageLoaded =true;
        isApiError=false;
        string result =fileServer.unassignedfiles(sourceDir);
        if(Test.isRunningTest()){
            result ='{"Filelist":[{"Filename":"200144117.pdf","LastModified":"2019-10-16T16:24:15.534902-04:00"},{"Filename":"200144509.pdf","LastModified":"2019-10-12T12:30:10.6684224-04:00"},{"Filename":"phDocumentTesting.pdf","LastModified":"2019-10-11T17:16:34.6903516-04:00"}],"Message":"Success","ResultCode":"0"}';
        }
        FileChangeRequestResponseStruct foo = (FileChangeRequestResponseStruct )JSON.deserialize(result, FileChangeRequestResponseStruct .class);
        fileList = foo.FileList;
        
        if(hdpGrpMember){
            result =fileServer.unassignedfiles(sourceEmpDir);
            FileChangeRequestResponseStruct hdpFoo = (FileChangeRequestResponseStruct )JSON.deserialize(result, FileChangeRequestResponseStruct .class);
            fileList.addAll(hdpFoo.FileList);           
        }
        
        //'r:/salesforceattachments_sandbox/testing/population_health/utilization_management/unassigned/'
        result =fileServer.unassignedfiles(fileServer__c.getInstance().baseDirectory__c+fileServer__c.getInstance().populationHealthDestinationDirectory__c + 'unassigned/');
        if(Test.isRunningTest()){
            result ='{"Filelist":[{"Filename":"200144117.pdf","LastModified":"2019-10-16T16:24:15.534902-04:00"},{"Filename":"200144509.pdf","LastModified":"2019-10-12T12:30:10.6684224-04:00"},{"Filename":"phDocumentTesting.pdf","LastModified":"2019-10-11T17:16:34.6903516-04:00"}],"Message":"Success","ResultCode":"0"}';
        }
        FileChangeRequestResponseStruct reassigned = (FileChangeRequestResponseStruct )JSON.deserialize(result, FileChangeRequestResponseStruct .class);
        reassigned_fileList = reassigned.FileList;
        
    }
    
    public void setReassignedFileName(){
         string file_name = ApexPages.CurrentPage().getParameters().get('fn');
        if(file_name ==null){
            return;
        }
        
        string fileTextName = file_name.replaceAll('&', '%26');
        system.debug(reassigned_sourceDir);
        system.debug(fileTextName );
        if(fileTextName ==''){
            fileName=null;
        }else{
            fileName = fileserverurl+'openfile?filepath='+reassigned_sourceDir + fileTextName+ '&oid=' + orgId + '&sid=' + sid + '&uid=' +userId;
        }
        
    }
    
    public void setFileName(){
         string file_name = ApexPages.CurrentPage().getParameters().get('fn');
        if(file_name ==null){
            return;
        }
        
        string fileTextName = file_name.replaceAll('&', '%26');
        system.debug(fileTextName);
        
        if(fileTextName ==''){
            fileName=null;
        }else{
            fileName = fileserverurl+'openfile?filepath='+sourceDir+ fileTextName+ '&oid=' + orgId + '&sid=' + sid + '&uid=' +userId;
        }
        
    }
    
    
}