public with sharing class PopulateHistory {

	list<LeadHistory> lhStatus = new list<LeadHistory>([select field, leadid, createdbyID,createdDate,oldValue,newValue from LeadHistory where createdDate >= :date.today()-1 and createdDate < :date.today()]);
	list<OpportunityFieldHistory> ohStatus = new list<OpportunityFieldHistory >([select field, createdbyID,OpportunityId, createdDate,oldValue,newValue from OpportunityFieldHistory where createdDate >= :date.today()-1 and createdDate < :date.today()]);
	list<Patient_Case__History> phStatus = new list<Patient_Case__History >([select field, createdbyID,parentId, createdDate,oldValue,newValue from Patient_Case__History where createdDate >= :date.today()-1 and createdDate < :date.today()]);
	
	list<History__c> newHist = new list<History__c>();

	public void run(){
		if(!lhStatus.isempty()){
	
		for(leadHistory l: lhStatus){
	
			History__c tempHistory = new History__c();
			tempHistory.createdDate__c = l.createdDate;
			tempHistory.field__c = l.field;
			tempHistory.oldFieldValue__c = string.valueof(l.oldvalue);
			tempHistory.newFieldValue__c = string.valueof(l.newvalue);
	        tempHistory.parentID__c = l.leadid;
            tempHistory.ChangedBy__c = l.createdbyID;
            tempHistory.originalID__c = l.id;	        
			newHist.add(tempHistory);
	
		}	
	
		}

		if(!ohStatus.isempty()){
		
			for(OpportunityFieldHistory o: ohStatus){
		
				History__c tempHistory = new History__c();
				tempHistory.createdDate__c = o.createdDate;
				tempHistory.field__c = o.field;
				tempHistory.oldFieldValue__c = string.valueof(o.oldvalue);
				tempHistory.newFieldValue__c = string.valueof(o.newvalue);
		        tempHistory.parentID__c = o.OpportunityId;
		        tempHistory.ChangedBy__c = o.createdbyID;
            	tempHistory.originalID__c = o.id;
				newHist.add(tempHistory);
		
			}	
		
		}
		
		if(!phStatus.isempty()){
		
			for(Patient_Case__History p: phStatus){
		
				History__c tempHistory = new History__c();
				tempHistory.createdDate__c = p.createdDate;
				tempHistory.field__c = p.field;
				tempHistory.oldFieldValue__c = string.valueof(p.oldvalue);
				tempHistory.newFieldValue__c = string.valueof(p.newvalue);
		        tempHistory.parentID__c = p.parentid;
		        tempHistory.ChangedBy__c = p.createdbyID;
            	tempHistory.originalID__c = p.id;
				newHist.add(tempHistory);
		
			}	
		
		}
		

		if(!newHist.isEmpty()){
		
			insert newHist;
		
		}
	}
	
	static testMethod void testPH(){
		
		PopulateHistory ph = new PopulateHistory();
		ph.run();
		
	}


}