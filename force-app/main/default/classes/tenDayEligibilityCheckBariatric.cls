public class tenDayEligibilityCheckBariatric{    

    messaging.Singleemailmessage[] finalMail = new messaging.Singleemailmessage[]{};
    Bariatric_Stage__c[] theOpplist = new list<Bariatric_Stage__c>();
    
    Eligibilty__c[]                eligList = new Eligibilty__c[]{};
    string[]                       addEligibility = new string[]{};
    string[]                       sendemailto = new string[]{'hdpeligibility@hdplus.com'};
    
    id queueGroupId;
    
    public void run(){
    
        try{
            set<id> idSet = new set<id>();
            for(Bariatric_Stage__c p : [select id, name from Bariatric_Stage__c where status__c != 'Closed' and status__c != 'Completed' and (Estimated_Arrival__c >= :date.today().addDays(1) or Actual_Arrival__c > :date.today().addDays(1) or (Estimated_Arrival__c = null or Actual_Arrival__c=null)) and actual_departure__c = null]){
                
                idSet.add(p.id);
                if(idSet.size()>=90){
                  futureElig.bariatricEligibility(idSet);
                  idSet.clear();
                }
                
            }
            if(!idSet.isEmpty()){
                futureElig.bariatricEligibility(idSet);
            }
                
        }catch (exception e){
            batchProcess.batchException(e.getMessage(),e.getLineNumber());                 
        }  
            
    }
   

}