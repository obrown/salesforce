public with sharing class oncologyManualReferralController {
    public oncologyCaseEncryptedData oc { get; set; }

    public Oncology__c obj { get; set; }

    public Oncology_Physician__c physician { get; set; }

    public Oncology_Physician__c[] physicians { get; set; }

    public string objJSON;

    public Oncology__c pc { get; set; }

    public oncologyManualReferralController() {
        string cases = ApexPages.CurrentPage().getParameters().get('cases');
        ApexPages.CurrentPage().getParameters().put('cases',  null);
        oc = (oncologyCaseEncryptedData) JSON.deserialize(
            cases,
            oncologyCaseEncryptedData.class
        );
        pc = [
            select
                Carrier__c,
                Carrier__r.name,
                client_facility__r.client__r.Group_Number__c,
                client_facility__r.facility__r.name,
                Client_Name__c,
                Date_Eligibility_Verified__c,
                Date_of_Diagnosis_Patient_Reported__c,
                Employee_Healthplan__c,
                Employee_Healthplan__r.name,
                Employee_Other_Phone__c,
                Estimated_Arrival_Date__c,
                Estimated_Departure_Date__c,
                Evaluation_Hotel__c,
                Evaluation_Hotel__r.name,
                Evaluation_Hotel_Check_in_Date__c,
                Evaluation_Hotel_Check_Out_Date__c,
                Evaluation_Travel_Type__c,
                expeditedTravelRequest__c,
                Facility_Nurse_Email__c,
                Facility_Nurse_Name__c,
                Facility_Nurse_Phone_Number__c,
                Gender__c,
                HDP_Nurse__c,
                HDP_Nurse_Email__c,
                HDP_Nurse_Name__c,
                HDP_Nurse_Phone_Number__c,
                Last_Appointment_Date__c,
                Last_Appointment_Date_and_Time__c,
                Name,
                Other_Pertinent_Medical_Information__c,
                Patient_Best_Time_To_Contact__c,
                Patient_City__c,
                PatientLAT_AMPM__c,
                Patient_LAT_Hours__c,
                PatientLAT_Minutes__c,
                Patient_LAT_Timezone__c,
                Patient_Medical_Plan_Number__c,
                Patient_Phone__c,
                Patient_Phone2__c,
                Patient_Phone3__c,
                Patient_Preferred_Phone__c,
                Patient_State__c,
                Patient_Zip_Code__c,
                Preferred_Language__c,
                Primary_Diagnosis_Patient_Reported__c,
                Referral_Date__c,
                Relationship_to_Subscriber__c,
                Translation_Services_Required__c,
                Travel_Needs_Comments__c,
                Travel_Notes__c,
                Treatment_Patient_Reported__c,
                trfCreatedDate__c
            from Oncology__c
            where id = :oc.id
        ];

        loadPhysicians();
    }

    void loadPhysicians() {
        physicians = new Oncology_Physician__c[]{};
        physicians = [
            select
                id,
                Associated_Facilities__c,
                City__c,
                Fax_Number__c,
                Local_Physician__c,
                Oncology__c,
                Phone_Number__c,
                Specialty__c,
                State__c,
                Street_Address__c,
                Zip_Code__c
            from Oncology_Physician__c
            where Oncology__c = :oc.id and Local_PCP__c = true
        ];
    }
}
