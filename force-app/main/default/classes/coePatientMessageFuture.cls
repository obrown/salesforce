public class coePatientMessageFuture{

    //Set should contain 1 id
    @future(callout=true)
    public static void sendPushNotification(set<string> messageId ){
        
        map<string, string> messageMap =new map<string, string>();
        string ins_id;
        coeMobileMessaging.apiMessageBody apiMessageBody = new coeMobileMessaging.apiMessageBody();
        
        for(ecen_message__c m : [select body__c, subject__c,Insurance_Id__c from ecen_message__c where id in :messageId]){
            
            ins_id=m.Insurance_Id__c;
            apiMessageBody.subject=m.subject__c;
            
            if(m.body__c.length()>50){
                apiMessageBody.body=m.body__c.unescapeHtml4().left(50)+' ...';
            }else{
                apiMessageBody.body=m.body__c.unescapeHtml4();
            }
            
            
            
        }
        ECEN_Message__c[] mList = new ECEN_Message__c[]{};
            
            try{
                mList = [select Patient_Sent_Message__c, user_read__c from ECEN_Message__c where Insurance_Id__c =:ins_id and Patient_Sent_Message__c=false and user_read__c=false];
            }catch(exception e){}
        apiMessageBody.count=mList.size();//
        string mBody = JSON.serialize(apiMessageBody);
        messageMap.put(ins_id.tolowercase(), mBody);
        
        for(ecen_device__c d : [select address__c,ins_id__c, channel__c from ecen_device__c where ins_id__c in :messageMap.keyset() order by createdDate desc limit 10]){
               
               http h = new http();
        
                HttpRequest req = new HttpRequest ();
                //req.setEndPoint('https://connect.hdplus.com:7443/mapi/sendPushNotification/');
                req.setEndPoint('https://connect.hdplus.com/mapi/sendPushNotification/');
                req.setBody(messageMap.get(d.ins_id__c.tolowercase()));
                req.setHeader('device_token',d.address__c);
                
                if(d.channel__c==null||d.channel__c==''){
                    req.setHeader('channel','APNS');
                
                }else{
                    req.setHeader('channel',d.channel__c);
                }    
                
                req.setmethod('POST');
                req.setTimeout(120000);
        
                HttpResponse res = h.send(req);   
                system.debug(res.getbody());
             
        }
        
    }
    
}