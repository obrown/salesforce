public with sharing class FileUploader{

    public boolean isError { get; set; }

    public PageReference saveClaimsFileRandomSelectionVF() {
        insertRecords=true;
        saveClaimsFileRandomSelection();
        return null;
    }


    public string nameFile {get;set;}
    public transient blob contentFile {get;set;}
    
    public integer total {get; set;}
    public integer newAudits {get; set;}
    
    public string percentToAudit {get; set;}
    
    public boolean insertRecords {get; set;}
    
    //public Claim_Audit__c dummyClaimAudit {get;set;}
    public final string theURL {get; private set;}
    transient String[] filelines = new String[]{};
    public caClaim__c[] clmsToUpload {get;set;}
    list<caClaim__c> newclmsToUpload;
    set<Claims_Audit_Client__c> nCacList;
    public uploadInformation[] uploadInformation {get; set;} 
    Transplant_Notes__c[] transNotes;
    
    public FileUploader(){
        theURL = URL.getSalesforceBaseUrl().toExternalForm();
    }
    
    class uploadInformation{
        public string temp {get;set;}
        public string client {get;set;}
        public string processor {get;set;}
        public string total {get;set;}
        public string newAudits {get;set;}
    }
    
    public void saveClaimsFileRandomSelection(){
        isError=true;
        if(percentToAudit==null||percentToAudit==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please provide the percent to be audited'));
            return;
        }
        try{
           newclmsToUpload = new list<caClaim__c>();
           if(clmsToUpload==null){
               ReadClaimsFile();
           }
           
           //Create client records that don't exist
           if(nCacList != null && nCacList.size()>0){
               
               database.insert(new list<Claims_Audit_Client__c>(nCacList), false);
           
               for(caClaim__c c : clmsToUpload){
                   for(Claims_Audit_Client__c nc: nCacList ){
                       if(c.Client__c == null && c.Client__r.name == nc.name){
                          c.Client__c = nc.id; 
                          break;
                       }
                   }
               }
           
           }
           
           map<string, caClaim__c[]> cFoo = new map<string, caClaim__c[]>(); 
           caClaims_Processed__c[] processedCount = new caClaims_Processed__c[]{};
           map<id,map<string, caClaims_Processed__c>> processedMapCount = new map<id,map<string, caClaims_Processed__c>>();
           
           for(caClaims_Processed__c c : [select client__r.id, Claims_Processed__c, Month_Paid__c from caClaims_Processed__c]){
                map<string, caClaims_Processed__c> foo = processedMapCount.get(c.client__c);
                if(foo==null){
                    foo = new map<string, caClaims_Processed__c>();
                }
                foo.put(c.month_paid__c.year()+'-'+c.month_paid__c.month(), c);
                processedMapCount.put(c.client__r.id, foo);
                
           }
           
           for(caClaim__c c : clmsToUpload){
               if(integer.valueof(percentToAudit)>0){
                   caClaim__c[] foo = cFoo.get(c.Client__r.id+''+c.Claims_Processor_Crosswalk__c);
                   if(foo==null){
                       foo = new caClaim__c[]{};
                   }
                   foo.add(c);
                   cfoo.put(c.Client__r.id+''+c.Claims_Processor_Crosswalk__c, foo);
                   map<string, caClaims_Processed__c> cpfoo = processedMapCount.get(c.Client__r.id);
                   
                   if(cpfoo==null){
                        cpfoo = new map<string, caClaims_Processed__c>();
                   }
                   
                   caClaims_Processed__c cp = cpfoo.get(c.date_paid__c.year()+'-'+c.date_paid__c.month());
                   
                   if(cp==null){
                        cp= new caClaims_Processed__c(client__c=c.Client__r.id,Month_Paid__c=date.valueof(c.date_paid__c.year()+'-'+c.date_paid__c.month()+'-1'),Claims_Processed__c=0);
                   }
                   if(cp.Claims_Processed__c==null){cp.Claims_Processed__c =0;}
                   cp.Claims_Processed__c = cp.Claims_Processed__c+1;
                   cpfoo.put(c.date_paid__c.year()+'-'+c.date_paid__c.month(),cp);
                   processedMapCount.put(c.Client__c, cpfoo);
                  
               }
           }
           
           for(id i : processedMapCount.keyset()){
           
                for(string d : processedMapCount.get(i).keyset()){
                    processedCount.add(processedMapCount.get(i).get(d));
                }
           
           }
           
           if(insertRecords){
               database.upsert(processedCount, false );
           }
           
           //cfoo: a map of claims, key is client and processor, value is the list of claims uploaded in the csv file
           decimal r = decimal.valueof(percentToAudit); //Percent to pull audit for the client, based on the database record
           r = r/100;
           
           uploadInformation= new uploadInformation[]{};
           for(string s: cfoo.keySet()){
           
               caClaim__c[] foo = cFoo.get(s); //List of claims processed , by client, by processor
               
               if(r!=0){
               
                    
                    integer x=foo.size(); // total number of claims processed by client, by processor
                    integer y = math.round(integer.valueof(x*r)) +1; //The number of records that will satisfy the percent to pull
                    set<integer> z = randomNumbers.randomWithLimit(x, y); //A set of randomly generated integers. The size of the set will be equal to y 
                    
                    uploadInformation ui = new uploadInformation();
                    ui.temp = s;
                    system.debug('ui.temp ' +ui.temp);
                    ui.newAudits=string.valueof(z.size());
                    ui.total= string.valueof(x);
                    uploadInformation.add(ui);
                    
                    if(x>0){
                        for(integer i : z){
                            try{
                                if(i==0){i=1;}
                                newclmsToUpload.add(foo[(i-1)]); //Add the claim as a new claim audit record to be audited later
                                
                            }catch(exception e){
                                //catch errors silently, add them to the email
                                //body += (i-1) + ' ' + s + ' ' + foo.size() + ' ' + e.getMessage() + '\n';
                                system.debug(e.getMessage()+''+foo[i]);
                            }
                        }
                    }
               
               }
               
           }
           
           newAudits = newclmsToUpload.size();
           
           map<string, string> pMap = new map<string, string>();
           map<string, string> cMap = new map<string, string>();
           
           for(uploadInformation ui : uploadInformation){
               //clientProcessorMap.put(ui.temp, null);
               pMap.put(ui.temp.right(18), null);
               
               if(ui.temp.left(4)!='null'){
                   cMap.put(ui.temp.left(18), null);
               }
           }
           
           for(Claims_Audit_Client__c cac : [select name from Claims_Audit_Client__c where id in :cMap.keyset()]){
               cMap.put(cac.id, cac.name);
           }
           
           
           for(Claims_Processor_Crosswalk__c cpc : [select id, Name ,Processor_Alias_Name__c from Claims_Processor_Crosswalk__c where id in :pMap.keySet()]){
               pMap.put(cpc.id, cpc.name);
           }
           
           for(uploadInformation ui : uploadInformation){
                ui.processor = pMap.get(ui.temp.right(18));
               
               if(ui.temp.left(4)!='null'){
                   ui.client = cMap.get(ui.temp.left(18));
               }
           }
           
           if(insertRecords){
               
               Set<caClaim__c> myset = new Set<caClaim__c>();
               List<caClaim__c> result = new List<caClaim__c>();
               myset.addAll(newclmsToUpload);
               result.addAll(myset);
               upsert result claim_number__c;
               
           }    
           
           set<id> clmIDset = new set<id>();
           for(caClaim__c c : newclmsToUpload){
               clmIDset.add(c.id);
           }
           //Update audit counts
           if(insertRecords){
               futureElig.setClaimAuditCount(clmIDset);
           }
           
           //Update Claim names
           if(insertRecords){
               claimAuditFuture.updateClaimAuditName(clmIDset);
           }
           
           
        }catch(dmlException e){
            
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                if(i<10){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getlineNumber()); 
                }else{
                    break;
                }
            }
            return;
        } 
        isError=false;
        //return new pageReference(theURL+'/apex/caClaimTab');
    }
   
   
    public pageReference saveClaimsFile(){
        try{
           
           if(clmsToUpload==null){
               ReadClaimsFile();
           }
           
           //Create client records that don't exist
           if(nCacList != null && nCacList.size()>0){
               
               database.insert(new list<Claims_Audit_Client__c>(nCacList), false);
           
               for(caClaim__c c : clmsToUpload){
                   for(Claims_Audit_Client__c nc: nCacList ){
                       if(c.Client__c == null && c.Client__r.name == nc.name){
                          c.Client__c = nc.id; 
                          break;
                       }
                   }
               }
           
           }
           
           upsert clmsToUpload claim_number__c;
           
           Claim_Audit__c[] caList = new Claim_Audit__c[]{};
           
           for(caClaim__c c : clmsToUpload){
               Claim_Audit__c ca = new Claim_Audit__c();
               ca.Claim__c = c.id;
               ca.Claim_Processor__c = c.Claims_Processor_Crosswalk__c;
               ca.Audit_Type__c= 'Random Selection';
               ca.Audit_Status__c = 'To Be Audited';
               caList.add(ca);
           }
           
           database.insert(caList, false);
           
        }catch (dmlException e){
            
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                if(i<10){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)); 
                }else{
                    break;
                }
            }
            return null;
        } 
        
        return new pageReference(theURL+'/apex/caClaimTab');
    }
    
    
    public Pagereference ReadClaimsFile(){
        clmsToUpload = new caClaim__c[]{};
        nCacList = new set<Claims_Audit_Client__c>();
        transient map<string, Claims_Audit_Client__c > clientMap = new map<string, Claims_Audit_Client__c >();
        transient map<string, Claims_Processor_Crosswalk__c > processorMap = new map<string, Claims_Processor_Crosswalk__c >();
        
        for(Claims_Audit_Client__c cac : [select name,underwriter__c,group_number__c,percent_to_pull__c, id from Claims_Audit_Client__c]){
            
            
            if(cac.group_number__c != null){
           
                if(cac.group_number__c.contains(',')){
                    for(string c : cac.group_number__c.split(',')){
                        clientMap.put(cac.underwriter__c+c.toLowerCase().trim(), cac);
                        system.debug(cac.underwriter__c+' '+c.toLowerCase().trim());
                    }
                
                }else{
                    clientMap.put(cac.underwriter__c + cac.group_number__c.toLowerCase().trim(), cac);
                    system.debug(cac.underwriter__c+' '+cac.group_number__c);
           
                }
            }
        }
        
        for(Claims_Processor_Crosswalk__c cpc : [select id, Name ,Processor_Alias_Name__c from Claims_Processor_Crosswalk__c where isActive__c = true]){
            
            if(cpc.Processor_Alias_Name__c!=null){
                
                if(cpc.Processor_Alias_Name__c.contains(',')){
                    for(string s : cpc.Processor_Alias_Name__c.split(',')){
                        processorMap.put(s.toLowerCase().trim(), cpc);
                    }                
                }else{
                    processorMap.put(cpc.Processor_Alias_Name__c.toLowerCase().trim(), cpc);
                    
                }
            }
        
        }
        
        if(nameFile!=null){
        
        //nameFile=contentFile.toString();
        Utility_RowIterator ur = new Utility_RowIterator(nameFile);
        system.debug(clientMap);
        
        while(ur.hasNext()){
          String[] inputvalues = new String[]{};
          //inputvalues = ur.Next().split(',');
          inputvalues = parseCSV.parseCSV(ur.Next());
          system.debug(inputvalues);
          try{
          inputvalues[0] = inputvalues[0].replaceAll('"', '');
          inputvalues[1] = inputvalues[1].replaceAll('"', '');
          inputvalues[2] = inputvalues[2].replaceAll('"', '');
          inputvalues[3] = inputvalues[3].replaceAll('"', '');
          inputvalues[8] = inputvalues[8].replaceAll('"', '');
          inputvalues[9] = inputvalues[9].replaceAll('"', '');
          inputvalues[10] = inputvalues[10].replaceAll('"', '');
          inputvalues[11] = inputvalues[11].replaceAll('"', '');
          inputvalues[12] = inputvalues[12].replaceAll('"', '');
          
          if(inputvalues[0]!='PUNBR'){
            
            caClaim__c c = new caClaim__c();
            
            if(clientMap.get(inputvalues[0]+inputvalues[1].toLowerCase().trim())==null){
                Claims_Audit_Client__c nCac = new Claims_Audit_Client__c(name=inputvalues[0],underwriter__c=inputvalues[0], Group_Number__c=inputvalues[1],Active__c=true, Percent_to_pull__c =0);
                nCacList.add(nCac);
                c.Client__r = nCac;
            }else{
                
                c.Client__r = clientMap.get(inputvalues[0]+inputvalues[1].toLowerCase().trim());
                c.Client__c = clientMap.get(inputvalues[0]+inputvalues[1].toLowerCase().trim()).id;
                
            }    
                
            c.name = inputvalues[2];
            c.claim_number__c = inputvalues[2];
            
            if(processorMap.get(inputvalues[3].toLowerCase().trim()) != null){
                c.Claims_Processor_Crosswalk__r = processorMap.get(inputvalues[3].toLowerCase().trim());
                c.Claims_Processor_Crosswalk__c = processorMap.get(inputvalues[3].toLowerCase().trim()).id;
            }else{
                c.Claims_Processor_Crosswalk__r = processorMap.get('auditmanager');
                c.Claims_Processor_Crosswalk__c = processorMap.get('auditmanager').id;
            }
            
            string[] foo;
            if(inputvalues[6]!=''){
                foo = inputvalues[6].split('/');
                c.Claim_Received_Date__c= date.valueof(foo[2]+'-'+foo[0]+'-'+foo[1]);
                
            }else{
                c.Claim_Received_Date__c= null;
            }
            
            if(inputvalues[5]!=''){
                foo = inputvalues[5].split('/');
                c.Date_Paid__c = date.valueof(foo[2]+'-'+foo[0]+'-'+foo[1]);
                
            }else{
                c.Date_Paid__c =null;
            }
            
            if(inputvalues[4]!=''){
                foo = inputvalues[4].split('/');
                c.Claim_Processed_Date__c= date.valueof(foo[2]+'-'+foo[0]+'-'+foo[1]);
                
            }else{
                c.Claim_Processed_Date__c= null;
            }
          
            inputvalues[7] = inputvalues[7].replaceAll('"', '');  
            c.Total_Charge__c = decimal.valueof(inputvalues[7].replaceall(',',''));
            c.Product__c      = inputvalues[8];
            c.Form_Type__c    = inputvalues[9];
            c.Source_Type__c  = inputvalues[10];
            clmsToUpload.add(c);
          
          }
            
          }catch(exception e){
                system.debug(inputvalues);
                system.debug(e.getMessage());
          }
          
        }   
        
        total = clmsToUpload.size();
        if(insertRecords==null){
            insertRecords=false;
        }
        saveClaimsFileRandomSelection();
        
        }
    
        return null;
    }
    
    public void setFileContent(){system.debug(nameFile);}
    
   
}