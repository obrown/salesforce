public with sharing class parseAhEligibilityResponse{

    employee employee;
    hcc coverage;
    dependent[] dependents;
    String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
    String ns2 = 'http://alight.com/hro/benefits/healthDesignPlus/xsd';
    String ns1 = 'http://alight.com/hro/benefits/cm/xsd/v2_0'; 
    string xsi = 'http://www.w3.org/2001/XMLSchema-instance';
    dom.XmlNode envelope;
    public string responseDescription;
    
    public class dependent{
        public string firstName;
        public string lastName;
        
    }
    
    public class hcc {
        public string planLongDescriptionText;
        public string optionLongDescriptionText;
    }
    
    public class employee{
        public string firstName;
        public string lastName;
        public string employeeId;
        public string birthDate;
        public string gender;
        public string employmentStatusCode;
    }
    
    public void setReader(dom.Document doc){
        envelope =doc.getRootElement();
    }
    
    public parseAhEligibilityResponse(dom.Document doc){
        envelope =doc.getRootElement();
    
    }
    
    public employee getEmployee(){
        return employee;
    }
    
    public hcc getCoverage(){
        return coverage;
    }
    
    public dependent[] getdependents(){
        return dependents;
    }
    
    public string getResponseDescription(){
        string returnValue ='';
        try{
            returnValue =envelope.getChildElement('Body', soapNS).getChildElement('getMedicalCoverageResponse', ns2).getChildElement('responseHeader', ns2).getChildElement('responseDescription', ns1).getText();
        }catch(exception e){
            system.debug(e.getMessage()+' '+e.getLineNumber());
        }
        return returnValue;
       
    }
    
    public boolean parse(){
    
        responseDescription = getResponseDescription();
        if(responseDescription!='SUCCESS'){
            return false;
        }
        
        dom.XmlNode responseData = envelope.getChildElement('Body', soapNS).getChildElement('getMedicalCoverageResponse', ns2).getChildElement('responseData', ns2);
        
        if(responseData==null){
            return false;
        }
        
        boolean pe = parseEmployee(responseData );
        boolean phb = parsehealthBenefitsCurrentCoverage(responseData );
        boolean pdep = parseDependents(responseData );
        
        system.debug('employee '+ employee);
        system.debug('dependents '+ dependents); 
        system.debug('coverage '+ coverage);
        return true;
    }
    
    boolean parseEmployee(dom.XmlNode responseData ){
        
        dom.XmlNode personBasicData = responseData.getChildElement('personBasicData', ns2);
        
        employee = new employee();
        employee.firstName = personBasicData.getChildElement('firstName', ns2).getText();
        employee.lastName = personBasicData.getChildElement('lastName', ns2).getText();
        employee.employeeId= personBasicData.getChildElement('employeeId', ns2).getText();
        employee.employmentStatusCode = personBasicData.getChildElement('employmentStatusCode', ns2).getText();
        employee.birthDate= personBasicData.getChildElement('birthDate', ns2).getText();
        employee.gender= personBasicData.getChildElement('sex', ns2).getText();
        
        return true;
    }
    
    boolean parseDependents(dom.XmlNode responseData){
        dom.XmlNode depNodeArray = responseData.getChildElement('healthBenefitsCurrentCoverage', ns2).getChildElement('coveredDependentArray', ns2);
        
        dependents = new dependent[]{};
        dom.XmlNode[] coveredDependentArray = new dom.XmlNode[]{};
        
        try{
        coveredDependentArray= depNodeArray.getChildren();
        }catch(exception e){
            return false;
        }
         
        for(dom.XmlNode node : coveredDependentArray){
          
          dependent d = new dependent();
          d.firstName = node.getChildElement('firstName', ns2).getText();
          d.lastName =  node.getChildElement('lastName', ns2).getText();
          dependents.add(D);
          
        }
        
         
        return true;
    }
    
    boolean parseHealthBenefitsCurrentCoverage(dom.XmlNode responseData){
        dom.XmlNode hccDom = responseData.getChildElement('healthBenefitsCurrentCoverage', ns2);
        coverage= new hcc();
        
        if(hccDom.getChildElements().size()==0){return false;}
        
        coverage.planLongDescriptionText= hccDom.getChildElement('planLongDescriptionText', ns2).getText();
        coverage.optionLongDescriptionText= hccDom.getChildElement('optionLongDescriptionText', ns2).getText();
        
        
        return true;
    }
}