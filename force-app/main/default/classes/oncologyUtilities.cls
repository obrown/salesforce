public class oncologyUtilities{

    public static string formatSubdirectory(oncology__c o){
        string subDirectoryPrefix = o.patient_last_name__c+', '+o.patient_first_name__c+'_'+o.patient_dob__c.mid(5,2)+'_'+o.patient_dob__c.right(2)+'_'+o.patient_dob__c.left(4);
        subDirectoryPrefix= String.escapeSingleQuotes(subDirectoryPrefix);
        string result =PatientCase__c.getInstance().fileDirectory__c+'/'+subDirectoryPrefix;
        system.debug(result);
        return result;
    }

}