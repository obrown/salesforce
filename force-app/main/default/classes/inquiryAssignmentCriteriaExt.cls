public with sharing class inquiryAssignmentCriteriaExt {
    
    selectOption[] fields;
    public inquiryAssignmentCriteriaExt(){}
    string recordType;
    public inquiryAssignmentCriteriaExt(ApexPages.StandardController controller) {
        
        Claims_Assignment_Criteria__c  cac = (Claims_Assignment_Criteria__c )controller.getRecord();
        string parentid = cac.Claims_Assignment_Rule__c;
        
        if(parentId!=''&&parentId!=null){
           recordType = [select recordType.Name from Claims_Assignment_Rule__c where id = :parentId].recordType.Name;  
        }
        
    }
    
    public selectOption[] getFields(){
        
        if(recordType==null){
            return new selectOption[]{};
        }
        
        if(recordType=='EWT'){
            fields = getEwtFields();
        }else{
            fields = getInquiryFields();
        }    
        
        return fields;
        
    }
    
    selectOption[] getInquiryFields(){
        if(fields!=null){return fields;}
        fields = new selectOption[]{};
        Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Customer_Care_Inquiry__c.fields.getMap();
        for(Schema.SObjectField field : fieldMap.Values()){
            schema.describefieldresult dfield = field.getDescribe();
            dfield.getname();
            dfield.getLabel();
            selectOption f = new selectOption(dfield.getname(), dfield.getLabel());
            fields.add(f);
        
        }
        
        return fields;
    }
    
    selectOption[] getEwtFields(){
        if(fields!=null){return fields;}
        fields = new selectOption[]{};
        
        fields.add(new selectOption('', ' '));
        fields.add(new selectOption('Document_Type__c', 'Document Type'));
        fields.add(new selectOption('Underwriter__c', 'Underwriter'));
        fields.add(new selectOption('Work_Department__c', 'Work Department'));
        fields.add(new selectOption('Work_Task_Reason__c', 'Work Task Reason'));
        fields.add(new selectOption('Work_Task_Status__c', 'Work Task Status'));
        fields.add(new selectOption('Work_Task__c', 'Work Task'));
        
        return fields;
        
    }
    
}