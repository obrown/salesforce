public with sharing class searchLifestart{

    public final string st {get;set;}
    public Lifestart__c[] lstLS {get; private set;}
    
    public List<SelectOption> client {get; set;}
    public set<string> eSSNKeys {get; private set;}
    public map<string, hpPatientSearchStruct.PatSearchResultLogs[]> resultsMap {get; set;}
    
    public boolean hpSearchfound {get; private set;}
    public boolean hpSearchRun {get; private set;}
    map<string, string> clientMap {get; private set;}
    
    public integer hpsearchCount {get; private set;}
    
    public searchLifestart(){
        lstLS = new Lifestart__c[]{};
        
        st = ApexPages.currentPage().getParameters().get('st');
        
        if(st!=null){
            search();
        }
        loadClientMap();
    }
    
    void search(){
        set<id> dups = new set<id>();
        string[] foo = new string[]{};
        
        if(st.contains(' ')){
            for(string s : st.split(' ')){
                foo.add(s.toLowerCase());
            }
        }
        
        foo.add(st.toLowerCase());
        string ls;
        for(Lifestart__c l : [select id, eSSN__c, patient_date_of_birth__c, Est_Gestational_Age__c, Estimated_Delivery_Date__c, name, Patient_First_Name__c, Patient_Last_Name__c, createdDate, createdby.name, createdby.id from Lifestart__c where parent__c = null]){
            
            ls = l.Patient_First_Name__c +';'+l.Patient_Last_Name__c+';'+l.eSSn__c;
            ls = ls.toLowerCase();
            
            for(string s : foo){
                
                if(!dups.contains(l.id) && ls.indexOf(s)>-1){
                    dups.add(l.id);
                    lstLS.add(l);
                    system.debug(l.id);
                }
                
            }
            
        }
    
    }
    
    void loadClientMap(){
    
        if(clientMap==null){
            
            clientMap = new map<string, string>();
            client = new List<SelectOption>();
            client.add(new SelectOption('', ''));
            
            for(Client__c c : [select underwriter__c,active__c, name from Client__c order by name asc]){
                clientMap.put(c.underwriter__c, c.name);
                if(c.active__c){
                    if(c.underwriter__c==null){c.underwriter__c='';}
                    client.add(new SelectOption(c.underwriter__c, c.name));
                }
            }
        }
    
    }  
    
    public pageReference selectEmployeeAF(){
    
        string eSSN = ApexPages.currentPage().getParameters().get('pEssn');
        string pSeq  = ApexPages.currentPage().getParameters().get('pSeq');
        string grp  = ApexPages.currentPage().getParameters().get('pgrp');
        string uw   = ApexPages.currentPage().getParameters().get('puw');
        string pefn = ApexPages.currentPage().getParameters().get('pefn');
        string peln = ApexPages.currentPage().getParameters().get('peln');
        pefn = pefn.toLowerCase();
        pefn = pefn.capitalize();
        peln = peln.toLowerCase();
        peln = peln.capitalize();
        
        
        lifeStart__c ls;
        
        
        string selectedEmp = uw+''+pSeq+''+eSSN;
        
        for(hpPatientSearchStruct.PatSearchResultLogs s : resultsMap.get(eSSN)){
            
            if(selectedEmp==s.Underwriter+''+s.Sequence+''+s.SSN){
                ls = new lifestart__c();
                ls.client__c = setClient(uw);
                ls.Patient_First_Name__c = s.PatFirstName.toLowerCase();
                ls.Patient_First_Name__c = ls.Patient_First_Name__c.capitalize();
                
                string fooLastName = s.PatLastName.toLowerCase();
                fooLastName = fooLastName.capitalize();
                
                ls.Patient_Last_Name__c = fooLastname;
                if(s.PatDateOfBirth!=null && s.PatDateOfBirth.length()==10){
                    ls.Patient_Date_of_Birth__c = s.PatDateOfBirth.right(4)+'-'+s.PatDateOfBirth.left(2)+'-'+s.PatDateOfBirth.mid(3,2);
                }
                
                if(s.EmpDateOfBirth!=null && s.EmpDateOfBirth.length()==10){
                    ls.Employee_DOB__c = s.EmpDateOfBirth.right(4)+'-'+s.EmpDateOfBirth.left(2)+'-'+s.EmpDateOfBirth.mid(3,2);
                }
                
                ls.Employee_First_Name__c = s.EmpFirstName.toLowerCase();
                ls.Employee_First_Name__c = ls.Employee_First_Name__c.capitalize();
                ls.Employee_Last_Name__c = s.EmpLastName.toLowerCase();
                ls.Employee_Last_Name__c = ls.Employee_Last_Name__c.capitalize();
                ls.eSSN__c = s.SSN;
                ls.Sequence__c = s.Sequence;
                
                if(s.EmpDateOfBirth==s.PatDateOfBirth){
                    ls.Patient_is_Employee__c = true;
                }
                
                ls.Status__c = 'Open';
                ls.Status_Reason__c= 'Newly Identified';
                ls.Eligibility_Verified__c = date.today();
            }
            
        }
        
        if(ls!=null){
        
            insert ls;
        
        }
        
        return new pageReference('/apex/lifestartedit?id='+ls.id);
        //return null;
    }
    
    public void searchHealthPac(){
        
        string uw   = ApexPages.currentPage().getParameters().get('uw');
        string eSSN = ApexPages.currentPage().getParameters().get('essn');
        string cert = ApexPages.currentPage().getParameters().get('cert');  
        string efn  = ApexPages.currentPage().getParameters().get('efn'); 
        string eln  = ApexPages.currentPage().getParameters().get('eln'); 
        
        resultsMap = new map<string, hpPatientSearchStruct.PatSearchResultLogs[]>();
        hpPatientSearchStruct.PatSearchResultLogs[] searchResults;
        hpSearchRun = true;
        hpSearchfound = false;
        hpsearchCount=0;
        eSSNKeys = new set<string>();
        
        hpPatientSearchStruct foo = hp_patientSearch.searchHP(uw, essn, cert, efn, eln);
        
        //foo.ResultCode = '1';
        if(foo.ResultCode=='1'){
            //no results found
            return;
        }
        
        if(foo == null || foo.PatSearchResultLogs == null){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error has occured'));
            return;
        }
        
        for(hpPatientSearchStruct.PatSearchResultLogs s : foo.PatSearchResultLogs ){
            hpsearchCount++;
            eSSNKeys.add(s.SSN);
            
            if(resultsMap.keyset().contains(s.SSN)){
                searchResults = resultsMap.get(s.SSN);
            }else{
                 searchResults = new hpPatientSearchStruct.PatSearchResultLogs[]{};
            }
            
            searchResults.add(new hpPatientSearchStruct.PatSearchResultLogs(s.Underwriter,
                                                         s.Grp,
                                                         s.SSN,
                                                         s.EmpFirstName ,
                                                         s.EmpLastName ,
                                                         s.EmpDateOfBirth ,
                                                         s.Sequence ,
                                                         s.PatFirstName ,
                                                         s.PatLastName ,
                                                         s.PatDateOfBirth ,
                                                         decodeRelationship(s.Relationship),
                                                         null,
                                                         decodeStatus(s.Status),
                                                         setClient(s.Underwriter))); 
        
             
            resultsMap.put(s.SSN, searchResults);
    
        }
        
        if(resultsMap.keyset().size()>0){
            hpSearchfound=true;
        }else{
            hpSearchfound=false;
        }  
    }
    
    string decodeRelationship(string rel){
        
        if(rel=='1' || rel=='01'){
            return 'Employee';
        }else if(rel=='2' || rel=='02'){
            return 'Spouse';
        }else if(rel=='3' || rel=='03'){
            return 'Son';
        }else if(rel=='4' || rel=='04'){
            return 'Daughter';
        }else if(rel=='5' || rel=='05'){
            return 'Other';
        }
        
        return 'NA';
        
    }
    
    string decodeStatus(string status){
    
        if(status=='A'){
            return 'Active';
        }else if(status=='T'){
            return 'Termed';
        }
    
        return 'NA';
    
    }
    
    string setClient(string uw){
        loadClientMap();
        string foo = '';
        foo= clientMap.get(uw);
        if(foo==null){
            foo = uw;
        }
        return foo;
        
    }

}