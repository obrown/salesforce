global class intakeClassSchedule implements Schedulable {
    global void execute(SchedulableContext go){
        batchProcess bp = new batchProcess();

        bp.runIntakeOpen();
          //bp.tallyMemberCOEcases();
        bp.surveyMonkey();
        bp.transplantEligcheck();

        populationHealthBatchEligibility pbe = new populationHealthBatchEligibility();

        pbe.checkEligibility();

        populationHealthBatchCMEligibility pbcme = new populationHealthBatchCMEligibility();

        pbcme.checkEligibility();
    }
}