@isTest(SeeAllData=true)
private class onlineIntakeControllerTest 
{

    static testMethod void oic() 
    {
        onlineIntakeController oic = new onlineIntakeController();
        oic.empName = 'none';
        oic.checkEmpName(); 
        oic.empName = 'McKesson';    
        oic.checkEmpName(); 
        
        Patient_Case__c pc = new Patient_Case__c();
        pc.BID__c = '123456';
        pc.Language_Spoken__c = 'English';
        pc.Initial_Call_Discussion__c = 'The ICD';
        pc.Relationship_to_the_Insured__c = 'Spouse';
        
        pc.Employee_first_name__c = 'John';
        pc.Employee_last_name__c = 'Smith';
        pc.employee_dobe__c = '1980-01-01';
        pc.Employee_Gender__c = 'Male';
        pc.employee_ssn__c = '123456789';
        
        pc.Patient_First_Name__c = 'Jane';
        pc.Patient_last_name__c = 'Smith';
        pc.Patient_dobe__c = '1981-01-01';
        pc.Patient_Gender__c = 'Female';
        pc.Patient_ssn__c = '987654321';
        
        pc.Same_as_Employee_Address__c = true;
        
        pc.employee_street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85297';
        
        pc.Employee_Home_Phone__c = '(480) 555-1212';
        pc.Employee_Mobile__c = '(480) 555-1212';
        pc.Employee_Work_Phone__c = '(480) 555-1212';
        
        pc.Patient_Home_Phone__c = '(480) 555-1212';
        pc.Patient_Mobile__c = '(480) 555-1212';
        pc.Patient_Work_Phone__c = '(480) 555-1212';
        
        
        
        string jsonString = JSON.serialize(pc);
        
        jsonString = jsonString.left(jsonString.length()-1); 
        string bariatricjsonString = jsonString;
        
        
        jsonString += ',"caseType":"Back/Neck","empPrimary":"(480) 555-1212","empPrimaryType":"Work"}'; 
        bariatricjsonString += ',"caseType":"Bariatric","empPrimary":"(480) 555-1212","empPrimaryType":"Work"}'; 
        oic.jsonString = jsonString;
        oic.mySubmit();
        
        oic.jsonString = bariatricjsonString;
        oic.mySubmit();        
    }
}