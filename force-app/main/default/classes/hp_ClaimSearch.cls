public with sharing class hp_ClaimSearch extends hp_callWebApp{
    
    public hpClaimSearchStruct claimSearchResult = new hpClaimSearchStruct();
    
    public static hpClaimSearchStruct search(string searchStr){
        
        hp_ClaimSearch search = new hp_ClaimSearch ();
        search.endpoint='claim/claimNumberSearch';
        search.search.add('searchStr:'+searchStr);
        search.jsonBody = search.jb.eligSearch(search.search);
        string response;
        
        if(Test.IsRunningTest()){
           response = '{"ResultCode":"0","ErrorMsg":"","Status":"A","Payee":"P","Pricer":"A","HPPaidAmount":"131.51","ClaimAllowedAmount":"131.51","COBFlag":"N","HPPureStatus":"A","HPCOBOverride":"N","HPPlanCode":"BV1PXOPP","Relationship":"1","FIAFlag":"","HPSsnSeq":"26972897300","ClmFlagResult":"","NetworkFlag":"I","OONFlag":"N","EligibilityInfo":[{"Underwriter":"016","Group":"BV1","SSN":"         ","Sequence":"00","LastName":"Newman         ","FirstName":"Marla          ","MiddleInitial":"J","Suffix":"      ","Prefix":"","Address1":"114 N Main St Apt C           ","Address2":"PO Box 603                    ","City":"Arlington      ","State":"OH","Zip":"45814    ","HomePhone":"4197225473","WorkPhone":"          ","Gender":"F","DateOfBirth":"19690529","MaritalStatus":"S"}],"ServicelineInfo":null}';
        }else{   
           response = search.callWebApp(null);
        }
        
        if(response ==null ){
            search.claimSearchResult.setResultCode('2');
            search.claimSearchResult.setErrorMsg('Response was null');
            return search.claimSearchResult;
        }
        
        response = response.replaceAll('\"Group\"','\"Grp\"');
        
        search.claimSearchResult= (hpClaimSearchStruct)JSON.deserialize(response , hpClaimSearchStruct.class); 
        
        if(search.claimSearchResult.ResultCode=='2'){
            return search.claimSearchResult;
        }
        system.debug(search.claimSearchResult);
        hpClaimSearchStruct.EligibilityInfo[] empResults = new hpClaimSearchStruct.EligibilityInfo[]{};
        
        boolean isHDPGrpMember = UserUtil.getIsHDPGrpMember();
        for(hpClaimSearchStruct.EligibilityInfo clm : search.claimSearchResult.EligibilityInfo){
            if(!isHDPGrpMember || (clm.Grp != 'HDP' && clm.Grp != '000')){
                empResults.add(clm);
            }
        }
        
        search.claimSearchResult.setClaimNumber(searchStr);
        search.claimSearchResult.setEligibilityInfo(empResults);
        
        if(empResults.isEmpty()){
           search.claimSearchResult.setResultCode('1'); 
        }
        
        
        return search.claimSearchResult;
        
    }
 
    /*
       if(Test.IsRunningTest()){
           result = '{"ResultCode":"2","ErrorMsg":"","Results":[{"Id":0,"PatientType":"employee","Gender":"F","Ssn":"999999999","EssnHolder":{"String":"","Valid":false},"Essn":"","Dob":"19690605","Firstname":"JANE","Lastname":"DOE","Address1":"123 E Main St","Address2":"","City":"FORT SMITH","State":"AR","Zip":"72903","Phone":"","EffectiveDate":"20180101","PaidThroughDate":"","PaidThroughDateHolder":{"String":"","Valid":true},"TermDate":"","CoverageType":"MM","EmploymentStatusHolder":{"String":"","Valid":true},"EmploymentStatus":"","HireDateHolder":{"String":"19890710","Valid":true},"HireDate":"19890710","PlanCode":"","Bid":"12345678W","PatientId":0,"FamilyStatusHolder":{"String":"","Valid":true},"FamilyStatus":"","Relationship":"18","Plancode":"BLUE ADVANTAGE - HRA 1750","CoverageLevel":"EMP"}]}';
        }else{   
            result = callWebApp(null);
        }
        
    }    
    */
    
}