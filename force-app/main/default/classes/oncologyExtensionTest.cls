@isTest()
private class oncologyExtensionTest{
    
      static testMethod void ExtensionTest(){
         
         oncologyHeaderController ohc = new oncologyHeaderController();
         
         ohc.clientLogo ='UNIT TEST';
         
         OncologyTestData t = new OncologyTestData();
         t.loadData('Facebook','City of Hope');
         
         oncology__c oncology = t.oncology;
         oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
         system.assert(oncology.id!=null);
         
         Client_facility__c cf = t.addClientFacility(oncology.client__c, oncology.Procedure__c, t.facility.id);
         ApexPages.StandardController controller = new ApexPages.StandardController(oncology);
         oncologyExtension oExt= new oncologyExtension(controller);
         
         oExt.getclientOpts();
         oExt.getopServicesRelatedToDiagCancerLabel();
         oExt.getDiagnosticRadiologyReviewLabel();
         oExt.getDiagnosticRadiologyReviewLabel();
         oExt.getAdditionalPathologyReviewLabel();
         oExt.getAllothernecessaryevaluationLabel();
         oExt.getbtc();
         oExt.gettimezones();
         oExt.getproposedServiceType();
         
         oExt.addPatient();
         oExt.nOncology.client__c = oExt.o.client__c;
         oExt.nOncology = oExt.o;
         oExt.eModalDob = '01/01/1980';
         oExt.pModalDob = '01/01/1980';
         oExt.saveOncology();
         
         controller = new ApexPages.StandardController(oExt.o);
         oExt= new oncologyExtension(controller);
         
         oExt.getcarrierOpts();
         Carrier__c  carrier = t.addCarrier();
         oExt.o.carrier__c = carrier.id;
         client_carrier__c cc = new client_carrier__c(Carrier__c=Carrier.id, client__c=oncology.client__c);
         insert cc;
         oExt.getcarrierOpts();
         
         
         oExt.newElig();
         
         oExt.elig.oncology__c=null;
         oExt.saveElig();
         system.assert(oExt.elig.id==null);
         
         oExt.newElig();
         oExt.saveElig();
         system.assert(oExt.elig.id!=null);
         
         oExt.loadEligChecks();
         
         ApexPages.CurrentPage().getParameters().put('eid', oExt.elig.id);
         oExt.loadeligRecord();
         
         oExt.cancelElig();
         
         oExt.getThePhysician();
         
         oExt.Physician.Associated_Facilities__c='na';
         oExt.Physician.City__c='Mesa';
         oExt.Physician.Credentials__c='MD';
         oExt.Physician.Facility_PCP__c=false;
         oExt.Physician.Facility_Reported_Physician__c=false;
         oExt.Physician.Fax_Number__c='480-555-5555';
         oExt.Physician.First_Name__c='James';
         oExt.Physician.Last_Name__c='Jones';
         oExt.Physician.Local_PCP__c=true;
         oExt.Physician.Network_Status__c='In Network';
         oExt.Physician.Network_Status_Verification_Date__c=system.today()+5;
         oExt.Physician.Phone_Number__c='480-555-5555';
         oExt.Physician.Provider_Type__c='COE Managing Physician';
         oExt.Physician.Specialty__c='Medical Oncologist';
         oExt.Physician.State__c='AZ';
         oExt.Physician.Street_Address__c='2500 S Power Rd';
         oExt.Physician.Zip_Code__c='85205';
         
         oExt.savePhysician();
         system.assert(oExt.Physician.id!=null);
         
         ApexPages.CurrentPage().getParameters().put('physId', oExt.Physician.id);
         
         oExt.getThePhysician();
         oExt.cancelPhysician();
         
         oExt.o = t.ReadytoRefer(oExt.o);
         oExt.o.client_facility__c = cf.id;
         oExt.mySave();
         
         oExt.o = t.setFormulafield(oExt.o, 'hasLocalPCP__c', 1);
         oExt.o = t.setFormulafield(oExt.o, 'client_name__c', 'Facebook');
         
         oExt.referCase();
         oExt.updateFacilityData();
         
         oExt.sendAuthorizationEmail();
         
         oExt.hdpNurseChange();
         oExt.gethotelOpts();
         
         ApexPages.CurrentPage().getParameters().put('activeTab', '');
         oExt.setActiveTab();
         
         Oncology_Clinical_Facility__c ocf = new Oncology_Clinical_Facility__c();
         ocf.determination_date__c=date.today();
         ocf.parentId__c= oExt.o.id;
         ocf.Determination_Outcome__c='Program Eligibility Met';
         ocf.Determination_Outcome_Reason__c = 'Clinical - Intracardiac';
         ocf.Treatment__c='Previous Treatment';
         ocf.Clinical_Notes__c='Test';
         ocf.Date_of_Diagnosis__c=date.today();
         ocf.Primary_Diagnosis__c='Test';
         ocf.Physician_First_Name_Last_Name__c = 'John Smith';
         ocf.Physician_Specialty__c ='Somethnig';
         ocf.Physician_Country__c = 'US';
         ocf.Physician_Credentials__c='MD';
         ocf.Physician_Phone_Number__c='(480) 555-1212';
         ocf.Physician_Fax_Number__c='(480) 555-1210';
         ocf.Physician_Street_Address__c='123 E Main St';
         ocf.Physician_City__c='Mesa';
         ocf.Physician_State__c='AZ';
         ocf.Physician_Zip_Code__c='85212';
         ocf.Associated_Facilities__c='Test';
         
         ocf.Patient_Last_Name__c ='Test';
         ocf.Patient_DOB__c='1980-02-01';
         ocf.Patient_Insurance_Id__c='123456';
         ocf.Patient_State__c ='AZ';
         
         insert ocf;
         
         Oncology_Clinical_Facility__c pocOcf = new Oncology_Clinical_Facility__c();
         pocOcf.parentId__c=oExt.o.id;
         pocOcf.Patient_Last_Name__c ='Test';
         pocOcf.Patient_DOB__c='1980-02-01';
         pocOcf.Patient_Insurance_Id__c='123456';
         pocOcf.Patient_State__c ='AZ';
         
         pocOcf.Physician_First_Name_Last_Name__c = 'John Smith';
         pocOcf.Physician_Specialty__c ='Somethnig';
         pocOcf.Physician_Country__c = 'US';
         pocOcf.Physician_Credentials__c='MD';
         pocOcf.Physician_Phone_Number__c='(480) 555-1212';
         pocOcf.Physician_Fax_Number__c='(480) 555-1210';
         pocOcf.Physician_Street_Address__c='123 E Main St';
         pocOcf.Physician_City__c='Mesa';
         pocOcf.Physician_State__c='AZ';
         pocOcf.Physician_Zip_Code__c='85212';
         pocOcf.Associated_Facilities__c='Test';
         
         pocOcf.plan_of_care_submission_date__c = date.today();
         pocOcf.Consultation_Types__c= 'Verifying with facility';
         pocOcf.Proposed_Service_Type__c= 'Test';
         
         insert pocOcf;
         
         oExt.pfn = oExt.o.patient_first_name__c;
         oExt.pln = oExt.o.patient_last_name__c;
         oExt.pSearchDob='';
         oExt.searchOncology();
         oExt.newSearch();
         oExt.cancel();
         oExt.cancelOncology();
         oExt.saveNeedsAssessment();
         oExt.submitForEligibility();
         //Hotel__c hotel = t.addHotel();
         //ecen_hotel__c ch = new ecen_hotel__c(client_facility__c=oncology.client_facility__c);
         //insert ch;
         
         
      }   
}