public class searchBaritric extends searchEncrypted{
    
    public bariatric__c[] bariatricList {get; private set;}
    
    public searchBaritric(){
        
        string searchBy = ApexPages.CurrentPage().getParameters().get('st');
        
        if(searchBy==null){
            return;
        }
        
        setSearchTerm(searchBy);
    
        string[] searchFields = new string[]{};
        searchFields.add('Employee_First_Name__c');
        searchFields.add('Employee_Last_Name__c');
        searchFields.add('Employee_DOB__c');
        searchFields.add('Patient_First_Name__c');
        searchFields.add('Patient_Last_Name__c');
        searchFields.add('name');
        searchFields.add('Patient_SSN__c');
        searchFields.add('Employee_SSN__c');
        searchFields.add('BID__c');
        searchFields.add('Patient_Mobile_Phone__c');
        searchFields.add('Patient_Home_Phone__c');
        searchFields.add('Employee_Home_Phone__c');
        searchFields.add('Employee_Mobile_Phone__c');
        searchFields.add('Employee_Zip_Code__c');
        searchFields.add('Status__c');
        searchFields.add('Status_Reason__c');
        searchFields.add('Stage__c');
        searchFields.add('createddate');
        searchFields.add('createdby.name');
        searchFields.add('createdby.id');
        searchFields.add('lastmodifiedby.name');
        
        string searchString = '';
        
        for(String s : searchFields){
            searchString = searchString+','+s;
            system.debug(s);
        }
        
        
        
        searchString = 'Select ' + searchString.right(searchString.length()-1) + ' from Bariatric__c';
        bariatricList = search(searchString, searchFields);
        
        
    }
    

}