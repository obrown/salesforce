@RestResource(urlMapping='/providerPortal/v1/attachmentlist')
global with sharing class ProviderPortalAttachmentList {
    
    global class Filelist {
        File[] Filelist;
        string Message;
        string ResultCode;
        
    }
    class File {
        string FileName;
        string LastModified;
        public File(string fileName, string LastModified){
            this.fileName =fileName;
            this.LastModified =LastModified;
        }
    }
    @HttpPost
    global static string doPost() {
        return '';
    }
    
    @HttpGet
    global static Filelist doGet() {
        string caseNumber = RestContext.request.headers.get('Casenumber');
        if(!caseNumber.contains('PC-')){
            caseNumber='PC-'+caseNumber;
        }
        patient_case__c pc = [select patient_last_name__c, patient_first_name__c, patient_dobe__c, name from patient_case__c where name =:caseNumber ];
        string coeFileNamePrefix = pc.patient_last_name__c+', '+pc.patient_first_name__c+'_'+pc.patient_dobe__c.mid(5,2)+'_'+pc.patient_dobe__c.right(2)+'_'+pc.patient_dobe__c.left(4)+'/'+pc.name;
        string coeFileDirectory =PatientCase__c.getInstance().fileDirectory__c+'/'+coeFileNamePrefix;
        File[] Files = new File[]{};
        
        map<string, File> fm = new map<string, file>();
        
        for(ftpAttachment__c  a : [Select  fileName__c,LastModifiedDate from ftpAttachment__c where patient_case__c != null and patient_case__r.name = :caseNumber order by createdDate desc]){
            //Files.add(new File(a.fileName__c,''));//string.valueof(a.LastModifiedDate)
            fm.put(a.fileName__c, new File(a.fileName__c,''));
        }
        
        for(Attachment a : [select name, body, contentType from attachment where parentID =:pc.id and (name like '%ID Card.pdf' or name like '%_Referral.pdf')]){
            //Files.add(new File(a.name,''));
            fm.put(a.name, new File(a.Name,''));
        }
        
        for(string f : fm.keyset()){
            files.add(fm.get(f));
        }
        
        Filelist Filelist = new Filelist();
        Filelist.Filelist = Files;
        Filelist.Message= 'Success';
        Filelist.ResultCode = '0';
        system.debug(JSON.Serialize(Filelist));
        return Filelist;
    }

}