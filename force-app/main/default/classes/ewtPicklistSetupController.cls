public with sharing class ewtPicklistSetupController {
    
    public ewtSelectlist esl {get; set;}
    
    public string workDepartmentId { get; set; }
    public string workTaskReasonId { get; set; }
    public string workTaskId { get; set; }
    public string documentTypeId {get; set;}
    public string closedReasonId {get; set;}
    public string workTaskStatusId {get; set;}
    
    public string workTaskStatusName {get; set;}
    public string closedReasonName {get; set;}
    public string documentTypeName {get; set;}
    public string workTaskName {get; set;}
    public string workTaskReasonName {get; set;}
    public string workDepartmentName {get; set;}
    
    public EWT_Document_Type__c documentType {get; set;}
    map<id, EWT_Document_Type__c> documentTypeMap = new map<id, EWT_Document_Type__c>();
    
    public boolean error {get; private set;}
    
    public ewtPicklistSetupController(){
        esl = new ewtSelectlist();
        for(EWT_Document_Type__c edt: [select Description__c,Document_Type__c,Folder_Location__c,HP_Document_Type__c from EWT_Document_Type__c]){
            documentTypeMap.put(edt.id, edt);
            
        }
        esl.getTheworkTaskStatus();
    }
    
    public void setDocumentTypeToUpdate(){
        string i = ApexPages.CurrentPage().getParameters().get('documentTypeId');
        
        try{
            id.valueof(i);
            documentTypeId = i;
            documentType = documentTypeMap.get(documentTypeId);
        }catch(exception e){
            documentTypeId ='';
            if(i==null || i==''){
                documentType = new EWT_Document_Type__c();
            }
        }     
         
    }
    
    public void setClosedReasonToUpdate(){
        try{
            closedReasonId = id.valueof(ApexPages.CurrentPage().getParameters().get('closedReasonId'));
            closedReasonName = ApexPages.CurrentPage().getParameters().get('closedReasonName');
        }catch(exception e){
            closedReasonName ='';
            closedReasonId =null;
        } 
         
    }

    public void setworkTaskStatusToUpdate(){
        try{
            workTaskStatusId = id.valueof(ApexPages.CurrentPage().getParameters().get('workTaskStatusId'));
            workTaskStatusName = ApexPages.CurrentPage().getParameters().get('workTaskStatusName');
        }catch(exception e){
            workTaskStatusName ='';
            workTaskStatusId =null;
        } 
         
    }
    
    public void setWorkTaskReasonToUpdate(){
        try{
            workTaskReasonId  = id.valueof(ApexPages.CurrentPage().getParameters().get('workTaskReasonId'));
            workTaskReasonName = ApexPages.CurrentPage().getParameters().get('workTaskReasonName');
        }catch(exception e){
            system.debug(e.getMessage());
            workTaskReasonName='';
            workTaskReasonId =null;
        } 
         
    }
    
    public void setWorkDepartmentToUpdate(){
        try{
            workDepartmentId  = id.valueof(ApexPages.CurrentPage().getParameters().get('workDepartmentId'));
            workDepartmentName = ApexPages.CurrentPage().getParameters().get('workDepartmentName');
        }catch(exception e){
            system.debug(e.getMessage());
            workDepartmentName='';
            workDepartmentId =null;
        } 
         
    }
    
    public void setWorkTaskToUpdate(){
        try{
            workTaskId  = id.valueof(ApexPages.CurrentPage().getParameters().get('workTaskId'));
            workTaskName = ApexPages.CurrentPage().getParameters().get('workTaskName');
        }catch(exception e){
            system.debug(e.getMessage());
            workTaskName='';
            workTaskId =null;
        } 
         
    }
    
    public void saveDocumentType(){
        error=false;
        try{
            documentType.name = documentType.Document_Type__c;
            upsert documentType;
            documentTypeMap.put(documentType.id, documentType);
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
        esl = new ewtSelectlist();
    }
    
    public void saveWorkTask(){
        error=false;
        try{
            EWT_Work_Task__c ewtWT = new EWT_Work_Task__c(name=workTaskName);
            if(workTaskId!=null){
                ewtWT.id=workTaskId;
            }else{
                ewtWT.Document_Type__c=esl.documentTypeValue;
            }
            
            try{
                upsert ewtWT;
                esl.getTheWorkTask(); 
            }catch(dmlException e){
                error=true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
                }  
            }
            
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
        //esl = new ewtSelectlist(false, ewtWT.Id, esl.documentTypeValue, esl.workTaskReasonValue, esl.workDepartmentValue);
    }

    public void saveWorkDepartment(){
        error=false;
        try{
            EWT_Work_Department__c ewtWD = new EWT_Work_Department__c(name=workDepartmentName);
            if(workDepartmentId!=null){
                ewtWD.id=workDepartmentId;
            }
            
            ewtWD.Work_Task__c=esl.workTaskValue;
            ewtWD.Work_Task_Reason__c=esl.workTaskReasonValue;
            
            try{
                upsert ewtWD;
                esl.getTheWorkDepartment(); 
                esl.workDepartmentValue = ewtWD.id;      
            }catch(dmlException e){
                error=true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
                }  
            }
            
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
        
    }
    
    public void saveWorkTaskReason(){
        error=false;
        try{
            
            EWT_Work_Task_Reason__c ewtWTR = new EWT_Work_Task_Reason__c(name=workTaskReasonName);
            if(workTaskReasonId!=null){
                ewtWTR.id=workTaskReasonId;
            }else{
                ewtWTR.EWT_Work_Tasks__c=esl.workTaskValue;
            }
            
            try{
                upsert ewtWTR;
                esl.getTheWorkTaskReason(); 
            }catch(dmlException e){
                error=true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
                }  
            }
            
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
       
    }
    
    public void saveClosedReason(){
        error=false;
        try{
            EWT_Closed_Reason__c ewtCR = new EWT_Closed_Reason__c(name=closedReasonName);
            if(closedReasonId!=null){
                ewtCR.id=closedReasonId;
            }
            
            ewtCR.EWT_Document_Type__c=esl.documentTypeValue;
            ewtCR.EWT_Work_Task__c=esl.workTaskValue;
            ewtCR.EWT_Work_Task_Reason__c=esl.workTaskReasonValue;
            ewtCR.EWT_Work_Department__c=esl.workDepartmentValue;
            
            try{
                upsert ewtCR;
                esl.getTheClosedReason();
            }catch(dmlException e){
                error=true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
                }  
            }
            
            
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
        
    }

    public void saveworkTaskStatus(){
        error=false;
        try{
            EWT_Work_Task_Status__c ewtWTS = new EWT_Work_Task_Status__c(name=workTaskStatusName);
            if(workTaskStatusId!=null){
                ewtWTS.id=workTaskStatusId;
            }

            try{
                upsert ewtWTS;
                esl.getTheworkTaskStatus();
                esl.workTaskStatusValue = ewtWTS.id; 
            }catch(dmlException e){
                error=true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
                }  
            }
            
            
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
        
    } 

    
    public void removeEWTSO(){
        error=false;
        try{
            id ewtSoId = id.valueof(ApexPages.CurrentPage().getParameters().get('ewtSoId'));
            sObject so = Schema.getGlobalDescribe().get(string.valueof(ewtSoId.getSObjectType())).newSObject();
            so.id=ewtSoId;
            delete so;
            
            if(string.valueof(ewtSoId.getSObjectType())=='EWT_Document_Type__c'){
              esl = new ewtSelectlist();  
            }else if(string.valueof(ewtSoId.getSObjectType())=='EWT_Work_Task__c'){
              esl.getTheWorkTask();  
            
            }else if(string.valueof(ewtSoId.getSObjectType())=='EWT_Work_Task_Reason__c'){
              esl.getTheWorkTaskReason();  
              
            }else if(string.valueof(ewtSoId.getSObjectType())=='EWT_Work_Department__c'){
              esl.getTheWorkDepartment();  
            
            }else if(string.valueof(ewtSoId.getSObjectType())=='EWT_Closed_Reason__c'){
              esl.getTheClosedReason();  
            
            }else if(string.valueof(ewtSoId.getSObjectType())=='EWT_Work_Task_Status__c'){
              esl.getTheworkTaskStatus(); 
            }  
            
        }catch(exception e){
            error=true;
            system.debug(e.getMessage());
            system.debug(e.getLineNumber());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
    }
    
}