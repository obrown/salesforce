/**
* This class contains unit tests for validating the behavior of  the Direct Billing Application
*/

@isTest
private class directBillingTest{
    
    static testMethod void directBillingTest() {
        
        directBillingTestData.loadData();
    
        /* Test Search  Function */

        directBillingSearchVFcontroller dbsearch = new directBillingSearchVFcontroller();
        dbsearch.eSSN = '123456789';
        dbSearch.eln='';
        dbSearch.efn='';
        dbSearch.searchHealthpac();
        ApexPages.CurrentPage().getParameters().put('memberKey', 'MICHAELNEWBECK04/17/1949034');
        dbSearch.createDbMember();
        
        dbSearch.clear();
        dbsearch.eSSN = '555746607';
        dbSearch.eln='';
        dbSearch.efn='';
        dbSearch.searchHealthpac();
        ApexPages.CurrentPage().getParameters().put('id',dbsearch.sfFound[0].id);
        
        directBillingMemberVFcontroller dbCtrl = new directBillingMemberVFcontroller();
        system.assert(dbCtrl.member.id == dbsearch.sfFound[0].id);
        dbCtrl.member.comments__c = 'UNIT.TEST';        
        dbCtrl.saveMember();
        
        dbCtrl.newLeave();
        dbCtrl.saveLeave(); //will error
        
        //ApexPages.CurrentPage().getParameters().put('payType', 'Semi-Monthly');
        //dbCtrl.setPayScheduleVF();
        
        //ApexPages.CurrentPage().getParameters().put('payType', 'Monthly');
        //dbCtrl.setPayScheduleVF();
        
        ApexPages.CurrentPage().getParameters().put('payType', 'Bi-Weekly');
        dbCtrl.setPayScheduleVF();
        
        dbCtrl.startDate = dbCtrl.biWeekly[1].getValue();
        dbCtrl.saveLeave();
        dbCtrl.cancelLeave();
        
        ApexPages.CurrentPage().getParameters().put('leaveId', dbCtrl.leaveList[0].id);
        dbCtrl.loadLeaveVF();
        
        ApexPages.CurrentPage().getParameters().put('startDate', dbCtrl.startDate);
        dbCtrl.refreshDbRates();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'medical');
        ApexPages.CurrentPage().getParameters().put('planName', 'Waive');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'medical');
        ApexPages.CurrentPage().getParameters().put('planName', 'Consumer Select PPO Full Non-Tobacco');
        ApexPages.CurrentPage().getParameters().put('planTier', 'Employee Only');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'dental');
        ApexPages.CurrentPage().getParameters().put('planName', 'Waive');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'dental');
        ApexPages.CurrentPage().getParameters().put('planName', 'DHMO Full');
        ApexPages.CurrentPage().getParameters().put('planTier', 'Employee Only');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'medicalParma');
        ApexPages.CurrentPage().getParameters().put('planName', 'Waive');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'Vision');
        ApexPages.CurrentPage().getParameters().put('planName', '');
        ApexPages.CurrentPage().getParameters().put('planTier', 'Employee Only');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'Vision');
        ApexPages.CurrentPage().getParameters().put('planName', 'Vision');
        ApexPages.CurrentPage().getParameters().put('planTier', 'Employee Only');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'spouseLife');
        ApexPages.CurrentPage().getParameters().put('planName', '');
        ApexPages.CurrentPage().getParameters().put('planTier', '$5,000');
        dbCtrl.rateChange();
        
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'spouseLife');
        ApexPages.CurrentPage().getParameters().put('planName', 'spouseLife');
        ApexPages.CurrentPage().getParameters().put('planTier', '$5,000');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'dependentLife');
        ApexPages.CurrentPage().getParameters().put('planName', '');
        ApexPages.CurrentPage().getParameters().put('planTier', '$5,000 Child(ren) Only');
        dbCtrl.rateChange();
        
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'dependentLife');
        ApexPages.CurrentPage().getParameters().put('planName', 'dependentLife');
        ApexPages.CurrentPage().getParameters().put('planTier', '$5,000  Child(ren) Only');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'personalAccident');
        ApexPages.CurrentPage().getParameters().put('planName', '');
        ApexPages.CurrentPage().getParameters().put('planTier', '$5,000  Child(ren) Only');
        dbCtrl.rateChange();
        
        ApexPages.CurrentPage().getParameters().put('planRecordType', 'personalAccident');
        ApexPages.CurrentPage().getParameters().put('planName', 'personalAccident');
        ApexPages.CurrentPage().getParameters().put('planTier', 'Employee Only');
        dbCtrl.rateChange();
        
        dbCtrl.saveLeave();
        dbCtrl.leave.End_Invoice_Override__c = dbCtrl.leave.start_date__c.addMonths(1); 
        dbCtrl.runInvoices();
        
        ApexPages.CurrentPage().getParameters().put('leaveInvoiceId',dbCtrl.leaveInvoiceList[0].id);
        dbCtrl.loadLeaveInvoiceVF();
        dbCtrl.saveLeaveInvoice();
        dbCtrl.cancelLeaveInvoice();
        
        dbCtrl.leave.end_date__c = dbCtrl.leave.start_date__c.addMonths(3);
        dbCtrl.runInvoices();
        
        dbCtrl.leave.End_Invoice_Override__c=null;
        dbCtrl.leave.end_date__c = dbCtrl.leave.start_date__c.addMonths(3);
        dbCtrl.saveLeave(); 
        dbCtrl.runInvoices(); 
         
        dbCtrl.newLeaveNote();
        dbCtrl.leaveNote.Note__c ='Unit.Test';
        dbCtrl.saveLeaveNote();   
        dbCtrl.cancelLeaveNote(); 
        
        dbCtrl.newLeave();
        dbCtrl.startDate = string.valueof(dbCtrl.leaveList[0].start_date__c);
        dbCtrl.saveLeave(); //will error
        dbCtrl.startDate = string.valueof(dbCtrl.leaveList[0].start_date__c.addDays(1));
        dbCtrl.leave.end_date__c = dbCtrl.leaveList[0].end_date__c;
        dbCtrl.saveLeave(); //will error
        
        dbCtrl.startDate = string.valueof(dbCtrl.leaveList[0].start_date__c);
        dbCtrl.leave.end_date__c = dbCtrl.leaveList[0].end_date__c;
        dbCtrl.saveLeave(); //will error
        
        directBillingManualBatchInvoice dbm = new directBillingManualBatchInvoice();
        dbm.run();
        
        
        
    }
    
    static testMethod void directBillingBatchInvoiceControllerTest() {
        
        directBillingTestData.loadData();    
        
        directBillingSearchVFcontroller dbsearch = new directBillingSearchVFcontroller();
        dbsearch.eSSN = '123456789';
        dbSearch.eln='';
        dbSearch.efn='';
        dbSearch.searchHealthpac();
        ApexPages.CurrentPage().getParameters().put('memberKey', 'MICHAELNEWBECK04/17/1949034');
        dbSearch.createDbMember();
        
        dbSearch.clear();
        dbsearch.eSSN = '555746607';
        dbSearch.eln='';
        dbSearch.efn='';
        dbSearch.searchHealthpac();
        ApexPages.CurrentPage().getParameters().put('id',dbsearch.sfFound[0].id);
        
        directBillingMemberVFcontroller dbCtrl = new directBillingMemberVFcontroller();
        system.assert(dbCtrl.member.id == dbsearch.sfFound[0].id);
        dbCtrl.member.comments__c = 'UNIT.TEST';        
        dbCtrl.saveMember();
        
        dbCtrl.newLeave();
        dbCtrl.saveLeave(); //will error
        
        ApexPages.CurrentPage().getParameters().put('payType', 'Bi-Weekly');
        dbCtrl.setPayScheduleVF();
        
        dbCtrl.leave.pay_frequency__c = 'Bi-Weekly';
        dbCtrl.startDate = dbCtrl.biWeekly[1].getValue();
        dbCtrl.saveLeave();
        
        dbCtrl.leave.End_Invoice_Override__c = dbCtrl.leave.start_date__c.addMonths(1); 
        dbCtrl.saveLeave(); 
        dbCtrl.runInvoices();
        
        ApexPages.CurrentPage().getParameters().put('leaveInvoiceId',dbCtrl.leaveInvoiceList[0].id);
        dbCtrl.loadLeaveInvoiceVF();
        dbCtrl.saveLeaveInvoice();
        
        directBillingBatchInvoiceController dbbc = new directBillingBatchInvoiceController();
        
        Direct_Billing_Invoice__c invoice = new Direct_Billing_Invoice__c();
        invoice.db_leave__c = dbCtrl.leaveList[0].id;
        
        date invoiceMonth = date.today();
        invoiceMonth = date.valueof(invoiceMonth.year()+'-'+invoiceMonth.month()+'-01');
        invoice.Due_date__c = invoiceMonth.addMonths(1).toStartofMonth().addDays(-1);
        invoice.Invoice_Amount__c = 100.00;
        invoice.Invoice_Date__c = date.today(); 
        invoice.Invoice_Month__c = invoiceMonth;  
        invoice.payment_amount__c=50.00;
        insert invoice;
        
        invoice.payment_amount_2__c=50.00;
        update invoice;
    }
}