/**
 * @description       : 
 * @author            : Michael Martin
 * @group             : 
 * @last modified on  : 02-12-2021
 * @last modified by  : Michael Martin
 * Modifications Log 
 * Ver   Date         Author           Modification
 * 1.0   02-12-2021   Michael Martin   Initial Version
**/
public with sharing class populationHealthWorkFlow{
    
    public static void setSearchCert(phm_Patient__c[] patients){
        for(phm_Patient__c p : patients){
            if(p.cert__c!=null && p.cert__c.length()>4){
                p.certSearch__c = p.cert__c.left(5);
            }
        }
    }
    
    public static void validationRules(phm_Patient__c[] patients){
        
        for(phm_Patient__c p : patients){
        
            //Patient can only have 1 Residence address
            integer x = 0;
            
            if(p.Address_Type__c=='Residence'){
            x++;
            }
        
            if(p.Address_Type2__c=='Residence'){
            x++;
            }
        
            if(p.Address_Type3__c=='Residence'){
            x++;
            }
        
            if(p.Address_Type4__c=='Residence'){
            x++;
            }     
        
            if(x>1){
                p.addError('Only one address type can be marked as type \'Residence\'.');
            }
            
            //Patient can only have 1 preferred phone number
            x=0;
            if(p.Preferred_Phone__c){
            x++;
            }
        
            if(p.Preferred_Phone2__c){
            x++;
            }
        
            if(p.Preferred_Phone3__c){
            x++;
            }
        
            if(p.Preferred_Phone4__c){
            x++;
            }     
        
            if(x>1){
                p.addError('Only one preferred phone number is allowed.');
            }
            
            x=0;
            
            if(p.Date_of_Death__c != '' && p.Date_of_Death__c != null && (p.Source_of_Date_or_Death__c==null || p.Source_of_Date_or_Death__c=='')){
               p.addError('Please populate the source of the date of death'); 
            }
                
        }
        
    }
}