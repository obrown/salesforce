public class OwtUnassignedFiles{
   
   public string fname ='';
   public string fileName {get; set;}
   public final string sId = userinfo.getSessionid();
   public string getsid(){
       return sId;
   }
   
   public final string userId = userinfo.getuserid(); 
   public string getuserId(){
       return userId;
   }
   
   public final string fileserverurl = fileServer__c.getInstance().fileserverurl__c;
   public FileChangeRequestResponseStruct.Files[] fileList {get; set;}
   public boolean isApiError {get; private set;}
   public integer unassignedFileCount {get; private set;}
   final integer maxfilelistsize =25;
   public string fileDestRootDir = 'EWT';
   public string unassignedDir = '';
   public string srcfile_filepath; 
   string src,  dest;
   
   public OwtUnassignedFiles(){
       unassignedFileCount=0;
   }
   
   public string getfileserverurl(){
       return fileserverurl;
   }
   
   public void setFileName(){
        fileName=ApexPages.CurrentPage().getParameters().get('fn');
        system.debug('fileName: '+fileName);
        if(fileName=='' || fileName=='None'){
            fileName=null;
            return;
            
        }else{
            fname =fileName;
            fileName= fileserverurl+'openfile?filepath=' +srcfile_filepath+''+fname + '&oid=' + UserInfo.getOrganizationId() + '&sid=' + sid + '&uid=' +userId;
        }
        
        
  }
   
  public class response{
      public string ResultCode ;
      public string errorMsg;
      
  } 
   
  public response setFileList(string filepath){
       system.debug('filepath '+filepath);
        isApiError=false;
        response response = new response();
        response.ResultCode = '2';
        String result =fileServer.unassignedfiles(filepath);
        
        if(result=='401 error'){
            response.ErrorMsg = 'Error setting path: Unauthorized';
            return response;
        }
        
        srcfile_filepath = filepath;
        
        
        
        try{
            FileChangeRequestResponseStruct foo = (FileChangeRequestResponseStruct )JSON.deserialize(result, FileChangeRequestResponseStruct .class);
            fileList = foo.FileList;
            
            if(foo.ResultCode=='2'){
                response.ErrorMsg = 'Error setting path: '+ foo.Message;
                return response;
            }
            
            if(!fileList.isEmpty()){
                unassignedFileCount = fileList.size();
            }
        
            if(unassignedFileCount > maxfilelistsize){
                FileChangeRequestResponseStruct.Files[] tFoo = new FileChangeRequestResponseStruct.Files[]{};
                integer count=0;
                for(integer i=0; i<=maxfilelistsize;i++){ 
                    tfoo.add(fileList[i]);                    
                }
                fileList =tfoo;
            }
            
            
        }catch(exception e){
            system.debug('Error setting file drive ' + e.getMessage() + ' on line number ' + e.getLineNumber());
            response.errorMsg = 'Error setting file drive ' + e.getMessage();
            return response;
        }
        
        response.ResultCode = '0';
        return response;
   }
   
   public string moveFile(string fileSrc, string ewtName, id wtaid, string docType){
        
        datetime dtNow = datetime.now();
        string safeFname = ewtName.replace('-','')+'_'+utilities.formatDateTime(dtNow).replaceAll('/','').replaceAll(' ','_').replaceAll(':','').replaceAll('_pm',dtNow.second()+'pm').replaceAll('_am',dtNow.second()+'am')  +'_' + docType + '.pdf';
        FileChangeRequestResponseStruct result = fileServer.moveFile(fileSrc, fileDestRootDir+'/'+ewtName+'/'+safeFname);
        string basDir = fileServer__c.getInstance().baseDirectory__c;
        
        if(result.ErrorReason==null){
            insert new Work_Task_Attachment__c(name=safeFname,SalesforceAttachmentLocation__c=basDir +'/'+fileDestRootDir+'/'+ewtName+'/'+safeFname, Operations_Work_Task__c=wtaid);
        }else{
            return result.ErrorReason;
        }
        
        return null;
    }
    
}