public without sharing class futureLifestart {

    @future
    public static void deletePregnancyOutcome(set<id> theParentID){
        
        Lifestart_Pregnancy_History__c[] delRecords = new Lifestart_Pregnancy_History__c[]{};
        map<id, Lifestart_Pregnancy_History__c[]> opMap = new map<id, Lifestart_Pregnancy_History__c[]>();
        Lifestart_Pregnancy_History__c[] foo = new Lifestart_Pregnancy_History__c[]{};
        
        for(Lifestart_Pregnancy_History__c l : [select id,lifestart__c  from Lifestart_Pregnancy_History__c where Lifestart__c in :theParentID and recordtype.name = 'outcomePregnancy' order by lifestart__c, createdDate desc]){
            
            foo =   opMap.get(l.lifestart__c);
            if(foo==null){
                foo=new Lifestart_Pregnancy_History__c[]{};
            }
            foo.add(l);
            opMap.put(l.lifestart__c, foo);
        }

        integer count=0;
        for(id i : opMap.keyset()){
            foo =   opMap.get(i);
            count = foo.size();
            for(integer x=1; x < count; x++){
                system.debug(foo[x].id);
                delRecords.add(foo[x]);
            }

        }

        delete delRecords;
        
        
    }

}