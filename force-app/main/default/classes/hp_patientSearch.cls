public class hp_patientSearch extends hp_callWebApp{
    
    public hpPatientSearchStruct returnObj = new hpPatientSearchStruct();
    
    public static hpPatientSearchStruct searchHP(string uw, string essn, string cert, string efn, string eln){
        
        hp_patientSearch searchHP = new hp_patientSearch();
        searchHP.searchHealthPac(uw,essn,cert,efn,eln);
        return searchHP.returnObj;
        
    }
    
    public hpPatientSearchStruct searchHealthPac(string uw, string essn, string cert, string efn, string eln){
        endpoint='patientSearch';
        
        if(cert==null){cert='';}
        if(essn==null){essn='';}
        if(efn==null){efn='';}
        if(eln==null){eln='';}
        if(uw==null){uw='';}
        
        if(cert!=''){
            cert = cert.toUpperCase().trim();
        }
        
        search.add('Cert:'+cert);
        search.add('Edob:'+'');
        search.add('FirstName:'+efn);
        search.add('Group:'+'');
        search.add('LastName:'+eln);
        search.add('Ssn:'+essn);
        search.add('Underwriter:'+uw);
        jsonBody = jb.eligSearch(search);
        
        if(Test.IsRunningTest()){
           result = '{"ErrorMsg":"","ResultCode":"2","PatSearchResultLogs":[{"Underwriter":"034","Grp":"CP3","SSN":"555746607","EmpLastName":"NEWBECK","EmpDateOfBirth":"19490417","Status":"T","Sequence":"00","EmpFirstName":"MICHAEL","PatFirstName":"MICHAEL","PatLastName":"NEWBECK","PatDateOfBirth":"19490417","Relationship":"1","MemberNumber":"1ABCD123456", "Dept":"034","EmpAddress1":"UNIT TEST","EmpAddress2":"UNIT TEST","EmpCity":"UNIT TEST","EmpState":"AZ","EmpZip":"85212"},{"Underwriter":"034","Grp":"CP3","SSN":"555746607","EmpLastName":"NEWBECK","EmpDateOfBirth":"19490417","Status":"T","Sequence":"00","EmpFirstName":"MICHAEL","PatFirstName":"MICHAEL","PatLastName":"NEWBECK","PatDateOfBirth":"19490417","Relationship":"1","MemberNumber":"1ABCD123456", "Dept":"008","EmpAddress1":"UNIT TEST","EmpAddress2":"UNIT TEST","EmpCity":"UNIT TEST","EmpState":"AZ","EmpZip":"85212"},{"EmpLastName":"NEWBECK","Grp":"M01","SSN":"555746607","EmpFirstName":"MICHAEL","PatLastName":"NEWBECKER","Relationship":"4","Underwriter":"034","Sequence":"05","EmpDateOfBirth":"19490417","PatFirstName":"STEPHANIE","PatDateOfBirth":"19831012","Status":"T","MemberNumber":"1ABCD123456", "Dept":"034","EmpAddress1":"UNIT TEST","EmpAddress2":"UNIT TEST","EmpCity":"UNIT TEST","EmpState":"AZ","EmpZip":"85212"},{"Sequence":"01","EmpFirstName":"MICHAEL","PatFirstName":"ELLEN","SSN":"555746607","Grp":"CP3","EmpLastName":"NEWBECK","EmpDateOfBirth":"19490417","PatLastName":"NEWBECK","PatDateOfBirth":"19470210","Relationship":"2","Status":"T","Underwriter":"034","MemberNumber":"1ABCD123456", "Dept":"034","EmpAddress1":"UNIT TEST","EmpAddress2":"UNIT TEST","EmpCity":"UNIT TEST","EmpState":"AZ","EmpZip":"85212"},{"EmpFirstName":"MICHAEL","EmpLastName":"NEWBECK","EmpDateOfBirth":"19490417","PatDateOfBirth":"19490417","Grp":"OR1","SSN":"555746607","PatFirstName":"MICHAEL","PatLastName":"NEWBECK","Relationship":"1","Status":"A","Underwriter":"034","Sequence":"00","MemberNumber":"1ABCD123456", "Dept":"034","EmpAddress1":"UNIT TEST","EmpAddress2":"UNIT TEST","EmpCity":"UNIT TEST","EmpState":"AZ","EmpZip":"85212"},{"EmpDateOfBirth":"19470210","PatDateOfBirth":"19470210","Relationship":"2","Status":"A","Underwriter":"008","Grp":"OR1","Sequence":"00","PatFirstName":"ELLEN","PatLastName":"NEWBECK","SSN":"555746607","EmpFirstName":"MICHAEL","EmpLastName":"NEWBECK","MemberNumber":"1ABCD123456", "Dept":"034","EmpAddress1":"UNIT TEST","EmpAddress2":"UNIT TEST","EmpCity":"UNIT TEST","EmpState":"AZ","EmpZip":"85212"}]}';
        }else{
           result = callWebApp(null); 
        }
        
        try{
            returnObj = (hpPatientSearchStruct)JSON.deserialize(result, hpPatientSearchStruct.class);
            
        }catch(exception e){
            returnObj.ResultCode='0';  
            if(returnObj.ErrorMsg==null){
               returnObj.ErrorMsg= result; 
            }  
        }
        
        if(returnObj.ResultCode=='2'){
            returnObj.cleanUpLog();
        }
        
        return returnObj;
    }
    
}