public with sharing class umDenialNetworkVariance{
    
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c  uml, Utilization_Management_Clinician__c clinician, string logoPath){
        
        StaticResource sr = [SELECT Id,NamespacePrefix,SystemModstamp FROM StaticResource WHERE Name = 'medicalDirectorSignature' LIMIT 1];
         
        String prefix = sr.NamespacePrefix;
        if( String.isEmpty(prefix) ) {
            prefix = '';
        } else {
            //If has NamespacePrefix
            prefix += '__';
        }
        String srPath = '/resource/' + sr.SystemModstamp.getTime() + '/' + prefix + 'medicalDirectorSignature';
        
        string letterText='';
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=um.patient__r.Address__c+'<br/>';
        letterText+=um.patient__r.City__c+'&nbsp';
        letterText+=um.patient__r.State__c+'&nbsp';
        letterText+=um.patient__r.Zip__c+'<br/><br/>';
        letterText+='</div>';
        letterText+='<p>';  
        letterText+='Contigo Health is a third-party administrator (TPA) that performs Care Management Services for the self-funded group health plan of '+um.patient__r.Patient_Employer__r.name+', Health System Group Benefit Plan (the “Plan”) covered by ERISA.';
        letterText+='</p>';
        
        letterText+='<p>';  
        letterText+='Contigo Health has denied the Network Variance Request referenced below:';
        letterText+='</p>';
        
        letterText+='<div style="margin-top:10px">';
        letterText+='<div style="display:inline-block;width:25%">';
        letterText+='Case Number:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px;">';
        letterText+='&nbsp;'+um.HealthPac_Case_Number__c;
        letterText+='</div>';
        letterText+='</div>'; 
        
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%">';
        letterText+='Patient Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px;">';
        letterText+='&nbsp;'+um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c;
        letterText+='</div>';
        letterText+='</div>'; 
        
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%">';
        letterText+='Patient Date of Birth:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px;">';
        letterText+='&nbsp;'+um.patient__r.Patient_Date_of_Birth__c;
        letterText+='</div>';
        letterText+='</div>'; 
        
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%">';
        letterText+='Provider Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c+'/'+um.Facility_Name__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%">';
        letterText+='Date of Service:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;Beginning '+um.Admission_Date__c.month()+'/'+um.Admission_Date__c.day()+'/'+um.Admission_Date__c.year();
        letterText+='</div>';
        letterText+='</div>';
        
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%;">';
        letterText+='Type of Service Request:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+uml.Type_of_Request__c;
        letterText+='</div>';
        letterText+='</div>';
        
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%;">';
        letterText+='Service Description:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+uml.Type_of_Request__c;
        letterText+='</div>';
        letterText+='</div>';
        
        letterText+='<br/>';
        
        letterText+='Denial Reason: The request for&nbsp;_____________________________________, is denied because service(s) is available from an in network provider.';
        
        letterText+='Rationale Used to Make Determination: Summary Plan Document, Network Variance Requests and Determinations, Network Provider Directory';
        
        letterText+='<br/><br/>';
        letterText+='<div style="font-weight:bold">';
        letterText+='Network Variance Requests and Determinations:';
        letterText+='</div>';
        letterText+='To receive benefits under the Plan, you must use In-Network Providers as set forth herein. The Plan has contracted with the Network(s) to provide care to Covered Persons at discounted fees. Hospitals, Physicians and other Providers and facilities who have contracted with the Network are called "Network Providers." Those who have not contracted with the Network are referred to in this Plan as "Out-of-Network Providers."  This arrangement results in the following benefits to Covered Persons:';
        letterText+='<ol style="list-style-type:decimal">';
        letterText+='<li>';
        letterText+='The  Plan  provides  different  levels  of  benefits  based  on  whether  the  Covered  Persons  use  a Network or ';
        letterText+='Out-of-Network Providers. Unless the exception shown below applies, if a Covered Person elects to receive medical care from the Out-of-Network Provider, services will not be covered by the Plan. The following exceptions apply:</span>';
        letterText+='</li>';
        
        letterText+='<ul style="list-style-type:none;">';
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='a. The Network Provider level of benefits is payable at the allowable amount when a Covered Person receives Emergency Care either as an outpatient or if admitted at an Out-of-Network facility or from an out-of-network provider, covered expenses will be applied to your in-network deductible, coinsurance and out-of-pocket maximum up to the allowed amount Plus you will be responsible for paying any amounts above the allowed amount that the provider bills you. You also may need to file claim forms';
        letterText+='</li>';
        letterText+='<li>';
        letterText+='b. The Network Provider level of benefits is payable at usual and customary charges when a Covered Person utilizes a Network Facility of Physician office, but that Network Facility of Physician office utilizes an Out-of-Network ancillary provider.';
        letterText+='</li>';
        letterText+='<li>';
        letterText+='c. The member obtains a network variance because the service is not available from a network provider as described herein.';
        letterText+='</li>';
        letterText+='</ul>';
        
        letterText+='<li>';
        letterText+='If the charge billed by an Out-of-Network Provider for any covered service is higher than the Reasonable and Customary Charges determined by the Plan, Covered Persons are responsible for the excess (Balance Billing). Since Network Providers have agreed to accept the negotiated discounted fee as full payment for their services, Covered Persons are not responsible for any billed amount that exceeds that fee when utilizing a network provider.';
        letterText+='</li>';
        
        letterText+='<li>';
        letterText+='To receive benefit consideration, Covered Persons must submit claims for services provided by Out-of-Network Providers to the Claims Administrator. Network Providers have agreed to bill the Plan directly, so that Covered Persons do not have to submit claims.';
        letterText+='</li>';
        letterText+='</ol>';
        
        letterText+='<p>';
        letterText+='Although additional information is not required to submit an appeal, all information provided will be considered when rendering an Appeal Decision.';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='Additional Information Consideration:<br/>';
        letterText+='&nbsp;&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;&nbsp;Documentation representing material difference from information originally submitted.';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='All choices regarding the care and treatment of the patient remain the responsibility of the attending physician. In no way does this letter attempt to dictate the care the patient ultimately receives.';
        letterText+='</p>';
        
        letterText+='<br/><br/>';
        letterText+='Sincerely,<br/><br/>';
        //letterText+='<div style="margin-bottom: 0px" id="cke_1_signSpace" class="cke_1_signSpace">&nbsp;</div>';
        letterText+='<div ></div>';
        letterText+='<div style="display:none" >signhere</div>';
        letterText+='Eric M. Yasinow, M.D.<br/>';
        letterText+='Medical Director';
        letterText+='<br/><br/>';
        
        letterText+='SPANISH (Español): Para obtener asistencia en Español, llame al 330-656-1072<br/>';
        letterText+='TAGALOG (Tagalog):  Kung kailangan ninyo ang tulong sa Tagalog tumawag sa 330-656-1072<br/>';
        letterText+='CHINESE <span style="font-family: Arial Unicode MS">(中文):  如果需要中文的帮助，请拨打这个号码</span> 330-656-1072<br/>';
        letterText+='NAVAJO (Dinek\'ehgo  shika  at\'ohwol  ninisingo, kwiijigo  holne\' 330-656-1072<br/>';

        letterText+='<p>';
        letterText+='The plan complies with applicable Federal civil rights laws and does not discriminate on the basis of race, color, national origin, sex, age or disability above the appeal right section of the denial letters.';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='The claimant has the right to request, verbally or in writing and free of charge, any internal rule, guideline, protocol, scientific or clinical rationale, other similar criteria, or plan provision used to make the original determination. Submit requests to Contigo Health at the address on this letter or call Customer Service at the number on your Medical Card. ';
        letterText+='</p>';
        
        letterText+='<div style="page-break-after:always;"/>';
        letterText+='<p style="text-align:center;text-decoration:underline">';
        letterText+='RIGHT TO APPEAL';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='First appeal level:  The claimant has the right to initiate a formal appeal concerning any denied or partially denied claim verbally or in writing. The appeal should include the reason(s) for the request, any additional facts or documentation and/or a copy of the Explanation of Benefits that supports the appeal. To submit a first level appeal, send to Contigo Health within 180 days of receipt of this letter or call Customer Service at the number on your Medical Card. If the claimant does not submit an appeal on time, he or she will lose the right to the appeal and the right to file suit in court.';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='An Expedited Appeal is available when the adverse determination relates to a claim involving urgent care, where the application of standard appeal time periods could seriously jeopardize the life or health of the claimant or the ability to regain maximum function; or where in the opinion of a physician with knowledge of the claimant’s medical condition, would subject them to severe pain that cannot be adequately managed without the care or treatment that is the subject of the appeal.';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='To file an Expedited Appeal, please call the Appeals Department at 1-877-230-0996 or fax the request to 1-877-891-2691 Attention: Appeals Coordinator.  Only Expedited Appeals will be accepted at these numbers.';
        letterText+='</p>';
        
        letterText+='<div style="margin-top:10px">';
        
        letterText+='<div style="display:inline-block;">';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='CC:'; 
        letterText+='</div>';
        
        letterText+=um.Facility_Name__c;
                      
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        boolean hasFacility = (um.Facility_Name__c!=null && um.Facility_Street__c != null && um.Facility_City__c  != null && um.Facility_State__c  != null && um.Facility_Zip_Code__c  != null);
        
        letterText+='<div style="display:inline-block;">';
        letterText+= hasFacility ? um.Facility_Street__c : '';
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasFacility ? um.Facility_City__c+', ' : ' ';
        letterText+=hasFacility ? um.Facility_State__c+' ' : ' ';
        letterText+=hasFacility ? um.Facility_Zip_Code__c : '';
        letterText+='</div>';
        letterText+='</div>';
        letterText+='</div>';  
        letterText+='<br/>';                                                                                                                                                                                                                                                                   
        letterText+='<div style="margin-top:10px">';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        letterText+='<div style="display:inline-block;">';
        letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        boolean hasClinician = (Clinician.Street__c!=null&&Clinician.City__c!=null&&Clinician.State__c!=null&&Clinician.Zip_Code__c!=null);
        
        letterText+='<div style="display:inline-block;"/>';
        letterText+=hasClinician ? Clinician.Street__c : '';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasClinician ? Clinician.City__c+', ' : ', ';
        letterText+=hasClinician ? Clinician.State__c+' ' : ' ';
        letterText+=hasClinician ? Clinician.Zip_Code__c : '';
        letterText+='</div>';
        letterText+='</div><br/>';         
        
        letterText+='<p>';
        letterText+='If you have complied with and exhausted the claim and appeal procedures described above you have a right to bring legal action for benefits under ERISA section 502(a).  Nothing contained in this letter should be construed as a waiver of any rights or defenses under the Plan. This determination has been made in good faith and without prejudice under the terms and conditions of the Plan, whether or not specifically mentioned herein.';
        letterText+='</p>';
        
        letterText+='</div>';
        return letterText;
    }
    
}