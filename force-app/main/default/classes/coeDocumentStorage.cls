public with sharing class coeDocumentStorage{
    
    public string saveDocument(string recordId, string coeFileDirectory, string filename, string file_description){
        ftpAttachment__c ftpAttachment = new ftpAttachment__c(patient_case__c=recordId);
        return saveFtpAttachment(ftpAttachment, coeFileDirectory, fileName, file_description);         
    }
    
    public string saveOncologyDocument(string recordId, string coeFileDirectory, string filename, string file_description){
        
        ftpAttachment__c ftpAttachment = new ftpAttachment__c(oncology__c=recordId);
        return saveFtpAttachment(ftpAttachment, coeFileDirectory, fileName, file_description);           
    }
    
    public string saveBariatricDocument(string recordId, string coeFileDirectory, string filename, string file_description){
        
        ftpAttachment__c ftpAttachment = new ftpAttachment__c(Bariatric__c=recordId);
        return saveFtpAttachment(ftpAttachment, coeFileDirectory, fileName, file_description);
    }

    public string saveCustomerCareDocument(string recordId, string customerCareFileDirectory, string filename, string file_description){
        
        ftpAttachment__c ftpAttachment = new ftpAttachment__c(Inquiry__c=recordId);
        return saveFtpAttachment(ftpAttachment, customerCareFileDirectory, fileName, file_description);
    }


    public string saveOVRDocument(string recordId, string customerCareFileDirectory, string filename, string file_description){
        
        ftpAttachment__c ftpAttachment = new ftpAttachment__c(Control__c=recordId);
        return saveFtpAttachment(ftpAttachment, customerCareFileDirectory, fileName, file_description);
    }
        
    id saveFtpAttachment(ftpAttachment__c ftpAttachment, string coeFileDirectory, string fileName, string file_description){
        ftpAttachment.subDirectory__c= coeFileDirectory;
        ftpAttachment.fileName__c= fileName;
        ftpAttachment.file_Description__c= file_description;
        
        try{
            insert ftpAttachment;
        }catch(exception e){
           // isError=true;
           // message=e.getMessage();
           return null;
        }
        return ftpAttachment.id;       
    }
}