public without sharing class oncologyOnlineIntakeExtension {
    //Example link used to set the client changing the client=clientname to display since the text on the page is displayed based on client.
    //*******https://staging-hdplus.cs4.force.com/nextera**********************
    //*******https://c.cs4.visual.force.com/apex/oncologyOnlineIntake/nextera**********************

    public String callBackAmPm {
        get; set;
    }
    public string csNumber {
        get; private set;
    }
    public string employeeLabel {
        get; private set;
    }
    public string bid {
        get; private set;
    }
    public string clientName {
        get; private set;
    }
    public string empFirstName {
        get; set;
    }
    public string empLastName {
        get; set;
    }
    public string empDob {
        get; set;
    }
    public string empAddress {
        get; set;
    }
    public string empEmail {
        get; set;
    }
    public string empEmailConfirm {
        get; set;
    }
    public string empAdditionalContact {
        get; set;
    }

    public string additionalInformation {
        get; set;
    }

    public selectOption[] empAltType {
        get; set;
    }
    public selectOption[] empTimeZone {
        get; set;
    }

    public string patFirstName {
        get; set;
    }
    public string patLastName {
        get; set;
    }
    public string patDob {
        get; set;
    }
    public string patAddress {
        get; set;
    }
    public string patEmail {
        get; set;
    }
    public string patEmailConfirm {
        get; set;
    }
    public selectOption[] patAltType {
        get; set;
    }
    public selectOption[] patTimeZone {
        get; set;
    }

    selectOption[] phoneType;
    selectOption[] timezones;
    public string isMemberPatient {
        get; set;
    }
    public string empAddySame {
        get; set;
    }

    selectOption[] relOpts;
    selectOption[] btc;

    public Oncology_Physician__c pro {
        get; set;
    }
    public Oncology_Physician__c proA {
        get; set;
    }
    ApexPages.StandardController controller;
    public Oncology__c oRecord {
        get; set;
    }

    public oncologyOnlineIntakeExtension(ApexPages.StandardController controller) {
        this.controller = controller;
        oRecord = (Oncology__c)controller.getRecord();
        employeeLabel = 'Employee';
        bid = 'Insurance Medical Id';
        pro = new Oncology_Physician__c();
        proA = new Oncology_Physician__c();
        csNumber = '(877) 885-0652';

        String BaseURL = site.getBaseUrl();

        string client = ApexPages.CurrentPage().getParameters().get('client');

        if (client != null) {
            if (client == 'sisc' || client == 'siscinfo') {
                clientName = 'SISC';
                csNumber = '(877) 220-3556';
            }
            else if (client == 'amazon') {
                clientName = 'Amazon';
            }
            else if (client == 'NextERA') {
                clientName = 'NextERA';
                csNumber = '(877) 230-0988';
            }
            else{
                clientName = '';
            }
        }
        else if (BaseURL != null) {
            string[] bulist = BaseURL.split('/');
            if (bulist != null && !bulist.isempty()) {
                string test = bulist [bulist.size() - 1];
                test = test.tolowercase();
                if (test == 'sisc' || test == 'siscinfo') {
                    clientName = 'SISC';
                    csNumber = '(877) 220-3556';
                }
                else if (test == 'dg') {
                    clientName = 'Dollar General';
                    csNumber = '(833) 867-3699';
                }
                else if (test == 'amazon') {
                    clientName = 'Amazon';
                    //4-30-21 added NextERA
                }
                else if (test == 'NextERA') {
                    clientName = 'NextERA';
                    csNumber = '(877) 230-0988';
                }
                else{
                    clientName = '';
                }
            }
        }
    }

    public boolean isError {
        get; set;
    }

    public void mySubmit(){
        isError = false;
        try{
            syncPatientandEmployeeDemo();
            syncPatientandEmployeeAddress();

            try{
                string[] dob;
                if (patDob != '' && patDob != null) {
                    dob = patDob.split('/');
                    oRecord.Patient_DOB__c = dob [2] + '-' + dob [0] + '-' + dob [1];
                }

                if (empDob != '' && empDob != null) {
                    dob = empDob.split('/');
                    oRecord.Employee_DOB__c = dob [2] + '-' + dob [0] + '-' + dob [1];
                }

                oRecord.procedure__c = [select id from procedure__c where name = 'Oncology'].id;

                if (clientName == 'SISC') {
                    oRecord.client__c = [select id from client__c where name = 'SISC'].id;
                    oRecord.employer__c = 'SISC';
                    oRecord.Referral_Source__c = 'Self Referral';
                }
                else if (clientName == 'Dollar General') {
                    oRecord.client__c = [select id from client__c where name = 'Dollar General'].id;
                    oRecord.employer__c = 'Dollar General';
                }
                else if (clientName == 'NextERA') {
                    oRecord.client__c = [select id from client__c where name = 'NextEra Energy'].id;
                    oRecord.employer__c = 'NextEra Energy';
                    oRecord.Referral_Source__c = 'Self Referral';
                }
                else{
                    oRecord.client__c = [select id from client__c where name = 'Amazon'].id;
                    oRecord.Referral_Source__c = 'Self Referral';
                    oRecord.employer__c = 'Amazon';
                }
                //oRecord.Referral_Source__c = 'Accolade';

                if (oRecord.Id == null) {
                    upsert oRecord;
                }

                //oRecord = [select name from oncology__c where id =:orecord.id];

                if (pro.first_name__c != '' && pro.first_name__c != null && pro.last_name__c != null && pro.last_name__c != '') {
                    pro.oncology__c = oRecord.id;
                    insert pro;
                }

                if (proA.first_name__c != '' && proA.first_name__c != null && proA.last_name__c != null && proA.last_name__c != '') {
                    proA.oncology__c = oRecord.id;
                    insert proA;
                }
            }catch (dmlException e) {
                isError = true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i) + ' ' + e.getLineNumber());
                }
            }
        }catch (exception e) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    public void syncPatientandEmployeeDemo(){
        oRecord.Patient_First_Name__c = patFirstName;
        oRecord.Patient_Last_Name__c = patLastName;

        oRecord.Employee_First_Name__c = empFirstName;
        oRecord.Employee_Last_Name__c = empLastName;

        oRecord.Patient_Email_Address__c = patEmailConfirm;
        oRecord.Employee_Email_Address__c = empEmailConfirm;

        oRecord.Patient_DOB__c = patDob;
        oRecord.Employee_DOB__c = empDob;

        if (isMemberPatient == 'Yes') {
            oRecord.Relationship_to_Subscriber__c = 'Employee';
            patFirstName = empFirstName;
            patLastName = empLastName;
            oRecord.Patient_First_Name__c = patFirstName;
            oRecord.Patient_Last_Name__c = patLastName;
            patDob = empDob;
            oRecord.Patient_DOB__c = empDob;
            oRecord.Gender__c = oRecord.Employee_Gender__c;
            patEmail = empEmail;
            patEmailConfirm = empEmailConfirm;

            oRecord.Patient_Phone__c = oRecord.Employee_Phone__c;
            oRecord.Patient_Phone2__c = oRecord.Employee_Mobile_Phone__c;
            oRecord.Patient_Phone3__c = oRecord.Employee_Other_Phone__c;
        }
    }

    public void syncPatientandEmployeeAddress(){
        oRecord.Patient_Address__c = patAddress;
        oRecord.Employee_Address__c = empAddress;

        if (empAddySame == 'Yes') {
            patAddress = empAddress;
            oRecord.Patient_Address__c = empAddress;
            oRecord.Patient_City__c = oRecord.Employee_City__c;
            oRecord.Patient_State__c = oRecord.Employee_State__c;
            oRecord.Patient_Zip_Code__c = oRecord.Employee_Zip_Code__c;
            oRecord.Same_as_Patient_Address__c = true;
        }
        else{
            oRecord.Same_as_Patient_Address__c = false;
            patAddress = '';
            oRecord.Patient_Address__c = '';
            oRecord.Patient_City__c = '';
            oRecord.Patient_State__c = '';
            oRecord.Patient_Zip_Code__c = '';
        }
    }

    public selectOption[] getrelOpts(){
        relOpts = new selectOption[] {};
        relOpts.add(new selectOption('', ''));
        relOpts.add(new selectOption('Employee', 'Employee'));
        relOpts.add(new selectOption('Spouse', employeeLabel + '\'s Spouse'));
        relOpts.add(new selectOption('Child', employeeLabel + '\'s Child'));
        relOpts.add(new selectOption('Domestic Partner', employeeLabel + '\'s Domestic Partner'));
        return relOpts;
    }

    public selectOption[] gettimezones(){
        phoneType = new selectOption[] {};
        phoneType.add(new selectoption('', 'Timezone'));
        phoneType.add(new selectoption('Pacific', 'Pacific'));
        phoneType.add(new selectoption('Mountain', 'Mountain'));
        phoneType.add(new selectoption('Central', 'Central'));
        phoneType.add(new selectoption('Eastern', 'Eastern'));
        return phoneType;
    }

    public selectOption[] getbtc(){
        btc = new selectOption[] {};
        btc.add(new selectoption('', ''));
        btc.add(new selectoption('1', '1'));
        btc.add(new selectoption('2', '2'));
        btc.add(new selectoption('3', '3'));
        btc.add(new selectoption('4', '4'));
        btc.add(new selectoption('5', '5'));
        btc.add(new selectoption('6', '6'));
        btc.add(new selectoption('7', '7'));
        btc.add(new selectoption('8', '8'));
        btc.add(new selectoption('9', '9'));
        btc.add(new selectoption('10', '10'));
        btc.add(new selectoption('11', '11'));
        btc.add(new selectoption('12', '12'));

        return btc;
    }

    public selectOption[] getphoneType(){
        phoneType = new selectOption[] {};
        phoneType.add(new selectoption('', 'Type'));
        phoneType.add(new selectoption('Mobile', 'Mobile'));
        phoneType.add(new selectoption('Home', 'Home'));
        phoneType.add(new selectoption('Work', 'Work'));
        return phoneType;
    }

    public void validateCallBackTimeVF(){
        if (!validateCallBackTime(oRecord.Online_Intake_Callback_Date__c, oRecord.Online_Intake_Callback_Time__c, callBackAmPm)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'The call back time should be 24 hours in the future or further.'));
            return;
        }

        if (!validateCallBackTime(oRecord.Online_Intake_Callback_Date__c)) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, 'The call back time should be during the week.'));
            return;
        }
    }

    boolean validateCallBackTime(date callBackDate){
        if (callBackDate == null) {
            return true;
        }

        datetime dt = (DateTime)callBackDate;
        integer i = integer.valueof(dt.format('u'));

        if (i == 5 || i == 6) {
            return false;
        }
        return true;
    }

    boolean validateCallBackTime(date callBackDate, string callbackTime, string ampm){
        if (callBackDate == null || ampm == '' || ampm == null || callbackTime == null || callbackTime == '') {
            return true;
        }

        oRecord.Online_Intake_Callback_Time__c = callbackTime + ':00 ' + callBackAmPm;
        integer x = integer.valueof(callbackTime);
        if (callBackAmPm == 'PM') {
            x = x + 12;
        }
        datetime twentyfourhours = datetime.now().addHours(24);
        date d = callBackDate;

        Time myTime = Time.newInstance(x, 0, 0, 0);
        DateTime dt = DateTime.newInstance(d, myTime);

        if (twentyfourhours > dt) {
            return false;
        }
        return true;
    }
}