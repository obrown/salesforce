public with sharing class convertLead {

    id leadID;
    public account__c theAccount {get; set;}
    public Account__c[] acctList {get; private set;}
    
    public final lead__c theLead {get; private set;}
    
    public boolean error {get; private set;}
    
    public string oppID {get; private set;}
    public string oppName {get; set;}
    
    public string searchTerm {get; set;}
    
    public convertLead(){
        
        leadID = ApexPages.CurrentPage().getParameters().get('id');    
        
        theLead = [select Status__c,
                          name, 
                          Industry__c, 
                          Number_of_Employees__c,
                          Producer__c,
                          website__c,
                          recordtypeid from Lead__c where id = :leadID];
                          
        oppName = theLead.name;
        
        try{
            theAccount = [select name,Number_of_Employees__c, id, Industry__c, phone__c, Website__c from account__c where name = :theLead.name];
            searchTerm = theLead.Name;
        }catch(QueryException e){
            theAccount = new Account__c(name=theLead.name, Number_of_Employees__c=theLead.Number_of_Employees__c, Industry__c=theLead.Industry__c, website__c= theLead.website__c);
        }    
        
        acctList = new Account__c[]{};
        
    }

    public void createOpportunity(){
        error = false;
        try{
            
            Opportunity__c opp = new Opportunity__c();
            opp.account__c = theAccount.id;
            opp.Producer__c = theLead.Producer__c ;
            opp.Name = oppName;
            opp.Status__c = 'New';
            insert opp;
            
            oppID = opp.id;
            theLead.Status__c= 'Converted';
            theLead.Opportunity__c = opp.id;
            theLead.RecordTypeId = [select id from recordtype where sobjecttype = 'Lead__c' and name = 'converted'].id;
            update theLead;
            
        }catch(exception e){
            
        }
        
        
    }
    
    public void searchAccounts(){
        
        error = false;
        
        
        
        try{
            
            if(searchTerm != ''){
                searchTerm = searchTerm + '%';
                //'%' + 
                acctList = [select id, name, createdDate from Account__c where name like :searchTerm];
                
            }
        
        }catch(QueryException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }   
        
    }
    
    public void selectAcct(){
    
        string aid = ApexPages.CurrentPage().getParameters().get('acctID');
        ApexPages.CurrentPage().getParameters().put('acctID', null);
        
        for(Account__c a : acctList){
            
            if(a.id == aid){
                theAccount = a;
                break;
            }
            
        }
        
    }
    
    public void saveAccount(){
        error = false;
        try{
        
            insert theAccount;
        
        }catch(QueryException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }
    
}