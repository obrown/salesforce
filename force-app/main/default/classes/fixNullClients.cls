public without sharing class fixNullClients{
    
    public static void fixNullClients(){
        patient_case__c[] pclist = [select client__c,nclient_name__c,client_name__c from patient_case__c where  client_name__c =''];

        for(patient_case__c pc : pclist){
            pc.client_name__c = pc.nclient_name__c;
        }
        
        update pclist;
        
    }
    
    
    
}