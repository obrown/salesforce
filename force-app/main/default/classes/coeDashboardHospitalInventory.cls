public with sharing class coeDashboardHospitalInventory{

    transient patient_case__c[] CaseList;
    
    public transient set<string> openCaseSet {get; private set;}
    public transient set<string> openCaseInnerSet {get; set;}
    public transient map<string, map<string, integer>> OpenCaseCount {get; private set;}
    public transient map<string, integer> OpenCaseTotal {get; private set;}
    public transient map<string, map<string, patient_case__c[]>> OpenCase {get; private set;}
    
    sObject generic;
    string grouping = 'client_name__c';
    
    id coeID;
    
    public coeDashboardHospitalInventory(){
        string foo = ApexPages.CurrentPage().getParameters().get('sortByHI');
        
        if(foo=='client_name__c'||foo=='referred_facility__c'){
           grouping = foo;     
        }
        
        CaseList = [select case_type__c, ownerid ,program_type__c,converted_date__c, isConverted__c,Status__c,status_reason__c,CreatedDate,Name,lastModifiedDate, patient_first_name__c, patient_last_name__c, client_name__c, referred_facility__c, actual_departure__c,actual_arrival__c from patient_case__c where isconverted__c = true and status__c = 'Open' order by client_name__c];
        openCaseInnerSet = new set<string>();
        for(patient_case__c p :caselist){
            generic = p;
            openCaseInnerSet.add((string)generic.get(grouping));
        }
    }
    
    public void populateOpenCase(){
        
        openCaseSet = new set<string>();
        
        OpenCase  = new map<string, map<string, patient_case__c[]>>();
        OpenCaseCount = new map<string, map<string, integer>>();
        OpenCaseTotal = new map<string, integer>();
        
        inHouse();
        awaitingClaim();
        awaitingPOC();
        
        openCaseSet = OpenCase.keySet();
        
        map<string, integer> fooMap;

        /*
            Loop over the openCaseMap, ie CM, CS, Awaiting POC, etc
            Then loop over 
        */
        
                
        for(string ojkey : openCaseSet){
            
            integer i =0;
            fooMap=new  map<string, integer>();
            for(string s: openCaseInnerSet){
              fooMap.put(s,0);
            
            }

            this.OpenCaseCount.put(ojkey, fooMap);
            
            for(string ss : fooMap.keyset()){
                
                patient_case__c[] fooList = OpenCase.get(ojkey).get(ss);
                if(fooList==null){
                    fooList = new patient_case__c[]{};
                }
                
                fooMap.put(ss, fooList.size());
                i +=fooList.size();
                this.OpenCaseCount.put(ojkey, fooMap);
                
            }
            
            OpenCaseTotal.put(ojkey, i);
        }
        
    } 

    void awaitingPOC(){
    
        map<string, patient_case__c[]> ct = new map<string, patient_case__c[]>();
        
        for(string s : openCaseInnerSet){
            ct.put(s,new patient_case__c[]{});
        }
        
        patient_case__c[] plist = new patient_case__c[]{};
       
        for(patient_case__c p : CaseList){
          if(p.status_reason__c == 'Awaiting Plan of Care' && p.isConverted__c == true && p.status__c == 'Open'){
                generic = p;
                plist = ct.get((string)generic.get(grouping));
                
                if(plist ==null){
                    plist = new patient_case__c[]{};
                }
            
                plist.add(p);
                
            }
        
        }
        
        OpenCase.put('Awaiting POC', ct);
        
    }

    void awaitingClaim(){
    
        map<string, patient_case__c[]> ct = new map<string, patient_case__c[]>();
        
        for(string s : openCaseInnerSet){
            ct.put(s,new patient_case__c[]{});
        }
        
        patient_case__c[] plist = new patient_case__c[]{};
       
        for(patient_case__c p : CaseList){
          if(p.status__c == 'Open' && p.isConverted__c && (p.status_reason__c == 'Claim Pending' || p.status_reason__c == 'Claim received')){
                generic= p;
                plist = ct.get((string)generic.get(grouping));
                
                if(plist ==null){
                    plist = new patient_case__c[]{};
                }
            
                plist.add(p);
                
            }
        
        }
        
        OpenCase.put('Awaiting Claim', ct);
       // return ct;
    }
    
    void inHouse(){
    
        map<string, patient_case__c[]> ct = new map<string, patient_case__c[]>();
        
        for(string s : openCaseInnerSet){
            ct.put(s,new patient_case__c[]{});
        }
        
        patient_case__c[] plist = new patient_case__c[]{};
       
        for(patient_case__c p : CaseList){
          if(p.status__c == 'Open' && p.actual_arrival__c <= date.today() && p.actual_departure__c == null){
                generic= p;
                plist = ct.get((string)generic.get(grouping));
                
                if(plist ==null){
                    plist = new patient_case__c[]{};
                }
            
                plist.add(p);
                
            }
        
        }
        
        OpenCase.put('In House', ct);
        //return ct;
    }
        
}