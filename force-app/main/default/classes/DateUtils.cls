public with sharing class DateUtils{


       public static string dayOfWeek(datetime myDateTime){
           return myDateTime.format('u');
       }
       
       public static boolean isWeekDay(datetime myDateTime){
           string day = DateUtils.dayOfWeek(myDateTime);
           if(day == '6' || day == '7'){
               return false;
           }
           return true;
       }
        
}