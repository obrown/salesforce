global class coeIDcardScheduler implements Schedulable{
    
    global void execute(SchedulableContext go){
        coeIDCardAutomationBatching.run();
    }
    
}