public with sharing class multiReferralFormsController {

     public boolean trf {get; private set;}
     public Patient_Case__c obj {get;set;}
     public Transplant__c objTrans {get;set;}
     public string theattachviewID {get;set;}
     public string thetempID;
     public string finalattachID {get;set;}
     public string URL;
     string theID;
     public string retURL {get;set;}
     public string theTo {get;set;}      
     public string theOpp;
     public string theCC{get;set;}
     public string thebody {get;set;}
     public string theSubject {get;set;}
     public string errorMessage;
     public string error {get;set;}
     public boolean renderpdf {get;set;}
    
     public multiReferralFormsController() {
     theID = ApexPages.currentPage().getParameters().get('Id');
     
     obj = new Patient_Case__c();
            obj = [Select Referred_to_Vendor_Date__c,Original_Referral_Date__c,Client_Carrier__r.Carrier_Email__c, Previously_Referred_Facility__c ,Previously_Referred_Date__c ,Referred_Vendor__c,Referred_Vendor__r.Referral_Email_Contact__c,Referred_Vendor__r.facility__r.name,client_facility__r.Referral_Email_Contact__c,nReferred_Facility__c,client_facility__r.facility__r.name,client_facility__c,client__r.name, client__c, ecen_procedure__c,ecen_procedure__r.name, Referred_Facility__c,nProposed_Procedure__c,carrier_name__c,Is_a_Caregiver_Available__c, Referral_Type__c, coeOverride__c,Status_Reason_Sub_List__c,Status_Roll_Up__c,HealthPac_Plan_Name__c, case_type__c, Caregiver_Exception_Requested__c,Joint_Referral_facility_contact__c,isRelated__c,current_nicotine_user__c,RecordType.Name,converted_date__c,certID__c,member__c,Caregiver_DOBe__c,Patient_DOBe__c,Patient_Email_Address__c,Employee_DOBe__c,Patient_DOB__c,Employee_DOB__c,Employee_Primary_Health_Plan_ID__c,Relationship_to_the_Insured__c,Patient_Preferred_Phone__c,Patient_Gender__c,Patient_City__c,Patient_State__c, recent_testing__c,caregiver_name__c,caregiver_home_phone__c,Caregiver_Mobile__c,diagnosis__c,proposed_procedure__c,patient_symptoms__c,Other_Pertinent_Medical_Info_History__c,procedure_complexity__c,eligible__c,
                  Patient_Zip_Postal_Code__c,Patient_Country__c,attachEncrypt__c,Patient_Street__c,Patient_SSN__c,Patient_Work_Phone__c,Patient_Home_Phone__c,Patient_Mobile__c,isConverted__c,Medicare_As_Secondary_Coverage__c,Language_Spoken__c,Employee_Mobile__c,Employee_Preferred_Phone__c,Employee_Home_Phone__c,Employee_SSN__c,Employee_Work_Phone__c,Employee_Last_Name__c,Employee_First_Name__c,Employee_Street__c,Employee_City__c,Employee_State__c,Employee_Country__c,Employee_Zip_Postal_Code__c,Employee_gender__c,recordtypeID,BID__c,client_name__c,Referral_facility_contact__c,Patient_First_Name__c,Patient_Last_Name__c, Name,Employee_Primary_Health_Plan_Name__c, Expedited_Referral__c from Patient_Case__c where id = :theID];
            
            string cfid = ApexPages.CurrentPage().getParameters().get('cfid');
            string recordTypeName='';
            
            if(cfid!=null){
                client_facility__c cf = [select Referral_Email_Contact__c, facility__r.name from client_facility__c where id = :ApexPages.CurrentPage().getParameters().get('cfid')];
                obj.client_facility__c = cf.id;
                obj.Previously_Referred_Facility__c = obj.nReferred_facility__c;
                obj.Referred_Facility__c = cf.facility__r.name;
                obj.Previously_Referred_Date__c = obj.Converted_Date__c;
                obj.Transition_Reason__c = ApexPages.CurrentPage().getParameters().get('tr');
                recordTypeName = cf.facility__r.name+' '+obj.ecen_procedure__r.name;
                theTo=cf.Referral_Email_Contact__c;
                trf = true;
                
            }else{
                obj.referred_facility__c = obj.nReferred_Facility__c;
                    retURL = '/apex/pcredirect?id=' + obj.id;
                    recordTypeName = obj.client__r.Name+' '+obj.ecen_procedure__r.name;
                    trf=false;    
                if(obj.Referred_Vendor__c==null){
                    theTo=obj.client_facility__r.Referral_Email_Contact__c;
                }else{
                    theTo=obj.Referred_Vendor__r.Referral_Email_Contact__c;
                }
                
            }
                
            string casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the ' + obj.client__r.name + ' Centers of Excellence program. ';
            string casecontact ='1-877-885-0654 or HDPCOEClinical@contigohealth.com.';
            
            
            theCC = convertCase.hdpemail(obj.client__r.name)+', ' + UserInfo.getUserEmail();
            
            if(recordTypeName == 'Walmart Cardiac'){
                
                if(obj.isRelated__c){
                URL = '/apex/wmcardiacrelatedreferral?id=' + obj.id;
                }else{
                URL = '/apex/wmcardiacreferral?id=' + obj.id;   
                }
                casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the Walmart Centers of Excellence program. ';
                casecontact ='1-877-286-3551 or WMCOEClinical@contigohealth.com.';
                //theTo = obj.Referral_facility_contact__c;
            }
            
            if(recordTypeName == 'Walmart Spine'){
                URL = '/apex/spineReferralForm?id=' + obj.id;
                
                casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the Walmart Centers of Excellence program. ';
                casecontact ='1-877-286-3551 or WMCOEClinical@contigohealth.com.';
                
            }
            
            if(recordTypeName == 'Walmart Joint'){
                URL = '/apex/jointReferralForm?id=' + obj.id;
                
                casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the Walmart Centers of Excellence program. ';
                casecontact ='1-877-286-3551 or WMCOEClinical@contigohealth.com.';
            }
            
            if(recordTypeName == 'Lowes Joint'){
                URL = '/apex/jointReferralForm?id=' + obj.id;
                    casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the Lowe’s Centers of Excellence program. ';
                    casecontact ='1-877-885-0654 or HDPCOEClinical@contigohealth.com.';
                    
             }else if(recordTypeName == 'Lowes Spine'){
                    URL = '/apex/spineReferralForm?id=' + obj.id;
                    casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the Lowe’s Centers of Excellence program. ';
                    casecontact ='1-877-885-0654 or HDPCOEClinical@contigohealth.com.';
                    
             }else if(recordTypeName == 'Lowes Cardiac'){
                if(obj.isRelated__c){
                    URL = '/apex/lowescardiacrelatedreferral?id=' + obj.id;
                }else{
                    URL = '/apex/lowescardiacreferral?id=' + obj.id;    
                }                       
                    casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the Lowe’s Cleveland Clinic program. ';
                    casecontact ='1-877-885-0654 or HDPCOEClinical@contigohealth.com.';
             }
                
             if(recordTypeName == 'jetBlue Spine'){
                URL = '/apex/spineReferralForm?id=' + obj.id;
                
                casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the JetBlue Centers of Excellence Clinic program. ';
                casecontact ='1-844-207-8216 or HDPCOEClinical@contigohealth.com.';
                                    
             }else if(recordTypeName == 'McKesson Joint'){
                URL = '/apex/jointReferralForm?id=' + obj.id;
                
                casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the McKesson Centers of Excellence program. ';
                casecontact ='1-877-885-0654 or HDPCOEClinical@contigohealth.com.';
             }             

             if(recordTypeName == 'Kohls Cardiac'){
                if(obj.isRelated__c){
                    URL = '/apex/kohlscardiacrelatedreferral?id=' + obj.id;
                }else{
                    URL = '/apex/kohlscardiacreferral?id=' + obj.id;    
                }                   
                    casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the Cleveland Clinic Travel Surgery program. ';
                    casecontact ='1-877-885-0652 or HDPCOEClinical@contigohealth.com.';
             }

                if(recordTypeName == 'HCR ManorCare Cardiac'){
                if(obj.isRelated__c){
                    URL = '/apex/hcrCardiacrelatedreferral?id=' + obj.id;
                }else{
                    URL = '/apex/hcrCardiacreferral?id=' + obj.id;  
                }                   

                    casetype = 'Hello,\n\nAttached is the Contigo Health Referral Form for the HCR ManorCare Advanced Heart Care program. ';
                    casecontact ='1-877-885-0651 or HDPCOEClinical@contigohealth.com.';
                }

                if(recordTypeName == 'PepsiCo Cardiac'){
                if(obj.isRelated__c){
                    URL = '/apex/pepsicoCardiacrelatedreferral?id=' + obj.id;
                }else{
                    URL = '/apex/pepsicoCardiacreferral?id=' + obj.id;  
                }                   

                    casetype = 'Hello,\n\nAttached is the Contigo Health Johns Hopkins Travel Surgery Benefit Referral Form.';
                    casecontact ='1-877-286-3562 or HDPCOEClinical@contigohealth.com.';                 
                }
                
                if(recordTypeName == 'PepsiCo Joint'){
                if(obj.isRelated__c){
                    URL = '/apex/pepsicoJointRelatedreferral?id=' + obj.id;
                }else{
                    URL = '/apex/pepsicoJointreferral?id=' + obj.id;    
                }                   
                    casetype = 'Hello,\n\nAttached is the Contigo Health Johns Hopkins Travel Surgery Benefit Referral Form.';
                    casecontact ='1-877-286-3562 or HDPCOEClinical@contigohealth.com.';                  
                }
             
             
             if(URL==null){
                 
                 if(recordTypeName.contains('Digital Physical Therapy')){
                     URL = '/apex/digitalTherapyReferralForm?id=' + obj.id; 
                 }else if(recordTypeName.contains('Joint')){
                     URL = '/apex/jointReferralForm?id=' + obj.id; 
                 }else{
                     URL = '/apex/spineReferralForm?id=' + obj.id; 
                 }
             }
             
             if(obj.isRelated__c && !recordTypeName.contains('Digital Physical Therapy')){

                 theBody = casetype + 'Please note this referral is related to a previous referral for this patient. Referral data has been updated.\n\nThis is NOT a DUPLICATE referral.\n\nIf you have questions regarding this form or the process, please contact us at ' + casecontact + '\n\nThank you,\n\nCare Management Team\nContigo Health';
                 theSubject = recordTypeName  + ' COE Referral- ' + obj.Patient_Last_Name__c +', '+obj.Patient_First_Name__c;
                
             }else{ 
                
                 theBody = casetype +  'If you have questions regarding this form or the process, please contact us at ' + casecontact + '\n\nThank you,\n\nCare Management Team\nContigo Health';
                 theSubject = 'New Referral for ' + recordTypeName + ' COE– ' + obj.Patient_Last_Name__c +', '+obj.Patient_First_Name__c;
                 
                 
             }
             
             if(obj.Referred_Facility__c == 'Geisinger'){
                 theSubject = theSubject + ' - ' + obj.case_type__c;
             }
        
    }
    

    public void runrenderpdf(){
        renderpdf=true;
    } 
    
    public string getURL(){
    return URL;
    }
    
    public void selectedForm(){
        
    }
    
    public string geterrorMessage(){

        return errorMessage;
    }
    
    public void saveObj(){
        update obj;
        obj = [Select Referred_to_Vendor_Date__c,Original_Referral_Date__c,Client_Carrier__r.Carrier_Email__c,Referred_Vendor__r.facility__r.name,client_facility__r.facility__r.name,Previously_Referred_Facility__c ,Previously_Referred_Date__c ,client_facility__r.Referral_Email_Contact__c,nReferred_Facility__c,client_facility__c,client__r.name, client__c, ecen_procedure__c,ecen_procedure__r.name, Referred_Facility__c,nProposed_Procedure__c,carrier_name__c,Is_a_Caregiver_Available__c, Referral_Type__c, coeOverride__c,Status_Reason_Sub_List__c,Status_Roll_Up__c,HealthPac_Plan_Name__c, case_type__c, Caregiver_Exception_Requested__c,Joint_Referral_facility_contact__c,isRelated__c,current_nicotine_user__c,RecordType.Name,converted_date__c,certID__c,member__c,Caregiver_DOBe__c,Patient_DOBe__c,Patient_Email_Address__c,Employee_DOBe__c,Patient_DOB__c,Employee_DOB__c,Employee_Primary_Health_Plan_ID__c,Relationship_to_the_Insured__c,Patient_Preferred_Phone__c,Patient_Gender__c,Patient_City__c,Patient_State__c, recent_testing__c,caregiver_name__c,caregiver_home_phone__c,Caregiver_Mobile__c,diagnosis__c,proposed_procedure__c,patient_symptoms__c,Other_Pertinent_Medical_Info_History__c,procedure_complexity__c,eligible__c,
        Patient_Zip_Postal_Code__c,Patient_Country__c,attachEncrypt__c,Patient_Street__c,Patient_SSN__c,Patient_Work_Phone__c,Patient_Home_Phone__c,Patient_Mobile__c,isConverted__c,Medicare_As_Secondary_Coverage__c,Language_Spoken__c,Employee_Mobile__c,Employee_Preferred_Phone__c,Employee_Home_Phone__c,Employee_SSN__c,Employee_Work_Phone__c,Employee_Last_Name__c,Employee_First_Name__c,Employee_Street__c,Employee_City__c,Employee_State__c,Employee_Country__c,Employee_Zip_Postal_Code__c,Employee_gender__c,recordtypeID,BID__c,client_name__c,Referral_facility_contact__c,Patient_First_Name__c,Patient_Last_Name__c, Name,Employee_Primary_Health_Plan_Name__c, Expedited_Referral__c from Patient_Case__c where id = :theID];
        
            
    }
    
    public void formSelected(){
     error='false';
     errorMessage =null;
    
      if(theCC == '' || theCC == Null){
      }else{            
        if(theCC.contains('@') && theCC.contains('.')){
        }else{
            error='true';
            errorMessage = 'There seems to be an issue with the CC address.';
    
        }   
      }
      
      if(error=='false'){
        errorMessage=Null;
        list<String> theCClist = new list<String>();
        if(theCC.length()>0){
            if(theCC.contains(',')){
                theCClist = theCC.split(',', -2);
            }else{
                theCClist.add(theCC);
            }   
        }
        
        try{
             if(utilities.isRunningInSandbox()){
                 theTo = UserInfo.getUserEmail();
                 theCClist = new string[]{UserInfo.getUserEmail()};
             }
             
             convertCase cc = new convertCase();
             theTo= theTo.removeEnd(',');
             
            
             theOpp = cc.sendReferralEmail(obj, theSubject, theBody, theTo, theCClist);
             system.debug('theOpp '+theOpp);
             if(theOpp == ''){
                error = 'true';
                errorMessage = 'An error occurred';
             }
        }catch (exception e){
            error='true';
            errorMessage = e.getmessage()+' '+e.getlineNumber();
        }   
      } 
    
    }
    
    public string gettheOpp(){
      
       string foo= 'apex/pcredirect?id=' + theid;
        return foo;
    }
  
}