public with sharing class utilizationManagamentLetterPDFext{
 
    public Utilization_Management_Denial_Letter__c[] lettersList {get; private set;}
    public Utilization_Management__c um {get; private set;}
    
    set<string> lids = new set<string>();
    set<string> umids = new set<string>();
    set<id> umRecords = new set<id>();
    
    public utilizationManagamentLetterPDFext() {
        
        
        lettersList = new Utilization_Management_Denial_Letter__c[]{};
        string lid=Apexpages.currentpage().getParameters().get('lids');
        if(lid!=null){
            string[] lidsFoo = lid.split(':');
            
            lids.addAll(lidsFoo);
            for(Utilization_Management_Denial_Letter__c umm : [select letterText__c,Utilization_Management__c from Utilization_Management_Denial_Letter__c  where id in :lids]){                
                umids.add(umm.Utilization_Management__c);
                lettersList.add(umm);
            }
            
            //utilizationManagementWorkflow.lettersPrinted(lids);
            
           
        }
    }
 /*  
    2-15-22 code check in
    public Utilization_Management_Clinical_Code__c[] diagnosisCodes {get; private set;}
    public Utilization_Management_Clinical_Code__c[] clinicalCodes  {get; private set;}
    public map<id,Utilization_Management_Clinical_Code__c[]> diagnosisCodesMap {get; private set;}
    public map<id,Utilization_Management_Clinical_Code__c[]> clinicalCodesMap {get; private set;}
    public map<string, clinicianWrapperClass> clinicianMap {get; private set;}
    
    public Utilization_Management_Denial_Letter__c[] lettersList {get; private set;}
    
    string umRecordId;
    
    public utilizationManagamentLetterPDFext(ApexPages.StandardController controller) {
        //Utilization_Management_Denial_Letter__c um = (Utilization_Management_Denial_Letter__c)controller.getRecord();
    }
    
    void loadDiagnosisCodes(){
       
        diagnosisCodesMap = new map<id,Utilization_Management_Clinical_Code__c []>();
        Utilization_Management_Clinical_Code__c[] dCodes;
        if(!umids.isEmpty()){
            for(id umRecordId:umids){
                diagnosisCodesMap.put(umRecordId, new Utilization_Management_Clinical_Code__c[]{});
            }
            for(Utilization_Management_Clinical_Code__c umc : [select name, Utilization_Management__c, Code_Type__c,Description__c, createdDate,CreatedBy.name from Utilization_Management_Clinical_Code__c where Utilization_Management__c in :umids and code_type__c = 'Diagnosis' order by name desc]){
                dCodes = diagnosisCodesMap.get(umc.Utilization_Management__c);
                if(dCodes==null){
                    dCodes = new Utilization_Management_Clinical_Code__c[]{};
                }
                
                dcodes.add(umc);
                diagnosisCodesMap.put(umc.Utilization_Management__c, dCodes);
                system.debug(dcodes);
            }
            system.debug(diagnosisCodesMap);
        }
    
    }
    
    void loadClinicalCodes(){
        
        clinicalCodesMap = new map<id,Utilization_Management_Clinical_Code__c []>();
        Utilization_Management_Clinical_Code__c[] cCodes;
        
        if(!umids.isEmpty()){    
            for(id umRecordId:umids){
                clinicalCodesMap.put(umRecordId, new Utilization_Management_Clinical_Code__c[]{});
            }
            for(Utilization_Management_Clinical_Code__c umc : [select name, Utilization_Management__c,Code_Type__c,Description__c, createdDate,CreatedBy.name from Utilization_Management_Clinical_Code__c where Utilization_Management__c in :umids and code_type__c = 'Clinical' order by name desc]){
                cCodes = clinicalCodesMap.get(umc.Utilization_Management__c);
                if(cCodes==null){
                    cCodes = new Utilization_Management_Clinical_Code__c[]{};
                }
                
                cCodes.add(umc);
                clinicalCodesMap.put(umc.Utilization_Management__c, cCodes);
                
            }
        }
    
    }
    
    void loadClinicians(){
        clinicianMap = new map<string, clinicianWrapperClass>();
        
        if(!umids.isEmpty()){
            for(Utilization_Management_Clinician__c umc : [select City__c,Clinician_Fax__c,Contact_Name__c,Contact_Phone_Number__c,Credentials__c,
                                                                  First_Name__c,
                                                                  last_Name__c,
                                                                  Network_Status__c,
                                                                  Ordering_Clinician__c,
                                                                  State__c,Street__c,Salutation__c,
                                                                  Utilization_Management__c,
                                                                  Zip_Code__c from Utilization_Management_Clinician__c where Utilization_Management__c in :umids]){
                                                                  
                
                clinicianMap.put(umc.Utilization_Management__c, new clinicianWrapperClass(umc));
               
            }
        }
        
    }
    
    public class clinicianWrapperClass{
        
        public String clinicianFax {get; set;}
        public String contactName {get; set;}
        public String contactPhoneNumber {get; set;}
        public String firstName {get; set;}
        public String lastName {get; set;}
        
        public String Street {get; set;}
        public String city {get; set;}
        public String State {get; set;}
        public String ZipCode {get; set;}
        
        public clinicianWrapperClass(Utilization_Management_Clinician__c umc){
            if(umc.City__c!=null){
                city = umc.City__c.capitalize();
            }else{
                city= '';
            }
            clinicianFax = umc.Clinician_Fax__c;
            
            if(umc.Contact_Name__c!=null){
                contactName = umc.Contact_Name__c.capitalize();
            }else{
                contactName = '';
            }
            contactPhoneNumber = umc.Contact_Phone_Number__c;
            
            if(umc.First_Name__c!=null){
                firstName = umc.First_Name__c.capitalize();
            }else{
                firstName = '';
            }
            
            if(umc.last_Name__c!=null){
                lastName = umc.last_Name__c.capitalize();
            }else{
                lastName = '';
            }
            
            if(umc.Street__c!=null){
                street = umc.Street__c.capitalize();
            }else{
                street = '';
            }
            State = umc.State__c.capitalize();
            ZipCode = umc.Zip_Code__c.capitalize();
            
        }
    }
*/
}