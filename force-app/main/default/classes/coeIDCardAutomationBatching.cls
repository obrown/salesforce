public with sharing class coeIDCardAutomationBatching{

    public coeIDCardAutomationBatching(){}
    
    public static boolean run(){
        integer counter=10;
        list<id> pcIDs = new list<id>();
        for(patient_case__c pc : [select id from patient_case__c where Welcome_Packet_Sent__c = null and Plan_of_Care_accepted_at_HDP__c != null and estimated_departure__c != null and estimated_arrival__c != null and ID_Card_Requested__c = null and status__c = 'Open' limit 20]){
            pcIDs.add(pc.id);
        }
        
        for(integer i=0; i<pcIDs.size(); i=i+counter){
            set<id> idSet = new set<id>();
            integer upper = i+counter;
            if(upper>pcIds.size()){
                upper=pcIds.size();
            }
            
            for(integer lower=i;lower<upper;lower++){
                idset.add(pcIDs[lower]);
            }
            
            coeIDCardFuture.emailIDCardRequests(idset);
        }
        
        return true;
    }
    
    public static boolean runOncology(){
        integer counter=10;
        list<id> ids = new list<id>();
        for(oncology__c pc : [select id from oncology__c where Determination_Outcome__c ='Program Eligibility Met' and ID_Card_Requested__c = null]){
            ids.add(pc.id);
        }
        
        for(integer i=0; i<ids.size(); i=i+counter){
            set<id> idSet = new set<id>();
            integer upper = i+counter;
            if(upper>ids.size()){
                upper=ids.size();
            }
            
            for(integer lower=i;lower<upper;lower++){
                idset.add(ids[lower]);
            }
            
            coeIDCardFuture.emailOncologyIDCardRequests(idset);
        }
        
        return true;
    }

    public static boolean runBariatric(){
        integer counter=5;
        list<id> ids = new list<id>();
        for(Bariatric_Stage__c bs : [select id from Bariatric_Stage__c where Estimated_Arrival__c!=null and ID_Card_Requested__c=null]){
            ids.add(bs.id);
        }
        
        for(integer i=0; i<ids.size(); i=i+counter){
            set<id> idSet = new set<id>();
            integer upper = i+counter;
            if(upper>ids.size()){
                upper=ids.size();
            }
            
            for(integer lower=i;lower<upper;lower++){
                idset.add(ids[lower]);
            }
            
            coeIDCardFuture.emailBariatricIDCardRequests(idset);
        }
        
        return true;
    }    
}