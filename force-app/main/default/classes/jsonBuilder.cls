public without sharing class jsonBuilder{

    public string eligSearch(string[] str)
    {
         
         string result='{';
         str.add('Username:'+UserInfo.getUserName());
         str.sort();
         for(string s: str)
         {
            result += '"' +s.left(s.indexOf(':'))+'"';
            result += ':"' +s.right((s.length()-s.indexOf(':'))-1)+'",';
             
         }
         
         result = result.left(result.length()-1);   
         result += '}';  
         return result;   
    }

}