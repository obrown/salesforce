/**
* This class contains unit tests for validating the behavior of the Bariatric application */

@isTest(seealldata=true)
private class BariatricTest{
    
    
    public bariatric__c bariatric;
    public map<string, id> rtMap;
    
    public map<string, id> getrtMap(){
        
        if(rtMap==null){
            rtMap= new map<string, id>();
            for(recordtype rt : [select name, id from recordType where sobjecttype = 'Bariatric_stage__c' and isActive = true]){
                rtMap.put(rt.name, rt.id);
            }
            
        }
        
        return rtMap;
        
    } 
    
    public bariatric__c getbariatric(){
        
        if(bariatric==null){
            bariatric = new bariatric__c(); 
            bariatric.bid__c = '12345678';
            bariatric.status__c = 'Open';
            bariatric.status_reason__c = 'In Process';
            bariatric.Date_Provider_Verification_Form_Received__c = date.today().addDays(-8);
            bariatric.Date_Provider_Verification_Form_Signed__c = date.today();
            bariatric.Intake_Information_Complete__c = 'Yes';
            bariatric.Date_Intake_Information_is_Complete__c = date.today();
            bariatric.Referred_Facility__c = 'Geisinger';
            bariatric.employee_first_name__c = 'Unit';
            bariatric.employee_last_name__c = 'Test';
            bariatric.employee_gender__c = 'Male';
            date dob = date.today().addyears(-35);
            
            bariatric.employee_dob__c = dob.month()+'/'+dob.day()+'/'+dob.year();
            bariatric.employee_home_phone__c = '(480) 555-1212';
            bariatric.employee_email_Address__c = 'none@none.gmail.com';
            bariatric.employee_SSN__c ='1234';
            bariatric.Employee_Street__c ='123 E Main St';
            bariatric.Employee_City__c ='Mesa';
            bariatric.Employee_State__c ='AZ';
            bariatric.Employee_Zip_Code__c='12345';
            
            bariatric.patient_gender__c = 'Male';
            bariatric.patient_first_name__c = 'Unit';
            bariatric.patient_last_name__c = 'Test';
            bariatric.patient_dob__c = dob.month()+'/'+dob.day()+'/'+dob.year();
            bariatric.patient_home_phone__c = '(480) 555-1212';
            bariatric.patient_email_Address__c = 'none@none.gmail.com';    
            bariatric.patient_SSN__c ='1234';
            bariatric.patient_Street__c ='123 E Main St';
            bariatric.patient_City__c ='Mesa';
            bariatric.patient_State__c ='AZ';
            bariatric.patient_Zip_Code__c='12345';
            
            bariatric.Patient_Height_FT__c = '5';
            bariatric.Patient_Height_Inches__c= '6';
            bariatric.Patient_Weight__c = '300';
            bariatric.Relationship_to_the_Insured__c ='Patient is Insured';
            bariatric.Type_2_Diabtetic__c = 'Yes';
            bariatric.current_nicotine_user__c = 'No';
            
            bariatric.COB_Received__c = date.today(); 
        }
        return bariatric;
    }

    static testMethod void BariatricCancelledAppointmentsTest() {
        
        Test.StartTest();
        
        BariatricTest bt = new BariatricTest(); 
        bariatric__c bariatric = bt.getbariatric();
        
        bariatric.Eligible__c = 'Yes';
        bariatric.carrier_name__c = 'UHC';
        bariatric.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        bariatric.Verified_Employee_DOB__c = true;
        bariatric.Verified_Employee_SSN__c = true;
        bariatric.Verified_Patient_DOB__c = true;
        bariatric.Verified_Patient_SSN__c = true;
        bariatric.patient_ssn__c='123456789';
        bariatric.employee_ssn__c='123456789';
        
        BariatricWorkflow.recommendedFacility(new bariatric__c[]{bariatric});
        
        bariatric.referred_facility__c = bariatric.recommended_facility__c;
        
        insert bariatric;
        
        bariatricController bc = new bariatricController(bariatric.id);
        
        bc.obj.Referral_Date__c = date.today();
        
        update bc.obj;
        bc.preStage = new Bariatric_Stage__c(Bariatric__c=bariatric.id, recordtypeid=bt.getrtMap().get('Pre'));
        bc.preStage.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        bc.savePre();
        
        //system.assert(bc.preStage.id != '');
        
        bc.newCancelledAppointment();
        
        bc.ca.Date_cancelled__c = date.today();
        bc.ca.Cancelled_by__c = 'UNIT.TEST';
        bc.ca.Estimated_Arrival__c = date.today();
        bc.ca.Cancelled_Reason__c = 'UNIT.TEST';
        bc.ca.Evaluation_Date__c = date.today();
        bc.ca.Estimated_Departure__c = date.today();
        
        bc.saveCancelledAppointment();
        ApexPages.currentPage().getParameters().put('caID', bc.ca.id);
        bc.getCancelledAppointment();
        
        ApexPages.currentPage().getParameters().put('caID', bc.ca.id);
        bc.deleteCancelledAppointment();
        
        bc.globalStage = new Bariatric_Stage__c(Bariatric__c=bc.obj.id, recordtypeid=bt.getrtMap().get('Global'));
        insert bc.globalStage;
        
        bc.newGlobalCancelledAppointment();
        
        bc.ca.Date_cancelled__c = date.today();
        bc.ca.Cancelled_by__c = 'UNIT.TEST';
        bc.ca.Estimated_Arrival__c = date.today();
        bc.ca.Cancelled_Reason__c = 'UNIT.TEST';
        bc.ca.Evaluation_Date__c = date.today();
        bc.ca.Estimated_Departure__c = date.today();
        
        bc.saveCancelledAppointment();
        
        ApexPages.currentPage().getParameters().put('caID', bc.ca.id);
        bc.getCancelledAppointment();
        
        Test.StopTest();
        
    }       
   
    static testMethod void BariatricReferralFormTest() {
        
        Test.StartTest();
        
        BariatricTest bt = new BariatricTest(); 
        bariatric__c bariatric = bt.getbariatric();
        
        bariatric.Eligible__c = 'Yes';
        bariatric.carrier_name__c = 'UHC';
        bariatric.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        bariatric.Verified_Employee_DOB__c = true;
        bariatric.Verified_Employee_SSN__c = true;
        bariatric.Verified_Patient_DOB__c = true;
        bariatric.Verified_Patient_SSN__c = true;
        bariatric.patient_ssn__c='123456789';
        bariatric.employee_ssn__c='123456789';
        bariatric.Referral_Date__c = date.today();
        insert bariatric;
        
        bariatric_Stage__c preStage = new Bariatric_Stage__c(Bariatric__c=bariatric.id, recordtypeid=bt.getrtMap().get('Pre'));
        preStage.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        insert preStage;
        
        bariatricController bc = new bariatricController(bariatric.id);
        
        bariatricReferralFormController refForm = new bariatricReferralFormController(new ApexPages.StandardController(bc.obj));
        
        ApexPages.CurrentPage().getParameters().put('plName', bc.obj.patient_last_name__c);
        ApexPages.CurrentPage().getParameters().put('pfName', bc.obj.patient_first_name__c);
        ApexPages.CurrentPage().getParameters().put('eFname', bc.obj.employee_last_name__c);
        ApexPages.CurrentPage().getParameters().put('eLname', bc.obj.employee_first_name__c);
        ApexPages.CurrentPage().getParameters().put('pSSN', bc.obj.Patient_SSN__c);
        ApexPages.CurrentPage().getParameters().put('pCity', bc.obj.Patient_City__c);
        ApexPages.CurrentPage().getParameters().put('pStreet', bc.obj.Patient_Street__c);
        ApexPages.CurrentPage().getParameters().put('pZip', bc.obj.Patient_Zip_Code__c);
        ApexPages.CurrentPage().getParameters().put('pHomePhone', bc.obj.Patient_Home_Phone__c);
        ApexPages.CurrentPage().getParameters().put('pMobilePhone', bc.obj.Patient_Mobile_Phone__c);
        ApexPages.CurrentPage().getParameters().put('pBID', bc.obj.certID__c);
        ApexPages.CurrentPage().getParameters().put('pDOB', bc.obj.Patient_DOB__c);
        refForm = new bariatricReferralFormController(new ApexPages.StandardController(bc.obj));
        
        Test.StopTest();
        
    }   
    
    static testMethod void BariatricPreStageTest() {
        
        Test.StartTest();
        
        BariatricTest bt = new BariatricTest(); 
        bariatric__c bariatric = bt.getbariatric();
        
        bariatric.Eligible__c = 'Yes';
        bariatric.carrier_name__c = 'UHC';
        bariatric.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        bariatric.Verified_Employee_DOB__c = true;
        bariatric.Verified_Employee_SSN__c = true;
        bariatric.Verified_Patient_DOB__c = true;
        bariatric.Verified_Patient_SSN__c = true;
        bariatric.patient_ssn__c='123456789';
        bariatric.employee_ssn__c='123456789';
        
        BariatricWorkflow.recommendedFacility(new bariatric__c[]{bariatric});
        
        bariatric.referred_facility__c = bariatric.recommended_facility__c;
        
        insert bariatric;
        
        bariatricController bc = new bariatricController(bariatric.id);
        
        bc.refer();
        
        system.assert(bc.error==false);
        system.assert(bc.obj.Referral_Date__c != null);
        
        bc.authRequirements();
        
        system.assert(bc.error==false);
        system.assert(bc.obj.Referral_Date__c != null);
        system.assert(bc.preStage.id!=null);
        
        ApexPages.currentPage().getParameters().put('stage', 'foo');
        bc.sendFacilityPatientPaymentInformation();
        
        ApexPages.currentPage().getParameters().put('stage', bc.preStage.id);
        
        bc.obj.coverage_level__c = 'Associate Only';
        bc.preStage.coverage_level__c = 'Associate Only';
        bc.preStage.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        bc.preStage.Deductible_Met__c = 1000.00;
        bc.preStage.Out_of_Pocket_Met_Individual__c = 1000.00;
        bc.sendFacilityPatientPaymentInformation();
        
        
        bc.saveClaimInfo();
        
        /* ********************* Test Pre Auto-Statusing ********************** */
        
        bc.preStage.Information_Packet_Sent__c= date.today();
        bc.preStage.Plan_of_Care_Accepted_at_HDP__c= null;
        bc.preStage.Patient_Intro_Call_Completed_Date__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').subStatus_Reason__c=='Introduction Call');
        
        bc.preStage.Patient_Intro_Call_Completed_Date__c= date.today();
        bc.preStage.Educational_Seminar_Completed_Date__c = null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').subStatus_Reason__c=='Educational Webinar');
        
        bc.preStage.Patient_Intro_Call_Completed_Date__c= date.today();
        bc.preStage.Educational_Seminar_Completed_Date__c =  date.today();
        bc.preStage.Patient_Questionnaire_Completed_Date__c = null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').subStatus_Reason__c=='Patient Questionnaire');
        
        bc.preStage.Patient_Intro_Call_Completed_Date__c= date.today();
        bc.preStage.Educational_Seminar_Completed_Date__c = date.today();
        bc.preStage.Patient_Questionnaire_Completed_Date__c = date.today();
        bc.preStage.Medical_Records_Received_Date__c = null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').subStatus_Reason__c=='Medical Records');
        
        bc.preStage.Plan_of_Care_Accepted_at_HDP__c= date.today();
        bc.preStage.Program_Forms_Received_Date__c= null;
        bc.preStage.COB_Received__c= null;
        bc.preStage.Caregiver_Form_Received__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').Status_Reason__c=='Awaiting Forms');
        
        bc.preStage.Plan_of_Care_Accepted_at_HDP__c= date.today();
        bc.preStage.Program_Forms_Received_Date__c= date.today();
        bc.preStage.COB_Received__c= date.today();
        bc.preStage.Caregiver_Form_Received__c= date.today();
        bc.preStage.Estimated_Arrival__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').Status_Reason__c=='Accepted- Appt date');
        
        bc.preStage.Auth_Number_Emailed__c= date.today();
        bc.preStage.Plan_of_Care_Accepted_at_HDP__c= date.today();
        bc.preStage.Estimated_Arrival__c= date.today();
        bc.preStage.actual_arrival__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').Status_Reason__c=='Ready- Eval');
        
        bc.preStage.actual_departure__c = null;
        bc.preStage.actual_arrival__c= date.today();
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').Status_Reason__c=='Patient at COE');
        
        bc.preStage.actual_departure__c = date.today();
        bc.preStage.claim_received__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').Status_Reason__c=='Claim Pending- Eval');
        
        bc.preStage.claim_received__c= date.today();
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').Status_Reason__c=='Claim Received- Eval');
        
        bc.preStage.Claim_Paid__c= date.today();
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.preStage, 'pre').Status_Reason__c=='Claim Paid- Eval');
        
        /* ********************* End Test Pre Auto-Statusing ********************** */
        
        bc.preStage.Estimated_Arrival__c = date.today();
        bc.preStage.Estimated_Departure__c = date.today();
        bc.preStage.Evaluation_Date__c = date.today();
        bc.preStage.Educational_Seminar_Completed__c= 'Yes';
        bc.preStage.Patient_Questionnaire_Completed__c= 'Yes';
        bc.preStage.Patient_Intro_Call_Completed__c = 'Yes';
        bc.preStage.Patient_Intro_Letter_Sent__c = date.today();
        bc.preStage.ROI_Returned__c= 'Yes';
        bc.preStage.Medical_Records_Received__c= 'Yes';
        bc.preStage.Additional_Diagnostics_Ordered__c= 'Yes';
        bc.preStage.Determination_Received_at_HDP__c= date.today();
        bc.preStage.Patient_Accepted_for_Evaluation_Visit__c= 'Yes';
        bc.preStage.Plan_of_Care_Type__c= 'Evaluation';
        bc.preStage.Sleep_Apnea_Test__c= 'No';
        bc.preStage.Current_Weight__c = '300';
        bc.preStage.Verification_of_Patient_Payment_to_COE__c= 'No';
        bc.preStage.Transportation_Type__c= 'No';
        bc.preStage.Hotel__c= 'UNIT.TEST';
        bc.preStage.Hotel_Check_in__c= date.today();
        bc.preStage.Hotel_Check_out__c= date.today();
        bc.preStage.Actual_Arrival__c= date.today();
        bc.preStage.Actual_Departure__c= date.today();
        bc.preStage.patient_Intro_Call_Completed_Date__c=date.today();
        bc.preStage.Educational_Seminar_Completed_Date__c=date.today();
        bc.preStage.Patient_Questionnaire_Completed_Date__c=date.today();
        bc.preStage.Medical_Records_Received_Date__c=date.today();
        bc.preStage.Program_Forms_Received_Date__c=date.today();
        bc.preStage.Plan_of_Care_Accepted_at_HDP__c=date.today();
        bc.preStage.PHI_Release_Date__c=date.today();
        bc.preStage.Program_Forms_Received_Date__c=date.today();
        
        bc.savePre();
        
        bc.sendAuthEmailPre();
        ApexPages.currentPage().getParameters().put('stageid', bc.preStage.id);
        bc.sendCarrierEmail();
        ApexPages.currentPage().getParameters().put('stage', 'pre');
        bc.WLSClaimsEmail();
        
        bc.preStage.coverage_level__c = 'Associate and Dependent(s)';
        bc.preStage.Sleep_Apnea_Test__c = 'Yes';
        bc.updatePreClaim();
        
        Test.StopTest();
        
    }  

    static testMethod void BariatricGlobalStageTest() {
        Test.StartTest();
        
        BariatricTest bt = new BariatricTest(); 
        bariatric__c bariatric = bt.getbariatric();
        
        bariatric.Eligible__c = 'Yes';
        bariatric.carrier_name__c = 'UHC';
        bariatric.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        bariatric.Verified_Employee_DOB__c = true;
        bariatric.Verified_Employee_SSN__c = true;
        bariatric.Verified_Patient_DOB__c = true;
        bariatric.Verified_Patient_SSN__c = true;
        bariatric.patient_ssn__c='123456789';
        bariatric.employee_ssn__c='123456789';
        bariatric.current_nicotine_user__c = 'Yes';
        
        BariatricWorkflow.recommendedFacility(new bariatric__c[]{bariatric});
        bariatric.referred_facility__c = bariatric.recommended_facility__c;
        
        insert bariatric;
        
        bariatricController bc = new bariatricController(bariatric.id);
        
        bc.refer();
        bc.createGlobal();
        
        bc.globalStage.X2nd_Nutritional_Counseling_Date__c = date.today();
        bc.globalStage.Determination_Received_at_HDP__c = date.today();
        bc.globalStage.Patient_Accepted_for_Weight_Loss_Surgery__c = 'Yes';
        bc.globalStage.Caregiver_Form_Received__c = date.today();
        bc.preStage.PHI_Release_Date__c = date.today().addYears(-1).addDays(-60);
        bc.globalStage.Program_Forms_Received_Date__c = date.today().addYears(-1).addDays(-60);
        bc.globalStage.COB_Received__c = date.today().addYears(-1).addDays(-60);
        bc.globalStage.Procedure__c = date.today();
        
        bc.globalStage.Plan_of_Care_Accepted_at_HDP__c = date.today();
        
        bc.globalStage.Proposed_Procedure__c = 'Sleeve';
        bc.globalStage.Proposed_Clinical_Code__c = '621';
        bc.globalStage.Estimated_Arrival__c = date.today();
        bc.globalStage.Estimated_Departure__c = date.today();
        bc.globalStage.Estimated_Pre_Op__c = date.today();
        bc.globalStage.Estimated_Hospital_Admit__c = date.today();
        bc.globalStage.Estimated_Procedure__c = date.today();
        bc.globalStage.Estimated_Hospital_Discharge__c= date.today();
        bc.globalStage.Caregiver_Required__c = 'No';
        bc.globalStage.Patient_Cleared_for_Surgery__c = 'Yes';
        bc.globalStage.Transportation_Type__c = 'Drive';
        bc.globalStage.Hotel__c = 'Transylvania'; 
        
        bc.globalStage.Deductible_Met__c= 5000.00;
        bc.globalStage.Out_of_Pocket_Met_Individual__c = 5000.00;
        
        bc.globalStage.Actual_Arrival__c = date.today();
        bc.globalStage.Actual_Departure__c = date.today();
        bc.globalStage.Hospital_Admit__c = date.today();
        bc.globalStage.Hospital_Discharge__c = date.today();
        bc.globalStage.Clear_to_Travel_Appointment__c = date.today();
        
        
        update bc.preStage;  
        bc.saveGlobal();
        bc.globalAuthRequirements();
        
        bc.preStage.PHI_Release_Date__c = date.today();
        bc.globalStage.COB_Received__c = date.today();
        bc.globalStage.Program_Forms_Received_Date__c = date.today();
        
        update bc.preStage;  
        bc.saveGlobal();
        bc.globalAuthRequirements();
        bc.sendAuthEmailGlobal();
        bc.sendFacilityPatientPaymentInformation();
        
        bc.saveNicotine();
        bc.newNicoNote();
        bc.saveClaimInfo();

        /* ********************* Test Global Auto-Statusing ********************** */
        
        bc.globalStage.Plan_of_Care_Accepted_at_HDP__c= date.today();
        bc.globalStage.Program_Forms_Received_Date__c= null;
        bc.globalStage.COB_Received__c= null;
        bc.globalStage.Caregiver_Form_Received__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.globalStage, 'global').Status_Reason__c=='Awaiting Forms');
        
        bc.globalStage.Plan_of_Care_Accepted_at_HDP__c= date.today();
        bc.globalStage.Program_Forms_Received_Date__c=  date.today();
        bc.globalStage.COB_Received__c=  date.today();
        bc.globalStage.Caregiver_Form_Received__c=  date.today();
        bc.globalStage.Estimated_Arrival__c=  null;
        bc.globalStage.Auth_Number_Emailed__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.globalStage, 'global').Status_Reason__c=='Accepted- Appt date');
        
        bc.globalStage.Auth_Number_Emailed__c= date.today();
        bc.globalStage.actual_arrival__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.globalStage, 'global').Status_Reason__c=='Ready- Surgery');
        
        bc.globalStage.COB_Received__c=  date.today();
        bc.globalStage.Caregiver_Form_Received__c=  date.today();
        bc.globalStage.Estimated_Arrival__c=  date.today();
        bc.globalStage.actual_arrival__c= date.today();
        bc.globalStage.actual_departure__c=null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.globalStage, 'global').Status_Reason__c=='Patient at COE');
        
        bc.globalStage.actual_departure__c= date.today();
        bc.globalStage.claim_received__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.globalStage, 'global').Status_Reason__c=='Claim Pending- Global');
        
        bc.globalStage.claim_received__c= date.today();
        bc.globalStage.claim_paid__c= null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.globalStage, 'global').Status_Reason__c=='Claim Received- Global');
        
        bc.globalStage.claim_paid__c= date.today();
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, bc.globalStage, 'global').Status_Reason__c=='Claim Paid- Global');
        
        /* ********************* End Test Global Auto-Statusing ********************** */
        
        ApexPages.currentPage().getParameters().put('stageid', bc.globalStage.id);
        bc.sendCarrierEmail();
        
        bc.globalStage.Plan_of_Care_Accepted_at_HDP__c = date.today();
        bc.globalStage.Estimated_Arrival__c = date.today();
        bc.globalStage.Estimated_Hospital_Admit__c = date.today();
        bc.globalStage.Estimated_Procedure__c = date.today();
        bc.globalStage.Estimated_Hospital_Discharge__c  = date.today();
        bc.globalStage.Estimated_Cleared_to_Travel__c  = date.today();
        bc.globalStage.Estimated_Departure__c  = date.today();
        
        bc.saveGlobal();
        
        Test.StopTest();         
        
    }
    
    static testMethod void BariatricIntakeTest() {
        
        Test.StartTest();
        
        BariatricTest bt = new BariatricTest(); 
        string clientId = [select id,client__c from client_facility__c where procedure__r.name ='Weight Loss Surgery' limit 1 ].client__c;
        ApexPages.CurrentPage().getParameters().put('client',clientId);
        system.debug('clientId '+clientId);
        bariatricController bc = new bariatricController();
        bc.obj = bt.getbariatric();
        
        bc.mySave();
        bc.getOffset();
        
        system.assert(bc.obj.id != null);
        system.assert(bc.obj.encyptionKey__c!='');
        
        bc = new bariatricController(bc.obj.id);
        
        bc.obj.Patient_Height_FT__c = '5';
        bc.obj.Patient_Height_Inches__c= '6';
        bc.obj.Patient_Weight__c = '110';
        bc.obj.Type_2_Diabtetic__c = 'No';
        bc.mySave();
        
        bc.obj.Type_2_Diabtetic__c = 'Yes';
        bc.mySave();
        
        bc.obj.Patient_Weight__c = '300';
        bc.obj.Type_2_Diabtetic__c = 'No';
        bc.mySave();
        //added 9-9-21 ----------start--------------
        bc.obj.Type_2_Diabtetic__c = 'Yes';
        bc.obj.Medication_List__c='Not Included';//added 9-9-21        
        bc.obj.Patient_Height_FT__c = '5';
        bc.obj.Patient_Height_Inches__c= '6';
        bc.obj.Patient_Weight__c = '245';
        bc.mySave();

        bc.obj.Type_2_Diabtetic__c='No';
        bc.obj.Respiratory_Disorders__c= 'No';
        bc.obj.Osteoarthritis__c= 'No';
        bc.obj.Lipid_Abnormalities__c= 'No';        
        bc.obj.Gastrointestinal_Disorders__c= 'No';
        bc.obj.hypertension__c= 'No';
        bc.mySave();
        
        bc.obj.Referred_Facility__c = 'St. Vincent';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'HSA(Saver Plan)';
        bc.mySave();
          
        //added 9-9-21 ----------end--------------
        
        
        //added 9-10-21   -----------start---------------------
       //test plan names
        bc.obj.Referred_Facility__c = 'St. Vincent';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'HSA(Saver Plan)';
        bc.mySave();
                
        bc.obj.Referred_Facility__c = 'Northwest Medical Center';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'HSA(Saver Plan)';
        bc.mySave();        

        bc.obj.Referred_Facility__c = 'Northeast Baptist';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'HSA(Saver Plan)';
        bc.mySave();
                
        bc.obj.Referred_Facility__c = 'Scripps';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'HSA(Saver Plan)';
        bc.mySave();         

        bc.obj.Referred_Facility__c = 'University Hospitals';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'ACP (Banner)';
        bc.mySave();          

        bc.obj.Referred_Facility__c = 'Ochsner Health System';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'HSA(Saver Plan)';
        bc.mySave();         

        bc.obj.Referred_Facility__c = 'Non-Network';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'HSA(Saver Plan)';
        bc.mySave();   
        
        bc.obj.Referred_Facility__c = 'Emory';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'Walmart Saver Plan';
        bc.mySave();              

        bc.saveNote();
        bc.newNote();
        bc.editNote(); 
        bc.getnotesList();  
        //-----------end---------------------
        
        bc.submitForEligibility();
        
        bc.obj.Eligible__c = 'Yes';
        bc.obj.carrier_name__c = 'UHC';
        bc.obj.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        bc.obj.Verified_Employee_DOB__c = true;
        bc.obj.Verified_Employee_SSN__c = true;
        bc.obj.Verified_Patient_DOB__c = true;
        bc.obj.Verified_Patient_SSN__c = true;
        
        bc.mySave();
        
        system.assert(bc.obj.Date_Eligibility_Check_with_Client__c  != null);
        
        bc.refer();
        bc.sendCarrierNotificationEmail('scheduledEval');//added 9-10-21 o.brown

        system.assert(bc.error==true);
        
        ApexPages.currentPage().getParameters().put('status', 'Open');
        bc.statusChange();
        
        ApexPages.currentPage().getParameters().put('statusReason', 'In Process');
        bc.statusReasonChange();
        /* ********************* Test Intake Auto-Statusing ********************** */
        
        bc.obj.Relationship_to_the_Insured__c ='Other';
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, null, 'pre').substatus_reason__c=='Dependent');
        
        bc.obj.Relationship_to_the_Insured__c ='Patient is Insured';
        bc.obj.Eligible__c ='No';
        
        //system.assert(BariatricWorkflow.autoStatus(bc.obj, null, 'pre').substatus_reason__c=='Health Plan'); o.brown removed causing test to fail


        bc.obj.Eligible__c ='Yes';
        bc.obj.Date_Provider_Verification_Form_Received__c = null;
        
        system.assert(BariatricWorkflow.autoStatus(bc.obj, null, 'pre').status_reason__c=='Inquiry');
        
        /* ********************* End Test Intake Auto-Statusing ********************** */
        
        //bariatricWorkflow.referralCriteriaMet
        bariatricWorkflow.referralCriteriaMet(new bariatric__c());
        
        Test.stopTest();
        
    }   
    
    static testMethod void wlsPatientResponsiblityTest() {
        
        wlsPatientResponsiblity.PreResponsibilityAmount(0.00, 188.00, 'Scripps Mercy');
        wlsPatientResponsiblity.PreResponsibilityAmount(0.00, 180.00, 'Geisinger');
        wlsPatientResponsiblity.PreResponsibilityAmount(0.00, 0.00, 'Scripps Mercy');
        wlsPatientResponsiblity.PreResponsibilityAmount(755.00,0.00, 'Geisinger');
        wlsPatientResponsiblity.PreResponsibilityAmount(200.00, 1080.00,'Geisinger');
        wlsPatientResponsiblity.PreResponsibilityAmount(200.00, 50.00, 'Scripps Mercy');
        
        wlsPatientResponsiblity.GlobalResponsibilityAmount(0.00,100.00,'Scripps Mercy','','619');
        wlsPatientResponsiblity.GlobalResponsibilityAmount(10000.00,null,'Scripps Mercy','','620');
        wlsPatientResponsiblity.GlobalResponsibilityAmount(100.00,null,'Scripps Mercy','','621');
        
        wlsPatientResponsiblity.GlobalResponsibilityAmount(0.00,100.00,'Geisinger','Bypass','620');
        wlsPatientResponsiblity.GlobalResponsibilityAmount(0.00,100.00,'Geisinger','Bypass','621');
        wlsPatientResponsiblity.GlobalResponsibilityAmount(10000.00,null,'Geisinger','Sleeve','620');
        wlsPatientResponsiblity.GlobalResponsibilityAmount(100.00,null,'Geisinger','','621');
        
        wlsPatientResponsiblity.GlobalResponsibilityAmount(0.00,100.00,'Northeast Baptist','Bypass','620');
        wlsPatientResponsiblity.GlobalResponsibilityAmount(0.00,100.00,'Northeast Baptist','Bypass','621');
        wlsPatientResponsiblity.GlobalResponsibilityAmount(10000.00,null,'Northeast Baptist','Sleeve','620');
        wlsPatientResponsiblity.GlobalResponsibilityAmount(100.00,null,'Northeast Baptist','','621');
        
        wlsPatientResponsiblity.oopRemaining(null, 50.00, 50.00);
        wlsPatientResponsiblity.oopRemaining('Associate Only', 50.00, 50.00);
        wlsPatientResponsiblity.oopRemaining('Associate AND DEP', 45.00, 50.00);
        wlsPatientResponsiblity.oopRemaining('Associate AND DEP', 55.00, 50.00);
        
        wlsPatientResponsiblity.deductibleRemaining(null, 50.00);
        wlsPatientResponsiblity.deductibleRemaining(100.00, 50.00);
        
    }
    
    static testMethod void BariatricStatusTest(){
        
        //Test bariatricStatus class
        
        bariatricStatus.status('Pre');
        
        bariatricStatus.statusReason('Open',null);
        bariatricStatus.statusReason('Pend','Intake');
        bariatricStatus.statusReason('Closed','Intake');
        
        bariatricStatus.statusReason('Open','PRE');
        bariatricStatus.statusReason('Pend','PRE');
        bariatricStatus.statusReason('Closed','PRE');
        bariatricStatus.statusReason('Completed','PRE');
        
        bariatricStatus.statusReason('Open','global');
        bariatricStatus.statusReason('Pend','global');
        bariatricStatus.statusReason('Closed','global');
        bariatricStatus.statusReason('Completed','global');
        bariatricStatus.statusReason('Open','post');
        bariatricStatus.statusReason('Pend','post');//9-13-21
        bariatricStatus.statusReason('Closed','post');//9-13-21
        bariatricStatus.statusReason('Completed','post');//9-13-21
        
        bariatricStatus.subStatusReason('In Process','Intake');
        bariatricStatus.subStatusReason('Ineligible - Clinical','Intake');
        bariatricStatus.subStatusReason('Inquiry','Intake');
        bariatricStatus.subStatusReason('No Benefit Eligibility',null);
        
        
        bariatricStatus.subStatusReason('Pre-Qualification - In Process','PRE');
        bariatricStatus.subStatusReason('Pre-Qualification','PRE');
        bariatricStatus.subStatusReason('Cancelled By Patient','PRE');
        bariatricStatus.subStatusReason('Cancelled By Facility','PRE');
        bariatricStatus.subStatusReason('Clinical','PRE');
        bariatricStatus.subStatusReason('OON Negotiations','PRE');//added 9-13-21
        bariatricStatus.subStatusReason('OON Negotiations','global');//added 9-13-21
        bariatricStatus.subStatusReason('No Benefit Eligibility','global');//added 9-13-21 
        bariatricStatus.subStatusReason('No Claim - Global','global');//added 9-13-21
        bariatricStatus.subStatusReason('Infectious Disease','pre');//added 9-13-21
        bariatricStatus.subStatusReason('No Claim - PRE','pre');//added 9-13-21
        bariatricStatus.subStatusReason('Pre-Qualification- In Process','pre');//added 9-13-21
        bariatricStatus.subStatusReason('No Benefit Eligibility','pre');//added 9-13-21
        bariatricStatus.subStatusReason('Infectious Disease','global');//added 9-13-21
        bariatricStatus.subStatusReason('Infectious Disease','intake');//added 9-13-21
        bariatricStatus.subStatusReason('OON Negotiations','intake');//added 9-13-21
        //bariatricStatus.subStatusReason('No Benefit Eligibility','post');//added 9-13-21
        
        
        bariatricStatus.subStatusReason('Clinical','GLOBAL');
        bariatricStatus.subStatusReason('Cancelled By Patient','GLOBAL');
        bariatricStatus.subStatusReason('Cancelled By Facility','GLOBAL');
        bariatricStatus.subStatusReason('Clinical','POST');
        
        
        //9-13-21 o.brown ------------------start-----------------------------
        bariatricStatus.subStatusReason('No Benefit Eligibility','POST');
        bariatricStatus.subStatusReason('Cancelled by Patient','POST');
        bariatricStatus.subStatusReason('Cancelled by Facility','POST');
        bariatricStatus.subStatusReason('No Claim - Post','POST');
        bariatricStatus.subStatusReason('Infectious Disease','POST');
        bariatricStatus.subStatusReason('OON Negotiations','POST');
        //bariatricStatus.statusReason('OON Negotiations','POST');
        
       // bariatricStatus.status('Pend');
        
        
        //9-13-21 o.brown ------------------end-------------------------------
        
        
    }
    
    //static testMethod void BariatricTest() {
        
        //bc.createPost();
        //bc.savePost();
        
        
    //}

    static testMethod void BariatricRecommendedFacilityTest() {
        
        bariatric__c bariatric = new bariatric__c();
        
        bariatric.patient_state__c = 'WA';
        
        bariatric__c[] blist = new bariatric__c[]{bariatric};
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'HI';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'AK';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'OR';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'CA';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'ID';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'NV';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'AZ';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'MT';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'WY';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'UT';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'CO';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'NM';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'ND';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'SD';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'NE';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'KS';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'OK';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'TX';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'MN';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'IA';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'MO';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'AR';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'LA';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'WI';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'IL';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'TN';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'MS';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'MI';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'IN';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'KY';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'MN';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'AL';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'OH';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'GA';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'FL';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'ME';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'VT';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'NH';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'MA';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'RI';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'CT';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'NJ';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'NY';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'DE';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'MD';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'WV';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'VA';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'NC';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatric.patient_state__c = 'SC';
        BariatricWorkflow.recommendedFacility(bList);
        
        bariatricUtils.dobDatabaseEncode('01/01/80');    
        bariatricUtils.dobPageEncode('80-01-01');
        

    } 

}