@isTest
private class ovrTest{

     static testMethod void ovrTest(){
         
         Client__c  cac = new Client__c (underwriter__c='008',name='Cooper');
         insert cac;
         
         ovrController controller = new ovrController();
         controller.ovr.client__c = 'Unit.Test';
         controller.ovr.payee__c = 'Unit.Test';
         controller.ovr.Payor__c = 'Health Design Plus';
         controller.ovr.Status__c = 'Open';
         controller.ovr.type__c = 'Overpayment';
         controller.ovr.Comments__c = 'Test Case';
         
         controller.saveOVR(); 
         
         system.assert(controller.ovr.id != null);
         
         ApexPages.CurrentPage().getParameters().put('id',controller.ovr.id);
         
         controller = new ovrController();
         
         /* Requests */
         
         controller.newReq();
         
         controller.req.Letter_Type__c = 'A';
         controller.req.Request_Type__c= 'A';
         
         controller.saveReq();
         
         system.assert(controller.reqlist.size()==1);
         
         ApexPages.CurrentPage().getParameters().put('reqID',controller.reqlist[0].id);
         
         controller.getTheReq();
         controller.cancelReq();
         
         ApexPages.CurrentPage().getParameters().put('reqID',controller.reqlist[0].id);
         controller.delReq();
         
         system.assert(controller.reqlist.size()==0);
         
         /* End Requests */
         
         /* Claim */
         
         controller.newClaim();
         controller.clm.Claim_Number__c = '123456711';
         controller.clm.ESSN__c= '1234';
         controller.clm.Patient_First_Name__c = 'John';
         controller.clm.Patient_Last_Name__c  = 'Test';
         controller.clm.Error__c= 'No';
         controller.clm.Reason__c= 'UNIT.TEST';
         controller.clm.Overpayment_Amount__c= 5000.00;
         controller.clm.Amount_Waived__c= 1000.00;
         controller.clm.Amount_Waived_Note__c= 'UNIT.TEST';
         controller.clm.Comments__c= 'UNIT.TEST';
         
         controller.saveCLM();
         
         system.assert(controller.clmList.size()==1);
         
         ApexPages.CurrentPage().getParameters().put('clmID',controller.clmList[0].id);
         
         controller.getTheClaim();
         
         system.assert(controller.clm.id != null);
         
         controller.saveCLMNew();
         controller.cancelCLM();
         
         controller.doNothing();
         
         ApexPages.CurrentPage().getParameters().put('clmID',controller.clmList[0].id);
         
         controller.getTheClaim();
         
         /* End Claim */
         
         /* Refund */
         
         controller.newRefund();
         controller.refund.Refund_Amount__c =1000.00;
         controller.refund.Refund_Date__c= date.today();
         
         controller.saveRefund();
         
         system.assert(controller.refundList.size()==1);
         ApexPages.CurrentPage().getParameters().put('refID',controller.refundList[0].id);
         
         controller.getTheRefund();
         
         // test error catching
         controller.newRefund();
         controller.refund.ovr_claim__c =null;
         controller.saveRefund();
         
                  
         /* End Refund */
         
         /* Check */
         
         controller.newCheck();
         controller.chk.Check_Amount__c = 1000.00;
         controller.chk.Check_Number__c= '100';
         controller.chk.name= '100';
         controller.chk.Check_Received__c= date.today();
         controller.chk.Check_Type__c= 'UNIT.TEST';
         controller.chk.Finance_Received__c = date.today();
         controller.chk.Tech_Received__c= date.today();
         
         controller.saveChk();
         
         system.assert(controller.chkList.size()==1);
         ApexPages.CurrentPage().getParameters().put('chkID',controller.chkList[0].id);
         controller.getCheck();
         
         system.assert(controller.chk.id != null);
         
         controller.cancelChk();
         
         /* End Check */
         
         /* Attachment */
         
         ApexPages.CurrentPage().getParameters().put('fileName','Unit.Test');
         ApexPages.CurrentPage().getParameters().put('fileContent','Unit.Test');
         ApexPages.CurrentPage().getParameters().put('fileBody',EncodingUtil.Base64Encode(blob.valueof('Unit.Test')));
         
         
         controller.uploader();
         
         system.assert(controller.attachments.size()==1);
         
         ApexPages.CurrentPage().getParameters().put('attachmentID',controller.attachments[0].id);
         controller.getTheAttachment();
         
         ApexPages.CurrentPage().getParameters().put('attachmentID',controller.attachments[0].id);
         controller.ussscorpion();
         controller.cancelAttachment();
         
         ApexPages.CurrentPage().getParameters().put('attachmentID',controller.attachments[0].id);
         
         controller.deleteAttachment();
         
         /* End Attachment */
         
         /* Delete Check */
         
         ApexPages.CurrentPage().getParameters().put('chkID',controller.chkList[0].id);
         controller.deleteCheck();
         
         system.assert(controller.chkList.size()==0);
         
         /* End Delete Check */
         
         /* Delete Refund */
         
         ApexPages.CurrentPage().getParameters().put('clmID',controller.clmList[0].id);
         controller.getTheClaim();
         
         ApexPages.CurrentPage().getParameters().put('refID',controller.refundList[0].id);
         controller.deleteRefund();
         
         system.assert(controller.refundList.size()==0);
         
         /* End Delete Refund */
         
         /* Delete Claim */
         
         ApexPages.CurrentPage().getParameters().put('clmID',controller.clmList[0].id);
         controller.deleteCLM();
         
         /* End Delete Claim */
         
         /* Error catching */
         
         controller.newReq();
         controller.req.ovr_control__c = null;
         controller.saveReq();
         controller.cancelReq();
         
         controller.newClaim();
         controller.clm.ovr_control__c = null;
         controller.saveCLMNew();
         controller.saveCLM();
         controller.cancelCLM();
         
         controller.newCheck();
         controller.chk.ovr_control__c = null;
         controller.saveChk();
                         
         /* End Error catching */
         
     }

}