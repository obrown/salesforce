/**
* This class contains unit tests for validating the behavior of  the Apex newRxErrors class
*/

@isTest
private class newRxErrorsTest {

    static testMethod void newRxErrors() {
    
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = 'https://test-hdplus.cs3.force.com/createRxError/';  
        req.requestBody =blob.valueof('');
        
        req.httpMethod = 'POST';
        RestContext.request = req;
        
        Test.startTest();
        
        newRxErrors.newRxErrors();
        req.requestBody =blob.valueof('{"ResultCode":"0","RxTxns":[{"Transtype":"I","Transdate":"20160123","Underwriter":"008","Grpnbr":"CP4","Essn":"258238832","Patseq":"00","Dedamount":"         .00","Oopamount":"         .00","Dos":"20160123","ClaimNbr":"000000000000000000","ErrMsg":"AMOUNT MISMATCH ON RECON                          ","Filename":"medco_recon20160123.txt                           "},{"Transtype":"R","Transdate":"20160130","Underwriter":"008","Grpnbr":"CP4","Essn":"258238832","Patseq":"00","Dedamount":"         .00","Oopamount":"         .00","Dos":"20160130","ClaimNbr":"000000000000000000","ErrMsg":"AMOUNT MISMATCH ON RECON                          ","Filename":"medco_recon20160130.txt                           "},{"Transtype":"R","Transdate":"20160206","Underwriter":"008","Grpnbr":"CP4","Essn":"258238832","Patseq":"00","Dedamount":"         .00","Oopamount":"         .00","Dos":"20160206","ClaimNbr":"000000000000000000","ErrMsg":"AMOUNT MISMATCH ON RECON                          ","Filename":"medco_recon20160206.txt                           "}]}');
        newRxErrors.newRxErrors();
        
        hp_rxBatch hpb = new hp_rxBatch();
        hpb.searchHP('20160101');
        
        Test.stopTest();
        
    }
    
}