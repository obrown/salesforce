public class highDollarClaimAuditReview {

    Claim_Audit__c ca = new Claim_Audit__c();
    public boolean success {get;set;}
    public string newID {get;set;}
    
    public highDollarClaimAuditReview (){}
    
    public pagereference init(){
        id theID = ApexPages.currentpage().getParameters().get('Id');
        
        try{
        
            if(theID!=null){
            
                /* Get current claim audit and clone */
                             
                ca = [select Claim__c, 
                             Claim_Processor__c, 
                             Coding_Credits__c, 
                             Coding_Errors__c, 
                             Comments__c,
                             recordtypeid,
                             Audit_Type__c,
                             Audit_Status__c,
                             relatedClaimAudit__c from Claim_Audit__c where id = :theID];
                            
                Claim_Audit__c caHD = new Claim_Audit__c();
                
                if(ca.relatedClaimAudit__c==null){
                
                    caHD= ca.clone();
                
                }else{
                    
                    caHD = [select Audit_Status__c,
                                   Audit_Type__c,
                                   Claim__c,                 
                                   Claim_Processor__c, 
                                   Coding_Credits__c, 
                                   Coding_Errors__c, 
                                   Comments__c,
                                   recordtypeid,
                                   relatedClaimAudit__c from Claim_Audit__c where id = :ca.relatedClaimAudit__c];
                
                }
                
                caHD.recordtypeid =[select id from recordtype where sObjectType='Claim_Audit__c' and name = 'High Dollar' and isActive=true].id;
                caHD.Audit_Type__c='High Dollar';
                caHD.Audit_Status__c='To Be Audited';
                caHD.Claim_Auditor__c = UserInfo.getUserID();
                upsert caHD;

                ca.Audit_Status__c = 'Completed';
                ca.recordtypeid= [select id from recordtype where sObjectType='Claim_Audit__c' and name = 'Completed' and isActive=true].id;
                update ca;
                
                newID = string.valueof(caHD.id);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Record submitted. This window will now close'));
                success=true;
                
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Page loaded incorrectly. No ID parameter provided'));
                success=false;
                
            }
            
        }catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            success=false;
        }
        
        return null;
    
    }
}