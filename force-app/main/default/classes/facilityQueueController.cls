public with sharing class facilityQueueController{
    
    public transient patient_case__c[] newCases {get; private set;}
    public transient patient_case__c[] awaitingPOC {get; private set;}
    public transient patient_case__c[] atFacility {get; private set;}
    public transient patient_case__c[] arrivingThisWeek {get; private set;}
    
    string facility;
    set<id> loaded = new set<id>();
    
    public facilityQueueController(){
        
        facility = [select facility__c from user where id =:userInfo.getUserID()].facility__c;
        loadNewCases();
        loadPOC();
        loadAtFacility();
        loadArrivingThisWeek();
    }
    
    void loadNewCases(){
        newCases = new patient_case__c[]{};
        /*newCases = [select converted_date__c, 
                           patient_first_name__c,
                           patient_last_name__c, 
                           employee_first_name__c,
                           employee_last_name__c, 
                           case_type__c from patient_case__c where (referred_facility__c = :facility and isConverted__c = true and converted_date__c > :date.today().addDays(-7)) or id ='a04m0000000TY3g' ];*/
       
       for(patient_case__c p : [select converted_date__c, 
                                       patient_first_name__c,
                                       patient_last_name__c, 
                                       employee_first_name__c,
                                       employee_last_name__c, 
                                       case_type__c from patient_case__c where isConverted__c = true and id not in :loaded limit 3]){
           loaded.add(p.id);
           newCases.add(p);
       }
                                             
    }


    void loadPOC(){
        awaitingPOC = new patient_case__c[]{};
        /*awaitingPOC = [select converted_date__c, 
                           patient_first_name__c,
                           patient_last_name__c, 
                           employee_first_name__c,
                           employee_last_name__c, 
                           case_type__c from patient_case__c where (referred_facility__c = :facility and isConverted__c = true and converted_date__c > :date.today().addDays(-7)) or id ='a04m0000000TY3g' ];
        */
        for(patient_case__c p : [select converted_date__c, 
                                        patient_first_name__c,
                                        patient_last_name__c, 
                                        employee_first_name__c,
                                        employee_last_name__c, 
                                        case_type__c from patient_case__c where isConverted__c = true and id not in :loaded limit 3]){
           loaded.add(p.id);
           awaitingPOC.add(p);
        }
    }

    void loadAtFacility(){
        atFacility = new patient_case__c[]{};
        /*atFacility = [select converted_date__c, 
                           patient_first_name__c,
                           patient_last_name__c, 
                           employee_first_name__c,
                           employee_last_name__c, 
                           case_type__c from patient_case__c where (referred_facility__c = :facility and isConverted__c = true and converted_date__c > :date.today().addDays(-7)) or id ='a04m0000000TY3g' ];
        */
        
        for(patient_case__c p : [select converted_date__c, 
                                        patient_first_name__c,
                                        patient_last_name__c, 
                                        employee_first_name__c,
                                        employee_last_name__c, 
                                        case_type__c from patient_case__c where isConverted__c = true and id not in :loaded limit 3]){
           loaded.add(p.id);
           atFacility.add(p);
        }

    }

    void loadArrivingThisWeek(){
        arrivingThisWeek = new patient_case__c[]{};
        /*arrivingThisWeek = [select converted_date__c, 
                           patient_first_name__c,
                           patient_last_name__c, 
                           employee_first_name__c,
                           employee_last_name__c, 
                           case_type__c from patient_case__c where (referred_facility__c = :facility and isConverted__c = true and converted_date__c > :date.today().addDays(-7)) or id ='a04m0000000TY3g' ];
        */
        
        for(patient_case__c p : [select converted_date__c, 
                                        patient_first_name__c,
                                        patient_last_name__c, 
                                        employee_first_name__c,
                                        employee_last_name__c, 
                                        case_type__c from patient_case__c where isConverted__c = true and id not in :loaded limit 3]){
           loaded.add(p.id);
           arrivingThisWeek.add(p);
        }

    }

}