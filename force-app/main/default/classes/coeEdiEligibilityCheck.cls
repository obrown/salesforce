public with sharing  class coeEdiEligibilityCheck{

   
      hp_EdiElgSearch ediElig = new hp_EdiElgSearch();
      public hpEdiElgStruct.hpEdiElg[] patientEDIElig {get; private set;}
      public hpEdiElgStruct.hpEdiElg empEDIElig {get; private set;}
      public boolean ediSearchFail {get; private set;}
      public string eligible,CobraQualEvent,CobraStatus,Bid,CoverageLevel;
      public date PaidThroughDate,termDate,effectiveDate,LastUpdateDate,CobraEffDate,CobraEndDate;
      
      
      public class response{
          public Eligibilty__c     EligibilityRecord;
          public Patient_case__c   patientCase;
      }
      
      public date formatDate(string ediValue){
          date result;
          
          if(ediValue !='' && ediValue != null && ediValue.length()==10){
              string[] fooDate = ediValue.split('/');
              result = date.valueof(fooDate[2]+'-'+fooDate[0]+'-'+fooDate[1]);
              
          }         
          
          return result;
      }
      
      public static coeEdiEligibilityCheck isPcEligible(string patientSSN, string employeeSSN, string relationshipToInsured, date estimatedArrival, date estimatedDeparture, date actualArrival, date actualDeparture, boolean isReferred){
               coeEdiEligibilityCheck ediResult = new coeEdiEligibilityCheck();
               ediResult.searchEDI(patientSSN, employeeSSN, relationshipToInsured);
               if(!ediResult.ediSearchFail && (relationshipToInsured=='Patient is Insured' || !ediResult.patientEDIElig.isEmpty())){
                   //match found
                   ediResult.eligible = 'Yes';
                   ediResult.PaidThroughDate = ediResult.formatDate(ediResult.empEDIElig.PaidThroughDate);
                   
                   
                   if(ediResult.PaidThroughDate!=null){
                       if((ediResult.PaidThroughDate<=date.today() && actualDeparture==null && estimatedDeparture==null) || ediResult.PaidThroughDate<actualDeparture || ediResult.PaidThroughDate< estimatedDeparture){
                           if(isReferred){
                               ediResult.eligible = 'No';
                           }else{
                               try{
                                   if(ediResult.PaidThroughDate < date.today().addDays(-21)){
                                       ediResult.eligible = 'No';        
                                   }
                               }catch(exception e){
                                   ediResult.eligible = 'No';
                               }
                           }
                           
                       }
                   }
                   
                   ediResult.termDate= ediResult.formatDate(ediResult.empEDIElig.termDate);
                   if(ediResult.termDate!=null){
                       if((ediResult.termDate<=date.today() && actualDeparture==null && estimatedDeparture==null) || ediResult.termDate< actualDeparture || ediResult.termDate<estimatedDeparture){
                           ediResult.eligible= 'No';
                       }
                   }
                   
                   ediResult.LastUpdateDate= ediResult.formatDate(ediResult.empEDIElig.LastUpdateDate);
                   ediResult.EffectiveDate = ediResult.formatDate(ediResult.empEDIElig.EffectiveDate);
                   ediResult.CobraEffDate = ediResult.formatDate(ediResult.empEDIElig.CobraEffDate);
                   ediResult.CobraEndDate = ediResult.formatDate(ediResult.empEDIElig.CobraEndDate);
                   ediResult.CobraStatus=ediResult.empEDIElig.CobraStatus;
                   ediResult.CobraQualEvent=ediResult.empEDIElig.CobraQualEvent;
                   ediResult.CoverageLevel = ediResult.empEDIElig.CoverageLevel;
                   ediResult.Bid=ediResult.empEDIElig.Bid;
                   
              }else{
                  ediResult.eligible = 'No';
              }
              
              return ediResult;
      }
      
      public void searchEDI(string ssn, string essn, string patientType){
        string pType;
        if(patientType=='Patient is Insured'){
             pType = 'employee';
        }else {
             pType='dependent';
        }
        
        ediSearchFail=false;
        if(empEDIElig!=NULL){return;}
        
        ediElig.searchEDIGate(ssn, essn, pType);
            
        patientEDIElig=new hpEdiElgStruct.hpEdiElg[]{};
        hpEdiElgStruct.hpEdiElg[] allpatientEDIElig=new hpEdiElgStruct.hpEdiElg[]{};
            
        for(hpEdiElgStruct.hpEdiElg edi : ediElig.ediEligResult.Results){
            if(edi.PatientType=='employee'){
                empEDIElig=edi;
            }
                
            if(edi.PatientType=='dependent'){
                allpatientEDIElig.add(edi);
                if(edi.Ssn==ssn){
                    patientEDIElig.add(edi);
                    
                }
            }
        }
        if(patientEDIElig.isEmpty()){patientEDIElig=allpatientEDIElig;}
        
        if(empEDIElig==null){ediSearchFail=true;}
        
    }

    public static response pcEdiEligibilityCheck(patient_case__c p){
        response response = new response();

        ID queueGroupId;
        try{
            queueGroupId= [Select Id from Group where type='Queue' and Name= 'HDP Eligibility'].Id;
        }catch (exception e){}
        
        if(!p.Eligibility_Manual_Override__c){
                   p.eligible__c = '';
               }
               
               coeEdiEligibilityCheck ediResult = coeEdiEligibilityCheck.isPcEligible(p.patient_ssn__c, p.employee_ssn__c, p.relationship_to_the_insured__c, p.estimated_arrival__c,p.estimated_Departure__c, p.actual_Departure__c,p.actual_Departure__c,p.isConverted__c);
               //Call failed, reset fields
               if(ediResult.ediSearchFail){
                    p.Date_Eligibility_is_Checked_with_Client__c = null;
                    p.Patient_and_Employee_DOB_verified__c = false;
                    p.Patient_and_Employee_SSN_verified__c = false;
                    p.Carrier_and_Plan_Type_verified__c = false;
                    p.BID_verified__c = false;
                    p.eNotes__c='Patient not found in EDI';
                    p.pendingEligCheck__c=true;
                    
                    Eligibilty__c objE = new Eligibilty__c();
                    objE.Patient_case__c = p.id;
                    objE.Date_Care_Mgmt_Transfered_to_Eligibility__c = date.today();
                    objE.Plan__c = p.Employee_HealthPlan__r.Name;
                    objE.Notes__c='Patient not found in EDI';
                    if(queueGroupId != null){
                        objE.ownerID = queueGroupId;
                    }
                    response.EligibilityRecord= obje;
                    response.patientCase= p;
                    return response;
               }
                
                Eligibilty__c objE = new Eligibilty__c();
                objE.Patient_case__c = p.id;
                objE.Date_Care_Mgmt_Transfered_to_Eligibility__c= date.today();
                objE.Date_Eligibility_Checked_with_Client__c= date.today();
                objE.Effective_Date_of_Medical_Coverage__c = ediResult.effectiveDate;
                objE.coverage_level__c = ediResult.coverageLevel;
                objE.last_Update_Date__c = ediResult.LastUpdateDate;
                objE.Cobra_Effective_Date__c= ediResult.CobraEffDate;
                objE.Cobra_End_Date__c= ediResult.CobraEndDate;
                objE.Cobra_Qual_Event__c= ediResult.CobraQualEvent;
                objE.Cobra_Status__c= ediResult.CobraStatus;
                objE.Insurance_ID__c = ediResult.BID;
                 
                p.Effective_Date_of_Medical_Coverage__c = ediResult.effectiveDate;
                p.Paid_thru_Date__c = ediResult.PaidThroughDate;
                p.Termination_Date__c = ediResult.termDate;
                p.Eligibility_Last_Update_Date__c = ediResult.LastUpdateDate;
                p.Date_Eligibility_is_Checked_with_Client__c =date.today();
                p.Coverage_Level__c= ediResult.coverageLevel;
                objE.Paid_through_Date__c = ediResult.PaidThroughDate;
                objE.Termination_Date__c = ediResult.termDate;
                objE.Plan__c = ediResult.empEDIElig.Plncode;
                
                if(queueGroupId != null){
                    objE.ownerID = queueGroupId;
                }
                
                //User has manually overridden the eligibility results 
                if(p.Eligibility_Manual_Override__c){
                    objE.eligible__c = ediResult.eligible;
                }else{
                    p.eligible__c = ediResult.eligible;
                }    
                
                if(p.eligible__c=='No'){ 
                    objE.Eligible__c = 'No';
                    string notes = 'Patient not eligible based on the paid through and term dates';
                    if(p.Eligibility_Manual_Override__c){
                        notes+='\n Case not updated , manual override';
                    }
                    p.eNotes__c=notes;
                    objE.Notes__c=notes;
                    
                }else if(p.eligible__c=='Yes'){
                    objE.Eligible__c = 'Yes';
                    string notes = 'Patient eligible based on EDI Data';
                    if(p.Eligibility_Manual_Override__c){
                        notes+='\n Case not updated , manual override';
                    }
                    p.eNotes__c=notes;
                    objE.Notes__c=notes;
                    
                }
                
                response.EligibilityRecord= obje;
                response.patientCase= p;
                
                return response;
                
    }

}