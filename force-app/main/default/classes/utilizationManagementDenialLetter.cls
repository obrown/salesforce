public class utilizationManagementDenialLetter{
    
    public Utilization_Management_Denial_Letter__c[] denialLetters {get; private set;}
    public Utilization_Management_Denial_Letter__c   [] deletedumLetters {get; private set;}//8-18-22 o.brown added HDP-7023 update
    public Utilization_Management_Denial_Letter__c   denialLetter {get; set;}
    public selectOption[] denialReasons {get; private set;}
    public boolean error {get; private set;}
    public saveResponse saveResponse {get; private set;}
    id umRecordId;
    id clientId;
    final boolean deluml = Utilization_Management_Denial_Letter__c.sObjectType.getDescribe().isDeletable(); //8-18-22  o.brown added HDP-7023 update 
    
    public utilizationManagementDenialLetter(id umRecordId, id clientId){
        this.umRecordId = umRecordId;
        this.clientId = clientId;
        loadDenialLetters();
        setDenialReasons();
        loadDeletedUMLetters();//8-18-22  o.brown added HDP-7023 update
    }
    
    void setDenialReasons(){
       denialReasons = new selectOption[]{};
       denialReasons.add(new selectOption('', '--None--'));
       for(Utilization_Management_Denial_Reason__c dr : [select name from Utilization_Management_Denial_Reason__c where client__c = :clientId order by name asc]){
           denialReasons.add(new selectOption(dr.name, dr.name));
       }
       
    }
    
    public class saveResponse{
        
        public string resultCode;
        public string errorMsg;
        
        saveResponse(string resultCode){
            this.resultCode = resultCode;
        }
    }
    
    public void newDenialLetter(){
        denialLetter = new Utilization_Management_Denial_Letter__c(Utilization_Management__c=umRecordId);
    }
    
    public void cancel(){
        denialLetter=null;
    }
    
    public void save(){
        saveResponse = new saveResponse('0');
        error=false;
        try{
            upsert denialLetter;
            loadDenialLetters();
        }catch(dmlException e){
            saveResponse.resultCode ='2';
            error=true;
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                saveResponse.errorMsg = e.getDmlMessage(i);
                System.debug(e.getDmlMessage(i)); 
            }
            
        }
        
    }
    
    public void loadDenialLetters(){
        denialLetters = new Utilization_Management_Denial_Letter__c[]{};
        /*8-18-22 updated o.brown HDP-7023
        denialLetters = [select Name,Type_of_Letter__c,Signed_Date__c,Signed_by__c,Print_Date__c,Printed_By__c,createdDate, createdBy.Name from Utilization_Management_Denial_Letter__c where Utilization_Management__c = :umRecordId order by createdDate desc];
        */
         denialLetters = [select Name,Type_of_Letter__c,Signed_Date__c,Signed_by__c,Print_Date__c,Printed_By__c,createdDate, createdBy.Name,Deleted_Date__c from Utilization_Management_Denial_Letter__c where Utilization_Management__c = :umRecordId and Deleted_Date__c=null order by createdDate desc];
    }

    public void loadDeletedUMLetters(){
        deletedumLetters = new Utilization_Management_Denial_Letter__c[]{};
        deletedumLetters = [select Name,Type_of_Letter__c,Signed_Date__c,Signed_by__c,Print_Date__c,Printed_By__c,createdDate, createdBy.Name,Deleted_Date__c,Type_of_Letter_Deleted__c,Type_of_Provider_Letter_Deleted__c,Deleted_By__c  from Utilization_Management_Denial_Letter__c where Utilization_Management__c = :umRecordId and Deleted_Date__c!=null order by createdDate desc];
    
    }
    
    public void setDenialLetterPrintDate(){
        
        string passId = ApexPages.CurrentPage().getParameters().get('denialLetter');
        //validate input
        for(Utilization_Management_Denial_Letter__c  dl : denialLetters){
            if(dl.id==passId ){
                dl.Print_Date__c=date.today();
                string uName='';
                if(userinfo.getfirstname()!=''){
                    uName=userinfo.getfirstname()+' ';
                }
                uName = uName+userinfo.getlastname();
                update dl;
                loadDenialLetters();
                break;
            }
        }
    }

   //8-18-22 o.brown update HDP-7023
    public void deleteTheUML(){
        if(deluml){ 
            string umlID = ApexPages.currentPage().getparameters().get('umlID');
            string deletedlt=ApexPages.currentPage().getparameters().get('dlt');
            string deletedplt=ApexPages.currentPage().getparameters().get('dplt');

            Utilization_Management_Denial_Letter__c uml = new Utilization_Management_Denial_Letter__c(Utilization_Management__c=umRecordId);
            
            uml.Type_of_Letter_Deleted__c=deletedlt;
            uml.Type_of_Provider_Letter_Deleted__c=deletedplt;
            uml.Deleted_Date__c=datetime.now();
            uml.Deleted_By__c=UserInfo.getFirstName()+' '+UserInfo.getLastName();

        try {
              insert uml ;

        } catch (dmlException e) {
          system.debug(e.getDmlMessage(0));
          return;
        }
  
            system.debug('umlID = '+umlID);
            system.debug('Type of letter deleted= '+uml.Type_of_Letter_Deleted__c);
            system.debug('Type of provider letter deleted= '+uml.Type_of_Provider_Letter_Deleted__c);
            system.debug('Deleted Date= '+uml.Deleted_Date__c);
            system.debug('Deleted by= '+uml.Deleted_By__c);

        try {
              delete (new Utilization_Management_Denial_Letter__c (id=umlID)); //removed to test writing deleted letter record prior to delete

        } catch (dmlException e) {
          system.debug(e.getDmlMessage(0));
          return;
        }
            loadDenialLetters();
            loadDeletedUMLetters();
            
        }
    }
    
}