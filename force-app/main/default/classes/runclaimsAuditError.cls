@RestResource(urlMapping='/runClaimsAuditError/*')

global with sharing class runclaimsAuditError{

    @HttpPost
    global static string claimsAuditError(){
        claimsauditbatch cab = new claimsauditbatch();
        cab.run();
        {return 'success';}
    }
    
    webservice static void wsclaimsAuditError(){
        claimsauditbatch cab = new claimsauditbatch();
        cab.run();
        
    }
    
}