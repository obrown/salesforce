/* Pseudocode:
 * 1. Looks for an existing Portal POC first
 *    Scenarios:
 *      a. Finds an existing Portal POC and returns it.
 *      b. [Edge] Doesn't find existing Portal POC, but finds Case POC.
 *         Creates a new Portal POC based on the Case POC using the POC Sync class.
 *         May occur when a Facility is not on the Portal yet, but has been part of the COE Program already.
 *         If the nurse from the Facility communicates with the Contigo Health nurse outside of the Portal, and sends the POC manually. Our Contigo nurse might create the POC before it's in the Portal.
 *      c. Finds neither, returns nothing.
 */
public with sharing class ProviderPortalPocWorkflow{

    string case_name, requestBody, doesPatientQualify, doesPatientNeedPOC, submittingPOC, action;
    patient_case__c pc_case = new patient_case__c();
    response response;

    public class response {
        public EcenProviderPlanOfCare__c poc;
        public string clientFacility;
        public string caseType;
    }

    public ProviderPortalPocWorkflow(string case_name, string doesPatientQualify, string doesPatientNeedPOC, string requestBody, string action){
        if (case_name == null) {
          return;
        }

        this.doesPatientQualify = doesPatientQualify;
        this.doesPatientNeedPOC = doesPatientNeedPOC;
        this.case_name = case_name;
        this.requestBody = requestBody;
        this.submittingPOC = 'false';
        this.action = action;

        if (action == 'PUBLISH') {
            this.submittingPOC = 'true';
        }

        response = new response();
        set_pc_case();
    }

    void set_pc_case() {
        try {
            pc_case = [select id, name, providerPortalCandidateNotes__c, client_facility__c, client_facility__r.procedure__r.name from patient_case__c where name = :case_name];
            response.clientFacility = pc_case.client_facility__c;
            response.caseType = pc_case.client_facility__r.procedure__r.name;
        } catch (exception e) {
            response.clientFacility = null;
            response.caseType = null;
        }
    }

    public ProviderPortalPocWorkflow.response getplanofcare(){
        EcenProviderPlanOfCare__c poc = new EcenProviderPlanOfCare__c(patient_case__c = pc_case.id);
        boolean createPOC=false;
        system.debug('case_name ' + case_name );

        /*
        try {
            pc_poc__c pc_poc_test = new pc_poc__c();
            pc_poc_test = [select id from pc_poc__c where Patient_Case__r.Name = :case_name limit 1];

            if (pc_poc_test == null) {
                poc.proposedAddOnServices__c = '';
                poc.allowedModeOfTransportation__c = '';
                response.poc = poc;
                return response;
            }

        } catch (queryexception e) {
            poc.proposedAddOnServices__c = '';
                poc.allowedModeOfTransportation__c = '';
                response.poc = poc;
                return response;
        }
        */

        try {
            poc = [select Name,
                          additionalTrip__c,
                          authorizationNumber__c,
                          allowedModeOfTransportation__c,
                          cancelledReason__c,
                          complicationServiceTrip__c,
                          complicationType__c,
                          complicationServiceType__c,
                          conservativeTreatment__c,
                          CreatedDate__c,
                          //displayAuthNumber__c,
                          firstAppointmentDate__c,
                          firstAppointmentTime__c,
                          hipApproach__c,
                          hospitalAdmissionDate__c,
                          hospitalDischargeDate__c,
                          id,
                          lastAppointmentDate__c,
                          lastAppointmentTime__c,
                          lastModifiedDate,
                          lastModifiedBy.firstName,
                          lastModifiedBy.lastName,
                          lastModifiedBy.email,
                          Last_Modified_By_Portal_User__c,
                          Last_Modified_By_Portal_User_DateTime__c,
                          medicalDirectorInvolvement__c,
                          nicotineUser__c,
                          nicotineQuitDate__c,
                          originalSurgeryDate__c,
                          program__c,
                          patient_case__r.displayAuthNumber__c,
                          patient_case__r.ecen_procedure__r.name,
                          patientPreferredModeOfTravel__c,
                          patientArrivalDate__c,
                          patientReturnHomeDate__c,
                          planOfCareType__c,
                          physicianFollowUpDate__c,
                          physicianFollowUpTime__c,
                          pocSubmitted__c,
                          pocAccepted__c,
                          pocCancelled__c,
                          proposedAddOnServices__c,
                          proposedServiceTrip__c,
                          proposedServiceType__c,
                          proposedServiceReason__c,
                          Proposed_Service_Trip_Note__c,
                          providerPortalCandidateNotes__c,
                          providerPortalCandidateReason__c,
                          requestedBillableAdditionalServices__c,
                          surgicalProcedure__c,
                          surgicalProcedureDate__c,
                          surgicalProcedureTime__c,
                          scoliosisDiagnosed__c,
                          servicesRequestedOutsideofContracted__c,
                          status__c,
                          surgeon__c,
                          surgeryHistoryStatus__c,
                          travelNotes__c,
                          verifiedOutcomeOfVisit__c
                          from EcenProviderPlanOfCare__c where Patient_Case__r.Name = :case_name order by createdDate desc limit 1];
        } catch (queryException e) {
            system.debug(e.getMEssage());
            system.debug('Error searching for plan of care: ' + e.getMessage());
            // Errors will happen when no plan of care exists. We might need to create a new plan of care.
            createPOC = true;
        }

        if (createPOC || poc.id == null) {
            if (!createPortalPOC(poc)) {
                poc.proposedAddOnServices__c = '';
                poc.allowedModeOfTransportation__c = '';
                response.poc = poc;
                return response;
            }
        }

        if (poc.allowedModeOfTransportation__c == null) {
            poc.allowedModeOfTransportation__c = '';
        } else {
            poc.allowedModeOfTransportation__c = poc.allowedModeOfTransportation__c.replaceAll(';', ',');
        }

        if (poc.proposedAddOnServices__c == null) {
            poc.proposedAddOnServices__c = '';
        } else {
            poc.proposedAddOnServices__c = poc.proposedAddOnServices__c.replaceAll(';' , ',');
        }

        //poc.complicationServiceType__c=poc.complicationServiceType__c;
        if (poc.complicationServiceType__c == 'Complication') {
           poc.proposedServiceTrip__c = poc.complicationServiceTrip__c;
           poc.proposedServiceType__c = poc.complicationType__c;
        }

        if (poc.firstAppointmentTime__c != null) {
           if (!poc.firstAppointmentTime__c.contains(':')) {
               poc.firstAppointmentTime__c = civyTime(poc.firstAppointmentTime__c);
           }
        }

        if (poc.lastAppointmentTime__c != null) {
           if (!poc.lastAppointmentTime__c.contains(':')) {
               poc.lastAppointmentTime__c = civyTime(poc.lastAppointmentTime__c);
           }
        }

        if (poc.physicianFollowUpTime__c != null) {
           if (!poc.physicianFollowUpTime__c.contains(':')){
               poc.physicianFollowUpTime__c = civyTime(poc.physicianFollowUpTime__c);
           }
        }

        if (poc.surgicalProcedureTime__c != null){
           if (!poc.surgicalProcedureTime__c.contains(':')) {
               poc.surgicalProcedureTime__c = civyTime(poc.surgicalProcedureTime__c);
           }
        }

        response.poc = poc;
        system.debug('response ' + response);
        return response;
    }

    string civyTime(string iTime) {
        string mTime = iTime;

        if (mTime.length() == 4) {
            string hour = mTime.left(2);
            string minutes = mTime.right(2);
            string AMPM = 'AM';
            try {
                if (integer.valueOf(hour) > 12) {
                    hour = string.valueof(integer.valueOf(hour) - 12);
                    AMPM = 'PM';
                }

                if (integer.valueOf(hour) == 12){
                   AMPM = 'PM';
                }

                mTime = hour + ':' + minutes + ' ' + AMPM;
            } catch (exception e) {
                mTime = '';
            }
        }

        return mTime;
    }

    boolean createPortalPOC(EcenProviderPlanOfCare__c poc) {
        pc_POC__c pc;

        try {
            // Please keep it tidy and in alphabetical order
            pc = [select Additional_Trip__c,
                         Cancelled_Date__c,
                         Clinically_Allowable_Mode_of_Transportat__c,
                         Clinically_Verified_Outcome_of_Visit__c,
                         Complication_Service_Trip__c,
                         conservative_Treatment__c,
                         Current_Nicotine_User__c,
                         Diagnosis_of_scoliosis__c,
                         displayAuthNumber__c,
                         First_Appointment_Date__c,
                         First_Appointment_Time__c,
                         Hip_Approach__c,
                         Hospital_Admission_Date__c,
                         Hospital_Discharge_Date__c,
                         Inpatient_Complication_Type__c,
                         Last_Appointment_Date__c,
                         Last_Appointment_Time__c,
                         Medical_Director_Involvement__c,
                         Nicotine_Quit_Date__c,
                         original_Surgery_Date__c,
                         Outpatient_Complication_Type__c,
                         patient_case__c,
                         Physician_Appointment_Time__c,
                         Patient_Arrival_Date__c,
                         Patient_Preferred_Mode_of_Transportation__c,
                         Physician_Follow_up_Appointment_Date__c,
                         Plan_of_Care_Accepted__c,
                         Plan_of_Care_Submitted__c,
                         Portal_Status__c,
                         Proposed_Add_on_Services__c,
                         Proposed_Service_Trip__c,
                         Proposed_Service_Trip_Note__c,
                         Proposed_Service_Trip_Reason__c,
                         Proposed_Service_Type__c,
                         Received_Determination_at_HDP__c,
                         RecordType.Name,
                         Requested_Billable_Additional_Services__c,
                         Return_Home_Travel_Date__c,
                         Services_Requested_Outside_of_Contracted__c,
                         Standard_Add_on__c,
                         status__c,
                         Surgeon_Name__c,
                         Surgeon_Name_Reporting__c,
                         Surgery_History_Status__c,
                         Surgical_Procedure_Date__c,
                         Surgical_Procedure_Time__c,
                         Travel_Notes__c
                         from pc_POC__c where patient_case__r.Name = :case_name and status__c='Approved' order by createdDate desc limit 1];
        } catch (queryException pc_err) {
            response.poc = poc;
            system.debug('Error searching for patient case: ' + pc_err.getMessage());
            // No plan of care exists in the portal or on a case. Return a blank POC
            return false;
        }

        ProviderPortalPOCSync poc_sync = new ProviderPortalPOCSync(
            pc.patient_case__c
        );
        poc = poc_sync.Salesforce_to_Portal(pc);
        patient_case__c pc_case = new patient_case__c(id = pc.patient_case__c);
        pc_case.providerPortalGoodCandidate__c = 'true';
        update pc_case;

        return true;
    }

    class pocRequest {
        EcenProviderPlanOfCare__c pocRequest;
        POCUser user;
        string providerPortalCandidateReason;
        string providerPortalCandidateNotes;
    }

    class POCUser {
        string email;
    }

    class incoming_obj{

    }

    public string saveplanofcare_pc() {
        pc_case.providerPortalGoodCandidate__c = doesPatientQualify;
        pc_case.providerPortalCandidateNeedsPOC__c = doesPatientNeedPOC;
        EcenProviderPlanOfCare__c poc;
        requestBody = requestBody.replace(',"createdDate__c":"Pending",', ',').replace('createdDate__c', 'createdDDate__c');
        pocRequest pocRequest = (pocRequest)JSON.deserialize(requestBody, pocRequest.class);

        try {
            poc = pocRequest.pocRequest; // (EcenProviderPlanOfCare__c)JSON.deserialize(requestBody, EcenProviderPlanOfCare__c.class);

            try {
               poc.Last_Modified_By_Portal_User__c = [select id from aws_user__c where email__c = :pocRequest.user.email].id;
               poc.Last_Modified_By_Portal_User_DateTime__c = datetime.now();
            } catch (exception e) {
                system.debug('exception ' + e.getMessage());
            }

            if (poc.name != '' && poc.name != null){
                try {
                    poc.id = [select id from EcenProviderPlanOfCare__c where name = :poc.name].id;
                } catch (exception e) {
                    // BAD poc name/id is provided. Case_name should always be available
                    try {
                        poc.id = [select id from EcenProviderPlanOfCare__c where patient_case__r.name = :pc_case.name order by createddate asc limit 1].id;
                    } catch (queryException ee) {}
                }
            } else {
                // we might get a call where there is POC, but no poc name/id is provided. Case_name should always be available
                try {
                    poc.id = [select id from EcenProviderPlanOfCare__c where patient_case__r.name = :pc_case.name order by createddate asc limit 1].id;
                } catch (queryException e) {}
            }

            poc.patient_case__c = pc_case.Id;

            pc_case.providerPortalCandidateNotes__c = pocRequest.providerPortalCandidateNotes;
            pc_case.providerPortalCandidateReason__c = pocRequest.providerPortalCandidateReason;

            switch on action {
                when 'PUBLISH' {
                    poc.pocSubmittedDate__c = date.today();
                    pc_case.Has_New_Provider_Portal_POC__c = true;
                    if (doesPatientNeedPOC== 'true' && doesPatientQualify == 'true') {
                        poc.status__c = 'POC Needs Approval';
                        poc.ReceivedDeterminationAtHDPDate__c = date.today();
                        ProviderPortalPOCSync syncSalesforcePOC = new ProviderPortalPOCSync(pc_case.Id, pc_case.client_facility__c, poc);
                        syncSalesforcePOC.Portal_to_Salesforce();
                    }
                }

                when 'CANCEL' {
                    poc.pocSubmittedDate__c = date.today();
                    poc.Submitted_for_Cancellation__c = poc.Last_Modified_By_Portal_User__c;
                    poc.status__c = 'Pending Cancellation';
                    ProviderPortalPOCSync syncSalesforcePOC = new ProviderPortalPOCSync(pc_case.Id, pc_case.client_facility__c, poc);
                    syncSalesforcePOC.Portal_to_Salesforce();
                    pc_case.Has_New_Provider_Portal_POC__c = true;

                }
            }

            upsert poc;
            update pc_case;

        } catch (exception e) {
            system.debug(e.getMessage());
            system.debug(e.getLineNumber());
            return e.getMessage();
        }

        try {
            if (doesPatientQualify == 'false' && action == 'PUBLISH'){
                pc_case.Has_New_Provider_Portal_POC__c = true;

                if (pc_case.Received_Determination_at_HDP__c == null) {
                    pc_case.Received_Determination_at_HDP__c=date.today();
                }

                update pc_case;
            }

            return 'success'; // ProviderPortalPocListener is dependent on the value 'success'
        } catch (exception e) {
            system.debug(e.getMessage());
            return e.getMessage();
        }
    }
}
