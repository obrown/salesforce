/**
 * @description       :
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             :
 * @last modified on  : 01-14-2022
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
**/
public with sharing class oncologyNeedsAssessment{

    id parentId;

    public Oncology_Needs_Assessment__c Need {get; set;}

    public oncologyNeedsAssessment(id parentId){
        Need = new Oncology_Needs_Assessment__c (Oncology__c =parentId);
        setparentId(parentId);
        loadNeed();
    }

    public void setparentId(id parentId){
        this.parentId=parentId;
    }

    public void loadNeed(){
        try{
            //TODO: Reformat this so all fields are separated and sorted by field name. It makes it easier in the future to determine if a field has been added to a query
            Need = [select Accolade__c, Accolade_EmailSent__c,
                           Active_Fit_Direct__c,Active_Fit_Direct_EmailSent__c,
                           Advance_Medical__c, Advance_Medical_EmailSent__c,Additional_Support_Notes__c,
                           Aetna__c, Aetna_emailSent__c, Allstate_Benefits__c, Allstate_Benefits_EmailSent__c,
                           American_Cancer_Society__c,
                           American_Cancer_Society_EmailSent__c,
                           Amplifon__c,Amplifon_Email_Sent__c,
                           Anthem__c,Anthem_EmailSent__c,
                           AYCO_Survivor_Benefits__c,AYCO_Survivor_Benefits_EmailSent__c,
                           Banking_Services__c,Banking_Services_EmailSent__c,
                           Benefits_Plus_Mercer__c, Benefits_Plus_Mercer_EmailSent__c,
                           Best_Doctors__c,Best_Doctors_EmailSent__c,
                           Blue_Shield__c,Blue_Shield_EmailSent__c,
                           Bright_Horizons__c,Bright_Horizons_EmailSent__c,
                           Cancer_at_Facebook__c,
                           Cancer_at_Facebook_EmailSent__c,
                           Cancer_Care__c,
                           Cancer_Care_EmailSent__c,
                           Cancer_Support_Community__c,
                           Cancer_Support_Community_EmailSent__c,
                           Care__c,Care_EmailSent__c,
                           Carrum_Health__c,Carrum_Health_EmailSent__c,
                           Caring_Bridge__c,
                           Caring_Bridge_EmailSent__c,
                           CIGNA__c, CIGNA_EmailSent__c,
                           Clickotene__c, Clickotene_EmailSent__c,
                           COBRA_benefits__c,COBRA_benefits_EmailSent__c,
                           costco__c,
                           costco_emailSent__c,
                           Facility__c,
                           Facility_EmailSent__c,
                           Clubs_for_Causes__c,
                           Clubs_for_Causes_EmailSent__c,
                           ComPsych_Guidance_Resources__c,
                           ComPsych_Guidance_Resources_emailSent__c,
                           CorpHealth_FitnessEmailSent__c,Corporate_Health_and_Fitness_Centers__c,
                           Crossover_Health__c,
                           Crossover_Health_EmailSent__c,
                           CVS_Caremark__c,
                            CVS_Caremark_EmailSent__c,
                            Daycare_Reimbursement__c,
                            Daycare_Reimbursement_Email_Sent__c,
                            Delta_Dental__c,
                            Delta_Dental_EmailSent__c,
                            Dental_Carrier__c,Dental_Carrier_EmailSent__c,
                            EAP__c,EAP_EmailSent__c,
                            Education_Tuition_Assistance__c,Education_Tuition_Assistance_EmailSent__c,
                            Employee_Services__c,Employee_Services_EmailSent__c,
                            Epic_Hearing__c,Epic_Hearing_Email_Sent__c,
                            Express_Scripts__c,
                            Express_Scripts_EmailSent__c,
                            Fertility_Notes__c,
                            Flexible_Spending_Accounts__c,Flexible_Spending_Accounts_EmailSent__c,
                            Finance_Notes__c,
                            Gilda_Club_New_York_City__c, Gilda_Club_NewYorkCity_EmailSent__c,
                            Health_System_Notes__c,
                            Hinge_Health__c,
                            Hinge_Health_Email_Sent__c,
                            Home_Life_Notes__c,HR_FMLA__c,
                            HR_FMLA_EmailSent__c,
                            HR_PaidFamilyLeave__c,
                            HR_PaidFamilyLeave_EmailSent__c,
                            HR_WellnessReimbursement__c, HR_WellnessReimbursement2__c, HR_WellnessReimbursement_EmailSent__c, HR_WellnessReimbursement_EmailSent2__c,Human_Resources__c, Human_Resources_Email_Sent__c, Hyatt_Legal_Plans__c, Hyatt_Legal_Plans_Email_Sent__c,
                            Hartford_Amazon_Myleave__c,
                            Hartford_Amazon_Myleave_emailSent__c,
                            HRA__c,HRA_EmailSent__c,
                            HSA__c,HSA_EmailSent__c,
                            Lincoln_Financial_Group__c, Lincoln_Financial_Group_Email_Sent__c , Live_Strong__c, Live_Strong_EmailSent__c, Lotsahelpinghands__c, Lotsahelpinghands_EmailSent__c,
                            Legal_Insurance__c,Legal_Insurance_EmailSent__c,
                            Livongo__c, Livongo_EmailSent__c,
                            Lyra__c, Lyra_EmailSent__c,
                            Magic_Hour__c, Magic_Hour_EmailSent__c,
                            Medical_Carrier__c,Medical_Carrier_EmailSent__c,
                            Medical_Second_Opinion_Service__c,Medical_Second_Opinion_Service_EmailSent__c,
                            Meritain__c, Meritain_EmailSent__c,
                            MDLive__c,MDLive_EmailSent__c,
                            Magellan__c, Magellan_EmailSent__c,
                            Magellan_Calm_Counseling__c, Magellan_Calm_Counseling_EmailSent__c,
                            MetLife_High_or_Low_Legal_Plan__c, MetLife_High_or_Low_Legal_Plan_EmailSent__c,
                            MeVision__c,MeVision_EmailSent__c,
                            MyLowes_Benefits__c, MyLowes_Benefits_EmailSent__c,
                            Navitus__c,Navitus_EmailSent__c,
                            Name,
                            Oncology__c,
                            Physical_Activities_Notes__c, Progyny__c, Progyny_EmailSent__c, Psych_Notes__c,
                            Physical_Activities_HR__c,Physical_Activities_HR_emailSent__c,
                            Premera_BC__c,
                            Premera_BC_emailSent__c,
                            Prescription_Drugs__c,Prescription_Drugs_EmailSent__c,
                            Principal__c, Principal_EmailSent__c,
                            Qualsight__c,Qualsight_Email_Sent__c,
                            Rethink__c, Rethink_EmailSent__c,
                            Sedgewick_LOA__c, Sedgewick_LOA_EmailSent__c,
                            Sunflower_Wellness__c,
                            Sunflower_Wellness_EmailSent__c,
                            Supplemental_Resource__c,Supplemental_Resource_EmailSent__c,
                            Supplemental_Insurance__c,Supplemental_Insurance_EmailSent__c,
                            Teladoc__c, Teladoc_EmailSent__c,
                            Telemedicine__c,Telemedicine_EmailSent__c,
                            Tivity__c,Tivity_EmailSent__c,
                            Top_Concern1__c, Top_Concern2__c, Top_Concern3__c,Top_Concern4__c,Top_Concern5__c,Top_Concern6__c,
                            TrueHearing__c,TrueHearing_EmailSent__c,
                            YouDecide__c,YouDecide_EmailSent__c,
                            Vida_Health__c,Vida_Health_Email_Sent__c,
                            Vision_Carrier__c,Vision_Carrier_EmailSent__c,
                            VSP__c, VSP_EmailSent__c,
                            Workers_Compensation__c,Workers_Compensation_EmailSent__c,
                            Work_Life_Notes__c,  X2nd_MD__c, X2nd_MD_EmailSent__c,
                            X401_k__c,X401_k_EmailSent__c from Oncology_Needs_Assessment__c where Oncology__c = :parentId order by createdDate limit 1];
            if(need==null || need.id==null){
                newNeed();
            }
        }catch(queryException e){
            newNeed();
        }

    }

     void newNeed(){
        Need = new Oncology_Needs_Assessment__c (Oncology__c =parentId);

    }

    public class saveResponse{
        public boolean isError;
        public string message;
    }

    public saveResponse saveNeed(){
        saveResponse sr = new saveResponse();
        sr.isError=false;

        try{

            system.debug(need);
            upsert Need;
            loadNeed();
            return sr;

        }catch(dmlException e){
           System.debug(e.getDmlMessage(0)+' '+e.getLineNumber());

           sr.isError=true;
           sr.message=e.getDmlMessage(0);
           return sr;

        }

    }




}
