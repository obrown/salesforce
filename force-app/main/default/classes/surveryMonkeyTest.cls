@isTest
private class surveryMonkeyTest {
    static testMethod void testRun() {
       
        patient_case__c pc = new patient_case__c(patient_first_name__c='Unit',patient_last_name__c='Test',employee_first_name__c='Unit',employee_last_name__c='Test', Actual_Arrival__c=date.valueof('2015-01-16'), Actual_Departure__c=date.valueof('2015-01-18'));
        insert pc;
        
        surveyMonkeyReport smr = new surveyMonkeyReport();
        smr.run(true, date.valueof('2015-02-01'));
        smr.run(true, date.valueof('2015-02-15'));
        smr.run(true, date.valueof('2015-01-01'));
        smr.run(true, date.valueof('2015-01-02'));
        
    }
}