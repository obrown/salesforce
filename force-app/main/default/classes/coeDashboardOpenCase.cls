public with sharing class coeDashboardOpenCase{

    transient patient_case__c[] CaseList;
    
    public transient set<string> openCaseSet {get; private set;}
    public transient set<string> openCaseInnerSet {get; set;}
    public transient map<string, map<string, integer>> OpenCaseCount {get; private set;}
    public transient map<string, integer> OpenCaseTotal {get; private set;}
    public transient map<string, map<string, patient_case__c[]>> OpenCase {get; private set;}
    
    sObject generic;
    string grouping = 'client_name__c';
    
    id coeID;
    
    public coeDashboardOpenCase(patient_case__c[] foo, id coeID, set<string> oci){
        //this.openCaseInnerSet = new set<string>();
        this.openCaseInnerSet = oci;
        this.coeID = coeID;
        this.CaseList = foo;
    }
    
    public void populateOpenCase(){
        
        openCaseSet = new set<string>();
        
        OpenCase  = new map<string, map<string, patient_case__c[]>>();
        OpenCaseCount = new map<string, map<string, integer>>();
        OpenCaseTotal = new map<string, integer>();
        
        intakeOwner();
        awaitingPOC();
        MemberAdvocateCollection();
        ReadyforArrival();
        patientAtCOE();
        claimProcessing();
        openCaseSet = OpenCase.keySet();
        
        map<string, integer> fooMap;

        /*
            Loop over the openCaseMap, ie CM, CS, Awaiting POC, etc
            Then loop over 
        */
        
                
        for(string ojkey : openCaseSet){
            
            integer i =0;
            fooMap=new  map<string, integer>();
            for(string s: openCaseInnerSet){
              fooMap.put(s,0);
            
            }

            this.OpenCaseCount.put(ojkey, fooMap);
            
            for(string ss : fooMap.keyset()){
                
                patient_case__c[] fooList = OpenCase.get(ojkey).get(ss);
                if(fooList==null){
                    fooList = new patient_case__c[]{};
                }
                
                fooMap.put(ss, fooList.size());
                i +=fooList.size();
                this.OpenCaseCount.put(ojkey, fooMap);
                
            }
            OpenCaseTotal.put(ojkey, i);
        }
        
    }
    
    void patientAtCOE(){
        
        map<string, patient_case__c[]> ct = new map<string, patient_case__c[]>();
        
        for(string s : openCaseInnerSet){
            ct.put(s,new patient_case__c[]{});
        }
        
        patient_case__c[] plist = new patient_case__c[]{};
        string type;
        
        for(patient_case__c p : CaseList ){
           
            if(p.isConverted__c == true && p.status__c == 'Open' && p.status_reason__c == 'Patient at COE'){
                generic = p;    
                type=(string)generic.get(grouping);
                plist = ct.get(type);
                
                if(plist ==null){
                    plist = new patient_case__c[]{};
                }
            
                plist.add(p);
                
            }

        }
        ct.put(type, plist);
        OpenCase.put('Patient at COE', ct);
    
    }
    
    void ReadyforArrival(){
        
        map<string, patient_case__c[]> ct = new map<string, patient_case__c[]>();
        for(string s : openCaseInnerSet){
            ct.put(s,new patient_case__c[]{});
        }
        patient_case__c[] plist = new patient_case__c[]{};
        string type;
        
        for(patient_case__c p : CaseList ){
        
            if(p.isConverted__c == true && p.status__c == 'Open' && p.status_reason__c == 'Ready for Arrival'){
                generic = p;
                type = (string)generic.get(grouping);
                plist = ct.get(type);
                
                if(plist ==null){
                    plist = new patient_case__c[]{};
                }
            
                plist.add(p);
                
            }

        }
        
        ct.put(type, plist);
        OpenCase.put('Claim Processing', ct);
        
    }
    
    void claimProcessing(){
    
        map<string, patient_case__c[]> ct = new map<string, patient_case__c[]>();
        
        for(string s : openCaseInnerSet){
            ct.put(s,new patient_case__c[]{});
        }
        patient_case__c[] plist = new patient_case__c[]{};
        string type;
        
        for(patient_case__c p : CaseList ){
         
            if(p.isConverted__c == true && p.status__c == 'Open' && (p.status_reason__c == 'Claim Pending' || p.status_reason__c == 'Claim received')){
                generic = p; 
                type = (string)generic.get(grouping);
                plist = ct.get(type);
                
                if(plist ==null){
                    plist = new patient_case__c[]{};
                }
            
                plist.add(p);
                
            }

        }
        ct.put(type, plist);
        OpenCase.put('Claim Processing', ct);
        
    }
    
    void MemberAdvocateCollection(){
    
        map<string, patient_case__c[]> ct = new map<string, patient_case__c[]>();
        
        for(string s : openCaseInnerSet){
            ct.put(s,new patient_case__c[]{});
        }
        
        patient_case__c[] plist = new patient_case__c[]{};
        string type;
        
        for(patient_case__c p : CaseList ){
            if(p.isConverted__c == true && p.status__c == 'Open' && (p.status_reason__c == 'Awaiting Appointment Date Confirmations' || p.status_reason__c == 'Awaiting Forms' || p.status_reason__c == 'Awaiting Travel Confirmation')){
                generic = p;    
                type =(string)generic.get(grouping);
                plist = ct.get(type);
                
                if(plist ==null){
                    plist = new patient_case__c[]{};
                }
                
                plist.add(p);
            }

        }
        
        ct.put(type, plist);
        OpenCase.put('MA Collection', ct);
    }
    
    map<string, patient_case__c[]> awaitingPOC(){
    
        map<string, patient_case__c[]> ct = new map<string, patient_case__c[]>();
        
        for(string s : openCaseInnerSet){
            ct.put(s,new patient_case__c[]{});
        }
        
        patient_case__c[] plist = new patient_case__c[]{};
        string type;
        for(patient_case__c p : CaseList ){
           
            if(p.status_reason__c == 'Awaiting Plan of Care' && p.isConverted__c == true && p.status__c == 'Open'){
                generic = p; 
                type = (string)generic.get(grouping);
                plist = ct.get(type);
                
                if(plist ==null){
                    plist = new patient_case__c[]{};
                }
            
                plist.add(p);
                
            }

        }
        
        ct.put(type, plist);
        OpenCase.put('Awaiting POC', ct);
        
        return ct;
    }
    
    map<string, patient_case__c[]> intakeOwner(){
    
        map<string, patient_case__c[]> cm = new map<string, patient_case__c[]>();
        map<string, patient_case__c[]> cs = new map<string, patient_case__c[]>();
        
        for(string s : openCaseInnerSet){
            cm.put(s,new patient_case__c[]{});
            cs.put(s,new patient_case__c[]{});
        }
        
        patient_case__c[] fooplist = new patient_case__c[]{};
    
        map<string, patient_case__c[]> foo = new map<string, patient_case__c[]>();
        patient_case__c[] plist = new patient_case__c[]{};
       
        foo.put('CM', new patient_case__c[]{});
        foo.put('CS', new patient_case__c[]{});
        
        for(patient_case__c p : CaseList){
        generic = p;    
            if(p.status__c == 'Open' && p.isConverted__c == false){
            
                if(p.ownerID==coeID){
                    plist = foo.get('CM');
                    plist.add(p);
                    foo.put('CM', plist);
                    
                    fooplist = cm.get((string)generic.get(grouping));
                
                    if(fooplist ==null){
                        fooplist = new patient_case__c[]{};
                    }
            
                    fooplist.add(p);
                    cm.put((string)generic.get(grouping), fooplist);
                
                }else{
                    plist = foo.get('CS');
                    plist.add(p);
                    foo.put('CS', plist);
                    
                    fooplist = cs.get((string)generic.get(grouping));
                
                    if(fooplist ==null){
                        fooplist = new patient_case__c[]{};
                    }
            
                    fooplist.add(p);
                    cs.put((string)generic.get(grouping), fooplist);
                } 
                
                
            }

        }
        
        OpenCase.put('CM', cm);
        OpenCase.put('CS', cs);
        
        return foo;
    }
        
}