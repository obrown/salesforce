public with sharing class codeController {

        String codeType;
        String codeList;
        String theintake;
        String theProgram;
        public string isError {get;set;}
        public boolean isQualified;
        public Clinical_Codes__c objCC {get;set;}
        public transient PaginatedSelectList Options;
        ApexPages.StandardController theController;
        id theID;
                               
        public codeController(ApexPages.StandardController controller) {
                  
              theController = controller;
              isError = '0';
              if (!Test.isRunningTest()){ 
                list<String> theFields = new list<String>();
                theFields.add('code_type__c');
                theFields.add('code_lookup__c');
                theFields.add('Code_lookup__r.name');
                theController.addFields(theFields);
                this.objCC = (Clinical_Codes__c)theController.getRecord();
                
              }
              
              theID = theController.getId();
              if(Test.isRunningTest()){ 
                theID = theController.getId();
                this.objCC = [select Code_lookup__c,Code_type__c from Clinical_Codes__c where id = :theID];
              }
                  
        }
        
        public List<SelectOption> getcTypeItems() {
        
        List<SelectOption> options = new List<SelectOption>();
                //for(Clinical_Codes__c c: [select Code_Type__c, name from Clinical_Codes__c]){
                        
                        options.add(new SelectOption('--None--','--None--'));
                        options.add(new SelectOption('DRG','DRG'));
                        options.add(new SelectOption('CPT4','CPT4'));
                        options.add(new SelectOption('Pre-Admit','Pre-Admit'));
                        options.add(new SelectOption('ICD9 DIAG','ICD9 DIAG'));
                        options.add(new SelectOption('ICD9 PROC','ICD9 PROC'));
                //}
                
                return options;
    }
        
    public PaginatedSelectList getOptions(){
       // get{       
            if(codeType == null){
               codeType = objcc.code_type__c;
            }
            
            //if(Options2==null){
                  Options=new PaginatedSelectList();
                  for(Code__c c: [select code__c, Code_Description__c,name,type__c from Code__c where type__c =:codeType order by code__c asc]){
                        Options.add(new SelectOption(c.id,c.code__c + ' ' + c.code_description__c));
                  }
            //}
            return Options;
        //}
        //set;
    }
           
    public String getcodeList() {
       if(codeList == null){
       return objCC.Code_lookup__c;    
       }else{
       return codeList;
       }
            
       
    }
            
    public void setcodeList(String codeList) {
           this.codeList = codeList;
    }
    
    public String getcodeType() {
       if(codeType == null){
       return objCC.Code_Type__c;
       }else{
       return codeType;
       }
    }
            
    public void setcodeType(String codeType) {
        
           this.codeType = codeType;
    }
    
    public void refreshCodeList(){
        if(options!=null){
            options.getSelectList();
        }
    }

    public pagereference mySave(){
      PageReference returnPage;
      objCC.code_type__c = codeType;
      objCC.code_lookup__c = codeList;
      try{
      
        upsert objcc;
        returnPage = new PageReference('/' + objCC.id);
      
      }catch(exception e){
        isError = '1';
        string errorMessage;
        if(ApexPages.getMessages().isEmpty()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, utilities.errorMessageText(e.getMessage())));
        }

        returnPage = null;
      }     
        return returnpage;    
    }
          
}