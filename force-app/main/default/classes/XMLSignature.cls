public class XMLSignature{  
 
   public static String generateXMLSignature(String docBodyString, string sigId, string bodyId){
    
    string tsDigest;
    string bodyDigest;
    
    // 1. Create timestamp string and timestamp digest
    
    string tsid = 'A' + string.valueof(math.abs(crypto.getRandomLong()));
    string keyInfoID ='A' + string.valueof(math.abs(crypto.getRandomLong()));
    string securityTokenReferenceID  = 'A' + string.valueof(math.abs(crypto.getRandomLong()));
    
    string strcreated = datetime.now().formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
    string strExpires =datetime.now().addSeconds(600).formatGMT('yyyy-MM-dd\'T\'HH:mm:ss.SSS\'Z\'');
    
    string tsCanonicalString ='<wsu:Timestamp xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v2="http://alight.com/hro/benefits/cm/xsd/v2_0" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:xsd="http://alight.com/hro/benefits/healthDesignPlus/xsd" wsu:Id="TS-' + tsid + '">';
    string tsbody = '<wsu:Created>' + strcreated + '</wsu:Created>';
    tsbody += '<wsu:Expires>' + strExpires + '</wsu:Expires>';
    tsbody += '</wsu:Timestamp>';
    
    tsDigest = encodingUtil.Base64Encode(crypto.generateDigest('SHA-256',blob.valueof(tsCanonicalString+tsbody)));
    string tsDisplayString ='<wsu:Timestamp wsu:Id="TS-' + tsid + '">' + tsBody;
    
    // 2. Apply the Transforms, as determined by the application, to the data object.
    // (no transforms are done, since the body should be delivered in canonicalized form (Force.com offers no canonicalization algorithm))
    
    string bodyCanonicalString = '<soapenv:Body xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v2="http://alight.com/hro/benefits/cm/xsd/v2_0" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:xsd="http://alight.com/hro/benefits/healthDesignPlus/xsd" wsu:Id="id-' + bodyId + '">' + docBodyString;
    
    // 2. Calculate the digest value's over the resulting data object. 
    
    bodyDigest = encodingUtil.Base64Encode(Crypto.generateDigest('SHA-256',Blob.valueOf(bodyCanonicalString)));
    
    // 3. Create a Reference element, including the (optional) identification of the data object, any (optional) transform elements, the digest algorithm and the DigestValue.
    // (Note, it is the canonical form of these references that are signed and validated in next steps.)
    
    String canonicalReferenceString = '<ds:Reference URI="#id-' + bodyId + '">';
    String displayReferenceString = '<ds:Reference URI="#id-' + bodyId + '">';
    
    canonicalReferenceString += '<ds:Transforms>';
    displayReferenceString += '<ds:Transforms>';
    
    canonicalReferenceString += '<ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">';
    displayReferenceString += '<ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">';
    
    canonicalReferenceString += '<InclusiveNamespaces xmlns="http://www.w3.org/2001/10/xml-exc-c14n#" PrefixList="v2 xsd"></InclusiveNamespaces>';
    displayReferenceString += '<InclusiveNamespaces PrefixList="v2 xsd" xmlns="http://www.w3.org/2001/10/xml-exc-c14n#"/>';
    
    canonicalReferenceString += '</ds:Transform>';
    displayReferenceString += '</ds:Transform>';
    
    canonicalReferenceString += '</ds:Transforms>';
    displayReferenceString += '</ds:Transforms>';
    
    canonicalReferenceString += '<ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod>';
    displayReferenceString += '<ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>';
    
    canonicalReferenceString += '<ds:DigestValue>'+bodyDigest+'</ds:DigestValue>';
    displayReferenceString += '<ds:DigestValue>'+bodyDigest+'</ds:DigestValue>';
    
    canonicalReferenceString += '</ds:Reference>';   
    displayReferenceString += '</ds:Reference>';   
    
    canonicalReferenceString += '<ds:Reference URI="#TS-' + tsid + '">';
    displayReferenceString += '<ds:Reference URI="#TS-' + tsid + '">';
    
    canonicalReferenceString += '<ds:Transforms>';
    displayReferenceString += '<ds:Transforms>';
    
    canonicalReferenceString += '<ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">';
    displayReferenceString += '<ds:Transform Algorithm="http://www.w3.org/2001/10/xml-exc-c14n#">';
    
    canonicalReferenceString += '<InclusiveNamespaces xmlns="http://www.w3.org/2001/10/xml-exc-c14n#" PrefixList="wsse soapenv v2 xsd"></InclusiveNamespaces>';
    displayReferenceString += '<InclusiveNamespaces PrefixList="wsse soapenv v2 xsd" xmlns="http://www.w3.org/2001/10/xml-exc-c14n#"/>';
    
    canonicalReferenceString += '</ds:Transform>';
    displayReferenceString += '</ds:Transform>';
    
    canonicalReferenceString += '</ds:Transforms>';
    displayReferenceString += '</ds:Transforms>';
    
    canonicalReferenceString += '<ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"></ds:DigestMethod>';
    displayReferenceString += '<ds:DigestMethod Algorithm="http://www.w3.org/2001/04/xmlenc#sha256"/>';
    
    canonicalReferenceString += '<ds:DigestValue>'+ tsDigest +'</ds:DigestValue>';
    displayReferenceString += '<ds:DigestValue>'+ tsDigest +'</ds:DigestValue>';
    
    canonicalReferenceString += '</ds:Reference>';        
    displayReferenceString += '</ds:Reference>';        

    String signedInfoString = '<ds:SignedInfo>';
    String canonicalsignedInfoString = '<ds:SignedInfo xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:v2="http://alight.com/hro/benefits/cm/xsd/v2_0" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd" xmlns:xsd="http://alight.com/hro/benefits/healthDesignPlus/xsd">';
    
    canonicalsignedInfoString += '<ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"></ds:CanonicalizationMethod>';
    signedInfoString +='<ds:CanonicalizationMethod Algorithm="http://www.w3.org/TR/2001/REC-xml-c14n-20010315"/>';
    
    canonicalsignedInfoString += '<ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"></ds:SignatureMethod>';
    signedInfoString += '<ds:SignatureMethod Algorithm="http://www.w3.org/2001/04/xmldsig-more#rsa-sha256"/>';
    
    canonicalsignedInfoString += canonicalReferenceString;
    signedInfoString += displayReferenceString;
    
    canonicalsignedInfoString += '</ds:SignedInfo>';
    signedInfoString += '</ds:SignedInfo>';
    
    
    // 5. Canonicalize and then calculate the SignatureValue over SignedInfo based on algorithms specified in SignedInfo.
    // (no canonicalization is done, since the signedinfo element should be delivered in canonicalized form (Force.com offers no canonicalization algorithm))

    String signatureValueString = encodingUtil.Base64Encode(crypto.signwithCertificate('RSA-SHA256', blob.valueof(canonicalsignedInfoString), 'ASGWebservices'));
    
    // 6. Construct the Signature element that includes SignedInfo, Object(s) (if desired, encoding may be different than that used for signing), KeyInfo (if required), and SignatureValue. 
    String signatureString = '';
    
    signatureString += '<ds:Signature Id="SIG-' + sigId + '" xmlns:ds="http://www.w3.org/2000/09/xmldsig#">';
    signatureString += signedInfoString; 
    signatureString += '<ds:SignatureValue>';
    signatureString += signatureValueString;
    signatureString += '</ds:SignatureValue>';
    signatureString += '<ds:KeyInfo Id="KI-' + keyInfoID + '">';
    signatureString += '<wsse:SecurityTokenReference wsu:Id="STR-' + securityTokenReferenceID + '">';
    signatureString += '<wsse:Reference URI="#X509-' + sigId + '" ValueType="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-x509-token-profile-1.0#X509v3"/>';
    signatureString += '</wsse:SecurityTokenReference>';
    signatureString += '</ds:KeyInfo>';
    signatureString += '</ds:Signature>';
    
    signatureString += tsDisplayString;
    return signatureString;
  }
}