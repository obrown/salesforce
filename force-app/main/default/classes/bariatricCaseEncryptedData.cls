public with sharing class bariatricCaseEncryptedData {
    public string patientFirstName { get; set; }

    public string patientLastName { get; set; }

    public string patientStreet { get; set; }

    public string patientCSZ { get; set; }

    public string id { get; set; }

    public string certID { get; set; }
}
