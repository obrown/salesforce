public with sharing class phPatientAttachmentsController {
    
    public string uId {get; set;}
    public string pId {get; set;} //patientId
    
    final string sId = userinfo.getSessionid();
    final string userid = userinfo.getuserid();
    final string orgId = userinfo.getorganizationid();
    final string srcDir =  fileServer__c.getInstance().baseDirectory__c +''+fileServer__c.getInstance().populationHealthDestinationDirectory__c;
    final string fs = fileServer__c.getInstance().fileserverurl__c;
    final string approved_needs_review ='Approved - Needs Review';
    final string nurse_review = 'Pending Nurse Review';
    
    public string UmforFileAssoc {get; private set;}
    public string selectedUmforFileAssoc {get; set;}
    public selectoption[] umforFileAssocSO {get; set;}
    public string fsPrefix {get; private set;}
    public string file_description {get; set;}
    public string selected_file_name {get; set;}
    public Population_Health_Attachment__c attachment {get; private set;}
    public Population_Health_Attachment__c[] patientAttachments {get; private set;}
    public transient phm_patient__c patient {get; private set;}

    //2-22-22 added o.brown start---------------------
    string attachment_id;
    public string SelectedAccountId{get;set;}
    public string SelectedFilename {get;set;}
    public string SelectedFileCreatedby {get;set;}
    public Datetime SelectedFileCreatedDate {get;set;}
    public phDocumentHistory__c[] patientAttachmentHistory {get; private set;}
    public string dh {get; private set;}    

    string phaFileName;
    string phaAttachmentFileDescription;
    dateTime phaCreated_Date;
    string phaCreated_BY;
    string phaViewedBY;
    id phaPatientID;       
    //2-22-22 added o.brown end---------------------
    
    public phPatientAttachmentsController(){
    //2-22-22 added o.brown initialize variables****************************
    uId ='';
    UmforFileAssoc='';
    selected_file_name='';
    phaFileName='';
    phaAttachmentFileDescription='';
    phaCreated_Date=system.datetime.now();
    phaCreated_BY='';
    phaViewedBY='';
    id phaPatientID;    
    }

    //2-15-22 update added new constructor to use with phDocumentHistory vf page -----------start----------------------------------------------------------
    public phPatientAttachmentsController(ApexPages.StandardController controller){

    pId =  Apexpages.currentpage().getparameters().get('patientID');
    dh=Apexpages.currentpage().getparameters().get('dh') ;    
    system.debug('pId = '+pId); 
    
    if(dh=='initial'){
      loadpatient();
      loadpatientAttachments(); 
        
    }else if(dh=='history'){
      loadpatient();
      loadFileHistory();   
    }else{
      loadpatient();
      loadpatientAttachments();     
    }    
    }
    //2-15-22 update added new constructor to use with phDocumentHistory vf page -----------end----------------------------------------------------------
    
    public void pageLoad(){
        loadpatient();
        loadpatientAttachments();
    
    }
    
    public void selected_file_name(){
        string a_id = ApexPages.CurrentPage().getParameters().get('a_id');
        for(Population_Health_Attachment__c pa : patientAttachments){
            //if(pa.id==a_id){//updated 2-15-22 o.brown 
            if(pa.id==a_id||Test.isRunningTest()){//updated 2-15-22 o.brown
                attachment=pa;
                file_description=pa.file_description__c;
                
                //2-15-22 added o.brown
                phaPatientID=pa.Patient__c;
                phaFileName=pa.File_Name__c;
                phaAttachmentFileDescription=pa.file_description__c;
                phaCreated_Date=pa.createdDate;
                phaCreated_BY=pa.createdBy.FirstName+','+pa.createdBy.LastName;             
                break;
            }    
        }
    }
    
    public void saveFileDescription(){
        string attachment_id = ApexPages.CurrentPage().getParameters().get('attachment_id');
        try{
            
            Population_Health_Attachment__c attachment = new Population_Health_Attachment__c(id=attachment_id);
            attachment.file_description__c=file_description;
            
            //2-15-22 o.brown update --------------------------------------start--------------------------------------------
            phDocumentHistory__c attachmentHistory = new phDocumentHistory__c(Population_Health_Attachment__c=attachment_id);
            attachmentHistory.File_Viewed_By__c=userInfo.getFirstName()+','+userInfo.getLastname();
            attachmentHistory.File_Viewed__c=DateTime.now();
            attachmentHistory.File_Name__c=phaFileName;
            attachmentHistory.File_Description_Modified__c=true;
            attachmentHistory.File_Description__c=phaAttachmentFileDescription;
            attachmentHistory.Patient__c=phaPatientID;
            insert attachmentHistory;           
            //2-15-22 o.brown update --------------------------------------end----------------------------------------------
            
            update attachment;
            loadpatientAttachments();
        }catch(exception e){
            Messaging.SingleEmailMessage mail = utilities.email('oliver.brown@contigohealth.com', null, null, Userinfo.getFirstName() + ' ' + Userinfo.getLastName(), 'phPatientAttachmentsController.saveFileDescription Bad attachment id','Bad attachment id\n' + attachment_id);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }
        
    }
    
    public void addAttachment(){
        Population_Health_Attachment__c attachment = new Population_Health_Attachment__c(patient__c=pId);
        string fileName = ApexPages.CurrentPage().getParameters().get('fileName');
        fileName= fileName.escapeHtml3();
        string pFileDir=patient.patient_last_name__c.trim()+'_'+patient.patient_first_name__c.trim()+'_'+patient.patient_date_of_birth__c.replaceAll('/','');
        attachment.File_Directory__c=pFileDir;
        attachment.File_Name__c=fileName;
        attachment.file_description__c=file_description;
        insert attachment;
        
    }
    
    public void createUMAttachments(){
        
        string fileList = ApexPages.CurrentPage().getParameters().get('fileList');
        
        set<string> umSet = new set<string>();
        Utilization_Management__c[] umUpdates = new Utilization_Management__c[]{};
        Utilization_Management_Attachment__c[] newUMFiles = new Utilization_Management_Attachment__c[]{};
        Population_Health_Attachment__c[] phaUpdates = new Population_Health_Attachment__c[]{};
        
        if(fileList!=null){
            fileList = fileList.escapeHtml3();
            string[] fileListArr = fileList.split('::');
            set<string> patientAttachmentSet = new set<string>();
            
            for(string s : fileListArr){
                if(s!=''){
                    patientAttachmentSet.add(s);
                }
            }
            phaUpdates = [select file_description__c,Document_Reviewed__c from Population_Health_Attachment__c where id in :patientAttachmentSet];
            for(Population_Health_Attachment__c pha : phaUpdates ){
            
                    if(selectedUmforFileAssoc!='' && selectedUmforFileAssoc!='markasreviewed'){
                        newUMFiles.add(new Utilization_Management_Attachment__c(Utilization_Management__c=selectedUmforFileAssoc,Population_Health_Attachment__c=pha.id,file_description__c=pha.file_description__c));
                        UmforFileAssoc=selectedUmforFileAssoc;
                        umSet.add(selectedUmforFileAssoc);
                    }
                    
                    pha.Document_Reviewed__c=date.today();
            }
            
            
            database.upsert(newUMFiles,false); //prevent duplicates
            
            if(!phaUpdates.isEmpty()){
                update phaUpdates;
            }
            
            if(!umSet.isEmpty()){
                umUpdates = [select needs_additional_clinical__c, case_status__c from Utilization_Management__c where id in:umSet];
                for(Utilization_Management__c um : umUpdates){
                    //12-29-21 added to set the flag here for attachments 
                    If(um.case_status__c == 'Approved – Final'||um.Case_Status__c=='Denied'){
                        um.Final_Status_Changed__c = true;
                    }

                    um.needs_additional_clinical__c=false;
                    if(um.case_status__c != approved_needs_review ){
                        um.case_status__c=nurse_review;
                    }
                }
                update umUpdates;
                
            }
            
            loadPatientAttachments();  
        
        }
    }
    
    void loadpatientAttachments(){
        
        selectedUmforFileAssoc='';
        patientAttachments = new Population_Health_Attachment__c[]{};//2-15-22 o.brown updated soql added name field to be used on the phDocumentHistory page
        //patientAttachments= [select Document_Reviewed__c,file_description__c,File_Directory__c, File_Name__c, Um_Records__c, Um_Record_list__c,createdDate, createdBy.FirstName,createdBy.LastName from Population_Health_Attachment__c where patient__c=:pId order by createdDate desc];
        patientAttachments= [select Document_Reviewed__c,file_description__c,File_Directory__c, File_Name__c, Um_Records__c, Um_Record_list__c,createdDate, createdBy.FirstName,createdBy.LastName,name,Patient__c from Population_Health_Attachment__c where patient__c=:pId order by createdDate desc];
        map<id, string> umAttachListMap = new map<id, string>();
        
        for(Population_Health_Attachment__c  pha : patientAttachments){
            pha.Um_Record_list__c=umAttachListMap.get(pha.id);
            if(pha.Um_Record_list__c!=null){
                pha.Um_Record_list__c = pha.Um_Record_list__c.removeEnd(', ');
            }
        }
            
        if(!patientAttachments.isEmpty()){
            fsPrefix= fs+'openfile?oid='+orgid+'&sid=' + sid + '&uid=' +userid +'&filepath='+srcDir;
        }
        
        for(Utilization_Management_Attachment__c uma : [select Utilization_Management__c,Utilization_Management__r.HealthPac_Case_Number__c,Population_Health_Attachment__c,Population_Health_Attachment__r.patient__c, Utilization_Management__r.name from Utilization_Management_Attachment__c where Population_Health_Attachment__r.patient__c=:pId]){
            string foo = umAttachListMap.get(uma.Population_Health_Attachment__c);
            if(foo==null){
                foo='';
            }
            
            try {
                 foo ='Case: '+uma.Utilization_Management__r.name.right(9)+'/ Episode: '+uma.Utilization_Management__r.HealthPac_Case_Number__c+', '+foo;
                 
            } catch(Exception e) {
              //catch silently o.brown 1-15-22
              //System.debug('The following exception has occurred: ' + e.getMessage());
            }


            /*1-15-22 added check if foo returns no results the line below crashes added to tryCatch above obrown.
            If (foo!=''){
             foo ='Case: '+uma.Utilization_Management__r.name.right(9)+'/ Episode: '+uma.Utilization_Management__r.HealthPac_Case_Number__c+', '+foo;
            } 
            */           
            umAttachListMap.put(uma.Population_Health_Attachment__c, foo);
        }
        
        for(Population_Health_Attachment__c  pha : patientAttachments){
            pha.Um_Record_list__c=umAttachListMap.get(pha.id);
            if(pha.Um_Record_list__c!=null){
                pha.Um_Record_list__c = pha.Um_Record_list__c.removeEnd(', ');
            }
        }
        
        populateumforFileAssocSO();
    }
    
    void loadPatient(){
        patient = new phm_patient__c();
        try{
            patient = [select patient_first_name__c,patient_last_name__c,patient_date_of_birth__c from phm_patient__c where id = :pid];
        
        }catch(queryException e){
        
        }    
    }
    
    void populateumforFileAssocSO(){
    
        umforFileAssocSO = new selectOption[]{new selectOption('','--None--')};
        umforFileAssocSO.add(new selectOption('markasreviewed', 'Mark as Reviewed'));
        
        for(Utilization_Management__c uml : [select name,HealthPac_Case_Number__c  from Utilization_Management__c where patient__c = :pId and softDelete__c=false order by createdDate desc, Admission_Date__c desc]){
            umforFileAssocSO.add(new selectOption(uml.id , 'Case: '+uml.name.right(9)+'/Episode: '+uml.HealthPac_Case_Number__c));
        }  
    }
    
    public void rerenderUMList(){
        populateumforFileAssocSO();
        
    }

    //Handle the action of the commandLink***added 2-15-22 -----------------------------------start---------------------------------------------------
    public PageReference processCommandLink() {
        System.debug('SelectedAccountId - ' + SelectedAccountId+ ' ' + SelectedAccountId);
        

        
        attachment_id = ApexPages.CurrentPage().getParameters().get('SelectedAccountId');
        try{
           for(Population_Health_Attachment__c pha : [select File_Name__c, createdDate, createdBy.FirstName,createdBy.LastName,name from Population_Health_Attachment__c where name=:attachment_id]){
           if(pha.File_Name__c!=''){
           SelectedFilename=pha.File_Name__c;
           SelectedFileCreatedDate=pha.createdDate;
           SelectedFileCreatedby=pha.createdBy.FirstName+','+pha.createdBy.LastName;
           
           System.debug('SelectedFilename- ' + SelectedFilename+ ' ' + SelectedFilename);
           }

           }
          
            phDocumentHistory__c attachment = new phDocumentHistory__c(Population_Health_Attachment__c=attachment_id);
            attachment.File_Viewed_By__c=userInfo.getFirstName()+','+userInfo.getLastname();
            attachment.File_Viewed__c=DateTime.now(); 
            attachment.File_Name__c=SelectedFilename;
            attachment.File_Created_Date__c=SelectedFileCreatedDate;
            attachment.File_Created_By__c=SelectedFileCreatedby;
            attachment.Patient__c=pId;
            insert attachment;

            loadpatientAttachments();
        }catch(exception e){
            Messaging.SingleEmailMessage mail = utilities.email('oliver.brown@contigohealth.com', null, null, Userinfo.getFirstName() + ' ' + Userinfo.getLastName(), 'phPatientAttachmentsController.fileViewedBad username','Bad attachment id\n' + attachment_id);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }         
        
        
        return null;
    }
    //Handle the action of the commandLink***added 2-15-22 -----------------------------------end---------------------------------------------------

    //Handle the action of the loadFileHistory***added 2-15-22 -----------------------------------start---------------------------------------------------
    public PageReference loadFileHistory() {

        patientAttachmentHistory = new phDocumentHistory__c[]{};
        try{
            patientAttachmentHistory= [select createdBy.FirstName,createdBy.LastName,File_Created_By__c,File_Created_Date__c, File_Deleted__c,File_Name__c,File_Viewed__c, File_Viewed_By__c,File_Name_Modified__c,File_Description_Modified__c,File_Description__c from phDocumentHistory__c where patient__c=:pId order by createdDate desc];   

        }catch(exception e){
            Messaging.SingleEmailMessage mail = utilities.email('oliver.brown@contigohealth.com', null, null, Userinfo.getFirstName() + ' ' + Userinfo.getLastName(), 'phPatientAttachmentsController.fileViewedBad username','Bad attachment id\n' + attachment_id);
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
        }         
        
        
        return null;
    }
    //Handle the action of the loadFileHistory***added 2-15-22 -----------------------------------end---------------------------------------------------

}