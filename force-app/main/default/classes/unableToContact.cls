public with sharing class unableToContact{
    
    //public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician,string userName,string userTitle,string userPhone,string userEmail){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);

        string disclaimerPath;
        
        StaticResource logo;
        StaticResource[] srList = [SELECT Id,NamespacePrefix,SystemModstamp,Name FROM StaticResource WHERE Name = 'cmSurvey' order by Name asc];
        logo=srList[0];
        
        String prefix = logo.NamespacePrefix;
        if( String.isEmpty(prefix) ) {
            prefix = '';
        }else{
            //If has NamespacePrefix
            prefix += '__';
        }        

        disclaimerPath='/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'ohyDisclaimer';//added 1-30-21 to set pull in the disclaimerPath
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=cm.patient__r.Patient_First_Name__c+' '+cm.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Address__c: +'<br/>';
        letterText+='<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.City__c+', ' : ''+'&nbsp';
        letterText+=hasPatientAddress ? cm.patient__r.State__c+' ' : ''+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Zip__c : ''+'<br/><br/>';
        letterText+='<br/><br/>';

        letterText+='</div>';
        letterText+='<p>';
        letterText+='Dear&nbsp;'+cm.patient__r.Patient_First_Name__c+',<br/><br/>';
        letterText+='Thank you for choosing OhioHealthy!<br/><br/>';
        letterText+='I have been trying to contact you so that we can discuss your health and any health-related concerns you may have. <br/><br/>';
        letterText+='The OhioHealthy Healthcare Services team is ready and available to assist you in achieving your healthcare goals. We can provide education, support and coordination of your care, but ';
        letterText+='we do not replace the advice given to you by your doctors. There is no fee for our services.<br/><br/>';
        letterText+='Please call me at the number below between 8:00 a.m. and 5:00 p.m., Monday through Friday. If you leave a message, please leave your name, phone number and the best time to reach you.<br/><br/>';
        //letterText+='reach you.<br/><br/>';
        letterText+='I am here to help you take charge of your health. I hope to hear from you soon!';
        letterText+='</p>';
        
        letterText+='<p>';  
        letterText+='Sincerely,<br/><br/>';
        /*
        letterText+='Care Management Department<br/>';
        letterText+='Ohio Healthy<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';
        */
        letterText+=userName+'<br/>';
        letterText+=userTitle+'<br/>';
        letterText+=userPhone+'<br/>';
        letterText+=userEmail+'<br/>';        
        
        letterText+='</p>';
        
        letterText+='<div><img src="'+disclaimerPath+'" style="width:320px;margin-left:-3px"/></div><br/>';// pulls in the image of the surveyPath
       
        return letterText;
    }
    
}