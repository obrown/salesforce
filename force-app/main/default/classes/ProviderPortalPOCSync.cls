public with sharing class ProviderPortalPOCSync {
    public final string patientcaseid { get; private set; }
    string clientfacilityid; // TODO remove; pretty sure this is a dead variable (not really used)
    final EcenProviderPlanOfCare__c poc;
    public boolean isError { get; private set; }
    public string errorMsg { get; private set; }

    public ProviderPortalPOCSync(string patientcaseid, string clientfacilityid, EcenProviderPlanOfCare__c poc) {
        this.poc = poc;
        this.patientcaseid = patientcaseid;
        this.clientfacilityid = clientfacilityid;
    }

    public ProviderPortalPOCSync(string patientcaseid) {
        // poc
        this.patientcaseid = patientcaseid;
    }

    public EcenProviderPlanOfCare__c Salesforce_to_Portal(pc_poc__c pc_poc) {
        EcenProviderPlanOfCare__c poc;
        try {
            poc = new EcenProviderPlanOfCare__c(id=[select id from EcenProviderPlanOfCare__c where patient_case__c = :pc_poc.patient_case__c order by createdDate desc limit 1].id);
        } catch (queryexception e) {
            poc = new EcenProviderPlanOfCare__c(patient_case__c = patientcaseid);
        }

        poc.complicationServiceTrip__c = pc_poc.Complication_Service_Trip__c;
        if (pc_poc.Complication_Service_Trip__c == 'Inpatient') {
            poc.complicationServiceType__c = pc_poc.Inpatient_Complication_Type__c;
        } else {
            poc.complicationServiceType__c = pc_poc.Outpatient_Complication_Type__c;
        }

        poc.status__c = pc_poc.portal_status__c;
        poc.verifiedOutcomeOfVisit__c = pc_poc.Clinically_Verified_Outcome_of_Visit__c;
        poc.allowedModeOfTransportation__c = pc_poc.Clinically_Allowable_Mode_of_Transportat__c;

        poc.hipApproach__c = pc_poc.Hip_Approach__c;
        poc.nicotineQuitDateDate__c = pc_poc.Nicotine_Quit_Date__c;
        poc.ReceivedDeterminationAtHDPDate__c = pc_poc.Received_Determination_at_HDP__c;
        poc.firstAppointmentTime__c = pc_poc.first_Appointment_Time__c;
        poc.lastAppointmentTime__c = pc_poc.Last_Appointment_Time__c;
        poc.surgicalProcedureTime__c = pc_poc.Surgical_Procedure_Time__c;
        poc.physicianFollowUpTime__c = pc_poc.Physician_Appointment_Time__c;
        poc.originalSurgeryDate__c = formatStringDate(pc_poc.original_surgery_date__c);

        poc.pocAcceptedDate__c = pc_poc.Plan_of_Care_Accepted__c;
        poc.pocSubmittedDate__c = pc_poc.Plan_of_Care_Submitted__c;
        poc.pocAccepted__c           = formatStringDate(poc.pocAcceptedDate__c);
        poc.pocSubmitted__c          = formatStringDate(poc.pocSubmittedDate__c);
        poc.nicotineQuitDate__c      = formatStringDate(pc_poc.Nicotine_Quit_Date__c);
        poc.hospitalAdmissionDate__c = formatStringDate(pc_poc.Hospital_Admission_Date__c);
        poc.hospitalDischargeDate__c = formatStringDate(pc_poc.Hospital_Discharge_Date__c);
        poc.firstAppointmentDate__c  = formatStringDate(pc_poc.First_Appointment_Date__c);
        poc.lastAppointmentDate__c   = formatStringDate(pc_poc.Last_Appointment_Date__c);
        poc.patientArrivalDate__c    = formatStringDate(pc_poc.Patient_Arrival_Date__c);
        poc.surgicalProcedureDate__c = formatStringDate(pc_poc.Surgical_Procedure_Date__c);
        poc.surgicalProcedureDate__c = formatStringDate(pc_poc.Surgical_Procedure_Date__c);
        poc.patientReturnHomeDate__c = formatStringDate(pc_poc.Return_Home_Travel_Date__c);
        poc.physicianFollowUpDate__c = formatStringDate(pc_poc.Physician_Follow_up_Appointment_Date__c);
        poc.patientReturnHomeDate__c = formatStringDate(pc_poc.Return_Home_Travel_Date__c);
        poc.physicianFollowUpDate__c = formatStringDate(pc_poc.Physician_Follow_up_Appointment_Date__c);
        poc.surgicalProcedureDate__c = formatStringDate(pc_poc.Surgical_Procedure_Date__c);
        poc.medicalDirectorInvolvement__c = formatStringDate(pc_poc.Medical_Director_Involvement__c);

        poc.patientPreferredModeOfTravel__c = pc_poc.Patient_Preferred_Mode_of_Transportation__c;
        poc.proposedAddOnServices__c = pc_poc.proposed_Add_On_Services__c;
        poc.allowedModeOfTransportation__c = poc.allowedModeOfTransportation__c;

        poc.proposedServiceReason__c = pc_poc.Proposed_Service_Trip_Reason__c;
        poc.proposedServiceType__c = pc_poc.Proposed_Service_Type__c;
        poc.proposedServiceTrip__c = pc_poc.Proposed_Service_Trip__c;
        poc.scoliosisDiagnosed__c = pc_poc.Diagnosis_of_scoliosis__c;
        poc.servicesRequestedOutsideofContracted__c = pc_poc.Services_Requested_Outside_of_Contracted__c;
        poc.surgeon__c = pc_poc.Surgeon_Name__c;

        poc.travelNotes__c = pc_poc.Travel_Notes__c;
        poc.nicotineUser__c = pc_poc.current_nicotine_user__c;
        poc.planOfCareType__c = pc_poc.RecordType.Name;
        poc.authorizationNumber__c = pc_poc.displayAuthNumber__c;
        poc.requestedBillableAdditionalServices__c = pc_poc.Requested_Billable_Additional_Services__c;
        poc.conservativeTreatment__c = pc_poc.conservative_Treatment__c;
        poc.pocCancelled__c = formatStringDate(pc_poc.Cancelled_Date__c);
        poc.surgeryHistoryStatus__c = pc_poc.Surgery_History_Status__c;

        if (!string.isBlank(pc_poc.Additional_Trip__c)) {
            poc.additionalTrip__c = integer.valueof(pc_poc.Additional_Trip__c);
        }

        try {
            upsert poc;
        } catch (dmlException e) {
            system.debug(e.getDmlMessage(0));
            isError = true;
            errorMsg = e.getDmlMessage(0);
            return null;
        }

        return poc;
    }

    public void Portal_to_Salesforce() {
        id pc_poc_id;
        try {
            pc_poc_id = [select id from pc_POC__c where patient_case__c = :patientcaseid limit 1].id;
        } catch (exception e) {
            //Ok to fail
        }

        pc_POC__c pc_poc = new pc_POC__c();
        pc_poc.id = pc_poc_id;

        if (pc_poc_id == null) {
            pc_poc.patient_case__c = patientcaseid;
        }

        try {
            pc_poc.recordtypeid = [select id from recordtype where name = :poc.planOfCareType__c and SobjectType='pc_poc__c'].id;
        } catch (exception e) {
            system.debug(e.getMessage());
        }

        pc_poc.cancelledReason__c = poc.cancelledReason__c;
        pc_poc.proposed_Service_Trip_Note__c = poc.proposedServiceTripNote__c;
        pc_poc.Clinically_Verified_Outcome_of_Visit__c = poc.verifiedOutcomeOfVisit__c;
        pc_poc.Clinically_Allowable_Mode_of_Transportat__c = poc.allowedModeOfTransportation__c;
        pc_poc.Complication_Service_Trip__c = poc.complicationServiceTrip__c;
        pc_poc.Inpatient_Complication_Type__c = poc.complicationServiceType__c;
        pc_poc.Outpatient_Complication_Type__c = poc.complicationServiceType__c;
        pc_poc.Hip_Approach__c = poc.hipApproach__c;
        pc_poc.Received_Determination_at_HDP__c = poc.ReceivedDeterminationAtHDPDate__c;
        pc_poc.first_Appointment_Time__c = poc.firstAppointmentTime__c;
        pc_poc.Last_Appointment_Time__c = poc.lastAppointmentTime__c;
        pc_poc.Surgical_Procedure_Time__c = poc.surgicalProcedureTime__c;
        pc_poc.Physician_Appointment_Time__c = poc.physicianFollowUpTime__c;
        pc_poc.Plan_of_Care_Submitted__c = poc.pocsubmittedDate__c;
        pc_poc.Hospital_Admission_Date__c = formatDate(poc.hospitalAdmissionDate__c);
        pc_poc.Hospital_Discharge_Date__c = formatDate(poc.hospitalDischargeDate__c);
        pc_poc.First_Appointment_Date__c = formatDate(poc.firstAppointmentDate__c);
        pc_poc.Last_Appointment_Date__c = formatDate(poc.lastAppointmentDate__c);
        pc_poc.Patient_Arrival_Date__c = formatDate(poc.patientArrivalDate__c);
        pc_poc.Surgical_Procedure_Date__c = formatDate(poc.surgicalProcedureDate__c);
        pc_poc.Surgical_Procedure_Date__c = formatDate(poc.surgicalProcedureDate__c);
        pc_poc.Return_Home_Travel_Date__c = formatDate(poc.patientReturnHomeDate__c);
        pc_poc.Physician_Follow_up_Appointment_Date__c = formatDate(poc.physicianFollowUpDate__c);
        pc_poc.Return_Home_Travel_Date__c = formatDate(poc.patientReturnHomeDate__c);
        pc_poc.Physician_Follow_up_Appointment_Date__c = formatDate(poc.physicianFollowUpDate__c);
        pc_poc.Surgical_Procedure_Date__c = formatDate(poc.surgicalProcedureDate__c);

        pc_poc.Nicotine_Quit_Date__c = formatDate(poc.nicotineQuitDate__c);
        pc_poc.current_nicotine_user__c= poc.nicotineUser__c;

        pc_poc.Patient_Preferred_Mode_of_Transportation__c = poc.patientPreferredModeOfTravel__c;
        pc_poc.Proposed_Add_on_Services__c = poc.proposedAddOnServices__c;

        if (pc_poc.Proposed_Add_on_Services__c != null) {
            pc_poc.Proposed_Add_on_Services__c = poc.proposedAddOnServices__c.replaceAll(',', ';');
        }

        if (pc_poc.Clinically_Allowable_Mode_of_Transportat__c != null) {
            pc_poc.Clinically_Allowable_Mode_of_Transportat__c = pc_poc.Clinically_Allowable_Mode_of_Transportat__c.replaceAll(',',';');
        }

        pc_poc.Proposed_Service_Trip_Reason__c = poc.proposedServiceReason__c;
        pc_poc.Proposed_Service_Trip__c = poc.proposedServiceTrip__c;
        pc_poc.Proposed_Service_Type__c = poc.proposedServiceType__c;
        pc_poc.Diagnosis_of_scoliosis__c = poc.scoliosisDiagnosed__c;
        pc_poc.Services_Requested_Outside_of_Contracted__c = poc.servicesRequestedOutsideofContracted__c;
        pc_poc.Spine_Surgeon_Name__c = pc_poc.Surgeon_Name__c = poc.surgeon__c;
        pc_poc.Travel_Notes__c = poc.travelNotes__c;
        pc_poc.portal_status__c = poc.status__c;
        pc_poc.Requested_Billable_Additional_Services__c = poc.requestedBillableAdditionalServices__c;
        pc_poc.conservative_Treatment__c = poc.conservativeTreatment__c;
        pc_poc.Proposed_Service_Trip_Note__c = poc.Proposed_Service_Trip_Note__c;
        pc_poc.Surgery_History_Status__c = poc.surgeryHistoryStatus__c;

        if (poc.AdditionalTrip__c != null) {
            pc_poc.Additional_Trip__c = string.valueof(poc.additionalTrip__c);
        }

        if (poc.status__c == 'Pending Cancellation') {
            pc_poc.status__c = 'Pending Cancellation';
        }

        try {

            upsert pc_poc;
            upsert poc;

            patient_case__c pc = new patient_case__c(id = patientcaseid);
            pc.Has_New_Provider_Portal_POC__c = true;

            update pc;

        } catch(dmlException e) {
            system.debug(e.getDmlMessage(0));
            isError = true;
            errorMsg = e.getDmlMessage(0);

        }
    }

    time formatTime(string t) {
        time tm;
        integer hour,minute;
        try {
            string[] foo = t.split(' ');
            string[] bar = foo[0].split(':');
            hour = integer.valueof(bar[0]);
            minute = integer.valueof(bar[1]);

            if(foo[1] == 'PM') {
                hour += 12;
            }

            tm = Time.newInstance(hour, minute, 0, 0);

        } catch(exception e) {}

        return tm;
    }

    string formatStringDate(date d) {
        string result = '';
        system.debug('d: ' + d);

        if (d == null) {
            return result;
        }

        try {
            result = d.Month() + '/' + d.Day() + '/' + d.Year();
            system.debug('result: ' + result);
        } catch (exception e) {
            system.debug('Error formatting date for the portal: ' + e.getMessage());
        }

        return result;
    }

    date formatDate(string d) {
        date dt;
        system.debug('dt: ' + dt);

        try {
            string[] arr = d.split('/');
            dt = date.valueof(arr[2] + '-' + arr[0] + '-' + arr[1]);
        } catch(exception e) {}

        return dt;
    }

}
