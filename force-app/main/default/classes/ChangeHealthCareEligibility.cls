public with sharing class ChangeHealthCareEligibility{
    
    x270_271 x270 = new x270_271();
    
    public ChangeHealthCareEligibility(){
        x270.vendorName = 'Corporate Benefit Srvcs of Amer';
        x270.vendorTaxId= '770545013';
        x270.userName = 'B2BUP41593';
        x270.password = 'Bb4159392#';
    
    }
    
    public class response{
        
        public boolean error;
        public string message;
    }
     
    public static response sendRequest(patient_case__c pc){
        ChangeHealthCareEligibility.response response = new ChangeHealthCareEligibility.response();
        ChangeHealthCareEligibility cce = new ChangeHealthCareEligibility();
        
        cce.x270.fname=pc.patient_first_name__c;
        cce.x270.lname=pc.patient_last_name__c;
        cce.x270.dob=string.valueof(pc.patient_dob__c);
        cce.x270.ins_id=pc.bid__c;
        cce.x270.payorId='75261';
        
        string request =cce.x270.build270();
        http h = new http();
        HttpRequest req = new HttpRequest ();
        req.setEndPoint('https://b2b.Capario.net/b2b/X12Transaction');
        req.setBody(request);
        req.setmethod('POST');
        req.setClientCertificateName('comodorsacertificationauthority');       
        req.setHeader('Content-Type', 'text/xml');
        HttpResponse res;
        
        if(test.isrunningtest()){
            res = new HttpResponse();
            res.setstatuscode(200);
            res.setbody(oncologyTestData.cohx271());
             
        }else{
            res = h.send(req);
            
        }
        
        response.error=false;
        
        if(res.getstatuscode()==200){
            string x271 = res.getbody(); 
            
            string result = cce.x270.read271(x271, pc);
            system.debug('result '+result);
            
            if(result!=null){
                response.error=true;
                response.message=result;
                return response;         
            }
            
            
        }else{
            response.error=true;
            response.message=res.getbody(); 
            return response;
        }
         
        return response;
            
    } 
        
    public static response sendRequest(oncology__c o){
        ChangeHealthCareEligibility cce = new ChangeHealthCareEligibility();
        cce.x270.fname=o.patient_first_name__c;
        cce.x270.lname=o.patient_last_name__c;
        cce.x270.dob=o.patient_dob__c;
        cce.x270.ins_id=o.Patient_Medical_Plan_Number__c;
        system.debug(o.carrier__r.name);
        switch on o.carrier__r.name {
            when 'Meritain'{
                cce.x270.payorId='41124';
            }
            
            when 'Aetna'{
                cce.x270.payorId='60054';
            }
            
            when 'Premera Blue Cross'{
                cce.x270.payorId='SB930';
            }
            
            when 'Anthem (SISC)'{
                cce.x270.payorId='94036';
            }
            
            when 'Blue Shield (SISC)'{
                cce.x270.payorId='94036';
            }
            
            when 'WebTPA'{
                cce.x270.payorId='75261';
            }
        }
        system.debug(cce.x270.payorId);
        ChangeHealthCareEligibility.response response = new ChangeHealthCareEligibility.response();
        string request =cce.x270.build270();
        
        http h = new http();
        HttpRequest req = new HttpRequest ();
        req.setEndPoint('https://b2b.Capario.net/b2b/X12Transaction');
        req.setBody(request);
        req.setmethod('POST');
        req.setClientCertificateName('comodorsacertificationauthority');       
        req.setHeader('Content-Type', 'text/xml');
        HttpResponse res;
        
        if(test.isrunningtest()){
            res = new HttpResponse();
            res.setstatuscode(200);
            res.setbody(oncologyTestData.cohx271());
             
        }else{
            res = h.send(req);
            
        }
        
        if(res.getstatuscode()==200){
            string x271 = res.getbody(); 
            system.debug(x271);    
            string result = cce.x270.read271(x271, o);
            
            if(result!=null){
                response.error=true;
                response.message=result;
                return response;         
            }
            
        }else{
            response.error=true;
            response.message=res.getbody(); 
            return response;
        }
        
        response.error=false;
        return response;
     
        
    }
}