public with sharing class directBillingFuture{

    @future()
    public static void deleteAttachment(id attachID){
        delete (new attachment(id=attachID));
    }
    
    @future(callout=true)
    public static void createPDFinvoice(set<id> invoiceIds){
        directBillingCreateAttachments dbca = new directBillingCreateAttachments();
        dbca.createInvoice(invoiceIds);
    }
    
    @future(callout=true)
    public static void createPDFpastdueletter(set<id> invoiceIds){
        directBillingCreateAttachments dbca = new directBillingCreateAttachments();
        dbca.createPastDueLetter(invoiceIds);
    }

    @future(callout=true)
    public static void createPDFTerminationletter(set<id> invoiceIds){
        directBillingCreateAttachments dbca = new directBillingCreateAttachments();
        dbca.createTerminationLetter(invoiceIds);
    }    
    
    @future()
    public static void updateOutstandingBalance(set<id> leaveIds){
        Direct_Billing_Leave__c[] leaves = [select total_payments__c, total_charges__c, outstanding_balance__c from Direct_Billing_Leave__c where id in :leaveIds]; 
        for(Direct_Billing_Leave__c dbl : leaves){
            system.debug('dbl.total_charges__c: '+dbl.total_charges__c);
            system.debug('dbl.total_payments__c: '+dbl.total_payments__c);
            dbl.outstanding_balance__c=dbl.total_charges__c-dbl.total_payments__c;
        }
    
        update leaves;

    } 
}