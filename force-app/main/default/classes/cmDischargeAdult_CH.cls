public with sharing class cmDischargeAdult_CH{
    
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);

        //added new variables 3-1-21 start----------
        string openP;
        string closedP;
        
        openP='(';
        closedP=')';
        //added new variables 3-1-21 end----------
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=cm.patient__r.Patient_First_Name__c+' '+cm.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Address__c: +'<br/>';
        letterText+='<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.City__c+', ' : ''+'&nbsp';
        letterText+=hasPatientAddress ? cm.patient__r.State__c+' ' : ''+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Zip__c : ''+'<br/>';
        letterText+='<br/>';

        letterText+='</div>';
        letterText+='<p>';
        letterText+='Dear&nbsp;'+cm.patient__r.Patient_First_Name__c+':';//+'&nbsp;'+cm.patient__r.Patient_Last_Name__c+',';
        letterText+='</p>';
        letterText+='<p>';  
        letterText+='Thank you for participating in the Case Management program! {Our records indicate you have met your stated program goal. <b>Or</b> Per our conversation,}';
        letterText+=' I am glad you are managing well and have no further needs at this time.<br/>';
        letterText+='</p>';
        
        letterText+='<p>'; 
        letterText+='In order to continually improve the quality of our service, we seek feedback from those who have participated. We would appreciate your responses on';
        letterText+=' the enclosed questionnaire and have provided a return envelope for your convenience.<br/>';
        letterText+='</p>'; 
        
        letterText+='<p>';  
        letterText+='Please know that we value your opinion and will use your comments to improve our program. We ask that you do not include any personally identifiable';
        letterText+=' information on the form. Thanks again for participating.<br/>';
        letterText+='</p>';        
        
        letterText+='<p>';  
        letterText+='I enjoyed working with you and wish you the best!  If you have any questions or concerns in the future, please feel free to call me at 1-877-891-2690,';
        letterText+=' Monday-Friday from 8:30AM – 5:00PM Eastern Time.  If I am not able to take your call, please leave a message on my confidential voicemail';
        letterText+=' and I will return your call as soon as possible.<br/>';
        letterText+='</p>';         
        
        letterText+='<p>';
        letterText+='Sincerely,<br/><br/>';
        letterText+='Care Management Department<br/>';
        letterText+='Contigo Health<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/><br/>';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='Enclosures';
        letterText+='</p>';
        
        //2-28-22 added to push enclosure to the next page -----------------------------------start---------------------------------------
        letterText+='<p>';
        letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+=' *</br></br>';
        letterText+='</p>';

        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';
        
        letterText+='<p>';
        //letterText+=' *</br></br>';
        letterText+='</p>';                                
        //2-28-22 added to push enclosure to the next page -----------------------------------end---------------------------------------
         
        letterText+='<p>';
        letterText+='<div style="text-align:center;"font-size:320px;padding-top: 905px;">';
        letterText+='<b>Contigo Health™ Case Management Program<br>';
        letterText+='Satisfaction Survey</b>';
        letterText+='</div>';

        letterText+='</p>';
        letterText+='<p>';
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left;">';
        letterText+='Your Case Manager\'s Name_______________________________________<br>';  
        letterText+='We are interested in learning about your experience with the Contigo Health™ Case Management program. <br/> ';
        letterText+='Your completion of this anonymous survey helps us better understand how we can continually improve the<br/>';
        letterText+='program. To complete the survey, please circle your ratings for each numbered item.<br/><br/>';
        letterText+='<b>DO NOT INCLUDE ANY PERSONALLY IDENTIFIABLE INFORMATION ON THIS FORM. </b><br/>';
        letterText+='Return the completed form in the envelope provided to:<br/><br/>';
        letterText+='Contigo Health, LLC<br/>';
        letterText+='PO Box 2584 <br/>';
        letterText+='Hudson, OH  44236';
        letterText+='</div>';//2-28-22 moved from below
        letterText+='</p>';

        //2-28-22 new table for survey -----------------------------------start------------------------
        //header row
        letterText+='<table style="width:100%;border: 1px solid black;">';
        
        letterText+='<tr style="width:100%;border: 1px solid black;">';
        letterText+='<th style="border: 1px solid black;"></th>';
        letterText+='<th style="border: 1px solid black;"></th>';
        letterText+='<th style="border: 1px solid black;">All of the time</th>';
        letterText+='<th style="border: 1px solid black;">Most of the time</th>';
        letterText+='<th style="border: 1px solid black;">Some of the time</th>';
        letterText+='<th style="border: 1px solid black;">A Little of the time</th>';
        letterText+='<th style="border: 1px solid black;">None of the time</th>';
        letterText+='<th style="border: 1px solid black;">Not Sure/Not Applicable</th>'; 
        letterText+='</tr>';
        
        //row1
        letterText+='<tr style="border: 1px solid black;">';
        letterText+='<td style="border: 1px solid black;">1.</td>';
        letterText+='<td style="border: 1px solid black;">How well did Case Manager treat you with respect and dignity?</td>';
        
        letterText+='<td style="border: 1px solid black;text-align:center;">5</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">4</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">3</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">2</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">1</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">N/A</td>';        
        letterText+='</tr>';        

        //row2
        letterText+='<tr style="border: 1px solid black;">';
        letterText+='<td style="border: 1px solid black;">2.</td>';
        letterText+='<td style="border: 1px solid black;">Was your Case Manager a knowledgeable resource?</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">5</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">4</td>';        
        letterText+='<td style="border: 1px solid black;text-align:center;">3</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">2</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">1</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">N/A</td>';        
        letterText+='</tr>';     
        
        //row3
        letterText+='<tr style="border: 1px solid black;">';
        letterText+='<td style="border: 1px solid black;">3.</td>';
        letterText+='<td style="border: 1px solid black;">If you left a message for your Case Manager, was your call returned in a timely manner?</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">5</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">4</td>';        
        letterText+='<td style="border: 1px solid black;text-align:center;">3</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">2</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">1</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">N/A</td>';        
        letterText+='</tr>';         
        
        //row4
        letterText+='<tr style="border: 1px solid black;">';
        letterText+='<td style="border: 1px solid black;">4.</td>';
        letterText+='<td style="border: 1px solid black;">How well did your Case Manager answer your questions to your satisfaction?</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">5</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">4</td>';        
        letterText+='<td style="border: 1px solid black;text-align:center;">3</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">2</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">1</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">N/A</td>';        
        letterText+='</tr>';         

        //row5
        letterText+='<tr style="border: 1px solid black;">';
        letterText+='<td style="border: 1px solid black;">5.</td>';
        letterText+='<td style="border: 1px solid black;">How well did your Case Manager ask questions to better understand your needs?</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">5</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">4</td>';        
        letterText+='<td style="border: 1px solid black;text-align:center;">3</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">2</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">1</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">N/A</td>';        
        letterText+='</tr>';

        //row6
        letterText+='<tr style="border: 1px solid black;">';
        letterText+='<td style="border: 1px solid black;">6.</td>';
        letterText+='<td style="border: 1px solid black;">Overall, do you feel more confident in navigating your healthcare after working with your case manager?</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">5</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">4</td>';        
        letterText+='<td style="border: 1px solid black;text-align:center;">3</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">2</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">1</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">N/A</td>';        
        letterText+='</tr>';

        //row7
        letterText+='<tr style="border: 1px solid black;">';
        letterText+='<td style="border: 1px solid black;">7.</td>';
        letterText+='<td style="border: 1px solid black;">How likely would you recommend the Contigo Health Case Management program to friend, family, or co-worker?</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">Strongly Agree</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">Agree</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">Somewhat Agree</td>';
        //letterText+='<td style="border: 1px solid black;text-align:center;">Neutral</td>';//3-1-22 distorts the table check if ok to remove
        letterText+='<td style="border: 1px solid black;text-align:center;">Disagree</td>';
        letterText+='<td style="border: 1px solid black;text-align:center;">Somewhat Disagree</td>'; 
        letterText+='<td style="border: 1px solid black;text-align:center;">Strongly Disagree</td>'; 
        letterText+='</tr>';   
        letterText+='</table>';
        
        letterText+='<p>';
        //letterText+='Please share any other comments below <b><u>do not share personally identifiable information:</u></b></br>';
        letterText+='Please share any other comments below <b><u>'+openP+'do not share personally identifiable information:'+closedP+'</u></b>';
        letterText+='</p>';
        

        letterText+='<HR></HR></br>';
        letterText+='<HR></HR></br>';
        letterText+='<HR></HR></br>';
        letterText+='<HR></HR></br>';


        //2-28-22 new table for survey -----------------------------------end--------------------------
         
 
        return letterText;
 
 
    }
    
}