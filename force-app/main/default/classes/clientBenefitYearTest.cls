@isTest
private class clientBenefitYearTest {

    static testMethod void UnitTest(){
        
        clientBenefitYear.BenefitYearSelectOption(null);
        client__c client = new client__c(name='Unit.Test', underwriter__c = '008');
        insert client;
        
        Client_Benefit_Year__c cby = new Client_Benefit_Year__c(client__c = client.id);
        cby.start__c = date.valueof('2016-01-01');
        cby.end__c = date.valueof('2016-12-31');
        insert cby;
        
        clientBenefitYear.BenefitYearSelectOption(client.id);
    }
    
}