public with sharing class bariaticNoteController {
    
    public string coeFileDirectory {get; private set;}
    public boolean isError {get; private set;}
    public Bariatric_Note__c  note {get; set;}
    public Bariatric__c obj {get; set;}
    public ftpAttachment__c attachment {get; set;}
    public string fileserverurl {get; private set;}
    public string baseDirectory {get; private set;}
    public string theSID {get; private set;}
    public string theUserId {get; private set;}
    
    public bariaticNoteController(){
        id bariatric_note_id = ApexPages.CurrentPage().getParameters().get('id');
        id bariatric_id = ApexPages.CurrentPage().getParameters().get('bid');
        if(bariatric_id==null && bariatric_note_id == null){
            return;
        }
        
        set_obj(bariatric_id);
        
        if(bariatric_note_id==null){
            note = new Bariatric_Note__c(bariatric__c=bariatric_id);
            if(ApexPages.CurrentPage().getParameters().get('isnicotine')=='true'){
                note.subject__c='Nicotine Results';
            }
        }else{
            set_note(bariatric_note_id);
        }
        fileserverurl=fileServer__c.getInstance().fileserverurl__c;
        baseDirectory=fileServer__c.getInstance().baseDirectory__c;
        theSid = userinfo.getSessionid();
        
    } 
    
    void set_obj(string bariatric_id){
        obj = [select employee_first_name__c,
                      employee_last_name__c, 
            employee_DOB__c,
        Patient_DOB__c,
        Patient_first_name__c,
        Patient_last_name__c,Name from bariatric__c where id = :bariatric_id];
        coeFileDirectory = bariatricUtils.coeDirectory(obj);
    }
    void set_note(id bariatric_note_id){
            note = new Bariatric_Note__c();
            
            note = [select Activity_Type__c,
                           attachmentID__c,
                           Attachment__c,
                           Attachment__r.subDirectory__c,
                           Attachment__r.fileName__c,
                           Non_Clinical_Contact__c,
                           Clinical_Units__c,
                           Action__c,
                           Form_Attached__c,
                           Follow_Up_Date__c,
                           ftp_File_Name__c,
                           Issue_Resolved__c,
                           note__c,
                           Subject__c,
                           Subtype__c,
                           CreatedDate,
                           CreatedBy.Name from Bariatric_Note__c where id = :bariatric_note_id];
            
            if(note.attachment__c!=null){
                 attachment =[select subDirectory__c,fileName__c,name,createdBy.name,createdDate from ftpAttachment__c where id =:note.attachment__c];
                
            }               
                           
            
   }     
   
   public void note_addAttachment(){
       string file_id = ApexPages.CurrentPage().getParameters().get('file_id');
       ApexPages.CurrentPage().getParameters().put('file_id', null);
       note.Attachment__c= file_id;
       update note;
   }
    
   public void saveNote(){
        
        string fileName = ApexPages.CurrentPage().getParameters().get('fileName');
        ApexPages.CurrentPage().getParameters().put('fileName', null);
        isError =false;
        if(fileName==''){fileName=null;}
        
        if(note.Action__c=='Medication List' && fileName==null){
            isError =true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please attach the medication list'));
            return;
        }
        
        if(note.Subtype__c == 'Nicotine Results Attached' && fileName==null){
            isError =true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please attach the nicotine results'));
            return;
        }
        
        try{
            if(note.Activity_Type__c=='Clinical'){
                
                note.Non_Clinical_Contact__c=null;
            }else if(note.Activity_Type__c=='Non-Clinical'){
                note.Clinical_Units__c=null;
            
            }
            
            upsert note;
        
        }catch(dmlException e){
            system.debug(e.getMessage()+' '+e.getLineNumber());
            isError = true;
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                
            }  
            
        }  
    }    
    
    public void deleteFtpAttachment(){
        attachment = new ftpAttachment__c(id=note.attachment__c);
        delete attachment;
        attachment =null;
        
    }

}