public with sharing class claimsAssignment{
    
    public map<string, integer> processorCount;
    map<id, sObject[]> criteriaMap = new  map<id, sObject[]>();
    string ruleType = '';
    
    public claimsAssignment(){}
    
    public claimsAssignment(string Department, string recordTypeName){
        loadprocessorCount(Department, recordTypeName);
        
    }
    
    public class innerStruct{
    
        string determination {get; set;}
        string processor {get; set;}
        
        innerStruct(string determination, string processor){
            this.determination = determination;
            this.processor  = processor;
        }
    }
    
    public Operations_Work_Task__c[] setEwtList(Operations_Work_Task__c[] ewtList){
        
        innerStruct foo;
        
        if(processorCount==null){
            loadEwtProcessorCount('EWT');
        }
        
        for(Operations_Work_Task__c c : ewtList){
             foo = assign(c);
             if(foo!=null){
                 c.assigned__c = foo.processor;
                 c.Determination_Rule__c = foo.determination;
             }
        }
        
        return ewtList;
    }
    
    public customer_care_inquiry__c[] setList(customer_care_inquiry__c[] cciList, string Department, string recordTypeName){
        
        innerStruct foo;
        
        if(processorCount==null){
            loadprocessorCount(Department, recordTypeName);
        }
        
        for(customer_care_inquiry__c c : cciList){
             foo = assign(c);
             if(foo!=null){
                 c.assigned__c = foo.processor;
                 c.Determination_Rule__c = foo.determination;
             }
        }
        
        
        return cciList;
    }
    
    public innerStruct assign(sObject cc){
        
        innerStruct assignment;
        sObject[] cacList; 
        sObject rule;
        
        for(id i : criteriaMap.keySet()){
            
            cacList = criteriaMap.get(i);
            
            if(cacList.size()>1){
                
                boolean pass = true;
                
                for(sObject c : caclist){
                    string fld = string.valueof(c.get('field__c'));
                    
                    if(string.valueof(cc.get(string.valueof(c.get('field__c'))))== c.get('condition__c')){
                        rule = c;
                    }else{
                        pass=false;
                        break;
                    }
                    
                }
            
                if(pass){
                    
                    assignment = new innerStruct(string.valueof(rule.getSObject('Claims_Assignment_Rule__r').get('Name')), gettheProcessor(getProcessorList(string.valueof(rule.getSObject('Claims_Assignment_Rule__r').get('Processor_List__c')))));
                    return assignment;
                }
            
            }else if(cacList.size()==1){
                
                if(string.valueof(cc.get(string.valueof(cacList[0].get('field__c'))))== cacList[0].get('condition__c')){
                    
                    rule  = cacList[0];
                    system.debug('rule: '+rule);
                    try{        
                        assignment = new innerStruct(string.valueof(rule.getSObject('Claims_Assignment_Rule__r').get('Name')), gettheProcessor(getProcessorList(string.valueof(rule.getSObject('Claims_Assignment_Rule__r').get('Processor_List__c')))));
                    }catch(exception e){
                        system.debug(e.getLineNumber());
                        system.debug(e.getMessage());
                    }
                    
                    return assignment;
                    
                }
                
            }
        
        }
        
        return assignment;
    }
    
    string [] getProcessorList(string foo){
    
        string[] finalList = new string[]{};
        
        if(foo.contains(',')){
            
            for(string s : foo.split(',')){
                finalList.add(s.trim());
                
            }
            finalList = randomize(finalList);
        }else{
            finalList.add(foo);
            
        }
        system.debug('finalList '+finalList);
        return finalList;
        
    }

    void loadEwtProcessorCount(string RecordTypeName){
        if(processorCount==null){
        
            processorCount = new map<string, integer>();
            integer count;
            
            for(Operations_Work_Task__c c : [select Assigned__c from Operations_Work_Task__c where Work_Task_Status__c = 'Open' and assigned__c != null]){
                count = processorCount.get(c.Assigned__c);
                    
                if(count==null){
                    count=0;
                }
                
                count++;
                processorCount.put(c.Assigned__c, count);
                
            }
            
            setCacList(RecordTypeName);
        }            
    }

    void loadprocessorCount(string Department, string RecordTypeName){
    
        if(processorCount==null){
        
            processorCount = new map<string, integer>();
            integer count;
            
            for(Customer_Care_Inquiry__c c : [select Assigned__c from Customer_Care_Inquiry__c where Inquiry_Status__c = 'Open' and Work_Department__c = :Department and assigned__c != null]){
                count = processorCount.get(c.Assigned__c);
                    
                if(count==null){
                    count=0;
                }
                
                count++;
                processorCount.put(c.Assigned__c, count);
                
            }
            
            setCacList(RecordTypeName);
            
        }
    
    }
    
    void setCacList(string RecordTypeName){
        Claims_Assignment_Criteria__c[] cacList;
        for(Claims_Assignment_Criteria__c c : [select Claims_Assignment_Rule__r.Priority_Number__c, 
                                                          Claims_Assignment_Rule__r.name,
                                                          Claims_Assignment_Rule__c,
                                                          Claims_Assignment_Rule__r.Processor_List__c,
                                                          Claims_Assignment_Rule__r.recordtype.name,
                                                          Condition__c, 
                                                          field__c, 
                                                          name from Claims_Assignment_Criteria__c where Claims_Assignment_Rule__r.recordtype.name = :RecordTypeName and Claims_Assignment_Rule__r.Active__c=true order by Claims_Assignment_Rule__r.Priority_Number__c asc]){
                    
                cacList = criteriaMap.get(c.Claims_Assignment_Rule__c);
                    
                if(cacList == null){
                    cacList = new Claims_Assignment_Criteria__c[]{};
                }
                
            cacList.add(c);
            criteriaMap.put(c.Claims_Assignment_Rule__c, cacList);
            system.debug(criteriaMap);        
        }
    }
    
    string gettheProcessor(string[] theList){
        
        if(theList.size()==1){
            return theList[0]; 
        }
        
        integer least = 100000;
        integer count;
        string processor;
        
        for(string s : theList){
            count = processorCount.get(s);
            if(count==null){count=0;}
            if(count <  least){
                least = processorCount.get(s);
                processor = s;
            }
        }    
        
        updateProcessorCount(processor);    
        return processor;
        
    }
    
    void updateProcessorCount(string assigned){
    
        integer count =  processorCount.get(assigned);
        if(count==null){count=0;}
        count++;
        
        processorCount.put(assigned, count);
        
    }
   
    string[] randomize(string[] foo){
      
      integer i = foo.size()-1;
      string[] finalList = new string[]{};  
      set<Integer> selected = new set<Integer>();  
    
      while(selected.size()<=i){
          integer rand = Math.round(Math.random()*i);
          selected.add(rand);
      }
      
      for(integer x : selected){
            finalList.add(foo[x]);
      }
      
      return finalList;
    
    }
}