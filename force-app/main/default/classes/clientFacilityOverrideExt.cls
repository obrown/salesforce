public without sharing class clientFacilityOverrideExt {
    
    private ApexPages.StandardController c;
    public Client_Facility__c cf {get; set;}
    public map<string, Recommended_Facility_Rule__c[]> rfrMap {get; private set;}
    public selectOption[] planSelectOptions {get; private set;}
    
    public integer rfMapSize { get { return rfrMap.size(); } }
    
    public string[] authContactList {get; private set;}
    public string[] referralContactList {get; private set;}
    
    public ecen_hotel__c hotel {get; set;}
    
    public string clientLogo {get; private set;}
    public set<string> prioritySet {get; private set;}
    
    WFM_Self_Admin__c relationshipIds = WFM_Self_Admin__c.getInstance();
    Pattern emailRegEx = Pattern.compile('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,6}$');
    
    transient HealthPac_Plan_Code__c[] planCodes;
    public HealthPac_Plan_Code__c planCode {get; set;}
    
    public map<string, string> logicTipMap {get; private set;}
    
    public Client_Facility_Physician__c[] physicians {get; private set;}
    public Client_Facility_Physician__c provider {get; private set;}
    
    public clientFacilityOverrideExt(ApexPages.StandardController controller){
        c = controller;
        this.cf = (Client_Facility__c)controller.getRecord();
        clientLogo= '/logos/'+cf.client__r.logoDirectory__c;
        referralContactList = setContactList(cf.Referral_Email_Contact__c);
        authContactList = setContactList(cf.Authorization_Email_Contact__c);
        loadrfrMap();
        loadPhysicians();
    }
    
    string[] setContactList(string theFieldValue){
        
        string[] foo = new string[]{};
        
        if(theFieldValue==null||theFieldValue==''){
            return foo;    
        }
        
        for(string s : theFieldValue.split(',')){
            s = s.replaceAll(',','');
            if(s!=''){
                foo.add(s);
            }
        }
        
        return foo;
    }
    
    public void removeReferralEmail(){
        string emailAddress = ApexPages.CurrentPage().getParameters().get('emailAddress');
        ApexPages.CurrentPage().getParameters().put('emailAddress', null);
        string referralContact='';
        
        for(string s : referralContactList){
            if(s!=emailAddress){
                referralContact = referralContact + s + ','; 
            }
        }
        cf.Referral_Email_Contact__c = referralContact;
        update cf;
        referralContactList = setContactList(referralContact);
    }
    
    public void addReferralEmail(){
        string emailAddress = string.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('emailAddress'));
        emailAddress=emailAddress.trim().toLowerCase();
        
        if(emailAddress!=null && emailAddress!=''){
        
            Matcher MyMatcher = emailRegEx.matcher(emailAddress);
            if(!MyMatcher.matches()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid email address: '+emailAddress));
                return;
            }
            
            if(cf.Referral_Email_Contact__c==null){cf.Referral_Email_Contact__c='';}
            
            if(cf.Referral_Email_Contact__c != null && cf.Referral_Email_Contact__c!='' && cf.Referral_Email_Contact__c.indexOf(',')>0 && cf.Referral_Email_Contact__c.right(1)!=','){
                cf.Referral_Email_Contact__c = cf.Referral_Email_Contact__c+',';    
            }
            
            
            cf.Referral_Email_Contact__c = cf.Referral_Email_Contact__c + emailAddress+',';
            try{
                update cf;
                
            }catch(dmlexception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
                return;
            }
            referralContactList = setContactList(cf.Referral_Email_Contact__c);
            
        }
    }

    public void removeAuthorizationEmail(){
        string emailAddress = string.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('emailAddress'));
        string authContact='';
        
        for(string s : authContactList ){
            if(s!=emailAddress){
                authContact= authContact+ s + ','; 
            }
        }
        
        cf.Authorization_Email_Contact__c= authContact;
        
        try{
            update cf;
        
        }catch(dmlexception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            return;
        }
        authContactList = setContactList(authContact);    
    }

    public void addAuthorizationEmail(){
        string emailAddress = string.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('emailAddress'));
        emailAddress =emailAddress.trim().toLowerCase();
        
        if(emailAddress!=null && emailAddress!=''){
            
            Matcher MyMatcher = emailRegEx.matcher(emailAddress);
            if(!MyMatcher.matches()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid email address: '+emailAddress));
                return;
            }
            
            if(cf.Authorization_Email_Contact__c==null){cf.Authorization_Email_Contact__c='';}
            
            if(cf.Authorization_Email_Contact__c!= null && cf.Authorization_Email_Contact__c !='' && cf.Authorization_Email_Contact__c.indexOf(',')>0 && cf.Authorization_Email_Contact__c.right(1)!=','){
                cf.Authorization_Email_Contact__c= cf.Authorization_Email_Contact__c+',';    
            }
            
            cf.Authorization_Email_Contact__c= cf.Authorization_Email_Contact__c+ emailAddress+',';
            
            try{
                update cf;
                
            }catch(dmlexception e){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
                return;
            }
            
            authContactList = setContactList(cf.Authorization_Email_Contact__c);
        }
    }
    
    void loadrfrMap(){
        rfrMap = new map<string, Recommended_Facility_Rule__c[]>();
        Recommended_Facility_Rule__c[] rfrList = [select id,  Priority__c, Client_Facility__c, name from Recommended_Facility_Rule__c where Client_Facility__c= :cf.id order by Priority__c asc];
        prioritySet = new set<string>();
        set<id> rfrIds = new set<id>();
        logicTipMap = new map<string, string>();
        for(Recommended_Facility_Rule__c rfr : rfrList){
            prioritySet.add(rfr.Priority__c);
            rfrIds.add(rfr.id);
            Recommended_Facility_Rule__c[] fooList = rfrMap.get(rfr.Priority__c);
            if(fooList==null){
                fooList = new Recommended_Facility_Rule__c[]{};
            }
            
            foolist.add(rfr);
            rfrMap.put(string.valueof(rfr.Priority__c), fooList);
            logicTipMap.put(rfr.id, 'No Logic');
        }
        
        integer prev_level;
        
        for(Recommendation_Logic__c rl : [select Recommended_Facility_Rule__c, 
                                                 Level__c,
                                                 Recommended_Facility_Rule__r.L2Rule_Order__c,
                                                 Recommended_Facility_Rule__r.L1Rule_Order__c,
                                                 Recommended_Facility_Rule__r.L3Rule_Order__c, 
                                                 Recommended_Facility_Rule__r.L1Order__c,
                                                 Recommended_Facility_Rule__r.L2Order__c,
                                                 Recommended_Facility_Rule__r.L3Order__c,
                                                 Display_Value__c,
                                                 Field_Value__c, 
                                                 Field_Logic__c, 
                                                 Field_Label__c from Recommendation_Logic__c where Recommended_Facility_Rule__c in :rfrIds order by Recommended_Facility_Rule__c asc, level__c asc, createddate asc]){
            
            string tip = logicTipMap.get(rl.Recommended_Facility_Rule__c);
            integer level = integer.valueof(rl.Level__c);
            if(level!=null){
            if(tip==null || tip =='No Logic'){
                tip='';
            }
            
            if(prev_level!=null &&
              (prev_level != integer.valueof(rl.level__c))){
              
              tip = tip.left(tip.length()-5)+'\n';
              tip = tip+ rl.Recommended_Facility_Rule__r.get('L'+ prev_level +'Rule_Order__c')+ '\n';
              tip = tip+''+rl.Field_Label__c +' '+ rl.Field_Logic__c +' '+ rl.Display_Value__c+' '+rl.Recommended_Facility_Rule__r.get('L'+level+'Order__c')+'\n';
                
            }else{
              
              tip = tip+''+rl.Field_Label__c +' '+ rl.Field_Logic__c +' '+ rl.Display_Value__c+' '+rl.Recommended_Facility_Rule__r.get('L'+level+'Order__c')+'\n';
            }
            }
            logicTipMap.put(rl.Recommended_Facility_Rule__c, tip);
            
            prev_level= integer.valueof(rl.level__c);
                
        }
         
        for(id i : logicTipMap.keyset()){
            string s = logicTipMap.get(i);
            
            if(s.mid(1,2)=='OR'){
                s = s.right(s.length()-3);
            
            }else if(s.mid(1,3)=='AND'){
                s = s.right(s.length()-4);
            }
            
            if(s.right(5)=='null\n'){
                s = s.left(s.length()-5);
            }else{
            
                s = s.left(s.length()-3);
            }
            logicTipMap.put(i, s);
        }
        
    }
    
    public void updateRule(){
        
        string newPriority = ApexPages.CurrentPage().getParameters().get('newPriority');
        ApexPages.CurrentPage().getParameters().put('newPriority', null);
        
        string ruleID = ApexPages.CurrentPage().getParameters().get('ruleID');
        ApexPages.CurrentPage().getParameters().put('ruleID', null);
        
        
        if(newPriority==null || ruleID==null){
            return;
        }
        boolean newPriorityRow = newPriority=='z';        
        integer upper=0;
        if(newPriority=='z'){
            
            string previousPriority = ApexPages.CurrentPage().getParameters().get('previousPriority');
            ApexPages.CurrentPage().getParameters().put('previousPriority', null);
            
            integer dbPreviousPriority = integer.valueOf(previousPriority);
            dbPreviousPriority = dbPreviousPriority+1;
            
            
            for(string p: rfrMap.keyset()){
                if(integer.valueof(p)>upper){
                    upper = integer.valueof(p);
                }
            }
            
            
            if((upper+1)<=3){
                newPriority = string.valueof(upper+1);
            }else{
                newPriority = '3';
            }
        }
         
        Recommended_Facility_Rule__c tempRuleHolder;
        system.debug( rfrMap.keyset());
        for(string p: rfrMap.keyset()){
            system.debug(tempRuleHolder);
            if(tempRuleHolder==null){
           
                Recommended_Facility_Rule__c[] rules = rfrmap.get(p);
            
                for(Recommended_Facility_Rule__c rule : rules){
                    system.debug(rule.id+' '+ruleID);
                    if(rule.id.equals(ruleID)){
                        tempRuleHolder = rule;
                        break;
                    }
                    
                }
                system.debug(tempRuleHolder+' '+rules.size()+ ' '+integer.valueof(p)+' '+integer.valueof(newPriority));
                if(tempRuleHolder!=null && rules.size()==1 && integer.valueof(p)<integer.valueof(newPriority)){
                    //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'One rule must exist in each level before a new level is set. '));
                    return;
                
                }
            
            }
            
        }
        
        if(tempRuleHolder!=null){
        
            tempRuleHolder.priority__c = newPriority;
            system.debug(tempRuleHolder.priority__c);
            update tempRuleHolder;
            loadrfrMap();
        }
        
    }
    
    string hotelId(){
        string hotelId= ApexPages.CurrentPage().getParameters().get('ecenHotelId');
        ApexPages.CurrentPage().getParameters().put('ecenHotelId', null);
        return hotelId;
    }
    
    public void deleteHotel(){
        string hotelId= hotelId();
        
        try{
            delete ([select id from ecen_hotel__c where id = :hotelId]);
        }catch(dmlexception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getDmlMessage(0)));
            isError=true;
            return;
        }
        gethotels();
    }
    
    public void getEcnHotel(){
        string hotelId= hotelId();
        hotel = [select hotel__c from ecen_hotel__c where id =:hotelId];
        
    }
    
    public void newEcenHotel(){
        hotel = new ecen_hotel__c(client_facility__c=cf.id);
    }
    
    public boolean isError {get; private set;}
    
    public void saveEcnHotel(){
        isError=false;
        try{
            hotel.name = hotel.hotel__r.name;
            try{
            if(hotel.id==null){
                hotel.name = [select name from hotel__c where id = :hotel.hotel__c].name;
            }
            }catch(QueryException e){}
            
            upsert hotel;
        }catch(dmlexception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,e.getDmlMessage(0)));
            isError=true;
            return;
            
        }
        
    }
    
    
    
    transient ecen_hotel__c[] hotels;
    
    public ecen_hotel__c[] gethotels(){
        
        ecen_hotel__c[] hotels = new ecen_hotel__c[]{};
        hotels = [select hotel__r.Address__c,
                         hotel__r.City__c,
                         hotel__r.State__c,
                         hotel__r.Zip_Code__c,
                         hotel__r.name,
                         CreatedDate from ecen_hotel__c where client_facility__c = :cf.id];
        return hotels;
        
    }

    public pageReference newRecommendedFacilityRule(){
        
        string returnURL = '/' + Recommended_Facility_Rule__c.SObjectType.getDescribe().getKeyPrefix() + '/e?'
        + relationshipIds.Client_Facility_Rule_lkid__c+'='+ EncodingUtil.UrlEncode(cf.name,'UTF-8')
        + '&'+
        + relationshipIds.Client_Facility_Rule_lkid__c+'_lkid='+cf.id
        + '&retURL=%2F'+cf.id;
        
        return new pageReference(returnURL);
    }
    
   
    public void newPlanCode(){
        
        if(planSelectOptions==null){
            loadEmployeePlanOptions();
        }
        planCode = new HealthPac_Plan_Code__c(client_facility__c=cf.id, procedure__c=cf.procedure__c, facility__c = cf.facility__c);
    }
    
    public void saveHealthpacPlanCode(){
        isError=false;
        try{
            
            upsert planCode;
        
        }catch(dmlexception e){
            isError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            return;
        }
    }

    public void DeleteHealthpacPlanCode(){
        try{
            string planCodeId = getPlanCodeId();
            HealthPac_Plan_Code__c deleteCode = new HealthPac_Plan_Code__c(id=planCodeId);
            delete deletecode;
            
        }catch(dmlException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            return;
        
        }
    }
    
    void loadEmployeePlanOptions(){
        
        set<selectOption> plans = new set<selectOption>();
        
        for(Client_Employee_Healthplan__c ehp : [select name from Client_Employee_Healthplan__c where Client_carrier__r.client__c = :cf.client__c]){
            plans.add(new selectOption(ehp.name, ehp.name));    
        }
        
        if(plans.isEmpty()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'No Employee Healthplans have been added'));
        }else{
            plans.add(new selectOption('', '')); 
        }
        
         
        planSelectOptions = new list<selectOption>(plans);
        planSelectOptions.sort();
    
    }
    
    string getPlanCodeId(){
        string planCode = ApexPages.CurrentPage().getParameters().get('planCodeId');
        ApexPages.CurrentPage().getParameters().put('planCodeId', null);
        return planCode;
    
    }
    
    public void getPlanCode(){
        string pCode = getPlanCodeId();
        planCode = [select HealthPac_Plan_Code__c,Case_Plan_Name__c from HealthPac_Plan_Code__c where id = :pCode];
        loadEmployeePlanOptions();
        
    }
    
    public HealthPac_Plan_Code__c[] getplanCodes(){
    
        planCodes = new HealthPac_Plan_Code__c[]{};
        planCodes=[select Active__c,
                          Case_Plan_Name__c,
                          Client_Facility__r.name,
                          createdDate,
                          HealthPac_Plan_Code__c,
                          name from HealthPac_Plan_Code__c where Client_Facility__c = :cf.id order by active__c, name];
        
        return planCodes;
    }
         
    void loadPhysicians(){
        physicians = new Client_Facility_Physician__c[]{}; 
        physicians = [select createdDate,
                             Physician__r.name,
                             Physician__r.first_name__c,
                             Physician__r.last_name__c,
                             Physician__r.Credentials__c from Client_Facility_Physician__c where Client_Facility__c = :cf.id];
        
                          
    }

    public void newProvider(){
        provider = new Client_Facility_Physician__c(Client_Facility__c=cf.id);
           
    }
    
    string providerId(){
        string providerId = ApexPages.CurrentPage().getParameters().get('providerId');
        ApexPages.CurrentPage().getParameters().put('providerId', null);
        return providerId;
    }
    
    public void getClientProvider(){
        
        string providerId = providerId();
        provider = [select physician__c from Client_Facility_Physician__c where id = :providerId];
        
    }
    
    public void deleteProvider(){
        
        try{
            string providerId = providerId();
            Client_Facility_Physician__c deleteProvider = new Client_Facility_Physician__c(id=providerId);
            delete deleteProvider;
            loadPhysicians();
            
        }catch(dmlException e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            return;
        
        }
    }
    
    public void saveProvider(){
        try{
            upsert provider;
            loadPhysicians();
        }catch(dmlexception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            return;
        }
    }
    
    public void clearPageMessages(){
        ApexPages.getMessages().clear();
    }
}