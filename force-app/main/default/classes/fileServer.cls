public with sharing class fileServer extends hp_callWebApp{
    
    
    public static string unassignedfiles(string filesrc){
        fileServer getunassignedfiles = new fileServer();
        return getunassignedfiles.getunassignedfiles(filesrc);
        
    }
    
    public string getunassignedfiles(string filesrc){
        
        endpoint='filechangerequest/filelist';
        FileChangeRequestStruct request = new FileChangeRequestStruct('','');
        search.add('filesrc:' + filesrc);
        search.add('sessionid:'+request.sessionid);
        search.add('orgid:'+request.Orgid);
        search.add('userid:'+request.userid);
        search.add('id:filelist');
        
        jsonBody = jb.eligSearch(search);
        result = callWebApp(null);  
        return result;    
    }
    
    public static FileChangeRequestResponseStruct moveFile(string filesrc, string filedst){
        system.debug('\nfilesrc: '+filesrc+' \nfiledst: '+filedst);
        fileServer moveFile = new fileServer();
        FileChangeRequestResponseStruct response = new FileChangeRequestResponseStruct();
        try{
            response= (FileChangeRequestResponseStruct )JSON.deserialize(moveFile.mFile(filesrc, filedst, 'movefile'), FileChangeRequestResponseStruct .class);
        }catch(exception e){
            response.ResultCode ='2';
            response.Message =': Unable to connect to the file server. Please contact support.';
        }
        return response;
    }

    public static string copyFile(string filesrc, string filedst){
        
        fileServer copyFile = new fileServer();
        return copyFile.mFile(filesrc, filedst, 'copyfile');
        
    }
    
    public string mFile(string filesrc, string filedst, string ep){
        endpoint='filechangerequest/'+ep;
        FileChangeRequestStruct request = new FileChangeRequestStruct('','');
        search.add('filesrc:' + filesrc);
        search.add('filedst:' + '/'+filedst);
        search.add('sessionid:'+request.sessionid);
        search.add('orgid:'+request.Orgid);
        search.add('userid:'+request.userid);
        search.add('id:filemove');
        
        jsonBody = jb.eligSearch(search);
        if(Test.isRunningTest()){
            result='{"Message":"Successfully moved the file","ResultCode":"0"}';
        }else{
            result = callWebApp(null);
        }
        return result; 
    }
    
    public blob getFile(string sessionid, string userid, string Orgid, string filepath){
        string[] rfp = filePath.split('/');
        string fileName = rfp[rfp.size()-1];
        system.debug(fileName );
        if(fileName.left(2)=='a0'){
            fileName=fileName.right(fileName.length()-19);
        }
        
        http h = new http();
        HttpRequest req = new HttpRequest();
        filepath= filepath.replaceAll(':','%3A').replaceAll(',','%2C').replaceAll(' ','%20');
        string url = fileServer__c.getInstance().Ussscorpion__c+'openfile/'+fileName+'?filepath='+filepath+'&oid='+ Orgid + '&sid=' + sessionid + '&uid='+ userid;
        Blob header = Blob.valueOf('sfattachsftp:' + floridaKeys__c.getInstance().USSSCORPION__c);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(header);
        req.setHeader('Authorization', authorizationHeader);
        req.setEndPoint(url );
        req.setmethod('GET');
        req.setTimeout(120000);
        HttpResponse res = h.send(req);
        system.debug(res.getBody());
        try{
                   
                if(res.getStatusCode()!=200){ 
                    system.debug(res.getStatusCode()+' error');
                    insert new Api_error__c(Status_Code__c=string.valueof(res.getStatusCode()), Message__c=res.getBody().left(200));
                   return null;
                }
            }catch(exception e){
                system.debug(e.getMessage()+' error');
                return null;
            }
        
        return res.getBodyAsBlob();
    }
    
    public static string safeName(string x){
        string y=x;
        y=y.replaceAll(',','_');
        y=y.replaceAll(' ','');
        return y;
    }
        
}