public with sharing class travelformcontroller {

    public Patient_Case__c obj {get;set;}
    public pc_POC__c poc {get; set;}
    public string casetype {get;set;}
    public string theurl{get;set;}
    public EmployeeContacts__c ec {get;set;}
    public string ct_patientLastName {get; private set;}
    
    Public integer employeeContactsCount{get; private set;}
    AggregateResult[] employeeContactCount;
        
    public travelformcontroller () {
       set<id> eIdSet = new set<id>();//added 7-27-20
        
       if(ApexPages.currentPage().getParameters().get('id') != null){
            
           obj = [select nHotel_Name__c, nreferred_facility__c, referred_facility__c,client_name__c, RecordType.Name,Shuttle_Information_Departure__c,Shuttle_Information_Arrival__c,departing_Flight_Time__c,Arriving_Flight_Time__c,departing_flight_number__c,arriving_flight_number__c,Hotel_Name__c,travel_type__c,Travel_Needs_Comments__c,OP_Pre_op_Evaluation__c, Procedure__c, trfCreatedDate__c, Travel_Notes__c,Physician_Follow_up__c,Departing_Airport_Transportation__c,Arriving_Airport_Tranpsortation__c,Hotel_Check_In_Date__c, Hotel_CheckOut_Date__c, expeditedTravelRequest__c,Estimated_Arrival__c,Estimated_Departure__c,Actual_Arrival__c, Actual_Departure__c, case_type__c, caregiver_mobile__c,caregiver_home_phone__c,caregiver_dob__c,caregiver_dobe__c,caregiver_name__c,employee_street__c,employee_city__c,employee_state__c,employee_Zip_Postal_Code__c,employee_last_name__c,employee_first_name__c,bid__c, certid__c, Patient_Email_Address__c, patient_mobile__c, PATIENT_HOME_PHONE__c, patient_street__c, patient_city__c,patient_state__c,Patient_Zip_Postal_Code__c, lastmodifieddate,patient_first_name__c,patient_last_name__c,Patient_DOB__c,Patient_DOBe__c,EmployeeID__c,Caregiver_Relationship__c from Patient_case__c where id = :ApexPages.currentPage().getParameters().get('id')];
           ct_patientLastName = obj.patient_last_name__c;
           ct_patientLastName = ct_patientLastName .replaceAll(',','%2C');
           try{
           poc = [select Last_Appointment_Date__c, Last_Appointment_Time__c from pc_POC__c where patient_case__c = :obj.id order by createdDate desc limit 1];
           }catch(exception e){
               poc = new pc_poc__c(patient_case__c=obj.id); 
           }
           //added 7-27-20 start***************************************
           try{
           for(RelatedtoEmployee__c rte: [select id, RelatedEmployeeID__c, EmployeeContactID__c from RelatedtoEmployee__c where RelatedEmployeeID__c !=null and RelatedEmployeeID__c = :obj.EmployeeID__c]){
            eIdSet.add(rte.EmployeeContactID__c );
           }   
          
           employeeContactCount=[select count(id) cnt from employeeContacts__c where id in :eIdSet and EmployeeRelationShip__r.name !='Employee']; 

           if(employeeContactCount!=null){ 
               Integer I = integer.valueof(employeeContactCount[0].get('cnt')); // Integer.valueOf(str) ; 
               system.debug('Integer Value = '+I);
        
               employeeContactsCount=I+1;        
           }   
        
           If(employeeContactsCount>1){                 
               ec=[select FirstName__c,LastName__c,DOB__c,DOBe__c,HomePhoneNumber__c,MobilePhoneNumber__c,Traveler__c,EmployeeRelationShip__r.name from EmployeeContacts__c where id in :eIdSet and EmployeeRelationShip__r.name !='Employee' and Traveler__c=true limit 1];
               system.debug('ec= '+ec);
           } 
          }catch(exception e){}  
          //added 7-27-20 end***************************************             
           init(ApexPages.currentPage().getParameters().get('id'));
       }
        
    }
    
    public void init(id theID){
                
        try{
        obj.patient_dob__c = date.valueof(obj.patient_dobe__c);
        obj.Caregiver_DOB__c= date.valueof(obj.Caregiver_DOBe__c);
        
        
        }catch(exception e){}
        
        try{
            ec.DOB__c=date.valueof(ec.DOBe__c); //sends the wrong vallue to the page ***Mon Sep 01 00:00:00 GMT 1980 *** <-- Not if you use apex:outputfield

        }catch(exception e){}
        
    
        casetype = obj.RecordType.Name;
    
        Document doc = new Document();
        if(Test.isRunningTest()){
            doc.id = '015m00000004lHD';    
        }else{
            doc = [Select id from Document where name = 'hdLogo'];
        }
        
        if(utilities.isRunningInSandbox()){
            theurl = 'https://hdplus--test--c.documentforce.com/servlet/servlet.ImageServer?id=015A00000035DEY&oid=00DQ000000EeOC8';
        }else{
            theurl = 'https://c.'+Environment__c.getInstance().instance__c+'.content.force.com/servlet/servlet.ImageServer?id=015A00000035DEY&oid=00DA0000000gEEo';
        }
    }
    
    public void updatetrfdate() {
         if(obj.trfCreatedDate__c==null){
          obj.trfCreatedDate__c = date.today();
          update obj;  
        }
        
        if(obj.Actual_Departure__c == null && obj.Estimated_Departure__c!= null){
            obj.Actual_Departure__c = obj.Estimated_Departure__c;
        }
        
        if(obj.Actual_Arrival__c == null && obj.Estimated_Arrival__c != null){
            obj.Actual_Arrival__c = obj.Estimated_Arrival__c;
        }
    }
    
    public string getTransportMethod(){
        
        if(obj.travel_type__c=='Flying'){
            return 'Flying';
        }else if(obj.travel_type__c!=''){
            return 'Driving';
        }
        
        return '';
    }
    
    public string getAirport(){
        if(obj.travel_type__c!='Flying'){
            return 'NA';
        }else if(obj.nreferred_facility__c=='Johns Hopkins'){
            return 'BWI';
        }else if(obj.nreferred_facility__c=='Mercy Springfield'){
            return 'SGF';
        }else if(obj.nreferred_facility__c=='Virginia Mason'){
            return 'SEA';
        }else if(obj.nreferred_facility__c=='Kaiser'){
            return 'SNA';
        }
        
        return '';
    }
    
    public string getHotel(){
        return obj.nHotel_Name__c;

        
    }
    
    public string getCar(){
        if(obj.travel_type__c!='Flying'){
            return 'NA';
        }else if(obj.nreferred_facility__c=='Johns Hopkins'){
            return 'KD Elite';
        }else if(obj.nreferred_facility__c=='Mercy Springfield'){
            return 'Scheduled by Mercy';
        }else if(obj.nreferred_facility__c=='Virginia Mason'){
            return 'A Plus Town Car';
        }else if(obj.nreferred_facility__c=='Kaiser'){
            return obj.Arriving_Airport_Tranpsortation__c;
        }else if(obj.nreferred_facility__c=='Mayo Jacksonville'){
            return 'About Town Limo';
        }else if(obj.nreferred_facility__c=='Mayo Phoenix'){
            return 'Desert Sedan';
        }else if(obj.nreferred_facility__c=='Mayo Rochester'){
            return 'Destiny Limousine';
        }
        
        return '';
    }
    
    public String getWordPrintViewXML(){
        // doesn't need to be on multiple lines, it's just for readability
    return   '<!--[if gte mso 9]>' +
        '<xml>' +
        '<w:WordDocument>' +
        '<w:View>Print</w:View>' +
        '<w:Zoom>100</w:Zoom>' +
        '<w:DoNotOptimizeForBrowser/>' +
        '</w:WordDocument>' +
        '</xml>' +
        '<![endif]>';
        
    }

}