public without sharing class patientCaseTestData{
     public Client__c client;
     public Client_Facility__c clientFacility;
     public Facility__c facility;
     public Procedure__c ecenProcedure;
     public patient_case__c pc;
     sObject[] sObjects = new sObject[]{};
    
     public void loadData(string clientName, string facilityName, string procedureName){
        client = new Client__c(name=clientName);
        sObjects.add(client);
        
        ecenProcedure = new Procedure__c(name=procedureName);
        sObjects.add(ecenProcedure);
        
        facility = new Facility__c(name=facilityName);
        facility.Facility_Address__c = '2500 S Power Rd';
        facility.Facility_City__c= 'Mesa';
        facility.Facility_State__c= 'AZ';
        facility.Facility_Zip__c= '85205';
        sObjects.add(facility);
        
        insert sObjects;
        clientFacility = new client_facility__c();
        clientFacility.Procedure__c= ecenProcedure.id;
        clientFacility.client__c = client.id;
        clientFacility.Facility__c = Facility.id;
        
        insert clientFacility;
        
        pc = new patient_case__c(Ecen_Procedure__c=ecenProcedure.id, client__c=client.id, client_facility__c=clientFacility.id);
        
        pc.Employee_First_Name__c ='John';
        pc.Employee_Last_Name__c ='Smith';
        pc.Employee_dobe__c ='1980-01-02';
        
        pc.Patient_Last_Name__c ='Smith';
        pc.Patient_First_Name__c ='John';
        pc.Patient_dobe__c ='1980-01-02';
        
        pc.status__c='Open';
        pc.status_reason__c='New';
        
    }
    
    public void populateDemo(){
        pc.employee_street__c ='2500 S Power Rd';
        pc.employee_city__c ='Mesa';
        pc.employee_state__c ='AZ';
        pc.Employee_Zip_Postal_Code__c='85206';
        
        pc.patient_street__c ='2500 S Power Rd';
        pc.patient_city__c ='Mesa';
        pc.patient_state__c ='AZ';
        pc.patient_Zip_Postal_Code__c='85206';
        
    }
    
    public pc_poc__c addPOC(id theId){
        
        pc_poc__c poc = new pc_poc__c(patient_case__c=theId);
        poc.First_Appointment_Date__c = date.today();
        poc.First_Appointment_Time__c = '10:30 am';
        poc.Hospital_Admission_Date__c= date.today();
        poc.Hospital_Discharge_Date__c= date.today();
        poc.Last_Appointment_Date__c= date.today();
        poc.Surgical_Procedure_Date__c= date.today();
        poc.Patient_Arrival_Date__c= date.today();
        poc.Physician_Follow_up_Appointment_Date__c= date.today();
        poc.Physician_Appointment_Time__c = '10:30 am';
        poc.Return_Home_Travel_Date__c = date.today();
        
        return poc;
        
    }

}