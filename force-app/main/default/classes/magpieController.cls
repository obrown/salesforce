public with sharing class magpieController {

    string errorID;
    public magpie_Error__c error {get; set;}
    public magpie_Error__c[] relatedErrors {get; private set;}
    
    public magpie_Work_Task__c[] workTaskList {get; set;}
    public magpie_Work_Task__c workTask {get; set;}
    public map<string, magpie_Work_Task__c> wtMap = new map<string, magpie_Work_Task__c>();
    public string initalNote {get; set;}
    
    public magpie_Notes__c[] notes {get; private set;}
    map<string, magpie_Notes__c[]> notesMap = new map<string, magpie_Notes__c[]>();
    public magpie_Notes__c note {get; set;}
    
    public boolean hasError {get; private set;}
    
    public selectOption[] userList {get; private set;}
    
    public PaginatedsObjectList pgNotesList {get; private set;}
    
    string userAlias;
    
    public magpieController(){
        try{
        
            errorID = ApexPages.CurrentPage().getParameters().get('id');
            
            if(string.valueof(id.valueof(errorID).getSObjectType())=='magpie_Work_Task__c'){
                workTask = new magpie_Work_Task__c(id=errorID);
                errorID = [select magPie_error__c from magpie_Work_Task__c where id = :errorID].magPie_error__c ;
            }
            
            if(errorID==null || errorID==''){
                newError();
            
            }else{
                init();
                loadRelatedErrors();
                
                if(workTask !=null && worktask.id != null){
                    getWorkTask(worktask.id);
                    
                }
                
            }
            
        }catch(exception e){
            system.debug(e.getMessage());
            newError();
        }
        
    }
    
    void newError(){
        error = new magpie_error__c(status__c='Open');
        relatedErrors = new magpie_Error__c[]{};
        workTaskList = new magpie_Work_Task__c[]{};
    }
    
    public magpieController(id errorID){
        
        this.errorID = errorID;
        init();
        loadRelatedErrors();
    }
    
    void init(){
        
        loadError();
        loadWorkTasks();
        notesInit();
        //loaduserList();
    }
    
    /* Error Methods */

    void loadError(){
        error = [select Client__r.Name,
                        Client__r.PBM_Vendor__c,
                        Client__r.Underwriter__c,
                        Client__r.Current_Start_Date__c,
                        Client__r.Current_End_Date__c,
                        Client__c,
                        Closed_Date__c,
                        createdDate,
                        createdBy.name,
                        Deductible_Amount__c,
                        Dispensed_Date__c,
                        Employee_SSN__c,
                        Error_Message__c,
                        Error_Reason__c,
                        Error_Type__c,
                        Error_Validated__c,
                        File_Name__c,
                        Group__c,
                        hpRecordId__c,
                        Import_Type__c,
                        lastModifiedDate,
                        Member_Name__c,
                        Member_Sequence__c,
                        MM_Deductible_Variance__c,
                        MM_OOP_Variance__c,
                        Name,
                        Outcome__c,
                        Out_of_Pocket_Amount__c,
                        Owner.name,
                        OwnerID,
                        Resolution_Type__c,
                        Review_Note__c,
                        Rx_Claim_Number__c,
                        RX_Deductible_Variance__c,
                        RX_OOP_Variance__c,
                        Source_Type__c,
                        Status__c,
                        Transaction_Date__c,
                        Underwriter__c,
                        Vendor_Name__c from magpie_Error__c where id = :errorID];
        
        if(error.Dispensed_Date__c!=null){
        
            string y = string.valueof(error.Dispensed_Date__c.year());
        
            if(error.Client__r.Current_Start_Date__c != null ){
                mc_rxFrom = error.Client__r.Current_Start_Date__c.month() + '/' + error.Client__r.Current_Start_Date__c.day() +'/'+ y;
            }
        
            if(error.Client__r.Current_End_Date__c != null ){
                mc_rxTo = error.Client__r.Current_End_Date__c.month() +'/'+ error.Client__r.Current_End_Date__c.day() +'/'+ y;
            }
        
        }
        
    }
    
    public string mc_rxTo {get; private set;}
    public string mc_rxFrom {get; private set;}
    
    public void saveError(){
    
        hasError=false;
        
        try{
        
            upsert error;
            if(errorID!='' && errorID!=null){
                loadError();
            }
            
        }catch(dmlException e){
            hasError=true;
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
            } 
            
        }
    
    }
    
    void loadRelatedErrors(){
    
        relatedErrors = new magpie_Error__c[]{};
        string key = error.employee_ssn__c.right(4)+error.underwriter__c+'%';
        
        for(magpie_Error__c me : [select employee_ssn__c, name, Member_Name__c,dispensed_date__c, Member_Sequence__c,status__c, error_message__c,createdBy.Name, CreatedDate, lastmodifiedby.name,lastmodifiedDate from magpie_Error__c where hpRecordId__c like :key and id != :error.id]){
            if(me.employee_ssn__c==error.employee_ssn__c){
                
                if(me.dispensed_date__c != null){
                date s = date.valueof(me.dispensed_date__c.year()+'-'+error.Client__r.Current_Start_Date__c.month()+'-'+error.Client__r.Current_Start_Date__c.day());
                date e = date.valueof(me.dispensed_date__c.year()+'-'+error.Client__r.Current_End_Date__c.month()+'-'+error.Client__r.Current_End_Date__c.day());

                if(me.dispensed_date__c >= s && me.dispensed_date__c <= e){
                
                    relatedErrors.add(me);
                }
                }
            }
            
        }
    
    }
    
    /* End Error Methods */
    
    /* Work Task Methods */
    
    void loadWorkTasks(){
        
        workTaskList = new magpie_Work_Task__c[]{};
        integer count=1;
        for(magpie_Work_Task__c wt :[select Assigned__c,
                                            createdDate,
                                            createdBy.Name,
                                            Follow_up_Date__c,
                                            Name,
                                            Number__c,
                                            Status__c,
                                            Work_Department__c,
                                            Work_Task__c from magpie_Work_Task__c where magpie_Error__c = :error.id order by createdDate asc]){
                                            
                                            
        
             
            wt.number__c=count;
            count++;
            workTaskList.add(wt);
            wtMap.put(wt.id, wt);
        }
        
    }
    
    public void newWorkTask(){
    
        workTask = new magpie_Work_Task__c(magpie_Error__c = error.ID, number__c=workTaskList.size()+1, status__c = 'Open');
        notes = null;
    }
    
    public void saveWorkTask(){
        hasError  =false;
        boolean newWT=false;
        magpie_Work_Task__c wt = workTask.clone(true, true);
        
        try{
            
            if(wt.id==null){
                newWT=true;
            }
            
            upsert wt;
            loadWorkTasks();
            
            if(newWT && initalNote !=''){
                note = new magpie_Notes__c(Work_Task__c = wt.id, notes__c = initalNote);
                initalNote=null;
                saveNote();
                  
            }
            
            workTask= wtMap.get(wt.id);
            loadError();    
            
        }catch(dmlException e){
            hasError=true;
            
            for (Integer i = 0; i < e.getNumDml(); i++) {
                 // Process exception here
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
            }
        }
    
    }
    
    void getWorkTask(string wtID){
        workTask = wtMap.get(wtID);
        notes = notesMap.get(wtID);
        
        if(notes == null ){
            notes = new magpie_Notes__c[]{};
        }
        
        if(pgNotesList==null){
            pgNotesList= new PaginatedsObjectList();
            
        }
        
        pgNotesList.setSoList(notes);
    
    }
    
    public void getWorkTask(){
        string wtID = ApexPages.CurrentPage().getParameters().get('wtID');
        getWorkTask(wtID);
        ApexPages.CurrentPage().getParameters().put('wtID', null);
    
    }
    
    public void deleteWorkTask(){
        
        id wtID = ApexPages.CurrentPage().getParameters().get('wtid');
        delete wtMap.get(wtID);
        loadWorkTasks();
        notesMap.put(wtID, null); 
        notes=null;
        ApexPages.CurrentPage().getParameters().put('wtid', null);
    
    }
    
    /* End Work Task Methods */
    
    /* Notes Methods */
    
    void notesInit(){
        
        notesMap = new map<string, magpie_Notes__c[]>();
        
        for(magpie_Notes__c mn : [select createdDate, 
                                         createdby.Name, 
                                         Notes__c,
                                         Write_to_Healthpac_EE__c,
                                         Write_to_Healthpac_Mbr__c,
                                         Work_Task__c from magpie_Notes__c where Work_Task__c = :wtMap.keySet() order by createdDate desc]){
                                         
            notes = notesMap.get(mn.Work_Task__c);        
                    
            if(notes == null ){
                notes = new magpie_Notes__c[]{};
            }
            
            notes.add(mn);
            notesMap.put(mn.Work_Task__c , notes);
        
        }
        
        notes = null;
        
    }
    
    public string notesPerPage {get; set;}
    
    public magpie_Notes__c[] getnotesList(){
        notes= (magpie_Notes__c[])pgNotesList.getviewlist();
        return notes;
    }
    
    public void loadNotes(){
        getWorkTask();
    }
    
    public void newNote(){
        note = new magpie_Notes__c(Work_Task__c = workTask.id);
    }
    
    /*
    public void getNote(){
        
        id noteID = ApexPages.CurrentPage().getParameters().get('noteID');
        ApexPages.CurrentPage().getParameters().put('noteID', null);
        
        for(magpie_Notes__c n : notes){
            
            if(n.id == noteID){
                note = n;
                break;
            }
        }
    
    }
    
    public void deleteNote(){
        
        id noteID = ApexPages.CurrentPage().getParameters().get('noteID');
        ApexPages.CurrentPage().getParameters().put('noteID', null);
        
        id wtID = ApexPages.CurrentPage().getParameters().get('wtid');
        ApexPages.CurrentPage().getParameters().put('wtid', null);
        
        for(magpie_Notes__c n : notes){
            
            if(n.id == noteID){
                delete n;
                ApexPages.CurrentPage().getParameters().put('wtID', wtID);
                loadNotes();
                break;
            }
        }
        
    }
    */
    
    public void saveNote(){
        hasError =false;
        try{
            
            if(userAlias==null){
                userAlias = UserUtil.CurrentUser.Alias;
            }
            
            if(note.Write_to_Healthpac_EE__c){
                hp_Sendnote.sendNote(error.underwriter__c, error.Employee_SSN__c, error.group__c, '00', 'EMP   ',userAlias, note.notes__c);
            }
            
            if(note.Write_to_Healthpac_Mbr__c){
                hp_Sendnote.sendNote(error.underwriter__c, error.Employee_SSN__c, error.group__c, error.Member_Sequence__c, 'DEP   ',userAlias, note.notes__c);
            }
            
            
            
            magpie_Notes__c foo = note.clone(true, true);
            upsert foo;
            notesInit();
            notes = notesMap.get(foo.Work_Task__c);
            
            
            
        }catch(Exception e){
            hasError=true;
            system.debug(e);
        }
    
    }
    
    /* End Notes Methods */
    
    public void loaduserList(){
        
        if(userList==null){
            userList = new selectOption[]{}; //[select name, UserRole.Name from user where isactive=true];
            for(User u : [select name, UserRole.Name from user where isactive=true order by name asc]){
                userList.add(new selectOption(u.id, u.name));
            }
        }
        
    }
    
    public void changeOwner(){
        hasError=false;
        
        try{
            update error;
            loadError();
        }catch(dmlException e){
             hasError=true;
             for (Integer i = 0; i < e.getNumDml(); i++) {
                 // Process exception here
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
            }
        }
        
    }
   

}