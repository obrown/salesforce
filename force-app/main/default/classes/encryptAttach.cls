public with sharing class encryptAttach {

    blob cryptoKey;
    
    public encryptAttach(){
        this.cryptoKey = Crypto.generateAesKey(128);
    }
    
    public encryptAttach(blob incomingKey){
        cryptoKey = incomingKey;
    }

    public blob encryptAttachmentBlob(blob decryptedBlob){
 
        Blob encryptedData = Crypto.encryptWithManagedIV('AES128', this.cryptoKey, decryptedBlob);
        return encryptedData;
 
    }
    
    public blob getTheKey(){
        return cryptoKey;
    }
    
    public blob decryptAttachmentBlob(blob encryptedBlob){
        
        Blob decryptedData = encryptedBlob;
        try{
           decryptedData = Crypto.decryptWithManagedIV('AES128', this.cryptoKey, encryptedBlob);
        }catch(exception e){}
        
        return decryptedData;
    }

}