/**

	Breaking up the testing of the transController

 */
@isTest
private class TransplantTest2 {

    private static testMethod void testTransAdmit() 
	{
		
		createTransplant ct = new createTransplant();
        Transplant__c tr = ct.newTransplant(true);
        system.assert(tr.id!=null);

        caregiver__c cg = ct.addCG(tr.id);
    	TransController tc = new Transcontroller(new ApexPages.StandardController(tr));
        
        ct.addPro(tc);
		tc.AddProvider();
		
		/* Convert */
        
        tc.convertLead();
        
        /* End */
        
        test.startTest();
        
        /* Transplant Listing Testing */
        
        tc.tltype = tc.obj.transplant_type__c;
        tc.obj.Transplant_Listing__c = date.today().addDays(-5);
        tc.obj.Transplant_Listing_Verification__c = date.today().addDays(-2);
        tc.obj.Status_7_Date__c = date.today();
        tc.obj.Status_7_detail__c = 'Testing';
        tc.addTransplantDates();
        
        system.assert(tc.TransplantDates.size()>0);
        
        tc.translistingID = tc.TransplantDates[0].id;
        tc.editTransListingDate();
        tc.updateTransListingDate();
        tc.translistingID = tc.TransplantDates[0].id;
        tc.deleteTransListing();
        tc.cancelTransType();
        
        /* End */

		/* Trans Admit Date Testing */
		
		tc.obj.transplant_type__c = 'Heart;Lung';
		tc.inlineSave();
		
		tc.obj.TransplantDateModal__c = date.today().addDays(-3);
		tc.obj.TransplantAdmissionDateModal__c = date.today().addDays(-3);
		tc.transadmittype ='Heart';
		tc.addTransAdmitDate();
		system.assert(tc.transAdmitList.size()>0);
		
		tc.transadmitid = tc.transAdmitList[0].id;
		tc.editTransAdmitDate();
		tc.obj.TransplantDischargeDateModal__c = date.today();
		
		tc.updateTransAdmitDate();
		tc.transadmitid = tc.transAdmitList[0].id;
		tc.delTransAdmitDate();
		
		
		/* End */

        test.stopTest();
	}
	
	private static testMethod void testConvertCaseRequiredFields() 
	{
		Transplant__c trans = new Transplant__c();
		trans.transplant_type__c = 'Heart';
		insert trans;
		
		Provider__c pro = new Provider__c(parentid__c=trans.id);
		insert pro;
		convertCase.walmartPreLoader(trans.id, 'trans');
	}
}