/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seealldata=true)
private class testBatchProcess {

    static testMethod void myUnitTest() {

        id lid;

        for(integer i=0;i<=3;i++){
        
        patient_case__c L = new patient_case__c();
        l.RecordTypeId = '012A0000000ViUe';
        l.Client_Name__c = 'Walmart';
        L.employee_Last_Name__c='Test1' + i;
        L.employee_First_Name__c='Demo' + i;
        //L.Company = 'Tester Company + i';
        L.Patient_Home_Phone__c='9587458452';
        L.Relationship_to_the_Insured__c = 'Patient is Insured';
        L.Date_of_Last_Primary_Care_Phys_Visit__c  = system.today();
        L.Medicare_As_Secondary_Coverage__c = 'Yes';
        L.Expedited_Referral__c  = 'No';
        L.Patient_last_Name__c = 'test Last';
        L.Patient_Preferred_Phone__c  = '4676464644';
        L.Patient_Emergency_Contact_Name__c  = 'test';
        L.Patient_Emergency_Contact_Phone__c  = '23123123';
        L.Patient_Emergency_Contact_Relationship__c ='Spouse';
        L.Patient_City__c = 'P city';
        L.Patient_Street__c = 'p street';
        L.Patient_State__c = 'OH';
        L.Patient_Country__c = 'USA';
        L.Patient_Symptoms__c = 'test symptoms'; 
        L.Patient_DOB__c = date.today();
        L.Patient_gender__c = 'Male';
        L.Actual_Arrival__c = date.today().addDays(+10);
         L.Patient_Symptoms__c = '2323';   
         L.Caregiver_Home_Phone__c  = '4850123';
         L.Caregiver_Name__c = 'Test CareGiver';    
         L.Other_Pertinent_Medical_Info_History__c = 'asdlfjasdjf';
         L.Recent_Testing__c ='eradfadsf';        
         L.Relationship_to_the_Insured__c = 'Patient is Insured';
         L.Diagnosis__c  ='testafas';
         L.Proposed_Procedure__c ='klhsaddklfhaskldfj';
         L.Procedure_Complexity__c = 'Non-Complex';
         L.Referred_Facility__c = 'test facility';
         L.Date_Eligibility_is_Checked_with_Client__c  = system.today();
         L.Eligible__c = 'Yes';
         L.Employee_Primary_Health_Plan_Name__c = 'Test';
         L.Employee_DOB__c =  date.today();
         L.Number_of_DRG_Codes__c  = 1.00;
         L.Employee_gender__c = 'Female';
         L.Employee_SSN__c  = '121123';
         L.Patient_SSN__c  = '122123';
         L.Employee_Street__c = 'E street';
         L.Employee_City__c = 'E City';
         L.Employee_State__c = 'AZ';
         L.Employee_Country__c = 'USA';
         L.Status__c = 'Open';
         SystemID__c sid = SystemID__c.getInstance();  
         try{
         list<User> testUser = new list<User>([Select u.Profile.Name, u.ProfileId From User u where u.Profile.id =:sid.csrProfID__c and isActive = true limit 1]);
         L.OwnerId = testUser[0].id;
         }catch(exception e){}
         
         insert L;
         lid = l.id;
         }
        batchProcess bp = new batchProcess();
        bp.runIntakeOpen();
        
        tenDayEligibilityCheck tdec = new tenDayEligibilityCheck();
        tdec.run();
        
        Claim__c testClaim = new Claim__c();
        testClaim.patient_case__c = lid;
        testClaim.Name = '12345678901';
        insert testClaim;
    
    }
}