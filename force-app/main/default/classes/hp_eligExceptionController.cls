public with sharing class hp_eligExceptionController {
    
    public logSumType.eligibilityLogs[] lst {get;set;}
    public list<list<logExceptionType.EligibilityRecs>> excepLst {get;set;}
    public list<list<logChangeType.EligChanges>> changes {get;set;}
    public string exceptToRun {get;set;}
    public map<string,string[]> cLoadDates {get;set;}
    public map<string,string[]> sumData {get;set;}
    public map<string,string> logFileName {get;set;}
    string theCSV;
    public boolean getLastTwentyResult {get;set;}
    public boolean changeError {get;set;}
    public boolean exceptionError {get;set;}
    public integer exceptionCount {get;set;}
    public integer changesCount {get;set;}
    public string contentType {get;set;}
    public string table {get;set;}
    public string clientName {get;set;}
    
    public string gettheCSV(){
        if(theCSV==null){return theCSV;}
        return theCSV;
    }
    
    public void exportToCSV(){
        
        //string tableToExport = ApexPages.currentPage().getParameters().get('table');
        string body='';
        //string fileName = ApexPages.currentPage().getParameters().get('fileName');
                
        if(table=='exceptions'){
            body+='Group,SSN,Seq,Priority,Message\n';
            
            for(logExceptionType.EligibilityRecs[] excp : excepLst){
                for(logExceptionType.EligibilityRecs el: excp){
                    
                    if(el.ElgxGrnbr!=null){
                        body+=el.ElgxGrnbr.escapeCsv()  +',';
                    }else{
                        body+=',';
                    }
                    
                    
                    if(el.ElgxEssn!=null){
                        body+=el.ElgxEssn.escapeCsv()   +',';
                    }else{
                        body+=',';
                    }
                    
                    if(el.ElgxDepseq!=null){
                        body+=el.ElgxDepseq.escapeCsv() +',';
                    }else{
                        body+=',';
                    }
                    
                    if(el.ElgxType!=null){
                        body+=el.ElgxType.escapeCsv()   +',';
                    }else{
                        body+=',';
                    }
                    
                    if(el.ElgxMsg!=null){
                        body+=el.ElgxMsg.escapeCsv()    +'\n';
                    }
                }
            }
            
        }else if(table=='changes'){
            body+='Group,SSN,Last Name,First Name,Field,Before,After\n';
            
            for(logChangeType.EligChanges[] el : changes){
            for(logChangeType.EligChanges e : el){
                
                if(e.EhsGrnbr!=null){
                    body+=e.EhsGrnbr.escapeCsv()   +',';
                }else{
                    body+=',';
                }
                
                if(e.EhsSsn!=null){
                    body+=e.EhsSsn.escapeCsv()     +',';
                }else{
                    body+=',';
                }
                
                if(e.EhsLname!=null){
                    body+=e.EhsLname.escapeCsv()   +',';
                }else{
                    body+=',';
                }
                
                if(e.EhsFtname!=null){
                    body+=e.EhsFtname.escapeCsv()  +',';
                }else{
                    body+=',';
                }
                
                if(e.EhsField!=null){
                    body+=e.EhsField.escapeCsv()   +',';
                }else{
                    body+=',';
                }
                
                if(e.EhsBefore!= null){
                    body+=e.EhsBefore.escapeCsv()  +',';
                }else{
                    body+=',';
                }
                
                if(e.EhsAfter!=null){
                    body+=e.EhsAfter.escapeCsv()   +'\n';
                }else{
                    body+='\n';
                }
            }
            } 
            
        }
        
        if(body!=''){
            contentType = 'text/csv#'+clientName+table+'.csv';
            theCSV=body.trim().removeStart('\n');
            //Messaging.SingleEmailMessage mail = utilities.email(userInfo.getUserEmail(),null,'mmartin@hdplus.com','System',fileName,'Please see the attached\n\nThanks');
            //Blob csvBlob = Blob.valueOf(body);
            //Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            //efa.setFileName(fileName+'.csv');
            //efa.setBody(csvBlob);
            //mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            //mail.setReplyTo('noreply@hdplus.com');
            //Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
        }
        
       
    }
    
    public hp_eligExceptionController(){
        contentType = 'text/html';
        //theCSV='';
        lst = new logSumType.eligibilityLogs[]{};
        excepLst = new list<list<logExceptionType.EligibilityRecs>>();
        changes= new list<list<logChangeType.EligChanges>>();   
        this.cLoadDates = new map<string,string[]>();
        this.sumData = new map<string,string[]>(); 
        this.logFileName = new map<string,string>(); 
        exceptionCount=0;
        changesCount=0;
        getLastTwenty();
            
    }
    
    public void getLastTwenty(){
        //theCSV='';
        logSumType foo = hp_eligExceptionLogs.getLastTwenty();
        if(foo.isError==true){
            getLastTwentyResult=true;
            //Messaging.SingleEmailMessage mail = utilities.email('mmartin@hdplus.com',null,null,Userinfo.getFirstName() + ' ' + Userinfo.getLastName(),'Exeception Reporting API Error, GetLastTwenty','Hello,\n\nAn error occurred in hp_eligExceptionLogs\n\n'+userinfo.getFirstName()+' '+userinfo.getLastName());
            //messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        }else{
            lst = foo.EligibilityLogs;
            getLastTwentyResult=false;
        }
       
    }
   /*
    public selectOption[] getClientList(){
        if(lst==null){getLastTwenty();}
        //if(lst==null){  return null;}
        set<string> clientsAdded = new set<string>();
        selectOption[] foo = new selectOption[]{};
        foo.add(new selectOption('--','--'));
        string[] lDates = new string[]{};
        lDates.add('Select Client Above');
        cLoadDates.put('--', lDates);
        string id = '';
        string[] lSumData = new string[]{};
        
        for(logSumType.eligibilityLogs le: lst){
            system.debug(le.LogUnderwriter);
                        
            if(le.LogUnderwriter!='   '){
            id=string.valueof(math.random());
            
            if(!clientsAdded.contains(le.LogUnderwriter)){
                clientsAdded.add(le.LogUnderwriter);
                foo.add(new selectOption(le.LogUnderwriter,le.LogClientname));
            }
            
            lDates = cLoadDates.get(le.LogUnderwriter);
            
            if(lDates==null){
                lDates = new string[]{};
                lDates.add('--:--');
            } 
            
            lDates.add(le.frlogRDATE+':'+id);
            
            cLoadDates.put(le.LogUnderwriter, lDates);
            
            lSumData.add(le.logTOTEXCS);
            lSumData.add(le.logSKPEXCS);
            
            sumData.put(id,lSumData.clone());
            logFileName.put(id,le.logFile);
            lSumData.clear();
        }    
        }
        foo.sort();
        return foo;
        
    }
    */
    public void EligibilityList(){
       exceptionCount =0; 
       exceptionError=false; 
       string underwriter = ApexPages.currentPage().getParameters().get('uw');
       string lDate = ApexPages.currentPage().getParameters().get('ld');
       logExceptionType.EligibilityRecs[]  foo = new logExceptionType.EligibilityRecs[]{};
       
       if(underwriter!=null && lDate != null){
           lDate=lDate.trim();
           if(excepLst==null){
               excepLst = new list<list<logExceptionType.EligibilityRecs>>(); 
           }else{
               excepLst.clear();
           }
           
           try{
               for(logExceptionType.EligibilityRecs hpe: hp_eligExceptionLogs.getExceptions(lDate,underwriter).EligExceptions){
                   exceptionCount++;
                   foo.add(hpe);
                   if(foo.size()==999){
                       excepLst.add(foo);
                       foo.clear();        
                   }
               }
               
               excepLst.add(foo);
           }catch(exception e){exceptionError=true;}
       }
    }
    
    public void emailFiles(){
       changesCount=0; 
       excepLst.clear();
       changes.clear();
       string underwriter = ApexPages.currentPage().getParameters().get('uw');
       string lDate = ApexPages.currentPage().getParameters().get('ld');
       
       if(underwriter!=null && lDate != null){
           lDate=lDate.trim();
           
       logChangeType.EligChanges[] foo = new logChangeType.EligChanges[]{};
       logExceptionType.EligibilityRecs[]  fooy = new logExceptionType.EligibilityRecs[]{};
       
           try{
              for(logChangeType.EligChanges lct: hp_eligExceptionLogs.getChanges(lDate,underwriter).EligChanges){
              
                   changesCount++;
                   foo.add(lct);
                   if(foo.size()==999){
                       changes.add(foo);
                       foo.clear();        
                   }
              }
               
              changes.add(foo);
           }catch(exception e){}
           
           try{
               for(logExceptionType.EligibilityRecs hpe: hp_eligExceptionLogs.getExceptions(lDate,underwriter).EligExceptions){
                   exceptionCount++;
                   fooy.add(hpe);
                   if(fooy.size()==999){
                       excepLst.add(fooy);
                       fooy.clear();        
                   }
               }
               
               excepLst.add(fooy);
           }catch(exception e){}
           
           ApexPages.currentPage().getParameters().put('fileName',ApexPages.currentPage().getParameters().get('clientName')+' exceptions '+lDate);
           ApexPages.currentPage().getParameters().put('table','exceptions');
           exportToCSV();
           
           ApexPages.currentPage().getParameters().put('fileName',ApexPages.currentPage().getParameters().get('clientName')+' changes '+lDate);
           ApexPages.currentPage().getParameters().put('table','changes');
           exportToCSV();
       
       }
        
    }
    
    public void changeList(){
       changeError=false;  
       changesCount=0; 
       string underwriter = ApexPages.currentPage().getParameters().get('uw');
       string lDate = ApexPages.currentPage().getParameters().get('ld');
       logChangeType.EligChanges[] foo = new logChangeType.EligChanges[]{};
       
       if(underwriter!=null && lDate != null){
           lDate=lDate.trim();
           if(changes==null){
               changes= new list<list<logChangeType.EligChanges>>(); 
           }else{
               changes.clear();
           }
           
           try{
               for(logChangeType.EligChanges lct: hp_eligExceptionLogs.getChanges(lDate,underwriter).EligChanges){
               
                   changesCount++;
                   foo.add(lct);
                   if(changesCount>999){
                       changesCount=0;
                       changes.add(foo);
                       foo = new logChangeType.EligChanges[]{};        
                   }
               }
               
               changes.add(foo);
               system.debug(changes.size());
           }catch(exception e){
               system.debug(e.getMessage());
               changeError=true;
           }
           
       }
    }    
    
}