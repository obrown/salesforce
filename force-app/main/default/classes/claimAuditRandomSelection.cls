public  with sharing class claimAuditRandomSelection{ 
 
 public claimAuditRandomSelection(){}
 
 public string RandomSelection(caClaim__c[] clmsToUpload, Claims_Audit_Client__c client){
           if(clmsToUpload==null){
            return '0'; 
           }
           
           
           
           map<string, caClaim__c[]> cFoo = new map<string, caClaim__c[]>(); 
           /*
           caClaims_Processed__c[] processedCount = new caClaims_Processed__c[]{};
           map<string, caClaims_Processed__c> processedMapCount = new map<string, caClaims_Processed__c>();
           
           for(caClaims_Processed__c c : [select client__c, Claims_Processed__c, Month_Paid__c from caClaims_Processed__c where client__c = :client.id]){
                caClaims_Processed__c foo = processedMapCount.get(c.month_paid__c.year()+'-'+c.month_paid__c.month());
                if(foo==null){
                    foo = new caClaims_Processed__c(client__c=client.id,Month_Paid__c=date.valueof(c.month_paid__c.year()+'-'+c.month_paid__c.month()+'-1'),Claims_Processed__c=0);
                }
                processedMapCount.put(c.month_paid__c.year()+'-'+c.month_paid__c.month(), foo );
           }
           */
           
           for(caClaim__c c : clmsToUpload){
               if(client.percent_to_pull__c>0){
                   caClaim__c[] claims = cFoo.get(c.Claims_Processor_Crosswalk__c);
                   if(claims==null){
                       claims= new caClaim__c[]{};
                   }
                   claims.add(c);
                   cfoo.put(c.Claims_Processor_Crosswalk__c, claims);
                   
                   /*
                   caClaims_Processed__c cp = processedMapCount.get(c.date_paid__c.year()+'-'+c.date_paid__c.month());
                   
                   if(cp==null){
                        cp= new caClaims_Processed__c(client__c=client.id,Month_Paid__c=date.valueof(c.date_paid__c.year()+'-'+c.date_paid__c.month()+'-1'),Claims_Processed__c=0);
                   }
                   
                   if(cp.Claims_Processed__c==null){cp.Claims_Processed__c =0;}
                   cp.Claims_Processed__c = cp.Claims_Processed__c+1;
                   processedMapCount.put(c.date_paid__c.year()+'-'+c.date_paid__c.month(),cp);
                   */
                   
               }
           }
           /*
           for(string d : processedMapCount.keyset()){
                    processedCount.add(processedMapCount.get(d));
           }
           
           upsert processedCount;
           */
           string body= clmsToUpload.size() + '\n';
           clmsToUpload.clear();
           list<Messaging.SingleEmailMessage> themList = new list<Messaging.SingleEmailMessage>();
           
           //cfoo: a map of claims, key is client and processor, value is the list of claims uploaded in the csv file
           for(string s: cfoo.keySet()){
           
               caClaim__c[] foo = cFoo.get(s); //List of claims processed by processor
               decimal r = client.percent_to_pull__c; //Percent to pull audit for the client, based on the database record
               
               if(r!=0){
                    r = r/100;
                    integer x=foo.size(); // total number of claims processed by processor
                    integer y = math.round(integer.valueof(x*r)) +1; //The number iof records that will satisfy the percent to pull
                    set<integer> z = randomNumbers.randomWithLimit(x, y); //A set of randomly generated integers. The size of the set will be equal to y 
                    if(x<=0){x=1;z.add(0);}
                    
                       
                    if(x>0){
                        for(integer i : z){
                            try{
                                if(i==0){i=1;}
                                clmsToUpload.add(foo[(i-1)]); //Add the claim as a new claim audit record to be audited later
                                body += foo[(i-1)].Claims_Processor_Crosswalk__c +', '+client.name+ '\n'; //Body of confirmation email for the 2 percent
                                
                            }catch(exception e){
                                //catch errors silently, add them to the email
                                body += (i-1) + ' ' + s + ' ' + foo.size() + ' ' + e.getMessage() + '\n';
                            }
                        }
                    }
               
               }
               
           }
           
           
           //Messaging.SingleEmailMessage mail1 = utilities.email('mmartin@hdplus.com',null,null,Userinfo.getLastName(),string.valueof(clmsToUpload.size()), client.name);  
           //themList.add(mail1);
           //messaging.sendEmail(themList);
           
           database.insert(clmsToUpload, false);
           
           set<id> clmIDset = new set<id>();
           for(caClaim__c c : clmsToUpload){
               clmIDset.add(c.id);
           }
           
           //Update audit counts
           claimAuditFuture.setClaimAuditCount(clmIDset);
           
           //Update Claim names
           claimAuditFuture.updateClaimAuditName(clmIDset);
           
           return string.valueof(clmsToUpload.size());
           
 }
 
 

}