@RestResource(urlMapping='/providerPortal/v1/providerportalpoc_selectoptions')
global with sharing class ProviderPortalSelectOptionsListener {


    class response {
        string error;
        string errorMsg;
        ProviderPortalPocSelectOptions.poc_selectoptions selectoptions;

    }

    @HttpGet
    global static void getSelectOptions() {
        response result = new response();
        result.error = 'false';
        RestResponse res = RestContext.response;
        string caseName = RestContext.request.headers.get('EcenCase');

        ProviderPortalPocWorkflow.response poc_result;
        //system.debug(requestBodyBlob.toString());
        ProviderPortalPocWorkflow poc_wf =  new ProviderPortalPocWorkflow(caseName, '', '','', 'false');
        poc_result = poc_wf.getplanofcare();

        ProviderPortalPocSelectOptions options_setup = new ProviderPortalPocSelectOptions();

        ProviderPortalPocSelectOptions.poc_selectoptions opts = options_setup.getSelectOptions(poc_result.caseType, poc_result.clientFacility);
     //   result = opts;

        res.addHeader('Content-Type', 'application/json');

        try {
            system.debug(JSON.serialize(opts));
            res.responseBody = Blob.valueOf(JSON.serialize(opts));
            res.statusCode = 200;
        } catch (exception e) {
            result.error = 'true';
            result.errorMsg = e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serialize(result));
            res.statusCode = 400;
        }
    }

    @HttpPost
    global static void saveplanofcare() {
        response result = new response();
        result.error = 'false';
        RestResponse res = RestContext.response;

        string caseName = RestContext.request.headers.get('EcenCase');
        string submittingPOC = 'false';

        string action = RestContext.request.headers.get('Action');
        string doesPatientQualify = RestContext.request.headers.get('DoesPatientQualify');
        string doesPatientNeedPOC = RestContext.request.headers.get('DoesPatientNeedPOC');
        for(string h : RestContext.request.headers.keyset()){
            system.debug(h+': '+RestContext.request.headers.get(h));
        }
        system.debug(RestContext.request.headers.get('programCandidateReason'));
        system.debug(RestContext.request.headers.get('DoesPatientNeedPOC'));
        system.debug(doesPatientQualify + ' '+ doesPatientNeedPOC);
        system.debug(RestContext.request.headers);
        blob requestBodyBlob = RestContext.request.requestBody;
        system.debug(requestBodyBlob.toString());
        system.debug('action ' + action);

        if (action == 'PUBLISH') {
          submittingPOC = 'true';
        }

        ProviderPortalPocWorkflow poc_wf = new ProviderPortalPocWorkflow(caseName, doesPatientQualify, doesPatientNeedPOC, requestBodyBlob.toString(), action);
        string savePoC = poc_wf.saveplanofcare_pc();
        switch on savePoC {
            when 'success' {
                res.addHeader('Content-Type', 'application/json');
                res.responseBody = Blob.valueOf(JSON.serialize(result));
                res.statusCode = 200;
            } when else {
                result.error = 'true';
                result.errorMsg = savePoC;
                res.responseBody = Blob.valueOf(JSON.serialize(result));
                res.statusCode = 400;
            }
        }

    }
}
