public without sharing class directBillingSearchVFcontroller{

    public hpPatientSearchStruct.PatSearchResultLogs[] results {get; set;}
    public map<string, hpPatientSearchStruct.PatSearchResultLogs> resultsMap {get; set;}
    
    public direct_billing_member__c[] sfFound {get; private set;}
    
    public boolean hpSearchfound {get; private set;}
    public boolean hpSearchRun {get; private set;}
    public boolean error {get; private set;}
    public set<string> sfFoundMemberIDs {get; private set;}
    
    public string eSSN {get; set;}
    public string efn {get; set;}
    public string eln {get; set;}
    public string manualEfn {get; set;}
    public string manualEln {get; set;}
    public string cert {get; set;}
    public string uw {get; set;}
    
    public string newCaseId {get; private set;}
    
    public Direct_Billing_Member__c member {get; set;}
    
    public boolean hasError {get; set;}
    
    public directBillingsearchVFcontroller(){
        member = new Direct_Billing_Member__c();
        eln = ApexPages.CurrentPage().getParameters().get('eln');
        if(eln!=null && eln!=''){
            searchHealthpac();
        }
        sfFoundMemberIDs = new set<string>();
    }
    
    public void clear(){
        eSSN = null;
        efn  = null;
        eln = null;
        cert = null;
        uw = null;
        hasError =false;
        hpsearchRun=false;
        results = new hpPatientSearchStruct.PatSearchResultLogs[]{};
        resultsMap = new map<string, hpPatientSearchStruct.PatSearchResultLogs>{};
        sfFound = new direct_billing_member__c[]{};
    }
    
    public void searchHealthpac(){
        
        string localOnly = ApexPages.CurrentPage().getParameters().get('local');
        
        
        if(eSSN!=null){eSSN=eSSN.trim();eSSN=eSSN.replaceAll('-','');}
        if(efn!=null){efn=efn.toUpperCase().trim();}
        if(eln!=null){eln=eln.toUpperCase().trim();}
        if(cert!=null){cert=cert.trim();}
        
        results = new hpPatientSearchStruct.PatSearchResultLogs[]{};
        resultsMap = new map<string, hpPatientSearchStruct.PatSearchResultLogs>{};
        
        hpSearchfound = false;
        
        sfFound = new direct_billing_member__c[]{};
        sfFoundMemberIDs = new set<string>();
        direct_billing_member__c[] sfResults = [select createdDate,
                                                       Address_1__c, 
                                                       Address_2__c,
                                                       City__c,
                                                       Comments__c,
                                                       Department_Code__c,
                                                       Name,
                                                       EE_SSN__c,
                                                       Employee_Name__c,
                                                       State__c,
                                                       Zip_Code__c from direct_billing_member__c order by createdDate desc];
         
        for(direct_billing_member__c db : sfResults){
                
                string ename = db.Employee_Name__c;
                if(ename!=null){
                    ename = ename.toUppercase();
                    if(eln != null && eln != '' && ename.contains(eln)){
                        sfFound.add(db);
                        sfFoundMemberIDs.add(db.name);
                    }
                    
                    if(efn != null && efn != '' && ename.contains(efn)){
                        sfFound.add(db);
                        sfFoundMemberIDs.add(db.name);
                    }
                }
                
                if(eSSN!=null && eSSN !=''){
                    if(db.EE_SSN__c==eSSN){
                        sfFound.add(db);
                        sfFoundMemberIDs.add(db.name);
                    }
                }
                
                if(cert != null && cert != ''){
                    if(cert==db.name){
                        sfFound.add(db);
                        sfFoundMemberIDs.add(db.name);
                    }
                }  
            
        }
        
        
        
        if(localOnly=='local'){return;}
        
        hpSearchRun = true;
        hpPatientSearchStruct.PatSearchResultLogs[] searchResults;
        hpPatientSearchStruct foo = hp_patientSearch.searchHP(uw, essn, cert, efn, eln);
        error=false;
        
        if(foo.ResultCode=='1'){
            //no results found
            return;
        }
        
        if(foo == null || foo.ResultCode != '2'){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error has occured in the healthpac API connection'));
            error=true;
            return;
        }
        
        if(foo.PatSearchResultLogs == null){return;}
        
        for(hpPatientSearchStruct.PatSearchResultLogs s : foo.PatSearchResultLogs){
            
            if((s.Underwriter != null && s.Underwriter.trim()=='034') && (s.Grp !=null && s.Grp.trim() != 'HDP')){
            
            hpPatientSearchStruct.PatSearchResultLogs psr = new hpPatientSearchStruct.PatSearchResultLogs(
                                                         s.Underwriter,
                                                         s.Grp,
                                                         s.SSN,
                                                         s.EmpFirstName ,
                                                         s.EmpLastName ,
                                                         s.EmpDateOfBirth ,
                                                         s.EmpAddress1 ,
                                                         s.EmpAddress2 , 
                                                         s.EmpCity ,
                                                         s.EmpState ,
                                                         s.EmpZip ,
                                                         s.Sequence ,
                                                         s.PatFirstName ,
                                                         s.PatLastName ,
                                                         s.PatDateOfBirth ,
                                                         decodeRelationship(s.Relationship),
                                                         s.Gender,
                                                         decodeStatus(s.Status),
                                                         s.Dept,
                                                         safeTrim(s.MemberNumber),
                                                         '');
            
                
                if(psr.Relationship=='Employee' ){
                    hpSearchfound=true;
                    if(!sfFoundMemberIds.contains(psr.MemberNumber)){
                        string key = psr.EmpFirstName.trim()+psr.EmpLastName.trim()+psr.EmpDateOfBirth.trim()+psr.Underwriter.trim();
                        key = key.replaceAll(' ','');
                        resultsMap.put(key, psr);
                        results.add(psr); 
                        
                    }
                }
            
            }
            
        }
                
    }
    
    //Record being created via data from HealthPac
    public void createDbMember(){
        
        newCaseId='';
        
        string key = ApexPages.CurrentPage().getParameters().get('memberKey');
        ApexPages.CurrentPage().getParameters().put('memberKey', null);
        key=key.replaceAll(' ','');
        hpPatientSearchStruct.PatSearchResultLogs psr = resultsMap.get(key);
        Direct_Billing_Member__c dbm = new Direct_Billing_Member__c();
        dbm.Employee_Name__c =safeTrim(psr.EmpFirstName)+' '+safeTrim(psr.EmpLastName);
        dbm.EE_SSN__c = psr.SSN;
        dbm.name =psr.MemberNumber;
        dbm.Address_1__c =psr.EmpAddress1;
        dbm.City__c = psr.EmpCity;
        
        try{ //If it fails, it fails
        if(psr.EmpAddress2 != null && psr.EmpAddress2.trim().left(dbm.City__c.length()) !=  dbm.City__c){
            dbm.Address_2__c =psr.EmpAddress2;
        }else{dbm.Address_2__c='NA';}
        }catch(exception e){dbm.Address_2__c='NA';}
        
        dbm.State__c = psr.EmpState;
        dbm.Zip_Code__c= psr.EmpZip;
        dbm.Department_Code__c= psr.Dept;
        
        try{
            insert dbm;
            newCaseId = dbm.id;
        }catch(dmlException e){
        
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
            } 
        }
    }
    
    //Record being created manually
    public void saveNewDbMember(){
        
        try{
            member.Employee_Name__c = manualEfn + ' ' + manualEln;
            member.name = member.name.toUpperCase();
            if(member.EE_SSN__c!=null){
                member.EE_SSN__c=member.EE_SSN__c.replaceAll('-','');
            }
            upsert member;
            newCaseId = member.id;
            
        }catch(dmlException e){
        
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
            } 
        }
        
    }
    
    string decodeStatus(string status){
    
        if(status=='A'){
            return 'Active';
        }else if(status=='T'){
            return 'Termed';
        }
    
        return 'NA';
    }
    
    string decodeRelationship(string rel){
        
        if(rel=='1' || rel=='01'){
            return 'Employee';
        }else if(rel=='2' || rel=='02'){
            return 'Spouse';
        }else if(rel=='3' || rel=='03'){
            return 'Son';
        }else if(rel=='4' || rel=='04'){
            return 'Daughter';
        }else if(rel=='5' || rel=='05'){
            return 'Other';
        }
        
        return 'NA';
        
    }
    
    string safeTrim(string xx){string x = xx;if(x==null){x='';}else{x=x.trim();}return x;}
    
}