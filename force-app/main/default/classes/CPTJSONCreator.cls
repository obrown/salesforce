public with sharing class CPTJSONCreator {

    public String getJSON(){
        String cptName = Apexpages.currentPage().getParameters().get('cptName');
        if(cptName==null){return null;}
        if(cptName.length()<2){return null;}
        List<CptWrapper> wrp = new List<CptWrapper>();
        for (CPT_Code__c a : [Select a.Description__c, a.Name From CPT_Code__c a WHERE Name Like : '%'+cptName+'%' or Description__c Like : '%'+cptName+'%']) {  
               CptWrapper w = new CptWrapper (a.Name, a.Description__c);
               wrp.add(w);
        }
        return JSON.serialize(wrp);
    }

    public class CptWrapper{
        String cptName, cptDescription;

        public CptWrapper(String iName, String iDescription)
        {
            cptName = iName;
            cptDescription = iDescription;
        }
    }

}