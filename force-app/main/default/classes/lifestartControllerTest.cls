/**
    This class contains unit tests for validating the behavior of the Apex lifestartController class
    97% coverage on 9/2/2015
*/

@isTest
private class lifestartControllerTest {

    static testMethod void lifeStartTest(){
        
        lifestartController lsc = new lifestartController();
    
        Lifestart__c ls = new Lifestart__c(Patient_First_Name__c='Sally',Patient_Last_Name__c='Smith');
        insert ls;
        
        Apexpages.currentpage().getparameters().put('id', ls.id);
        lsc = new lifestartController();
        lsc.dob = date.today().addyears(-25);
        lsc.ls.Estimated_Delivery_Date__c = date.today().addDays(140);
        lsc.ls.Marital_Status__c = 'Married';
        lsc.ls.Patient_Education__c ='B';
        lsc.ls.Home_Phone_Number__c ='(480) 555-1212';
        lsc.ls.Race_Ethnicity__c = 'White';
        lsc.ls.Height_ft__c = 5;
        lsc.ls.Height_in__c = 6;
        lsc.ls.Pregravid_Weight__c = 115;
        lsc.ls.Weight__c = 135;
        lsc.QuickSave();
        
        lsc.ph.Delivered__c = 'Yes';
        lsc.ph.Delivered_Outcome__c = 'Single Liveborn';
        lsc.ph.Type_of_Delivery__c = 'VBAC';
        lsc.ph.Weeks_Gestation__c = '38-40 weeks';
        lsc.ph.baby_gender__c = 'Female';
        lsc.ph.Delivery_Date__c = date.today().addYears(-2);
        lsc.ph.Weight_at_Birth_lbs__c = 6;
        lsc.ph.Weight_at_Birth_oz__c =5;
        
        lsc.savePH();
        
        lsc.ph.Delivered__c = 'Yes';
        lsc.ph.Delivered_Outcome__c = 'Single Liveborn';
        lsc.ph.Type_of_Delivery__c = 'VBAC';
        lsc.ph.Weeks_Gestation__c = '38-40 weeks';
        lsc.ph.baby_gender__c = 'Female';
        lsc.ph.Delivery_Date__c = date.today().addYears(-4);
        lsc.ph.Weight_at_Birth_lbs__c = 6;
        lsc.ph.Weight_at_Birth_oz__c =5;
        
        lsc.savePH();
        
        system.assert(lsc.phList.size()>0);
        Apexpages.currentpage().getparameters().put('phid', lsc.phList[0].id);
        lsc.getPh();
        
        Apexpages.currentpage().getparameters().put('deletePH', lsc.phList[1].id);
        lsc.deletePH();
        lsc.phCancel();
        
        lsc.drug.Drug_Type__c ='Something';
        lsc.drug.Drug_Amount_Day__c = 'Something';      
        lsc.drug.Drug_Comment__c = 'Something';  
        lsc.saveDrug();
        
        system.assert(lsc.drugList.size()>0);
        Apexpages.currentpage().getparameters().put('phid', lsc.drugList[0].id);
        lsc.getDrug();
        
        Apexpages.currentpage().getparameters().put('deletePH', lsc.drugList[0].id);
        lsc.deleteDrug();
        lsc.drugCancel();
        
        lsc.completeIA();
        system.assert(lsc.ls.Initial_Assesment_Complete__c==true);
        
        lsc.ls.Estimated_Delivery_Date__c = date.today().addDays(20);
        
        lsc.quickSave();
        
        List<SelectOption> selectedSOPML = new List<SelectOption>();
        selectedSOPML.add(new SelectOption('Low backache - constant or rhythmic pressure','Low backache - constant or rhythmic pressure'));
        selectedSOPML.add(new SelectOption('Pelvic pressure - feeling "like the baby is going to fall out','Pelvic pressure - feeling "like the baby is going to fall out'));
        lsc.ls20_26.Signs_of_Premature_Labor__c = 'Intestinal cramps (gas pains) - with or without diarrhea;Low backache - constant or rhythmic pressure';
        lsc.getselectedSOPML20_26();
        lsc.setselectedSOPML20_26(selectedSOPML);
        lsc.getallSOPML20_26();
        
        lsc.ls27_33.Signs_of_Premature_Labor__c = 'Intestinal cramps (gas pains) - with or without diarrhea;Low backache - constant or rhythmic pressure';
        lsc.getselectedSOPML27_33();
        lsc.setselectedSOPML27_33(selectedSOPML);
        lsc.getallSOPML27_33();
        
        lsc.ls34_40.Signs_of_Premature_Labor__c = 'Intestinal cramps (gas pains) - with or without diarrhea;Low backache - constant or rhythmic pressure';
        lsc.getselectedSOPML34_40();
        lsc.setselectedSOPML34_40(selectedSOPML);
        lsc.getallSOPML34_40();
        
        List<SelectOption> selectedDS = new List<SelectOption>();
        selectedDS.add(new SelectOption('Pain in abdomen or back which feels like a period or is severe','Pain in abdomen or back which feels like a period or is severe'));
        selectedDS.add(new SelectOption('Pain, redness in leg','Pain, redness in leg'));
        lsc.ls20_26.danger_signs__c ='Blurred vision, double vision or seeing spots or flashes;Severe headache';
        lsc.getselectedDS20_26();
        lsc.setselectedDS20_26(selectedDS);
        lsc.getallDS20_26();
        
        lsc.ls27_33.danger_signs__c ='Blurred vision, double vision or seeing spots or flashes;Severe headache';
        lsc.getselectedDS27_33();
        lsc.setselectedDS27_33(selectedDS);
        lsc.getallDS27_33();
        
        lsc.ls34_40.danger_signs__c ='Blurred vision, double vision or seeing spots or flashes;Severe headache';
        lsc.getselectedDS34_40();
        lsc.setselectedDS34_40(selectedDS);
        lsc.getallDS34_40();
        
        lsc.lca.Communication_Type__c ='Call';
        lsc.lca.Contact_Description__c='Unit test';
        lsc.lca.Contact_Name__c='Sally Smith';
        lsc.lca.Contact_Type__c='Patient';
        lsc.lca.Initiated__c='Inbound';
        
        lsc.saveLCA();
        system.assert(lsc.lcaList.size()>0);
        
        Apexpages.currentpage().getparameters().put('lcaid', lsc.lcaList[0].id);
        lsc.getLCA();
        
        Apexpages.currentpage().getparameters().put('deleteLCA', lsc.lcaList[0].id);
        lsc.deleteLCA();
        
        lsc.lcaCancel();
        
        lsc.outcomePregnancy.Baby_Name__c = 'Sally Smith'; 
        lsc.outcomePregnancy.Delivery_Date__c = date.today();
        lsc.outcomePregnancy.Baby_Name__c = 'Sally Smith';
        lsc.outcomePregnancy.Delivery_Time__c= '1:00 am';
        lsc.outcomePregnancy.Weight_at_Birth_lbs__c= 6;
        lsc.outcomePregnancy.Weight_at_Birth_oz__c= 5;
        lsc.outcomePregnancy.Type_of_Delivery__c = 'VBAC';
        
        lsc.quickSave();
        
        system.assert(lsc.outcomePregnancyList .size()>0);
        
        Apexpages.currentpage().getparameters().put('opID', lsc.outcomePregnancyList [0].id);
        lsc.editOutcomePregnancy();
        
        system.assert(lsc.outcomePregnancyList[0].id == lsc.outcomePregnancy.id);
        
        Apexpages.currentpage().getparameters().put('opID', lsc.outcomePregnancyList [0].id);
        lsc.deleteOutcomePregnancy();
        
        lsc.newOutcomePregnancy();
        lsc.Cancel();
        
        //catch some errors
        lsc.drug.lifestart__c = null;
        lsc.saveDrug();
        
        lsc.lca.lifestart__c = null;
        lsc.saveLCA();
        
        lsc.ph.lifestart__c = null;
        lsc.savePH();
      
        lsc.OutcomePregnancy.delivery_date__c = date.today();
        lsc.OutcomePregnancy.lifestart__c = null;
        
        lsc.quicksave();
        
        lsc.newProvider();
        lsc.pro.first_name__c = 'John';
        lsc.pro.last_name__c = 'Smith';
        lsc.pro.Specialty__c = 'OB/GYN';
        lsc.pro.Letter__c= true;
        
        lsc.SaveProvider();
        lsc.SaveNewProvider();
        system.assert(lsc.proList.size() > 0);
        
        Apexpages.currentpage().getparameters().put('proID', lsc.proList[0].id);
        lsc.getProvider();
        
        system.assert(lsc.pro.id == lsc.proList[0].id);
        
        Apexpages.currentpage().getparameters().put('delProID', lsc.pro.id);
        lsc.deleteProvider();
        
        system.assert(lsc.proList.size() == 0);
        
        Apexpages.currentpage().getparameters().put('id', lsc.ls34_40.id);
        lsc = new lifestartController();
        
        
        
    }

}