public with sharing class referralFormController {
    
    public list<Provider__c> proList {get;set;}
    public list<Caregiver__c> cgList {get;set;}
    public Patient_Case__c pc {get;set;}
    public Transplant__c trans {get;set;}
    public string hdpContact {get;set;}  
    public boolean hasHTC {get;set;}
    public string pfName {get;set;}
    public string plName {get;set;}
    public string pSSN {get;set;}
    public string pDOB {get;set;}
    public string pStreet {get;set;}
    public string pCity {get;set;}  
    public string pZip {get;set;}   
    public string pHomePhone {get;set;}         
    public string pMobilePhone {get;set;}   
    public string pEmail {get;set;}     
    public string pBID {get;set;}   
    public string eFname {get;set;} 
    public string eLname {get;set;} 
    public string eStreet {get;set;}    
    public string eCity {get;set;}      
    public string eZip {get;set;}       
    public string cn {get;set;}
    public string cnHP {get;set;} 
    public string cnMP {get;set;}   
    public string pMonth {get;set;} 
    public string cDOB {get;set;}
    public string eSSN {get;set;}        
    public string eDOB {get;set;}
    public string theTo {get;set;}
    public string thecgnote {get;set;}
    public string proposedServiceType {get;set;}
    public string diagnosis {get;set;}
    boolean isPC = false;    
    public string surgeonName {get;set;}
    
    public referralFormController(ApexPages.StandardController stdController) {
        
        id theID = stdController.getid();
        if(utilities.getobjtype(string.valueOf(theID).left(3))=='Patient_Case__c'){
            pc = (Patient_Case__c)stdController.getRecord();
            loadcgnote();
            isPC = true;  
            try{
                pc_poc__c poc = [select diagnosis__c,Proposed_Service_Type__c,Surgeon_Name_Reporting__c from pc_poc__c where patient_case__c = :theID order by createdDate desc limit 1];
                diagnosis = poc.diagnosis__c;
                proposedServiceType =poc.Proposed_Service_Type__c;
                Client_Facility_Physician__c cfp = [select Physician__r.First_Name__c,Physician__r.Last_Name__c from Client_Facility_Physician__c where id =:poc.Surgeon_Name_Reporting__c];
                surgeonName = cfp.Physician__r.First_Name__c+' '+cfp.Physician__r.Last_Name__c;
            }catch(exception e){}
              
        }else{
            trans = (Transplant__c)stdController.getRecord();
            
            //if(ApexPages.currentPage().getParameters() .get('pDOB')!=null){
            loadcaregiverData();   
            // }
            
            if(Trans.Referred_Facility__c=='Mayo Clinic Jacksonville'){
                if(!Trans.Transplant_Type__c.contains('Liver') && !Trans.Transplant_Type__c.contains('Bone Marrow')){
                    theTo ='hartley.lindsay@mayo.edu, hayes.ivy@mayo.edu, clark.tamara@mayo.edu, grens.christophermartin@mayo.edu, bulacan.christopher@mayo.edu';
                }else if(Trans.Transplant_Type__c.contains('Liver')){
                    theTo='hartley.lindsay@mayo.edu, hayes.ivy@mayo.edu, clark.tamara@mayo.edu, grens.christophermartin@mayo.edu, bulacan.christopher@mayo.edu, Denny.michele@mayo.edu, Yuchi.nancy@mayo.edu, Oatman.deborah@mayo.edu, Murray.serenia@mayo.edu, Jordan.joann@mayo.edu';
                }else if(Trans.Transplant_Type__c=='Bone Marrow'){
                    theTo='wade.john@mayo.edu, lesperance.virginia@mayo.edu, hartley.lindsay@mayo.edu, hayes.ivy@mayo.edu, clark.tamara@mayo.edu, grens.christophermartin@mayo.edu, bulacan.christopher@mayo.edu';
                    
                }
            }else if(Trans.Referred_Facility__c=='Mayo Clinic Jacksonville'){
                theTo ='king.linda@mayo.edu, kopke.martha@mayo.edu, bahr.matthew@mayo.edu, zelner.margaret@mayo.edu, wimbley.shayna@mayo.edu, balady.edward@mayo.edu, snodgrass.susan@mayo.edu';
            }else{
                theto='800-385-7085 ';
            }
        }
        
        if(ApexPages.currentPage().getParameters().get('pfName')!=null){
            pfName = ApexPages.currentPage().getParameters().get('pfName');
        }else{
            if(isPC){
                pfName = pc.patient_first_name__c;
            }else{
                
                pfName = trans.patient_first_name__c;   
            }
        }

        if(ApexPages.currentPage().getParameters().get('plName')!=null){
            plName = ApexPages.currentPage().getParameters().get('plName');
        }else{
            if(isPC){
                plName = pc.patient_last_name__c;
            }else{
                
                plName = trans.patient_last_name__c;   
            }
        }
        
        if(isPC){
            if(pc.case_type__c.toLowerCase().contains('walmart')){
                hdpContact= '877-286-3551 / WMCOEClinical@hdplus.com';
                    
            }else{
                hdpContact= '877-885-0654 / HDPCOEClinical@hdplus.com';
            }
        }            
        
        if(ApexPages.currentPage().getParameters().get('pDOB')!=null){
            
            if(isPC){
                pc.patient_dob__c =  date.valueof(ApexPages.currentPage().getParameters().get('pDOB'));
                pDOB = string.valueof(pc.Patient_DOB__c.month()) + '/' + string.valueof(pc.Patient_DOB__c.day())+ '/' + string.valueof(pc.Patient_DOB__c.year());
            }else{
                trans.patient_dob__c =  date.valueof(ApexPages.currentPage().getParameters().get('pDOB'));
                pDOB = string.valueof(trans.Patient_DOB__c.month()) + '/' + string.valueof(trans.Patient_DOB__c.day())+ '/' + string.valueof(trans.Patient_DOB__c.year());
            }
            
        }else{
            if(isPC){
                if(pc.Patient_DOBe__c!=null){
                    pc.Patient_DOB__c = date.valueof(pc.Patient_DOBe__c);
                    pDOB = string.valueof(pc.Patient_DOB__c.month()) + '/' + string.valueof(pc.Patient_DOB__c.day())+ '/' + string.valueof(pc.Patient_DOB__c.year());
                }
            }else{
                if(trans.Patient_DOBe__c!=null){
                    system.debug('4');
                    trans.Patient_DOB__c = date.valueof(trans.Patient_DOBe__c);
                    pDOB = string.valueof(trans.Patient_DOB__c.month()) + '/' + string.valueof(trans.Patient_DOB__c.day())+ '/' + string.valueof(trans.Patient_DOB__c.year());
                }
            }
        }

        if(ApexPages.currentPage().getParameters().get('eDOB')!=null){
            
            if(isPC){
                pc.employee_dob__c =  date.valueof(ApexPages.currentPage().getParameters().get('eDOB'));
                eDOB = string.valueof(pc.employee_DOB__c.month()) + '/' + string.valueof(pc.employee_DOB__c.day())+ '/' + string.valueof(pc.employee_DOB__c.year());
            }else{
                trans.employee_dob__c =  date.valueof(ApexPages.currentPage().getParameters().get('eDOB'));
                eDOB = string.valueof(trans.employee_DOB__c.month()) + '/' + string.valueof(trans.employee_DOB__c.day())+ '/' + string.valueof(trans.employee_DOB__c.year());
            }
            
        }else{
            if(isPC){
                if(pc.employee_DOBe__c!=null){
                    pc.employee_DOB__c = date.valueof(pc.employee_DOBe__c);
                    eDOB = string.valueof(pc.employee_DOB__c.month()) + '/' + string.valueof(pc.employee_DOB__c.day())+ '/' + string.valueof(pc.employee_DOB__c.year());
                }
            }else{
                if(trans.employee_DOBe__c!=null){
                    
                    trans.employee_DOB__c = date.valueof(trans.employee_DOBe__c);
                    eDOB = string.valueof(trans.employee_DOB__c.month()) + '/' + string.valueof(trans.employee_DOB__c.day())+ '/' + string.valueof(trans.employee_DOB__c.year());
                }
            }
        }
        
        if(ApexPages.currentPage().getParameters().get('cDOB')!=null){
            if(isPC){
                pc.caregiver_dob__c =  date.valueof(ApexPages.currentPage().getParameters().get('cDOB'));
                cDOB = string.valueof(pc.caregiver_DOB__c.month()) + '/' + string.valueof(pc.caregiver_DOB__c.day())+ '/' + string.valueof(pc.caregiver_DOB__c.year());
            }
            
        }else{
            if(isPC){
                if(pc.caregiver_DOBe__c!=null){
                    pc.caregiver_DOB__c = date.valueof(pc.caregiver_DOBe__c);
                    cDOB = string.valueof(pc.caregiver_DOB__c.month()) + '/' + string.valueof(pc.caregiver_DOB__c.day())+ '/' + string.valueof(pc.caregiver_DOB__c.year());
                }
            }
            
        }

        if(ApexPages.currentPage().getParameters().get('pSSN')!=null){
            pSSN = ApexPages.currentPage().getParameters().get('pSSN');
            
            pSSN = pSSN.left(3) + '-' + pSSN.mid(3,2)+ '-' + pSSN.right(4); 
        }else{
            if(isPC){
                pSSN = pc.patient_ssn__c;
                if(pSSN!=null){
                    pSSN = pSSN.left(3) + '-' + pSSN.mid(3,2)+ '-' + pSSN.right(4);
                } 
            }else{
                
                pSSN = trans.patient_ssn__c;
                if(pSSN!=null){
                    pSSN = pSSN.left(3) + '-' + pSSN.mid(3,2)+ '-' + pSSN.right(4);
                }   
            }
        }  
        
        if(ApexPages.currentPage().getParameters().get('eSSN')!=null){
            eSSN = ApexPages.currentPage().getParameters().get('eSSN');
            eSSN = eSSN.left(3) + '-' + eSSN.mid(3,2)+ '-' + eSSN.right(4); 
        }else{
            if(isPC){
                eSSN = pc.employee_ssn__c;
                if(eSSN!=null){
                    eSSN = eSSN.left(3) + '-' + eSSN.mid(3,2)+ '-' + eSSN.right(4); 
                }
            }else{
                eSSN = trans.Employee_SSN__c;
                if(eSSN!=null){
                    eSSN = eSSN.left(3) + '-' + eSSN.mid(3,2)+ '-' + eSSN.right(4);
                }   
            }
        }       

        if(ApexPages.currentPage().getParameters().get('pStreet')!=null){
            pStreet = ApexPages.currentPage().getParameters().get('pStreet');
        }else{
            if(isPC){
                pStreet = pc.patient_street__c;
            }else{
                pStreet = trans.patient_street__c;  
            }
        }       
        
        if(ApexPages.currentPage().getParameters().get('pCity')!=null){
            pCity = ApexPages.currentPage().getParameters().get('pCity');
        }else{
            
            if(isPC){
                pCity = pc.patient_city__c;
            }else{
                
                pCity = trans.patient_city__c;  
            }           
            
        }

        if(ApexPages.currentPage().getParameters().get('pZip')!=null){
            pZip = ApexPages.currentPage().getParameters().get('pZip');
        }else{
            if(isPC){
                pZip = pc.Patient_Zip_Postal_Code__c;
            }else{
                system.debug('12');
                pZip = trans.Patient_Zip_Postal_Code__c;    
            }
            
        }

        if(ApexPages.currentPage().getParameters().get('pHomePhone')!=null){
            pHomePhone = ApexPages.currentPage().getParameters().get('pHomePhone');
        }else{
            if(isPC){
                pHomePhone = pc.Patient_Home_Phone__c;
            }else{
                system.debug('13');
                pHomePhone = trans.Patient_Home_Phone__c;   
            }
            
        }

        if(ApexPages.currentPage().getParameters().get('pMobilePhone')!=null){
            pMobilePhone = ApexPages.currentPage().getParameters().get('pMobilePhone');
        }else{
            if(isPC){
                pMobilePhone = pc.Patient_Mobile__c;
            }else{
                system.debug('14');
                pMobilePhone = trans.Patient_Mobile__c;
            }               
            
        }

        if(ApexPages.currentPage().getParameters().get('pEmail')!=null){
            pEmail = ApexPages.currentPage().getParameters().get('pEmail');
        }else{
            if(isPC){
                pEmail = pc.Patient_Email_Address__c;
            }else{
                system.debug('15');
                pEmail = trans.Patient_Email_Address__c;   
            }           
            
        }
        
        if(ApexPages.currentPage().getParameters().get('pBID')!=null){
            pBID = ApexPages.currentPage().getParameters().get('pBID');
        }else{
            
            if(isPC){
                if(pc.isConverted__c == true){
                    if(pc.Converted_Date__c <= date.valueof('2013-06-16')){
                        pBID = pc.BID__c;
                    }else{
                        pBID = pc.certID__c;
                    }
                }else{
                    pBID = pc.certID__c;
                }
            }else{
                system.debug('15');
                pBID = trans.certID__c;
            }
            
        }

        if(ApexPages.currentPage().getParameters().get('eFname')!=null){
            eFname = ApexPages.currentPage().getParameters().get('eFname');
        }else{
            if(isPC){
                eFname = pc.Employee_First_Name__c;
            }else{
                system.debug('16');
                eFname = trans.Employee_First_Name__c;  
            }           
            
        }

        if(ApexPages.currentPage().getParameters().get('eLname')!=null){
            eLname = ApexPages.currentPage().getParameters().get('eLname');
        }else{
            if(isPC){
                eLname = pc.Employee_Last_Name__c;
            }else{
                eLname = trans.Employee_Last_Name__c;   
            }           
            
        }
        
        if(ApexPages.currentPage().getParameters().get('eStreet')!=null){
            eStreet = ApexPages.currentPage().getParameters().get('eStreet');
        }else{
            if(isPC){
                eStreet = pc.Employee_Street__c;
            }else{
                eStreet = trans.Employee_Street__c; 
            }           
            
        }
        
        if(ApexPages.currentPage().getParameters().get('eCity')!=null){
            eCity = ApexPages.currentPage().getParameters().get('eCity');
        }else{
            if(isPC){
                eCity = pc.Employee_City__c;
            }else{
                eCity = trans.Employee_City__c; 
            }           
            
        }

        if(ApexPages.currentPage().getParameters().get('eZip')!=null){
            eZip = ApexPages.currentPage().getParameters().get('eZip');
        }else{
            if(isPC){
                eZip = pc.Employee_Zip_Postal_Code__c;
            }else{
                eZip = trans.Employee_Zip_Postal_Code__c;   
            }
            
        }
        
        if(ApexPages.currentPage().getParameters().get('cn')!=null){
            cn = ApexPages.currentPage().getParameters().get('cn');
        }else{
            if(isPC){
                cn = pc.Caregiver_Name__c;
            }else{
                cn = trans.Caregiver_Name__c;   
            }
            
        }   
        
        if(ApexPages.currentPage().getParameters().get('cnHP')!=null){
            cnHP = ApexPages.currentPage().getParameters().get('cnHP');
        }else{
            if(isPC){
                cnHP = pc.Caregiver_Home_Phone__c;
            }else{
                cnHP = trans.Caregiver_Home_Phone__c;   
            }           
            
        }

        if(ApexPages.currentPage().getParameters().get('cnMP')!=null){
            cnMP = ApexPages.currentPage().getParameters().get('cnMP');
        }else{
            if(isPC){
                cnMP = pc.Caregiver_Mobile__c;
            }else{
                cnMP = trans.Caregiver_Mobile__c;               
            }           

        }
        
        loadProData();
        
    }
    
    private void loadcgnote(){
        
        try{
            for(Program_Notes__c pn : [select Notes__c, subject__c from Program_Notes__c where Patient_Case__c = :pc.id order by createdDate desc]){
                if(pn.subject__c == 'Caregiver Exception Requested'){
                    thecgnote = pn.notes__c;
                    break;
                }
            }
        }catch(exception e){}
    }
    
    private void loadProData(){
        hasHTC=false;
        proList = new Provider__c[]{};
        if(isPC){
            
            for(Provider__c p : [select name,id,Type__c,Home_Transition_Care__c,First_Name__c,Last_Name__c,Phone_Number__c,Fax_Number__c,Email_Address__c,city__c,Credentials__c,Street__c,State__c,Zip_Postal_Code__c from Provider__c where patient_case__c = :ApexPages.currentPage().getParameters().get('id') order by createddate asc]){
                if(p.Home_Transition_Care__c=='Yes'){
                    hasHTC=true;
                }
                proList.add(p);
            }
            
        }else{
            proList = [select name,id,Type__c,Home_Transition_Care__c,First_Name__c,Last_Name__c,Phone_Number__c,Fax_Number__c,Email_Address__c,city__c,Credentials__c,Street__c,State__c,Zip_Postal_Code__c from Provider__c where parentID__c = :trans.id order by createddate asc];  
        }
        
    }

    private void loadcaregiverData(){
        
        cgList = [select Caregiver_First_Name__c,Caregiver_Last_Name__c,Caregiver_Home_Phone__c,Caregiver_Mobile_Phone__c,HomePhone__c,MobilePhone__c,lastName__c,Firstname__c from Caregiver__c where parentID__c = :trans.id and isActive__c = true and isPCM__c = false order by createddate desc];
        
        if(ApexPages.currentPage().getParameters().get('cgfn0')!=null){    
            
            integer i=0;
            
            for(Caregiver__c c: cglist){
                c.Firstname__c = ApexPages.currentPage().getParameters().get('cgfn'+i);
                c.lastName__c = ApexPages.currentPage().getParameters().get('cgln'+i);
                c.Caregiver_Home_Phone__c=ApexPages.currentPage().getParameters().get('cghp'+i);
                c.Caregiver_Mobile_Phone__c=ApexPages.currentPage().getParameters().get('cgmp'+i);
                i++;
            }   
            
        }else{
            for(Caregiver__c c: cglist){
                c.Firstname__c = c.Caregiver_First_Name__c;
                c.lastName__c = c.Caregiver_Last_Name__c;
                c.Caregiver_Home_Phone__c=c.HomePhone__c;
                c.Caregiver_Mobile_Phone__c=c.MobilePhone__c;
            }
        }  
        
    }
    
    public referralFormController(ApexPages.StandardController stdController, string FirstName) {
        
        id theID = stdController.getid();
        if(theID !=null){
            system.debug(utilities.getobjtype(string.valueOf(theID).left(3)));
            if(utilities.getobjtype(string.valueOf(theID).left(3))=='Patient_Case__c'){
                pc = (Patient_Case__c)stdController.getRecord();
                loadcgnote();    
                isPC = true;
            }else{
                trans = (Transplant__c)stdController.getRecord();
                loadcaregiverData();
            }
        }
        loadProData();
        
    }
    

}