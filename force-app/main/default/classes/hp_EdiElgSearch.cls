public with sharing class hp_EdiElgSearch extends hp_callWebApp{
    
    public hpEdiElgStruct ediEligResult = new hpEdiElgStruct();
    
     public static hpEdiElgStruct.hpEdiElg searchEDI(string ssn, string essn, string patientType){
        
        hp_EdiElgSearch searchEDI = new hp_EdiElgSearch();
        searchEDI.searchEDIGate(ssn, essn, patientType);
        
        if(searchEDI.ediEligResult.Results.isempty()){
            return null;
        }
        return searchEDI.ediEligResult.Results[0];
        
    }
    
    public hpEdiElgStruct searchEDIGate(string ssn, string essn, string patientType){
        
        endpoint='elig/detail';
        string pt = patientType;
        search.add('ssn:'+ssn);
        search.add('essn:'+essn);
        if(patientType!='Patient is Insured'){
           pt='dependent';
        }
        
        search.add('patienttype:'+pt);
        
        jsonBody = jb.eligSearch(search);
        if(Test.IsRunningTest()){
           result = '{"ResultCode":"2","ErrorMsg":"","Results":[{"Id":0,"PatientType":"employee","Gender":"F","Ssn":"999999999","EssnHolder":{"String":"","Valid":false},"Essn":"","Dob":"19690605","Firstname":"JANE","Lastname":"DOE","Address1":"123 E Main St","Address2":"","City":"FORT SMITH","State":"AR","Zip":"72903","Phone":"","EffectiveDate":"20180101","PaidThroughDate":"","PaidThroughDateHolder":{"String":"","Valid":true},"TermDate":"","CoverageType":"MM","EmploymentStatusHolder":{"String":"","Valid":true},"EmploymentStatus":"","HireDateHolder":{"String":"19890710","Valid":true},"HireDate":"19890710","PlanCode":"","Bid":"12345678W","PatientId":0,"FamilyStatusHolder":{"String":"","Valid":true},"FamilyStatus":"","Relationship":"18","Plancode":"BLUE ADVANTAGE - HRA 1750","CoverageLevel":"EMP"}]}';
        }else{   
            result = callWebApp(null);
        }
        result = result.replaceAll('Plancode', 'Plncode');
        ediEligResult = (hpEdiElgStruct)JSON.deserialize(result, hpEdiElgStruct.class); 
        
        if(ediEligResult.Results ==null){
           ediEligResult.Results = new hpEdiElgStruct.hpEdiElg[]{}; 
        }
        
        ediEligResult.cleanUp();
        
        return ediEligResult;
        
    }    
    
    
}