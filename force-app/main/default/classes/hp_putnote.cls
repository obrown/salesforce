public inherited sharing class hp_putnote extends hp_callWebApp{

    public hpRxSearchStruct returnObj = new hpRxSearchStruct();
    
    public static hpRxSearchStruct putnote(string uw, string essn, string grp, string seq, string noteType, string username, string noteBody, string episodenumber){
        
        hp_putnote putnote = new hp_putnote();
        putnote.putthenote(uw, essn, grp, seq, noteType, username, noteBody, episodenumber);
        return putnote.returnObj;
        
    }
    
    public hpRxSearchStruct putthenote(string uw, string essn, string grp, string seq, string noteType, string username, string noteBody, string episodenumber){
        endpoint = 'putnote';
        
        string c_username = username;
        c_username = c_username.touppercase();
        c_username = c_username.replace('NULL ','');
        string c_uw = uw;
        string c_essn = essn;
        string c_seq = seq;
        string c_grp = grp;
        string c_episodenumber = episodenumber;
        
        if(uw==null){c_uw ='';}
        if(essn==null){c_essn='';}
        if(seq==null){c_seq='';}
        if(grp==null){c_grp='';}
        if(episodenumber==null){c_episodenumber='';}
        
        search.add('Essn:'+c_essn);
        search.add('Underwriter:'+c_uw);
        search.add('Group:'+c_grp);
        search.add('NoteType:'+noteType);
        search.add('Noteusername:'+c_username);
        search.add('Notebody:'+noteBody.touppercase());
        search.add('Seq:'+c_seq);
        search.add('EpisodeNumber:'+ c_episodenumber);
        
        jsonBody = jb.eligSearch(search);
        
        result = callWebApp(null);
        if(result != null){
            result = result.replaceAll('":"','__c":"');
            result = result.replace('ResultCode__c','ResultCode');
            result = result.replace('ErrorReason__c','ErrorReason');
        }
         try{
            returnObj = (hpRxSearchStruct)JSON.deserialize(result, hpRxSearchStruct.class);
            
        }catch(exception e){
            system.debug(e.getMessage());
        }
        
        
        return returnObj;
    }
    
    
    
}