public with sharing class caseAttachments {

    Attachment[] Attachments; 
    ftpAttachment__c [] ftpAttachments;
    
    final id relatedID;
    
    public caseAttachments(id relatedID){
        this.relatedID = relatedID;
        
    }
    
    public void setftpAttachments(ftpAttachment__c [] ftpAttachments){
        this.ftpAttachments =ftpAttachments;
    }
    
    public caseAttachments(ApexPages.StandardController controller){
        this.relatedID = controller.getid();
        getAttachments();
    }
    
    public ftpAttachment__c[] getftpAttachments(){
        
        if(ftpAttachments == null){
            ftpAttachments= [Select  subDirectory__c,fileName__c,LastModifiedDate,CreatedBy.Name from ftpAttachment__c where patient_case__c != null and patient_case__c = :relatedID order by createdDate desc];
        }
        
        return ftpAttachments;
    }
    
    public Attachment[] getAttachments(){
        
        if(Attachments == null){
            Attachments = [Select  BodyLength,Name,LastModifiedDate,CreatedBy.Name from Attachment where parentID != null and parentID = :relatedID order by createdDate desc];
        }
        
        return Attachments;
    }
    
    public void DeleteFTPAttach(string attachID){
        for(ftpAttachment__c ftpa : ftpAttachments){
            if(ftpa.id==attachId){
                delete ftpa;
                break;
            }
        }
        
        
    }
    
    public void DeleteAttach(string attachID){
        Attachment tobeDeleted = null;
        // if for any reason we are missing the reference
        if (attachID == null) {
            return;
        }else{
            
            if(attachID.left(3)=='00P'){     
                // find the Task record within the collection
                
                for(Attachment t : Attachments){
                    if (t.Id == attachID) {
                        tobeDeleted = t;
                        break;
                    }
                }
                
                
                //if task record found delete it
                if (tobeDeleted != null) {
                    Delete tobeDeleted;
                }
                
            }else{    
                /*system.debug(ntID + ' ntID');
                ENZ__FTPAttachment__c tobeDeleted = null;
                for(ENZ__FTPAttachment__c t : ftpattachments){
                    if (t.Id == ntid) {
                        tobeDeleted = t;
                        break;
                    }
                }*/
                
                //if task record found delete it
                //if (tobeDeleted != null) {
                //    Delete tobeDeleted;
                //}
            }
            
            list<Program_Notes__c> notestoupdate = new list<Program_Notes__c>();
            notestoupdate = [select attachDeleted__c,attachID__c,isAuthEmailAttach__c,attachment__c from Program_Notes__c where attachID__c = :attachID];
            
            for(Program_Notes__c n: notestoupdate){
                n.attachID__c = null;
                n.isAuthEmailAttach__c =false;
                n.attachment__c = false;
                n.attachDeleted__c = true;
            }
            
            if(!notestoupdate.isEmpty()){
                update notestoupdate;
            }
            
            
            
        }   
    }
    
    public boolean decryptAttach(string attachID, blob key){
        
        if(attachID == null || attachID.left(3)=='a0M'){
            return false;
        }else{
            try{
                Attachment a = [select body from attachment where ID = :id.valueof(attachID)];
                encryptAttach ea = new encryptAttach(key);
                blob decryptedbody;
                decryptedbody= ea.decryptAttachmentBlob(a.body);
                a.body=decryptedbody;
                update a;
            }catch(exception e){
                system.debug(e.getMessage() +' '+e.getLineNumber());
                return false;
            }
        }
        return true;
    }
    
    public boolean encryptAttach(string attachID, blob key){
        try{
        
            Attachment a = [select body from attachment where ID = :id.valueof(attachID)];
            encryptAttach ea = new encryptAttach(key);
            blob thebody= a.body;
            blob encryptedbody;
            encryptedbody=ea.encryptAttachmentBlob(thebody);
            a.body=encryptedbody;
            update a;
            
        }catch(exception e){
            system.debug(e.getMessage() +' '+e.getLineNumber());
            return false;
        }
        return true;
    }    

}