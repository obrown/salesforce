public with sharing class LetterController {

	id theID;
	public Patient_Case__c pc {get;set;}
	public Letters__c Letter {get;set;}
	public string pcNumber {get;set;}
	public boolean isError {get;set;}
	public string message {get;set;}
    public void save() {

		try{
    		insert Letter;
    		isError = false;
		}catch (exception e){
			isError = true;
			if(e.getMessage().contains('_EXCEPTION')){message = utilities.errorMessageText(e.getMessage());}else{message = 'REQUIRED FIELD MISSING: ' + utilities.errorMessageTextRF(e.getMessage());}
		}
        
    }

	public pagereference cancel(){
		PageReference pageRef = new PageReference('/apex/caseDetail?id='+pc.id);
		return pageRef; 
		
	}

    public LetterController() {
    	theID=ApexPages.currentPage().getParameters().get('pcid');
    	if(theID!=null){
    		pc = [select patient_case__c, name, id from Patient_Case__c where id = :theID];
    		main();
    	}
	   
    }
			 
    public LetterController(ApexPages.StandardController controller) {
    	pc = (Patient_Case__c)controller.getRecord();
    	if(pc!=null){
    		main();
    	}

	
    }
    
    private void main(){
    	
    	Letter = new Letters__c();
    	isError=false;
    	pcNumber = pc.Patient_Case__c;
    	letter.Patient_Case__c = pc.id;
    }


}