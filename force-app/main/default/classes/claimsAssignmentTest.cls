/**
* This class contains unit tests for validating the behavior of  the Apex claimsAssignment class
*/

@isTest
private class claimsAssignmentTest {

    static testMethod void claimsAssignment() {
        
        //Test the insert and update workflow
        
        customer_care__c cc = new customer_care__c();
        insert cc;
        
        customer_care_Inquiry__c cci = new customer_care_Inquiry__c(customer_care__c=cc.id, Inquiry_Status__c='Open',Inquiry_Work_Task__c='');
        cci.work_department__c ='Claims';
        cci.Inquiry_Work_Task__c ='Anthem Aged Claims';
        cci.Inquiry_Reason__c = 'Accident Details';
        insert cci;
        
        cci.work_department__c ='';
        update cci;
        
        cci.work_department__c ='Claims';
        update cci;
        
        cci.anthem_aged_claim__c =true;
        update cci;
        
        cci.assigned__c=null;
        update cci;
        
        Claims_Assignment_Rule__c car = new Claims_Assignment_Rule__c(name='Unit Test',Priority_Number__c=1,Processor_List__c='Unit Test',Active__c=true);
        insert car;
        
        Claims_Assignment_Criteria__c cac0 = new Claims_Assignment_Criteria__c(Claims_Assignment_Rule__c=car.id, condition__c='Anthem Aged Claims', field__c='Inquiry_Work_Task__c');
        Claims_Assignment_Criteria__c cac1 = new Claims_Assignment_Criteria__c(Claims_Assignment_Rule__c=car.id, condition__c='Anthem Aged Claims', field__c='Inquiry_Work_Task__c');
        
        insert cac0;
        insert cac1;
        
        claimsAssignment ca = new claimsAssignment('Claims', 'Claims');
        ca.setList(new customer_care_Inquiry__c[]{cci}, 'Claims', 'Claims');
        
        delete cac1;
        
        car.Processor_List__c='Unit Test,Unit Test';
        update car;
        
        ca = new claimsAssignment('Claims', 'Claims');
        ca.setList(new customer_care_Inquiry__c[]{cci}, 'Claims', 'Claims');
           
    }
    
}