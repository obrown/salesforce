public without sharing class claimAuditFuture{

    @future
    public static void setClaimAuditProcessor(set<id> theIDs){
        Claim_Audit__c[] caList = [select Claim__r.Claims_Processor_Crosswalk__c, Claim_Processor__c from Claim_Audit__c where id in :theIDs];
    
        for(Claim_Audit__c c : caList){
            system.debug(c.Claim__r.Claims_Processor_Crosswalk__c);
            c.Claim_Processor__c = c.Claim__r.Claims_Processor_Crosswalk__c;   
        }
        
        update caList;
    }
    
    @future
    public static void setClaimAuditAuditor(set<id> theIDs, set<id> clients){
        
        set<id> clientSet = new set<id>();
        Claim_Audit__c[] cUpdateList = new Claim_Audit__c[]{};
        map<string, id> auditorMap = new map<string, id>();
        system.debug(clients);
        //where Claims_Client__c in :clients
        for(Claims_Auditor__c ca : [select Audit_Type__c,Claims_Client__c from Claims_Auditor__c ]){
            auditorMap.put(ca.Claims_Client__c+ca.Audit_Type__c, ca.id);
        }
        
        for(Claim_Audit__c ca : [select claim__r.client__c, Claim_Auditor__c, Audit_Type__c from Claim_Audit__c where id in :theIDS]){
            if(ca.Audit_Type__c!='High Dollar'){
                ca.Claim_Auditor__c = auditorMap.get(ca.claim__r.client__c+ca.Audit_Type__c );
                cUpdateList.add(ca);
            }
        }
        
       
        update cUpdateList;
        
        
    }
    
    @future
    public static void updateClaimAuditName(set<id> theIDs){
    
        Claim_Audit__c[] toBeUpdated = new Claim_Audit__c[]{};
        map<id, caClaim__c> claimMap = new map<id, caClaim__c>([select Employee_First_Name__c,
                                                                                       Employee_Last_Name__c,
                                                                                       Patient_Age__c,
                                                                                       Patient_First_Name__c,
                                                                                       Patient_Last_Name__c,
                                                                                       Relationship_to_Member__c,
                                                                                       Number_of_lines__c from caClaim__c where id IN :theIDs]);
        
        
        
        for(Claim_Audit__c ca : [select Claim__c,
                                        Employee_Name__c,
                                        Patient_Name__c,
                                        Patient_Age__c,
                                        Relationship_to_Member__c,
                                        Number_of_lines__c from Claim_Audit__c where claim__c in :theIDs]){
                                        
            Claim_Audit__c foo = new Claim_Audit__c(id=ca.id);
            
            if(claimMap.get(ca.claim__c).Employee_Last_Name__c!=null){
                foo.Employee_Name__c = claimMap.get(ca.claim__c).Employee_First_Name__c + ' ' + claimMap.get(ca.claim__c).Employee_Last_Name__c;
            }else{
                foo.Employee_Name__c = null;
            }
            
            if(claimMap.get(ca.claim__c).Patient_Last_Name__c!=null){
                foo.Patient_Name__c= claimMap.get(ca.claim__c).Patient_First_Name__c + ' ' + claimMap.get(ca.claim__c).Patient_Last_Name__c;
            }else{
                foo.Patient_Name__c= null;
            }
            
            foo.Patient_Age__c= claimMap.get(ca.claim__c).Patient_Age__c;
            foo.Relationship_to_Member__c = claimMap.get(ca.claim__c).Relationship_to_Member__c;
            
            if(claimMap.get(ca.claim__c).Number_of_lines__c != null){
                foo.Number_of_lines__c = claimMap.get(ca.claim__c).Number_of_lines__c;
            }
            
            toBeUpdated.add(foo);
        }    
        
        update toBeUpdated;
    
    }
    
    @future
    public static void setClaimAuditCount(set<id> claims){
        
        //Create Audits
        Claim_Audit__c[] caList = new Claim_Audit__c[]{};
        set<string> clients = new set<string>();
        date datePaid = date.today();
        for(caClaim__c c : [select Claims_Processor_Crosswalk__c, id,Client__c,date_paid__c from caClaim__c  where id in :claims]){
            Claim_Audit__c ca = new Claim_Audit__c();
            ca.Claim__c = c.id;
            ca.Claim_Processor__c = c.Claims_Processor_Crosswalk__c;
            ca.Audit_Type__c= 'Random Selection';
            ca.Audit_Status__c = 'To Be Audited';
            if(datePaid > c.Date_Paid__c){
                datePaid=c.Date_Paid__c;
            }
            
            caList.add(ca);
            clients.add(c.client__c);    
        }
        
        database.insert(caList, false);
  

    }
    
    @future
    public static void setClaimProcessedCount(set<id> claimsProcessed){
         
         map<id, claimsProcessed> clientMap = new map<id, claimsProcessed>();
         map<string, integer> clmCount = new map<string, integer>(); //cpId
         
         date minDate, maxDate;
         set<id> clientSet = new set<id>();
         
         for(caClaims_Processed__c c : [select month_paid__c,client__c from caClaims_Processed__c where id in :claimsProcessed]){
             claimsProcessed cp= new claimsProcessed();
             
             cp.clientId = c.client__c;
             cp.startDate = c.month_paid__c;
             cp.endDate = Date.newInstance(c.month_paid__c.year(), c.month_paid__c.month(),Date.daysInMonth(c.month_paid__c.year(), c.month_paid__c.month())); 
             cp.cpRecord=c;
             
             if(minDate==null || minDate>c.month_paid__c){
                 minDate=c.month_paid__c;
             }
             
             if(maxDate==null || maxDate<cp.endDate){
                 maxDate=cp.endDate;
             }
             
             clientSet.add(c.client__c);
             clientMap.put(c.client__c, cp);
            
         }
         
         for(caClaim__c ca :[select Client__c,Date_Paid__c from caClaim__c where Client__c in :clientMap.keySet() and Date_Paid__c >= :minDate and Date_Paid__c <= :maxDate] ){
             
             string key = clientMap.get(ca.client__c).cpRecord.id+':'+ca.date_paid__c.month()+''+ca.date_paid__c.year();
             system.debug(key);
             integer count = clmCount.get(key);
             system.debug(count);
             if(count==null){count=0;} 
             claimsProcessed cp = clientMap.get(ca.client__c);
             
             system.debug(ca.Date_Paid__c);
             system.debug(cp.startDate);
             system.debug(cp.endDate);
             
             if(cp.startDate<=ca.Date_Paid__c  && cp.endDate>=ca.Date_Paid__c ){
                 count++;
                 clmCount.put(key, count);
             }   
         
         }
         
         caClaims_Processed__c[] records = new caClaims_Processed__c[]{};
         
         for(string k : clmCount.keyset()){
             records.add(new caClaims_Processed__c(id=k.left(k.indexof(':')),Claims_Audited__c=clmCount.get(k)));
         
         }
         
         upsert records;
         
            
    }
    
    @future(callout=true)
    public static void sendClaimAuditErrors(set<id> theIDs, boolean isPlan){
        Claim_Audit__c[] cacList = new Claim_Audit__c[]{};
        Claim_Audit_Form__c[] cafList = new Claim_Audit_Form__c[]{};
        //map<id, id> ceMap = new map<id, id>();
        Messaging.SingleEmailMessage[] mailRoom = new Messaging.SingleEmailMessage[]{};
        map<id, blob> blobMap = new map<id, blob>();
        
        //Plan build email
        string planbuildEmail = [select Email_Address__c from Claims_Processor_Crosswalk__c where name = 'PlanBuild' limit 1].Email_Address__c ;
        
        for(Claim_Audit__c c : [select id, name,
                                       Audit_Form_Date__c,
                                       Audit_Status__c,
                                       Comments__c,
                                       Claim__r.Group_Number__c,
                                       Claim__r.Claim_Number__c,
                                       Claim__r.Source_Type__c,
                                       Claim_Auditor__r.User__r.firstName,
                                       Claim_Auditor__r.User__r.lastName,
                                       Claim_Auditor__r.User__r.name,
                                       Claim_Auditor__r.User__r.email,
                                       WA__c,
                                       WA__r.User__r.email,
                                       Claim_Processor__r.name,
                                       Claim_Processor__r.Email_Address__c,
                                       Client__c from Claim_Audit__c where id in :theIDs]){
            try{
                
                Claim_Audit_Form__c caf = new Claim_Audit_Form__c(claim_audit__c=c.id, name=c.name +'_AuditForm' + '.doc');
                cafList.add(caf);
                
                c.Audit_Status__c = 'Error Review';
                cacList.add(c);
                
             }catch(exception e){
                 system.debug('****************************************** ' + e.getMessage() + ' ******************************************');
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+', '+e.getlinenumber()));
             } 
        }

        /* This loop makes use of the getContent() method. In Summer '15 a change was made so that DML statements cannot fire before getContent method */
        blob aBlob;
        
        for(Claim_Audit__c c : cacList){
            
            PageReference auditForm = new ApexPages.StandardController(c).view();
            auditForm = page.auditForm;
            auditForm.getParameters().put('id',c.ID);
            auditForm.getParameters().put('isPlan', string.valueof(isPlan));
            
            if(!Test.isRunningtest()){
                aBlob = auditForm.getContent();
            }else{
                aBlob = blob.valueof('1234');
            }
            
            blobMap.put(c.id, aBlob);
            
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                                   
            //Determine Recipients
            
            if(utilities.isRunningInSandbox()){
                string[] sendemailto = new string[]{};
                sendemailto.add(userInfo.getUserEmail());
                mail.setToAddresses(sendemailto);
                  
                string[] ccList = new string[]{};
                ccList.add(userInfo.getUserEmail());
                mail.setCCAddresses(ccList);
                               
            }else{
                
                string[] sendemailto = new string[]{};
                
                if(isPlan){
                
                    if(planbuildEmail != null && planbuildEmail != ''){
                        sendemailto.add(planbuildEmail);
                        
                    }else{
                        sendemailto.add(userInfo.getUserEmail());
            
                    }
                
                }else{
                    
                    if(c.Claim_Processor__c == null){
                        sendemailto.add(userInfo.getUserEmail());
                    }else{
                        sendemailto.add(c.Claim_Processor__r.Email_Address__c);
                        
                    }
                        
                }
                
                mail.setToAddresses(sendemailto);
                mail.setCCAddresses(new string[]{'auditnotification@hdplus.com'});
                
            }
                
            mail.setSubject(c.Name+' Audit form needs review and response');
            mail.setPlainTextBody('Hello,\n\nPlease review the attached ' +c.Name+ ' Audit Form and make any necessary corrections.\n\nWhen completed forward to all and indicate in the body of the email that the corrections have been made. Should you disagree with the audit instructions-please forward to all and describe your reasons for disagreeing with the auditor.\n\nThere is a closed loop process in place-so please remember to respond to all audit notices so the claims can be properly closed in the audit database. IF this does not occur-you could be receiving second request notice.\n\nThanks!');
            
            if(c.WA__c==null){
                mail.setReplyTo(c.Claim_Auditor__r.User__r.email);
            }else{
                mail.setReplyTo(c.wa__r.User__r.email);
            }
            mail.setSenderDisplayName('Audit Form');
                
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            efa.setFileName(c.name +'_AuditForm' + '.doc');
            efa.setBody(aBlob);
                
            mail.setFileAttachments(new Messaging.EmailFileAttachment[]{efa});
            mailRoom.add(mail);         
            
        }
        
        insert cafList;
        update cacList;
        
        Attachment[] aList = new Attachment[]{};
        
        for(Claim_Audit_Form__c caf : cafList){
            
            //Create Attachment
            Attachment theform = new Attachment(parentId = caf.id, name=caf.name, body = blobMap.get(caf.claim_audit__c)); 
            aList.add(theform);
            
        }
        
        
        insert aList;
        
        for(Attachment a : aList){
        
            for(Claim_Audit_Form__c  c : cafList){
                if(a.name == c.name){
                    string fileURL = URL.getSalesforceBaseUrl().toExternalForm();
                    fileURL += '/servlet/servlet.FileDownload?file=' + a.id;
                    c.attachment_link__c = fileURL;
                    break;
                }
            }
            
        }
        
        update cafList;
        
        //Send E-mail
        integer esize = mailRoom.size();
        integer batchSize = 10;
        
        for(integer i=0; i<esize; i=i+batchSize){
            
            Messaging.SingleEmailMessage[] fooMailList = new Messaging.SingleEmailMessage[]{};
            integer upper = i+batchSize;
            if(upper>eSize){upper=eSize;}
            
            for(integer x=i; x<upper;x++){
                Messaging.SingleEmailMessage mail = mailroom[x];
                fooMailList.add(mail);
            }
            
            Messaging.sendEmail(fooMailList);    
        }
        
                
    }

}