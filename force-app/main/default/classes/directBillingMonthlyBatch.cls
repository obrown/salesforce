public with sharing class directBillingMonthlyBatch{
    
    map<id, date> invoiceMap = new map<id, date>();
    date invoiceEndDate=date.today().addMonths(1).toStartofMonth().addDays(-1);
    map<string, string> empName = new map<string, string>();
    map<string, string> empAddy1 = new map<string, string>();
    map<string, string> empAddy2 = new map<string, string>();
    
    public directBillingMonthlyBatch(){}
        
    public void run(){
    
        date invoiceMonth = date.today();
        invoiceMonth = date.valueof(invoiceMonth.year()+'-'+invoiceMonth.month()+'-01');
        for(Direct_Billing_Leave__c leave : [select id,end_date__c,start_date__c from Direct_Billing_Leave__c where createdDate >:date.valueof('2019-01-01') and (Last_Invoiced_Date__c=null or (start_date__c <= :invoiceEndDate and (end_date__c >= :invoiceMonth or end_date__c=null)))]){
            if(leave.end_date__c!=null && leave.end_date__c>=invoiceEndDate){
                invoiceMap.put(leave.id, invoiceEndDate);
            }else{
                invoiceMap.put(leave.id, leave.end_date__c);
            }
        }
        
        //Create the invoice records
        set<id> inv =directBillingInvoiceWorkflow.createInvoices(invoiceMap);
        
        directBillingWorkflow.pastDueLetters(invoiceMap, invoiceMonth);
        
        string[] sendEmailTo = new string[]{};
        if(utilities.isRunningInSandbox() || Test.isRunningTest()){
            sendEmailTo.add(UserInfo.getUserEmail());
        }else{
            sendEmailTo.add(DirectBillingCustomSettings__c.getInstance().batchInvoiceEmail__c);
        }
        
        string emailURL = Url.getSalesforceBaseUrl().toExternalForm().replace('http:', 'https:')+'/apex/directBillingBatchInvoice';
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setSubject(string.valueof(date.today().month())+'/'+string.valueof(date.today().year())+' Direct Billing Invoices');
        mail.setHTMLBody('Please follow the link below to print or download the invoices for '+string.valueof(date.today().month())+'/'+string.valueof(date.today().year()) +'<br/><br/> <a href="'+emailURL +'">'+ string.valueof(date.today().month())+'/'+string.valueof(date.today().year()) +' Direct Billing Invoices</a>' );
        mail.setToAddresses(sendEmailTo);
        
        Messaging.sendEmail(new messaging.Singleemailmessage[]{mail});
        
        
        
    }
    
}