@isTest(seealldata=true)
private class coePatientMessagingTest {
    
  static testMethod void messaging() {
        client_facility__c[] cfs = [select client__r.name, client__c, procedure__c, procedure__r.name from client_facility__c];
        
        map<string, string> cfIdMap = new map<string, string>();
        map<string, string> procedureIdMap = new map<string, string>();
        
        for(client_facility__c cf : cfs){
            cfIdMap.put(cf.client__r.name, cf.client__c);
            procedureIdMap.put(cf.procedure__r.name, cf.procedure__c);
        }
         
        Patient_Case__c pc = new Patient_Case__c();
        
        ApexPages.currentPage().getParameters().put('client',cfIdMap.get('Walmart'));
        ApexPages.currentPage().getParameters().put('procedure',procedureIdMap.get('Joint'));
        pc.Employee_First_Name__c  ='John';pc.Employee_Last_Name__c  ='Smith';
        caseController cc = new caseController();
        
        cc.obj.status__c= 'Open';
        cc.obj.status_reason__c= 'In Process';
        
        cc.inlineSave();
        system.assert(cc.message!='');
        
        cc.obj.BID__c = '123456';
        cc.obj.BID_id__c = '123456';
        cc.obj.Language_Spoken__c = 'English';
        cc.obj.Initial_Call_Discussion__c = 'The ICD';
        cc.obj.Relationship_to_the_Insured__c = 'Spouse';
        cc.obj.Caller_relationship_to_the_patient__c='Spouse';
        cc.obj.caller_name__c='John Doe';
        
        cc.obj.Employee_first_name__c = 'John';
        cc.obj.Employee_last_name__c = 'Smith';
        cc.obj.employee_dob__c = date.valueof('1980-01-01');
        cc.obj.Employee_Gender__c = 'Male';
        cc.obj.employee_ssn__c = '123456789';
        cc.obj.employee_street__c = '123 E Main St';
        cc.obj.Employee_City__c = 'Mesa';
        cc.obj.Employee_State__c = 'AZ';
        cc.obj.Employee_Zip_Postal_Code__c = '85297';
        cc.obj.Employee_Country__c = 'USA';
        
        cc.obj.Patient_First_Name__c = 'Jane';
        cc.obj.Patient_last_name__c = 'Smith';
        cc.obj.Patient_dob__c = date.valueof('1981-01-01');
        cc.obj.Patient_Gender__c = 'Female';
        cc.obj.Patient_ssn__c = '987654321';
        cc.obj.Patient_Email_Address__c = 'none';
        cc.obj.Patient_street__c = '123 E Main St';
        cc.obj.Patient_City__c = 'Mesa';
        cc.obj.Patient_State__c = 'AZ';
        cc.obj.Patient_Zip_Postal_Code__c = '85297';
        cc.obj.Patient_Country__c = 'USA';
        
        cc.obj.Same_as_Employee_Address__c = true;
        
        
        cc.obj.Employee_Home_Phone__c = '(480) 555-1212';
        cc.obj.Employee_Mobile__c = '(480) 555-1212';
        cc.obj.Employee_Work_Phone__c = '(480) 555-1212';
        
        cc.obj.Patient_Home_Phone__c = '(480) 555-1212';
        cc.obj.Patient_Mobile__c = '(480) 555-1212';
        cc.obj.Patient_Work_Phone__c = '(480) 555-1212';
        
        cc.obj.CallBack_Number__c = '(480) 555-1212';
        cc.obj.Caregiver_DOBe__c = '1982-01-01';
        
        cc.obj.Listed_as_Program_Covered_Procedure__c = 'Yes';
        cc.obj.What_procedure_was_recommended__c = 'Testing';
        cc.obj.Carrier_Name__c = 'Aetna';
        cc.obj.attachEncrypt__c = EncodingUtil.base64encode(Crypto.generateAesKey(128));
        
        cc.inlineSave();
        
        system.assert(cc.obj.id!=null);
        
        ECEN_Message__c message = new ECEN_Message__c();
        message.patient_case__c = cc.obj.id;
        
        message.body__c = 'UNIT TEST';
        message.subject__c = 'UNIT TEST';
        message.Sender_Email__c= 'UNIT.TEST@test.com';
        message.Sender_Name__c= 'UNIT TEST';
        message.Insurance_Id__c = '123456';
        
        insert message;
        
        coePatientMessages patientMessages = new coePatientMessages('123456', cc.obj.id, cc.obj.patient_first_name__c+' '+cc.obj.patient_last_name__c,cc.obj.patient_email_address__c,'patient_case__c');
        
        ApexPages.currentPage().getParameters().put('pmid',message.id);
        patientMessages.loadMessageVF();
        
        
        patientMessages.newMessageVF();
        
        patientMessages.message.body__c = 'UNIT TEST';
        message.subject__c = 'UNIT TEST';
        patientMessages.message.Sender_Email__c= 'UNIT.TEST@test.com';
        patientMessages.message.Sender_Name__c= 'UNIT TEST';
        patientMessages.message.Insurance_Id__c = '123456';
        
        
        patientMessages.saveMessageVF();
        
        ApexPages.currentPage().getParameters().put('pmid',message.id);
        
        patientMessages.message.body__c = 'UNIT TEST';
        message.subject__c = 'UNIT TEST';
        patientMessages.message.Sender_Email__c= 'UNIT.TEST@test.com';
        patientMessages.message.Sender_Name__c= 'UNIT TEST';
        patientMessages.message.Insurance_Id__c = '123456';
        patientMessages.message.dateSent__c = datetime.now();
        patientMessages.replyToMessageVF();
        
  }
  
}