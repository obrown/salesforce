/**
* This class contains unit tests for validating the behavior of the AMS application
*/

@isTest
private class amsTest {

    static testMethod void UnitTest() {
        
        Client__c client = new Client__c(active__c = true, name='Unit Test', Underwriter__c='008');
        insert client; 
        
        AmsController ctrl = new AmsController();
        ctrl.error.Client__c = client.id;
        ctrl.error.Dispensed_Date__c = date.today().addDays(-30);
        
        ctrl.error.Employee_SSN__c = '555746607';
        ctrl.saveError();
        
        ctrl.error.Employee_SSN__c = '555746607';
        ctrl.error.Member_Name__c = 'Michael Newbeck';
        ctrl.error.Underwriter__c = '008';
        ctrl.saveError();
              
        ctrl = new AmsController(ctrl.error.id);
        
        ctrl.newWorkTask();
        ctrl.workTask.Work_Task__c='Supervisor Accum Recalc';
        ctrl.workTask.Work_Department__c = 'Supervisor';
        ctrl.saveWorkTask();
        
        ApexPages.CurrentPage().getParameters().put('wtID', ctrl.workTask.id);
        ctrl.getWorkTaskVF();
        
        ctrl.newNote();
        ctrl.note.Notes__c = 'Unit.Test';
        ctrl.saveNote();
        
        ctrl.newNote();
        ctrl.note.Notes__c = 'Unit.Test';
        ctrl.note.Write_to_Healthpac_EE__c = true;
        ctrl.note.Write_to_Healthpac_Mbr__c= true;
        
        ctrl.saveNote();
        
        ApexPages.CurrentPage().getParameters().put('wtID', ctrl.workTask.id);
        ctrl.deleteWorkTask();
        
        ctrl.loaduserList();
        ctrl.changeOwner();
        
        magpie_Error__c[] meList = new magpie_Error__c[]{};
        
        for(integer x=0;x<120;x++){
            magpie_Error__c me = new magpie_error__c();
            me.Client__c = client.id;
            me.Employee_SSN__c = '555746607';
            me.Underwriter__c = '008';
            meList.add(me);
        
        }
        
        insert meList;
        
        ApexPages.CurrentPage().getParameters().put('id', ctrl.error.id);
        
        ctrl = new AmsController();
        ctrl.saveError();
        
        ApexPages.CurrentPage().getParameters().put('st', 'Newbeck');
        AmsSearchController searcher = new AmsSearchController();  
        
        ApexPages.CurrentPage().getParameters().put('st', 'unittest');
        searcher = new AmsSearchController();
        
        ApexPages.CurrentPage().getParameters().put('openId', ctrl.error.id);
        searcher.openCase();
          
    }

}