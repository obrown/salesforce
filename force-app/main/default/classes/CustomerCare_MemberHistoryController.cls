public with sharing class CustomerCare_MemberHistoryController{

    public map<string, string> clientMap {get; private set;}
    public set<string> keys { get; set; } 
    public integer numberofintakes { get; set; } 
    public boolean HasIntakes { get; set; }
    Public integer InquiryCount { get; set; }
    Public integer InquiryNum { get; set; }
    public map<string, inquiriesWrapper> intakeMap {get; private set;}
    
    string searchTerms, currentIntake;
    
    public integer recordsFound {get; set;}
    public string patName{get;set;}
    
    public void searchByIntake (string searchTerms , string currentIntake){
        this.searchTerms = searchTerms;
        this.currentIntake = currentIntake;
        init();
    }
      
    public class inquiriesWrapper{
        public inquiriesWrapper(customer_care__c intake){
            inquiries = new customer_care_inquiry__c[]{};
            this.intake = intake;
        }
        public customer_care_inquiry__c[] inquiries { get; set; }
        public customer_care__c intake { get; set; }
        public boolean getlastitem() { return inquiries.isEmpty();}
        Public integer geticount() {return inquiries.size();}
       
    }
    
    void init(){
        numberofintakes=0;
        HasIntakes =false;
        InquiryNum=0; 
        intakeMap = new map<string, inquiriesWrapper>();
        intakeSearch is = new intakeSearch(searchTerms, 'eSSN');
        is.search();
        clientMap = is.clientMap;
        keys = new set<string>();
        recordsFound=is.ccList.size();
        for(customer_care__c intake : is.cclist){
            if(intake.name != currentIntake){
                intakeMap.put(intake.id, new inquiriesWrapper(intake)); 
                keys.add(intake.id); 
                numberofintakes++;
                HasIntakes =true;
            }
            
        }
        
        for(customer_care_inquiry__c inquiry: is.ccilist){
            if(inquiry.customer_care__r.name != currentIntake){
            
            inquiriesWrapper iw = intakeMap.get(inquiry.customer_care__c);
            InquiryNum++;
            inquiry.Inquiry_Number__c = InquiryNum;
            iw.inquiries.add(inquiry);
            intakeMap.put(inquiry.customer_care__c, iw);
            keys = intakeMap.keyset();
            }
            
            
        }
       
    }
}