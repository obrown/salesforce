@isTest
private class ewtselectlistTest {

    static testMethod void ewtselectlistTest() {
        
        /* begin setup */
        
        ewtPicklistSetupController epsc = new ewtPicklistSetupController();
        epsc.setDocumentTypeToUpdate();
        epsc.documentType.document_type__c='Unit Test';
        epsc.documentType.Description__c='Unit Test';
        epsc.documentType.HP_Document_Type__c='UT';
        epsc.documentType.Folder_Location__c='unit test';
        
        epsc.saveDocumentType();
        system.assert(epsc.documentType.id!=null);
        
        ApexPages.CurrentPage().getParameters().put('documentTypeId',epsc.documentType.id);
        epsc.esl.documentTypeValue = epsc.documentType.id;
        string documentTypeId = epsc.documentType.id;
        
        epsc.workTaskName= 'Unit Test Work Task';
        epsc.saveWorkTask();
        epsc.workTaskId = epsc.esl.workTask[1].getValue();
        system.assert(epsc.workTaskId!=null);
        epsc.esl.workTaskValue =epsc.workTaskId;
        string workTaskId = epsc.workTaskId;
        
        ApexPages.CurrentPage().getParameters().put('workTaskId',epsc.workTaskId);
        epsc.workTaskReasonName= 'Unit Test Work Task Reason';
        epsc.saveWorkTaskReason();
        epsc.workTaskReasonId = epsc.esl.workTaskReason[1].getValue();
        system.assert(epsc.workTaskReasonId !=null);
        epsc.esl.workTaskReasonValue=epsc.workTaskReasonId ;
        string workTaskReasonId = epsc.workTaskReasonId;
        
        ApexPages.CurrentPage().getParameters().put('workTaskReasonId',epsc.workTaskReasonId);
        epsc.workDepartmentName= 'Unit Test Work Department';
        epsc.saveWorkDepartment();
        epsc.workDepartmentId = epsc.esl.workDepartment[1].getValue();
        system.assert(epsc.workDepartmentId !=null);
        epsc.esl.workDepartmentValue=epsc.workDepartmentId;
        string workDepartmentId = epsc.workDepartmentId;
        
        epsc.closedReasonName= 'Unit Test Closed Reason';
        epsc.saveClosedReason();
        epsc.closedReasonId = epsc.esl.closedReason[1].getValue();
        system.assert(epsc.closedReasonId!=''); 
        string closedReasonId= epsc.closedReasonId;
        system.debug('closedReasonId '+closedReasonId);
        /* End Setup */
        
        ewtSelectlist esl = new ewtSelectlist(false, documentTypeId, workTaskId, workTaskReasonId, workDepartmentId, closedReasonId, null);
        
        esl.getWorkTaskName(workTaskId);
        esl.getWorkTaskReasonName(workTaskReasonId);
        esl.getWorkDepartmentName(workDepartmentId);
        esl.getClosedReasonName(closedReasonId);
         
        ApexPages.CurrentPage().getParameters().put('workTaskId', workTaskId); 
        esl.setWorkTask();
        
        ApexPages.CurrentPage().getParameters().put('workTaskReasonId', workTaskReasonId); 
        esl.setWorkTaskReason();  
        
        ApexPages.CurrentPage().getParameters().put('workDepartmentId', workDepartmentId); 
        esl.setWorkDepartment();   
        
        esl.reset();
        esl.clearWorkTask();
        esl.clearWorkTaskReason();
        esl.clearWorkDepartment();
        esl.clearClosedReason();
        
    }
    
}