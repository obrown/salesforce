public with sharing class authNumberCheckController{

    public patient_case__c pc {get; private set;}
    public string frr {get; private set;}
    public string par {get; private set;}
    public string PlanofCareaccepted {get; private set;}
    public string crr {get; private set;}
    public string continineCompleted {get; private set;} 
    public string authFormAttached {get; private set;} 
    public string authSent {get; private set;} 
    
    public authNumberCheckController(id theID) {
        checkRequirements(theID);
    } 
    
    public authNumberCheckController() {
        checkRequirements(apexPages.currentPage().getParameters().get('id'));
    }
    
    void checkRequirements(id theID){
        pc = [select client_name__c,
                     current_nicotine_user__c, 
                     continineCompleted__c,
                     referred_facility__c,
                     recordtype.name, 
                     Program_Authorization_Received__c, 
                     case_type__c, 
                     displayAuthNumber__c,
                     authReady__c,
                     Plan_of_Care_accepted_at_HDP__c,
                     Financial_Responsibility_Waiver__c,
                     authSent__c,
                     Caregiver_Responsibility_Received__c from patient_case__c where id = :theID];
        
        par = 'No';
        PlanofCareaccepted ='No';
        frr='No';
        crr='No';
        continineCompleted= 'Not Required';
        authFormAttached ='No';
        authSent = 'No';
        
        if(pc.Program_Authorization_Received__c != null){
            par = 'Yes';
        }
        
        if(pc.Plan_of_Care_accepted_at_HDP__c != null){
            PlanofCareaccepted = 'Yes';
        }
        
        if(pc.client_name__c == 'Walmart' && !pc.case_type__c.contains('Joint')){
            
            if(pc.Financial_Responsibility_Waiver__c != null){
                frr = 'Yes';
            }
            
            if(pc.Caregiver_Responsibility_Received__c != null){
                crr = 'Yes';
            }
            
        }else if(pc.client_name__c != 'Walmart'){
            
            frr = 'Not Required';
            crr = 'Not Required';
        
        }else if(pc.client_name__c == 'Walmart' && pc.case_type__c.contains('Joint')){
            
            frr = 'Not Required';
            
            if(pc.Caregiver_Responsibility_Received__c != null){
                crr = 'Yes';
            }
            
        }
        
        if(!pc.case_type__c.contains('Cardiac') && pc.current_nicotine_user__c == 'Yes'){
            
            if(pc.case_type__c.contains('Spine') && pc.referred_facility__c== 'Cleveland Clinic'){
            
            }else{
               if(!pc.continineCompleted__c){
                   
                   continineCompleted = 'No';
               }else if(pc.continineCompleted__c){
                   continineCompleted = 'Yes';
               } 
            }
                
        }
        
        Program_Notes__c[] authNote = new Program_Notes__c[]{};
        
        try{
            authNote = [select id,isAuthEmailAttach__c from Program_Notes__c where patient_case__c = :theID and isAuthEmailAttach__c = true];
            
        }catch(exception e){}
        
        if(authNote.size()>0){
            authFormAttached = 'Yes';
        }
        
        if(pc.authSent__c){
            authSent = 'Yes';
        }
    
    }
    
    

    
}