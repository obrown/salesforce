@isTest()
private class runclaimsAuditErrorTest {

    static testMethod void myUnitTest() {
    
        RestRequest req = new RestRequest(); 
        req.addHeader('Content-Type', 'application/json');
        string JSONMsg='{"message":"success"}';
        req.requestBody = Blob.valueof(JSONMsg);
        req.httpMethod = 'POST';        // Perform a POST
        
        
        RestResponse res = new RestResponse();
        
        RestContext.request = req;
        RestContext.response = res;
        
        insert (new Claims_Processor_Crosswalk__c(name = 'PlanBuild',Email_Address__c='unittest@test.com'));
        
        caClaim__c[] claimList = new caClaim__c[]{};
string claimBase = '30000000';

for(integer x=0; x<10; x++){
    string cn='';
    if(x<=9){
        cn='00' + string.valueof(x);
        
    }else if(x >9 && x<=99){
        cn='0' + string.valueof(x);
        
    }else if(x>99){
    
        cn = string.valueof(x);
    system.debug(claimBase+cn); 
    }
    
    claimList.add(new caClaim__c(name=claimBase+cn, Amount_to_be_paid__c=100+x));
}
insert claimList;

Claim_Audit__c[] auditList = new Claim_Audit__c[]{};
for(caClaim__c claim : claimList){
    auditList.add(new Claim_Audit__c(claim__c=claim.id));

}
insert auditList;

Claim_Audit_Error__c[] errorList = new Claim_Audit_Error__c[]{};

for(Claim_Audit__c audit : auditList){
    errorList.add(new claim_audit_error__c(claim_audit__c=audit.id));

}

insert errorList;

        
        
        string results = runclaimsAuditError.claimsAuditError();
        
        runclaimsAuditError.wsclaimsAuditError();

        
    }
    
}