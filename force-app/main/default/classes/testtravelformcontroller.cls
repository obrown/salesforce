/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers for the Travel Request form controller.
 */
@isTest
private class testtravelformcontroller{

    static testMethod void testtrfClass(){
    
        Recordtype rt = [select id,name from Recordtype where SobjectType = 'Patient_case__c' and Name = 'Walmart Spine' and isActive = true];
        
        Patient_Case__c pc = new Patient_Case__c ();
        pc.bid__c = '123456';
        pc.client_name__c = 'Walmart';
        pc.Employee_Last_name__c = 'Tester';
        pc.Employee_First_name__c = 'Johnny';
        pc.Patient_Last_name__c = 'Tester';
        pc.Patient_First_name__c = 'Jamie';
        pc.Estimated_Departure__c = date.today().addDays(3);
        pc.Estimated_Arrival__c= date.today().addDays(3);
        pc.recordtypeid = rt.id;
        pc.status__c = 'Open';
        pc.initial_call_discussion__c = 'test ICD';
        insert pc;
        system.assert(pc.id!=null);
        
        pc_poc__c poc = new pc_poc__c(patient_case__c=pc.id,last_appointment_date__c=date.today(),last_appointment_time__c='1:00 PM');
        insert poc;
        
        ApexPages.currentPage().getParameters().put('Id',pc.id);
        travelformcontroller trf = new travelformcontroller();
        
        trf.updatetrfdate();
        trf.getTransportMethod();
        trf.getAirport();
        trf.getHotel();
        trf.getCar(); 
        
        trf.obj.Travel_Type__c = 'Flying';
        //trf.obj.Referred_Facility__c = 'Johns Hopkins';
        trf.getAirport();
        
        //trf.obj.Referred_Facility__c = 'Mercy Springfield';
        trf.getAirport();
        
       // trf.obj.Referred_Facility__c = 'Virginia Mason';
        trf.getAirport();
        
        //trf.obj.Referred_Facility__c = 'Johns Hopkins';
        trf.obj.Hotel_Name__c = 'Test';
        trf.getHotel();

        
       // trf.obj.Referred_Facility__c = 'Virginia Mason';
        trf.getCar();
        
       // trf.obj.Referred_Facility__c = 'Mercy Springfield';
        trf.getCar();
        
        //trf.obj.Referred_Facility__c = 'Johns Hopkins';
        trf.getCar();
        
        //trf.obj.Referred_Facility__c = 'Mayo Jacksonville';
        trf.getCar();
        
 //       trf.obj.Referred_Facility__c = 'Mayo Phoenix';
        trf.getCar();
        
   //     trf.obj.Referred_Facility__c = 'Mayo Rochester';
        trf.getCar();
        
        trf.getTransportMethod();
        
        trf.getWordPrintViewXML();
    }

}