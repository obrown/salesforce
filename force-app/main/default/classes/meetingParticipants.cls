public with sharing class meetingParticipants {

    public id meetingID {get; set;}
    public string searchType {get; set;}
    public string searchTerm {get; set;}
    public Participant[] theParticipants {get; private set;}
    public Participant[] selectedParticipants {get; private set;} 
    map<id, Participant> participantMap = new map<id,Participant>();
    public string selected {get; set;}
    
    public meetingParticipants(){
       init();
    }
    
    public void init(){
        meetingID = ApexPages.CurrentPage().getParameters().get('id');
        theParticipants = new Participant[]{};
        loadParticipants();
    }
    
    public void search(){
        
        if(searchTerm=='' || searchTerm == null){
            return;
        }
        
        if(searchType=='' || searchType== null){
            return;
        }
        
        theParticipants = new Participant[]{};
        
        if(searchType.contains('HDP') || searchType.contains('All')){
            searchUser();
        }
        
        if(searchType.contains('Producers') || searchType.contains('All')){
            searchProducers();
        }
        
        if(searchType.contains('Contacts') || searchType.contains('All')){
            searchContacts();
        }
    }
    
    void loadParticipants(){
    
        selectedParticipants = new Participant[]{};
        Participant p;
        
        for(Meeting_User__c m : [select user__c, user__r.Name, user__r.firstName, user__r.lastName, user__r.email,user__r.phone, user__r.MobilePhone, createdDate from Meeting_User__c where Meeting__c = :meetingID]){
            p = new Participant(m.user__r.Name, m.user__r.email,m.user__r.phone,m.user__r.MobilePhone, m.createdDate, 'HDP', m.id, m.user__c);
            selectedParticipants.add(p); 
            participantMap.put(m.user__c, p);
        }
        
        for(Meeting_Producer__c m : [select producer__r.First_Name__c, producer__r.Last_Name__c, producer__r.Mobile__c, producer__r.Phone__c,producer__r.Email__c,createdDate from Meeting_Producer__c where Meeting__c = :meetingID]){
            p = new Participant(m.producer__r.First_Name__c+' '+isNull(m.producer__r.Last_Name__c), m.producer__r.Email__c,m.producer__r.Phone__c,m.producer__r.Mobile__c, m.createdDate, 'Producer', m.id, m.Producer__c);
            selectedParticipants.add(p); 
            participantMap.put(m.producer__c, p);
        }
        
    }
    
    
    void searchUser(){
        searchTerm = '%'+ searchTerm + '%';
        
        for(User u : [select name, phone, MobilePhone, email, CreatedDate from user where id not in :participantMap.keyset() and (name like :searchTerm or phone like :searchTerm) ]){
            Participant foo = new Participant(u.Name, u.email,u.phone,u.MobilePhone, u.createdDate, 'HDP', u.id);
            theParticipants.add(foo);
            participantMap.put(foo.recordID, foo); 
            
        }
        
    }
    
    void searchProducers(){
        searchTerm = searchTerm + '%';
        
        for(Producer__c p : [select First_Name__c, Last_Name__c, Phone__c, Mobile__c, Email__c, CreatedDate from Producer__c where id not in :participantMap.keyset() and (First_Name__c like :searchTerm or Last_Name__c like :searchTerm or Phone__c like :searchTerm ) ]){
            Participant foo = new Participant(p.First_Name__c+' '+isNull(p.Last_Name__c), p.Email__c,p.phone__c,p.Mobile__c, p.createdDate, 'Producer', p.id);
            theParticipants.add(foo); 
            participantMap.put(foo.recordID, foo); 
        }
        
    }
    
    void searchContacts(){
        //searchTerm = searchTerm + '%';
        
        //for(Producer__c p : [select First_Name__c, Last_Name__c, Phone__c, Mobile__c, Email__c, CreatedDate from Producer__c where (First_Name__c like :searchTerm or Phone__c like :searchTerm) ]){
        //    Participant foo = new Participant(p.First_Name__c+' '+isNull(p.Last_Name__c), p.Email__c,p.phone__c,p.Mobile__c, p.createdDate, 'Producer', p.id);
        //    theParticipants.add(new Participant(p.First_Name__c+' '+isNull(p.Last_Name__c), p.Email__c,p.phone__c,p.Mobile__c, p.createdDate, 'Producer', p.id)); 
        //    participantMap.put(foo.recordID, foo); 
        //}
        
    }
    
    public void addToSelected(){
        
        meetingParticipants.selectedParticipants sp = new meetingParticipants.selectedParticipants();
        sp = (meetingParticipants.selectedParticipants)JSON.deserialize(selected, meetingParticipants.selectedParticipants.class);
        
        Meeting_Producer__c[] mpList = new Meeting_Producer__c[]{};
        Meeting_User__c[] mpUser = new Meeting_User__c[]{};
        
        for(selectedParticipants s: sp.selectedParticipants){
            system.debug(s.id);
            participant p = participantMap.get(s.id);
            
            if(p!=null){
            selectedParticipants.add(participantMap.get(s.id));
            
            if(p.parentID==null){
                if(p.contactType =='Producer'){
                    mpList.add(new Meeting_Producer__c(Meeting__c=meetingID, Producer__c=p.recordid));
                
                }
            
                if(p.contactType =='HDP'){
                    mpUser.add(new Meeting_User__c(Meeting__c=meetingID, User__c=p.recordid));
                
                }
            }
            
            }
            
        }
        
        insert mpList;
        insert mpUser ;
        loadParticipants();    
    }
    
    public void deleteParticipants(){
    
        string theid = ApexPages.CurrentPage().getParameters().get('delID');
        string type = ApexPages.CurrentPage().getParameters().get('type');
        ApexPages.CurrentPage().getParameters().put('delID',null);
        ApexPages.CurrentPage().getParameters().put('type',null);
        
        if(type=='Producer'){
            Meeting_Producer__c mp = new Meeting_Producer__c (id=theid);
            delete mp;
        }
        
        if(type=='HDP'){
            Meeting_User__c mu = new Meeting_User__c(id=theid);
            delete mu;
        }
        
        loadParticipants();
    }
    
    string isNull(string theVal){
    
        if(theVal==null){
            return '';
        }
        
        return theVal;
    }
    
    class selectedParticipants{
        string ctype;
        string id;
        
        selectedParticipants[] selectedParticipants;
    }
    
}