public with sharing class cmUTRMinorTemplate{
    
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=cm.patient__r.Patient_First_Name__c+' '+cm.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Address__c: +'<br/>';
        letterText+='<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.City__c+', ' : ''+'&nbsp';
        letterText+=hasPatientAddress ? cm.patient__r.State__c+' ' : ''+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Zip__c : ''+'<br/>';
        letterText+='<br/>';

        letterText+='</div>';
        letterText+='<p>';
        letterText+='RE:&nbsp;'+cm.patient__r.Patient_First_Name__c+'&nbsp;'+cm.patient__r.Patient_Last_Name__c+'<br/><br/>';
        letterText+='Dear Parent/Guardian of&nbsp;'+cm.patient__r.Patient_First_Name__c;//+'&nbsp;'+cm.patient__r.Patient_Last_Name__c+',';
        letterText+='</p>';
        letterText+='<p>';  
        letterText+='Contigo Health and  '+cm.patient__r.Patient_Employer__r.name+' are committed to supporting members with their health management by providing a variety of health and';
        letterText+=' wellness programs. Our Case Management program is a telephonic education and support program that is voluntary and available at no additional cost to the member';
        letterText+=' or the member’s eligible dependents.<br/>';
        letterText+='</p>';

        letterText+='Unfortunately, I was unable to reach you by telephone to share information regarding the Case Management program that is available to you and';
        letterText+=' your eligible dependents. I am available to work with you and your family to provide ongoing support and education to help you take control';
        letterText+=' of your care and make informed health care decisions. We do not replace your medical team, but work with your doctors and providers in';
        letterText+=' partnership to ensure that you have the necessary information and help you coordinate the treatment plan with your plan benefits.';
        letterText+='<p>';  
        
        letterText+='Please feel free to call me at 1-877-891-2690, Monday-Friday from 8:30AM –5:00PM Eastern Time. If I am not able to take your call,';
        letterText+=' please leave a message on my confidential voicemail and I will return your call as soon as possible.';
        letterText+='</p>';         

        letterText+='I encourage you to continue to see your physician as necessary and I look forward to hearing from you soon.<br/><br/>';

        letterText+='Sincerely,<br/><br/>';
        letterText+='Care Management Department<br/>';
        letterText+='Contigo Health<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';

        return letterText;
    }
    
}