@isTest(seealldata=true)
private class transitionReferralControllerTest {

    static testMethod void transitionReferralTest() {
        
        Patient_Case__c pc = new Patient_Case__c();
        
        pc.client_name__c= 'Walmart';
        pc.recordtypeid = [select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name = 'Walmart Joint' limit 1].id;
        
        pc.Status__c = 'Open';
        pc.BID__c = '123456';
        pc.Language_Spoken__c = 'English';
        pc.Initial_Call_Discussion__c = 'The ICD';
        pc.Relationship_to_the_Insured__c = 'Spouse';
        pc.referral_source__c = 'Company Video';
        pc.Employee_Primary_Health_Plan_ID__c ='123456';
        pc.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        pc.Carrier_Name__c = 'Aetna';
        pc.Carrier_Nurse_Name__c = 'John Smith';
        pc.Carrier_Nurse_Phone__c = '(480) 555-1212';
        
        
        pc.Employee_first_name__c = 'John';
        pc.Employee_last_name__c = 'Smith';
        pc.employee_dobe__c = '1980-01-01';
        pc.Employee_Gender__c = 'Male';
        pc.employee_ssn__c = '123456789';
        
        pc.Patient_First_Name__c = 'Jane';
        pc.Patient_last_name__c = 'Smith';
        pc.Patient_dobe__c = '1981-01-01';
        pc.Patient_Gender__c = 'Female';
        pc.Patient_ssn__c = '987654321';
        pc.Patient_Email_Address__c = 'something@hdplus.com';
        
        pc.Same_as_Employee_Address__c = true;
        
        pc.employee_street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85297';
        
        pc.Employee_Home_Phone__c = '(480) 555-1212';
        pc.Employee_Mobile__c = '(480) 555-1212';
        pc.Employee_Work_Phone__c = '(480) 555-1212';
        
        pc.Employee_Preferred_Phone__c = 'Home';
        
        pc.Patient_Home_Phone__c = '(480) 555-1212';
        pc.Patient_Mobile__c = '(480) 555-1212';
        pc.Patient_Work_Phone__c = '(480) 555-1212';
        
        pc.Patient_Preferred_Phone__c = 'Home';
        pc.Callback_Number__c = '(480) 555-1212';
        
        pc.Caregiver_Home_Phone__c = '(480) 555-1212';
        pc.Caregiver_Mobile__c = '(480) 555-1212';
        
        
        pc.Caregiver_Name__c = 'John Smith';
        pc.Caregiver_DOBe__c = '1982-01-01';
        
        insert pc;
        
        ApexPages.currentPage().getParameters().put('id',pc.id);
        caseController cc = new caseController();
        cc.obj.Provider_Type__c='Family';
        cc.profname='Jane';
        cc.prolname='Smith';
        cc.proemail = 'something@hdplus.com';
        cc.prostreet = '123 E Main St';
        cc.procity = 'Mesa';
        cc.prozip = '85212';
        
        cc.AddProvider();
        
        code__c fooCode = new code__c(type__c='DRG');
        insert fooCode;
        
        cc.obj.Clinical_Code_Status__c = 'Proposed';
        cc.codeType = 'DRG';
        cc.codeList =  fooCode.id;
        cc.addCC();
        
        cc.obj.Eligible__c = 'Yes';
        cc.obj.Patient_and_Employee_DOB_verified__c = true;
        cc.obj.Patient_and_Employee_SSN_verified__c = true;
        cc.obj.BID__c = '123549';
        cc.obj.BID_Verified__c = true;
        cc.obj.Carrier_and_Plan_Type_verified__c = true;
        
        update cc.obj;
        
        ApexPages.currentPage().getParameters().put('id', cc.obj.id);
        transitionReferralController trc = new transitionReferralController();
        
        trc.referCase();
        
        pc.Is_a_Caregiver_Available__c= 'Yes';
        update pc;
        
        trc.referCase();
        
        
    }
    
}