public with sharing class populationHealthBatchEligibility {
    public populationHealthBatchEligibility() {
    }

    pubic void checkEligibility(){
        set<id> idset = new set<id>();
        integer i = 0;

        for (Utilization_Management__c um : [select Patient__c from Utilization_Management__c where Case_Status__c != 'Approved – Final' and Case_Denied__c = false]) {
            idset.add(um.Patient__c);
            if (i == 100) {
                populationHealthFuture.eligbility(idset);
                idset.clear();
                i = 0;
            }
            else{
                i++;
            }
        }

        populationHealthFuture.eligbility(idset);
    }
}
