@isTest(seealldata=true)
private class hp_eligExceptionController_test{

    static testMethod void myUnitTest() {
        hp_eligExceptionController hec = new hp_eligExceptionController();
        ApexPages.currentPage().getParameters().put('ld','20140901');
        ApexPages.currentPage().getParameters().put('uw','026');
        
        hec.EligibilityList();
        hec.changeList();
        
        logExceptionType.EligibilityRecs[] fooletlist = new logExceptionType.EligibilityRecs[]{};
        logExceptionType.EligibilityRecs foolet = new logExceptionType.EligibilityRecs();
        foolet.ElgxGrnbr ='026';
        foolet.ElgxEssn ='123456789';
        foolet.ElgxDepseq ='01';
        foolet.ElgxType ='45';
        foolet.ElgxMsg ='45';
        fooletlist.add(foolet);
        hec.excepLst.add(fooletlist);
        
        
        hec.table='exceptions';
        hec.exportToCSV();

        logChangeType.EligChanges[] foolctlist = new logChangeType.EligChanges[]{};
        logChangeType.EligChanges foolct = new logChangeType.EligChanges();
        foolct.EhsGrnbr='026';
        foolct.EhsSsn ='123456789';
        foolct.EhsLname ='01';
        foolct.EhsFtname ='45';
        foolct.EhsField='Test';
        foolct.EhsBefore='1';
        foolct.EhsAfter='2';
        foolctlist.add(foolct);
        hec.changes.add(foolctlist);

        hec.table='changes';
        
        hec.exportToCSV();
        hec.emailFiles();
        hec.EligibilityList();
        hec.changeList();
        
    }
    
}