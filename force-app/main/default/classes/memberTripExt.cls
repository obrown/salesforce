public with sharing class memberTripExt {

    string caseId;
    id memberTripId, employeeID,clientId,procedureId;
    
    public boolean isError {get; private set;}
    public string patientName {get; private set;}
    public string caseName {get; private set;}
    
    public memberTrips__c memberTrip {get; set;}
    public airlineTrip flightTrip {get; set;}
    //public airlineTrip airlineTrip {get; set;}
    public hotelTrip hotelTrip {get; set;}
    
    string pArrival;
    string pDeparture;
    string pWheelChairNeeded; 
    string pExtraTall;        
    
    public map<id, string> motMap {get; private set;} 
    
    public selectOption[] facilities {get; private set;}
    public selectOption[] ModeOfTransportation {get; private set;}
    
    ApexPages.StandardController thisController;
    map<id, Client_Facility__c> facilityMap = new map<id, Client_Facility__c>();
    
    patient_case__c patientCase;
    pc_POC__c poc;
    public Client_Facility__c facility {get; private set;}
    
    public memberTripExt(ApexPages.StandardController controller) {
        memberTripId = controller.getId();
        memberTrip = (memberTrips__c )controller.getRecord();
        thisController = controller;
        try{
            caseID = ApexPages.Currentpage().getParameters().get('cid');
            if(caseId ==null){
                patientName = 'Error occurred';
                caseName = 'Error occurred';
            
            }else{
                
                caseID= caseID.escapeHtml3();
                try{ 
                patientCase = [select client_facility__c,
                                      client__c, 
                                      ecen_procedure__c,
                                      employeeID__c,
                                      Extra_space_for_large_tall_patient__c, 
                                      Estimated_arrival__c,
                                      Estimated_departure__c,
                                      Maximum_flight_length__c,
                                      nMileage__c,
                                      Mileage__c,
                                      patient_first_name__c, 
                                      patient_last_name__c,
                                      Patient_Arrival_Date__c,
                                      Return_Home_Travel_Date__c,
                                      Wheelchair_needed_at_airport__c,
                                      recordtype.name,
                                      name from patient_case__c where id = :caseId];
               } catch(exception err){}  
                                            
               
                                             
                if(memberTrip.Id == null){
                    try{             
                    string pocId = Apexpages.CurrentPage().getParameters().get('poc');
                    if(pocId!=null){
                        pc_POC__c poc = [select Patient_Arrival_Date__c, Return_Home_Travel_Date__c,Hospital_Admission_Date__c, Hospital_Discharge_Date__c from pc_POC__c where id = :pocId];
                        memberTrip.EstimatedArrivalDate__c=poc.Patient_Arrival_Date__c;
                        memberTrip.EstimatedDepartureDate__c=poc.Return_Home_Travel_Date__c ;
                        memberTrip.Admit_to_Hospital__c =poc.Hospital_Admission_Date__c;
                        memberTrip.Discharge_from_Hospital__c =poc.Hospital_Discharge_Date__c ;
                    }else if(patientCase.RecordType.Name.contains('Cardiac')){
                        //memberTrip.EstimatedArrivalDate__c=patientCase.Estimated_arrival__c;
                        //memberTrip.EstimatedDepartureDate__c=patientCase.Estimated_departure__c;
                        
                        memberTrip.EstimatedArrivalDate__c=patientCase.Patient_Arrival_Date__c;
                        memberTrip.EstimatedDepartureDate__c=patientCase.Return_Home_Travel_Date__c; 
                       
                        
                    }
                    
                    }catch(exception err){}                              
                    
                }    
                
                memberTrip.patient_case__c= caseId;
                employeeID=patientCase.employeeID__c;
                clientId=patientCase.client__c;
                procedureId=patientCase.ecen_procedure__c;
                
                
                try{
                    if(memberTrip.Driving_Mileage__c==null && patientCase.nMileage__c!=null){
                        memberTrip.Driving_Mileage__c=patientCase.nMileage__c*2;
                    }
                }catch(exception err){}
                
                patientName = patientCase.patient_first_name__c+' '+ patientCase.patient_last_name__c;
                caseName = patientCase.name;
                loadFacilities();
                
                if(patientCase.client_facility__c!=null){
                    memberTrip.ClientFacility__c=patientCase.client_facility__c;
                }
                
                if(memberTrip.ClientFacility__c!=null){
                    setFacility();
                }
                loadModeOfTransportation();
                   
                for(RelatedtoEmployee__c rte: [select id, RelatedEmployeeID__c, EmployeeContactID__c from RelatedtoEmployee__c where RelatedEmployeeID__c = :employeeID or EmployeeContactID__c = :employeeID]){
                    eIdSet.add(rte.EmployeeContactID__c );
                }
                
                flightTrip = new airlineTrip(memberTripId, patientCase.id, eIdSet); //loads flights
                hotelTrip = new hotelTrip(memberTripId, eIdSet, patientCase.client__c, patientCase.employeeID__c, patientCase.id); //loads hoteltrips
                
            }
            
        }catch(exception e){
            patientName = 'Error occurred';
            caseName = 'Error occurred';
            system.debug(e.getMessage()+' '+e.getLineNumber());
        }
    }
   

       
    public void save(){
        isError=false;
           
        try{
            upsert memberTrip;
            memberTripId=memberTrip.id;
            memberTrip = (memberTrips__c )thisController.getRecord();
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Record Saved'));
        }catch(exception e){
            isError=true;
            system.debug(e.getMessage());
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Info, e.getMessage()));
        }
        
        
        
    }
    
    set<id> eIdSet = new set<id>();
    
    public void cancelHotel(){
        hotelTrip.cancelHotel();
        saveHotel();
    }
    
    public void saveHotel(){
        save();
        if(isError){
            return;
        }
        hotelTrip.setMemeberTripId(memberTripid);
        
        hotelTrip.saveHotel();
        isError=hotelTrip.isError;
        if(isError){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error, hotelTrip.message));
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.confirm, hotelTrip.message));
        }
        
    }
    
    public void newHotel(){
        isError=false;
        try{
            
            if(memberTrip.ClientFacility__c==null){
                isError=true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error, 'Please choose a facility'));
                return;
            }
            
            upsert memberTrip;
            hotelTrip.newHotel(memberTrip.ClientFacility__c);
            /*8-3-20 removed per Nate B
            hotelTrip.hotelTrip.CheckInDate__c=memberTrip.EstimatedArrivalDate__c;
            hotelTrip.hotelTrip.CheckOutDate__c=memberTrip.EstimatedDepartureDate__c;
            */
            
        }catch(exception e){
            system.debug(e.getLineNumber()+' '+e.getMessage());
        }
    }
    
    public void deleteHotelTrip(){
        string hotelTripId = ApexPages.CurrentPage().getParameters().get('hotelTripId');
        hotelTrip.deleteHotelTrip(hotelTripId);
    }
    
    public void getHotelTrip(){
        string hotelTripId = ApexPages.CurrentPage().getParameters().get('hotelTripId');
        hotelTrip.getHotelTrip(hotelTripId,memberTrip.ClientFacility__c);
    }
    
    public void newFlight(){
        upsert memberTrip;
        flightTrip.newFlight();
        syncflightData();
    }
    
    public void getFlightInformation(){
        string flightId = ApexPages.CurrentPage().getParameters().get('airlineTripId');
        flightTrip.getFlightInformation(flightId);
    }
    
    public void saveFlightandNew(){
        save();
        if(isError){
            return;
        }
        flightTrip.setMemeberTripId(memberTripid);
        flightTrip.saveFlightandNew();
        if(flightTrip.isError){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error, flightTrip.message));
            return;
        }
        syncflightData();
        
    }
    
    public void saveFlight(){
        
        save();
        if(isError){
            return;
        }
        
        flightTrip.setMemeberTripId(memberTripId);
        flightTrip.saveFlight();
        isError=flightTrip.isError;
        if(isError){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.error, flightTrip.message));
        }else{
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.confirm, flightTrip.message));
        }
        
    }
    
    public void deleteAirlineTrip(){
        string flightId = ApexPages.CurrentPage().getParameters().get('airlineTripId');
        flightTrip.deleteAirlineTrip(flightId);
    }
    
    public void deleteAirlineAdjTrip(){
        string flightId = ApexPages.CurrentPage().getParameters().get('airlineAdjTripId');
        flightTrip.deleteAirlineAdjTrip(flightId);
    }
    
    void syncflightData(){
        if(memberTrip.EstimatedArrivalDate__c!=null){
            try{
            string fa=string.valueof(memberTrip.EstimatedArrivalDate__c)+' '+'06:00:00';
            //YYYY-MM-DD
            flightTrip.airlineTrip.ArrivalFlightDateTime__c = datetime.valueof(fa.mid(0,4)+ '-' +fa.mid(5,2)+ '-' + fa.mid(8,2)+ ' ' +fa.mid(11,8));
            }catch(exception e){}
        }
        
        if(memberTrip.EstimatedDepartureDate__c!=null){
            try{
            string fd=string.valueof(memberTrip.EstimatedDepartureDate__c)+' '+'06:00:00';
            //YYYY-MM-DD
            flightTrip.airlineTrip.DepartureFlightDateTime__c= datetime.valueof(fd.mid(0,4)+ '-' +fd.mid(5,2)+ '-' + fd.mid(8,2)+ ' ' +fd.mid(11,8));
            }catch(exception e){}
        }
        
        if(patientCase.recordtype.name.contains('Cardiac')){
           flightTrip.airlineTrip.Extra_space_for_large_tall_patient__c=patientCase.Extra_space_for_large_tall_patient__c;
           flightTrip.airlineTrip.Wheelchair_Needed__c=patientCase.Wheelchair_needed_at_airport__c;  
           flightTrip.airlineTrip.Maximum_flight_length__c=string.valueof(patientCase.Maximum_flight_length__c); 
        }else{
            try{
                pc_poc__c poc = [select Extra_space_for_large_tall_patient__c,Maximum_flight_legnth__c,Wheelchair_needed_at_airport__c from pc_poc__c where patient_case__c = :patientCase.id order by createdDate desc limit 1];
                flightTrip.airlineTrip.Extra_space_for_large_tall_patient__c=poc.Extra_space_for_large_tall_patient__c;
                flightTrip.airlineTrip.Wheelchair_Needed__c=poc.Wheelchair_needed_at_airport__c;
                flightTrip.airlineTrip.Maximum_flight_length__c=string.valueof(poc.Maximum_flight_legnth__c);
                
            }catch(exception e){
                system.debug(e.getMessage());
            }
            
        }
    }
        
    public void setFacility(){
        facility=facilityMap.get(memberTrip.ClientFacility__c);
        
    }
    
    void loadFacilities(){ //obj.nReferred_facility__c
        facilities=coeSelfAdminPickList.selectOptionList(null, clientId, procedureId, null, coeSelfAdminPickList.referredFacility, null, true);
        
        for(selectOption so : facilities){
            if(so.getValue()!=''){
                facilityMap.put(so.getValue(), null);
            }
        }
        
        for(Client_Facility__c f : [select Facility__r.Facility_Address__c,
                                           Facility__r.Facility_City__c,
                                           Facility__r.Facility_State__c,
                                           Facility__r.Facility_Zip__c,
                                           Facility__r.Latitude__c,
                                           Facility__r.Longitude__c from Client_Facility__c  where id in :facilityMap.keySet()]){
            facilityMap.put(f.id, f);
        }
    }
    
    void loadModeOfTransportation(){    
        ModeOfTransportation = new selectoption[]{};
        ModeOfTransportation.add(new selectOption('','-- None --'));
        motMap = new map<id, string>();
        motMap.put(null, ''); //required for new records or records where mode of transportation is not defined
        for(ModeOfTransportation__c mt : [select name from ModeOfTransportation__c]){
            ModeOfTransportation.add(new selectoption(mt.id, mt.name));
            motMap.put(mt.id, mt.name);
        }
    }
}