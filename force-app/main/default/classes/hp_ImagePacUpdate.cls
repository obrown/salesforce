public class hp_ImagePacUpdate extends hp_callWebApp{
    
    
    public static string sendImagePacUpdate(hpImpagePacStruct hpi){
        if(hpi==null){
            return 'Attachment information is null';
        }
        hp_ImagePacUpdate hpU = new hp_ImagePacUpdate();
        
        return hpU.sendImagePacUpdateWebApp(hpi);
    }
    
    public string sendImagePacUpdateWebApp(hpImpagePacStruct hpi){
        endpoint= 'filerouteimagepac';
        //jsonBody = JSON.Serialize(hpi);
        
        
        search.add('Filename:'+hpi.Filename);
        search.add('ReceivedDate:'+hpi.ReceivedDate);
        search.add('ScanDate:'+hpi.ScanDate);
        search.add('ServerDesc:'+hpi.ServerDesc);
        search.add('UserDesc:'+hpi.UserDesc);
        search.add('ImageType:'+hpi.ImageType);
        search.add('EntityType:'+hpi.EntityType);
        search.add('HpKeyPartA:'+hpi.HpKeyPartA);
        search.add('HpKeyPartB:'+hpi.HpKeyPartB);
        search.add('HpKeyPartC:'+hpi.HpKeyPartC);
        jsonBody = jb.eligSearch(search);
        jsonBody = jsonBody.replaceAll('null','');
        
        if(Test.IsRunningTest()){
           result = '{"ResultCode":"0"}';
        }else{
           result = callWebApp(null); 
        }
        
        Map<String, Object> r = (Map<String, Object>)JSON.deserializeUntyped(result);
        if(r.get('ResultCode').toString()=='0'){
            return null;
        }else{
            return r.get('ErrorMsg').toString();
        }
        
        
        
        //return null;
    }
    
}