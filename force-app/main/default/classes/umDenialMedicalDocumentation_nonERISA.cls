public with sharing class umDenialMedicalDocumentation_nonERISA{
    
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c  uml, Utilization_Management_Clinician__c clinician){
        boolean hasAddress = (um.patient__r.Address__c!=null&&um.patient__r.City__c!=null&&um.patient__r.State__c!=null&&um.patient__r.Zip__c!=null);
        boolean hasClinician = (Clinician.Street__c!=null&&Clinician.City__c!=null&&Clinician.State__c!=null&&Clinician.Zip_Code__c!=null);
        
        string letterText='';
        
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasAddress ? um.patient__r.Address__c + '<br/>': '' + '<br/>';
        letterText+=hasAddress ? um.patient__r.City__c +', ': ', ';
        letterText+=hasAddress ? um.patient__r.State__c  + ' ' : ' ';
        letterText+=hasAddress ? um.patient__r.Zip__c +'<br/><br/>': '' +'<br/><br/>';

        letterText+='RE:&nbsp;&nbsp;Patient Name:&nbsp;'+um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c;
        letterText+='<br/>';
        letterText+='&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth:&nbsp;'+um.patient__r.Patient_Date_of_Birth__c;
        letterText+='</div>';
        letterText+='<p>';
        letterText+='Dear&nbsp;'+um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c+',';
        letterText+='</p>';
        letterText+='<p>';  
        letterText+='Contigo Health, LLC is a third-party administrator that performs Care Management services for the '+um.patient__r.Patient_Employer__r.name+' self-funded group health plan of '+um.patient__r.Patient_Employer__r.name+' (Plan).<br/>';
        letterText+='</p>';
                      
        letterText+='The request referenced below has been denied for administrative reasons:<br/><br/>';
        letterText+='<div style="margin-top:10px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Case Number:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px;">';
        letterText+='&nbsp;'+um.HealthPac_Case_Number__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Physician Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Facility Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+um.Facility_Name__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Dates of Service:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;Beginning '+um.Admission_Date__c.month()+'/'+um.Admission_Date__c.day()+'/'+um.Admission_Date__c.year();
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%;vertical-align:top">';
        letterText+='Type of Request:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+uml.Type_of_Request__c;
        letterText+='</div>';
        letterText+='</div>';
        
        letterText+='<br/>';
        letterText+='Denial Reason: Despite three requests on our part, no clinical information has been received to support medical necessity. Therefore, '+uml.Admission_of__c+' has been denied until such time as the information has been received and reviewed.<br/><br/>';

        letterText+='Sincerely,<br/><br/>';
        letterText+='Care Management Department<br/>';
        letterText+='Contigo Health<br/>';
        letterText+='1755 Georgetown Rd<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';
        
        letterText+='All choices regarding the care and treatment of the patient remain the responsibility of the attending physician. In no way does this letter attempt to dictate the care the patient ultimately receives.<br/><br/>';

        letterText+='<p>';
        letterText+='The plan complies with applicable Federal civil rights laws and does not discriminate on the basis of race, color, national origin, sex, age or disability above the appeal right section of the denial letters.';
        letterText+='</p>';
        
        letterText+='<div>';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='CC:'; 
        letterText+='</div>';
        letterText+='<div style="display:inline-block;">';
        
        letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;"/>';
        letterText+=hasClinician ? Clinician.Street__c : '';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasClinician ? Clinician.City__c + ', ' : ', ';
        letterText+=hasClinician ? Clinician.State__c  +' ' : ' ';
        letterText+=hasClinician ? Clinician.Zip_Code__c : '';
        letterText+='</div>';
        letterText+='</div><br/>';         
        
        letterText+='<div style="margin-top:10px">';
        
        letterText+='<div style="display:inline-block;">';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';

        return letterText;
    }
    
}