public with sharing class coeTaskListController {
    
    public boolean isError {get; private set;}
    
    public boolean patient {get; set;}
    public boolean facility {get; set;}
        
    public string case_id {get; set;}
    public string case_name {get; set;}
    public boolean loading {get; set;}
    public string body {get; set;}
    public integer numberOfTasks {get; set;}
    public Patient_Case_Task__c[] tasks {get; private set;}
    public Patient_Case_Task__c task {get; set;}
    public taskListWrapper[] taskList {get; private set;}
    public taskListWrapper[] completed_taskList {get; private set;}
    
    class taskListWrapper{
        public Patient_Case_Task__c task {get; set;}
        public selectOption[] opts {get; set;} 
        integer id;
        public string completedDate {get; set;}
    }
    
    public coeTaskListController(){numberOfTasks=1;loading=true;completed_taskList = new taskListWrapper[]{};}
    
    public void load_Tasks(){
        load_jcsTasks();
        newTask();
    }
    
    public void set_case_id(){
        case_id = ApexPages.CurrentPage().getParameters().get('id');
    }
    
    public void saveTask(){
        isError=false;
        
        try{
            system.debug('task '+task);
            if(task.name==null && task.dueDate__c==null && task.status__c==null && task.organization__c==null){
                tasks = new Patient_Case_Task__c[]{};
                for(taskListWrapper tlw : taskList){
                    if(tlw.task.status__c=='Complete'){
                        tlw.task.completedDate__c=date.today();
                    }
                    system.debug('tlw.task.visible_to_facility__c: ' + tlw.task.visible_to_facility__c);
                    tasks.add(tlw.task);
                }
                for(taskListWrapper tlw : completed_taskList){
                    if(tlw.task.status__c!='Complete'){
                        tlw.task.completedDate__c=null;
                    }
                    tasks.add(tlw.task);
           
                }
                upsert tasks;
                setTasks(tasks, false);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Task List Saved'));
            }else{
                if(task.name==null){return;}
                task.isCustom__c=true;
                upsert task;
                tasks.add(task);
                setTasks(tasks, false);
                newTask();
            
            }
            
        }catch(exception e){
            isError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, e.getMessage()));
        }
    }
    
    public void save(){
        isError=false;
        try{
            tasks = new Patient_Case_Task__c[]{};
            for(taskListWrapper tlw : taskList){
                system.debug('tlw.task.visible_to_facility__c: ' + tlw.task.visible_to_facility__c);
                tasks.add(tlw.task);
            }
            for(taskListWrapper tlw : completed_taskList){
                tasks.add(tlw.task);
            }
            upsert tasks;
            setTasks(null, false);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm, 'Task List Saved'));
            
        }catch(exception e){
            isError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Fatal, e.getMessage()));
        }
    }
    public void newTask(){
        task = new Patient_Case_Task__c(patient_case__c=case_id);
    }
    public void clearError(){
        ApexPages.getMessages().clear();
    }
    
    void load_jcsTasks(){
        patient_case__c pc = [select name,client_facility__r.Client__r.name, client_facility__r.Procedure__r.name from patient_case__c where id = :case_id];
        case_name = [select name from patient_case__c where id = :case_id].name;
        setTasks(null, true);
        if(tasks == null || tasks.isEmpty()){
            ecen_tasks et = new ecen_tasks(false);
            tasks = et.getPatientCaseTasks(pc).tasks;    
        }
        
        loading=false;
    }
    
    void setTasks(Patient_Case_Task__c[] tasks, boolean checkforNewTasks){
        
        if(tasks==null){
            tasks = new Patient_Case_Task__c[]{};
            tasks = [select field__c,isCustom__c,completedDate__c,name,status__c,Status_Options__c,description__c,organization__c,dueDate__c,dueDateDate__c,visible_to_facility__c,Case_Task__r.Status_Options__c,Patient_Case__c, Patient_Case__r.client_facility__r.client__r.name,Patient_Case__r.client_facility__r.procedure__r.name from Patient_Case_Task__c where patient_case__c = :case_id order by name asc];
            if(tasks.isEmpty()){
                return;
            }
        }
        
        ecen_tasks et = new ecen_tasks(false);
        if(checkforNewTasks){
            tasks.addAll(et.checkForNewProgramTasks(tasks));
        }
        
        taskList = new taskListWrapper[]{};
        completed_taskList = new taskListWrapper[]{};
        integer i=0;
        for(Patient_Case_Task__c t : tasks){
            taskListWrapper tw = new taskListWrapper();
            tw.id = i;
            tw.task = t;
            selectOption[] foo_opts = new selectOption[]{};
            foo_opts.add(new selectOption('', '--None--'));
            foo_opts.add(new selectOption('N/A', 'N/A'));
            foo_opts.add(new selectOption('Not Started', 'Not Started'));
            foo_opts.add(new selectOption('In Progress', 'In Progress'));
            foo_opts.add(new selectOption('Complete', 'Complete'));
            
            tw.opts=foo_opts;
            if(t.completedDate__c==null && t.status__c != 'N/A'){
                taskList.add(tw);
            }else{
                completed_taskList.add(tw);
            }
            
            
        }        
        
    }
    
}