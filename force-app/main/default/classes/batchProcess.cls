public without sharing class batchProcess {
    
    SystemID__c sid = SystemID__c.getInstance();  
    
    public void runIntakeOpen(){
    
        try{
                
            //Build a list of intakes where the Owner has changed
            list<Patient_Case__History> intakeHistory = new list<Patient_Case__History >([Select Field, parentID From Patient_Case__History where field = 'Owner']);
            set<ID> historyIDs = new set<ID>();
            //Populate a set of ID's so we can exclude in a future SOQL call
            for(Patient_Case__History l: intakeHistory){
                if(!historyIDs.contains(l.parentID)){
                    historyIDs.add(l.parentID);
                    
                }
            }
            
            //Open leads that have NOT been notified
            list<Patient_Case__c> openIntake = new list<Patient_Case__c>([select patient_case__c,name, Client_Name__c, CreatedBy.Name,status__c from Patient_Case__c where (status__c = 'Open' and id not IN :historyIDs and openNotification__c = false and Client_Name__c = 'Walmart') and (Owner.ProfileID = :sid.csrProfID__c or Owner.ProfileID=:sid.csrSuperProfid__c)]);       
            list<Patient_Case__c> secondOpenIntake = new list<Patient_Case__c>([select patient_case__c,name, Client_Name__c,CreatedBy.Name, status__c,status_reason__c,Status_Reason_Sub_List__c from Patient_Case__c where status__c = 'Open' and id not IN :historyIDs and openNotification__c = true and Client_Name__c = 'Walmart']);
            
            for(Patient_Case__c l: openIntake){
                
                l.openNotification__c = true;
            }
            
            if(Test.isRunningTest()){
                if(openIntake.isEmpty()){
                    
                    for(Patient_Case__c l:[select id, name, Client_Name__c, CreatedBy.Name,status__c from Patient_Case__c limit 3]){
                        openIntake.add(L);
                    }
                    
                }
            }
            
            if(!openIntake.isEmpty()){
               
                string bodyofEmail;
                for(Patient_Case__c l: openIntake){
                    
                    if(bodyofEmail == null){
                        bodyofEmail = 'Hello,' + '\n\n' +  'Patient Case ' + l.patient_case__c + ' has been left in the open status. The intake was created by '+ l.CreatedBy.Name + '.\n\nPlease click on the following link to review and close as neccessary. ' +  '\n\n' + 'https://'+Environment__c.getInstance().instance__c+'.salesforce.com/' + l.id + '\n\n';              
                    }else{
                        bodyofEmail = bodyofEmail + 'Patient Case ' + l.patient_case__c + ' has been left in the open status. The intake was created by '+ l.CreatedBy.Name + '.\n\nPlease click on the following link to review and close as neccessry. ' + '\n\n' + 'https://na7.salesforce.com/' + l.id + '\n\n';
                    }       
                    
                }
                
                bodyofEmail = bodyofEmail + 'Thank you';
                                
                    list<string> sendemailto = new list<string>();  
                    list<messaging.Singleemailmessage> finalMail = new list<messaging.Singleemailmessage>();  
                    sendemailto.add('mmartin@hdplus.com');
                    sendemailto.add('jmccormick@hdplus.com');
                    
                    
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                     
                    mail.setReplyTo('mmartin@hdplus.com');
                    mail.setToAddresses(sendemailto);
                    mail.setSenderDisplayName('Walmart open intakes');
                    mail.setSubject('[PHI] Walmart open intakes');
                    mail.setPlainTextBody(bodyofEmail);
                    finalMail.add(mail);
                    
                    messaging.sendEmail(finalmail);
            
            update openIntake;
            
            }
            
            for(Patient_Case__c l: secondOpenIntake){
                l.status__c = 'Closed';
                l.status_reason__c = 'Patient Declined';
                l.Status_Reason_Sub_List__c = 'Inquiry';
            }
            update secondOpenIntake;
    
        }catch (exception e){
            batchException(e.getMessage(),e.getLineNumber());   
            system.debug(e);
        }

    }
    
    public void tallyMemberCOEcases(){
     
        map<id, contact> cIds = new map<id,contact>();
        contact[] clist = new contact[]{};
        
        for(contact c: [select Number_of_COE_cases__c from contact]){
            cIds.put(c.id, c);
        }
        
        sObject[] results = [SELECT count(id)cnt, member__c FROM Patient_case__c where member__c in :cIds.keySet() group by Member__c];

        for(sObject ar: results){
            contact c = cIds.get(string.valueof(ar.get('member__c')));
            if(integer.valueof(ar.get('cnt')) >1){
               system.debug(c.id);
            }
            c.Number_of_COE_cases__c = integer.valueof(ar.get('cnt'));
            clist.add(c);
        }
        
        update clist;
    
    }
    
    public void zonechanges(){
     
        zoneChanges zc = new zoneChanges();
        zc.setchanges();
    
    }
    
    public void transplantEligcheck(){
     
        transplantEligCheck tec = new transplantEligCheck();
        tec.init();
    
    }
    
    public void surveyMonkey(){
     
        surveyMonkeyReport smr = new surveyMonkeyReport();
        smr.run(false, null);
    
    }
    
    public void transplantApptDatesEightDaysOut(){
     
        Date targetDate = date.today();
        integer i = 0;
        while(i<8){ //ignore Saturday and Sunday
        
          Date startOfWeek = targetDate.toStartOfWeek();
            Integer dayOfWeek = targetDate.day()-startOfWeek.day();      
        
            targetDate = targetDate.addDays(1);
          if(dayOfWeek != 0 && dayOfWeek != 6){
             i++;  
          }
        
        }
        map<id,Transplant__c>tmap = new map<id,Transplant__c>();
        for(Transplant__c t: [select id,Patient_last_name__c,Patient_first_name__c from Transplant__c where (Stage__c = 'Pre' or Stage__c = 'Global' or Stage__c = 'Post') and intake_status__c = 'Open' ]){
          tmap.put(t.id,t);
        }
        
        list<list<Task>> tasklists = new list<list<task>>();
        
         user[] ulist = new user[]{};
                set<id> uset = new set<id>();
               Group gp = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = 'Transplant'];
                for(GroupMember g: [select userOrGroupId from GroupMember where groupid = :gp.id]){
                    uset.add(g.userOrGroupId);
                }
                ulist = [select id from User where isActive = true and id IN :uset];
        
    
        //Walmart Transplant Triggers – Visio #2
        for(Appointment__c a : [select id,Transplant__c,Stage__c from Appointment__c where Transplant__c IN :tmap.keyset() and (Estimated_Appointment_End__c = :targetDate or Actual_Appointment_End__c = :targetDate) and Canceled_Date__c = null]){
       
         Task[] temptlist = new task[]{};
          Transplant__c tempt = new Transplant__c();
          tempt = tmap.get(a.Transplant__c);
        
            for(User u : ulist){
                  
              Task task = new Task(
                            ownerid=u.id,
                            whatid=a.Transplant__c,
                            Subject= tempt.Patient_last_name__c + ', ' + tempt.Patient_first_name__c + ' - Verify ' +a.Stage__c +  ' Appointment Dates with Mayo');
                            
              temptlist.add(task);
                            
            }
        
          tasklists.add(temptlist);
       
        }
       
        
        map<id,Transplant__c>tmap1 = new map<id,Transplant__c>();
        for(Transplant__c t: [select id,Patient_last_name__c,Patient_first_name__c from Transplant__c where (Stage__c = 'Pre' or Stage__c = 'Post') and intake_status__c = 'Open' ]){
          tmap1.put(t.id,t);
        }
    
        //Walmart Transplant Triggers – Visio #3
        targetDate = date.today().adddays(-1);
        
        for(Appointment__c a : [select id,Transplant__c,Stage__c from Appointment__c where Transplant__c IN :tmap1.keyset() and Actual_Appointment_Start__c = null and Canceled_Date__c = null and (Estimated_Appointment_Start__c = :targetDate)]){
          Task[] temptlist = new task[]{};
          Transplant__c tempt = new Transplant__c();
          tempt = tmap1.get(a.Transplant__c);
        
            for(User u : ulist){
                  
              Task task = new Task(
                            ownerid=u.id,
                            whatid=a.Transplant__c,
                            Subject= tempt.Patient_last_name__c + ', ' + tempt.Patient_first_name__c + ' - Confirm & populate Appointment (' + a.stage__c + ') Actual Start Date.');
                            
              temptlist.add(task);
                            
            }
        
          tasklists.add(temptlist);
    
        }
        
    
        //Walmart Transplant Triggers – Visio #4
        targetDate = date.today().adddays(2);
        
        for(Appointment__c a : [select id,Transplant__c,Stage__c from Appointment__c where Transplant__c IN :tmap.keyset() and Canceled_Date__c = null and (Estimated_Appointment_End__c = :targetDate)]){
          Task[] temptlist = new task[]{};
          Transplant__c tempt = new Transplant__c();
          tempt = tmap.get(a.Transplant__c);
        
            for(User u : ulist){
                  
              Task task = new Task(
                            ownerid=u.id,
                            whatid=a.Transplant__c,
                            Subject= tempt.Patient_last_name__c + ', ' + tempt.Patient_first_name__c + ' - Request Clinical Documentation from Mayo.');
                            
              temptlist.add(task);
                            
            }
        
          tasklists.add(temptlist);
    
        }
        
        tmap.clear();
        for(Transplant__c t: [select id,Patient_last_name__c,Patient_first_name__c from Transplant__c where intake_status__c = 'Open' ]){
          tmap.put(t.id,t);
        }
        
        //Walmart Transplant Triggers – Visio #6 & #7
        targetDate = date.today().addDays(-30);
        for(Stage__c s : [select Transplant__c from Stage__c where Transplant__c IN :tmap.keySet() and Stage__c = 'Post' and stageTerm__c = :targetDate]){
        system.debug(s.transplant__c + ' stage transplant');    
          Task[] temptlist = new task[]{};
          Task[] temptlist1 = new task[]{};
          Transplant__c tempt = new Transplant__c();
          tempt = tmap.get(s.Transplant__c);
        
            for(User u : ulist){
                  
              Task task = new Task(
                            ownerid=u.id,
                            whatid=s.Transplant__c,
                            Subject= tempt.Patient_last_name__c + ', ' + tempt.Patient_first_name__c + ' - Request appointment dates from Mayo.');
                            
              temptlist.add(task);
            
            Task task1 = new Task(
                            ownerid=u.id,
                            whatid=s.Transplant__c,
                            Subject= tempt.Patient_last_name__c + ', ' + tempt.Patient_first_name__c + ' - Notify carrier that patient is in last month of the Transplant Benefit and will then be permanently transitioned out.');
                            
              temptlist1.add(task1);
                            
            }
        
          tasklists.add(temptlist);
          tasklists.add(temptlist1);    
    
        }
    
        for(list<Task> t : tasklists){
          insert t;
        }
    
    }
    
    public static void batchException(string message, integer lineNumber){
        list<messaging.Singleemailmessage> finalMail = new list<messaging.Singleemailmessage>();
        list<string> sendemailto = new list<string>(); 
        string BodyofEmail;
        sendemailto.add('mmartin@hdplus.com');
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setReplyTo('mmartin@hdplus.com');
        mail.setToAddresses(sendemailto);
        mail.setSenderDisplayName('Batch Error');
        bodyofEmail = 'Error' + '\n\n';              
        mail.setPlainTextBody(message+'\n\nLine: '+lineNumber);
        finalMail.add(mail);
            if(finalmail.size()>0){ 
                messaging.sendEmail(finalmail);
            }   
    
    }
}