public with sharing class UtilizationManagementDeleteRecord {
    date umDenyDate = date.valueof('1980-01-01');
    
    public void cancel() {
        //stupid salesforce trick
    }
    public pageReference deleteRecord() {
        error=false;
        if(irecordId==null){
            irecordId = ApexPages.CurrentPage().getParameters().get('deleteId');
            iRecordId = iRecordId.escapehtml3();
        }
        try{
            utilization_management__c um = new utilization_management__c(id=irecordId,softDelete__c=true);
            um.Event_Type__c='MED';
            um.Med_Cat__c='Medical';
            um.Admission_Date__c = umDenyDate;
            um.Approved_Through_Date__c = umDenyDate;
            um.Discharge_Date__c = umDenyDate;
            um.Approved_Days__c=0;
            um.Next_Review_Date__c=null;
            um.visits__c=null;
            um.Quality_Codes__c='RUQ';
            um.Comment_Line_for_Claims__c='DENIED:Created in error please disregard';
            um.case_status__c= 'Case Closed';
            um.validateCase__c=false;
            um.Submitted_for_Medical_Director_Review__c=null;
            update um;
            
            Utilization_Management_Note__c note = new Utilization_Management_Note__c(Utilization_Management__c=irecordId);
            note.Communication_Type__c='Note';
            note.Subject__c='Other';
            note.Body__c= 'Record Deleted by ' +userinfo.getfirstname()+' '+userinfo.getlastname();// + datetime.now() (Insert user and date time)';
            
            insert note;
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.error, e.getMessage())); 
        }
        return null;
    }
    
    public string irecordId {get; set;}
    public boolean error {get; set;}
}