@RestResource(urlMapping='/providerPortal/v1/providerportalpoc_pc')
global with sharing class ProviderPortalPocListener {

    class POC_user {
        string email;
        string firstName;
        string lastName;
        string lastModified;
    }

    class response {
        string error;
        string errorMsg;
        EcenProviderPlanOfCare__c poc;
        ProviderPortalPocSelectOptions.poc_selectoptions selectoptions;
        POC_user user;
    }

    @HttpGet
    global static void getplanofcare() {
        response result = new response();
        result.error = 'false';
        RestResponse res = RestContext.response;
        string caseName = RestContext.request.headers.get('EcenCase');
        // string doesPatientQualify = RestContext.request.headers.get('doesPatientQualify');
        // blob requestBodyBlob = RestContext.request.requestBody;

        ProviderPortalPocWorkflow.response poc_result;
        //system.debug(requestBodyBlob.toString());
        ProviderPortalPocWorkflow poc_wf =  new ProviderPortalPocWorkflow(caseName, '', '', 'false');
        poc_result = poc_wf.getplanofcare();

        result.poc = poc_result.poc;

        ProviderPortalPocSelectOptions options_setup = new ProviderPortalPocSelectOptions();

        ProviderPortalPocSelectOptions.poc_selectoptions opts = options_setup.getSelectOptions(poc_result.caseType, poc_result.clientFacility);
        result.selectoptions = opts;
        // set user
        POC_user u_poc = new POC_user();
        try {
            aws_user__c aws_user = [select email__c, firstname__c, lastName__c from aws_user__c where id = :poc_result.poc.Last_Modified_By_Portal_User__c];
            u_poc.email = aws_user.email__c;
            u_poc.firstName = aws_user.firstname__c;
            u_poc.lastName = aws_user.lastName__c;
        } catch(exception e) {
            u_poc.email = result.poc.lastModifiedBy.email;
            u_poc.firstName = result.poc.lastModifiedBy.firstName;
            u_poc.lastName = result.poc.lastModifiedBy.lastName;
        }

        try {
            u_poc.lastModified = result.poc.Last_Modified_By_Portal_User_DateTime__c.format('MMMMM dd, yyyy hh:mm:ss a');
        } catch (exception e) {
            if (result.poc.LastModifiedDate != null){
                u_poc.lastModified = result.poc.LastModifiedDate.format('MMMMM dd, yyyy hh:mm:ss a');
            }
        }
        result.user = u_poc;

        res.addHeader('Content-Type', 'application/json');

        try {
            system.debug(JSON.serialize(result));
            res.responseBody = Blob.valueOf(JSON.serialize(result));
            res.statusCode = 200;
        } catch (exception e) {
            result.error = 'true';
            result.errorMsg = e.getMessage();
            res.responseBody = Blob.valueOf(JSON.serialize(result));
            res.statusCode = 400;
        }
    }

    @HttpPost
    global static void saveplanofcare() {
        response result = new response();
        result.error = 'false';
        RestResponse res = RestContext.response;

        string caseName = RestContext.request.headers.get('EcenCase');
        string submittingPOC = 'false';

        string action = RestContext.request.headers.get('Action');
        string doesPatientQualify = RestContext.request.headers.get('doesPatientQualify');
        string doesPatientNeedPOC = RestContext.request.headers.get('doesPatientNeedPOC');

        blob requestBodyBlob = RestContext.request.requestBody;
        system.debug(requestBodyBlob.toString());
        system.debug('action ' + action);

        if (action == 'PUBLISH') {
          submittingPOC = 'true';
        }

        ProviderPortalPocWorkflow poc_wf = new ProviderPortalPocWorkflow(caseName, doesPatientQualify, doesPatientNeedPOC, requestBodyBlob.toString(), action);
        string savePoC = poc_wf.saveplanofcare_pc();
        switch on savePoC {
            when 'success' {
                res.addHeader('Content-Type', 'application/json');
                res.responseBody = Blob.valueOf(JSON.serialize(result));
                res.statusCode = 200;
            } when else {
                result.error = 'true';
                result.errorMsg = savePoC;
                res.responseBody = Blob.valueOf(JSON.serialize(result));
                res.statusCode = 400;
            }
        }

    }
}
