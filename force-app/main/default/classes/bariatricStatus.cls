public class bariatricStatus {
    private bariatricStatus(){
    }

    public static selectoption[] status(string stage){
        string tStage = stage;

        selectOption[] so = new selectOption[] {};

        so.add(new selectOption('Open', 'Open'));
        so.add(new selectOption('Pend', 'Pend'));
        so.add(new selectOption('Closed', 'Closed'));

        if (tStage == null) {
            tStage = 'intake';
        }
        else{
            tStage = tStage.tolowercase();
        }

        if (tStage == 'pre' || tStage == 'global' || tStage == 'post') {
            so.add(new selectOption('Completed', 'Completed'));
        }

        return so;
    }

    public static selectoption[] statusReason(string status, string tStage){
        string stage = tStage;
        string homeProvider = 'No Home Provider';

        selectOption[] so = new selectOption[] {
            new selectOption('', '--None--', false)
        };

        if (stage == null) {
            stage = 'intake';
        }
        else{
            stage = stage.tolowercase();
        }

        if (stage == 'intake') {
            if (status == 'Open') {
                so.add(new selectOption('In Process', 'In Process'));
                so.add(new selectOption('Validated', 'Validated'));
            }
            else if (status == 'Pend') {
                so.add(new selectOption('No Benefit Eligibility', 'No Benefit Eligibility'));
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('OON Negotiations', 'OON Negotiations'));
            }
            else if (status == 'Closed') {
                so.add(new selectOption('No Benefit Eligibility', 'No Benefit Eligibility'));
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('Incomplete', 'Incomplete'));
                so.add(new selectOption('Ineligible- Clinical', 'Ineligible- Clinical'));
                so.add(new selectOption('Inquiry', 'Inquiry'));
            }
        }
        else if (stage == 'pre') {
            if (status == 'Open') {
                so.add(new selectOption('Referred', 'Referred'));
                so.add(new selectOption('Pre-Qualification- In Process', 'Pre-Qualification- In Process'));
                so.add(new selectOption('Accepted- Appt date', 'Accepted- Appt date'));
                so.add(new selectOption('Ready- Eval', 'Ready- Eval'));
                so.add(new selectOption('Patient at COE', 'Patient at COE'));
                so.add(new selectOption('Claim Pending- Eval', 'Claim Pending- Eval'));
                so.add(new selectOption('Claim Received- Eval', 'Claim Received- Eval'));
            }
            else if (status == 'Pend') {
                so.add(new selectOption('Invalid Information', 'Invalid Information'));
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('Pre-Qualification', 'Pre-Qualification'));
                so.add(new selectOption('Clinical', 'Clinical'));
                so.add(new selectOption(homeProvider, homeProvider));
                so.add(new selectOption('OON Negotiations', 'OON Negotiations'));
            }
            else if (status == 'Closed') {
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('Pre-Qualification', 'Pre-Qualification'));
                so.add(new selectOption('Cancelled By Patient', 'Cancelled By Patient'));
                so.add(new selectOption('Cancelled By Facility', 'Cancelled By Facility'));
                so.add(new selectOption('No Benefit Eligibility', 'No Benefit Eligibility'));
            }
            else if (status == 'Completed') {
                so.add(new selectOption('Claim Paid', 'Claim Paid'));
                so.add(new selectOption('No Claim - PRE', 'No Claim - PRE'));
            }
        }
        else if (stage == 'global') {
            if (status == 'Open') {
                so.add(new selectOption('Awaiting Forms', 'Awaiting Forms'));
                so.add(new selectOption('Accepted- Appt date', 'Accepted- Appt date'));
                so.add(new selectOption('Ready- Surgery', 'Ready- Surgery'));
                so.add(new selectOption('Patient at COE', 'Patient at COE'));
                so.add(new selectOption('Claim Pending- Global', 'Claim Pending- Global'));
                so.add(new selectOption('Claim Received- Global', 'Claim Received- Global'));
            }
            else if (status == 'Pend') {
                so.add(new selectOption('Clinical', 'Clinical'));
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('Nicotine', 'Nicotine'));
                so.add(new selectOption(homeProvider, homeProvider));
                so.add(new selectOption('Caregiver', 'Caregiver'));
                so.add(new selectOption('OON Negotiations', 'OON Negotiations'));
            }
            else if (status == 'Closed') {
                so.add(new selectOption('Cancelled By Patient', 'Cancelled By Patient'));
                so.add(new selectOption('Cancelled By Facility', 'Cancelled By Facility'));
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('No Benefit Eligibility', 'No Benefit Eligibility'));
            }
            else if (status == 'Completed') {
                so.add(new selectOption('Claim Paid- Global', 'Claim Paid- Global'));
                so.add(new selectOption('No Claim - Global', 'No Claim - Global'));
            }
        }
        else if (stage == 'post') {
            if (status == 'Open') {
                so.add(new selectOption('Awaiting Forms', 'Awaiting Forms'));
                so.add(new selectOption('Accepted- Appt date', 'Accepted- Appt date'));
                so.add(new selectOption('Ready- Surgery', 'Ready- Surgery'));
                so.add(new selectOption('Patient at COE', 'Patient at COE'));
                so.add(new selectOption('Claim Pending- Post', 'Claim Pending- Post'));
                so.add(new selectOption('Claim Received- Post', 'Claim Received- Post'));
            }
            else if (status == 'Pend') {
                so.add(new selectOption('Clinical', 'Clinical'));
                so.add(new selectOption('Nicotine', 'Nicotine'));
                so.add(new selectOption(homeProvider, homeProvider));
                so.add(new selectOption('Caregiver', 'Caregiver'));
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('OON Negotiations', 'OON Negotiations'));
            }
            else if (status == 'Closed') {
                so.add(new selectOption('Cancelled By Patient', 'Cancelled By Patient'));
                so.add(new selectOption('Cancelled By Facility', 'Cancelled By Facility'));
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('No Benefit Eligibility', 'No Benefit Eligibility'));
            }
            else if (status == 'Completed') {
                so.add(new selectOption('Claim Paid- Post', 'Claim Paid- Post'));
                so.add(new selectOption('No Claim - Post', 'No Claim - Post'));
            }
        }

        return so;
    }

    public static selectoption[] subStatusReason(string statusReason, string tStage){
        string stage = tStage;
        string medicalRecords = 'Medical Records';

        selectOption[] so = new selectOption[] {
            new selectOption('', '--None--', false)
        };

        if (stage == null) {
            stage = 'intake';
        }
        else{
            stage = stage.tolowercase();
        }

        if (stage == 'intake') {
            if (statusReason == 'No Benefit Eligibility') {
                so.add(new selectOption('Dependent', 'Dependent'));
                so.add(new selectOption('Health Plan', 'Health Plan'));
            }
            else if (statusReason == 'In Process') {
                so.add(new selectOption('Awaiting Verification', 'Awaiting Verification'));
                so.add(new selectOption('Pending Eligibility', 'Pending Eligibility'));
            }
            else if (statusReason == 'Ineligible- Clinical') {
                so.add(new selectOption('BMI', 'BMI'));
                so.add(new selectOption('Type 2 Diabetes', 'Type 2 Diabetes'));
                so.add(new selectOption('BMI & Type 2 Diabetes', 'BMI & Type 2 Diabetes'));
            }
            else if (statusReason == 'Inquiry') {
                so.add(new selectOption('Carrier', 'Carrier'));
                so.add(new selectOption('Patient', 'Patient'));
                so.add(new selectOption('Provider', 'Provider'));
                so.add(new selectOption('Other', 'Other'));
            }
            else if (statusReason == 'Infectious Disease') {
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('Disengaged', 'Disengaged'));
                so.add(new selectOption('Local Hospital', 'Local Hospital'));
                so.add(new selectOption('Patient Declined', 'Patient Declined'));
                so.add(new selectOption('Reduced Symptoms', 'Reduced Symptoms'));
            }
            else if (statusReason == 'OON Negotiations') {
                so.add(new selectOption('New', 'New'));
                so.add(new selectOption('Awaiting NNP from GR', 'Awaiting NNP from GR'));
                so.add(new selectOption('In Discussions', 'In Discussions'));
                so.add(new selectOption('Negotiations Completed', 'Negotiations Completed'));
                so.add(new selectOption('Discontinued', 'Discontinued'));
                so.add(new selectOption('Discussions on Hold', 'Discussions on Hold'));
            }
        }
        else if (stage == 'pre') {
            if (statusReason == 'No Benefit Eligibility') {
                so.add(new selectOption('Dependent', 'Dependent'));
                so.add(new selectOption('Health Plan', 'Health Plan'));
            }
            else if (statusReason == 'Pre-Qualification- In Process') {
                so.add(new selectOption('Introduction Call', 'Introduction Call'));
                so.add(new selectOption('Educational Webinar', 'Educational Webinar'));
                so.add(new selectOption('Patient Questionnaire', 'Patient Questionnaire'));
                so.add(new selectOption(medicalRecords, medicalRecords));
            }
            else if (statusReason == 'Pre-Qualification') {
                so.add(new selectOption('Disengaged', 'Disengaged'));
            }
            else if (statusReason == 'Clinical') {
                so.add(new selectOption('Medical Condition', 'Medical Condition'));
                so.add(new selectOption('Medical Clearance', 'Medical Clearance'));
            }
            else if (statusReason == 'Cancelled By Patient') {
                so.add(new selectOption('Local', 'Local'));
                so.add(new selectOption('Financial', 'Financial'));
                so.add(new selectOption('Disengaged', 'Disengaged'));
                so.add(new selectOption('Personal', 'Personal'));
                so.add(new selectOption('Work', 'Work'));
                so.add(new selectOption('Home Provider', 'Home Provider'));
            }
            else if (statusReason == 'Cancelled By Facility') {
                so.add(new selectOption('Deferred', 'Deferred'));
                so.add(new selectOption('Declined', 'Declined'));
                so.add(new selectOption('Non-compliance', 'Non-compliance'));
                so.add(new selectOption('BMI < 35', 'BMI < 35'));
                so.add(new selectOption(medicalRecords, medicalRecords));
            }
            else if (statusReason == 'No Claim - PRE') {
                so.add(new selectOption('Warranty', 'Warranty'));
                so.add(new selectOption('Bundle', 'Bundle'));
                so.add(new selectOption('Cancel After Arrival', 'Cancel After Arrival'));
                so.add(new selectOption('Billed Carrier', 'Billed Carrier'));
                so.add(new selectOption('Timely Filing', 'Timely Filing'));
            }
            else if (statusReason == 'Infectious Disease') {
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('Disengaged', 'Disengaged'));
                so.add(new selectOption('Local Hospital', 'Local Hospital'));
                so.add(new selectOption('Patient Declined', 'Patient Declined'));
                so.add(new selectOption('Reduced Symptoms', 'Reduced Symptoms'));
            }
            else if (statusReason == 'OON Negotiations') {
                so.add(new selectOption('New', 'New'));
                so.add(new selectOption('Awaiting NNP from GR', 'Awaiting NNP from GR'));
                so.add(new selectOption('In Discussions', 'In Discussions'));
                so.add(new selectOption('Negotiations Completed', 'Negotiations Completed'));
                so.add(new selectOption('Discontinued', 'Discontinued'));
                so.add(new selectOption('Discussions on Hold', 'Discussions on Hold'));
            }
        }
        else if (stage == 'global') {
            if (statusReason == 'No Benefit Eligibility') {
                so.add(new selectOption('Dependent', 'Dependent'));
                so.add(new selectOption('Health Plan', 'Health Plan'));
            }
            else if (statusReason == 'Clinical') {
                so.add(new selectOption('Medical Condition', 'Medical Condition'));
                so.add(new selectOption('Medical Clearance', 'Medical Clearance'));
            }
            else if (statusReason == 'Cancelled by Patient') {
                so.add(new selectOption('Local', 'Local'));
                so.add(new selectOption('Financial', 'Financial'));
                so.add(new selectOption('Disengaged', 'Disengaged'));
                so.add(new selectOption('Personal', 'Personal'));
                so.add(new selectOption('Work', 'Work'));
                so.add(new selectOption('Caregiver', 'Caregiver'));
                so.add(new selectOption('Home Provider', 'Home Provider'));
            }
            else if (statusReason == 'Cancelled by Facility') {
                so.add(new selectOption('Deferred', 'Deferred'));
                so.add(new selectOption('Declined', 'Declined'));
                so.add(new selectOption('Non-compliance', 'Non-compliance'));
                so.add(new selectOption('BMI < 35', 'BMI < 35'));
                so.add(new selectOption(medicalRecords, medicalRecords));
            }
            else if (statusReason == 'No Claim - Global') {
                so.add(new selectOption('Warranty', 'Warranty'));
                so.add(new selectOption('Bundle', 'Bundle'));
                so.add(new selectOption('Cancel After Arrival', 'Cancel After Arrival'));
                so.add(new selectOption('Billed Carrier', 'Billed Carrier'));
                so.add(new selectOption('Timely Filing', 'Timely Filing'));
            }
            else if (statusReason == 'Infectious Disease') {
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('Disengaged', 'Disengaged'));
                so.add(new selectOption('Local Hospital', 'Local Hospital'));
                so.add(new selectOption('Patient Declined', 'Patient Declined'));
                so.add(new selectOption('Reduced Symptoms', 'Reduced Symptoms'));
            }
            else if (statusReason == 'OON Negotiations') {
                so.add(new selectOption('New', 'New'));
                so.add(new selectOption('Awaiting NNP from GR', 'Awaiting NNP from GR'));
                so.add(new selectOption('In Discussions', 'In Discussions'));
                so.add(new selectOption('Negotiations Completed', 'Negotiations Completed'));
                so.add(new selectOption('Discontinued', 'Discontinued'));
                so.add(new selectOption('Discussions on Hold', 'Discussions on Hold'));
            }
        }
        else if (stage == 'post') {
            if (statusReason == 'No Benefit Eligibility') {
                so.add(new selectOption('Dependent', 'Dependent'));
                so.add(new selectOption('Health Plan', 'Health Plan'));
            }
            else if (statusReason == 'Clinical') {
                so.add(new selectOption('Medical Condition', 'Medical Condition'));
                so.add(new selectOption('Medical Clearance', 'Medical Clearance'));
            }
            else if (statusReason == 'Cancelled by Patient') {
                so.add(new selectOption('Local', 'Local'));
                so.add(new selectOption('Financial', 'Financial'));
                so.add(new selectOption('Disengaged', 'Disengaged'));
                so.add(new selectOption('Personal', 'Personal'));
                so.add(new selectOption('Work', 'Work'));
                so.add(new selectOption('Caregiver', 'Caregiver'));
                so.add(new selectOption('Home Provider', 'Home Provider'));
            }
            else if (statusReason == 'Cancelled by Facility') {
                so.add(new selectOption('Deferred', 'Deferred'));
                so.add(new selectOption('Declined', 'Declined'));
                so.add(new selectOption('Non-compliance', 'Non-compliance'));
                so.add(new selectOption('BMI < 35', 'BMI < 35'));
                so.add(new selectOption(medicalRecords, medicalRecords));
            }
            else if (statusReason == 'No Claim - Post') {
                so.add(new selectOption('Warranty', 'Warranty'));
                so.add(new selectOption('Bundle', 'Bundle'));
                so.add(new selectOption('Cancel After Arrival', 'Cancel After Arrival'));
                so.add(new selectOption('Billed Carrier', 'Billed Carrier'));
                so.add(new selectOption('Timely Filing', 'Timely Filing'));
            }
            else if (statusReason == 'Infectious Disease') {
                so.add(new selectOption('Infectious Disease', 'Infectious Disease'));
                so.add(new selectOption('Disengaged', 'Disengaged'));
                so.add(new selectOption('Local Hospital', 'Local Hospital'));
                so.add(new selectOption('Patient Declined', 'Patient Declined'));
                so.add(new selectOption('Reduced Symptoms', 'Reduced Symptoms'));
            }
            else if (statusReason == 'OON Negotiations') {
                so.add(new selectOption('New', 'New'));
                so.add(new selectOption('Awaiting NNP from GR', 'Awaiting NNP from GR'));
                so.add(new selectOption('In Discussions', 'In Discussions'));
                so.add(new selectOption('Negotiations Completed', 'Negotiations Completed'));
                so.add(new selectOption('Discontinued', 'Discontinued'));
                so.add(new selectOption('Discussions on Hold', 'Discussions on Hold'));
            }
        }

        return so;
    }
}
