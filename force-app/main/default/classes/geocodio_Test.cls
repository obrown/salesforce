@IsTest
public class geocodio_Test {
    
    // This test method should give 100% coverage
    static testMethod void testParse() {
        String json = '{\"input\":{\"address_components\":{\"number\":\"123\",\"street\":\"Main\",\"suffix\":\"St\",\"formatted_street\":\"Main St\",\"city\":\"Boston\",\"state\":\"MA\",\"zip\":\"02109\",\"country\":\"US\"},\"formatted_address\":\"123 Main St, Boston, MA 02109\"},\"results\":[{\"address_components\":{\"number\":\"103\",\"predirectional\":\"E\",\"street\":\"Main\",\"suffix\":\"St\",\"formatted_street\":\"E Main St\",\"city\":\"Boston\",\"county\":\"Suffolk County\",\"state\":\"MA\",\"zip\":\"02124\",\"country\":\"US\"},\"formatted_address\":\"103 E Main St, Boston, MA 02124\",\"location\":{\"lat\":42.292721,\"lng\":-71.093689},\"accuracy\":0.6,\"accuracy_type\":\"nearest_rooftop_match\",\"source\":\"Office of Geographic Information (MassGIS), Commonwealth of Massachusetts, MassIT\"}]}';
        geocodio r = geocodio.parse(json);
        System.assert(r != null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        geocodio.Address_components objAddress_components = new geocodio.Address_components(System.JSON.createParser(json));
        System.assert(objAddress_components != null);
        System.assert(objAddress_components.number_Z == null);
        System.assert(objAddress_components.street == null);
        System.assert(objAddress_components.suffix == null);
        System.assert(objAddress_components.formatted_street == null);
        System.assert(objAddress_components.city == null);
        System.assert(objAddress_components.state == null);
        System.assert(objAddress_components.zip == null);
        System.assert(objAddress_components.country == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        geocodio.Input objInput = new geocodio.Input(System.JSON.createParser(json));
        System.assert(objInput != null);
        System.assert(objInput.address_components == null);
        System.assert(objInput.formatted_address == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        geocodio objgeocodio = new geocodio(System.JSON.createParser(json));
        System.assert(objgeocodio != null);
        System.assert(objgeocodio.input == null);
        System.assert(objgeocodio.results == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        geocodio.Results objResults = new geocodio.Results(System.JSON.createParser(json));
        System.assert(objResults != null);
        System.assert(objResults.address_components == null);
        System.assert(objResults.formatted_address == null);
        System.assert(objResults.location == null);
        System.assert(objResults.accuracy == null);
        System.assert(objResults.accuracy_type == null);
        System.assert(objResults.source == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        geocodio.Address_components_Z objAddress_components_Z = new geocodio.Address_components_Z(System.JSON.createParser(json));
        System.assert(objAddress_components_Z != null);
        System.assert(objAddress_components_Z.number_Z == null);
        System.assert(objAddress_components_Z.predirectional == null);
        System.assert(objAddress_components_Z.street == null);
        System.assert(objAddress_components_Z.suffix == null);
        System.assert(objAddress_components_Z.formatted_street == null);
        System.assert(objAddress_components_Z.city == null);
        System.assert(objAddress_components_Z.county == null);
        System.assert(objAddress_components_Z.state == null);
        System.assert(objAddress_components_Z.zip == null);
        System.assert(objAddress_components_Z.country == null);

        json = '{\"TestAMissingObject\": { \"TestAMissingArray\": [ { \"TestAMissingProperty\": \"Some Value\" } ] } }';
        geocodio.Location objLocation = new geocodio.Location(System.JSON.createParser(json));
        System.assert(objLocation != null);
        System.assert(objLocation.lat == null);
        System.assert(objLocation.lng == null);
        
        geocodioGeoCoding.lookupAddress('','','','');
    }
}