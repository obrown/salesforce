/**
* This class contains unit tests for validating the behavior of the hpPatientSearchStruct class
*/

@isTest
private class hpPatientSearchStructTest {

    static testMethod void UnitTest() {
        
        hpPatientSearchStruct.PatSearchResultLogs psrl = new hpPatientSearchStruct.PatSearchResultLogs('008','CP3','123456789','John','Doe','19800101','00',null,'John','Doe','19800101','01','Active','Cooper');
        hpPatientSearchStruct.PatSearchResultLogs psr2 = new hpPatientSearchStruct.PatSearchResultLogs('008','CP3','123456789','John','Doe','19800101',null,null,null,null,null,'00',null,'John','Doe','19800101','01','Active',null,null,'Cooper');
        
        hpPatientSearchStruct hpss = new hpPatientSearchStruct();
        
        hpss.PatSearchResultLogs = new hpPatientSearchStruct.PatSearchResultLogs[]{psrl}; 
        hpss.cleanUpLog();
    
    }
    
    
}