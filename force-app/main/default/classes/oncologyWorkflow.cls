public with sharing class oncologyWorkflow {
    // merge into masterBranch
  public class response {
      public string resultCode;
      public boolean error;
      public string message;
  }

  public static void selfSubcriber(set<oncology__c> oList){
      for (oncology__c o : oList) {
          o.Employee_Last_Name__c = o.Patient_Last_Name__c;
          o.Employee_First_Name__c = o.Patient_First_Name__c;
          o.Employee_DOB__c = o.Patient_DOB__c;
          o.Employee_Gender__c = o.Gender__c;
          o.Employee_Email_Address__c = o.Patient_Email_Address__c;

          o.Employee_Preferred_Phone__c = o.Patient_Preferred_Phone__c;
          o.Employee_Phone__c = o.Patient_Phone__c;
          o.Employee_Mobile_Phone__c = o.Patient_Phone2__c;
          o.Employee_Other_Phone__c = o.Patient_Phone3__c;
          o.Employee_Preferred_Contact_Time__c = o.Preferred_Contact_Time__c;
      }
  }

  public static void sameAddress(set<oncology__c> oList){
      for (oncology__c o : oList) {
          o.Employee_Address__c = o.Patient_Address__c;
          o.Employee_City__c = o.Patient_City__c;
          o.Employee_State__c = o.Patient_State__c;
          o.Employee_Zip_Code__c = o.Patient_Zip_Code__c;
          o.Employee_Country__c = o.Patient_Country__c;
      }
  }

  public static void formatPDOB(set<oncology__c> oList){
      for (oncology__c o : oList) {
          o.Patient_Age__c = date.today().year() - date.valueOf(o.Patient_DOB__c).year();

          if (date.valueOf(o.Patient_DOB__c).month() > date.today().month()) {
              o.Patient_Age__c = o.Patient_Age__c - 1;
          }
          else if (date.valueOf(o.Patient_DOB__c).month() == date.today().month()) {
              if (date.valueOf(o.Patient_DOB__c).day() > date.today().day()) {
                  o.Patient_Age__c = o.Patient_Age__c - 1;
              }
          }
      }
  }

  public static void pocAuth(set<oncology__c> oList){
      for (oncology__c o : oList) {
          o.Send_POC_Auth__c = false;
          //o.Facility_Managing_Physician__c!=null &&
          /* (o.Evaluation_Date_1__c!=null || o.Evaluation_Date_2__c!=null) &&
           * o.Patient_FAT_Hours__c !=null &&
           * o.PatientFAT_Minutes__c!=null &&
           * o.PatientFAT_AMPM__c!=null &&
           * o.Patient_FAT_Timezone__c!=null &&
           * o.patient_LAT_Hours__c!=null &&
           * o.PatientLAT_Minutes__c!=null &&
           * o.PatientLAT_AMPM__c!=null &&
           * o.Patient_LAT_Timezone__c!=null &&
           * o.Last_Appointment_Date__c!=null &&*/
          //(o.Outpatient_Psychiatric__c==true||
          //o.Diagnostic_Radiology_Review__c==true||
          //o.Additional_Pathology_Review__c==true||
          //o.All_other_necessary_evaluation__c==true||
          //o.Addl_diagnostics_outside_bundle_None__c==true)
          if (o.Proposed_Service_Type__c == 'Virtual Evaluation' &&
              o.Consultation_Types__c != null &&
              o.Facility_Plan_of_Care_Submission_Date__c != null &&
              o.Plan_of_Care_Accepted_Date__c != null &&
              o.Date_HDP_Forms_Completed__c != null &&
              o.HDP_Program_Forms_Completed__c == true &&
              o.Patient_Agree_to_POC__c == 'Yes'
              ) {
              o.Send_POC_Auth__c = true;
          }
          else if (o.Proposed_Service_Reason__c != null &&
                   o.Consultation_Types__c != null &&
                   o.Facility_Plan_of_Care_Submission_Date__c != null &&
                   o.Proposed_Service_Type__c != null &&
                     //o.Facility_Managing_Physician__c!=null &&
                   o.Plan_of_Care_Accepted_Date__c != null &&
                   /*(o.Evaluation_Date_1__c!=null || o.Evaluation_Date_2__c!=null) &&
                    * o.Patient_FAT_Hours__c !=null &&
                    * o.PatientFAT_Minutes__c!=null &&
                    * o.PatientFAT_AMPM__c!=null &&
                    * o.Patient_FAT_Timezone__c!=null &&
                    * o.patient_LAT_Hours__c!=null &&
                    * o.PatientLAT_Minutes__c!=null &&
                    * o.PatientLAT_AMPM__c!=null &&
                    * o.Patient_LAT_Timezone__c!=null &&
                    * o.Last_Appointment_Date__c!=null &&
                    * o.Estimated_Arrival_Date__c!=null &&
                    * o.Estimated_Departure_Date__c!=null &&*/
                   o.Recommended_Mode_of_Transportation__c != null &&
                   o.Allowable_Mode_of_Transportation__c != null &&
                   o.Date_HDP_Forms_Completed__c != null &&
                   o.HDP_Program_Forms_Completed__c == true &&
                   o.Patient_Agree_to_POC__c == 'Yes'
                     //(o.Outpatient_Psychiatric__c==true||
                     //o.Diagnostic_Radiology_Review__c==true||
                     //o.Additional_Pathology_Review__c==true||
                     //o.All_other_necessary_evaluation__c==true||
                     //o.Addl_diagnostics_outside_bundle_None__c==true)
                   ) {
              o.Send_POC_Auth__c = true;
          }
      }
  }

  public static response requestIDCard(oncology__c obj){
      response response = new response();

      response.resultCode = '2';
      response.error = false;
      try{
          oncologyCaseEncryptedData foo = new oncologyCaseEncryptedData();
          foo.patientName = obj.patient_first_name__c + ' ' + obj.patient_last_name__c;
          foo.patientStreet = obj.Patient_Address__c;
          foo.patientCSZ = obj.patient_city__c + ', ' + obj.patient_state__c + ' ' + obj.Patient_Zip_Code__c;

          foo.id = obj.id;

          Messaging.SingleEmailMessage[] mailRoom = new Messaging.SingleEmailMessage[] {};

          PageReference oncologyIDCard = page.oncologyIDCard;
          oncologyIDCard.getParameters().put('cases', JSON.serialize(foo));
          system.debug(JSON.serialize(foo));
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          string[] sendemailto = new string[] {};

          if (utilities.IsRunninginSandbox() || obj.Client_Facility__r.Authorization_Email_Contact__c == null) {
              sendemailto.add(userInfo.getUserEmail());
          }
          else{
              if (obj.Client_Facility__r.Authorization_Email_Contact__c.contains((','))) {
                  sendemailto = obj.Client_Facility__r.Authorization_Email_Contact__c.removeEnd(',').split(',');
              }
              else{
                  sendemailto.add(obj.Client_Facility__r.Authorization_Email_Contact__c);
              }
          }

          mail.setToAddresses(sendemailto);
          mail.setSubject(obj.Client_Name__c + ' Cancer Care Program ID Card  – ' + obj.Patient_Last_Name__c + ', ' + obj.Patient_First_Name__c);
          mail.setPlainTextBody('Hello,\n\nAttached is the Contigo Health ID Card for the Cancer Care program. If you have questions regarding' +
              ' the ID Card or the process, please contact us at 1-844-207-8126 or HDPCancerNurse@contigohealth.com.\n\nThank you,\nCancer Patient Advocate Nurse\n' +
              'Contigo Health\n\n[ID Card is attachment]');
          Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
          string fileName =
              obj.Patient_Last_Name__c +
              ', ' +
              obj.Patient_First_Name__c +
              ' ID Card.pdf';
          efa.setFileName(obj.Patient_Last_Name__c + ', ' + obj.Patient_First_Name__c + ' ID Card.pdf');

          blob content = blob.valueof('UNIT TEST');
          if (!Test.IsRunningTest()) {
              content = oncologyIDCard.getContent();
          }

          efa.setBody(content);

          mail.setFileAttachments(new Messaging.EmailFileAttachment[] { efa });
          mail.setReplyTo('HDPCancerNurse@contigohealth.com');
          mail.setSenderDisplayName('HDPCancerNurse@contigohealth.com');
          mailRoom.add(mail);
          Messaging.sendEmail(mailRoom);

          /*
           * Attach ID card to Oncology Record
           */

          attachment attachment = new attachment();
          attachment.name = fileName;
          attachment.parentId = obj.id;
          attachment.body = content;
          insert attachment;

          ftpAttachment__c ftpAttachment = new ftpAttachment__c(
              oncology__c = obj.id
              );

          ftpAttachment.subDirectory__c = oncologyUtilities.formatSubdirectory(obj);
          ftpAttachment.fileName__c = fileName;
          ftpAttachment.file_Description__c = 'Oncology Id Card';
          ftpAttachment.attachmentID__c = attachment.id;
          insert ftpAttachment;

          Messaging.sendEmail(mailRoom);

            //1-4-20 update start
          Oncology_Case_Note__c newNote = new Oncology_Case_Note__c();

          newNote.Oncology__c = obj.id;
          newNote.Subject__c = 'ID Card';
          newNote.Body__c = 'ID Card has been created and sent to admin to print';
          newNote.Contact_Type__c = 'Other';
          newNote.Communication_Type__c = 'Email';

          Attachment theIdCard = new Attachment(
              parentId = obj.ID,
              name = obj.Patient_Last_Name__c +
                     ', ' +
                     obj.Patient_First_Name__c +
                     ' ID Card.pdf',
              body = efa.getBody()
              );

          insert theIDCard;
          newNote.attachment__c = true;
          newNote.attachID__c = theIDCard.id;
          insert newNote;

          //1-4-20 update end

          obj.ID_Card_Requested__c = date.today();
          update obj;
      }catch (exception e) {
          response.Message = e.getMessage();
          response.error = true;
      }

      response.ResultCode = '0';
      response.Message = 'ID Card Request Sent';

      return response;
  }

  public static response requestEmailReferral(oncology__c obj){
      response response = new response();

      response.error = false;

      string[] reqList = oncologyRequiredToRefer.checkRequirements(obj);
      system.debug('reqList ' + reqList);
      if (reqList.isEmpty()) {
          try{
              oncologyCaseEncryptedData foo = new oncologyCaseEncryptedData();
              foo.patientStreet = obj.Patient_Address__c;
              foo.patientFirstName = obj.Patient_First_Name__c;
              foo.patientLastName = obj.Patient_Last_Name__c;
              foo.preferredName = obj.Patient_Preferred_Name__c;
              foo.patientDOB = obj.Patient_DOB__c;
              foo.employeeDOB = obj.Employee_DOB__c;
              foo.employeeFirstName = obj.Employee_First_Name__c;
              foo.employeeLastName = obj.Employee_Last_Name__c;
              foo.patientEmail = obj.patient_email_address__c;//3-16-22 added o.brown
              
              foo.id = obj.id;
              Messaging.SingleEmailMessage[] mailRoom = new Messaging.SingleEmailMessage[] {};

              PageReference oncologyManualReferralEmail = page.oncologyManualReferralEmail;
              oncologyManualReferralEmail.getParameters().put('cases', JSON.serialize(foo));

              Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
              string[] sendemailto = new string[] {};
              if (utilities.IsRunninginSandbox() || obj.Client_Facility__r.Referral_Email_Contact__c == null) {
                  sendemailto.add(userInfo.getUserEmail());
              }
              else{
                  if (obj.Client_Facility__r.Referral_Email_Contact__c.contains((','))) {
                      sendemailto = obj.Client_Facility__r.Referral_Email_Contact__c.removeEnd(',').split(',');
                  }
                  else{
                      sendemailto.add(obj.Client_Facility__r.Referral_Email_Contact__c);
                  }
              }
              mail.setToAddresses(sendemailto);
              mail.setSubject('New Referral for ' + obj.Client_Name__c + ' Cancer Care Program – ' + obj.Patient_Last_Name__c + ', ' + obj.Patient_First_Name__c);
              mail.setPlainTextBody('Hello,\n\nAttached is the Health Design Plus Referral Form for the Cancer Care program. If you have questions,\nregarding this form or the process, please contact us at 1-844-207-8126 or HDPCancerNurse@contigohealth.com.\n\nThank you,\nCancer Patient Advocate Nurse\nHealth Design Plus\n\n[Referral form is attachment]');
              Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
              efa.setFileName(obj.Patient_Last_Name__c + ', ' + obj.Patient_First_Name__c + ' Manual Referral Form.pdf');

              blob content = blob.valueof('UNIT TEST');
              if (!Test.IsRunningTest()) {
                  content = oncologyManualReferralEmail.getContent();
              }

              efa.setBody(content);

              mail.setFileAttachments(new Messaging.EmailFileAttachment[] { efa });
              mail.setReplyTo('HDPCancerNurse@contigohealth.com');
              mail.setSenderDisplayName('HDPCancerNurse@contigohealth.com');
              mailRoom.add(mail);

              Messaging.sendEmail(mailRoom);

              obj.Referral_Date__c = datetime.now();
              update obj;
          }catch (exception e) {
              response.Message = e.getMessage();
              response.error = true;
          }

          response.Message = 'Manual Referral Form Sent';
          response.error = false;
          return response;
      }
      else{
          response.error = true;
          string strReqList = '';
          for (string s : reqList) {
              strReqList += s + '\n';
          }

          response.Message = strReqList;
          return response;
      }
  }

  public static response requestAuthorizationNumber(oncology__c onc){
      response response = new response();

      response.resultCode = '2';
      response.error = false;

      try{
          Messaging.SingleEmailMessage[] mailRoom = new Messaging.SingleEmailMessage[] {};
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          string[] sendemailto = new string[] {};
          if (utilities.IsRunninginSandbox() || onc.Client_Facility__r.Authorization_Email_Contact__c == null) {
              sendemailto.add(userInfo.getUserEmail());
              mail.setCCAddresses(new string[] { userInfo.getUserEmail() });
          }
          else{
              if (onc.Client_Facility__r.Authorization_Email_Contact__c.contains((','))) {
                  sendemailto = onc.Client_Facility__r.Authorization_Email_Contact__c.removeEnd(',').split(',');
              }
              else{
                  sendemailto.add(onc.Client_Facility__r.Authorization_Email_Contact__c);
              }

              mail.setCCAddresses(new string[] { 'HDPCancerNurse@contigohealth.com' });
          }
          mail.setToAddresses(sendemailto);

          mail.setSubject(onc.Client_Name__c + ' Cancer Care Program Authorization Number – ' + onc.Patient_Last_Name__c + ',' + onc.Patient_First_Name__c);
          mail.setPlainTextBody('Hello,\n\nThe HDP Authorization Number for ' + onc.Patient_Last_Name__c + ', ' + onc.Patient_First_Name__c + ' is ' + onc.Authorization_Number__c + '.\n\n' +
              'If you have questions regarding this process, please contact us at, please contact us at 1-844-207-8126\nor HDPCancerNurse@contigohealth.com.\n\nThank you,\n' +
              'Cancer Patient Advocate Nurse\nHealth Design Plus');
          mail.setReplyTo('HDPCancerNurse@contigohealth.com');
          mail.setSenderDisplayName('HDPCancerNurse@contigohealth.com');
          mailRoom.add(mail);

          Messaging.sendEmail(mailRoom);

          onc.Date_Auth_Number_Sent__c = date.today();
          update onc;

          Oncology_Case_Note__c note = new Oncology_Case_Note__c(Oncology__c = onc.id);
          note.Subject__c = 'Authorization number emailed';
          note.Body__c = 'Authorization number ' + onc.Authorization_Number__c + ' emailed to the facility using email address ' + sendemailto;
          note.Initiated__c = 'Outbound';
          note.Communication_Type__c = 'Email';
          note.Contact_Type__c = 'Facility';

          insert note;
      }catch (exception e) {
          response.Message = e.getMessage();
          response.error = true;
      }

      response.ResultCode = '0';
      response.Message = 'Auth Number sent';

      return response;
  }

  public static void sendOncologyEligibilitySpreadsheet(){
      string body = 'Cert ID, Employee First Name, Employee Last Name, Employee SSN, Employee DOB, Employee Gender, Relationship to the Insured, Patient First Name, Patient Last Name, Patient SSN, Patient DOB, Patient Gender, Employee Street, Employee City, Employee State, Employee Zip, HP Group Number, HP Plan Name, HP Effective Date, HP Termination Date\n';

      oncology__c[] mailingList = new oncology__c[] {};
      map<string, string> hpMap = new map<string, string>();
      map<string, string> casePlanNameMap = new map<string, string>();

      mailingList = [select client_facility__c,
                     Client__r.Group_Number__c,
                     Patient_Medical_Plan_Number__c,
                     Employee_First_Name__c,
                     Employee_Last_Name__c,
                     Employee_DOB__c,
                     Employee_Gender__c,
                     Employee_SSN__c,
                     Relationship_to_Subscriber__c,
                     Patient_First_Name__c,
                     Patient_Last_Name__c,
                     Patient_DOB__c,
                     Patient_SSN__c,
                     Gender__c,
                     Employee_Address__c,
                     Employee_City__c,
                     Employee_State__c,
                     Employee_Zip_Code__c,
                     Employee_Healthplan__c,
                     Evaluation_Actual_Arrival__c,
                     Evaluation_Actual_Departure__c,
                     Healthpac_Group__c,
                     HP_Plan_Name__c,
                     HP_Effective_Date__c,
                     HP_Termination_Date__c from Oncology__c where createHP__c = true];

      if (!mailingList.isempty()) {
          for (oncology__c o : mailingList) {
              hpMap.put(o.client_facility__c, null);
              casePlanNameMap.put(o.Employee_Healthplan__c, null);
          }

          for (Client_Employee_Healthplan__c hpc : [select name from Client_Employee_Healthplan__c where id in :casePlanNameMap.keySet()]) {
              casePlanNameMap.put(hpc.id, hpc.name);
          }

          for (HealthPac_Plan_Code__c hpc : [select Case_Plan_Name__c, client_facility__c, HealthPac_Plan_Code__c from HealthPac_Plan_Code__c where client_facility__c in :hpMap.keySet()]) {
              hpMap.put(hpc.client_facility__c + '' + hpc.case_plan_name__c, hpc.HealthPac_Plan_Code__c);
          }

          for (oncology__c o : mailingList) {
              date ead = o.Evaluation_Actual_Departure__c;
              if (ead != null) {
                  ead = ead.addDays(1);
              }
              o.HP_Plan_Name__c = hpMap.get(o.client_facility__c + '' + casePlanNameMap.get(o.Employee_Healthplan__c));
              body = body + o.Patient_Medical_Plan_Number__c + ',' + o.Employee_First_Name__c + ',' + o.Employee_Last_Name__c + ',' + o.Employee_SSN__c + ',' + o.Employee_DOB__c + ',' + o.Employee_Gender__c + ',' + o.Relationship_to_Subscriber__c + ',' + o.Patient_First_Name__c + ',' + o.Patient_Last_Name__c + ',,' + o.Patient_DOB__c + ',' + o.Gender__c + ',' + o.Employee_Address__c + ',' + o.Employee_City__c + ',' + o.Employee_State__c + ',' + o.Employee_Zip_Code__c + ',' + o.Client__r.Group_Number__c + ',' + o.HP_Plan_Name__c + ',' + o.Evaluation_Actual_Arrival__c + ',' + ead + '\n';
              o.Loaded_into_HealthPac__c = date.today();
              o.createHP__c = false;
          }

          body = body.replaceAll('null', '');
          body = body.replaceAll(' 00:00:00', '');

          transplantEligCheck.mailthecsv(body, 'New Oncology Cases for HealthPAC', 'Please find a list of new Oncology Cases for HealthPAC');

          update mailingList;
      }
      else{
          transplantEligCheck.mailthecsv('1', 'New Oncology Cases for HealthPAC - NONE', 'There are no new Oncology Cases for HealthPAC');
      }
  }

  public static void receiveRecordsFromCoH(map<id, Oncology_Clinical_Facility__c> determinationMap, map<id, Oncology_Clinical_Facility__c> planOfCareMap, boolean addPhysician){
      boolean errorOccurred = false;
      string errorSavingPOC = '';

      oncology__c[] oncologyList = new oncology__c[] {};
      Oncology_Physician__c[] physicians = new Oncology_Physician__c[] {};

      boolean addtolist = false;

      for (oncology__c oncology : [select Addl_diagnostics_outside_bundle_None__c,
                                   Additional_diagnostics_outside_bundle__c,
                                   Additional_Pathology_Notes__c,
                                   Additional_Pathology_Review__c,
                                   Allowable_Mode_of_Transportation__c,
                                   Associated_Facilities__c,
                                   All_Other_Evaluation_Notes__c,
                                   All_other_necessary_evaluation__c,
                                   Date_of_Diagnosis__c,
                                   Date_of_Diagnosis_Patient_Reported__c,
                                   Determination_Date__c,
                                   Determination_Outcome__c,
                                   Determination_Outcome_Reason__c,
                                   Diagnostic_Radiology_Notes__c,
                                   Diagnostic_Radiology_Review__c,
                                   First_Appointment_Date__c,
                                   Patient_LAT_Hours__c,
                                   PatientLAT_Minutes__c,
                                   PatientLAT_AMPM__c,
                                   PatientFAT_AMPM__c,
                                   Patient_FAT_Hours__c,
                                   PatientFAT_Minutes__c, Clinical_Notes__c, Consultation_Types__c, Estimated_Arrival_Date__c, Estimated_Departure_Date__c, Evaluation_Date__c, Facility_Managing_Physician__c, First_Appointment_Time__c, Outpatient_Psychiatric__c, Outpatient_Psychiatric_Note__c, Patient_DOB__c, Patient_First_Name__c, Patient_Last_Name__c, Patient_State__c, Facility_Plan_of_Care_Submission_Date__c, Primary_Diagnosis__c, Primary_Diagnosis_Patient_Reported__c, Proposed_Service_Type__c, Recommended_Mode_of_Transportation__c, Treatment__c from oncology__c where(id in :planOfCareMap.keyset() or id in :determinationMap.keyset())]) {
          addtolist = false;
          Oncology_Clinical_Facility__c programDetermination = determinationMap.get(oncology.id);
          Oncology_Clinical_Facility__c planofcare = planOfCareMap.get(oncology.id);
          if (planofcare != null) {
              addtolist = true;
              oncology.CoH_Primary__c = planofcare.CoH_Primary__c;
              oncology.Facility_Managing_Physician__c = planofcare.CoH_Primary__c;
              oncology.Facility_Plan_of_Care_Submission_Date__c = planofcare.Plan_of_Care_Submission_Date__c;
              oncology.Consultation_Types__c = planofcare.Consultation_Types__c;
              oncology.Proposed_Service_Type__c = planofcare.Proposed_Service_Type__c;
              oncology.External_reference_id__c = planofcare.External_reference_id__c;
              oncology.Evaluation_Date_1__c = planofcare.Evaluation_Date_1__c;
              oncology.Evaluation_Date_2__c = planofcare.Evaluation_Date_2__c;
              oncology.Last_Appointment_Date_and_time__c = planofcare.Last_Appointment_Date_and_time__c;
              oncology.First_AppointmentTimeOn_First_Evaluation__c = planofcare.First_AppointmentTimeOn_First_Evaluation__c;
              if (oncology.First_AppointmentTimeOn_First_Evaluation__c != null) {
                  try{
                      string t = oncology.First_AppointmentTimeOn_First_Evaluation__c.right(oncology.First_AppointmentTimeOn_First_Evaluation__c.indexof(' ') - 2);
                      string[] ti = t.split(':');

                      if (integer.valueof(ti [0].trim()) > 12) {
                          oncology.PatientFAT_AMPM__c = 'PM';
                          try{
                              integer h = integer.valueof(ti [0].trim()) - 12;
                              oncology.Patient_FAT_Hours__c = string.valueof(h);
                          }catch (exception e) {
                              errorOccurred = true;
                              errorSavingPOC += e.getMessage() + ' ' + e.getLineNumber() + '\n\n';
                          }
                      }
                      else if (ti [0] == '12') {
                          oncology.PatientFAT_AMPM__c = 'PM';
                          oncology.Patient_FAT_Hours__c = ti [0];
                      }
                      else{
                          oncology.PatientFAT_AMPM__c = 'AM';
                          try{
                              integer h = integer.valueof(ti [0].trim());
                              oncology.Patient_FAT_Hours__c = string.valueof(h);
                          }catch (exception e) {
                              errorOccurred = true;
                              errorSavingPOC += e.getMessage() + ' ' + e.getLineNumber() + '\n\n';
                          }
                      }

                      oncology.PatientFAT_Minutes__c = ti [1];
                      oncology.First_Appointment_Date__c = date.valueof(oncology.First_AppointmentTimeOn_First_Evaluation__c.left(oncology.First_AppointmentTimeOn_First_Evaluation__c.indexof(' ')));
                  }catch (exception e) {
                      oncology.First_AppointmentTimeOn_First_Evaluation__c = planofcare.First_AppointmentTimeOn_First_Evaluation__c;
                      oncology.PatientFAT_AMPM__c = '';
                      oncology.Patient_FAT_Hours__c = '';
                      oncology.PatientFAT_Minutes__c = '';
                      system.debug('Error in oncologyWorkflow ' + e.getMessage() + ' ' + e.getLineNumber());
                      errorOccurred = true;
                      errorSavingPOC += e.getMessage() + ' ' + e.getLineNumber() + '\n\n';
                  }
              }

              if (oncology.Last_Appointment_Date_and_Time__c != null) {
                  try{
                      string t = oncology.Last_Appointment_Date_and_Time__c.right(oncology.Last_Appointment_Date_and_Time__c.indexof(' ') - 2);
                      string[] ti = t.split(':');

                      if (integer.valueof(ti [0].trim()) > 12) {
                          oncology.PatientLAT_AMPM__c = 'PM';
                          try{
                              integer h = integer.valueof(ti [0].trim()) - 12;
                              oncology.Patient_LAT_Hours__c = string.valueof(h);
                          }catch (exception e) {
                              errorOccurred = true;
                              errorSavingPOC += e.getMessage() + ' ' + e.getLineNumber() + '\n\n';
                          }
                      }
                      else if (ti [0] == '12') {
                          oncology.PatientLAT_AMPM__c = 'PM';
                          oncology.Patient_LAT_Hours__c = ti [0];
                      }
                      else{
                          oncology.PatientLAT_AMPM__c = 'AM';
                          try{
                              integer h = integer.valueof(ti [0].trim());
                              oncology.Patient_LAT_Hours__c = string.valueof(h);
                          }catch (exception e) {
                              errorOccurred = true;
                              errorSavingPOC += e.getMessage() + ' ' + e.getLineNumber() + '\n\n';
                          }
                      }

                      oncology.PatientLAT_Minutes__c = ti [1];
                      oncology.Last_Appointment_Date__c = date.valueof(oncology.Last_Appointment_Date_and_Time__c.left(oncology.Last_Appointment_Date_and_Time__c.indexof(' ')));
                  }catch (exception e) {
                      system.debug('Error in oncologyWorkflow ' + e.getMessage() + ' ' + e.getLineNumber());
                      oncology.Last_Appointment_Date_and_Time__c = planofcare.Last_Appointment_Date_and_time__c;
                      oncology.Last_Appointment_Date__c = null;
                      oncology.PatientLAT_AMPM__c = '';
                      oncology.Patient_LAT_Hours__c = '';
                      oncology.PatientLAT_Minutes__c = '';
                      errorOccurred = true;
                      errorSavingPOC += e.getMessage() + ' ' + e.getLineNumber() + '\n\n';
                  }
              }

              if (planofcare.Additional_diagnostics_outside_bundle__c != null) {
                  if (planofcare.Additional_diagnostics_outside_bundle__c.contains('Additional Pathology Review')) {
                      oncology.Additional_Pathology_Review__c = true;
                      oncology.Additional_Pathology_Notes__c = planofcare.Additional_Pathology_Notes__c;
                  }

                  if (planofcare.Additional_diagnostics_outside_bundle__c.contains('All other necessary evaluation and diagnostics not included in the In-Person Evaluation – Outpatient Services Bundle')) {
                      oncology.Addl_diagnostics_outside_bundle_None__c = true;
                      oncology.Additional_diagnostics_outside_bundle__c = planofcare.Additional_diagnostics_outside_bundle__c;
                  }

                  if (planofcare.Additional_diagnostics_outside_bundle__c.contains('Diagnostic Radiology Review')) {
                      oncology.Diagnostic_Radiology_Review__c = true;
                      oncology.Diagnostic_Radiology_Notes__c = planofcare.Diagnostic_Radiology_Notes__c;
                  }

                  if (planofcare.Additional_diagnostics_outside_bundle__c.contains('Outpatient Psychiatric')) {
                      oncology.Outpatient_Psychiatric__c = true;
                      oncology.Diagnostic_Radiology_Notes__c = planofcare.Diagnostic_Radiology_Notes__c;
                  }
              }

              oncology.Evaluation_Date_and_Time__c = planofcare.Evaluation_Date_and_Time__c;
              oncology.Estimated_Arrival_Date__c = planofcare.Estimated_Arrival_Date__c;
              oncology.Estimated_Departure_Date__c = planofcare.Estimated_Departure_Date__c;
              oncology.Recommended_Mode_of_Transportation__c = planofcare.Recommended_Mode_of_Transportation__c;
          }

          if (programDetermination != null) {
              addtolist = true;
              oncology.Determination_Date__c = programDetermination.Determination_Date__c;
              oncology.Determination_Outcome__c = programDetermination.Determination_Outcome__c;
              oncology.Determination_Outcome_Reason__c = programDetermination.Determination_Outcome_Reason__c;
              oncology.Treatment__c = programDetermination.Treatment__c;
              oncology.Clinical_Notes__c = programDetermination.Clinical_Notes__c;
              oncology.Date_of_Diagnosis__c = programDetermination.Date_of_Diagnosis__c;
              oncology.Primary_Diagnosis__c = programDetermination.Primary_Diagnosis__c;
              oncology.Program_Determination_Accepted_Date__c = datetime.now();
              oncology.Program_Determination_Accepted_By__c = userinfo.getuserid();

              if (addPhysician) {
                  Oncology_Physician__c physician = new Oncology_Physician__c(Oncology__c = oncology.id, Facility_PCP__c = true);

                  if (programDetermination.Physician_First_Name_Last_Name__c != null) {
                      string[] physicianName = programDetermination.Physician_First_Name_Last_Name__c.split(' ');
                      if (physicianName.size() == 2) {
                          physician.First_Name__c = physicianName [0];
                          physician.Last_Name__c = physicianName [1];
                      }
                      else if (physicianName.size() == 3) {
                          physician.First_Name__c = physicianName [1];
                          physician.Last_Name__c = physicianName [2];
                      }
                      else{
                          physician.First_Name__c = programDetermination.Physician_First_Name_Last_Name__c;
                      }
                  }

                  physician.Specialty__c = programDetermination.Physician_Specialty__c;
                  physician.Credentials__c = programDetermination.Physician_Credentials__c;
                  physician.Phone_Number__c = programDetermination.Physician_Phone_Number__c;
                  physician.Fax_Number__c = programDetermination.Physician_Fax_Number__c;
                  physician.Street_Address__c = programDetermination.Physician_Street_Address__c;
                  physician.City__c = programDetermination.Physician_City__c;
                  physician.State__c = oncolcogyStateFormat.stateAbbrv(programDetermination.Physician_State__c);
                  physician.Zip_Code__c = programDetermination.Physician_Zip_Code__c;
                  physician.Associated_Facilities__c = programDetermination.Associated_Facilities__c;

                  physicians.add(physician);
              }
          }

          if (addtolist) {
              oncologyList.add(oncology);
          }
      }
      try{
          if (oncologyList.isEmpty()) {
              errorOccurred = true;
              errorSavingPOC += 'planOfCareMap: ' + string.valueof(planOfCareMap.keyset()) + '\n\n';
              errorSavingPOC += 'determinationMap: ' + string.valueof(determinationMap.keyset()) + '\n\n';
          }
          update oncologyList;
      }catch (exception e) {
          errorOccurred = true;
          errorSavingPOC += e.getMessage() + ' ' + e.getLineNumber() + '\n\n';
      }

      if (errorOccurred) {
          Messaging.SingleEmailMessage[] mailRoom = new Messaging.SingleEmailMessage[] {};
          Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
          string[] sendemailto = new string[] {};
          sendemailto.add('oliver.brown@contigohealth.com');
          mail.setToAddresses(sendemailto);
          mail.setSubject('An error occured receiving a record from COH');
          mail.setPlainTextBody(errorSavingPOC);
          mailRoom.add(mail);
          Messaging.sendEmail(mailRoom);
      }
  }
}