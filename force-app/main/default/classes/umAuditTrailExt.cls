public with sharing class umAuditTrailExt{
    
    public Utilization_Management_History__c[] umAuditTrailRecords {
        get; private set;
    } 

    public Utilization_Management_History__c[] umInitialAuditTrailRecords {
        get; private set;
   }     
    string umAuditTrailID;
    public string at;
   
    public umAuditTrailExt(ApexPages.StandardController controller){

    umAuditTrailID=  Apexpages.currentpage().getparameters().get('umID');   
    system.debug('umAuditTrailID= '+umAuditTrailID);               
      
    at=Apexpages.currentpage().getparameters().get('at') ; 
    system.debug('at= '+at);  
    
    if(at=='initial'){
      loadumInitialAuditTrail();

      
    }else if(at!='initial'){
      loadumAuditTrail();

    }  
    }

    public umAuditTrailExt(){
 
    }
   
    public void loadumAuditTrail(){
    umAuditTrailID=  Apexpages.currentpage().getparameters().get('umID') ; 
        umAuditTrailRecords = new Utilization_Management_History__c[] {};

        for (Utilization_Management_History__c atRecords : [
                 SELECT id, name, Changed_By__c, Changed_On__c,
                 New_Value__c,Old_Value__c,LastModifiedDate,LastModifiedBy.Name,
                 createdDate,createdBy.name
                 FROM Utilization_Management_History__c
                 where Utilization_Management__c =:umAuditTrailID  order by createdDate desc
             ]) {

             if(atRecords.New_Value__c!=null){
                  umAuditTrailRecords.add(atRecords);
                  system.debug('umAuditTrailRecords='+umAuditTrailRecords);
                 }
            
        }


    }

    public void loadumInitialAuditTrail(){
    umAuditTrailID=  Apexpages.currentpage().getparameters().get('umID') ; 
    umInitialAuditTrailRecords = new Utilization_Management_History__c[] {};
    
         for (Utilization_Management_History__c atRecords : [
                 SELECT Admission_Date__c,Admission_Source__c,
                 Approved_Through_Date__c,createdDate,createdBy.name,
                 Discharge_Date__c,Event_Type__c,Facility_Network_Status__c,
                 id,LastModifiedDate,LastModifiedBy.Name,
                 Med_Cat__c,name,Nurses__c,
                 Comment_Line_for_Claims__c,Quality_Codes__c
                 FROM Utilization_Management_History__c
                 where Initial_History__c=true and Utilization_Management__c =:umAuditTrailID order by createdDate desc
             ]) {
             if(atRecords.LastModifiedBy.Name!=''){
                  umInitialAuditTrailRecords.add(atRecords);
                  system.debug('umInitialAuditTrailRecords ='+umInitialAuditTrailRecords );
                 }
            
        }   
  
    }
      
}