public with sharing class umTransplantLetter{
    
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c  uml, Utilization_Management_Clinician__c clinician){
        
        string letterText='';
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=um.patient__r.Address__c+'<br/>';
        letterText+=um.patient__r.City__c+', ';
        letterText+=um.patient__r.State__c+' ';
        letterText+=um.patient__r.Zip__c+'<br/><br/>';
        letterText+='Contigo Health performs medical necessity reviews on behalf of&nbsp;'+um.Patient__r.Patient_Employer__r.name+'. The request referenced below has been approved.<br/><br/>';
                               
        letterText+='<div style="margin-top:10px">';
        letterText+='<div style="display:inline-block;width:25%">';
        letterText+='Case Number:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px;">';
        letterText+='&nbsp;'+um.HealthPac_Case_Number__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%">';
        letterText+='Patient Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%">';
        letterText+='Physician Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+clinician.First_Name__c+' '+clinician.Last_Name__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%">';
        letterText+='Facility Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+um.Facility_Name__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%;vertical-align:top">';
        letterText+='Type of Request:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+uml.Type_of_Request__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:25%;vertical-align:top">';
        letterText+='Description:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+uml.Description__c;
        letterText+='</div>';
        letterText+='</div>';
        
        letterText+='<p>'; 
        letterText+='Please contact us at 877-891-2692 with the date(s) of service';
        letterText+='</p>';
        
        letterText+='<p>';  
        letterText+='All choices regarding the care and treatment of the patient remain the responsibility of the ordering physician. In no way does this letter attempt to dictate the care the patient ultimately receives.<br/><br/>';
        letterText+='All authorizations and services are contingent upon the patients continued participation in their plan at the time the service(s) are rendered. Payment is subject to the Plan benefit/provisions and eligibility at the time of service. The patient is responsible for all co-pays and deductibles applicable to the claim.<br/><br/>';
        letterText+='Thank you for helping us to serve your needs. If there are any questions you can reach us at the phone number listed above, or mail any correspondence to 1755 Georgetown Rd., Hudson, OH  44236 c/o Care management.<br/><br/>';
        letterText+='</p>';
      
        letterText+='<div>';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='CC:'; 
        letterText+='</div>';
        letterText+='<div style="display:inline-block;">';
        letterText+=clinician.First_Name__c+' '+clinician.Last_Name__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        boolean hasClinician = (Clinician.Street__c!=null&&Clinician.City__c!=null&&Clinician.State__c!=null&&Clinician.Zip_Code__c!=null);
        
        letterText+='<div style="display:inline-block;"/>';
        letterText+=hasClinician ? Clinician.Street__c : '';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasClinician ? Clinician.City__c + ', ' : ', ';
        letterText+=hasClinician ? Clinician.State__c  +' ' : ' ';
        letterText+=hasClinician ? Clinician.Zip_Code__c : '';
        letterText+='</div>';
        letterText+='</div><br/>';         
        
        letterText+='<div style="margin-top:10px">';
        
        letterText+='<div style="display:inline-block;">';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        boolean hasFacility = (um.Facility_Name__c!=null && um.Facility_Street__c != null && um.Facility_City__c  != null && um.Facility_State__c  != null && um.Facility_Zip_Code__c  != null);
        
        letterText+=hasFacility ? um.Facility_Name__c : '';
                      
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasFacility ? um.Facility_Street__c : '';
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasFacility ? um.Facility_City__c + ', ' : '';
        letterText+=hasFacility ? um.Facility_State__c +' ' : '';
        letterText+=hasFacility ? um.Facility_Zip_Code__c : '';
        letterText+='</div>';
        letterText+='</div>';
        letterText+='</div>';  
                                                                                                                                                                                                                                                                                      
        letterText+='</div>';
        return letterText;
    }
    
}