global class claimsAuditSchedule implements Schedulable{
    
    global void execute(SchedulableContext go){
        
        claimsAuditBatch cb = new claimsAuditBatch();
        cb.run();
        
    }
    
     
}