public with sharing class searchController {


        //list<Patient_Case__c> pcList = new list<Patient_Case__c>();
        public list<List<sObject>> searchResults  = new List<List<sObject>>{};
        public string searchTerm {get;set;}
        public string stDisplay {get;set;}
        public string searchingDisplay {get;set;}
        public list<Patient_Case__c> foundPC = new list<Patient_Case__c>();
        public list<Account> foundAccts = new list<Account>();
        public list<Contact> foundMembers = new list<Contact>();
        
        public string fname {get;set;}
        public string lname {get;set;}
        public string ssn {get;set;}
        public string phone {get;set;}
        public string city {get;set;}
        public boolean isError {get;set;}
        public string message {get;set;}
        public boolean advSearch {get;set;}
        boolean expandSearch;
        
        public list<string> stList = new list<string>();
        public string searchObject {get;set;}
        public boolean renderPC {get;set;}
        public boolean renderAccts {get;set;}
        public boolean renderLeads {get;set;}
        public boolean renderOpps {get;set;} 
        public boolean renderMembers {get;set;}
        public boolean notRunFind; //flag as a dup search from a patient case
        public string pfname; 
        public string plname; 
        public string pbday;
        sObject dupRecord; 
        public string theID {get;set;}
        public string recID {get;set;}
        public string oppID {get;set;}
        public string jsonIn {get;set;}
        public string jsonPat {get;set;}
        public string newID {get;set;}
        integer maxQueryRows = 49995; //Other utility queries need to run
        SystemID__c sid = SystemID__c.getInstance();
        
        void init(){

                fname='';
                lname='';
                ssn='';
                phone='';
                renderPC = false;
                renderAccts=false;
                renderMembers=false;
                searchingDisplay ='';
                theID='';
                foundMembers=new list<Contact>();
                foundAccts=null;
                stList = new list<string>();
                searchTerm ='';
                searchResults = new List<List<sObject>>{};
                
                searchTerm = ApexPages.currentPage().getParameters().get('st');
                searchObject = ApexPages.currentPage().getParameters().get('sel');
                
                if(searchTerm==null||searchTerm.length()<2){return;}
                searchTerm = searchTerm.toLowerCase();
                
                if(stDisplay==null){    
                    stDisplay = searchTerm; 
                }else{
                    searchTerm=stDisplay;
                }
                
                if(searchTerm!=null){
                    getsearchterms();
                    soslSearch();
                    
                        
                    if(searchObject=='pc'){
                        searchingDisplay = 'Patient Cases';
                        renderPC= true;
                        loadPCSearchData(false);   
                        theID = setTHEID(foundPC);
                        
                    }else if(searchObject=='coverage'){
                        searchingDisplay = 'Coverage';
                        renderAccts=true;
                        loadAcctSearchData();
                        theID = setTHEID(foundAccts);
                        
                    }else if(searchObject=='member'){
                        searchingDisplay = 'Members';
                        renderMembers=true;
                        loadMemberSearchData();                        
                        theID = setTHEID(foundMembers);
                        
                    }else if(searchObject=='SearchAll'){
                        searchingDisplay = 'All';
                        
                        renderPC=true;
                        renderAccts=true;
                        renderMembers=true;
                        
                        loadPCSearchData(false);
                        loadAcctSearchData();
                        loadMemberSearchData();
                        
                                     
                   }
                }
        
        } 
          
        public searchController() {
                
        init();
        }
    
    void searchObjectPC(string so){
        
        if(so=='pc'){
            searchingDisplay = 'Patient Cases';
            renderPC= true;
            loadPCSearchData(false);   
            theID = setTHEID(foundPC);
        }
    }
    
    string setTHEID(sObject[] sObj){
    
        if(sObj.size()==1){
            return sObj[0].id;
        }else{
            return null;
        }
    
    }
    
    public searchController(boolean notRunFind,string fName, string lName) {
        this.fName=fName.toLowerCase(); 
        this.lName=lName.toLowerCase();
        this.ssn='';
        this.phone=''; 
        this.notRunFind=notRunFind;
        this.expandSearch=false;
    }

    public sObject findDup(){
        stList = new string[]{};
        stList.add(fName.toLowerCase());
        stList.add(lName.toLowerCase());
        expandSearch=true;
        loadPCSearchData(true);
        system.debug(foundPC);
        if(foundPC!=null&&!foundPC.isEmpty()){
            return foundPC[0];
        }
        return null;
    }
    
    public searchController(string st){
        //for easier testing
        searchTerm = st;
    }
        
    public searchController(apexPages.standardController controller){
            
        searchTerm = ApexPages.currentPage().getParameters().get('st');
        searchObject = ApexPages.currentPage().getParameters().get('sel'); 
            
    }
    
    public pagereference pcExpandSearch(){
        expandSearch=true;
        loadPCSearchData(false);
        return null;
    }
    
    public void advSearch(){
        isError = false;
        message = '';
        
        if(fname=='' && lname=='' && ssn =='' && phone==''){
            
            //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO,'Please include at least one search term'));
            isError = true;
            message = 'Please include at least one search term';
            
        }else{
            
            notRunFind=true;
            
            loadPCSearchData(true);
                        
        }
        
    }

    public void advSearchClear(){
        isError = false;
        message = '';
        fname='';
        lname='';
        ssn ='';
        phone='';
        foundPC.clear();
        advSearch=false;
        
    }
    
    public List<SelectOption> getsearchItems() {
        
        List<SelectOption> options = new List<SelectOption>();
        
        if(utilities.isInternal()){
            options.add(new SelectOption('SearchAll','Search All'));
            options.add(new SelectOption('pc','Patient Cases'));
            Profile p = [select name from Profile where id = :userInfo.getProfileId()];
            if(!p.name.tolowercase().contains('customer service')){
                options.add(new SelectOption('trans','Transplants'));
            }
            
            options.add(new SelectOption('coverage','Coverage'));
            options.add(new SelectOption('member','Members'));  
        }else{
            options.add(new SelectOption('pc','Patient Cases'));  
        }    
        return options;
    
    }
    
    //public list<Lead> getfoundLeads(){
        //return foundLeads;
    //}

    public list<Patient_Case__c> getfoundPC(){
        return foundPC;
    }

    public list<Account> getfoundAccts(){
        return foundAccts;
    }
    
    //public list<Opportunity> getfoundOpps(){
        //return foundOpps;
    //}

    public list<Contact> getfoundMembers(){
        return foundMembers;
    }
       
    
    private void loadAcctSearchData(){
        
        list<Account> acctList1 = ((List<Account>)searchResults[0]);
        
        set<ID> theIDS = new set<ID>();
        foundAccts = new Account[]{};
        system.debug(limits.getQueryRows());
        for(Account[] acctList : [select createdby.name, Client_Name__c ,createddate,Name, Employee_First_Name__c,Employee_Last_Name__c from Account order by lastmodifieddate desc limit :maxQueryRows-limits.getQueryRows()]){       
            for(Account a: acctList){
            
                    string result;
                    
                    result = a.name + ';' + a.Employee_First_Name__c + ';' + a.Employee_Last_Name__c;
                    
                    for(string s: stList){
                    if(s!=';' && s!=''){    
                        if(result.indexOfIgnoreCase(s) != -1){
                            if(!theIDS.contains(a.id) && a.id!=null){
                                foundAccts.add(a);      
                            }
                        theIDS.add(a.id);
                        }
                    }   
                    }
                
            }
        }
        
        for(Account a: acctList1){
            if(!theIDS.contains(a.id) && a.id!=null){
                foundAccts.add(a);
            }

        }
 
    }


    private void loadMemberSearchData(){
        foundMembers.clear();
        list<Contact> oppList1 = ((List<Contact>)searchResults[2]);
        set<ID> theIDS = new set<ID>();
        system.debug(limits.getQueryRows());
        for(Contact[] oppList : [select firstname,lastname,createdby.name, createdby.id,createddate,Relationship_to_the_Insured__c,First_Name__c,last_Name__c,SSN__c from Contact order by lastmodifieddate desc limit :maxQueryRows-limits.getQueryRows()]){        
            for(Contact c: oppList){
            
                    string result;
                    
                    result = c.First_Name__c + ';' + c.last_Name__c + ';' + c.ssn__c;
                    
                    for(string s: stList){
                        if(s!=';' && s!=''){
                            if(result.indexOfIgnoreCase(s) != -1 && !theIDS.contains(c.id) && c.id!=null){
                                    foundMembers.add(c);       
                            }
                            
                            theIDS.add(c.id);
                            
                        }
                    }
                
            }
        }
        
        for(Contact c: oppList1){
            
            if(!theIDS.contains(c.id) && c.id!=null){
                foundMembers.add(c);
            }

        }
        
    }

    
    private void loadPCSearchData(boolean adv){
        foundPC.clear();
        string result='';
        integer x=stList.size();
        string sep='';
        
        if(expandSearch==null||expandSearch==false){
            sep=';';
        }
        
        for(integer i=0;i<5;i++){
            if(x<i){
                stList.add(sep);
            }
        }
        system.debug(stList);
        system.debug(limits.getQueryRows());
        for(Patient_Case__c[] pCaseList : [select Employee_DOBe__c, Employee_City__c, Employee_State__c, caregiver_name__c,employee_ssn__c, Estimated_Arrival__c,certID__c,Caregiver_Home_Phone__c,Caregiver_Mobile__c,name,isConverted__c, Employee_Home_Phone__c, Employee_Mobile__c, Employee_Work_Phone__c, BID__c,employee_first_name__c, employee_last_name__c,recordtype.name,patient_city__c,Patient_Mobile__c, Patient_Home_Phone__c, Patient_Work_Phone__c, patient_state__c,status__c,Caller_Name__c,Patient_DOBe__c,Client_Name__c,patient_ssn__c, patient_first_name__c, patient_last_name__c,createdby.name, createddate from Patient_Case__c order by lastmodifieddate desc limit :maxQueryRows-limits.getQueryRows()]){        
            
             if(adv==true){
     
                for(Patient_Case__c p: pCaseList){
                    result='';
                    if(fName!=''){
                        result = p.patient_first_name__c + ';' + p.employee_first_name__c + ';' + p.Caller_Name__c + ';' + p.caregiver_name__c;
                    }
                    
                    if(lName!=''){
                        result += p.patient_last_name__c + ';' + p.employee_last_name__c + ';' + p.Caller_Name__c + ';' + p.caregiver_name__c;
                    }
                    
                    if(ssn!=null && ssn!=''){
                        result += p.patient_ssn__c + ';' + p.employee_ssn__c + ';';
                    }
                    
                    if(phone!=null && phone!=''){
                        result += p.Patient_Home_Phone__c + ';' + p.Patient_Work_Phone__c + ';' + p.Employee_Home_Phone__c + p.Employee_Mobile__c + ';' + p.Employee_Work_Phone__c + ';' + p.Patient_Mobile__c + ';' + p.caregiver_name__c + ';' + p.Caregiver_Home_Phone__c + ';' + p.Caregiver_Mobile__c+ ';' + p.caregiver_name__c;
                    }
                     
                    if(result.indexOfIgnoreCase(fName) != -1 
                    && result.indexOfIgnoreCase(lName) != -1
                    && result.indexOfIgnoreCase(ssn)   != -1
                    && result.indexOfIgnoreCase(phone) != -1
                    ){
                        foundPC.add(p);
                        
                    }
                    
            }
                
            }else{
                
                for(Patient_Case__c p: pCaseList){
                    result = p.employee_ssn__c + ';' + p.patient_ssn__c + ';' + p.patient_first_name__c + ';' + p.patient_last_name__c + ';' + p.Caller_Name__c + ';' + string.valueof(p.patient_dobe__c) + ';' + p.employee_first_name__c + ';' + p.employee_last_name__c + ';' + p.BID__c + ';' + p.certID__c + ';' + p.Patient_Mobile__c + ';' + p.Patient_Home_Phone__c + ';' + p.Patient_Work_Phone__c + ';' + p.Employee_Home_Phone__c + p.Employee_Mobile__c + ';' + p.Employee_Work_Phone__c + ';' + p.Caregiver_Home_Phone__c + ';' + p.Caregiver_Mobile__c+ ';' + p.caregiver_name__c+';'+p.name;
                    
                    if(expandSearch==null||expandSearch==false){
                        if(result.indexOfIgnoreCase(stList[0]) != -1 
                        && result.indexOfIgnoreCase(stList[1]) != -1
                        && result.indexOfIgnoreCase(stList[2]) != -1
                        && result.indexOfIgnoreCase(stList[3]) != -1
                        ){
                            foundPC.add(p);

                        }
                    }else{
                        for(string s: stList){
                            system.debug(s);    
                            system.debug(result.indexOfIgnoreCase(s));    
                            if(s!=';' && s!='' && result.indexOfIgnoreCase(s) != -1){
                                   foundPC.add(p); 
                               
                            }
                        
                        }
                        
                    }
                           
                }
                
                
                
            }
        }
        
        
        
    }
    
    
    private void soslSearch(){
        
        string searchString;
        
        for(String s: stList){
            if(s!=';' && s!=''){    
                s = string.escapeSingleQuotes(s);
        
                searchString = 'FIND \'' + s + '\' IN ALL FIELDS RETURNING Account (id,name, Client_Name__c,createdby.name, createddate),'+ 
                                                                  'Patient_Case__c (Caregiver_Name__c ,Estimated_Arrival__c,name,BID__c,isConverted__c,patient_case__c,employee_first_name__c, employee_last_name__c, patient_city__c, patient_state__c, status__c,id,patient_ssn__c, patient_first_name__c, patient_last_name__c,createdby.name,recordtype.name, createddate,Client_Name__c),'+ 
                                                                  'Contact (First_Name__c,last_Name__c,firstname,lastname,id,ssn__c,Relationship_to_the_Insured__c,createdby.name, createddate),'+ 
                                                                  'Transplant__c(Name,stage__c,Patient_SSN__c,Patient_State__c,Patient_City__c,Patient_First_Name__c,Patient_Last_Name__c,employee_first_name__c, employee_last_name__c,Caregiver_Name__c,intake_status__c,Caregiver_Home_Phone__c ,createdby.name, createddate)';
        
                searchResults =+  search.query(searchString);
            }
        }
                
    }
    
    private void getsearchterms(){
        
        if(searchTerm.containsWhitespace()){
        
            for(string s: searchTerm.split(' ')){
                stList.add(s);
            }

        }else{
        
            stList.add(searchTerm);
        
        }
    }
    
    public void DeletePC(){
      // if for any reason we are missing the reference 
      
      if (recID == null) {
      
         return;
      }
     
      // find the record within the collection
      Patient_Case__c tobeDeleted = null;
      for(Patient_Case__c t : foundPC)
       if (t.Id == recID) {
          tobeDeleted = t;
          break;
       }
      
      //if pc record found delete it
      if (tobeDeleted != null) {
       Delete tobeDeleted;
      }
      
      //refresh the data
      soslSearch();
      loadPCSearchData(false);    
    }
    
    public boolean getisDeletePC(){
        Schema.DescribeSObjectResult dLead = Schema.SObjectType.Patient_Case__c;
        return dLead.isDeletable();
    }
 
    
}
