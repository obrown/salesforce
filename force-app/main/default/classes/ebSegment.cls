public with sharing class ebSegment{
    
   public map<string, map<string, string>> ebMap = new map<string, map<string, string>>();
   
   public ebSegment(){
      
      ebMap.put('1',ebSeg01());
      ebMap.put('2',ebSeg02());
      ebMap.put('3',ebSeg03());
      ebMap.put('5',ebSeg05());
      ebMap.put('6',ebSeg06());
      ebMap.put('12',ebSeg12());
   }
   
   map<string, string> ebSeg01(){
       map<string, string> segmap = new map<string,string>();
      segmap.put('1','Active Coverage');
      segmap.put('2','Active Full Risk Capitation');
      segmap.put('3','Active Services Capitated');
      segmap.put('4','Active Services Capitated to Primary capitated Physician');
      segmap.put('4','Active Services Capitated to Primary capitated Physician');
      segmap.put('5','Active Pending Investigation');
      segmap.put('6','Inactive');
      segmap.put('7','Inactive Pending Eligibility Update');
      segmap.put('8','Inactive Pending Investigation');
      segmap.put('A','Co-Insurance');
      segmap.put('B','Co-Payment');
      segmap.put('C','Deductible');
      segmap.put('CB','Coverage Basis');
      segmap.put('D','Benefit Description');
      segmap.put('E','Exclusions');
      segmap.put('F','Limitations');
      segmap.put('G','Out of Pocket (Stop Loss)');
      segmap.put('H','Unlimited');
      segmap.put('I','Non-Covered');
      segmap.put('J','Cost Containment');
      segmap.put('H','Unlimited');
      segmap.put('K','Reserve');
      segmap.put('L','Primary Care Provider');
      segmap.put('M','Pre-existing Condition');
      segmap.put('MC','Managed Care Coordinator');
      segmap.put('N','Services Restricted to Following Provider');
      segmap.put('O','Not Deemed a Medical Necessity');
      segmap.put('P','Benefit Disclaimer');
      segmap.put('Q','Second Surgical Opinion Required');
      segmap.put('R','Other or Additional payor');
      segmap.put('S','Prior Years History');
      segmap.put('T','Cards Reported Lost or stolen');
      segmap.put('U','Contact Following entity for Eligibility or Benefit Information');
      segmap.put('V','Cannot Process');
      segmap.put('W','Other Source of Data');
      segmap.put('X','health Care Facility');
       
       return segmap;
   }    
   map<string, string> ebSeg02(){
       map<string, string> segmap = new map<string, string>();
       segmap.put('CHD','Children Only');
       segmap.put('DEP','Dependents only');
       segmap.put('EMP','Employee Only');
       segmap.put('ECH','Employee and Children');
       segmap.put('ESP','Employee and Spouse');
       segmap.put('FAM','Family');
       segmap.put('IND','Individual');
       segmap.put('SPC','Spouse and Children');
       segmap.put('SPO','Spouse Only');
       return segmap;
   }
   
   
   map<string, string> ebSeg03(){
       map<string, string> segmap= new map<string, string>();
       segmap.put('1','Medical Care');
       segmap.put('2','Surgical');
       segmap.put('3','Consultation');
segmap.put('4','Diagnostic X-Ray');
segmap.put('5','Diagnostic Lab');
segmap.put('6','Radiation Therapy');
segmap.put('7','Anesthesia');
segmap.put('8','Surgical Assistance');
segmap.put('10','Blood');
segmap.put('11','Durable Medical Equipment Used');
segmap.put('12','Durable Medical Equipment Purchased');
segmap.put('14','Renal Supplies');
segmap.put('17','Pre-Admission Testing');
segmap.put('18','Durable Medical Equipment Rental');
segmap.put('19','Pneumonia Vaccine');
segmap.put('20','Second Surgical Opinion');
segmap.put('21','Third Surgical Opinion');
segmap.put('22','Social Work');
segmap.put('23','Diagnostic Dental');
segmap.put('24','Periodontics');
segmap.put('25','Restorative');
segmap.put('26','Endodontics');
segmap.put('27','Maxillofacial Prosthetics');
segmap.put('28','Adjunctive Dental Services');
segmap.put('30','Health Benefit Plan Coverage');
segmap.put('32','Plan Waiting Period');
segmap.put('33','Chiropractic');
segmap.put('35','Dental Care');
segmap.put('36','Dental Crowns');
segmap.put('37','Dental Accident');
segmap.put('38','Orthodontics');
segmap.put('39','Prosthodontics');
segmap.put('40','Oral Surgery');
segmap.put('41','Preventive Dental');
segmap.put('42','Home Health Care');
segmap.put('43','Home Health Prescriptions');
segmap.put('45','Hospice');
segmap.put('46','Respite Care');
segmap.put('47','Hospitalization');
segmap.put('49','Hospital - Room and Board');
segmap.put('54','Long Term Care');
segmap.put('55','Major Medical');
segmap.put('56','Medically Related Transportation');
segmap.put('60','General Benefits');
segmap.put('61','In-vitro Fertilization');
segmap.put('62','MRI Scan');
segmap.put('63','Donor Procedures');
segmap.put('64','Acupuncture');
segmap.put('65','Newborn Care');
segmap.put('66','Pathology');
segmap.put('67','Smoking Cessation');
segmap.put('68','Well Baby Care');
segmap.put('69','Maternity');
segmap.put('70','Transplants');
segmap.put('71','Audiology');
segmap.put('72','Inhalation Therapy');
segmap.put('73','Diagnostic Medical');
segmap.put('74','Private Duty Nursing');
segmap.put('75','Prosthetics');
segmap.put('76','Dialysis');
segmap.put('77','Otology');
segmap.put('78','Chemotherapy');
segmap.put('79','Allergy Testing');
segmap.put('80','Immunizations');
segmap.put('81','Routine Physical');
segmap.put('82','Family Planning');
segmap.put('83','Infertility');
segmap.put('84','Abortion');
segmap.put('85','HIV - AIDS Treatment');
segmap.put('86','Emergency Services');
segmap.put('87','Cancer Treatment');
segmap.put('88','Pharmacy');
segmap.put('89','Free Standing Prescription Drug');
segmap.put('90','Mail Order Prescription Drug');
segmap.put('91','Brand Name Prescription Drug');
segmap.put('92','Generic Prescription Drug');
segmap.put('93','Podiatry');
segmap.put('94','Podiatry Office');
segmap.put('95','Podiatry Nursing Home');
segmap.put('96','Professional Physician');
segmap.put('97','Anesthesiologist');
segmap.put('98','Professional Physician Office');
segmap.put('99','Professional Physician Inpatient');
segmap.put('A0','Professional Physician Outpatient');
segmap.put('A1','Professional Physician Nursing Home');
segmap.put('A2','Professional Physician SNF');
segmap.put('A3','Professional Physician Home');
segmap.put('A4','Psychiatric');
segmap.put('A6','Psychotherapy');
segmap.put('A7','Psychiatric - Inpatient');
segmap.put('A8','Psychiatric - Outpatient');
segmap.put('A9','Rehabilitation');
segmap.put('AA','Rehabilitation Room and Board');
segmap.put('AB','Rehabilitation - Inpatient');
segmap.put('AC','Rehabilitation - Outpatient');
segmap.put('AD','Occupational Therapy');
segmap.put('AE','Physical Medicine');
segmap.put('AF','Speech Therapy');
segmap.put('AG','Skilled Nursing Care');
segmap.put('AH','Skilled Nursing Care Room and Board');
segmap.put('AI','Substance Abuse');
segmap.put('AJ','Alcoholism Treatment');
segmap.put('AK','Drug Addiction');
segmap.put('AL','Optometry');
segmap.put('AM','Frames');
segmap.put('AO','Lenses');
segmap.put('AP','Routine Eye Exam');
segmap.put('AQ','Nonmedically Necessary Physical (These physicals are required by other entities e.g., insurance application, pilot license, employment or school)');
segmap.put('AR','Experimental Drug Therapy');
segmap.put('B1','Burn Care');
segmap.put('B2','Brand Name Prescription Drug - Formulary');
segmap.put('B3','Brand Name Prescription Drug - Non-Formulary');
segmap.put('BA','Independent Medical Evaluation');
segmap.put('BB','Psychiatric Treatment Partial Hospitalization');
segmap.put('BC','Day Care (Psychiatric)');
segmap.put('BD','Cognitive Therapy');
segmap.put('BE','Massage Therapy');
segmap.put('BF','Pulmonary Rehabilitation');
segmap.put('BG','Cardiac Rehabilitation');
segmap.put('BH','Pediatric');
segmap.put('BI','Nursery Room and Board');
segmap.put('BJ','Skin');
segmap.put('BK','Orthopedic');
segmap.put('BL','Cardiac');
segmap.put('BM','Lymphatic');
segmap.put('BN','Gastrointestinal');
segmap.put('BP','Endocrine');
segmap.put('BQ','Neurology');
segmap.put('BR','Eye');
segmap.put('BS','Invasive Procedure');
segmap.put('BT','Gynecological');
segmap.put('BU','Obstetrical');
segmap.put('BV','Obstetrical/Gynecological');
segmap.put('BW','Mail Order Prescription Drug: Brand Name');
segmap.put('BX','Mail Order Prescription Drug: Generic');
segmap.put('BY','Physician Visit - Sick');
segmap.put('BZ','Physician Visit - Well');
segmap.put('C1','Coronary Care');
segmap.put('CK','Screening X-ray');
segmap.put('CL','Screening laboratory');
segmap.put('CM','Mammogram, High Risk Patient');
segmap.put('CN','Mammogram, Low Risk Patient');
segmap.put('CO','Flu Vaccination');
segmap.put('CP','Eyewear Accessories');
segmap.put('CQ','Case Management');
segmap.put('DG','Dermatology');
segmap.put('DM','Durable Medical Equipment');
segmap.put('DS','Diabetic Supplies');
segmap.put('E0','Allied Behavioral Analysis Therapy');
segmap.put('E1','Non-Medical Equipment (non DME)');
segmap.put('E2','Psychiatric Emergency');
segmap.put('E3','Step Down Unit');
segmap.put('E4','Skilled Nursing Facility Head Level of Care');
segmap.put('E5','Skilled Nursing Facility Ventilator Level of Care');
segmap.put('E6','Level of Care 1');
segmap.put('E7','Level of Care 2');
segmap.put('E8','Level of Care 3');
segmap.put('E9','Level of Care 4');
segmap.put('E10','Radiographs');
segmap.put('E11','Diagnostic Imaging');
segmap.put('E12','Basic Restorative - Dental');
segmap.put('E13','Major Restorative - Dental');
segmap.put('E14','Fixed Prosthodontics');
segmap.put('E15','Removable Prosthodontics');
segmap.put('E16','Intraoral Images - Complete Series');
segmap.put('E17','Oral Evaluation');
segmap.put('E18','Dental Prophylaxis');
segmap.put('E19','Panoramic Images');
segmap.put('E20','Sealants');
segmap.put('E21','Flouride Treatments');
segmap.put('E22','Dental Implants');
segmap.put('E23','Temporomandibular Joint Dysfunction');
segmap.put('E24','Retail Pharmacy Prescription Drug');
segmap.put('E25','Long Term Care Pharmacy');
segmap.put('E26','Comprehensive Medication Therapy Management Review');
segmap.put('E27','Targeted Medication Therapy Management Review');
segmap.put('E28','Dietary/Nutritional Services');
segmap.put('E32','Intensive Cardiac Rehabilitation - Technical Component');
segmap.put('E33','Intensive Cardiac Rehabilitation');
segmap.put('E34','Pulmonary Rehabilitation - Technical Component');
segmap.put('E35','Pulmonary Rehabilitation - Professional Component');
segmap.put('E36','Convenience Care');
segmap.put('E37','Telemedicine');
segmap.put('E38','Pharmacist Services');
segmap.put('E39','Diabetic Education');
segmap.put('EA','Preventive Services');
segmap.put('EB','Specialty Pharmacy');
segmap.put('EC','Durable Medical Equipment New');
segmap.put('ED','CAT Scan');
segmap.put('EE','Ophthalmology');
segmap.put('EF','Contact Lenses');
segmap.put('F1','Medical Coverage');
segmap.put('F2','Social Work Coverage');
segmap.put('F3','Dental Coverage');
segmap.put('F4','Hearing Coverage');
segmap.put('F5','Prescription Drug Coverage');
segmap.put('F6','Vision Coverage');
segmap.put('F7','Orthodontia Coverage');
segmap.put('F8','Mental Health Coverage');
segmap.put('GF','Generic Prescription Drug - Formulary');
segmap.put('GN','Generic Prescription Drug - Non-Formulary');
segmap.put('GY','Allergy');
segmap.put('IC','Intensive Care');
segmap.put('MH','Mental Health');
segmap.put('NI','Neonatal Intensive Care');
segmap.put('ON','Oncology');
segmap.put('PE','Positron Emission Tomography (PET) Scan');
segmap.put('PT','Physical Therapy');
segmap.put('PU','Pulmonary');
segmap.put('RN','Renal');
segmap.put('RT','Residential Psychiatric Treatment');
segmap.put('SMH','Serious Mental Health');
segmap.put('TC','Transitional Care');
segmap.put('TN','Transitional Nursery Care');
segmap.put('UC','Urgent Care');

       return segmap;
   }
   
   map<string, string> ebSeg05(){
       map<string, string> segmap= new map<string, string>();
       segmap.put('12','Medicare Secondary Working Aged Beneficiary or Spouse with Employer Group Health Plan');
       segmap.put('13','Medicare Secondary EndStage Renal Disease Beneficiary in the 12 month coordination period with an employer\'s group health plan');
       segmap.put('14','Medicare Secondary, Nofault insurance including insurance in which auto is primary');
       segmap.put('15','Medicare Secondary Worker\'s Compensation');
       segmap.put('16','Medicare Secondary Public Health Service or Other Federal Agency');
       segmap.put('41','Medicare Secondary Black Lung');
       segmap.put('42','Medicare Secondary Veteran\'s Administration');

segmap.put('43','Medicare Secondary Disabled Beneficiary Under Age 65 with Large Group Health Plan (LGHP)');
segmap.put('47','Medicare Secondary, Other Liability Insurance is Primary');
segmap.put('AP','Auto Insurance Policy');
segmap.put('C1','Commercial');
segmap.put('CO','COBRA');
segmap.put('CP','Medicare Conditionally Primary');
segmap.put('D','Disability');
segmap.put('DB','Disability Benefits');
segmap.put('EP','Exclusive Provider Organization (for selfinsured risks)');
segmap.put('FF','Family and Friends');
segmap.put('GP','Group Policy');
segmap.put('HM','Health Maintenance Organization (HMO)');
segmap.put('HN','Health Maintenance Organization (HMO) Medicare Advantage');
segmap.put('HS','Special Low Income Medicare Beneficiary');
segmap.put('IN','Indemnity');
segmap.put('IP','individual Policy');
segmap.put('LC','Long Term Care');
segmap.put('LD','Long Term Policy');
segmap.put('LI','Life Insurance');
segmap.put('LT','Litigation');

segmap.put('MA','Medicare Part A');
segmap.put('MB','Medicare Part B');
segmap.put('MC','Medicaid');
segmap.put('MD','Medicare Part D');
segmap.put('MH','Medigap Part A');
segmap.put('MI','Medigap Part B');
segmap.put('MP','Medicare Primary');
segmap.put('PC','Personal Care');
segmap.put('PE','Property Insurance Personal');
segmap.put('PR','Preferred Provider Organization (PPO)');
segmap.put('PS','Point of Service (POS)');
segmap.put('QM','Qualified Medicare Beneficiary');
segmap.put('RP','Property Insurance Real');
segmap.put('SP','Supplemental Policy');
segmap.put('WC','Workers Compensation');
       segmap.put('WU','Wrap Up Policy');

       return segmap;
   }
   
   map<string, string> ebSeg06(){
       map<string, string> segmap= new map<string, string>();     
       
       segmap.put('6','Hour');
       segmap.put('7','Day');
       segmap.put('13','24 Hours');
       segmap.put('21','Years');
       segmap.put('22','Service Year');
       segmap.put('23','Calendar Year');
       segmap.put('24','Year to date');
       segmap.put('25','Contract');
       segmap.put('26','episode');
       segmap.put('27','Visit');
       segmap.put('28','Outlier');
       segmap.put('29','Remaining');
       segmap.put('30','Exceed');
       segmap.put('31','Not Exceeded');
       segmap.put('32','Lifetime');
       segmap.put('33','lifetime Remaining');
       segmap.put('34','Month');
       segmap.put('35','Week');
       segmap.put('36','Admission');
       
       return segmap;
   }
   
   map<string, string> ebSeg12(){
       map<string, string> segmap= new map<string, string>();     
       segmap.put('Y','In network');
       segmap.put('N','Out of network');
       segmap.put('U','Unknown');
       segmap.put('W','In network and Out of network benefits are the same');
       
       return segmap;
   }    
}