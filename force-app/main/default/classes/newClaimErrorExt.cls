public with sharing class newClaimErrorExt {
    
    public sObject obj {get; set;}
    public caClaim__c claim {get; set;}
    public string claimName {get; private set;}
    public boolean isError {get; private set;}
    public string theUrl {get; private set;}
    
    public newClaimErrorExt(ApexPages.StandardController controller){
        isError = true;
        theUrl = URL.getSalesforceBaseUrl().toExternalForm();
        string type = controller.getRecord().getSObjectType().getDescribe().getName();
        
        if(type=='Claim_Audit_Error__c'){
            obj = new Claim_Audit_Error__c();
            obj = (Claim_Audit_Error__c)controller.getRecord();
        
        }else if(type=='Claim_Audit__c'){
            obj = new Claim_Audit__c();
            obj = (Claim_Audit__c)controller.getRecord();
        
        }    

        claim = new caClaim__c();
        if(UserInfo.getUserName().contains('jmsassoc')){
            try{
            claim.client__c = [select id from Claims_Audit_Client__c where name = 'Bridgestone'].id;
            }catch(exception e){}
        }
        
    }
    
    public void mySave(){
    isError = false;
        
        try{
            insert obj;
            
        }catch(DmlException e){
            
            isError = true;
            integer x = e.getNumDml();
            for(integer i=0; i<x; i++){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
            }    
            
        }
        
    }

    public void newClaimSave(){
    isError = false;
        
        try{
            insert claim;
            claimName = [select name from caClaim__c where id = :claim.id].name;
        }catch(DmlException e){
            
            isError = true;
            integer x = e.getNumDml();
            for(integer i=0; i<x; i++){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
            }    
            
        }
        
    }

}