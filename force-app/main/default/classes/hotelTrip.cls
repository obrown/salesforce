public with sharing class hotelTrip{
    
    public HotelTrips__c   hotelTrip {get; set;}
    public ContactRelatedtoHotel__c[] hotelTravelers {get; private set;} 
    public hotelTrips__c[] hotelTrips {get; private set;} 
    public hotelContactWrapper[] hotelContacts {get; private set;} 
    public selectOption[] hotels {get; private set;}
    
    id clientId,procedureId,clientFacilityId,patientCaseId;
    id memberTripId;
    set<id> eIdSet = new set<id>();
    
    public boolean isError {get; private set;}
    public string message {get; private set;}
    
    public hotelTrip(id memberTripId, id patientCaseId){
        this.memberTripId=memberTripId;
        this.patientCaseId=patientCaseId;
    }
    
    public hotelTrip(id memberTripId, set<id> eIdSet,id clientId, id procedureId, id patientCaseId){
        this.memberTripId=memberTripId;
        this.eIdSet=eIdSet;
        this.clientId=clientId;
        this.procedureId=procedureId;
        this.patientCaseId=patientCaseId;
        loadHotelTrips();
    }
    
    public void newHotel(id clientFacilityID){
        try{
            hotelTrip = new HotelTrips__c(MemberTripID__c=memberTripId);
            loadHotelContacts(null);
            this.clientFacilityId=clientFacilityId;
            loadHotels();
        }catch(exception e){
            system.debug(e.getLineNumber()+' '+e.getMessage());
        }
    }
    
    public void deleteHotelTrip(id hotelTripId){
        hotelTrips__c ht = new hotelTrips__c (id=hotelTripId);
        delete ht;
        
        patient_case__c pc = new patient_case__c(id=patientCaseId);
        pc.Hotel_Amount__c = null;
        pc.Hotel_Rate__c = null;
        pc.Hotel_Check_In_Date__c= null;
        pc.Hotel_Checkout_Date__c= null;
        pc.Shuttle_Information_Arrival__c= null;
        pc.Shuttle_Information_Departure__c= null;
        update pc;
        
        loadHotelTrips();
        loadHotelContacts(null);
    }
    
    public void getHotelTrip(id hotelTripId, id clientFacilityID){
        loadHotelContacts(hotelTripId );
        this.clientFacilityId=clientFacilityId;
        loadHotels();
        for(hotelTrips__c ht: hotelTrips){
            if(ht.id == hotelTripId){
                hotelTrip = ht;
                break;
            }
        }
    }
    
    public void setMemeberTripId(id theId){
        this.memberTripId=theId;
    }
    
    public void cancelHotel(){
        hotelTrip.Cancelled__c=true;
    }
    
    
    
    public void saveHotel(){
        isError=false;
        message='';
        set<id> selectHotelContacts = new set<id>();
        
        try{
        try{    
            if(hotelTrip.memberTripID__c==null){
                hotelTrip.memberTripID__c=memberTripID;
            } 
            if(hotelTrip.id!=null){
                try{
                    database.delete([select id from ContactRelatedtoHotel__c where HotelTrip__c =:hotelTrip.id]);
                    
                }catch(exception e){
                    system.debug(e.getMessage());
                }
           
            }

            boolean guestSelected=false;
            for(hotelContactWrapper hcw : hotelContacts){
                
                if(hcw.isChecked){
                    guestSelected=true;
                }
                  
            }  
            
            if(!guestSelected){
                isError=true;
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'Please add hotel guests'));  
                return;
            }
            
            upsert hotelTrip;
            ContactRelatedtoHotel__c[] crhList = new ContactRelatedtoHotel__c[]{};
            
            for(hotelContactWrapper hcw : hotelContacts){
                
                if(hcw.isChecked){
                    crhList.add(new ContactRelatedtoHotel__c(EmployeeContact__c=hcw.contact.id,HotelTrip__c=hotelTrip.id));
                    
                }
            }
            
            insert crhlist;
            loadHotelTrips();
            
            patient_case__c pc = new patient_case__c(id=patientCaseId);
            pc.ECEN_Hotel__c=hotelTrip.ECEN_Hotel__c;
            pc.Hotel_Amount__c = hotelTrip.Estimated_Hotel_Amount__c;
            pc.Hotel_Rate__c =hotelTrip.HotelRate__c;
            pc.Hotel_Check_In_Date__c=hotelTrip.CheckInDate__c;
            pc.Hotel_Checkout_Date__c=hotelTrip.CheckOutDate__c;
            pc.Shuttle_Information_Arrival__c=hotelTrip.ArrivalShuttleInformation__c;
            pc.Shuttle_Information_Departure__c=hotelTrip.DepartureShuttleInformation__c;
            update pc;
               
            message='Record Saved';
            
        }catch(dmlexception e){
                    isError=true;
                    system.debug(e.getMessage());
                    system.debug(e.getLineNUmber());
            
                    message=e.getDMLMessage(0);
        }
            
        }catch(exception e){
                    isError=true;
                    system.debug(e.getMessage());
                    message=e.getMessage();
        }
    }
    
    void loadHotelContacts(id hotelTripId){
        
        set<id> hotelContactsSet = new set<id>();
        
        if(hotelTripId!=null){
            for(ContactRelatedtoHotel__c crh : [select EmployeeContact__c from ContactRelatedtoHotel__c where hotelTrip__c = :hotelTripId]){
                hotelContactsSet.add(crh.EmployeeContact__c);
            }
        }
        
        hotelContacts = new hotelContactWrapper[]{};
        for(employeeContacts__c ec : [select id,FirstName__c,
                                                LastName__c,
                                                Gender__c,
                                                EmployeeRelationShip__r.name,
                                                EmployeeRelationShip__c from employeeContacts__c where id in :eIdSet]){
            
            if(hotelContactsSet.contains(ec.id)){
                hotelContacts.add(new hotelcontactWrapper(true, ec, null));
            }else{
                hotelContacts.add(new hotelcontactWrapper(false, ec, null));
            }
        
        }
        
    }
    
    void loadHotels(){ 
        hotels= coeSelfAdminPickList.selectOptionList(null,clientId, procedureId, clientFacilityId, coeSelfAdminPickList.hotelName, null, true);
        
    }
    
    void loadHotelTrips(){
        hotelTrips = new hotelTrips__c[]{};
        loadHotelContacts(null);
        
        try{
            
            hotelTrips = [select ArrivalShuttleInformation__c,
                                Airport_Transport__c,
                                Name,
                                ECEN_Hotel__r.name,
                                Cancelled__c,
                                CheckInDate__c,
                                CheckOutDate__c,
                                DepartureShuttleInformation__c,
                                Estimated_Parking__c,
                                Estimated_Hotel_Amount__c,
                                HotelAmount__c,
                                Hotel_Stay_days__c,
                                HotelRate__c,
                                hotelTripName__c,
                                Hospital_Transport__c,
                                MemberTripID__r.patient_case__c,
                                ConfirmationNumber__c,
                                LateCheckoutNeeded__c,
                                NumberOfGuests__c,
                                MemberTripID__c,
                                Parking__c,
                                Tax__c,
                                Transportation_Tax__c,
                                Total_Hotel_Amount__c,
                                createdDate,
                                createdBy.name from HotelTrips__c where MemberTripID__c = :memberTripId and MemberTripID__c !=null];
       
        }catch(queryException qe){
            //catch silently
            system.debug(qe.getMessage());
        }
    }
    
    
    public class hotelContactWrapper{
    
        public boolean isChecked {get; set;}
        public employeeContacts__c contact {get; set;}
        public HotelTrips__c hotel {get; set;}
        
        public hotelContactWrapper(boolean isChecked, employeeContacts__c contact, HotelTrips__c hotel){
            this.isChecked = isChecked;
            this.contact = contact;
            this.hotel = hotel;
            
        }
        
    }
}