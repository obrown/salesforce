public with sharing class highDollarController {
    public Claim_Audit__c obj {
        get; private set;
    }
    Claim_Audit__c caHD = new Claim_Audit__c();
    public boolean error {
        get; private set;
    }
    public string newID {
        get; set;
    }
    public string pName {
        get; private set;
    }
    public string eName {
        get; private set;
    }
    public string theAge {
        get; private set;
    }
    public string theRel {
        get; private set;
    }
    id theID;
    id highDollarRecordId;
    id highClaimAuditorId;
      //8-04-21 gitlabs docker was down was not able to deploy.
    public highDollarController(ApexPages.StandardController controller) {
        theID = controller.getId();
        highDollarRecordId = [select id from recordtype where sObjectType = 'Claim_Audit__c' and name = 'High Dollar' and isActive = true].id;
        highClaimAuditorId = [select id from Claims_Auditor__c where audit_type__c = 'High Dollar' limit 1].id;
        loadObj();
        pName = ApexPages.CurrentPage().getParameters().get('pName');
        eName = ApexPages.CurrentPage().getParameters().get('eName');
        theAge = ApexPages.CurrentPage().getParameters().get('age');
        theRel = ApexPages.CurrentPage().getParameters().get('rel');
    }

    public highDollarController(){
        theID = ApexPages.currentPage().getParameters().get('id');
        highDollarRecordId = [select id from recordtype where sObjectType = 'Claim_Audit__c' and name = 'High Dollar' and isActive = true].id;
        highClaimAuditorId = [select id from Claims_Auditor__c where audit_type__c = 'High Dollar' limit 1].id;
        loadObj();
    }

    void loadObj(){
        if (theID != null) {
            obj = [select Adjusted_Claim__c,
                   Adjusted_Claim_Reason__c,
                   Claim_Processor__c,
                   Claim_Auditor__c,
                   claim__r.Amount_to_be_paid__c,
                   claim__r.Allowed_Amount__c,
                   claim__r.Date_Received__c,
                   claim__r.Total_Charge__c,
                   claim__r.Claim_Number__c,
                   Claim__c,
                   claim__r.Diagnosis__c,
                   claim__r.Diagnosis_Description__c,
                   Claim_Auditor__r.user__r.email,
                   claim__r.Procedure__c,
                   claim__r.Procedure_Description__c,
                   claim__r.claim_received_date__c,
                   Coding_Credits__c,
                   Coding_Errors__c,
                   Comments__c,
                   Does_COB_Apply__c,
                   Does_Subrogation_Apply__c,
                   DRG_Pricing__c,
                   Final__c,
                   recordtypeid,
                   Audit_Type__c,
                   Audit_Status__c,
                   Pricing_Method_if_not_DRG__c,
                   In_Network__c,
                   Network_Name__c,
                   Negotiation_Attempted__c,
                   Negotiated_Amount__c,
                   Number_of_lines__c,
                   Employee_Name__c,
                   relatedClaimAudit__c,
                   Patient_Age__c,
                   Patient_Name__c,
                   Patient_Effective_Date__c,
                   Patient_Termination_Date__c,
                   Precert__c,
                   Precert_Begin__c,
                   Precert_End__c,
                   Precert_Number_of_Days__c,
                   Relationship_to_Member__c,
                   fDOS__c,
                   tDOS__c,
                   Client__c,
                   name,
                   Stop_Loss_Notification_Initiated__c,
                   Stop_Loss_Notification_Date__c,
                   Stop_Loss_Comments__c,
                   Unresolved_Errors__c,
                   Provider_Name__c,
                   recordtype.name,
                   wa__c,
                   wa__r.user__r.email from Claim_Audit__c where id = :theID];
        }

        pName = ApexPages.CurrentPage().getParameters().get('pName');
        eName = ApexPages.CurrentPage().getParameters().get('eName');
        theAge = ApexPages.CurrentPage().getParameters().get('age');
    }

    public void Save(){
        error = false;
        try{
            if (obj.Precert_Begin__c != null && obj.Precert_End__c != null) {
                obj.Precert_Number_of_Days__c = obj.Precert_Begin__c.daysBetween(obj.Precert_End__c) + 1;
            }

            update obj;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Record Saved'));
        }catch (exception e) {
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
    }

    public void Submit(){
        error = false;

        if (obj.Unresolved_Errors__c > 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Claim Audit still has unresolved errors'));
            return;
        }

        if (obj.recordType.name == 'High Dollar Review') {
            return;
        }

        try{
              //Clone current claim Audit
            if (obj.relatedClaimAudit__c == null) {
                caHD = obj.clone();
            }
            else{
                caHD = [select Audit_Status__c,
                        Audit_Type__c,
                        Claim__c,
                        Claim_Auditor__c,
                        Claim_Processor__c,
                        Coding_Credits__c,
                        Coding_Errors__c,
                        Comments__c,
                        recordtypeid,
                        number_of_lines__c,
                        relatedClaimAudit__c from Claim_Audit__c where id = :obj.relatedClaimAudit__c];
            }

            caHD.recordtypeid = highDollarRecordId;
            caHD.Audit_Type__c = 'High Dollar';
            caHD.Audit_Status__c = 'To Be Audited';
            caHD.Amount_to_be_paid__c = obj.claim__r.Amount_to_be_paid__c;
            caHD.Claim_Auditor__c = highClaimAuditorId;
            id processorID;

            try{
                if (obj.Claim_Auditor__c == null) {
                    processorID = [select id from Claims_Processor_Crosswalk__c where email_address__c = :obj.wa__r.user__r.email limit 1].id;
                }
                else{
                    processorID = [select id from Claims_Processor_Crosswalk__c where email_address__c = :obj.Claim_Auditor__r.user__r.email limit 1].id;
                }
            }catch (exception e) {
                if (obj.Claim_Auditor__c == null) {
                    error = true;
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please add a either a AAA processor or a WA processor to the audit'));
                    return;
                }
                User uFoo = [select email, firstname, lastname from user where id = :obj.Claim_Auditor__r.user__c];
                Claims_Processor_Crosswalk__c foo = new Claims_Processor_Crosswalk__c(isActive__c = true, name = uFoo.firstName + ' ' + uFoo.lastName, email_address__c = uFoo.email);
                insert foo;
                processorID = foo.id;
            }

            caHD.Claim_Processor__c = processorID;
            upsert caHD;

            obj.Audit_Status__c = 'Completed';
            obj.recordtypeid = [select id from recordtype where sObjectType = 'Claim_Audit__c' and name = 'Completed' and isActive = true].id;
            update obj;

            newID = string.valueof(caHD.id);
        }catch (exception e) {
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ', line number ' + e.getlineNumber()));
              //Clean newly created record
            if (caHD.id != null) {
                delete caHD;
            }
        }
    }

    public pageReference createPDF(){
        error = false;

        string theError = checkforErrors();

        if (theError != '') {
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, theError));
            return null;
        }

        try{
            //Create pdf high dollar form for review

            PageReference auditForm = new ApexPages.StandardController(obj).view();

            auditForm = page.highDollarClaimAuditPDF;

            auditForm.getParameters().put('pName', obj.Patient_Name__c);
            auditForm.getParameters().put('eName', obj.Employee_Name__c);
            auditForm.getParameters().put('age', obj.Patient_Age__c);
            auditForm.getParameters().put('rel', obj.Relationship_to_Member__c);
            auditForm.getParameters().put('id', obj.ID);

            blob aBlob;
            if (!Test.isRunningtest()) {
                aBlob = auditForm.getContent();
            }
            else{
                aBlob = [select body from attachment limit 1].body;
            }

            Attachment theForm = new Attachment(parentId = obj.ID, name = obj.name + ' High Dollar Committee Review.xls', body = aBlob);

            if (obj.recordType.name == 'High Dollar Review') {
                theForm.name = obj.name + '_Updated.xls';
            }
            else{
                obj.Audit_Status__c = 'High Dollar Committee Review';
                update obj;
            }

            insert theForm;

            Claim_Audit_Form__c caf = new Claim_Audit_Form__c(claim_audit__c = obj.id, name = theform.name);
            string theurl = string.valueof(URL.getSalesforceBaseUrl());

            if (theurl.indexof('https://') > 1) {
                theurl = theurl.substring(theurl.indexof('https://'), theurl.indexof('https://') + 39);
            }

              //theurl = theurl + '/servlet/servlet.FileDownload?file=' + string.valueOf(theForm.id);
            theurl = 'https://contigohealth.my.salesforce.com/servlet/servlet.FileDownload?file=' + string.valueOf(theForm.id);//8-4-21 o.brown hardcoded fix need to update if instance changes again******
            caf.attachment_link__c = theurl;
            insert caf;
        }catch (exception e) {
            error = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage() + ', line number ' + e.getlineNumber()));
        }

        return null;
    }

    string checkforErrors(){
        string theError = '<br>';

        if (obj.Adjusted_Claim__c == null) {
            theError = theError + 'Is this an Adjusted Claim?<br><br>';
        }

        if (obj.Adjusted_Claim__c == 'Yes' && (obj.Adjusted_Claim_Reason__c == null || obj.Adjusted_Claim_Reason__c == '')) {
            theError = theError + 'Please populate the adjusted claim reason<br><br>';
        }

        if (obj.In_Network__c == null) {
            theError = theError + 'Is this claim in-network?<br><br>';
        }

        if (obj.In_Network__c == 'Yes' && (obj.Network_Name__c == null || obj.Network_Name__c == '')) {
            theError = theError + 'Please populate the network name<br><br>';
        }

        if (obj.DRG_Pricing__c == null) {
            theError = theError + 'DRG Pricing?<br><br>';
        }

        if (obj.DRG_Pricing__c == 'No' && (obj.Pricing_Method_if_not_DRG__c == null || obj.Pricing_Method_if_not_DRG__c == '')) {
            theError = theError + 'Please populate the DRG pricing method<br><br>';
        }

        if (obj.Negotiation_Attempted__c == null) {
            theError = theError + 'Was negotiation attempted?<br><br>';
        }

        if (obj.Negotiation_Attempted__c == 'Yes' && obj.Negotiated_Amount__c == null) {
            theError = theError + 'Please populate the Negotiated Amount<br><br>';
        }

        if (obj.Precert__c == null) {
            theError = theError + 'Precert?<br><br>';
        }

        if (obj.Precert__c == 'Yes') {
            if ((obj.Precert_Begin__c == null || obj.Precert_End__c == null) && obj.Precert_Number_of_Days__c == null) {
                theError = theError + 'Please populate the precert number of days<br><br>';
            }
        }

        if (obj.Does_COB_Apply__c == null) {
            theError = theError + 'Does COB apply?<br><br>';
        }

        if (obj.Does_Subrogation_Apply__c == null) {
            theError = theError + 'Does the subrogration apply?<br><br>';
        }

        if (obj.Stop_Loss_Notification_Initiated__c == null) {
            theError = theError + 'Stop Loss Notification Initiated?<br><br>';
        }

        if (obj.Stop_Loss_Notification_Initiated__c == 'Yes' && obj.Stop_Loss_Notification_Date__c == null) {
            theError = theError + 'Please populate the date stop loss initiated<br><br>';
        }

        if (theError == '<br>') {
            theError = '';
        }

        return theError;
    }
}