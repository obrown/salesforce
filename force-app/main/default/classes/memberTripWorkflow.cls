public with sharing class memberTripWorkflow{
    
    
    public static void deletedTrip(set<id> patient_id_set){
        set<string> fields = new set<string>();
        
        fields.add('Actual_Arrival__c');
        fields.add('Actual_Departure__c');
        fields.add('Estimated_Arrival__c');
        fields.add('Estimated_Departure__c');
        
        fields.add('Travel_to_COE__c');
        fields.add('Travel_Type__c');
        fields.add('Roundtrip_Mileage_to_COE__c');
        fields.add('Driving_Mileage_Reimbursement__c');//added 10-12-20
        fields.add('Airport_Mileage_Reimbursement__c');
        fields.add('Roundtrip_Mileage_to_the_Airport_Station__c');
        fields.add('Arriving_Flight_Number__c');
        fields.add('Arriving_Flight_Time__c');
        fields.add('Departing_Flight_Number__c');
        fields.add('Departing_Flight_Time__c');
        fields.add('Departing_Flight_Number__c');//added 10-12-20
        fields.add('Shuttle_Information_Arrival__c');
        fields.add('Shuttle_Information_Departure__c');
        fields.add('Hotel_Amount__c');
        fields.add('Hotel_Check_In_Date__c');
        fields.add('Hotel_Checkout_Date__c');
        
        patient_case__c[] cases_to_update = new patient_case__c[]{};
        
        for(id case_id : patient_id_set){
            
            patient_case__c pc = new patient_case__c(id=case_id);
            
            for(string f : fields){
                pc.put(f, null);
            }
            
            cases_to_update.add(pc);
        
        }
        
        database.saveResult[] results = database.update(cases_to_update, false);
        system.debug(results);
    }
}