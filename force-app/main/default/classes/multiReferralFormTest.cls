/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
* */

@isTest
private class multiReferralFormTest {

    static testMethod void myUnitTest() {
        
        Patient_Case__c pc = new Patient_Case__c();
        pc.Relationship_to_the_Insured__c = 'Patient is Insured';
        pc.Employee_First_Name__c = 'Johnny';
        pc.Employee_Last_Name__c = 'Test';
        pc.Employee_DOB__c = date.today().addyears(-30);
        pc.Employee_Gender__c = 'Male';
        pc.Employee_SSN__c = '123456789';
        pc.Same_as_Employee_Address__c = true;
        pc.Employee_Street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85206';
        pc.Employee_Home_Phone__c = '(480) 555-1212';        
        pc.bid__c = '123456';
                
        
        pc.recordtypeid = Schema.SObjectType.Patient_Case__c.getRecordTypeInfosByName().get('Walmart Cardiac').getRecordTypeId();
        pc.Client_Name__c = 'Walmart';
        insert pc;
        ApexPages.currentPage().getParameters().put('id', pc.id); 
        multiReferralFormsController mrf5 = new multiReferralFormsController();
        
        pc.recordtypeid = Schema.SObjectType.Patient_Case__c.getRecordTypeInfosByName().get('Walmart Spine').getRecordTypeId();
        update pc;
        multiReferralFormsController mrf6 = new multiReferralFormsController();
        
        pc.recordtypeid = Schema.SObjectType.Patient_Case__c.getRecordTypeInfosByName().get('Lowes Cardiac').getRecordTypeId();
        update pc;
        multiReferralFormsController mrf7 = new multiReferralFormsController();                             

        pc.recordtypeid = Schema.SObjectType.Patient_Case__c.getRecordTypeInfosByName().get('Lowes Spine').getRecordTypeId();
        update pc;
        multiReferralFormsController mrf8 = new multiReferralFormsController();  

        pc.recordtypeid = Schema.SObjectType.Patient_Case__c.getRecordTypeInfosByName().get('PepsiCo Cardiac').getRecordTypeId(); 
        update pc;
        multiReferralFormsController mrf10 = new multiReferralFormsController();        

        pc.recordtypeid = Schema.SObjectType.Patient_Case__c.getRecordTypeInfosByName().get('HCR ManorCare Cardiac').getRecordTypeId();  
        update pc;     
        multiReferralFormsController mrf12 = new multiReferralFormsController();

        mrf12.runrenderpdf();
        mrf12.getURL();
        mrf12.selectedForm();
        mrf12.geterrorMessage();
        mrf12.formSelected();
        mrf12.gettheOpp();

    }
}