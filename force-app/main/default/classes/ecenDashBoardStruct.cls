public without sharing class ecenDashBoardStruct{
       
    
    public string ToDoItemsComplete;
    public string WelcomeMessage;
    
    /*
    
    0= false
    1= true
    2= do not use
    
    */
    
    public class graph{
        public string[] items;
        public integer graphPercent;
        
        public graph(){
            items = new string[]{};
        }
        
        public graph(string[] items , integer graphPercent){
            this.items=items ;
            this.graphPercent=graphPercent;
            
        }
    }
     
    public class milestone{
        public string label;
        public integer value;
        public string color;  
        
        public milestone(string label, integer value){
            this.label=label;
            this.value=value;
        
        }
    
    } 

}