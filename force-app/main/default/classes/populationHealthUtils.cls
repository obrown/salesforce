public with sharing class populationHealthUtils {
    public class patientKeyStruct {
        string cert, ssn, sequence, underwriter, dob, grp;
        public patientKeyStruct(string cert, string ssn, string sequence, string underwriter, string dob, string grp){
            this.cert = cert;
            this.ssn = ssn;
            this.sequence = sequence;
            this.underwriter = underwriter;
            this.dob = dob;
            this.grp = grp;
        }
    }

    public static string patientkey(patientKeyStruct patientKey){
        string pk = '';

        if (patientKey.cert != null &&
            patientKey.cert.length() > 3) {
            pk = patientKey.cert.right(4);
        }

        if (patientKey.ssn != null &&
            patientKey.ssn.length() > 3) {
            pk = pk + '' + patientKey.ssn.right(4);
        }

        pk += '' + patientKey.sequence + '' + patientKey.underwriter + '' + patientKey.dob.left(2) + '' + patientKey.grp;
        return pk;
    }

    public static boolean isUmCaseApproved(string caseStatus){
        boolean result = false;

        if (caseStatus == 'Approved - Needs Review' || caseStatus == 'Approved – Final' || caseStatus == 'Approved') {
            result = true;
        }

        return result;
    }

    public static boolean isUmCaseDenied(string caseStatus){
        boolean result = false;

        if (caseStatus == 'Denied' || caseStatus == 'Canceled') {
            result = true;
        }

        return result;
    }

    public static string replaceSpecialCharacters(string s){
        string final_string = '';
        long x = s.length();
        string replacement_character = '_'; //,'\\'

        string[] characters = new string[] {
            '/', '–', ':', '(', ')'
        };
        boolean replace = false;

        for (integer i = 0; i < x; i++) {
            replace = false;
            for (string c : characters) {
                if (c == s.substring(i, i + 1)) {
                    replace = true;
                    break;
                }
            }
            if (replace) {
                final_string = final_string + '' + replacement_character;
            }
            else{
                final_string = final_string + '' + s.substring(i, i + 1);
            }
        }
        final_string = final_string.replaceAll('  ', ' ').replaceAll('⅒', 'a tenth').replaceAll('⅓', 'a third').replaceAll('¼', 'a quarter').replaceAll('½', 'a half');
        return final_string;
    }
}