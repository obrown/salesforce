public without sharing class createEcenRecords{

    public static Client__c createClient(string name, string underwriter, string grp){
        Client__c theClient = new Client__c(name=name, Underwriter__c=underwriter, Group_Number__c=grp, active__c=true);
        insert theClient;
        return theClient;
    }
    
    public static Procedure__c createProcedure(string name){
        Procedure__c theProcedure = new Procedure__c(name=name);
        insert theProcedure;
        return theProcedure;
    }
    
    public static Facility__c createFacility(string name, string address, string city, string state, string zip, string lat, string lng){
        Facility__c theFacility = new Facility__c(name=name,Facility_Address__c=address,Facility_City__c=city,Facility_State__c=state,Facility_Zip__c=zip,Latitude__c=lat,Longitude__c=lng);
        insert theFacility;
        return theFacility;
    }
    
    public static Client_Facility__c createClientFacility(id client, id procedure, id facility){
        Client_Facility__c theCF = new Client_Facility__c(name='Anything',Client__c=client,Facility__c=facility,Procedure__c=procedure);
        insert theCF ;
        return theCF ;
    }
    
    public static Carrier__c createCarrier(string name, string address, string city, string state, string zip){
        Carrier__c theCarrier = new Carrier__c(name=name, Address__c=address, City__c=city, State__c=state, Zip_Code__c=zip);
        insert theCarrier;
        return theCarrier;
    
    }

    public static Client_Carrier__c createClientCarrier(id carrier, id client){
        Client_Carrier__c theCarrier = new Client_Carrier__c (Carrier__c=carrier, Client__c=client);
        insert theCarrier;
        return theCarrier;
    
    }
    
    public static Physician__c createPhysician(){
        Physician__c Physician = new Physician__c(name='john Doe', First_Name__c='John', Last_Name__c='Doe', Credentials__c='MD');
        insert Physician;
        return Physician;
    
    }
    
    public static Hotel__c createHotel(string name){
        Hotel__c Hotel = new Hotel__c(name=name);
        insert Hotel;
        return Hotel;
    
    }

    public static patient_case__c createPatientCase(string clientName, string programType){
        Patient_Case__c patientCase = new Patient_Case__c();
        
        patientCase = (Patient_Case__c)setFormulafield(patientCase, 'Client_Name__c',clientName);
        patientCase = (Patient_Case__c)setFormulafield(patientCase, 'Program_Type__c',programType);
        
        insert patientCase;
        patientCase = [select Client_Name__c, Program_Type__c, name from Patient_Case__c where id = :patientCase.id];
        return patientCase;
    
    }    
    public static SObject setFormulafield(SObject sObj, String fieldName, Object value){
        String jsonString = JSON.serialize(sObj);
        Map<String,Object> dataMap = (Map<String,Object>)JSON.deserializeUntyped(jsonString);
        dataMap.put(fieldName, value);
        jsonString = JSON.serialize(dataMap);
        return (SObject)JSON.deserialize(jsonString, SObject.class);
    }

}