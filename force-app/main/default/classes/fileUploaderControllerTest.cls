/**
* This class contains unit tests for validating the behavior of  the Apex FileUploader class
*/

@isTest(seealldata=false)
private class fileUploaderControllerTest {

    static testMethod void fileUploader() {
        
        fileUploader fu = new FileUploader();
        fu.percentToAudit = '20';
        fu.insertRecords = true;
        
        caClaim__c[] caList = new caClaim__c[]{};
        Claims_Audit_Client__c cac = new Claims_Audit_Client__c(Underwriter__c='123',group_number__c='123', percent_to_pull__c=2);
        insert cac;
                
        caClaim__c ca0  = new caClaim__c(name='12345678900');
        caClaim__c ca1  = new caClaim__c(name='12345678901');
        caClaim__c ca2  = new caClaim__c(name='12345678902');
        caClaim__c ca3  = new caClaim__c(name='12345678903');
        caClaim__c ca4  = new caClaim__c(name='12345678904');
        caClaim__c ca5  = new caClaim__c(name='12345678905');
        caClaim__c ca6  = new caClaim__c(name='12345678906');
        caClaim__c ca7  = new caClaim__c(name='12345678907');
        caClaim__c ca8  = new caClaim__c(name='12345678908');
        caClaim__c ca9  = new caClaim__c(name='12345678909');
        caClaim__c ca10  = new caClaim__c(name='12345678910');
        caClaim__c ca11  = new caClaim__c(name='12345678911');
        caClaim__c ca12  = new caClaim__c(name='12345678912');
        caClaim__c ca13  = new caClaim__c(name='12345678913');
        caClaim__c ca14  = new caClaim__c(name='12345678914');
        caClaim__c ca15  = new caClaim__c(name='12345678915');
        caClaim__c ca16  = new caClaim__c(name='12345678916');
        caClaim__c ca17  = new caClaim__c(name='12345678917');
        caClaim__c ca18  = new caClaim__c(name='12345678918');
        caClaim__c ca19  = new caClaim__c(name='12345678919');
        caClaim__c ca20  = new caClaim__c(name='12345678920');
        
        caList.add(ca0);
        caList.add(ca1);
        caList.add(ca2);
        caList.add(ca3);
        caList.add(ca4);
        caList.add(ca5);
        caList.add(ca6);
        caList.add(ca7);
        caList.add(ca8);
        caList.add(ca9);
        caList.add(ca10);
        caList.add(ca11);
        caList.add(ca12);
        caList.add(ca13);
        caList.add(ca14);
        caList.add(ca15);
        caList.add(ca16);
        caList.add(ca17);
        caList.add(ca18);
        caList.add(ca19);
        caList.add(ca20);
        //insert caList;
                    //"030","CCH","21888810700","MCAMER",9/17/2018,9/24/2018,9/14/2018,52.00,"MM","H","ANTHEM",11.50,10.35

                       
 string testFile = 'PUNBR,GRNBR,CLAIM,CHGBY,CHGDATE,PAYDATE,INPDATE,TOTCHG,PRODUCT,FORM,INPBY\n';
        testFile += '123,123,12345678900,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,"1,111.50",10.35\n';
        testFile += '123,123,12345678901,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678902,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678904,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678905,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678906,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678907,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678908,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678909,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678910,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678911,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678912,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678913,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678914,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678915,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678916,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678917,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678918,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678919,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
        testFile += '123,123,12345678920,ANTHEM,2/7/2015,2/16/2015,2/6/2015,123,MM,H,ANTHEM,11.50,10.35\n';
                
        fu.nameFile = testFile;
        fu.ReadClaimsFile();
        /*
        system.assert(fu.clmsToUpload[0].Claim_Received_Date__c == date.valueof('2015-02-06'));
        system.assert(fu.clmsToUpload[0].Date_Paid__c == date.valueof('2015-02-16'));
        system.assert(fu.clmsToUpload[0].Claim_Processed_Date__c == date.valueof('2015-02-07'));
        system.assert(fu.clmsToUpload[0].Total_Charge__c == 123);
        system.assert(fu.clmsToUpload[0].Product__c == 'MM');
        system.assert(fu.clmsToUpload[0].Form_Type__c == 'H');
        system.assert(fu.clmsToUpload[0].Source_Type__c == 'ANTHEM');
        */
        fu.saveClaimsFile();
        
        fu = new FileUploader();
        fu.percentToAudit = '20';
        fu.insertRecords = true;
        fu.nameFile = testFile;
        //fu.ReadClaimsFile();
        fu.saveClaimsFileRandomSelection();
        
    }
}