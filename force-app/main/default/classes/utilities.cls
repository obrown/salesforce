public with sharing class utilities {
    
    string NeverHappenDate = '1000-1-1';
    
    public static boolean getIsHDPGrpMember(){
        try{
            groupMember gm = [select group.DeveloperName from GroupMember where UserOrGroupId = :UserInfo.getUserId() and group.DeveloperName ='Group_HDP'];
            if(gm.id!=null){
                return true;
            }
        }catch(queryException e){
            return false;
        }
        return false;
    }
    
    public static Boolean isRunningInSandbox(){
        return !Environment__c.getInstance().Production__c;
    }
    
    public static list<string> emailRec(id profID){
        
        list<String> emails = new list<String>();
        if(!utilities.isRunningInSandbox()){
            if(string.valueof(profID).left(3)=='00G'){
                set<id> users = new set<id>();
                GroupMember[] gs =[select UserOrGroupId,groupid From GroupMember where groupid = :profid];
                for(GroupMember g: gs){
                    users.add(g.UserOrGroupId);
                }
                
                for(User u :[select email from User where id IN :users]){
                    emails.add(u.email);
                }
                
            }else{
            
                list<User> theUsers = new list<User>([Select email From User where profileID = :profID and isActive = true]);
                for(User u: theUsers){
                    emails.add(u.email);
                }
            }
        }
        if(emails.isEmpty()){
            emails.add('mmartin@hdplus.com');
        }   
        return emails;   
    }
    
    public static String convertCRtoBR(String StringIn) {
    
    String result = '';

    if ((StringIn != null) && (StringIn != '')){    
      List<String> StringLines = StringIn.split('\n',0);
  
      for (String StringLine : StringLines){
        if ((result != '') && (StringLine != null)){
         result += '<BR>';
        }
            if (StringLine != null){
                result += StringLine;
            }
        }
      }  
    return result;
  }
  
  public static String errorMessageText(string theError){
    string foo = theError;
    integer firstLoc = theError.indexOf('_EXCEPTION,')+11;
    integer lastLoc = theError.indexOf(': [');
    try{
    foo = foo.substring(firstLoc, lastLoc);
    }catch (exception e){
        system.debug(e.getMessage());
    }
    return foo;
    
  }
  
  public static String errorMessageTextRF(string theError){
    string tError = theError;
    integer firstLoc = tError.indexOf(' missing:')+11;
        integer lastLoc = tError.indexOf(']:');
    
    try{
        tError = tError.substring(firstLoc, lastLoc);
    }catch (exception e){
        system.debug(e.getMessage());
    }
    
    return tError;
    
  }
  
    public static String authcontactEmail(string client, string facility, string authnum){
    
    if(isRunningInSandbox() && !test.isRunningTest()){
        return userInfo.getUserEmail();
    }
    
    if(client == 'Lowes' || client == 'HCR ManorCare' || client == 'Kohls'){
            
            if(facility == 'cardiac'){
                if(authnum=='N/A'){
                    return 'pre-access@ccf.org, CCMNSPAMC@ccf.org, CCPAMCHVISurgicalreferral@ccf.org, CCPAVEPAMC@ccf.org, CCPASPAMC@ccf.org';
                }else{
                   return 'CCPAMCHVISurgicalReferral@ccf.org, CCMNSPAMC@ccf.org';  
                }
                 
            }
            
            if(facility == 'spine' && client == 'Lowes'){
                return 'pre-access@ccf.org, CCMNSPAMC@ccf.org, CCPAMCORTHO-SPINEReferral@ccf.org, CCPAVEPAMC@ccf.org, CCPASPAMC@ccf.org'; 
            }
            
            if(facility == 'spine'){
                return 'CCMNSPAMC@ccf.org, CCPAVEPAMC@ccf.org'; 
            }
            
    }

    if(client == 'PepsiCo'){
        return 'bdoud@jhmi.edu'; 
    }
        
    return 'Unknown';
    
    }
  
  public static String contactEmail(string client, string facility){
    
    if(utilities.isRunningInSandbox() && !test.isRunningTest()){
        
        return userinfo.getUserEmail();
    }
    
    if(client == 'Walmart'){
        
        if(facility == 'Cleveland Clinic'){
            return 'CCMNSPAMC@ccf.org, CCPAMCHVISurgicalReferral@ccf.org';
        } 
        
        if(facility == 'Geisinger'){
            return '"HDPlus@geisinger.edu';
        }
        
        if(facility == 'Mercy Springfield'){
            return 'WMCOEClinical@hdplus.com,WMTSupport@hdplus.com';
        } 
                
        if(facility == 'Scott & White'){
            return 'Medicalconcierge@sw.org';
        } 

        if(facility == 'Virginia Mason'){
            return 'episodic.care.a@vmmc.org';
        } 
    }

    if(client == 'Lowes'&& facility == 'cardiac'){
        return 'CCPAMCHVISurgicalReferral@ccf.org, CCMNSPAMC@ccf.org'; 
    }


    if(client == 'PepsiCo'){
        return 'bdoud@jhmi.edu'; 
    }

    if(client == 'HCR ManorCare' || client == 'Kohls'){
        return 'CCPAMCHVISurgicalReferral@ccf.org, CCMNSPAMC@ccf.org '; 
    }
        
    return 'Unknown';
  }
  
  public static string RecordTypeName(string theID, string theobj){
    
    string rt = '';
    
    for(Recordtype ot : [select id,name from Recordtype where SobjectType =:theobj]){
        
        if(ot.id == theid){
            
            rt = ot.name;
            break;
        }
        
    }
    
    return rt;
  }
    
  //public static id RecordTypeIDbyName(string theName){
  //  return Schema.SObjectType.Patient_Case__c.getRecordTypeInfosByName().get(theName).getRecordTypeId();
 // }
    

  
  public static Messaging.SingleEmailMessage email(String tt, String theCC, String theBCC, string SenderDisplay,string theSubject, string theBody){
    string theTo = tt;
    list<String> finalTo = new list<String>();
    list<String> finalCC = new list<String>();
    list<String> finalBCC = new list<String>();
      
    if(theTo!=null){
        theTo=theTo.removeEnd(',');
        if(theTo.contains(',')){
            finalTo = theTo.split(',', -2);
        }else{
            finalTo.add(theTo);
        }
    }
    
    if(theCC!=null){
        if(theCC.contains(',')){
            finalCC = theCC.split(',', -2);
        }else{
            finalCC.add(theCC);
        }
    }
    
    if(theBCC!=null){
        if(theBCC.contains(',')){
            finalBCC = theBCC.split(',', -2);
        }else{
            finalBCC.add(theBCC);
        }
    }
            
    Messaging.SingleEmailMessage theMessage = new Messaging.SingleEmailMessage();
      if(finalTo != null){
       theMessage.setToAddresses(finalTo);
      }     
     
      if(finalCC != null){
       theMessage.setCcAddresses(finalCC);
      }      

      if(finalBCC != null){
       theMessage.setBccAddresses(finalBCC);
      } 
      
      if(SenderDisplay != null){
        theMessage.setSenderDisplayName(SenderDisplay);
      }     
      
      theMessage.setSubject(theSubject);
      theMessage.setPlainTextBody(theBody);
        
      return theMessage; 
  }           
  
  public static string formatDate(Integer i){
        string s = string.valueof(i);
        if(s.length()==1){
            return '0'+s;
        }
        
        return s;
  }
  
  public static string formatTime(dateTime dt){
      string foo ='';
      
      if(dt.hour()<=12){
          foo += ' '+dt.hour()+':'+ utilities.formatDate(dt.minute()) +' am';
      }else{
          foo += ' '+ (dt.hour()-12) +':'+ utilities.formatDate(dt.minute()) +' pm';
      }
      
      return foo;
  }
  
  public static string formatDateTime(dateTime dt){
      if(dt==null){return '';}
      string foo = dt.month()+'/'+dt.day()+'/'+dt.year();
      
      if(dt.hour()<=12){
          foo += ' '+dt.hour()+':'+ utilities.formatDate(dt.minute()) +' am';
      }else{
          foo += ' '+ (dt.hour()-12) +':'+ utilities.formatDate(dt.minute()) +' pm';
      }
      
      return foo;
  }
  
  public static string formatUSdate(date d){
    string foo = 'null';
    if(d!=null){
      
      foo = d.month() + '/' + d.day() + '/' + d.year(); 
    }
    
    return foo;
  }

  public static string formatUSdate(string d){
    string foo = '';
    
    if(d!=null){
      
       string theyear = d.left(4);  
       string themonth = d.substring(5,7);
       string theday = d.right(2);
          
      foo = themonth + '/' + theday + '/' + theyear; 
    }
    
    return foo;
  }
  
  public static string formatUSdateDashes(string d){
    string foo = '';
    
    if(d!=null){
       string[] fooArr = d.split('/');
       string theyear = fooArr[2];  
       string themonth = fooArr[0];
       if(integer.valueof(themonth)<10){
           theMonth='0'+theMonth;
       }  
       
       string theday = fooArr[1];  
       if(integer.valueof(theday)<10){
           theday='0'+theday;
       }
          
      foo = theyear + '-' + themonth + '-' + theday; 
    }
    
    return foo;
  }
  
  public date minEffectiveDate(id parentID, string stage, string theid){
    
    Date min=null;
    list<stage__c> minlist = new list<stage__c>();

        minlist = [select StageEff__c from stage__c where Transplant__c = :parentID and Stage__c = :stage order by StageEff__c asc];
        if(!minlist.isEmpty()){
            if(minlist[0].id == theid){
            min = date.valueof(NeverHappenDate);
            }else{
            min = minList[0].StageEff__c;   
            }  
        }
    
    return min;
  } 

  public date maxEffectiveDate(id parentID, string stage, string theid){
    
    Date max = null;
    list<stage__c> maxlist = new list<stage__c>();
    
    maxlist = [select StageEff__c from stage__c where Transplant__c = :parentID and Stage__c = :stage order by StageEff__c desc];
    if(maxlist.size()>0){
        if(maxList[0].id == theid){
            max = date.valueof(NeverHappenDate);
        }else{
            max = maxList[0].StageEff__c;   
        }
    }
            
    return max;
  }
  
  public date minTermDate(id parentID, string stage, string theid){
    
    Date min=null;
    list<stage__c> minlist = new list<stage__c>();
    
    minlist = [select StageTerm__c from stage__c where Transplant__c = :parentID and Stage__c = :stage order by StageTerm__c asc];
    if(minlist.size()>0){
        if(minlist[0].id == theid){
            min = date.valueof(NeverHappenDate);
        }else{
            min = minList[0].StageTerm__c;  
        }
    }
    
    
    return min;
  } 

  public date maxTermDate(id parentID, string stage, string theid){
    
    Date max = null;
    list<stage__c> maxlist = new list<stage__c>();
    
    maxlist = [select StageTerm__c from stage__c where Transplant__c = :parentID and Stage__c = :stage order by StageTerm__c desc];
    if(maxlist.size()>0){
        if(maxList[0].id == theid){
            max = date.valueof(NeverHappenDate);
        }else{
            max = maxList[0].StageTerm__c;  
        }
    }
    return max;
  } 
  
  public static boolean isInternal(){
     
     UserRole ur = new UserRole();
     try{
         ur = [select id, DeveloperName , ParentRoleId from UserRole where id = :userInfo.getUserRoleId()];
     }catch (exception e){
        system.debug(e.getMessage() );
     }
     
     if(ur == null){
         return false;
     }
     
     
     if(ur.DeveloperName != 'External' && ur.DeveloperName != 'Internal'){
        try{
            ur  = [select id, DeveloperName from UserRole where id = :ur.ParentRoleId]; //if the ur has no parentroleid , we are at the top
            system.debug(ur.DeveloperName + ' #');
        }catch(exception e){
            ur.DeveloperName = 'External';
        }
     }
  
     return ur.DeveloperName == 'Internal';
  
  }
  
  public static string getObjType(string prefix){
    
    string foo = '';
    
    Map<String,Schema.SobjectType> describe = Schema.getGlobalDescribe();
    
    for(string s: Schema.getGlobalDescribe().keySet() ){
        
        if(describe.get(s).getDescribe().getKeyPrefix() == prefix){
            foo = string.valueof(describe.get(s));
            break;
        }

    }
    
    return foo;
    
  }

  public static string appendMac(string body,string key, string alg, string pk){
    
    Blob mac = Crypto.generateMac(alg, blob.valueof(body), blob.valueOf(pk));
    string tBody = body.left(body.length()-1);
    tbody += ',"'+ key +'":"'+ EncodingUtil.base64Encode(mac) +'"}';
        
    return tBody;
    
  } 
  
  public static map<string, client__c> clientMap(){
      
      map<string,client__c> clientMap = new map<string,client__c>();
      
      for(Client__c c :[select underwriter__c, name from client__c order by name asc]){
          clientMap.put(c.underwriter__c, c);
      }
      
      return clientMap;
  }
    
  public static String toCamelCase(String s){
    if(s==null){
        return s;
    }
    
    String[] parts = s.split(' ');
    String camelCaseString = '';
    
    for (String part : parts){
       camelCaseString = camelCaseString +' '+toProperCase(part);
    }
    
    return camelCaseString;
  }
    
  public static String toProperCase(String s) {
    
    if(s==null){
        return s;
    }
    
    if(s.length()<2){
        return s.toUpperCase();
    }
    return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
  } 
  
  public static string[] carrierEmail(string carrierEmail){
      string[] result = new string[]{};
      // || utilities.isrunninginsandbox()
      
      if(carrierEmail==null || utilities.isrunninginsandbox()){
          result.add(userinfo.getuseremail());
      }else{
          for(string email : carrierEmail.split(',')){
              result.add(email);
          }
                    
      }
                    
      return result;
      
  }
  
  public static string decodeStatus(string status){
      
      switch on status{
          when 'A'{
              return 'Active';
          }
          when 'T'{
              return 'Termed';
          }
          when 'C'{
              return 'Cobra';
          }
          when 'R'{
              return 'Retired';
          }
          when 'D'{
              return 'Disabled';
          }
          when 'N'{
              return 'Cobra EHC';
          }
          when 'M'{
              return 'Medicare';
          }

      }
    
      return status;
  }
  
  public static string protocolAndHost(){
      
      string protocolAndHost ='';
      String orgId = UserInfo.getOrganizationId();
      String userId = UserInfo.getUserId();
      String sessionId = UserInfo.getSessionId();

      //we can trust getSalesforceBaseUrl within batches and schedules (but not in VF)
      if (sessionId == null) return Url.getSalesforceBaseUrl().toExternalForm().replace('http:', 'https:');

            PageReference pr = new PageReference('/id/' + orgId + '/' + userId);
            pr.getParameters().put('oauth_token', sessionId);
            pr.getParameters().put('format', 'json');

            //within test context use url class, else derive from identity api
            String data = Test.isRunningTest() ? '{"urls": {"rest": "' + Url.getSalesforceBaseUrl().toExternalForm() + '"}}' : pr.getContent().toString();
            Map<String,Object> result = (Map<String,Object>)Json.deserializeUntyped(data);
            Map<String,Object> urls = (Map<String,Object>)result.get('urls');
            Url rest = new Url((String)urls.get('rest'));
            protocolAndHost = rest.getProtocol() + '://' + rest.getHost();
        

        return protocolAndHost;
    
  }
  
  public static Boolean checkValidEmail(String emailAddress){
        String emailRegex = '([a-zA-Z0-9_\\-\\.]+)@((\\[a-z]{1,3}\\.[a-z]{1,3}\\.[a-z]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})';
        Pattern MyPattern = Pattern.compile(emailRegex);

        Matcher MyMatcher = MyPattern.matcher(emailAddress);

        if(MyMatcher.matches()){
           return true;
        }else{
           return false;
        }
    }
    
  public static  SObject setFormulafield(SObject sObj, String fieldName, Object value){
        String jsonString = JSON.serialize(sObj);
        Map<String,Object> dataMap = (Map<String,Object>)JSON.deserializeUntyped(jsonString);
        dataMap.put(fieldName, value);
        jsonString = JSON.serialize(dataMap);
        return (SObject)JSON.deserialize(jsonString, SObject.class);
    }
}
