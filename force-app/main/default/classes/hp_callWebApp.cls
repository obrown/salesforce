public with sharing virtual class hp_callWebApp{
    
    public string endpoint;
    floridaKeys__c fk = floridaKeys__c.getInstance();
    public jsonBuilder jb = new jsonBuilder();
    public string[] search = new string[]{};
    public string jsonBody ='';
    public string result;
    
    public string callWebApp(string testString){
        
        if(testString != null){
            return testString;
        }
        
        try{
            jsonBody = utilities.appendMac(jsonBody, 'bigmac', 'hmacSHA256', fk.eligApi__c);
            
            if(utilities.isRunningInSandbox()){
                result = ediUtil.callRH(jsonBody, 'https://secure.hdplus.com:6443/api/'+endpoint+'/');
                system.debug('body of call: ' + jsonBody);
                system.debug('result of call: ' + result);
                
            }else{
                
                result = ediUtil.callRH(jsonBody, 'https://secure.hdplus.com/api/'+endpoint+'/');
                if(userinfo.getUserEmail()=='mmartin@hdplus.com'){
                  system.debug('body of call: ' + jsonBody);
                  system.debug('result of call: ' + result);
                }
                
            }
                        
        }catch(exception e){
            return result;
        }
        
        return result;
    }

}