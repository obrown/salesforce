public with sharing class oncologyClaim{
    
    id parentId;
    public boolean showForm {get; set;}
    
    public OncologyClaim__c claim {get; set;}
    public OncologyClaim__c[] claims {get; set;}
    public string codeType {get; set;} 
    public Clinical_Codes__c theCCPOC {get; set;}
    public transient PaginatedSelectList codeOptions; 
    
    
    public oncologyClaim(id parentId){
        claim = new OncologyClaim__c (Oncology_Case__c=parentId);
        setparentId(parentId);
        loadclaims();
        showForm=false;
    }
    
    public void setparentId(id parentId){
        this.parentId=parentId;
    }
    
    public void loadclaims(){
        claims = new OncologyClaim__c[]{};
        try{
        claims = [select Additional_Comments__c,
                 Claim_Number__c,
                 Claims_Age__c,
                 Client__c,
                 client__r.take_deductible_and_coinsurance_for_all__c,
                 Clinical_Code__c,
                 Clinical_Code_Description__c,
                 Clinical_Code_Status__c,
                 Clinical_Code_Type__c,
                 Clinically_Appropriate_to_Release_Claim__c,
                 Clinical_Review__c,
                 Clinical_Reviewed__c,
                 Clinical_Review_Comment__c,
                 Clinical_Review_Date__c,
                 Clinical_Reviewed_By__c,
                 COE_Workflow_Manager_Updated__c,
                 Completed_Claim_Received__c,
                 Coverage_Level__c,
                 createdby.name,
                 createdDate,
                 Date_HDP_Paid_the_Claim__c,
                 Date_Received__c,
                 Deductible_Amount_1__c,
                 Deductible_Amount_2__c, 
                 Deductible_Met__c,
                 Deductible_Remaining__c,
                 Deductible_Verification_1__c,
                 Deductible_Verification_2__c,
                 Employee_HealthPlan__r.Associate_Only_Deductible__c,
                 Employee_HealthPlan__r.Associate_and_Dependents_Deductible__c,
                 Employee_HealthPlan__r.Associate_and_Family_Deductible__c,
                 Employee_HealthPlan__r.Annual_Out_of_Pocket_Max_Individual__c,
                 Employee_HealthPlan__r.Annual_Out_of_Pocket_Max_Assc_and_Dep__c,
                 Employee_HealthPlan__r.Annual_Out_of_Pocket_Max_Family__c,
                 Employee_Name__c,
                 Hit_Outlier__c,
                 Match_Proposed_Clinical_Codes__c,
                 Name,OOP_Remaining_Family__c,
                 Oncology_Case__r.Employee_HealthPlan__c,
                 OOP_Remaining_Individual__c,
                 Out_of_Pocket_Met_Family__c,
                 Out_of_Pocket_Met_Individual__c,
                 Patient_Name__c,
                 Processed_Date__c,
                 Reason_for_Clinical_Review__c,
                 Services_Outside_Bundle__c,
                 Total_Dollar_Outlier_after_Discount__c,
                 Total_Dollar_Outlier_in_excess__c,
                 Total_Dollar_Outlier_up_to_Threshold__c from OncologyClaim__c where Oncology_Case__c= :parentId order by createdDate desc];
                             
            }catch(queryException e){}   
              
            if(!claims.isEmpty()){
                claim = claims[0];
            }
    }
    
   
    Public void newclaim(){
        claim = new OncologyClaim__c (Oncology_Case__c=parentId);
        showForm=true;
        
    }       
    
    Public void openClaim(){
        string cid = ApexPages.CurrentPage().getParameters().get('cid');
        ApexPages.CurrentPage().getParameters().put('cid', null);
        for(OncologyClaim__c c : claims){
            if(c.id==cid){
                claim=c;
                showForm=true;
                break;
            }
            
        }
        
        
    }
    
    public class saveResponse{
        public boolean isError;
        public string message;
    }
    
    public saveResponse saveclaim(){
        saveResponse sr = new saveResponse();
        sr.isError=false;
        
        try{
            upsert claim;
            loadclaims();
            return sr;
           
        }catch(dmlException e){
           System.debug(e.getDmlMessage(0)+' '+e.getLineNumber()); 
           ApexPages.addMessages(e);
           sr.isError=true;
           sr.message=e.getDmlMessage(0);
           return sr; 
           
        }
        
    }

    public List<SelectOption> getcTypeItems() {
        
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('','--None--'));
        options.add(new SelectOption('DRG','DRG'));
        options.add(new SelectOption('CPT4','CPT4'));
        
        
        return options;
    }   

    public void refreshCodeList(){
        //Just here to cause a rerender
    }
    
    
    public PaginatedSelectList getcodeOptions(){
        codeOptions=new PaginatedSelectList();
        codeOptions.add(new SelectOption('','--'));
        if(codeType!=null && codetype!=''){
            for(Code__c c: [select code__c, Code_Description__c,name,type__c from Code__c where type__c =:codeType order by code__c asc]){
                codeOptions.add(new SelectOption(c.id,c.code__c + ' ' + c.code_description__c));
            }
        }

        return codeOptions;
        
    }

}