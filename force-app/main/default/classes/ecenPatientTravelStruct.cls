global class ecenPatientTravelStruct{
   
   public string travelType;
   public hotel hotel;
   public arrival arrival;
   public departure departure;
   public facility facility;
   
  // public string LoginSite;
  // public string LoginInstructions;
   
   public class travelItem{
       public string label='';
       public string displayDate='';
       public string displayTime='';
       
   }
   
   public class hotel{
       public string label;
       public string checkin='';
       public string checkout='';
   }
   
   public class arrival{
       public string recordNumber;
       public string displayDate;
       public string displayTime;
   }
   
   public class departure{
       public string recordNumber;
       public string displayDate;
       public string displayTime;
   }
   
   public class facility{
       
       public travelItem[] Appointments;
       public string label;
       public facility(){
           Appointments = new travelItem[]{};    
       }
   
   }
   
   public ecenPatientTravelStruct(){
       
       facility= new facility();
       
   }
   
}