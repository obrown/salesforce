public with sharing class utilizationManagementDenialLetterExt {
    public Utilization_Management_Clinical_Code__c[] diagnosisCodes {
        get; private set;
    }
    public Utilization_Management_Clinical_Code__c[] clinicalCodes  {
        get; private set;
    }
    public Utilization_Management_Clinician__c clinician {
        get; private set;
    }
    public map<id, Utilization_Management_Clinician__c> clinicianMap {
        get; set;
    }
    public selectOption[] umClinicians {
        get;  set;
    }
    public string letter {
        get; set;
    }
    public string medicalSignatureDirectory {
        get; private set;
    }
    public boolean isMedicalDirector {
        get; private set;
    }

    id umRecordId;

    public string denialReason {
        get; set;
    }
    public string letterType {
        get; set;
    }
    public selectOption[] letterOptions {
        get; set;
    }
    public Utilization_Management_Denial_Letter__c uml {
        get; set;
    }

    public boolean isError {
        get; private set;
    }

    //2-1-22 update ----------------------------start-------------------------------
    SystemID__c sid = SystemID__c.getInstance();
    id profID; 
    String logoPath;
    StaticResource logo, signature;//moved scope
    //2-1-22 update ----------------------------end---------------------------------  

    public utilizationManagementDenialLetterExt(ApexPages.StandardController controller){
        PermissionSetAssignment[] currentUserPerSet = [SELECT Id, PermissionSet.Name, AssigneeId FROM PermissionSetAssignment where AssigneeId = :Userinfo.getUserId()];
        isMedicalDirector = false;
        for (PermissionSetAssignment psa: currentUserPerSet) {
            if ('Medical_Director'.equals(psa.PermissionSet.Name)) {
                isMedicalDirector = true;
                break;
            }
        }
        /*2-4-22 o.brown refactored to pass logo based on profile
        StaticResource[] srList = [SELECT Id, NamespacePrefix, SystemModstamp, Name FROM StaticResource WHERE Name = 'HDP_Corporate_Logo' or Name = 'medicalDirectorSignature' order by Name asc];
        StaticResource logo, signature;

        logo = srList [0];
        signature = srList [1];
        */

        //2-4-22 o.brown update ------------------------------------------start---------------------------------------
        profID = userInfo.getProfileID();        
        
        if (profid != sid.Ohio_Healthy__c) {
            StaticResource[] srList = [SELECT Id,NamespacePrefix,SystemModstamp,Name FROM StaticResource WHERE Name = 'HDP_Corporate_Logo' or name ='medicalDirectorSignature' order by Name asc];
            system.debug('srList  ='+srList);
            logo=srList[0];
            signature=srList[1];
            
        }else{
            StaticResource[] srList = [SELECT Id,NamespacePrefix,SystemModstamp,Name FROM StaticResource WHERE Name = 'OhioHealthy_Logo' order by Name asc];//new update for OHY
            logo=srList[0];
        } 
        //2-4-22 o.brown update ------------------------------------------end---------------------------------------
        
        String prefix = logo.NamespacePrefix;

        if (String.isEmpty(prefix)) {
            prefix = '';
        }
        else{
              //If has NamespacePrefix
            prefix += '__';
        }

        //String logoPath = '/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'HDP_Corporate_Logo'; 2-4-22 o.brownupdated to switch if OHy
        if (profid != sid.Ohio_Healthy__c) {
             logoPath = '/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'HDP_Corporate_Logo';
        }else{
             logoPath = '/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'OhioHealthy_Logo';
        }       

        if (isMedicalDirector) {
            prefix = logo.NamespacePrefix;
            if (String.isEmpty(prefix)) {
                prefix = '';
            }
            else{
                  //If has NamespacePrefix
                prefix += '__';
            }

            medicalSignatureDirectory = '/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'medicalDirectorSignature';
        }

        string letterID = ApexPages.CurrentPage().getParameters().get('letter');
        string clinicianID = ApexPages.CurrentPage().getParameters().get('clinician');
        string letterType = ApexPages.CurrentPage().getParameters().get('lt');

        string[] fieldList = new string[] {};
        fieldList.add('Admission_Date__c');
        fieldList.add('patient__r.Patient_Date_of_Birth__c');

        if (!Test.isRunningTest()) {
            controller.addFields(fieldList);
        }

        Utilization_Management__c um = (Utilization_Management__c)controller.getRecord();

        umRecordId = um.id;
        uml = new Utilization_Management_Denial_Letter__c(Utilization_Management__c = umRecordId);

        loadDiagnosisCodes();
        loadClinicalCodes();
        loadUmClinicians();
        letterOptions = new selectOption[] {};
        letterOptions.add(new selectOption('', '-- Choose a letter --'));
        letterOptions.add(new selectOption('Denial - Benefit Exclusion', 'Denial - Benefit Exclusion'));
        letterOptions.add(new selectOption('Denial - Benefit Exclusion_ERISA', 'Denial - Benefit Exclusion_ERISA'));
        letterOptions.add(new selectOption('Denial - Benefit Exclusion_NON-ERISA', 'Denial - Benefit Exclusion_NON-ERISA'));
        letterOptions.add(new selectOption('Denial - Medical Necessity', 'Denial - Medical Necessity'));
        letterOptions.add(new selectOption('Denial - Medical Necessity_ERISA', 'Denial - Medical Necessity_ERISA'));
        letterOptions.add(new selectOption('Denial - Medical Necessity_NON-ERISA', 'Denial - Medical Necessity_NON-ERISA'));
          //letterOptions.add(new selectOption('Denial - Network Variance','Denial - Network Variance'));
        letterOptions.add(new selectOption('UHHS_Denial Network Variance', 'UHHS_Denial Network Variance'));//added 6-3-21
        letterOptions.add(new selectOption('Denial - Admin Denial_ERISA','Denial - Admin Denial_ERISA'));//Updated 10-21-21
        letterOptions.add(new selectOption('Denial - Admin Denial_NON-ERISA','Denial - Admin Denial_NON-ERISA'));//added 10-20-21    
        letterOptions.add(new selectOption('Transplant', 'Transplant'));
        //2-7-22 o.brown added new CH letters ---start---------------------------------------------------------------------------------
        letterOptions.add(new selectOption('CH Denial Benefit Exclusion-Empl','CH Denial Benefit Exclusion-Empl')); 
        letterOptions.add(new selectOption('CH Denial Benefit Exclusion-Non-Empl','CH Denial Benefit Exclusion-Non-Empl'));
        letterOptions.add(new selectOption('CH Denial Medical Necessity-Empl','CH Denial Medical Necessity-Empl'));  
        letterOptions.add(new selectOption('CH Denial Medical Necessity-Non-Empl','CH Denial Medical Necessity-Non-Empl'));
        //2-7-22 o.brown added new CH letters ---end-----------------------------------------------------------------------------------          
        //2-4-22 o.brown added OHy UM letters---------------------------start-----------------------------------------------------------
        letterOptions.add(new selectOption('OHY Denial Medical Necessity-Empl','OHY Denial Medical Necessity-Empl'));        
        letterOptions.add(new selectOption('OHY Denial Medical Necessity-Non-Empl','OHY Denial Medical Necessity-Non-Empl'));
        letterOptions.add(new selectOption('OHY Denial Benefit Exclusion-Empl','OHY Denial Benefit Exclusion-Empl')); 
        letterOptions.add(new selectOption('OHY Denial Benefit Exclusion-Non-Empl','OHY Denial Benefit Exclusion-Non-Empl'));  
        //2-4-22 o.brown added OHy UM letters---------------------------end-----------------------------------------------------------      

        if (clinicianID != null) {
            setClinician(clinicianID);
        }
        else{
            clinician = new Utilization_Management_Clinician__c();
        }

        if (letterID != null) {
            try{
                letterID = letterID.escapehtml3();
                id.valueof(letterID);
                uml = [select LetterText__c, Salutation__c, Utilization_Management_Clinician__r.First_Name__c, Utilization_Management_Clinician__r.Last_Name__c,
                       Utilization_Management_Clinician__r.Street__c,
                       Utilization_Management_Clinician__r.City__c,
                       Utilization_Management_Clinician__r.State__c,
                       Utilization_Management_Clinician__r.Zip_Code__c,
                       utilization_Management_Clinician__r.Salutation__c,
                       utilization_Management_Clinician__r.Credentials__c,
                       Utilization_Management__r.Patient__r.Patient_First_Name__c,
                       Utilization_Management__r.Patient__r.Patient_Last_Name__c,
                       Utilization_Management__r.Patient__r.Subscriber_First_Name__c,
                       Utilization_Management__r.Patient__r.Subscriber_Last_Name__c,
                       Utilization_Management__r.Patient__r.Patient_Date_of_Birth__c,
                       Utilization_Management__r.Patient__r.Gender__c,
                       Utilization_Management__r.patient__r.Address__c,
                       Utilization_Management__r.patient__r.City__c,
                       Utilization_Management__r.patient__r.State__c,
                       Utilization_Management__r.patient__r.Zip__c,
                       Utilization_Management__r.HealthPac_Case_Number__c,
                       Utilization_Management__r.Facility_Name__c,
                       Utilization_Management__r.Facility_Zip_Code__c,
                       Utilization_Management__r.Facility_City__c,
                       Utilization_Management__r.Facility_State__c,
                       Utilization_Management__r.Admission_Date__c,
                       Utilization_Management__r.Facility_Street__c,
                       Utilization_Management__r.Patient__r.Patient_Employer__r.name, Provider_Name__c, Provider_Street__c, Provider_City__c, Provider_State__c, Provider_Zip_Code__c, Network_Variance_Provider_Clinician_Name__c, Network_Variance_Provider_Clinician__c, Description__c, Utilization_Management_Clinician__c, Admission_of__c, Clinical_Rationale__c, Denial_Reason__c, HealthCare_Benefits_Plan_Document__c, Medical_Benefit_Plan_Covers__c, Section_Title__c, Specific_Plan_Exclusions__c,
                       Exclusion_language_quote__c, Type_of_Letter__c, Services_Denied__c, Type_of_Request__c, Traditional_and_Consumer_Select_Medical__c, signed_by__c, signed_date__c from Utilization_Management_Denial_Letter__c where id =:letterID];

                // setClincian();
            }catch (exception e) {
                system.debug(e.getMessage() + ' ' + e.getlinenumber());
            }
        }
        else{
            uml = new Utilization_Management_Denial_Letter__c(Utilization_Management__c = umRecordId);
        }
        system.debug('letterType ' + letterType);
        if (uml.lettertext__c == null) {
            if (uml.Type_of_Letter__c == null) {
                uml.Type_of_Letter__c = letterType;
            }

            if (uml.Utilization_Management_Clinician__c == null) {
                uml.Utilization_Management_Clinician__c = clinicianID;
            }
            else{
                setClinician(uml.Utilization_Management_Clinician__c);
            }

        //2-4-22 o.brown update ----------------------------start--------------------------------
        //string letterText = '<div><img src="' + logoPath + '" style="width:290px;margin-left:-5px"/></div><br/>'; o.browm updated to display OHy logo on their letters
        string letterText;// = '<div><img src="' + logoPath + '" style="width:290px;margin-left:-5px"/></div><br/>';

        if(uml.Type_of_Letter__c=='OHY Denial Medical Necessity-Non-Empl'||uml.Type_of_Letter__c=='OHY Denial Medical Necessity-Empl'||uml.Type_of_Letter__c=='OHY Denial Benefit Exclusion-Non-Empl'||uml.Type_of_Letter__c=='OHY Denial Benefit Exclusion-Empl'){
           logoPath = '/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'OhioHealthy_Logo';
           letterText='<div><img src="'+logoPath+'" style="width:290px;margin-left:-5px"/></div><br/>';          
        }else{
              letterText = '<div><img src="' + logoPath + '" style="width:290px;margin-left:-5px"/></div><br/>'; 
        }
        //2-4-22 o.brown update ----------------------------end--------------------------------
        letterText += '<div style="font-size: 15px;">';
        letterText += '<div style="text-align:left;"><br/>';
        letterText += date.today().format();
        letterText += '</div><br/>';

            switch on letterType {
                when 'Denial - Network Variance' {//2-14-22 o.brown added to add test code coverage on letter no longer in use
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                      letterText+=umDenialNetworkVariance.letterText(um, uml, clinician, logoPath);//4-29-21 updated to use updated version
                    letterText += umDenialNetworkVariance_UH.letterText(um, uml, clinician, logoPath);
                    uml.letterText__c = letterText;
                }
                when 'Denial - Network Variance_UH' {//2-14-22 o.brown added to add test code coverage on letter no longer in use
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += umDenialNetworkVariance_UH.letterText(um, uml, clinician, logoPath);
                    uml.letterText__c = letterText;
                }                
                when 'UHHS_Denial Network Variance' {
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += UHHS_DenialNetworkVariance.letterText(um, uml, clinician, logoPath);
                    uml.letterText__c = letterText;
                }
                when 'Denial - Benefit Exclusion' {
                    uml.section_title__c = '“[section title]”';
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += umDenialBenefitExclusion.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes, logoPath);
                    uml.letterText__c = letterText;
                }
                when 'Denial - Benefit Exclusion_ERISA' {
                    uml.section_title__c = '“[section title]”';
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += umDenialBenefitExclusion_ERISA.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes, logoPath);
                    uml.letterText__c = letterText;
                }
                when 'Denial - Benefit Exclusion_NON-ERISA' {
                    uml.section_title__c = '“[section title]”';
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += umDenialBenefitExclusion_nonERISA.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes, logoPath);
                    uml.letterText__c = letterText;
                }

                when 'Denial - Medical Necessity' {
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += umDenialMedicalNecessity.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c = letterText;
                }

                when 'Denial - Medical Necessity_ERISA' {
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += umDenialMedicalNecessity_ERISA.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c = letterText;
                }
                when 'Denial - Medical Necessity_NON-ERISA' {
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += umDenialMedicalNecessity_nonERISA.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c = letterText;
                }

                 //when 'Denial - Admin Denial'{updated letter name as requested 9-27-21
                when 'Denial - Admin Denial_ERISA'{ 
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    uml.Admission_of__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += umDenialMedicalDocumentation.letterText(um, uml, clinician);
                    uml.letterText__c = letterText;
                }

                //9-27-21 added new letter o.brown
                when 'Denial - Admin Denial_NON-ERISA'{
                    uml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                    uml.Admission_of__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText+=umDenialMedicalDocumentation_nonERISA.letterText(um, uml, clinician);  
                    uml.letterText__c=letterText;
                }

                when 'Transplant' {
                    uml.Type_of_Request__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    uml.Description__c = 'XxXxXxXxXxXxXxXxXxXxX';
                    letterText += umTransplantLetter.letterText(um, uml, clinician);
                    uml.letterText__c = letterText;
                }
                
                //2-8-22 new UM letters ------------------start-----------------------------------------
                when 'CH Denial Benefit Exclusion-Empl'{
                    uml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                    uml.Description__c='XxXxXxXxXxXxXxXxXxXxX'; 
                    letterText+= umEmployeeDenialBenefitExclusion_CH.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c=letterText;
                }
                when 'CH Denial Benefit Exclusion-Non-Empl'{
                    uml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                    uml.Description__c='XxXxXxXxXxXxXxXxXxXxX'; 
                    letterText+= umNonEmployeeDenialBenefitExclusion_CH.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c=letterText;
                }                      
                when 'CH Denial Medical Necessity-Empl'{
                    uml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                    uml.Description__c='XxXxXxXxXxXxXxXxXxXxX'; 
                    letterText+= umEmployeeDenialMedicalNecessity_CH.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c=letterText;
                }                      
                when 'CH Denial Medical Necessity-Non-Empl'{
                    uml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                    uml.Description__c='XxXxXxXxXxXxXxXxXxXxX'; 
                    letterText+= umNonEmployeeDenialMedicalNecessity_CH.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c=letterText;
                }
                
                //2-4-22 o.brown added OHy letters**********************************************************************************
                when 'OHY Denial Medical Necessity-Non-Empl'{
                    uml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                    uml.Description__c='XxXxXxXxXxXxXxXxXxXxX'; 
                    letterText+=umNonEmployeeDenialMedicalNecessity_OHy.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c=letterText;
                }  
                when 'OHY Denial Medical Necessity-Empl'{
                    uml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                    uml.Description__c='XxXxXxXxXxXxXxXxXxXxX'; 
                    letterText+=umEmployeeDenialMedicalNecessity_OHy.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c=letterText;
                }                                                                
                when 'OHY Denial Benefit Exclusion-Non-Empl'{
                    uml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                    uml.Description__c='XxXxXxXxXxXxXxXxXxXxX'; 
                    letterText+=umNonEmployeeDenialBenefitExclusion_OHy.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c=letterText;
                }                                                                                  
                when 'OHY Denial Benefit Exclusion-Empl'{
                    uml.Type_of_Request__c='XxXxXxXxXxXxXxXxXxXxX';
                    uml.Description__c='XxXxXxXxXxXxXxXxXxXxX'; 
                    letterText+=umEmployeeDenialBenefitExclusion_OHy.letterText(um, uml, clinician, diagnosisCodes, clinicalCodes);
                    uml.letterText__c=letterText;
                }                              
                              
            }
        }

        //}
    }

    void loadDiagnosisCodes(){
        diagnosisCodes = new Utilization_Management_Clinical_Code__c[] {};
        if (umRecordId != null) {
            diagnosisCodes = [select name, Code_Type__c, Description__c, createdDate, CreatedBy.name from Utilization_Management_Clinical_Code__c where Utilization_Management__c = :umRecordId and code_type__c = 'Diagnosis' order by name desc];
        }
    }

    void loadClinicalCodes(){
        clinicalCodes = new Utilization_Management_Clinical_Code__c[] {};
        if (umRecordId != null) {
            clinicalCodes = [select name, Code_Type__c, Description__c, createdDate, CreatedBy.name from Utilization_Management_Clinical_Code__c where Utilization_Management__c = :umRecordId and code_type__c = 'Clinical' order by name desc];
        }
    }

    public void signTheDocument(){
        isError = false;

        //if(uml==null){
        //    uml = new Utilization_Management_Denial_Letter__c(Utilization_Management__c=umRecordId);
        //}

        uml.Signed_Date__c = dateTime.now();
        uml.Signed_By__c = UserInfo.getUserId();
        try{
            upsert uml;
        }catch (dmlexception e) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDMLMessage(0)));
        }
    }

    public void saveLetter(){
        isError = false;
        //if(uml==null){
        //    uml = new Utilization_Management_Denial_Letter__c(Utilization_Management__c=umRecordId);
        //}

        try{
            upsert uml;
        }catch (dmlexception e) {
            isError = true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDMLMessage(0)));
        }
    }

    public void denyCase(){
    }

    public void setLetterText(){
    }

    public void setLetterType(){
    }
    public void setClinician(){
        clinician = clinicianMap.get(uml.Utilization_Management_Clinician__c);
    }
    public void setClinician(string clincianID){
        clinician = clinicianMap.get(clincianID);
    }

    void loadUmClinicians(){
        umClinicians = new selectOption[] {};
        if (umRecordId != null) {
            clinicianMap = new map<id, Utilization_Management_Clinician__c>();
            umClinicians.add(new selectOption('', '-- Choose a clinician --'));
            for (Utilization_Management_Clinician__c um : [select Salutation__c,
                                                           City__c,
                                                           Contact_Name__c,
                                                           Contact_Phone_Number__c,
                                                           Credentials__c,
                                                           Clinician_Fax__c,
                                                           LastModifiedDate,
                                                           LastModifiedBy.Name,
                                                           CreatedBy.Name,
                                                           CreatedDate,
                                                           Name,
                                                           Network_Status__c,
                                                           First_Name__c,
                                                           Last_Name__c,
                                                           State__c,
                                                           Street__c,
                                                           Zip_Code__c from Utilization_Management_Clinician__c where Utilization_Management__c = :umRecordId]) {
                umClinicians.add(new selectOption(um.id, um.First_Name__c + ' ' + um.Last_Name__c));
                clinicianMap.put(um.id, um);
            }
        }
    }
}