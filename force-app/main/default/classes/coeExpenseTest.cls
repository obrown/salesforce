@isTest(seealldata=true)
private class coeExpenseTest{

  private static testMethod void testcoeExpense(){
  
      Patient_Case__c pc = new Patient_Case__c();
        client_facility__c cf = [select id, procedure__c, client__c from client_facility__c where procedure__r.name ='Joint' and client__r.name ='Walmart' limit 1];
        pc.client_name__c= 'Walmart';
        pc.recordtypeid = [select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name = 'Walmart Joint' limit 1].id;
        
        pc.client_facility__c = cf.id;
        pc.ecen_procedure__c = cf.procedure__c;
        pc.client__c= cf.client__c;
        
        pc.Mileage__c = '200';
        pc.Status__c = 'Open';
        pc.BID__c = '123456';
        pc.Language_Spoken__c = 'English';
        pc.Initial_Call_Discussion__c = 'The ICD';
        pc.Relationship_to_the_Insured__c = 'Spouse';
        pc.referral_source__c = 'Company Video';
        pc.Employee_Primary_Health_Plan_ID__c ='123456';
        pc.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        pc.Carrier_Name__c = 'Aetna';
        pc.Carrier_Nurse_Name__c = 'John Smith';
        pc.Carrier_Nurse_Phone__c = '(480) 555-1212';

        
        pc.Employee_first_name__c = 'John';
        pc.Employee_last_name__c = 'Smith';
        pc.employee_dobe__c = '1980-01-01';
        pc.Employee_Gender__c = 'Male';
        pc.employee_ssn__c = '123456789';
        
        pc.Patient_First_Name__c = 'Jane';
        pc.Patient_last_name__c = 'Smith';
        pc.Patient_dobe__c = '1981-01-01';
        pc.Patient_Gender__c = 'Female';
        pc.Patient_ssn__c = '987654321';
        pc.Patient_Email_Address__c = 'something@hdplus.com';
        
        pc.Same_as_Employee_Address__c = true;
        
        pc.employee_street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85297';
        
        pc.Employee_Home_Phone__c = '(480) 555-1212';
        pc.Employee_Mobile__c = '(480) 555-1212';
        pc.Employee_Work_Phone__c = '(480) 555-1212';
        
        pc.Employee_Preferred_Phone__c = 'Home';
        
        pc.Patient_Home_Phone__c = '(480) 555-1212';
        pc.Patient_Mobile__c = '(480) 555-1212';
        pc.Patient_Work_Phone__c = '(480) 555-1212';
        
        pc.Patient_Preferred_Phone__c = 'Home';
        pc.Callback_Number__c = '(480) 555-1212';
        
        pc.Caregiver_Home_Phone__c = '(480) 555-1212';
        pc.Caregiver_Mobile__c = '(480) 555-1212';
        
        pc.Caregiver_Name__c = 'John Smith';
        pc.Caregiver_DOBe__c = '1982-01-01';
        
        insert pc;
        system.assert(pc.id!=null);
        
        ApexPages.currentPage().getParameters().put('id',pc.id);
        caseController cc = new caseController();
        
        cc.obj.Provider_Type__c='Family';
        cc.profname='Jane';
        cc.prolname='Smith';
        cc.proemail = 'something@hdplus.com';
        cc.prostreet = '123 E Main St';
        cc.procity = 'Mesa';
        cc.prozip = '85212';
        
        cc.AddProvider();
        system.assert(cc.Providers.size()>0);
        
        Provider__c delPro = cc.Providers[0].clone();
        insert delPro;
        ApexPages.currentPage().getParameters().put('theProId',cc.Providers[0].id);
        cc.editPro();
        ApexPages.currentPage().getParameters().put('theProId',null);
        cc.DeletePro();
        ApexPages.currentPage().getParameters().put('theProId',delPro.id);
        cc.DeletePro();
        cc.closePro();
        
        cc.obj.Expedited_Referral__c = 'Yes';
        cc.obj.Medicare_As_Secondary_Coverage__c = 'No';
        cc.obj.Other_Pertinent_Medical_Info_History__c = 'Test';
        cc.obj.Patient_Symptoms__c = 'Testing';
        cc.obj.Proposed_Procedure__c = 'Testing';
        cc.obj.Recent_Testing__c = 'Testing';
        //cc.obj.Referred_facility__c = 'John Hopkins';
        cc.obj.Diagnosis__c = 'Testing';
        
        cc.inlineSave();
        
        cc.getcodeOptions();
        cc.getcTypeItems();
        
        cc.obj.Clinical_Code_Status__c = 'Proposed';
        cc.codeType = 'DRG';
        cc.codeList =  [select id from Code__c where type__c = 'DRG' limit 1].id;
        cc.addCC();
        
        system.assert(cc.cc.size()>0);
            
        cc.submitForEligibility();
        cc.obj.Eligible__c = 'Yes';
        cc.obj.Patient_and_Employee_DOB_verified__c = true;
        cc.obj.Patient_and_Employee_SSN_verified__c = true;
        cc.obj.BID__c = '123549';
        cc.obj.BID_Verified__c = true;
        cc.obj.Carrier_and_Plan_Type_verified__c = true;
        
        update cc.obj;
        
        cc.convertLead();
        system.debug(cc.message=='');
        
        ApexPages.currentPage().getParameters().put('Id',cc.obj.id);
        multiReferralFormsController mfc = new multiReferralFormsController();   
        mfc.formSelected();
        cc.obj.isConverted__c = true;
        
        cc.obj.travel_type__c = 'Flying';
        cc.obj.Length_of_Episode__c =3;
        cc.obj.Roundtrip_Mileage_to_the_Airport_Station__c = 15;
        cc.obj.Hotel_Check_in_Date__c = date.today().addDays(-6);
        cc.obj.Hotel_Checkout_Date__c = date.today().addDays(-10);
        cc.obj.hotel_rate__c=100;
        
        update cc.obj;
        
        Test.startTest();
        
        system.assert(cc.obj.isConverted__c);
        
        coeExpenses coe = new coeExpenses();
        set<patient_case__c> pcSet = new set<patient_case__c>();
        pcSet.add(cc.obj);
        coe.dailyStipendHandler(pcSet, 0.00);
        coe.addlDailyStipendHandler(pcSet, 0.00);
        coeExpenses.offsetUpdate(new patient_case__c[]{cc.obj});
        
        cc.obj.travel_type__c = 'Driving Greater than 60 miles';
        update cc.obj;
        
        coe.dailyStipendHandler(pcSet, 0.00);
        coeExpenses.offsetUpdate(new patient_case__c[]{cc.obj});
        
        memberTrips__c  mt = new memberTrips__c(patient_case__c=cc.obj.id);
        insert mt;
        delete mt;
        
        test.stopTest();
  
  
  }
  
}