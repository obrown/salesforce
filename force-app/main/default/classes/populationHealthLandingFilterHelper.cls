public with sharing class populationHealthLandingFilterHelper {
    
    public string field {get; set;}
    public string value {get; set;}
    public string inputset {get; set;}
    string cm;
    SystemID__c sid = SystemID__c.getInstance();//12-02-21 o.brown added
    id profID;//12-02-21 o.brown added
        
    public picklistSelection[] picklistSelection {get; set;}
        
    public class picklistSelection{
       
       public string  label {get; set;}
       public boolean isSelected {get; set;}
       
       public picklistSelection(string label){
           this.label = label;
       }
       
   }
   
   public populationHealthLandingFilterHelper(){
       field= ApexPages.CurrentPage().getParameters().get('field');
       value= ApexPages.CurrentPage().getParameters().get('value');
       inputset= ApexPages.CurrentPage().getParameters().get('inputset');
       cm= ApexPages.CurrentPage().getParameters().get('cm');
       profID = userInfo.getProfileID(); //12-02-21 o.brown added
       
       if(field!=null){
           loadPickListSelection();
       }
   }
   
   void loadPickListSelection(){
   
        picklistSelection = new  picklistSelection[]{};
        Schema.DescribeFieldResult dfr;
        if(field.contains('Patient_Employer__r.')){
            field=field.right(field.length()-(field.indexof('.')+1));
            
            if(field=='name'){
               /*removed 12-02-21
                for(Client__c client : [select underwriter__c, name from Client__c where underwriter__c != '' and active__c=true order by name asc]){
                   picklistSelection.add(new picklistSelection(client.name));
               }
             */
                //12-02-21 added build new list client list for OHY
                if (profid != sid.Ohio_Healthy__c) {
                for(Client__c client : [select underwriter__c, name from Client__c where underwriter__c != '' and active__c=true order by name asc]){//12-29-21 o.brown added sort
                   picklistSelection.add(new picklistSelection(client.name));
               }
               }else{
                for(Client__c client : [select underwriter__c, name from Client__c where underwriter__c != '' and active__c=true and OHY_Client__c =true order by name asc]){
                   picklistSelection.add(new picklistSelection(client.name));
               }               
               }           
            }else{
                Map<String, Schema.SObjectField> patientFM = Schema.SObjectType.phm_patient__c.fields.getMap();
                dfr = patientFM.get(field).getdescribe();
                // filterFieldType=string.valueof(dfr.getType());
                List<Schema.PicklistEntry> ple = dfr.getPicklistValues();
                for( Schema.PicklistEntry f : ple){
                    // fieldValues.add(new SelectOption(f.getLabel(), f.getValue()));
                    picklistSelection.add(new picklistSelection(f.getLabel()));
                }
            }
        }else if(field=='messages'||field=='Needs_Additional_Clinical__c'||field=='Priority__c'||field=='faxesforreview'){
            picklistSelection = new  picklistSelection[]{};
            picklistSelection.add(new picklistSelection('true'));
            picklistSelection.add(new picklistSelection('false'));
        
        
        }else if(field.contains('RecordType.name')){
            picklistSelection = new  picklistSelection[]{};
            picklistSelection.add(new picklistSelection('Inpatient'));
            picklistSelection.add(new picklistSelection('Outpatient'));
            
        }else if(field=='Case_Manager__c'||field=='Case_Sub_status__c'||field=='Cost_Containment__c'||field=='Cost_Driver__c'||field=='Engagement__c'||field=='Expected_Cost__c'||field=='HCC__c'||field=='MDC__c'||field=='MDC_Sub_Category__c'||field=='Referral_Source__c' ||field=='Case_Status__c'&&cm=='cmrecord'){
        Map<String, Schema.SObjectField> cmFM = Schema.SObjectType.Case_Management__c.fields.getMap();
        dfr = cmFM.get(field).getdescribe();
        List<Schema.PicklistEntry> ple = dfr.getPicklistValues();
            for( Schema.PicklistEntry f : ple){
                // fieldValues.add(new SelectOption(f.getLabel(), f.getValue()));
                picklistSelection.add(new picklistSelection(f.getLabel()));
            }        
        
        }else{
            Map<String, Schema.SObjectField> umFM = Schema.SObjectType.utilization_management__c.fields.getMap();
            dfr = umFM.get(field).getdescribe();
            // filterFieldType=string.valueof(dfr.getType());
            List<Schema.PicklistEntry> ple = dfr.getPicklistValues();
            for( Schema.PicklistEntry f : ple){
                // fieldValues.add(new SelectOption(f.getLabel(), f.getValue()));
                picklistSelection.add(new picklistSelection(f.getLabel()));
            }
        }
        
   }

}