public with sharing class directBillingTermInvoiceStruct{

    public Direct_billing_leave__c[]   leaves {get; private set;}
    public Direct_Billing_Invoice__c[] invoices {get; private set;}
    
    public directBillingTermInvoiceStruct(Direct_Billing_Invoice__c[] invoices, Direct_billing_leave__c[] leaves){
        this.leaves = leaves ;
        this.invoices = invoices;
    }
    
}