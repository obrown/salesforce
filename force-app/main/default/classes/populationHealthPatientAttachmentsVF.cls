public with sharing class populationHealthPatientAttachmentsVF {

    public String pid { get; set; }
    public String newWindow { get; set; }
    
    public populationHealthPatientAttachmentsVF (){
        string ppId = ApexPages.CurrentPage().getParameters().get('patientRecordId');
        //newWindow = ApexPages.CurrentPage().getParameters().get('newWindow'); 
        
        pId = ppId.escapeHtml3();
    }
}