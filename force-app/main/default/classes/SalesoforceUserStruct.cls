public class SalesoforceUserStruct {

    public class Photos {
        public String picture {get;set;} 
        public String thumbnail {get;set;} 

        public Photos(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'picture') {
                            picture = parser.getText();
                        } else if (text == 'thumbnail') {
                            thumbnail = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Photos consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Status {
        public Object created_date {get;set;} 
        public Object body {get;set;} 

        public Status(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'created_date') {
                            created_date = parser.readValueAs(Object.class);
                        } else if (text == 'body') {
                            body = parser.readValueAs(Object.class);
                        } else {
                            System.debug(LoggingLevel.WARN, 'Status consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public String id {get;set;} 
    public Boolean asserted_user {get;set;} 
    public String user_id {get;set;} 
    public String organization_id {get;set;} 
    public String username {get;set;} 
    public String nick_name {get;set;} 
    public String display_name {get;set;} 
    public String email {get;set;} 
    public Boolean email_verified {get;set;} 
    public String first_name {get;set;} 
    public String last_name {get;set;} 
    public String timezone {get;set;} 
    public Photos photos {get;set;} 
    public Object addr_street {get;set;} 
    public Object addr_city {get;set;} 
    public Object addr_state {get;set;} 
    public Object addr_country {get;set;} 
    public Object addr_zip {get;set;} 
    public Object mobile_phone {get;set;} 
    public Boolean mobile_phone_verified {get;set;} 
    public Boolean is_lightning_login_user {get;set;} 
    public Status status {get;set;} 
    public Urls urls {get;set;} 
    public Boolean active {get;set;} 
    public String user_type {get;set;} 
    public String language {get;set;} 
    public String locale {get;set;} 
    public Integer utcOffset {get;set;} 
    public String last_modified_date {get;set;} 

    public SalesoforceUserStruct(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'id') {
                        id = parser.getText();
                    } else if (text == 'asserted_user') {
                        asserted_user = parser.getBooleanValue();
                    } else if (text == 'user_id') {
                        user_id = parser.getText();
                    } else if (text == 'organization_id') {
                        organization_id = parser.getText();
                    } else if (text == 'username') {
                        username = parser.getText();
                    } else if (text == 'nick_name') {
                        nick_name = parser.getText();
                    } else if (text == 'display_name') {
                        display_name = parser.getText();
                    } else if (text == 'email') {
                        email = parser.getText();
                    } else if (text == 'email_verified') {
                        email_verified = parser.getBooleanValue();
                    } else if (text == 'first_name') {
                        first_name = parser.getText();
                    } else if (text == 'last_name') {
                        last_name = parser.getText();
                    } else if (text == 'timezone') {
                        timezone = parser.getText();
                    } else if (text == 'photos') {
                        photos = new Photos(parser);
                    } else if (text == 'addr_street') {
                        addr_street = parser.readValueAs(Object.class);
                    } else if (text == 'addr_city') {
                        addr_city = parser.readValueAs(Object.class);
                    } else if (text == 'addr_state') {
                        addr_state = parser.readValueAs(Object.class);
                    } else if (text == 'addr_country') {
                        addr_country = parser.readValueAs(Object.class);
                    } else if (text == 'addr_zip') {
                        addr_zip = parser.readValueAs(Object.class);
                    } else if (text == 'mobile_phone') {
                        mobile_phone = parser.readValueAs(Object.class);
                    } else if (text == 'mobile_phone_verified') {
                        mobile_phone_verified = parser.getBooleanValue();
                    } else if (text == 'is_lightning_login_user') {
                        is_lightning_login_user = parser.getBooleanValue();
                    } else if (text == 'status') {
                        status = new Status(parser);
                    } else if (text == 'urls') {
                        urls = new Urls(parser);
                    } else if (text == 'active') {
                        active = parser.getBooleanValue();
                    } else if (text == 'user_type') {
                        user_type = parser.getText();
                    } else if (text == 'language') {
                        language = parser.getText();
                    } else if (text == 'locale') {
                        locale = parser.getText();
                    } else if (text == 'utcOffset') {
                        utcOffset = parser.getIntegerValue();
                    } else if (text == 'last_modified_date') {
                        last_modified_date = parser.getText();
                    } else {
                        System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Urls {
        public String enterprise {get;set;} 
        public String metadata {get;set;} 
        public String partner {get;set;} 
        public String rest {get;set;} 
        public String sobjects {get;set;} 
        public String search_Z {get;set;} // in json: search
        public String query {get;set;} 
        public String recent {get;set;} 
        public String tooling_soap {get;set;} 
        public String tooling_rest {get;set;} 
        public String profile {get;set;} 
        public String feeds {get;set;} 
        public String groups {get;set;} 
        public String users {get;set;} 
        public String feed_items {get;set;} 
        public String feed_elements {get;set;} 
        public String custom_domain {get;set;} 

        public Urls(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'enterprise') {
                            enterprise = parser.getText();
                        } else if (text == 'metadata') {
                            metadata = parser.getText();
                        } else if (text == 'partner') {
                            partner = parser.getText();
                        } else if (text == 'rest') {
                            rest = parser.getText();
                        } else if (text == 'sobjects') {
                            sobjects = parser.getText();
                        } else if (text == 'search') {
                            search_Z = parser.getText();
                        } else if (text == 'query') {
                            query = parser.getText();
                        } else if (text == 'recent') {
                            recent = parser.getText();
                        } else if (text == 'tooling_soap') {
                            tooling_soap = parser.getText();
                        } else if (text == 'tooling_rest') {
                            tooling_rest = parser.getText();
                        } else if (text == 'profile') {
                            profile = parser.getText();
                        } else if (text == 'feeds') {
                            feeds = parser.getText();
                        } else if (text == 'groups') {
                            groups = parser.getText();
                        } else if (text == 'users') {
                            users = parser.getText();
                        } else if (text == 'feed_items') {
                            feed_items = parser.getText();
                        } else if (text == 'feed_elements') {
                            feed_elements = parser.getText();
                        } else if (text == 'custom_domain') {
                            custom_domain = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Urls consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static SalesoforceUserStruct parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new SalesoforceUserStruct(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }
    
}