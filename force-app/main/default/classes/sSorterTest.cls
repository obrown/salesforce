@isTest
private class sSorterTest {

    static testMethod void UnitTest() {
        
        rxAudit__c[] rxList = new rxAudit__c[]{};
        
        rxList.add(new rxAudit__c(name='Unit.test'));
        rxList.add(new rxAudit__c(name='Unit.test'));
        rxList.add(new rxAudit__c(name='Unit.test'));
        rxList.add(new rxAudit__c(name='Unit.test'));
        rxList.add(new rxAudit__c(name='Unit.test'));
        rxList.add(new rxAudit__c(name='Unit.test'));
        rxList.add(new rxAudit__c(name='Unit.test'));
        rxList.add(new rxAudit__c(name='Unit.test'));
        
        insert rxList;
        
        sSorter.sortByField(rxList, 'Name');
        sSorter.sortByField(rxList, 'CreatedDate');
        
        sSorter.reverse(rxList);
        
    }

}