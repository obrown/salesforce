@isTest
private class customerCareTest{

    static testMethod void customerCareTest() {
        
         /* begin setup */
        
        cciPicklistSetupController cpsc = new cciPicklistSetupController();
        cpsc.setInquiryReasonToUpdate();
        
        cpsc.workTaskName= 'Unit Test Work Task';
        cpsc.saveWorkTask();
        cpsc.workTaskId = cpsc.isl.workTask[1].getValue();
        system.assert(cpsc.workTaskId!=null);
        cpsc.isl.workTaskValue =cpsc.workTaskId;
        string workTaskId = cpsc.workTaskId;
        
        ApexPages.CurrentPage().getParameters().put('workTaskId',cpsc.workTaskId);
        cpsc.InquiryReasonName= 'Unit Test Inquiry Reason';
        cpsc.saveInquiryReason();
        cpsc.InquiryReasonId = cpsc.isl.InquiryReason[1].getValue();
        system.assert(cpsc.InquiryReasonId !=null);
        cpsc.isl.inquiryReasonValue=cpsc.inquiryReasonId ;
        string inquiryReasonId = cpsc.inquiryReasonId;
        
        ApexPages.CurrentPage().getParameters().put('inquiryReasonId',cpsc.inquiryReasonId);
        cpsc.workDepartmentName= 'Unit Test Work Department';
        cpsc.saveWorkDepartment();
        cpsc.workDepartmentId = cpsc.isl.workDepartment[1].getValue();
        system.assert(cpsc.workDepartmentId !=null);
        cpsc.isl.workDepartmentValue=cpsc.workDepartmentId;
        string workDepartmentId = cpsc.workDepartmentId;
        
        cpsc.closedReasonName= 'Unit Test Closed Reason';
        cpsc.saveClosedReason();
        cpsc.closedReasonId = cpsc.isl.closedReason[1].getValue();
        system.assert(cpsc.closedReasonId!=''); 
        string closedReasonId= cpsc.closedReasonId;
        system.debug('closedReasonId '+closedReasonId);
        /* End Setup */
        
        Client__c  cac = new Client__c (underwriter__c='008',name='Cooper');
        insert cac;
        
        // Setup test data
        // Create a unique UserName
        String uniqueUserName = 'standarduser' + DateTime.now().getTime() + '@testorg.com';
        // This code runs as the system user
        Profile p = [SELECT Id FROM Profile WHERE Name='Customer Care'];
        User u = new User(Alias = 'standt', Email='standarduser@testorg.com',
        EmailEncodingKey='UTF-8', LastName='Testing', LanguageLocaleKey='en_US',
        LocaleSidKey='en_US', ProfileId = p.Id,
        TimeZoneSidKey='America/Los_Angeles',
         UserName=uniqueUserName);
        customer_care__c cc = new customer_care__c();
        ApexPages.StandardController controller = new ApexPages.StandardController(cc);
        
        customerCareController ccController  = new customerCareController(controller);
        System.runAs(u) {
              // The following code runs as user 'u'
              System.debug('Current User: ' + UserInfo.getUserName());
              System.debug('Current Profile: ' + UserInfo.getProfileId());
         
        
        

        
        cc.Caller_Name__c ='UNIT TEST';
        cc.Caller_Phone_Number__c = '(480) 555-1212';
        
        insert cc;
        
        }
         
        controller = new ApexPages.StandardController(cc);
        ccController = new customerCareController(controller);
        system.debug(ccController.theiD);
        
        ApexPages.currentPage().getParameters().put('uw', '008');
        ApexPages.currentPage().getParameters().put('essn', '555746607');
        ccController.searchHealthpac();
        
        system.assert(ccController.resultsMap.keySet().contains('555746607'));
        
        ApexPages.currentPage().getParameters().put('puw', '008');
        ApexPages.currentPage().getParameters().put('pEssn', '555746607');
        ApexPages.currentPage().getParameters().put('pSeq', '00');
        ApexPages.currentPage().getParameters().put('pgrp','OR1');
        ApexPages.currentPage().getParameters().put('pefn',''); 
        ApexPages.currentPage().getParameters().put('peln',''); 
        
        ccController.selectEmployee();
        system.assert(ccController.ccdList.size()>0);
        cc.eessn__c = '555746607';
        cc.ESSN__c = '6607';
        
        System.runAs(u) {
            update cc;
        }
        
        system.assert(cc.eessn__c == '555746607');
        
        string ccname = cc.name;
        
        ApexPages.currentPage().getParameters().put('patientName', 'Michael');
        ApexPages.currentPage().getParameters().put('Relationship', 'Employee');
        
        
        system.debug('ccController.cci '+ccController.cci);
        
        ccController.cci.SA_Inquiry_Reason__c = inquiryReasonId;
        ccController.cci.SA_Inquiry_Work_Department__c = workDepartmentId;
        ccController.cci.SA_Inquiry_Work_Task__c= workTaskId ;
        
        ccController.saveCCI();
        system.assert(!ccController.cciList.isEmpty());
        
        customer_care_inquiry__c cciFoo = ccController.cci.clone();
        insert cciFoo;
        
        ApexPages.currentPage().getParameters().put('cciID', ccController.cciList[0].id);
        
        ccController.getTheCCI();
        
        system.assert(ccController.cci.id == ccController.cciList[0].id);
        
        ccController.newInquiryNote();
        
        ccController.ccwn.notes__c='UNIT.TEST';
        ccController.saveInquiryNote();
        
        system.assert(ccController.ccwnList.size()>0);
        
        ccController.cancelInquiryNote();
        
        ApexPages.currentPage().getParameters().put('cciID', ccController.cci.id);
        ccController.deleteTheCCI();
        ccController.cancelCCI();
        
        ccController.newCCD();
        
        ApexPages.currentPage().getParameters().put('ccdEdob','1980-01-05');
        ApexPages.currentPage().getParameters().put('ccdPdob','1982-01-05');
        ApexPages.currentPage().getParameters().put('ccdClient','008');
        ApexPages.currentPage().getParameters().put('ccdRel','Spouse');
        ApexPages.currentPage().getParameters().put('ccdEssn','999999999');
        ApexPages.currentPage().getParameters().put('ccdPfn','Jane');
        ApexPages.currentPage().getParameters().put('ccdPln','Smith');
        ApexPages.currentPage().getParameters().put('ccdEfn','John');
        ApexPages.currentPage().getParameters().put('ccdEln','Smith');
        
        ccController.saveCCD();
        system.assert(ccController.ccdList.size()>0);
        
        ApexPages.currentPage().getParameters().put('pEssn','555746607');
        ApexPages.currentPage().getParameters().put('pSeq','00');
        ApexPages.currentPage().getParameters().put('pgrp','OR1');
        ApexPages.currentPage().getParameters().put('puw','008'); 
               
        ccController.selectEmployee();
        
        ApexPages.currentPage().getParameters().put('patientName', 'Jane');
        ApexPages.currentPage().getParameters().put('Relationship', 'Spouse');
        ApexPages.currentPage().getParameters().put('Reason', 'Appeal');
        
        ApexPages.currentPage().getParameters().put('inquiryStatus', 'Open');
        ApexPages.currentPage().getParameters().put('PriorityFlag', 'false');
        ApexPages.currentPage().getParameters().put('SecondRequest', 'false');
        
        ApexPages.currentPage().getParameters().put('WorkDepartment', 'UNIT.TEST');
        ApexPages.currentPage().getParameters().put('InquiryWorkTask', 'UNIT.TEST');
        ApexPages.currentPage().getParameters().put('cciID', '');
        ApexPages.currentPage().getParameters().put('InquiryNote', '216-150312-00');
        
        ccController.saveCCI();
        
        /* Attachment */
         
        ApexPages.CurrentPage().getParameters().put('fileName','Unit.Test');
        ccController.uploader();
         
        system.assert(ccController.attachments.size()==1);
         
        
        
        
        ApexPages.CurrentPage().getParameters().put('recordName','Unit.Test');
        
         
        ApexPages.CurrentPage().getParameters().put('attachmentID',ccController.attachments[0].id);
        ccController.deleteAttachment();
         
        /* End Attachment */
        
        //Additional Code Coverage
        
        ccController.resetSearch();
        ccController.cancelCCD();
        
        ApexPages.currentPage().getParameters().put('ccid', cciFoo.id);
        ccController = new customerCareController(controller);
        ccname = [select name from customer_care__c where id = :cc.id].name;
        
        ApexPages.StandardController cciController = new ApexPages.StandardController(cciFoo);
        cciRedirectExt cciRedirect = new cciRedirectExt(cciController);
        cciRedirect.fwdUser();
        
        intakeSearch si = new intakeSearch('555746607', 'All', null, null);
        
        si = new intakeSearch(ccname.substringAfter('-'), 'intakeNumber', null, null);
        si.preSetDateSelection='1Y';
        si.setDates();
        ApexPages.CurrentPage().getParameters().put('searchfield','intakeNumber');
        ApexPages.CurrentPage().getParameters().put('newst',ccname.substringAfter('-'));
        si.newSearch();
        si.preSetDateSelection='1';
        si.setDates();
        si.preSetDateSelection='7';
        si.setDates();
        si.preSetDateSelection='14';
        si.setDates();
        si.preSetDateSelection='30';
        si.setDates();
        si.preSetDateSelection='6M';
        si.setDates();
        //si = new intakeSearch('555746607', 'eSSN', null, null);
        si = new intakeSearch('Smith', 'lastName', null, null);
        si = new intakeSearch('555746607', null, null, null);
        si = new intakeSearch('Smith', null, null, null);
        si = new intakeSearch(ccname.substringAfter('-'), null, null, null);
        si = new intakeSearch('216-150312-00', 'claims', null, null);
        si = new intakeSearch('216-150312-00', 'All', null, null);
        
        
        CustomerCare_MemberHistoryController viewHistory = new CustomerCare_MemberHistoryController();
        viewHistory.searchByIntake(cc.eessn__c, cc.name);
        new CustomerCare_MemberHistoryController.inquiriesWrapper(cc);
       
    }
}