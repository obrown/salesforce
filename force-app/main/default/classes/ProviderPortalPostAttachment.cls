@RestResource(urlMapping='/providerPortal/v1/post_attachment')
global with sharing class ProviderPortalPostAttachment {
    global class Filelist {
        File[] Filelist;
        string Message;
        string ResultCode;
        
    }
    class File {
        string FileName;
        string LastModified;
        public File(string fileName, string LastModified){
            this.fileName =fileName;
            this.LastModified =LastModified;
        }
    }
    @HttpPost
    global static Filelist doPost() {
        RestRequest req = RestContext.request;
        string subDirectory = RestContext.request.headers.get('path');
        string filename = RestContext.request.headers.get('filename');
        string caseNumber = RestContext.request.headers.get('caseNumber');
        id case_id;
        Filelist Filelist = new Filelist();
        Filelist.ResultCode='2';
        try{
        
            case_id = [select id from patient_case__c where name = :caseNumber].id;
        }catch(exception e){
            system.debug(e.getMessage());
            return FileList;
        }
        
        system.debug('subDirectory: '+subDirectory);
        system.debug('filename: '+filename);
        
        /* 
            Param1: Blob of file
            Param2: filname eg: string.valueof(inv.invoice_month__c.month())+string.valueof(inv.invoice_month__c.year())+'.pdf'
            Param3: directory where the file is stored: eg Direct Billing/First Name Last Name/leave Number/invoice_month and year
            param4: fileServer__c.getInstance().ussscorpion__c+'savefile/'
        */ 
        
        //HttpResponse res = multipartform.uploadFile(req.requestBody,filename , subDirectory , fileServer__c.getInstance().ussscorpion__c+'savefile/');
        //system.debug(res.getBody());
        
        attachment attachment = new attachment();
        attachment.name = filename;
        attachment.parentId= case_id;
        attachment.body = req.requestBody;
        insert attachment;
        
        /*
        http h = new http();
        HttpRequest r = new HttpRequest ();
        
        
        r.setEndPoint(fileServer__c.getInstance().ussscorpion__c+'savefilestream/');
        r.setHeader('Content-Type', 'application/json');
        r.setHeader('path', subDirectory);
        r.setHeader('filename', filename);
        r.setBody( EncodingUtil.base64Encode(req.requestBody));
        r.setmethod('POST');
        r.setTimeout(120000);
        HttpResponse res = h.send(r); 
        system.debug(res.getBody());
        */
        
        //Filelist = (Filelist)JSON.deserialize(res.getBody(),Filelist.class);
        system.debug(Filelist );
        
        //if(Filelist.ResultCode == '2'){
        //    return Filelist;
        //}
        
        //coeDocumentStorage ds = new coeDocumentStorage();
        //ds.saveDocument(case_id , subDirectory , filename , 'Uploaded via the Provider Portal', true);
        ftpAttachment__c ftpAttachment = new ftpAttachment__c(patient_case__c=case_id);
        ftpAttachment.subDirectory__c= subDirectory;
        ftpAttachment.fileName__c= fileName;
        ftpAttachment.file_Description__c= 'Uploaded via the Provider Portal';
        ftpAttachment.external__c= true;
        ftpAttachment.attachmentID__c=attachment.id;
        insert ftpAttachment;
        
        File[] Files = new File[]{};
        
        for(ftpAttachment__c  a : [Select  fileName__c,LastModifiedDate from ftpAttachment__c where patient_case__c != null and patient_case__r.name = :caseNumber order by createdDate desc]){
            Files.add(new File(a.fileName__c,''));//string.valueof(a.LastModifiedDate)
        }
        
        
        Filelist.Filelist = Files;
        Filelist.Message= 'Success';
        Filelist.ResultCode = '0';
        
        return Filelist;
    }
    
    
}