public with sharing class directBillingMemberVFcontroller {
    
    string none = '--None--';
    string leaveidText = 'leaveId';
    string waive = 'Waive';
    
    public Direct_Billing_Member__c member = new Direct_Billing_Member__c();
    
    public Direct_Billing_Leave__c leave {get;set;}
    public Direct_Billing_Leave__c[] leaveList {get; private set;}
    
    public DB_Note__c   leaveNote {get; set;}
    public DB_Note__c[] leaveNoteList {get; set;}
    map<id, DB_Note__c> leaveNoteMap = new map<id, DB_Note__c>();
    
    public ftpAttachment__c[] invoiceAttachments {get; set;}
    
    public Direct_Billing_Invoice__c    leaveInvoice {get; set;}
    public Direct_Billing_Invoice__c[]  leaveInvoiceList {get; set;}
    
    map<id, Direct_Billing_Invoice__c>  leaveInvoiceMap = new map<id, Direct_Billing_Invoice__c>();
    
    public selectOption[] medicalTiers {get; set;}
    public selectOption[] dentalTiers {get; set;}
    public selectOption[] medicalParmaTiers {get; set;}
    public selectOption[] identityTheftTiers {get; set;}
    public selectOption[] legalPlanTiers {get; set;}
    public selectOption[] indemnityPlanTiers {get; set;}
    
    public selectOption[] indemnityPlanElections {get; set;}
    public selectOption[] legalPlanElections {get; set;}
    public selectOption[] identityTheftElections {get; set;}
    public selectOption[] visionElection {get; set;}
    public selectOption[] medicalElections {get; set;}
    public selectOption[] medicalParmaElections {get; set;}
    public selectOption[] personalAccidentElections {get; set;}
    public selectOption[] dependentLifeElections {get; set;}
    public selectOption[] spouseLifeElections {get; set;}
    public selectOption[] dentalElections {get; set;}
    
    public selectOption[] biWeekly {get; private set;}
    public selectOption[] semiMonthly {get; private set;}
    public selectOption[] monthly {get; private set;}
    public selectOption[] payEnd {get; private set;}
    
    public string startDate {get; set;}
    
    map<string, map<string, Decimal>> rateMap;
    map<string, set<string>> planMap;
    
    string recordID='';
    public string leaveId {get; private set;}
    public boolean isError {get; private set;}
    
    public Direct_Billing_Member__c getMember(){
        return this.member;
    }
    
    public void setMember(Direct_Billing_Member__c member){
        this.member = member;
    }
    
    void init(){
        if(recordID != null){
        
            loadMemberRecord();
            loadLeaveList();
            
            if(leaveId == null){
                if(leaveList!=null && !leaveList.isEmpty()){
                    loadLeave(leaveList[0].id);
                    
                }
                    
            }else{
                loadLeave(leaveidText);
            
            }
            
        }
    }
    
    public directBillingMemberVFcontroller(ApexPages.StandardController controller) {
        recordID = ApexPages.currentpage().getParameters().get('id');
        if(recordID != null){
            try{
                if(string.valueof(id.valueof(recordID).getSObjectType())=='direct_billing_Leave__c'){
                    leaveId = recordID;
                    recordID = [select DB_Member__c from direct_billing_Leave__c where id = :recordID].DB_Member__c;    
                }
            }catch(exception e){
                recordId=null;
            }
        }
        
        init();
    }
    
    public directBillingMemberVFcontroller(){
        
        recordID = ApexPages.currentpage().getParameters().get('id');
        if(recordID != null){
            try{
                if(string.valueof(id.valueof(recordID).getSObjectType())=='direct_billing_Leave__c'){
                    leaveId = recordID;
                    recordID = [select DB_Member__c from direct_billing_Leave__c where id = :recordID].DB_Member__c;    
                }
            }catch(exception e){
                recordId=null;
            }
        }
        
        init();
        
    }
    
    void loadMemberRecord(){
        member = [select Address_1__c,
                         Address_2__c,
                         City__c,
                         Comments__c,
                         CreatedDate,
                         CreatedBy.Name,
                         Department_Code__c,
                         EE_ssn__c,
                         Employee_Name__c,
                         Name,
                         State__c,
                         Zip_Code__c from Direct_Billing_Member__c where id =:recordID];
        
    }
    
    public void deleteLeaveVF(){
        deleteLeave(ApexPages.CurrentPage().getParameters().put(leaveidText, null));
        ApexPages.CurrentPage().getParameters().put(leaveidText, null);
        
    }
    
    void deleteLeave(string leaveId){
        for(Direct_Billing_Leave__c dbl :leaveList){
            if(dbl.id == leaveid){
                leave=dbl;
                break;
            }
        }    
        delete leave;
        leave=null;
        loadLeaveList();
        
    }
    
    public void newLeave(){
        
        leave = new Direct_Billing_Leave__c(DB_Member__c=member.id);
        
        if(leaveList.size()>0){
            leave.pay_frequency__c = leaveList[0].pay_frequency__c;
        }
        
        loadDbRates();
        loadPayEndPeriods();
    }
    
    public void loadLeaveVF(){
        loadLeave(ApexPages.CurrentPage().getParameters().get(leaveidText));
        ApexPages.CurrentPage().getParameters().put(leaveidText, null);
    }
    
    void loadLeave(string leaveId){
        
        if(leaveId==null){
            leave = new Direct_Billing_Leave__c(DB_Member__c=member.id);
        }
        
        for(Direct_Billing_Leave__c dbl :leaveList){
            if(dbl.id == leaveid){
                leave=dbl;
                startDate = string.valueof(leave.start_date__c);
                
                break;
            }
        }    
        
        loadLeaveNotes();
        loadLeaveInvoices();
        loadDbRates();
        loadpayEndPeriods();
        setPaySchedule(leave.pay_frequency__c);
    }
    
    void loadLeaveList(){
        
        leaveList = [select createdBy.name,
                            createdDate,
                            Critical_Illness_Amount_Per_Pay__c,
                            Critical_Illness_Election__c,
                            Current_Balance__c,
                            lastModifiedDate,
                            DB_Member__c,
                            Dental_Election__c,
                            Dental_Premium__c,
                            Dental_Tier__c,
                            Dependent_Life_Election__c,
                            Dependent_Life_Premium__c,
                            Dependent_Life_Tier__c,
                            End_Date__c,
                            End_Invoice_Override__c,
                            Invoice_Count__c,
                            Identity_Theft_Election__c,
                            Identity_Theft_Premium__c,
                            Identity_Theft_Tier__c,
                            Indemnity_Plan_Election__c,
                            Indemnity_Plan_Premium__c,
                            Indemnity_Plan_Tier__c,
                            FSA_Amount_Per_Pay__c,
                            FSA_Election__c,
                            FSA_Premium__c,
                            Last_Due_Date__c,
                            Last_Invoiced_Amount__c,
                            Last_Invoiced_Date__c,
                            Legal_Plan_Election__c,
                            Legal_Plan_Premium__c,
                            Legal_Plan_Tier__c,
                            Life_and_AD_D_Election__c,
                            Life_and_AD_D_Premium__c,
                            Long_Term_Disability_Election__c,
                            Long_Term_Disability_Premium__c,
                            Medical_Election__c,
                            Medical_Premium__c,
                            Medical_Tier__c,
                            Medical_Parma_Election__c,
                            Medical_Parma_Tier__c,
                            Medical_Parma_Premium__c,
                            name,
                            Outstanding_Balance__c,
                            Pay_Frequency__c,
                            Personal_Accident_Amount_Per_Pay__c,
                            Personal_Accident_Election__c,
                            Personal_Accident_Tier__c,
                            Premiums_Total__c,
                            Reason__c,
                            Spouse_Life_Election__c,
                            Spouse_Life_Tier__c,
                            Spouse_Life_Premium__c, 
                            Start_Date__c,
                            Vision_Election__c,
                            Vision_Premium__c,
                            Vision_Tier__c,
                            Voluntary__c from Direct_Billing_Leave__c where DB_Member__c = :member.id order by End_Date__c desc];
        
    }
    
    public void cancelLeave(){
        if(isError==true){
            loadLeaveList();
        }
        isError=false;
        leave=null;
    }
    
    void reloadLeave(){
        if(leave==null){return;}
        for(Direct_Billing_Leave__c l: leaveList){
                
            if(leave.id==l.id){
                leave=l;
            }
        }
       
    }
    
    public void saveLeave(){
        isError=false;
        
        try{
            if(startDate != null){
                try{
                    leave.start_date__c   = date.valueof(startDate);    
                }catch(exception e){system.debug(e.getMessage());}
            }
            //12-11-20 set the value of the new employee name field to the value of the employee name from the member per Greg D so it can be used in the dbleave list view
            If(member.Employee_Name__c!=null){
            leave.Employee_Name__c=member.Employee_Name__c;
            
            }            
            upsert leave;
            loadLeaveList();
            reloadLeave();
            
            
        }catch(dmlexception e){
            isError=true;
           
                 for (Integer i = 0; i < e.getNumDml(); i++) {                 
                 // Process exception here                 
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));                 
                 System.debug(e.getDmlMessage(i)+' '+e.getLineNumber());              
                 } 
            
        }
        
    }
    
    /* Pay Schedule */
    
    void loadpayEndPeriods(){
        if(biWeekly !=null){
            return;
        }
        
        biWeekly = new selectOption[]{new selectOption('', none)};
        semiMonthly = new selectOption[]{new selectOption('', none)};
        monthly = new selectOption[]{new selectOption('', none)};
        
        //1-5-20 added to exclude dates from prior years
        date sDate = date.valueof(date.today().addYears(-1)+'-01-01');
        
        for(DB_Pay_Schedule__c db : [select pay_end__c, name, recordtype.name from DB_Pay_Schedule__c where pay_end__c > :sDate order by pay_end__c asc ]){
            string endDatePlusOne = dateFormat(db.pay_end__c.addDays(1));
            
            if(db.recordtype.name=='Bi-Weekly'){
                biWeekly.add(new selectOption(string.valueof(db.pay_end__c.addDays(+1)), endDatePlusOne));
            
            }else if(db.recordtype.name=='Semi-Monthly'){
                semiMonthly.add(new selectOption(string.valueof(db.pay_end__c.addDays(+1)), endDatePlusOne));
                
            }else if(db.recordtype.name=='Monthly'){
                monthly.add(new selectOption(string.valueof(db.pay_end__c), db.name));
            }
        }
    
    }
    
    string dateFormat(date d){
        
        integer x = d.month();
        integer y = d.day();
        
        string m;
        string day;
        
        if(x<10){
            m = '0'+ string.valueof(x);    
        
        }else{
            m = string.valueof(x);
        }
        
        if(y<10){
            day = '0'+ string.valueof(y);    
        
        }else{
            day = string.valueof(y);
        }
        
        m += '/'+ day +'/'+d.year();
        
        return m;
    }
    
    public void setPayScheduleVF(){
        string type = ApexPages.CurrentPage().getParameters().get('payType');
        setPaySchedule(type);
    }
    
    void setPaySchedule(string type){
        payEnd = null;
        leave.Pay_frequency__c = type;
        if(type=='Bi-Weekly'){
            payEnd = biWeekly;
        
        }else if(type=='Semi-Monthly'){
            payEnd = semiMonthly;    
        
        }else if(type=='Monthly'){
            payEnd = Monthly;    
        }
    }
    
    /* End Pay Schedule */
    
    /* Notes */
    
    public void loadLeaveNotes(){
        
        if(leave.id==null){
            leaveNoteList = new DB_Note__c[]{};
            return;
            
        }
        
        leaveNoteList = [select Note__c, createdBy.Name, createdDate from DB_Note__c where DB_Leave__c = :leave.id order by createdDate desc];
        for(DB_Note__c note :leaveNoteList){
            leaveNoteMap.put(note.id, note);
            
        }
    }
    
    public void newLeaveNote(){
        
        if(leave.id==null){
            return;
        }
        
        leaveNote = new DB_Note__c(db_leave__c=leave.id);
    }
    
    
    public void cancelLeaveNote(){
        leaveNote = null;
        
    }
    
    public void saveLeaveNote(){
        
        try{
            upsert leaveNote;
            loadLeaveNotes();
        }catch(exception e){
             for (Integer i = 0; i < e.getNumDml(); i++) {                 
             // Process exception here                 
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));                 
             System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* ');              
             } 
        }       
    
    }
    
    
    /* End Notes */
    
    public void saveMember(){
        isError=false;
        
        try{
            upsert member;
        
        }catch(exception e){
            isError=true;
             for (Integer i = 0; i < e.getNumDml(); i++) {                 
             // Process exception here                 
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));                 
             System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* ');              
             } 
            
        }
    }
    
    /* Rates picklists */
    
    public void refreshDbRates(){
        string sd = ApexPages.CurrentPage().getParameters().get('startDate');
        
        if(sd == ''){
            leave.start_date__c=null;
            loadDbRates();
            return;
        }
        
        if(sd!=null){
            leave.start_date__c = date.valueof(sd);
            loadDbRates();
        }
        
        
    }
    
    void loadDbRates(){
        
        medicalElections = new selectOption[]{new selectOption('', none)};
        medicalTiers = new selectOption[]{new selectOption('', none)};
        
        dentalTiers = new selectOption[]{new selectOption('', none)};
        dentalElections  = new selectOption[]{new selectOption('', none)};
        
        medicalParmaElections = new selectOption[]{new selectOption('', none)};
        medicalParmaTiers = new selectOption[]{new selectOption('', none)};
        
        spouseLifeElections = new selectOption[]{new selectOption('', none)};
        dependentLifeElections = new selectOption[]{new selectOption('', none)};
        personalAccidentElections = new selectOption[]{new selectOption('', none)};
        visionElection = new selectOption[]{new selectOption('', none)};
        
        identityTheftTiers = new selectOption[]{new selectOption('', none)};
        identityTheftElections = new selectOption[]{new selectOption('', none)};

        legalPlanTiers = new selectOption[]{new selectOption('', none)};
        legalPlanElections = new selectOption[]{new selectOption('', none)};
        
        indemnityPlanTiers = new selectOption[]{new selectOption('', none)};
        indemnityPlanElections = new selectOption[]{new selectOption('', none)};        
        
        map<string, set<string>> tierMapSet = new map<string, set<string>>();
        set<string> tierSet = new set<string>();
        
        rateMap = new map<string, map<string, Decimal>>();
        planMap = new map<string, set<string>>();
        
        for(DB_Rate_Tier__c rate : [select Name,
                                           Db_Rates__r.Start__c,
                                           Db_Rates__r.End__c,
                                           Db_Rates__r.RecordType.Name,
                                           Db_Rates__r.Name,
                                           Rate__c from DB_Rate_Tier__c where Db_Rates__r.Client_Benefit_Year__r.Client_Name__c='University Hospitals' order by Db_Rates__r.Name asc, rate__c asc]){
           
            if(leave.Start_Date__c >= rate.Db_Rates__r.Start__c.addMonths(-2) && leave.Start_Date__c <= rate.Db_Rates__r.End__c.addMonths(12)){
            
                tierSet = tierMapSet.get(rate.Db_Rates__r.RecordType.Name);
                if(tierSet==null){
                    tierSet = new set<string>();
                
                }
                
                tierSet.add(rate.Name);
                tierMapSet.put(rate.Db_Rates__r.RecordType.Name, tierSet);
            
                popRateMap(rate.Db_Rates__r.name, rate.Name, rate.Rate__c);         
                popPlanMap(rate.Db_Rates__r.RecordType.Name, rate.Db_Rates__r.name);
            
            }
        
        }
        
        for(string s : tierMapSet.keySet()){
            
            switch on s{
            
            when 'Medical'{
                for(string x : tierMapSet.get(s)){
                    if(x!=waive){
                        medicalTiers.add(new selectOption(x, x));
                    }
                }
            }
            
            when 'Medical - Parma'{
                for(string x : tierMapSet.get(s)){
                    if(x!=waive){
                        medicalParmaTiers.add(new selectOption(x, x));
                    }
                }
                
            }
            
            when 'Dental'{
                for(string x : tierMapSet.get(s)){
                    if(x!=waive){
                        dentalTiers.add(new selectOption(x, x));
                    }
                }
                
            }
            
            when 'Vision'{
                for(string x : tierMapSet.get(s)){
                    visionElection.add(new selectOption(x, x));
                    
                }
                
            }
            
            when 'Dependent Life'{
                for(string x : tierMapSet.get(s)){
                    dependentLifeElections.add(new selectOption(x, x));
                }
                
            }
            
            when 'Spouse Life'{
                for(string x : tierMapSet.get(s)){
                    spouseLifeElections.add(new selectOption(x, x));
                }
           
            }
            
            when 'Voluntary Personal Accident'{
                for(string x : tierMapSet.get(s)){
                    personalAccidentElections.add(new selectOption(x, x));
                }                
            }

            when 'Identity Theft'{
                for(string x : tierMapSet.get(s)){
                    identityTheftTiers.add(new selectOption(x, x));
                }                
            }
            when 'Legal Plan'{
                for(string x : tierMapSet.get(s)){
                    legalPlanTiers.add(new selectOption(x, x));
                }                
            }
            when 'Hospital Indemnity'{
                for(string x : tierMapSet.get(s)){
                    indemnityPlanTiers.add(new selectOption(x, x));
                }                
            }                        
            }
            
        }
        
        for(string s : planMap.keyset()){

            switch on s{
            
                when 'Medical'{
                    for(string x : planMap.get(s)){
                        medicalElections.add(new selectOption(x, x));
                    }
                }
            
                when 'Medical - Parma'{
                    for(string x : planMap.get(s)){
                        medicalParmaElections.add(new selectOption(x, x));
                    }
                }
            
                when 'Dental'{
                    for(string x : planMap.get(s)){
                        dentalElections.add(new selectOption(x, x));
                    } 
                         
                }
                when 'Identity Theft'{
                    for(string x : planMap.get(s)){
                        identityTheftElections.add(new selectOption(x, x));
                    } 
                         
                }
                when 'Hospital Indemnity'{
                    for(string x : planMap.get(s)){
                        indemnityPlanElections.add(new selectOption(x, x));
                    } 
                         
                } 
                when 'Legal Plan'{
                    for(string x : planMap.get(s)){
                        legalPlanElections.add(new selectOption(x, x));
                    } 
                         
                }                                             
            }
        }
        
    }
    
    void popPlanMap(string recordType, string planName){
        
        set<string> plans = planMap.get(recordType);
        
        if(plans==null){
            plans = new set<string>{};
        }
        
        plans.add(planName);
        planMap.put(recordType, plans);
    }
    
    void popRateMap(string planName, string tier, decimal rate){
        
        map<string, Decimal> foo = rateMap.get(planName);
        
        if(foo==null){
            foo = new map<string, Decimal>();
        }
        
        foo.put(tier, rate);
        rateMap.put(planName, foo); 
    }
    
    public void rateChange(){
        string planRecordType = ApexPages.CurrentPage().getParameters().get('planRecordType');
        string planName = ApexPages.CurrentPage().getParameters().get('planName');
        string planTier = ApexPages.CurrentPage().getParameters().get('planTier');
        
        switch on planName{
            when 'spouseLife'{
                planName='Spouse Life';
            }
            when 'dependentLife'{
                planName='Dependent Life';
            }
            when 'personalAccident'{
                planName='Personal Accident';
            }
        }
        
        map<string, decimal> foo = rateMap.get(planName);
        switch on planRecordType{
        
        when 'medical'{
            if(foo==null|| planName==waive){leave.Medical_Premium__c=0.00;leave.Medical_Tier__c=null;return;}
            leave.Medical_Premium__c = foo.get(planTier); 
        
        }
        
        when 'medicalParma'{
            if(planName==waive||foo==null){leave.Medical_Parma_Premium__c=0.00;leave.Medical_Parma_Tier__c=null;return;}
            leave.Medical_Parma_Premium__c= foo.get(planTier); 
        
        }
        
        when 'dental'{
            if(planName==waive||foo==null){leave.Dental_Premium__c=0.00;leave.Dental_Tier__c=null;return;}
            leave.Dental_Premium__c= foo.get(planTier); 
        
        }
        
        when 'Vision'{
            if(foo==null){leave.Vision_Premium__c =null;return;}
            leave.Vision_Premium__c = foo.get(planTier); 
            
        }
        
        when 'spouseLife'{
            if(foo==null){leave.Spouse_Life_Premium__c =null;return;}
            leave.Spouse_Life_Premium__c = foo.get(planTier); 
        
        }
        
        when 'dependentLife'{
            if(foo==null){leave.Dependent_Life_Premium__c =null;return;}
            leave.Dependent_Life_Premium__c = foo.get(planTier); 
        
        }
        when 'personalAccident'{
            if(foo==null){leave.Personal_Accident_Amount_Per_Pay__c=0.00;return;}
            leave.Personal_Accident_Amount_Per_Pay__c = foo.get(planTier); 

        }
        when 'identityTheft'{
            if(foo==null){leave.Identity_Theft_Premium__c=0.00;return;}
            leave.Identity_Theft_Premium__c= foo.get(planTier); 

        } 
        when 'legalPlan'{
            if(foo==null){leave.Legal_Plan_Premium__c=0.00;return;}
            leave.Legal_Plan_Premium__c= foo.get(planTier); 

        } 
        when 'indemnityPlan'{
            if(foo==null){leave.Indemnity_Plan_Premium__c=0.00;return;}
            leave.Indemnity_Plan_Premium__c= foo.get(planTier); 

        }                        
        }
        
        
    }
    
    /* End Rates picklists */
    
    /* Invoice */
    
    public void saveLeaveInvoice(){
        
        try{
            upsert leaveInvoice;
            loadLeaveInvoices();
            loadLeaveInvoice(leaveInvoice.id);
            loadleaveList();
            reloadLeave();
            
        }catch(exception e){
             for (Integer i = 0; i < e.getNumDml(); i++) {                 
             // Process exception here                 
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));                 
             System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* ');              
             } 
        }
    }
    
    public void loadLeaveInvoiceVF(){
        loadLeaveInvoice(ApexPages.CurrentPage().getParameters().get('leaveInvoiceId'));
        ApexPages.CurrentPage().getParameters().put('leaveInvoiceId', null);
    }
    
    void loadLeaveInvoice(string leaveInvoiceId){
        leaveInvoice = leaveInvoiceMap.get(leaveInvoiceId);
        loadInvoiceAttachments();
    }
    
    public void cancelLeaveInvoice(){
        
        leaveInvoice=null;
    }
    
    void loadLeaveInvoices(){
        
        if(leave.id==null){
            leaveInvoiceList = new Direct_Billing_Invoice__c[]{};   
            return;
        }
        
        leaveInvoiceList = [select CreatedBy.Name,
                                   createdDate, 
                                   Check_Number__c,
                                   Check_Number_2__c,
                                   Balance_Due__c,
                                   Days_since_Due__c,
                                   DB_Leave__c,
                                   Due_Date__c,
                                   Invoice_Amount__c,
                                   Invoice_month__c,
                                   Invoice_Date__c,
                                   Invoice_charges__c,
                                   Name,
                                   Pay_Dates_included__c,
                                   New_Charges__c,
                                   Payment_Amount__c,
                                   Payment_Amount_2__c,
                                   pastDueLetterNeeded__c,
                                   Physical_Invoice_Created__c,
                                   Payment_Received_Date__c,
                                   Payment_Received_Date_2__c,terminationLetterNeeded__c from Direct_Billing_Invoice__c where DB_Leave__c = :leave.Id order by invoice_month__c desc];
                                   
        leaveInvoiceMap = new map<id, Direct_Billing_Invoice__c>();
        
        for(Direct_Billing_Invoice__c dbi : leaveInvoiceList){
            leaveInvoiceMap.put(dbi.id, dbi);

           
        }    
       
    }
    
    void loadInvoiceAttachments(){
        invoiceAttachments = new ftpAttachment__c[]{};
        for(ftpAttachment__c ftpa : [select name, fileName__c, attachmentID__c from ftpAttachment__c where parentId__c = :leaveInvoice.id]){
            invoiceAttachments.add(ftpa);
            
        }
        
    }
    
    public date customLeaveDate {get;set;}
    
    map<id, date> setLeaveEndDate(Direct_Billing_Leave__c leave){
        map<id, date> foo = new map<id, date>();
        if(leave.end_date__c!=null){
            foo.put(leave.id, leave.end_date__c);
            return foo;
        }else if(leave.End_Invoice_Override__c==null && leave.End_Date__c==null){
            foo.put(leave.id, date.today());
            return foo;
        }else if(leave.End_Invoice_Override__c!=null){
                foo.put(leave.id, leave.End_Invoice_Override__c);
            
        }
        
        return foo;
    }
    
    public void runInvoices(){
        
        integer invoiceCount=0;
        if(leaveInvoicelist!=null){invoiceCount=leaveInvoicelist.size();}
        map<id, date> invoiceMap= setLeaveEndDate(leave);
        
        set<id> inv; 
        
        if(leave.End_Invoice_Override__c==null || leave.end_date__c<leave.End_Invoice_Override__c){
            inv=directBillingInvoiceWorkflow.createInvoices(invoiceMap);
            
        }else if(leave.end_date__c>leave.End_Invoice_Override__c || (leave.end_date__c==null && leave.End_Invoice_Override__c!=null)){
            map<id, date> foo = new map<id, date>();
            foo.put(leave.id, leave.End_Invoice_Override__c);
            inv=directBillingInvoiceWorkflow.createInvoices(foo);
            
        }
        
        loadLeaveInvoices();
        // || leave.Outstanding_Balance__c>0
        if(leaveInvoicelist.size()>1 && leaveInvoicelist[0].pastDueLetterNeeded__c){
            //if(leave.End_Invoice_Override__c==null){
            //    directBillingWorkflow.pastDueLetters(invoiceMap, invoiceMap.get(leave.id));

            //}else{
            //    directBillingWorkflow.pastDueLetters(invoiceMap, leave.End_Invoice_Override__c);

            //}
            directBillingCreateAttachments dbca = new directBillingCreateAttachments();
            dbca.createPastDueLetterBatchCalls(new set<id>(new id[]{leave.id}));
        }
        
        leave.End_Invoice_Override__c=null;
        loadLeaveList();
        reloadLeave();
                
        if(invoiceCount==leaveInvoicelist.size()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No invoices are needed at this time.'));
        }        
        
    }
    
    /* End Invoices */
    public void runTerminationLetter(){

        integer invoiceCount=leaveInvoicelist.size();
        map<id, date> invoiceMap= setLeaveEndDate(leave);
        
        set<id> inv; 
        if(leave.End_Invoice_Override__c==null || leave.end_date__c<leave.End_Invoice_Override__c){
            inv=directBillingInvoiceWorkflow.createInvoices(invoiceMap);
            
        }else if(leave.end_date__c>leave.End_Invoice_Override__c || (leave.end_date__c==null && leave.End_Invoice_Override__c!=null)){
            map<id, date> foo = new map<id, date>();
            foo.put(leave.id, leave.End_Invoice_Override__c);
            inv=directBillingInvoiceWorkflow.createInvoices(foo);
            
        }
        
        if(leave.End_Invoice_Override__c==null){
            directBillingWorkflow.terminationLetters(invoiceMap, invoiceMap.get(leave.id));
        }else{
            directBillingWorkflow.terminationLetters(invoiceMap, leave.End_Invoice_Override__c);
        }
        
        leave.End_Invoice_Override__c=null;
        loadLeaveList();
        loadLeaveInvoices();
        
        reloadLeave();
        
        
        /*
        if(invoiceCount==leaveInvoicelist.size()){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'No invoices are needed at this time.'));
        }
        */
        
    }
}