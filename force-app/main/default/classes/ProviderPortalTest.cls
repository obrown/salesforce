/**
* This class contains unit tests for validating the behavior of the ProviderPortal backend
*/
@isTest()
private class ProviderPortalTest {

    class POC_user{
        string email;
        string firstName;
        string lastName;
        string lastModified;

    }

    class response{
        string error;
        string errorMsg;
        EcenProviderPlanOfCare__c poc;
        ProviderPortalPocSelectOptions.poc_selectoptions selectoptions;
        POC_user user;

    }
    static testMethod void TestPortalAPI_getPOC() {

      coeTestData ctd = new coeTestData();
      id clientID = ctd.createClient(null);
      system.debug('clientID: '+clientID);

      id procedureID = ctd.createProcedure(null);
      system.debug('procedureID: '+procedureID);

      id facilityID = ctd.createFacility(null);
      system.debug('facilityID: '+facilityID);

      id clientFacilityID = ctd.createClientFacility(clientID, facilityID, procedureID);

      ctd.createRelationships();

      EmployeeContacts__c employee = ctd.addEmployee();
      EmployeeContacts__c patient = employee;

      patient_case__c patientCase = new patient_case__c();
      patientCase.client__c= clientId;
      patientCase.Ecen_Procedure__c = procedureID;
      patientCase.Client_Facility__c = clientFacilityID;
      patientCase.employeeID__c= employee.id;
      patientCase.patientID__c= patient.id;

      patientCase.Caregiver_name__c = employee.firstName__c+' '+employee.lastName__c;
      patientCase.Caregiver_Home_Phone__c= employee.HomePhoneNumber__c;
      patientCase.Caregiver_Mobile__c = employee.MobilePhoneNumber__c;
      patientCase.is_a_Caregiver_Available__c='Yes';
      patientCase.Caregiver_DOBe__c=employee.dobe__c;
      patientCase.Caregiver_Relationship__c='Caregiver';

      patientCase.Caller_Name__c = employee.firstName__c+' '+employee.lastName__c;
      patientCase.Caller_relationship_to_the_patient__c = 'Patient';
      patientCase.Callback_Number__c= '(480) 555-1212';
      patientCase.Relationship_to_the_Insured__c ='Patient is Insured';
      patientCase.bid__c = employee.InsuranceID__c;

      patientCase.Employee_First_Name__c = patientCase.Patient_First_Name__c = employee.FirstName__c;
      patientCase.Employee_Last_Name__c = patientCase.Patient_Last_Name__c = employee.LastName__c;
      patientCase.Employee_DOBe__c = patientCase.Patient_DOBe__c = employee.Dobe__c;
      patientCase.Employee_Gender__c = patientCase.Patient_Gender__c = employee.Gender__c;
      patientCase.Employee_SSN__c = patientCase.Patient_SSN__c = employee.SSN__c;

      patientCase.Employee_Street__c = patientCase.Patient_Street__c= employee.StreetAddress__c;
      patientCase.Employee_City__c = patientCase.Patient_City__c = employee.City__c;
      patientCase.Employee_State__c = patientCase.Patient_State__c = employee.State__c;
      patientCase.Employee_Zip_Postal_Code__c = patientCase.Patient_Zip_Postal_Code__c = employee.ZipCode__c;

      patientCase.Employee_Mobile__c = patientCase.Patient_Mobile__c = employee.MobilePhoneNumber__c;

      //Section 2
      patientCase.Total_Joint_Replacement_Recommended__c = 'Yes';
      patientCase.nProposed_Procedure__c = 'Right Knee Replacement';
      patientCase.Joint_Previously_Replaced__c = patientCase.Other_Joint_replaced_in_COE_program__c = 'No';
      patientCase.Employee_Insurance_Primary_Not_HMO__c = 'No';
      patientCase.Surgery_Related_to_Work_Injury__c = 'No';
      patientCase.Related_to_injury_seeking_legal_action__c ='No';

      //Section 3
      patientCase.Physician_specialty_recommend_tjr__c = 'Orthopedic Surgeon';
      patientCase.Joint_Recommendation_last_12_months__c = 'Yes';
      patientCase.Patient_Height_FT__c ='5';
      patientCase.Patient_Height_Inches__c = '10';
      patientCase.Diabetic__c ='No';
      patientCase.Current_Nicotine_User__c ='No';
      patientCase.Medication_for_Pain_Control__c = 'No';
      patientCase.Does_the_patient_use_marijuana__c ='No';
      patientCase.Does_patient_have_a_skin_condition__c ='No';

      //Section 4
      patientCase.jointNumberofTimesFallen__c = 'Never';


      patientCase.isConverted__c = true;
      patientCase.Converted_Date__c = date.today();
      insert patientCase;
      system.debug(patientCase.name);

      string case_name = [select name from patient_case__c where id = :patientCase.id].name;

      Test.startTest();

      RestRequest req = new RestRequest();
      req.addHeader('EcenCase', case_name);

      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/providerPortal/v1/providerportalpoc_pc';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      //req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;

      ProviderPortalPocListener.getplanofcare();

      req = new RestRequest();
      req.requestURI = '/services/apexrest/providerPortal/v1/providerportalpoc_pc';  //Request URL
      req.httpMethod = 'POST';//HTTP Request Type
      req.addHeader('EcenCase', case_name);
      req.addHeader('SubmittingPOC', 'false');
      req.addHeader('DoesPatientQualify', 'true');
      req.requestBody = Blob.valueof('{"pocRequest":{"acceptedDate__c":"Pending","authorizationNumber__c":"Pending","allowedModeOfTransportation__c":"Fly","complicationServiceTrip__c":null,"complicationServiceType__c":null,"createdDate__c":"Pending","firstAppointmentDate__c":"10/17/2019","firstAppointmentTime__c":null,"goodProgramCandidate__c":null,"hospitalAdmissionDate__c":"10/17/2019","hospitalDischargeDate__c":"10/17/2019","hipApproach__c":null,"lastAppointmentDate__c":"10/17/2019","lastAppointmentTime__c":"11:23 AM","medicalDirectorReview__c":null,"nicotineQuitDate__c":null,"nicotineUser__c":"No","patientPreferredModeOfTravel__c":"Fly","patientArrivalDate__c":"10/17/2019","patientReturnHomeDate__c":"10/17/2019","physicianFollowUpDate__c":"10/17/2019","physicianFollowUpTime__c":"11:23 AM","planOfCareType__c":"Initial","program__c":"Joint","proposedAddOnServices__c":"","proposedServiceTrip__c":"Program Surgery - Inpatient","proposedServiceType__c":"Right Knee","surgicalProcedureDate__c":"10/17/2019","surgicalProcedureTime__c":null,"scoliosisDiagnosed__c":"No","servicesRequestedOutsideofContracted__c":null,"status__c":"Draft","submittedDate__c":"Pending","surgeon__c":"a2BQ0000000Kn28MAC","travelNotes__c":null,"verifiedOutcomeOfVisit__c":null,"__typename__c":"PlanOfCareValues","providerPortalGoodCandidate__c":"true"},"user":{"email":"michaelanthonyaz@gmail.com","facility":"ggte","familyName":"Martin","givenName":"Michael"}}');

      RestContext.request = req;
      //RestContext.response= res;

      ProviderPortalPocListener.saveplanofcare();

      req = new RestRequest();
      req.addHeader('EcenCase', case_name);

      //RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/providerPortal/v1/providerportalpoc_pc';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      //req.requestBody = Blob.valueof(JsonMsg);
      RestContext.request = req;
      RestContext.response= res;

      ProviderPortalPocListener.getplanofcare();

      Test.stopTest();

  }

  static testMethod void TestPortalAPI_savePOC() {

      coeTestData ctd = new coeTestData();
      id clientID = ctd.createClient(null);
      system.debug('clientID: '+clientID);

      id procedureID = ctd.createProcedure(null);
      system.debug('procedureID: '+procedureID);

      id facilityID = ctd.createFacility(null);
      system.debug('facilityID: '+facilityID);

      id clientFacilityID = ctd.createClientFacility(clientID, facilityID, procedureID);

      ctd.createRelationships();

      EmployeeContacts__c employee = ctd.addEmployee();
      EmployeeContacts__c patient = employee;

      patient_case__c patientCase = new patient_case__c();
      patientCase.client__c= clientId;
      patientCase.Ecen_Procedure__c = procedureID;
      patientCase.Client_Facility__c = clientFacilityID;
      patientCase.employeeID__c= employee.id;
      patientCase.patientID__c= patient.id;

      patientCase.Caregiver_name__c = employee.firstName__c+' '+employee.lastName__c;
      patientCase.Caregiver_Home_Phone__c= employee.HomePhoneNumber__c;
      patientCase.Caregiver_Mobile__c = employee.MobilePhoneNumber__c;
      patientCase.is_a_Caregiver_Available__c='Yes';
      patientCase.Caregiver_DOBe__c=employee.dobe__c;
      patientCase.Caregiver_Relationship__c='Caregiver';

      patientCase.Caller_Name__c = employee.firstName__c+' '+employee.lastName__c;
      patientCase.Caller_relationship_to_the_patient__c = 'Patient';
      patientCase.Callback_Number__c= '(480) 555-1212';
      patientCase.Relationship_to_the_Insured__c ='Patient is Insured';
      patientCase.bid__c = employee.InsuranceID__c;

      patientCase.Employee_First_Name__c = patientCase.Patient_First_Name__c = employee.FirstName__c;
      patientCase.Employee_Last_Name__c = patientCase.Patient_Last_Name__c = employee.LastName__c;
      patientCase.Employee_DOBe__c = patientCase.Patient_DOBe__c = employee.Dobe__c;
      patientCase.Employee_Gender__c = patientCase.Patient_Gender__c = employee.Gender__c;
      patientCase.Employee_SSN__c = patientCase.Patient_SSN__c = employee.SSN__c;

      patientCase.Employee_Street__c = patientCase.Patient_Street__c= employee.StreetAddress__c;
      patientCase.Employee_City__c = patientCase.Patient_City__c = employee.City__c;
      patientCase.Employee_State__c = patientCase.Patient_State__c = employee.State__c;
      patientCase.Employee_Zip_Postal_Code__c = patientCase.Patient_Zip_Postal_Code__c = employee.ZipCode__c;

      patientCase.Employee_Mobile__c = patientCase.Patient_Mobile__c = employee.MobilePhoneNumber__c;

      //Section 2
      patientCase.Total_Joint_Replacement_Recommended__c = 'Yes';
      patientCase.nProposed_Procedure__c = 'Right Knee Replacement';
      patientCase.Joint_Previously_Replaced__c = patientCase.Other_Joint_replaced_in_COE_program__c = 'No';
      patientCase.Employee_Insurance_Primary_Not_HMO__c = 'No';
      patientCase.Surgery_Related_to_Work_Injury__c = 'No';
      patientCase.Related_to_injury_seeking_legal_action__c ='No';

      //Section 3
      patientCase.Physician_specialty_recommend_tjr__c = 'Orthopedic Surgeon';
      patientCase.Joint_Recommendation_last_12_months__c = 'Yes';
      patientCase.Patient_Height_FT__c ='5';
      patientCase.Patient_Height_Inches__c = '10';
      patientCase.Diabetic__c ='No';
      patientCase.Current_Nicotine_User__c ='No';
      patientCase.Medication_for_Pain_Control__c = 'No';
      patientCase.Does_the_patient_use_marijuana__c ='No';
      patientCase.Does_patient_have_a_skin_condition__c ='No';

      //Section 4
      patientCase.jointNumberofTimesFallen__c = 'Never';


      patientCase.isConverted__c = true;
      patientCase.Converted_Date__c = date.today();
      insert patientCase;
      system.debug(patientCase.name);

      string case_name = [select name from patient_case__c where id = :patientCase.id].name;

      Test.startTest();

      RestRequest req = new RestRequest();
      req.addHeader('EcenCase', case_name);
      req.addHeader('SubmittingPOC', 'false');
      req.addHeader('DoesPatientQualify', 'true');

      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/providerPortal/v1/providerportalpoc_pc';  //Request URL
      req.httpMethod = 'POST';//HTTP Request Type
      req.requestBody = Blob.valueof('{"pocRequest":{"acceptedDate__c":"Pending","authorizationNumber__c":"Pending","allowedModeOfTransportation__c":"Fly","complicationServiceTrip__c":null,"complicationServiceType__c":null,"createdDate__c":"Pending","firstAppointmentDate__c":"10/17/2019","firstAppointmentTime__c":null,"goodProgramCandidate__c":null,"hospitalAdmissionDate__c":"10/17/2019","hospitalDischargeDate__c":"10/17/2019","hipApproach__c":null,"lastAppointmentDate__c":"10/17/2019","lastAppointmentTime__c":"11:23 AM","medicalDirectorReview__c":null,"nicotineQuitDate__c":null,"nicotineUser__c":"No","patientPreferredModeOfTravel__c":"Fly","patientArrivalDate__c":"10/17/2019","patientReturnHomeDate__c":"10/17/2019","physicianFollowUpDate__c":"10/17/2019","physicianFollowUpTime__c":"11:23 AM","planOfCareType__c":"Initial","program__c":"Joint","proposedAddOnServices__c":"","proposedServiceTrip__c":"Program Surgery - Inpatient","proposedServiceType__c":"Right Knee","surgicalProcedureDate__c":"10/17/2019","surgicalProcedureTime__c":null,"scoliosisDiagnosed__c":"No","servicesRequestedOutsideofContracted__c":null,"status__c":"Draft","submittedDate__c":"Pending","surgeon__c":"a2BQ0000000Kn28MAC","travelNotes__c":null,"verifiedOutcomeOfVisit__c":null,"__typename__c":"PlanOfCareValues","providerPortalGoodCandidate__c":"true"},"user":{"email":"michaelanthonyaz@gmail.com","facility":"ggte","familyName":"Martin","givenName":"Michael"}}');
      RestContext.request = req;
      RestContext.response= res;

      ProviderPortalPocListener.saveplanofcare();

      req = new RestRequest();
      req.requestURI = '/services/apexrest/providerPortal/v1/providerportalpoc_pc';  //Request URL
      req.httpMethod = 'POST';//HTTP Request Type
      req.addHeader('EcenCase', case_name);
      req.addHeader('SubmittingPOC', 'false');
      req.addHeader('DoesPatientQualify', 'true');
      req.requestBody = Blob.valueof('{"pocRequest":{"acceptedDate__c":"Pending","authorizationNumber__c":"Pending","allowedModeOfTransportation__c":"Fly","complicationServiceTrip__c":null,"complicationServiceType__c":null,"createdDate__c":"Pending","firstAppointmentDate__c":"10/17/2019","firstAppointmentTime__c":null,"goodProgramCandidate__c":null,"hospitalAdmissionDate__c":"10/17/2019","hospitalDischargeDate__c":"10/17/2019","hipApproach__c":null,"lastAppointmentDate__c":"10/17/2019","lastAppointmentTime__c":"11:23 AM","medicalDirectorReview__c":null,"nicotineQuitDate__c":null,"nicotineUser__c":"No","patientPreferredModeOfTravel__c":"Fly","patientArrivalDate__c":"10/17/2019","patientReturnHomeDate__c":"10/17/2019","physicianFollowUpDate__c":"10/17/2019","physicianFollowUpTime__c":"11:23 AM","planOfCareType__c":"Initial","program__c":"Joint","proposedAddOnServices__c":"","proposedServiceTrip__c":"Program Surgery - Inpatient","proposedServiceType__c":"Right Knee","surgicalProcedureDate__c":"10/17/2019","surgicalProcedureTime__c":null,"scoliosisDiagnosed__c":"No","servicesRequestedOutsideofContracted__c":null,"status__c":"Draft","submittedDate__c":"Pending","surgeon__c":"a2BQ0000000Kn28MAC","travelNotes__c":null,"verifiedOutcomeOfVisit__c":null,"__typename__c":"PlanOfCareValues","providerPortalGoodCandidate__c":"true"},"user":{"email":"michaelanthonyaz@gmail.com","facility":"ggte","familyName":"Martin","givenName":"Michael"}}');

      RestContext.request = req;
      //RestContext.response= res;

      ProviderPortalPocListener.saveplanofcare();

      string portal_poc_name = [select name from EcenProviderPlanOfCare__c order by createdDate desc limit 1].name;

      req = new RestRequest();
      req.requestURI = '/services/apexrest/providerPortal/v1/providerportalpoc_pc';  //Request URL
      req.httpMethod = 'POST';//HTTP Request Type
      req.addHeader('EcenCase', case_name);
      req.addHeader('SubmittingPOC', 'true');
      req.addHeader('DoesPatientQualify', 'true');
      req.requestBody = Blob.valueof('{"pocRequest":{"name":"'+ portal_poc_name +'", "acceptedDate__c":"Pending","authorizationNumber__c":"Pending","allowedModeOfTransportation__c":"Fly","complicationServiceTrip__c":null,"complicationServiceType__c":null,"createdDate__c":"Pending","firstAppointmentDate__c":"10/17/2019","firstAppointmentTime__c":null,"goodProgramCandidate__c":null,"hospitalAdmissionDate__c":"10/17/2019","hospitalDischargeDate__c":"10/17/2019","hipApproach__c":null,"lastAppointmentDate__c":"10/17/2019","lastAppointmentTime__c":"11:23 AM","medicalDirectorReview__c":null,"nicotineQuitDate__c":null,"nicotineUser__c":"No","patientPreferredModeOfTravel__c":"Fly","patientArrivalDate__c":"10/17/2019","patientReturnHomeDate__c":"10/17/2019","physicianFollowUpDate__c":"10/17/2019","physicianFollowUpTime__c":"11:23 AM","planOfCareType__c":"Initial","program__c":"Joint","proposedAddOnServices__c":"","proposedServiceTrip__c":"Program Surgery - Inpatient","proposedServiceType__c":"Right Knee","surgicalProcedureDate__c":"10/17/2019","surgicalProcedureTime__c":null,"scoliosisDiagnosed__c":"No","servicesRequestedOutsideofContracted__c":null,"status__c":"Draft","submittedDate__c":"Pending","surgeon__c":"a2BQ0000000Kn28MAC","travelNotes__c":null,"verifiedOutcomeOfVisit__c":null,"__typename__c":"PlanOfCareValues","providerPortalGoodCandidate__c":"true"},"user":{"email":"michaelanthonyaz@gmail.com","facility":"ggte","familyName":"Martin","givenName":"Michael"}}');

      RestContext.request = req;
      ProviderPortalPocListener.saveplanofcare();

      req = new RestRequest();
      req.requestURI = '/services/apexrest/providerPortal/v1/providerportalpoc_pc';  //Request URL
      req.httpMethod = 'POST';//HTTP Request Type
      req.addHeader('EcenCase', case_name);
      req.addHeader('SubmittingPOC', 'true');
      req.addHeader('DoesPatientQualify', 'true');
      req.requestBody = Blob.valueof('{"pocRequest":{"name":"fake", "acceptedDate__c":"Pending","authorizationNumber__c":"Pending","allowedModeOfTransportation__c":"Fly","complicationServiceTrip__c":null,"complicationServiceType__c":null,"createdDate__c":"Pending","firstAppointmentDate__c":"10/17/2019","firstAppointmentTime__c":null,"goodProgramCandidate__c":null,"hospitalAdmissionDate__c":"10/17/2019","hospitalDischargeDate__c":"10/17/2019","hipApproach__c":null,"lastAppointmentDate__c":"10/17/2019","lastAppointmentTime__c":"11:23 AM","medicalDirectorReview__c":null,"nicotineQuitDate__c":null,"nicotineUser__c":"No","patientPreferredModeOfTravel__c":"Fly","patientArrivalDate__c":"10/17/2019","patientReturnHomeDate__c":"10/17/2019","physicianFollowUpDate__c":"10/17/2019","physicianFollowUpTime__c":"11:23 AM","planOfCareType__c":"Initial","program__c":"Joint","proposedAddOnServices__c":"","proposedServiceTrip__c":"Program Surgery - Inpatient","proposedServiceType__c":"Right Knee","surgicalProcedureDate__c":"10/17/2019","surgicalProcedureTime__c":null,"scoliosisDiagnosed__c":"No","servicesRequestedOutsideofContracted__c":null,"status__c":"Draft","submittedDate__c":"Pending","surgeon__c":"a2BQ0000000Kn28MAC","travelNotes__c":null,"verifiedOutcomeOfVisit__c":null,"__typename__c":"PlanOfCareValues","providerPortalGoodCandidate__c":"true"},"user":{"email":"michaelanthonyaz@gmail.com","facility":"ggte","familyName":"Martin","givenName":"Michael"}}');

      RestContext.request = req;
      ProviderPortalPocListener.saveplanofcare();

      req = new RestRequest();
      req.requestURI = '/services/apexrest/providerPortal/v1/providerportalpoc_pc';  //Request URL
      req.httpMethod = 'POST';//HTTP Request Type
      req.addHeader('EcenCase', case_name);
      req.addHeader('SubmittingPOC', 'true');
      req.addHeader('DoesPatientQualify', 'false');
      req.requestBody = Blob.valueof('{"pocRequest":{"name":"'+ portal_poc_name +'", "providerPortalCandidateReason__c":"BMI","providerPortalCandidateNotes__c":"Pending"}}');

      RestContext.request = req;
      ProviderPortalPocListener.saveplanofcare();

      Test.stopTest();

    }

    static testMethod void TestPortalAPI_pocSync() {

      coeTestData ctd = new coeTestData();
      id clientID = ctd.createClient(null);
      system.debug('clientID: '+clientID);

      id procedureID = ctd.createProcedure(null);
      system.debug('procedureID: '+procedureID);

      id facilityID = ctd.createFacility(null);
      system.debug('facilityID: '+facilityID);

      id clientFacilityID = ctd.createClientFacility(clientID, facilityID, procedureID);

      ctd.createRelationships();

      EmployeeContacts__c employee = ctd.addEmployee();
      EmployeeContacts__c patient = employee;

      patient_case__c patientCase = new patient_case__c();
      patientCase.client__c= clientId;
      patientCase.Ecen_Procedure__c = procedureID;
      patientCase.Client_Facility__c = clientFacilityID;
      patientCase.employeeID__c= employee.id;
      patientCase.patientID__c= patient.id;

      patientCase.Caregiver_name__c = employee.firstName__c+' '+employee.lastName__c;
      patientCase.Caregiver_Home_Phone__c= employee.HomePhoneNumber__c;
      patientCase.Caregiver_Mobile__c = employee.MobilePhoneNumber__c;
      patientCase.is_a_Caregiver_Available__c='Yes';
      patientCase.Caregiver_DOBe__c=employee.dobe__c;
      patientCase.Caregiver_Relationship__c='Caregiver';

      patientCase.Caller_Name__c = employee.firstName__c+' '+employee.lastName__c;
      patientCase.Caller_relationship_to_the_patient__c = 'Patient';
      patientCase.Callback_Number__c= '(480) 555-1212';
      patientCase.Relationship_to_the_Insured__c ='Patient is Insured';
      patientCase.bid__c = employee.InsuranceID__c;

      patientCase.Employee_First_Name__c = patientCase.Patient_First_Name__c = employee.FirstName__c;
      patientCase.Employee_Last_Name__c = patientCase.Patient_Last_Name__c = employee.LastName__c;
      patientCase.Employee_DOBe__c = patientCase.Patient_DOBe__c = employee.Dobe__c;
      patientCase.Employee_Gender__c = patientCase.Patient_Gender__c = employee.Gender__c;
      patientCase.Employee_SSN__c = patientCase.Patient_SSN__c = employee.SSN__c;

      patientCase.Employee_Street__c = patientCase.Patient_Street__c= employee.StreetAddress__c;
      patientCase.Employee_City__c = patientCase.Patient_City__c = employee.City__c;
      patientCase.Employee_State__c = patientCase.Patient_State__c = employee.State__c;
      patientCase.Employee_Zip_Postal_Code__c = patientCase.Patient_Zip_Postal_Code__c = employee.ZipCode__c;

      patientCase.Employee_Mobile__c = patientCase.Patient_Mobile__c = employee.MobilePhoneNumber__c;

      //Section 2
      patientCase.Total_Joint_Replacement_Recommended__c = 'Yes';
      patientCase.nProposed_Procedure__c = 'Right Knee Replacement';
      patientCase.Joint_Previously_Replaced__c = patientCase.Other_Joint_replaced_in_COE_program__c = 'No';
      patientCase.Employee_Insurance_Primary_Not_HMO__c = 'No';
      patientCase.Surgery_Related_to_Work_Injury__c = 'No';
      patientCase.Related_to_injury_seeking_legal_action__c ='No';

      //Section 3
      patientCase.Physician_specialty_recommend_tjr__c = 'Orthopedic Surgeon';
      patientCase.Joint_Recommendation_last_12_months__c = 'Yes';
      patientCase.Patient_Height_FT__c ='5';
      patientCase.Patient_Height_Inches__c = '10';
      patientCase.Diabetic__c ='No';
      patientCase.Current_Nicotine_User__c ='No';
      patientCase.Medication_for_Pain_Control__c = 'No';
      patientCase.Does_the_patient_use_marijuana__c ='No';
      patientCase.Does_patient_have_a_skin_condition__c ='No';

      //Section 4
      patientCase.jointNumberofTimesFallen__c = 'Never';


      patientCase.isConverted__c = true;
      patientCase.Converted_Date__c = date.today();
      insert patientCase;

      Test.startTest();

      ProviderPortalPOCSync sync = new ProviderPortalPOCSync(patientCase.id);
      pc_poc__c pc_poc = new pc_poc__c(patient_case__c=patientCase.id);
      insert pc_poc;
      sync.Salesforce_to_Portal(pc_poc);

      Test.stopTest();
    }

    static testMethod void TestPortalAPI_getSelectOptions() {

      Test.startTest();

      ProviderPortalPocSelectOptions ppSelectOptions = new ProviderPortalPocSelectOptions();
      ppSelectOptions.getSelectOptions('Spine', 'Johns Hopkins');
      ppSelectOptions.getSelectOptions('Joint', 'Johns Hopkins');

      Test.stopTest();
    }

    /* ****************************** TASK TESTING ****************************** */

    static testMethod void TestPortalAPI_saveTask() {

      coeTestData ctd = new coeTestData();
      id clientID = ctd.createClient(null);
      system.debug('clientID: '+clientID);

      id procedureID = ctd.createProcedure('Spine');
      system.debug('procedureID: '+procedureID);

      id facilityID = ctd.createFacility(null);
      system.debug('facilityID: '+facilityID);

      id clientFacilityID = ctd.createClientFacility(clientID, facilityID, procedureID);

      ctd.createRelationships();

      EmployeeContacts__c employee = ctd.addEmployee();
      EmployeeContacts__c patient = employee;

      patient_case__c patientCase = new patient_case__c();
      patientCase.client__c= clientId;
      patientCase.Ecen_Procedure__c = procedureID;
      patientCase.Client_Facility__c = clientFacilityID;
      patientCase.employeeID__c= employee.id;
      patientCase.patientID__c= patient.id;

      patientCase.Caregiver_name__c = employee.firstName__c+' '+employee.lastName__c;
      patientCase.Caregiver_Home_Phone__c= employee.HomePhoneNumber__c;
      patientCase.Caregiver_Mobile__c = employee.MobilePhoneNumber__c;
      patientCase.is_a_Caregiver_Available__c='Yes';
      patientCase.Caregiver_DOBe__c=employee.dobe__c;
      patientCase.Caregiver_Relationship__c='Caregiver';

      patientCase.Caller_Name__c = employee.firstName__c+' '+employee.lastName__c;
      patientCase.Caller_relationship_to_the_patient__c = 'Patient';
      patientCase.Callback_Number__c= '(480) 555-1212';
      patientCase.Relationship_to_the_Insured__c ='Patient is Insured';
      patientCase.bid__c = employee.InsuranceID__c;

      patientCase.Employee_First_Name__c = patientCase.Patient_First_Name__c = employee.FirstName__c;
      patientCase.Employee_Last_Name__c = patientCase.Patient_Last_Name__c = employee.LastName__c;
      patientCase.Employee_DOBe__c = patientCase.Patient_DOBe__c = employee.Dobe__c;
      patientCase.Employee_Gender__c = patientCase.Patient_Gender__c = employee.Gender__c;
      patientCase.Employee_SSN__c = patientCase.Patient_SSN__c = employee.SSN__c;

      patientCase.Employee_Street__c = patientCase.Patient_Street__c= employee.StreetAddress__c;
      patientCase.Employee_City__c = patientCase.Patient_City__c = employee.City__c;
      patientCase.Employee_State__c = patientCase.Patient_State__c = employee.State__c;
      patientCase.Employee_Zip_Postal_Code__c = patientCase.Patient_Zip_Postal_Code__c = employee.ZipCode__c;

      patientCase.Employee_Mobile__c = patientCase.Patient_Mobile__c = employee.MobilePhoneNumber__c;

      //Section 2
      patientCase.Total_Joint_Replacement_Recommended__c = 'Yes';
      patientCase.nProposed_Procedure__c = 'Right Knee Replacement';
      patientCase.Joint_Previously_Replaced__c = patientCase.Other_Joint_replaced_in_COE_program__c = 'No';
      patientCase.Employee_Insurance_Primary_Not_HMO__c = 'No';
      patientCase.Surgery_Related_to_Work_Injury__c = 'No';
      patientCase.Related_to_injury_seeking_legal_action__c ='No';

      //Section 3
      patientCase.Physician_specialty_recommend_tjr__c = 'Orthopedic Surgeon';
      patientCase.Joint_Recommendation_last_12_months__c = 'Yes';
      patientCase.Patient_Height_FT__c ='5';
      patientCase.Patient_Height_Inches__c = '10';
      patientCase.Diabetic__c ='No';
      patientCase.Current_Nicotine_User__c ='No';
      patientCase.Medication_for_Pain_Control__c = 'No';
      patientCase.Does_the_patient_use_marijuana__c ='No';
      patientCase.Does_patient_have_a_skin_condition__c ='No';

      //Section 4
      patientCase.jointNumberofTimesFallen__c = 'Never';
      patientCase.isConverted__c = true;
      patientCase.Converted_Date__c = date.today();
      insert patientCase;

      string case_name = [select name from patient_case__c where id = :patientCase.id].name;

      Test.startTest();

      ApexPages.StandardController sc = new ApexPages.StandardController(new case_task__c());
      caseTaskExtension cte = new caseTaskExtension(sc);
      cte.task.Procedure__c ='Spine';
      cte.task.Client__c ='Walmart';
      cte.task.Field__c ='Info_Packet_Sent__c';
      cte.task.Visible_to_Facility__c = true;
      cte.programValues=new string[]{};
      cte.programValues.add('Spine');
      cte.saveNewTask();
      id case_task_id = cte.task.id;
      sc = new ApexPages.StandardController(cte.task);
      cte = new caseTaskExtension(sc);
      cte.saveTask();

      cte.setfieldOptions();

      cte.task.procedure__c='Oncology';
      cte.programValues.clear();
      cte.programValues.add('Oncology');
      cte.setfieldOptions();

      cte.task.procedure__c='Weight Loss Surgery';
      cte.programValues.clear();
      cte.programValues.add('Weight Loss Surgery');
      cte.setfieldOptions();

      cte.setProgramOptions();

      cte.cancelTask();

      ApexPages.CurrentPage().getParameters().put('clone','true');
      sc = new ApexPages.StandardController(cte.task);
      cte = new caseTaskExtension(sc);

      RestRequest req = new RestRequest();

      req.addHeader('EcenCase', 'PC-000000');
      RestResponse res = new RestResponse();

      req.requestURI = '/services/apexrest/providerPortal/v1/providerportaltasks';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      ProviderPortalTaskListener.get_tasks();

      req = new RestRequest();
      req.addHeader('EcenCase', case_name);

      res = new RestResponse();

      req.requestURI = '/services/apexrest/providerPortal/v1/providerportaltasks';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      ProviderPortalTaskListener.get_tasks();

      req = new RestRequest();
      req.addHeader('EcenCase', case_name);

      res = new RestResponse();
      req.requestURI = '/services/apexrest/providerPortal/v1/providerportaltasks';  //Request URL
      req.httpMethod = 'POST';//HTTP Request Type
      req.RequestBody = blob.valueof('[{"description__c":"Caregiver Responsibility Sent","dueDate__c":null,"Name":"Caregiver Responsibility Sent","organization__c":null,"status__c":"Complete"}]');
      //"Case_Task__c":"'+case_task_id+'",
      RestContext.request = req;
      RestContext.response= res;
      ProviderPortalTaskListener.save_tasks();

      req = new RestRequest();
      req.addHeader('EcenCase', case_name);

      res = new RestResponse();
      req.requestURI = '/services/apexrest/providerPortal/v1/providerportaltasks';  //Request URL
      req.httpMethod = 'POST';//HTTP Request Type
      req.RequestBody = blob.valueof('[{"Patient_Case__c":"'+patientCase.id+'","description__c":"Caregiver Responsibility Sent","dueDate__c":null,"Name":"Caregiver Responsibility Sent","organization__c":null,"status__c":"Complete"}]');
      RestContext.request = req;
      ProviderPortalTaskListener.save_tasks();

      req = new RestRequest();
      req.addHeader('EcenCase', case_name);

      res = new RestResponse();

      req.requestURI = '/services/apexrest/providerPortal/v1/providerportaltasks';  //Request URL
      req.httpMethod = 'GET';//HTTP Request Type
      RestContext.request = req;
      RestContext.response= res;
      ProviderPortalTaskListener.get_tasks();

      Patient_Case_Task__c update_task = [select id from Patient_Case_Task__c where patient_case__c = :patientCase.id limit 1];
      patientCase.Info_Packet_Sent__c = date.today();
      update patientCase;

      update_task.field__c = 'Info_Packet_Sent__c';
      update update_task;

      Test.stopTest();

    }

    /* ************************************************************************** */


}
