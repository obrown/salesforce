public with sharing class bariatricHardstop{

    public class hardstop{
        public boolean hardstop;
        public boolean sendDenialLetter;
        public string message;
    }

    public static bariatricHardstop.hardstop isHardstop(bariatric__c b){
        bariatricHardstop.hardstop result = new bariatricHardstop.hardstop();
        result.hardstop=false;
        
        if(b.patient_bmi__c<35.0){
            result.hardstop=true;
            result.sendDenialLetter=true;
            result.message= 'Patient BMI is less than 35'; 
            return result;   
        }
        
        if(b.patient_bmi__c>=35.0 && b.patient_bmi__c<40.0){
           
           if(b.Medication_List__c!='Included' && 
              (b.Type_2_Diabtetic__c=='Yes' ||
              b.hypertension__c=='Yes' ||
              b.Respiratory_Disorders__c=='Yes' ||
              b.Heart_Disease__c=='Yes')){
           
              result.hardstop=true;
              result.sendDenialLetter=true;
              result.message= 'Patient BMI is less than 40, please include a medication list';
              return result;
           }
            
           if(b.Type_2_Diabtetic__c!='Yes' && 
               b.Respiratory_Disorders__c!='Yes' &&
               b.Heart_Disease__c!='Yes' &&
               b.Osteoarthritis__c!='Yes' &&
               b.Sleep_Apnea__c !='Yes' &&
               b.Non_alcoholic_Fatty_Liver_Disease__c!='Yes' &&
               b.Lipid_Abnormalities__c!='Yes' &&
               b.Gastrointestinal_Disorders__c!='Yes' &&
               b.hypertension__c!='Yes'){
                result.hardstop=true;
                result.sendDenialLetter=true;
                result.message= 'Patient BMI is less than 40 and no Comorbid condition indicated';
                return result;
           }   
            
        }
        
        return result;
    }

}