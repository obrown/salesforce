public with sharing class populationHealthBannerSearch {

    //public phm_Patient__c [] results { get; set; }
    //public phm_Patient__c[] patients {get; set;}
    //public phm_Patient__c npatient  {get; set;}
    
    public Utilization_Management__c [] results { get; set; }
    public Utilization_Management__c [] patients {get; set;}
    public Utilization_Management__c npatient  {get; set;}    
    public string searchTerm {get; private set;}
    public SelectOption[] client {get; set;}
    public string clientFilter {get; set;}
    
    public populationHealthBannerSearch(){
        this.searchTerm = ApexPages.CurrentPage().getParameters().get('searchTerm');
        //results = new phm_Patient__c[]{};
        results = new Utilization_Management__c []{};
        //searchpatients();
        loadClientList();
        searchUM();
    }

    void loadClientList(){
        client  = new selectOption[]{};
        client.add(new selectOption('','--None--'));
        for(Client__c  c : [select underwriter__c,active__c,logoDirectory__c, name,Care_Management__c,Disease_Management__c,Maternity_Management__c,Utilization_Management__c from Client__c where underwriter__c != '' order by name asc]){
            //clientmap.put(c.underwriter__c, c);
            if(c.Active__c){
            client.add(new selectOption(c.underwriter__c, c.name));
            }
        }
        
    }

    /*
    public void searchpatients(){
        patients = new phm_Patient__c[]{};
        
        //First Name Search
        if(patients.size()<1){
        patients =[select Patient_First_Name__c,Patient_Last_Name__c, Patient_Employer__r.name,City__c,State__c,Effective_Date__c,Relationship_to_Subscriber__c,lastmodifieddate, LastModifiedBy.Name from phm_Patient__c where Patient_First_Name__c=:searchTerm];
        }

        if(patients.size()<1){ 
        //Last Name Search
        
        patients =[select Patient_First_Name__c,Patient_Last_Name__c, Patient_Employer__r.name,City__c,State__c,Effective_Date__c,Relationship_to_Subscriber__c,lastmodifieddate, LastModifiedBy.Name from phm_Patient__c where Patient_Last_Name__c=:searchTerm];
        }    
    }
*/
    public void searchUM(){
    
        set<id> dupList = new set<id>();
        patients = new Utilization_Management__c []{};
        
        for(Utilization_Management__c um : [select Facility_Name__c,Med_Cat__c,Patient__r.Patient_Employer__r.underwriter__c,healthpac_case_number__c, Patient__r.Patient_First_Name__c,Patient__r.Patient_Last_Name__c,name,Patient__c,Patient__r.City__c,Patient__r.State__c,LastModifiedBy.Name,LastModifiedDate,patient__r.Patient_Employer__c,patient__r.Relationship_to_Subscriber__c from Utilization_Management__c]){
            if(!dupList.contains(um.id)){
            
                if(um.Patient__r.Patient_first_Name__c.containsIgnoreCase(searchTerm) || um.Patient__r.Patient_last_Name__c.containsIgnoreCase(searchTerm)){
                    patients.add(um);
                    dupList.add(um.id);
                
                }
                
                if(um.healthpac_case_number__c!=null && um.healthpac_case_number__c.contains(searchTerm)){
                    patients.add(um);
                    dupList.add(um.id);
                }
                
                if(um.name.contains(searchTerm)){
                    patients.add(um);
                    dupList.add(um.id);
                }
            }
        }
        
        
    }
    
    public void clientFilter(){
        set<id> dupList = new set<id>();
        patients = new Utilization_Management__c []{};
        
        for(Utilization_Management__c um : [select Facility_Name__c,Med_Cat__c, Patient__r.Patient_First_Name__c,Patient__r.Patient_Last_Name__c,name,Patient__c,Patient__r.City__c,Patient__r.State__c,LastModifiedBy.Name,LastModifiedDate,patient__r.Patient_Employer__c,patient__r.Relationship_to_Subscriber__c from Utilization_Management__c where patient__r.patient_employer__r.underwriter__c=:clientFilter]){
            if(!dupList.contains(um.id) && (um.Patient__r.Patient_first_Name__c.containsIgnoreCase(searchTerm) || um.Patient__r.Patient_last_Name__c.containsIgnoreCase(searchTerm))){
            
                patients.add(um);
                
            }
        }
        
    }

}