public class hp_rxSearch extends hp_callWebApp{
    
    public hpRxSearchStruct returnObj = new hpRxSearchStruct();
    
    public static hpRxSearchStruct searchHP(string uw, string essn, string grp, string seq, string dosStart, string dosEnd){
        
        hp_rxSearch searchHP = new hp_rxSearch();
        searchHP.searchHealthPac(uw,essn,grp,seq,dosStart, dosEnd);
        return searchHP.returnObj;
        
    }
    
    public hpRxSearchStruct searchHealthPac(string uw, string essn, string grp, string seq, string dosStart, string dosEnd){
        
        endpoint='getrxtrans';
        
        if(uw==null){uw='';}
        if(essn==null){essn='';}
        if(seq==null){seq='';}
        if(grp==null){grp='';}
        if(dosStart==null){dosStart='';}else{dosStart=formatDate(dosStart);}
        if(dosEnd==null){dosEnd='';}else{dosEnd=formatDate(dosEnd);}
        
        essn=essn.trim();
        
        search.add('Essn:'+essn);
        search.add('Underwriter:'+uw);
        search.add('Group:'+grp);
        search.add('DosStart:'+dosStart);
        search.add('DosEnd:'+dosEnd);
        search.add('Seq:'+seq);
        
        jsonBody = jb.eligSearch(search);
        
        if(Test.isRunningTest()){
            result =callWebApp('{"ResultCode":"0","RxTxns":[{"Transtype":"R","Transdate":"20160123","Underwriter":"008","Grpnbr":"CP4","Essn":"258238832","Patseq":"00","Dedamount":"         .00","Oopamount":"         .00","Dos":"20160123","ClaimNbr":"000000000000000000","ErrMsg":"AMOUNT MISMATCH ON RECON                          ","Filename":"medco_recon20160123.txt                           "},{"Transtype":"R","Transdate":"20160130","Underwriter":"008","Grpnbr":"CP4","Essn":"258238832","Patseq":"00","Dedamount":"         .00","Oopamount":"         .00","Dos":"20160130","ClaimNbr":"000000000000000000","ErrMsg":"AMOUNT MISMATCH ON RECON                          ","Filename":"medco_recon20160130.txt                           "},{"Transtype":"R","Transdate":"20160206","Underwriter":"008","Grpnbr":"CP4","Essn":"258238832","Patseq":"00","Dedamount":"         .00","Oopamount":"         .00","Dos":"20160206","ClaimNbr":"000000000000000000","ErrMsg":"AMOUNT MISMATCH ON RECON                          ","Filename":"medco_recon20160206.txt                           "}]}');
        }else{
            result = callWebApp(null);
        }
        
        result = result.replaceAll('":"','__c":"');
        result = result.replace('ResultCode__c','ResultCode');
        result = result.replace('ErrorReason__c','ErrorReason');
        
        returnObj.ResultCode='2';
        
        try{
            
            returnObj = (hpRxSearchStruct)JSON.deserialize(result, hpRxSearchStruct.class);
        }catch(exception e){
            system.debug(e.getMessage());
            
            
        }
        
        if(returnObj.ResultCode=='0'){
            //cleanUpLog(returnObj.RxErrors);
        }
        
        return returnObj;
    }
    
         

    
    
    //Expected Date format, yyyy-mm-dd
    string formatDate(string d){
        d = d.replaceAll('-','');
        return d;
    }
    
}