/**
* This class contains unit tests for validating the behavior of the Apex authNumberCheckController class
*/

@isTest(seealldata=true)
private class authNumberCheckControllerTest{

    static testMethod void authNumberCheckControllerTest() {
    
        PatientCase__c recordType = PatientCase__c.getInstance();  
        
        recordType rt = [select id, name, developerName from recordType where name = 'Walmart Joint'];
        system.debug(rt);
        
        patient_case__c pc = new patient_case__c();
        pc.Employee_Last_Name__c='Test';
        pc.Employee_First_Name__c='Unit';
        pc.Employee_Gender__c= 'Male';
        pc.Employee_DOBe__c= '1950-01-01';
        pc.Employee_Street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c= 'AZ';
        pc.Employee_Zip_Postal_Code__c= '85212';
        pc.Employee_Mobile__c= '(480) 555-1212';
        pc.Employee_Home_Phone__c= '(480) 555-1212';
        pc.Employee_Work_Phone__c= '(480) 555-1212';
        
        pc.Patient_Last_Name__c='Test';
        pc.Patient_First_Name__c='Unit';
        pc.Patient_Gender__c= 'Male';
        pc.Patient_DOBe__c= '1950-01-01';
        pc.Patient_Street__c = '123 E Main St';
        pc.Patient_City__c = 'Mesa';
        pc.Patient_State__c= 'AZ';
        pc.Patient_Zip_Postal_Code__c= '85212';
        pc.Patient_Mobile__c= '(480) 555-1212';
        pc.Patient_Home_Phone__c= '(480) 555-1212';
        pc.Patient_Work_Phone__c= '(480) 555-1212';
        //pc.Relationship_to_the_Insured__c = 'Patient is Insured';
        pc.recordtypeid = recordType.wmJoint__c;
        pc.recordtype =rt;
        pc.client_name__c ='Walmart';
        pc.Program_Authorization_Received__c=date.today();
        pc.Plan_of_Care_accepted_at_HDP__c=date.today();
        pc.Caregiver_Responsibility_Received__c=date.today();
        
        insert pc;
        
        authNumberCheckController acc = new authNumberCheckController(pc.id);
        
        pc.recordtypeid = recordType.wmSpine__c;
        pc.Financial_Responsibility_Waiver__c = date.today();
        pc.Caregiver_Responsibility_Received__c = date.today();
        
        update pc;
        
        ApexPages.CurrentPage().getParameters().put('id', pc.id);
        acc = new authNumberCheckController();
        
        pc.client_name__c ='Lowes';
        pc.recordtypeid = recordType.lowesSpine__c;
        
        pc.current_nicotine_user__c = 'Yes';
        
        update pc;
        acc = new authNumberCheckController(pc.id);
        
        
    }
    
}