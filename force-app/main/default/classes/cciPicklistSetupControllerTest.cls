@isTest
private class cciPicklistSetupControllerTest {

    static testMethod void SetupControllerTest() {
        
        cciPicklistSetupController cpsc = new cciPicklistSetupController();
        customerCareController ccc = new customerCareController();

        
        cpsc.workTaskName= 'Unit Test Work Task';
        cpsc.saveWorkTask();
        cpsc.workTaskId = cpsc.isl.workTask[1].getValue();
        system.assert(cpsc.workTaskId!=null);
        cpsc.isl.workTaskValue =cpsc.workTaskId;
        string workTaskId = cpsc.workTaskId;
        
        
        ApexPages.CurrentPage().getParameters().put('workTaskId',cpsc.workTaskId);
        cpsc.inquiryReasonName = 'Unit Test Inquiry Reason';
        cpsc.saveInquiryReason();
        cpsc.inquiryReasonId = cpsc.isl.inquiryReason[1].getValue();
        system.assert(cpsc.inquiryReasonId !=null);
        cpsc.isl.inquiryReasonValue =cpsc.inquiryReasonId ;
        string inquiryReasonId = cpsc.inquiryReasonId;
        
        ApexPages.CurrentPage().getParameters().put('inquiryReasonId',cpsc.inquiryReasonId);
        cpsc.saveInquiryReason();
        cpsc.workDepartmentName= 'Unit Test Work Department';
        cpsc.saveWorkDepartment();
        cpsc.workDepartmentId = cpsc.isl.workDepartment[1].getValue();
        system.assert(cpsc.workDepartmentId !=null);
        cpsc.isl.workDepartmentValue=cpsc.workDepartmentId;
        string workDepartmentId = cpsc.workDepartmentId;
        cpsc.saveWorkDepartment();
        cpsc.closedReasonName= 'Unit Test Closed Reason';
        cpsc.saveClosedReason();
        cpsc.closedReasonId = cpsc.isl.closedReason[1].getValue();
        system.assert(cpsc.closedReasonId!=null); 
        string closedReasonId= cpsc.closedReasonId;
        cpsc.saveClosedReason();
       
        ApexPages.CurrentPage().getParameters().put('workTaskId',null);
        cpsc.setWorkTaskToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('workTaskId',workTaskId);
        ApexPages.CurrentPage().getParameters().put('workTaskName','Test');
        cpsc.setWorkTaskToUpdate();
        cpsc.saveWorkTask();
        
        ApexPages.CurrentPage().getParameters().put('inquiryReasonId',null);
        cpsc.setInquiryReasonToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('inquiryReasonId',inquiryReasonId);
        ApexPages.CurrentPage().getParameters().put('inquiryReasonName','Test');
        cpsc.setInquiryReasonToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('workDepartmentId',null);
        cpsc.setWorkDepartmentToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('workDepartmentId',workDepartmentId);
        ApexPages.CurrentPage().getParameters().put('workDepartmentName','Test');
        cpsc.setWorkDepartmentToUpdate();
        
        
        ApexPages.CurrentPage().getParameters().put('closedReasonId',null);
        cpsc.setClosedReasonToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('closedReasonId',closedReasonId);
        ApexPages.CurrentPage().getParameters().put('closedReasonName','Test');
        cpsc.setClosedReasonToUpdate();
        
        ApexPages.CurrentPage().getParameters().put('ewtSoId',workDepartmentId);
        cpsc.removeEWTSO();
        
        ApexPages.CurrentPage().getParameters().put('ewtSoId',inquiryReasonId);
        cpsc.removeEWTSO();
        
        ApexPages.CurrentPage().getParameters().put('ewtSoId',workTaskId);
        cpsc.removeEWTSO();
        
        
        ApexPages.CurrentPage().getParameters().put('ewtSoId',null);
        cpsc.removeEWTSO();
        
        cpsc = new cciPicklistSetupController();
    }
    
}