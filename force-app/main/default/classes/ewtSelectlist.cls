public without sharing class ewtSelectlist{
    
    map<id, EWT_Document_Type__c> documentTypeMap;
    public map<string , string> workTaskMap {get;set;}
    public map<string , string> workTaskReasonMap {get;set;}
    public map<string , string> workDepartmentMap {get;set;}
    public map<string , string> closedReasonMap {get;set;}
    public map<string , string> workTaskStatusMap {get;set;}
    public map<string , string> workTaskStatusValueMap {get;set;}
    
    public selectOption[] documentType {get; set;}
    public selectOption[] workTask {get; set;}
    public selectOption[] worktaskReason {get; set;}
    public selectOption[] workDepartment {get; set;}
    public selectOption[] closedReason{get; set;}
    public selectOption[] workTaskStatus{get; set;}
    
    boolean isRelated;
    
    public integer workTaskReasonCount {get; private set;}
    
    public string workTaskValue {get; set;}
    public string documentTypeValue {get; set;}
    public string worktaskReasonValue {get; set;}
    public string workDepartmentValue {get; set;}
    public string closedReasonValue {get; set;}
    public string workTaskStatusValue {get; set;}
    
    public ewt_document_type__c documentTypeRecord {get; private set;}
    
    public string documentTypeName;
   
    public string drive;
        
    public void setWorkTaskType(string taskValue){
        this.workTaskValue = taskValue;
    }
    
    public ewtSelectlist(boolean isRelated, string documentTypeValue, string workTaskValue, string worktaskReasonValue, string workDepartmentValue, string closedReasonValue,string workTaskStatusValue){
        this.isRelated=isRelated;
        
        this.documentTypeValue=documentTypeValue;   
        getTheDocumentType();
        
        getTheWorkTask();
        this.workTaskValue=workTaskValue;
        
        getTheWorkTaskReason();
        this.worktaskReasonValue=worktaskReasonValue;
        
        getTheWorkDepartment();
        this.workDepartmentValue=workDepartmentValue;
        
        getTheClosedReason();
        this.closedReasonValue=closedReasonValue;
        
        getTheworkTaskStatus();
        this.workTaskStatusValue=workTaskStatusValue;   
    }
    
    public ewtSelectlist(){
        isRelated = false;
        getTheDocumentType();
        
    }
   
    /*
    public selectoption[] getworkSource(){
        
        workSource = new selectoption[]{};
        workSource.add(new selectoption('','--None--'));
        for(Operation_Work_Task_Question__c q : questions){
            if(q.recordType.Name==workSourceVal){
                workSource.add(new selectoption(q.Field_Value__c, q.Field_Value__c));
            }
        }
        return workSource;
    }
    */
    
    public EWT_Document_Type__c getDocumentName(id documentTypeValue){
        return documentTypeMap.get(documentTypeValue);
    }
    
    public string getWorkTaskName(string val){
        return workTaskMap.get(val);
    }
    
    public string getWorkTaskReasonName(string val){
        return workTaskReasonMap.get(val);
    }
    
    public string getWorkDepartmentName(string val){
        return workDepartmentMap.get(val);
    }
    
    public string getClosedReasonName(string val){
        return closedReasonMap.get(val);
    }

    public string getworkTaskStatusName(string val){
        return workTaskStatusMap.get(val);
    } 
    
    public void setdocumentType(){
       try{
           id idFoo = id.valueof(ApexPages.CurrentPage().getParameters().get('documentTypeId'));
           documentTypeValue = string.valueof(idFoo);
           getTheWorkTask();
           documentTypeRecord=documentTypeMap.get(documentTypeValue);
       }catch(exception e){
           clearWorkTask();
           clearWorkTaskReason();
           clearWorkDepartment();
           clearClosedReason();
           
       }
        
    }
    public void setWorkTask(){
        try{
           id idFoo = id.valueof(ApexPages.CurrentPage().getParameters().get('workTaskId'));
           workTaskValue = string.valueof(idFoo);
           getTheWorkTaskReason();
        }catch(exception e){
           clearWorkTaskReason();
           clearWorkDepartment();
           clearClosedReason();
        }
    }
    
    public void setWorkTaskReason(){
        try{
           id idFoo = id.valueof(ApexPages.CurrentPage().getParameters().get('workTaskReasonId'));
           workTaskReasonValue = string.valueof(idFoo);
           getTheWorkDepartment();
        }catch(exception e){
           clearWorkDepartment();
           clearClosedReason();
        }
    
    }
    
    public void setWorkDepartment(){
        try{
           id idFoo = id.valueof(ApexPages.CurrentPage().getParameters().get('workDepartmentId'));
           workDepartmentValue = string.valueof(idFoo);
           getTheClosedReason();
        }catch(exception e){
        
        }
    
    }
    
    public void setworkTaskStatus(){
        try{
           id idFoo = id.valueof(ApexPages.CurrentPage().getParameters().get('workTaskStatusId'));
           workTaskStatusValue = string.valueof(idFoo);
           
        }catch(exception e){
        
        }
    
    }
    
    public void setclosedReason(){}
    
    public selectoption[] getTheDocumentType(){
        workTask= new selectOption[]{new selectOption('','--None--')};
        worktaskReason = new selectoption[]{new selectOption('','--None--')};
        workDepartment= new selectoption[]{new selectoption('','--None--')};
            
        documentType= new selectoption[]{};
        documentType.add(new selectoption('','--None--'));
        documentTypeMap = new map<id, EWT_Document_Type__c>();
            
        for(EWT_Document_Type__c ewt : [select HP_Document_Type__c,Description__c,Document_Type__c, Folder_Location__c from EWT_Document_Type__c order by name asc]){
            documentType.add(new selectoption(ewt.id, ewt.Document_Type__c));
            documentTypeMap.put(ewt.id, ewt);    
                
        }
           
        documentTypeRecord = documentTypeMap.get(documentTypeValue);
        getTheClosedReason();
       
        return documentType;
    }
    
    public void clearWorkTask(){
        workTaskValue='';
        workTask = new selectoption[]{new selectoption('','--None--')};
    }
    
    public void clearWorkTaskReason(){
        worktaskReason = new selectoption[]{new selectoption('','--None--')};
        workTaskReasonValue='';
    }
    
    public void clearWorkDepartment(){
        workDepartment = new selectoption[]{new selectoption('','--None--')};
        workDepartmentValue='';
    }
    
    public void clearClosedReason(){
        closedReason = new selectoption[]{new selectoption('','--None--')};
        closedReasonValue='';
    }
    
    public selectoption[] getTheWorkTask(){
        
        workTaskMap = new map<string , string>();
        workTask= new selectoption[]{new selectoption('','--None--')};
        worktaskReason = new selectoption[]{new selectOption('','--None--')};
        workDepartment= new selectoption[]{new selectoption('','--None--')};
        closedReason= new selectoption[]{new selectoption('','--None--')};
        
        workTaskValue='';
        worktaskReasonValue='';
        workDepartmentValue='';
        closedReasonValue='';
        
        set<SelectOption> optionsSet = new set<SelectOption>();
        
        for(EWT_Work_Task__c wt : [select name from EWT_Work_Task__c where Document_Type__c = :documentTypeValue order by name asc]){
            optionsSet.add(new selectoption(wt.id,wt.name));
            workTaskMap.put(wt.id, wt.name);
        }
        
        workTask.addAll(optionsSet);
        
        
        return workTask;
    }
    
    /* Take in workTaskValue, query for ewt work task reasons 
       based on the parent worktask id which should already be set
       
       We could possibly store a map of the values
       
    */
    
    public selectoption[] getTheWorkTaskReason(){
        workTaskReasonMap = new map<string , string>();    
        worktaskReason= new selectoption[]{new selectoption('','--None--')};
        workTaskReasonCount=0;
        system.debug('workTaskValue  '+workTaskValue );
        set<SelectOption> optionsSet = new Set<SelectOption>();
        
        for(EWT_Work_Task_Reason__c wtr : [select name from EWT_Work_Task_Reason__c where EWT_Work_Tasks__c= :workTaskValue order by name asc]){
            optionsSet.add(new selectoption(wtr.id,wtr.name));
            workTaskReasonMap.put(wtr.id, wtr.name);
            workTaskReasonCount++;
        }
        
        getTheWorkDepartment();
        getTheClosedReason();
        
        worktaskReason.addAll(optionsSet);
        
        if(worktaskReason==null){
           worktaskReason= new selectoption[]{new selectoption('','--None--')};
        }
        
        return worktaskReason;
    }

    public selectoption[] getTheWorkDepartment(){
        workDepartmentMap = new map<string , string>();
        workDepartment= new selectoption[]{new selectoption('','--None--')};
        Set<SelectOption> optionsSet = new Set<SelectOption>();
        workDepartmentValue='';
        
        if(workTaskValue!=null && worktaskReasonValue!=null){
            for(EWT_Work_Department__c ewd : [select name, Work_Task__c, Work_Task_Reason__c from EWT_Work_Department__c where Work_Task_Reason__c =:worktaskReasonValue and Work_Task__c =:workTaskValue  order by name asc]){
                optionsSet.add(new selectoption(ewd.id,ewd.name));
                workDepartmentMap.put(ewd.id, ewd.name);
            }
        }
        
        if(workTaskValue!=null && worktaskReasonValue==null){
            for(EWT_Work_Department__c ewd : [select name, Work_Task__c, Work_Task_Reason__c from EWT_Work_Department__c where Work_Task__c =:workTaskValue and Work_Task_Reason__c =null order by name asc]){
                optionsSet.add(new selectoption(ewd.id,ewd.name));
                workDepartmentMap.put(ewd.id, ewd.name);
            }
        }
        
        workDepartment.addAll(optionsSet);
        if(workDepartment==null){
           workDepartment= new selectoption[]{new selectoption('','--None--')};
       
        }else if(workDepartment.size()==2){
           system.debug('workDepartment[1].getValue() '+workDepartment[1].getValue());
           workDepartmentValue = workDepartment[1].getValue();
           
        }
        
        getTheClosedReason();
 
            
        /* DocumentType=='Medical Records' and  Work Task == Accident Review */
        if(workTaskValue=='a2uQ0000001KI89IAG'){
            if(workDepartmentMap.keyset().contains(id.valueof('a2wQ0000001f8u4IAA'))){
                workDepartmentValue='a2wQ0000001f8u4IAA'; //Claims Technical
            }
        
        }else if(null!=null){
        
        }
        
        
       
             
       return workDepartment;
        
    }    
    
    public selectoption[] getTheClosedReason(){
        closedReasonMap = new map<string , string>();
        closedReason= new selectoption[]{};
        closedReason.add(new selectoption('','--None--'));
        
        for(EWT_Closed_Reason__c ecr : [select name from EWT_Closed_Reason__c where EWT_Document_Type__c =:documentTypeValue and 
                                                                                    EWT_Work_Task__c =:workTaskValue and 
                                                                                    EWT_Work_Task_Reason__c =:worktaskReasonValue and 
                                                                                    EWT_Work_Department__c =:workDepartmentValue order by name asc]){
        
            closedReason.add(new selectoption(ecr.id, ecr.name));
            closedReasonMap.put(ecr.id, ecr.name);
        
        }
        
        return closedReason;
    }
    
    public selectoption[] getTheworkTaskStatus(){
        workTaskStatusMap = new map<string , string>();
        workTaskStatusValueMap = new map<string , string>();
        workTaskStatus= new selectoption[]{};
        workTaskStatus.add(new selectoption('','--None--'));

        
        
        for(EWT_Work_Task_Status__c ewts : [select name from EWT_Work_Task_Status__c  order by name asc]){
        
            workTaskStatus.add(new selectoption(ewts.id, ewts.name));
            workTaskStatusMap.put(ewts.id, ewts.name);
            workTaskStatusValueMap.put(ewts.name, ewts.id);
        
        }
        
        return workTaskStatus;
    }    
    
    public void reset(){
        documentTypeValue='';
        workTaskValue='';
        worktaskReasonValue='';
        workDepartmentValue='';
        workTaskStatusValue ='';
        
        workTask= new selectOption[]{new selectOption('','--None--')};
        workDepartment= new selectOption[]{new selectOption('','--None--')};
        closedReason= new selectOption[]{new selectOption('','--None--')};
        worktaskReason= new selectOption[]{new selectOption('','--None--')};
        workTaskStatus=new selectOption[]{new selectOption('','--None--')};
        
        system.debug('documentTypeValue '+documentTypeValue);
        system.debug('workTaskValue '+workTaskValue);
        system.debug('worktaskReasonValue '+worktaskReasonValue);
        system.debug('workDepartmentValue '+workDepartmentValue);
    
    }
    
}