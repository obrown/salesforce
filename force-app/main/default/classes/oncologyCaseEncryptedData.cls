public with sharing class oncologyCaseEncryptedData{
        
        public string clientName {get; set;}
        
        public string patientName {get; set;}
        public string patientFirstName {get; set;}
        public string patientLastName {get; set;}
        public string patientDOB {get; set;}
        public string preferredName{get; set;}
        public string patientGender{get; set;}
        
        public string employeeFirstName {get; set;}
        public string employeeLastName {get; set;}
        public string employeeDOB {get; set;}
        
        public string patientStreet {get; set;}
        public string patientCSZ {get; set;}
        public string patientCity {get; set;}
        public string patientState {get; set;}
        public string patientZip {get; set;}
        public string patientPhone {get; set;}
        public string patientPhone2 {get; set;}
        public string patientPhone3 {get; set;}
        public string patientPreferredPhone{get; set;}
        public string PatientIDNumber {get; set;}
        
        public string employeeStreet {get; set;}
        public string employeeCSZ {get; set;}
        public string employeeCity {get; set;}
        public string employeeState {get; set;}
        public string employeeZip {get; set;}
        
        public string employeeEmail {get; set;}
        public string patientEmail {get; set;}
        
        
        public string facility {get; set;}
        public string logoUrl {get; set;}
        public string Grp {get; set;}
        public string planType {get; set;}
        public string id {get; set;}
        public string effDate {get; set;}
        public string termDate {get; set;}
        public string bestTimetoContact {get; set;}
        public string preferredLanguage {get; set;}
        public string translationServicesRequired {get; set;}
        
        public datetime referralDate {get; set;}
        public datetime dateEligibilityVerified {get; set;}
        public string relationshiptotheEmployee{get; set;}
        public string primaryDiagnosisPatientReported{get; set;}
        public date   dateofDiagnosisPatientReported {get; set;}
        public string treatmentPatientReported {get; set;}
        public string otherPertinentMedicalInformation {get; set;}
        
        public string localPhysician{get; set;}
        public string localPhysicianSpecialty {get; set;}
        public string localPhysicianAddress {get; set;}
        public string localPhysicianCity {get; set;}
        public string localPhysicianState {get; set;}
        public string localPhysicianZip {get; set;}
        public string localPhysicianPhoneNumber {get; set;}
        public string localPhysicianFaxNumber {get; set;}
        public string associatedFacilities {get; set;}
        
        public string cohNurse {get; set;}
        public string cohNursePhoneNumber {get; set;}
        public string cohNurseEmail {get; set;}
        public string caregiverFirstName {get; set;}
        public string caregiverLastName {get; set;}
        public string caregiverDOB {get; set;}
        public string caregiverHomePhone {get; set;}
        public string caregiverMobilePhone {get; set;}        
    }