public with sharing class vendorReferralController {
      // public string orf {get; private set;}
    public patient_case__c obj {
        get; set;
    }
    public patient_case__c newCase {
        get; set;
    }
    public string oldCaseRecordType {
        get; private set;
    }
    public map<string, string> miscMap {
        get; private set;
    }
    string rtName;

    transient selectOption[] vendorFacilityOpts;

    public vendorReferralController() {
        obj = [select createHP__c, Referred_Vendor__c, Referred_to_Vendor_Date__c, Actual_Arrival__c, Actual_Departure__c, Actual_Termination_Date__c, Advise_of_Pre_Travel_Testing__c, Advise_of_Pre_Travel_Testing_2__c, Advise_of_Pre_Travel_Testing_3__c, Additional_Airfare_Caregiver__c, Additional_Airfare_Patient__c, Additional_Stipend__c, Address_for_Stipend_Check__c, Advise_of_Nicotine_Testing_Process__c, Advise_on_potential_dental_clearance__c, Airfare_Train_Amount__c, Airfare_Train_Change_Fees__c, Airfare_Offset__c, Airfare_total__c, Airport_Parking__c, AMEX_Trip_ID__c, Anti_inflammatory_medicine__c, anitInlfam_taken_in_the_last_6_months__c, Antiinflam_medicine_discontinued_reason__c, Arrival_Date__c, Arriving_Airport_Tranpsortation__c, Arriving_Flight_Number__c, Arriving_Flight_Time__c, attachEncrypt__c, Authorization_Number__c, authReady__c, authSent__c, Baggage__c, BID__c, BID_Verified__c, Blood_Clotting_Disorder_Date__c, Callback_Number__c, Callback_Type__c, Caller_Name__c, Caller_relationship_to_the_patient__c, Cancelled_Accepted_for_Program_Surgery__c, Cancelled_BMI__c, Cancelled_Comorbid_Medical_Conditions__c, Cancelled_Nicotine__c, Cancelled_No_Surgery_Indicated__c, Cancelled_No_Surgery_Indicated_Joint__c, Cancelled_No_Surgery_Indicated_Spine__c, Caregiver_Cleared_to_Travel__c, Caregiver_Cleared_to_Travel_Reason__c, Caregiver_COVID_Testing_Results__c, Caregiver_Declined_Testing__c, Caregiver_End_Date_of_Testing__c, Caregiver_Pre_Travel_Test_Scheduled__c, Caregiver_Start_Date_of_Testing__c, Clinically_Verified_Outcome_of_Visit__c, client_facility__r.facility__r.name, nclient_name__c, Caregiver_DOB__c, Caregiver_Exception_Requested__c, Caregiver_Home_Phone__c, Caregiver_Medical_Necessity_Form__c, Caregiver_Mobile__c, Caregiver_Name__c, Caregiver_Relationship__c, Caregiver_Request_Approved__c, Caregiver_Responsibility_Received__c, Caregiver_Responsibility_Sent__c, Caregiver_Email__c, Carrier_and_Plan_Type_verified__c, Carrier_E_mail__c, Carrier_Name__c, Carrier_Nurse_Name__c, Carrier_Nurse_Phone__c, CaseLatitudeLongitude__c, Case_Type__c, certID__c, Check_Info_Not_Same_as_Employee__c, Client__c, Client__r.Current_Start_Date__c, client__r.take_deductible_and_coinsurance_for_all__c, Client_Carrier__c, Client_Carrier__r.Name, Client_Carrier__r.Carrier_Email__c, Client_Carrier__r.state__c, Client__r.logoDirectory__c, Client_facility__c, Client_Name__c, Client__r.name, Clinical_Code_Status__c, Closed_Reason__c, closedDate__c, Closure_Call__c, carrierCaseClosedNotification__c, carrierDepartureNotification__c, CM_submits_for_nico_scheduling__c, cmNotified__c, COE_Hip_Replacement_Approach__c, Companion_Form_Received__c, Confirmation_Packet_Sent_to_Patient__c, ContinineCompleted__c, Continineemailsent__c, coverage_level__c, Converted_Date__c, coeOverride__c, CreatedBy.Id, CreatedBy.Name, CreatedDate, creating_case_id__c, csrTransferred__c, csr_rScore__c, Current_Nicotine_User__c, Current_treatment_of_infusions__c, Daily_Stipend__c, daily_stipend_total__c, Date_advised_of_imaging_requirement__c, Date_of_last_injection__c, Date_Care_Mgmt_Transfers_to_Eligibility__c, Date_Eligibility_is_Checked_with_Client__c, Date_of_Last_Cardiac_Visit__c, Date_of_last_dental_visit__c, Date_of_Last_Joint_Related_Visit__c, Date_of_Last_Primary_Care_Phys_Visit__c, Date_of_Last_Specialist_Visit__c, Date_of_Last_Spine_Related_Visit__c, Date_of_Previous_Joint_Replacement__c, Date_range_of_retesting_begin__c, Date_range_of_retesting_end__c, Date_Range_of_Testing_Begin__c, Date_Range_of_Testing_End__c, Date_of_stroke__c, Date_of_Heart_Problems__c, Date_of_MRI__c, Date_Clinical_Received__c, Date_Clinical_Saved_to_Folder__c, Date_Transitioned_to_the_Carrier__c, Days_in_Hospital__c, Deductible_Met__c, Deductible_Remaining__c, Out_of_Pocket_Met_Individual__c, Out_of_Pocket_Met_Family__c, OOP_Remaining_Individual__c, Deductible_Amount_1__c, Deductible_Amount_2__c, Deductible_Verification_1__c, Deductible_Verification_2__c, deformityofKnee__c, Dental_Problems__c, Departing_Airport_Transportation__c, Departing_Flight_Number__c, Departing_Flight_Time__c, Departure_Date__c, Determination_by_Facility__c, Diabetic__c, Diagnosis__c, Did_you_receive_the_welcome_kit__c, displayAuthNumber__c, Does_the_patient_use_marijuana__c, Does_patient_have_a_skin_condition__c, Do_you_understand_how_the_stipend_works__c, Did_they_tell_you_your_MRI_was_abnormal__c, Driving_Stipend__c, Driving_Stipend_Mileage_Offset__c, Dvt_Date__c, ECEN_Hotel__c, ECEN_Hotel__r.name, Ecen_Procedure__c, Ecen_Procedure__r.name, Ecen_Procedure__r.logoDirectory__c, Effective_Date_of_Medical_Coverage__c, EligibiltyEmailSent__c, Eligibility_Manual_Override__c, Eligible__c, eligiblityNotified__c, Eligibility_Audit_Completed__c, Employee_Best_Time_To_Contact__c, Employee_City__c, Employee_Country__c, Employee_DOB__c, Employee_DOBe__c, Employee_Email_Address__c, Employee_First_Name__c, Employee_Gender__c, Employee_HealthPlan__c, Employee_HealthPlan__r.name, Employee_HealthPlan__r.Annual_Out_of_Pocket_Max_Family__c, Employee_HealthPlan__r.Annual_Out_of_Pocket_Max_Individual__c, Employee_HealthPlan__r.Associate_and_Dependents_Deductible__c, Employee_HealthPlan__r.Associate_Only_Deductible__c, OOP_Remaining_Family__c, Employee_HealthPlan__r.Annual_Out_of_Pocket_Max_Assc_and_Dep__c, Employee_HealthPlan__r.Associate_and_Family_Deductible__c, Employee_Home_Phone__c, EmployeeID__c, Employee_Last_Name__c, Employee_Last_Name_Sort__c, Employee_Mobile__c, Employee_Preferred_Phone__c, Employee_Primary_Health_Plan_ID__c, Employee_Primary_Health_Plan_Name__c, Employee_SSN__c, Employee_State__c, Employee_Street__c, Employee_Type__c, Employee_Work_Phone__c, Employee_Zip_Postal_Code__c, Employee_Insurance_Primary_Not_HMO__c, eNotes__c, Eligibility_Last_Update_Date__c, Eligibility_group__c, Est_Days_to_Arrive__c, Estimated_Arrival__c, Estimated_Departure__c, Expedited_Referral__c, expeditedTravelRequest__c, Explain_trans_to_home_medical_carrier__c, Facility_Travel_Confirmation__c, Fallen_in_the_last_3_months__c, fasttrack_referral__c, fearoffalling__c, Financial_Responsibility_Waiver__c, follow_up_date__c, had_stroke__c, hardstop__c, Has_the_patient_had_a_dental_visit__c, Has_the_patient_had_a_joint_related__c, Has_the_patient_had_a_PCP_visit__c, Have_you_had_an_MRI_of_your_spine__c, HealthPac_Plan_Name__c, Heart_Stents_Date__c, Hepatitis__c, History_of_DVT__c, History_of_Pulmonary_Embolism__c, History_of_Blood_Clotting_Disorder__c, History_of_Heart_Stents__c, Home_Phys_phone_access_to_program_phys__c, Home_Phys_to_manage_care_post_program__c, Home_Transition_Care__c, Hospital_Admit__c, Hospital_Discharge__c, Hotel_Amount__c, Hotel_Check_In_Date__c, Hotel_Checkout_Date__c, Hotel_Name__c, nHotel_Name__c, Hotel_Offset__c, Hotel_Parking__c, Hotel_Rate__c, How_many_injections__c, How_long_ago__c, How_long_was_it_helpful__c, How_long_did_the_patient_take_anti_infl__c, ID_Card_Requested__c, illegal_drugs__c, In_Town_Parking_Taxi__c, In_Town_Travel_Allowance__c, Info_Packet_Sent__c, Information_Packet_Follow_Up_Call__c, Initial_Call_Discussion__c, Initiated_Conservative_Treatment__c, Inpatient_No_Program_Surgery__c, Inpatient_Program_Surgery__c, Is_a_Caregiver_Available__c, isConverted__c, IsDeleted, isRelated__c, jointAssistiveDevices__c, jointKneeBrace__c, jointKneeXrays__c, jointantiInflammatoryMedication__c, jointDateoflastFall__c, jointNumberofTimesFallen__c, Joint_Recommendation_last_12_months__c, Joint_Referral_Facility_Contact__c, Joint_Replacement_First_or_Revision__c, Joint_Previously_Replaced__c, JointOutPatientTherapy__c, jointPToccuredinlast__c, jointPTlength__c, jointInjections__c, jointDiscontinuedReason__c, Kidney_Problems__c, Last_appointment_date__c, Last_Patient_contact__c, Language_Spoken__c, LastActivityDate, LastModifiedById, LastModifiedBy.Name, LastModifiedDate, Lead15ID__c, Legacy_Created_By__c, legacyAuthNumber__c, legacyCreatedDate__c, legacyLastModifiedBy__c, legacyLastModifiedDate__c, Length_of_Episode__c, Length_of_Episode_Additional_Days__c, Listed_as_Program_Covered_Procedure__c, Loaded_into_HealthPac__c, Major_Co_morbidities_Treatment_Date__c, Medical_Director_Involvement__c, Medical_History_Notes__c, Medical_Records_Received__c, Medicare_As_Secondary_Coverage__c, Medication_for_Pain_Control__c, Member__c, mileage_stipend_total__c, MRSA_Infection__c, mriSurgicalConsult__c, Mileage__c, Mileage_Stipend__c, MRI_Facility_Address__c, MRI_Facility_Name__c, MRI_Facility_Phone__c, nMileage__c, Name, Name_on_Check__c, Nicotine_Quit_Date__c, Nicotine_reTest_Results__c, Nicotine_test_re_scheduled__c, Nicotine_Test_Results__c, Nicotine_Test_Scheduled__c, Nicotine_Type__c, Number_of_Days__c, Number_of_DRG_Codes__c, Number_of_Hotel_Days__c, nReferred_Facility__c, OP_Pre_op_Evaluation__c, openNotification__c, Open_subrogation_claim__c, Open_workers_compensation_case__c, Original_Airfare_Caregiver__c, Original_Airfare_Patient__c, Other_Pertinent_Medical_Info_History__c, Other_Joint_replaced_in_COE_program__c, Other_Major_Co_morbidities_Treatment__c, Outlier_Date__c, Outpatient_evaluation_only__c, Outpatient_Extended_Evaluation__c, Outpatient_Program_Surgery__c, Overview_of_clinical_components_timeline__c, OwnerId, Owner.Name, Owner.email, Paid_Thru_Date__c, Participate_in_physical_therapy__c, Patient_Age__c, Patient_and_Employee_DOB_verified__c, Patient_and_Employee_SSN_verified__c, Patient_Best_Time_To_Contact__c, Patient_BMI__c, Patient_Case__c, Patient_City__c, Patient_Country__c, Patient_DOB__c, Patient_DOBe__c, Patient_Email_Address__c, Patient_First_Name__c, Patient_Gender__c, patientId__c, Patient_had_Heart_Problems__c, Patient_has_Hepatitis__c, Patient_had_Kidney_Problems__c, Patient_had_a_MRSA_Infection__c, Patient_has_had_Rheumatoid_Arthritis__c, Patient_has_had_Dental_Problems__c, Patient_has_Scoliosis__c, Patient_Height_FT__c, Patient_Height_Inches__c, Patient_Home_Phone__c, Patient_Last_Name__c, Patient_Last_Name_Sort__c, Patient_Mobile__c, Patient_Preferred_Phone__c, Patient_SSN__c, Patient_State__c, Patient_Street__c, Patient_Symptoms__c, Patient_Travel_Initiation__c, Patient_Weight__c, Patient_Work_Phone__c, Patient_Zip_Postal_Code__c, PatientNotAccepted__c, Patient_Cleared_to_Travel_Covid__c, Patient_Cleared_to_Travel__c, Patient_Cleared_to_Travel_Reason__c, Patient_COVID_Testing_Results__c, Patient_Declined_Testing__c, Patient_End_Date_of_Testing__c, Patient_Pre_Travel_Test_Scheduled__c, Patient_Start_Date_of_Testing__c, pcpAssist__c, Patient_Travel_Confirmation__c, pendingEligCheck__c, PHI_permission_granted_to__c, Physical_Therapy_Dates_of_Services__c, Physician_Follow_up__c, Plan_of_Care_accepted_at_HDP__c, Prepaid_Card_Additional_Load_Amount__c, Prepaid_Card_Additional_Load_Date__c, Prepaid_Card_CVV__c, Prepaid_Card_Expiration__c, Prepaid_Card_Initial_Load_Amount__c, Prepaid_Card_Initial_Load_Date__c, Prepaid_Card_Second_Load_Amount__c, Prepaid_Card_Second_Load_Date__c, Prepaid_Card_Number__c, Prescriptions_once_you_arrive_in_City__c, Previous_spine_surgery__c, Previously_Referred_Facility__c, Previously_Referred_Date__c, Procedure__c, Procedure_Complexity__c, Program_Authorization_Received__c, Program_Status_Last_Updated__c, Program_Type__c, Proposed_Procedure__c, nProposed_Procedure__c, Proposed_Procedure_Comments__c, Provide_options_to_obtain_form__c, Provide_options_to_obtain_waiver__c, Physician_specialty_recommend_tjr__c, Pulmonary_Embolism_Date__c, Reason__c, Rec_Airport_Parking__c, Rec_Baggage__c, Rec_In_Town_Parking_Taxi__c, Rec_In_Town_Travel_Allowance__c, Rec_Total_Stipend_Amount__c, Rec_Travel_Amount__c, Receipts_for_Tax_Offset__c, Received_Determination_at_HDP__c, Receive_an_injection_into_the_joint__c, Recent_Testing__c, Recent_visit_with_home_phys_prior_to_ref__c, Recommended_Facility__c, Recommended_Facility_Rule__c, Recommended_Facility_Rule__r.name, Recommended_Hip_Replacement_Approach__c, Recommended_Joint_Replacement_Type__c, Record_Locator__c, RecordTypeId, Recordtype.Name, Referral_facility_contact__c, Referral_Source__c, Referral_Type__c, Referred_Surgical_consult__c, Referral_Template__c, Referred_Facility__c, ReferredFacilityAddress__c, Rheumatoid_Arthritis__c, relatedCaseType__c, relatedCreated__c, Related_to_injury_seeking_legal_action__c, Relationship_to_the_Insured__c, Review_pre_post_op_medication_coverage__c, Roundtrip_Mileage__c, Roundtrip_Mileage_to_the_Airport_Station__c, Same_as_Employee_Address__c, Secondary_Coverage__c, seeing_a_provider_to_manage_your_spine__c, Shuttle_Information_Arrival__c, Shuttle_Information_Departure__c, Specific_Spine_Part__c, Stage__c, Status__c, Status_Reason__c, Status_Reason_Sub_List__c, Status_Roll_Up__c, Surgeon_Credentials__c, Surgeon_First_Name__c, Surgeon_Last_Name__c, Surgery_Related_to_Work_Injury__c, Tax_Summary_Sent_To_Client__c, Termination_Date__c, Total_Expenses_Paid__c, Total_Stipend_Amount__c, Total_Taxable_Offset__c, Total_Taxable_Income__c, Total_Joint_Replacement_Recommended__c, Total_Paycard_load__c, Translation_Services_Required__c, transitionReferralAuth__c, Travel_Amount__c, Travel_Needs__c, Travel_Needs_Comments__c, Travel_Notes__c, Travel_Request__c, Travel_Type__c, Treatment_of_dialysis__c, trfCreatedDate__c, Transition_Reason__c, Trip_ID__c, Type_of_Last_Joint_Replacement__c, Type_of_Surgeon__c, Welcome_Packet_Follow_Up_Call__c, Welcome_Packet_Sent__c, What_procedure_was_recommended__c, What_Type_of_Provider__c, X_Ray_Date__c, X_Ray_Facility_Name__c, X_Ray_Facility_Phone__c, X_Ray_Facility_Address__c, Travel_to_COE__c, Driving_Mileage_Reimbursement__c, Airport_Mileage_Reimbursement__c, Roundtrip_Mileage_to_COE__c, Total_Expense__c, Non_Taxable_Total__c, Taxable_Total__c, Additional_Card_Load_Total__c, Patient_Arrival_Date__c, Return_Home_Travel_Date__c, Wheelchair_needed_at_airport__c, Extra_space_for_large_tall_patient__c, Maximum_flight_length__c, Additional_Card_Load_Total_Miles__c, RT_Airport_to_Station_Total_Miles__c, RT_Mileage_to_COE_Total_Miles__c
               from patient_case__c where id = :apexpages.currentpage().getparameters().get('id')];
        oldCaseRecordType = obj.recordtype.name;
        miscMap = new map<string, string>();
        miscMap.put('convertresult', '');
        newCase = obj.clone();
        rtName = obj.ecen_procedure__r.name + ' Digital Physical Therapy';
        newCase.recordtypeid = [select id from recordtype where sObjectType = 'Patient_Case__c' and name = :rtName].id;
        newCase.ecen_procedure__c = [select id from procedure__c where name = :rtName].id;
        newCase.creating_case_id__c = obj.id;
        newCase.isRelated__c = true;
        newCase.eligible__c = 'Yes';
        newCase.status__c = 'Open';
        newCase.status_reason__c = 'New';
        newCase.converted_date__c = null;
        newCase.isConverted__c = false;
        newCase.pendingEligCheck__c = false;
        newCase.Received_Determination_at_HDP__c = null;
        newCase.Caller_relationship_to_the_patient__c = 'COE';
        newCase.caller_name__c = obj.client_facility__r.facility__r.name;
        newCase.actual_arrival__c = null;
        newCase.estimated_arrival__c = null;
        newCase.actual_departure__c = null;
        newCase.estimated_departure__c = null;
        obj.isRelated__c = true;
        obj.relatedCreated__c = true;
        obj.status__c = 'Pended';
        obj.status_reason__c = 'Physical Therapy';
    }

    public void referCase(){
        string result;

        miscMap.put('convertresult', 'false');
        insert newCase;

        try{
            result = convertCase.walmartPreLoader(newCase.id, 'pc');
            if (result == '1') {
                update obj;
                miscMap.put('convertresult', 'true');

                Program_Notes__c pn = new Program_Notes__c();
                pn.type__c = 'Other';
                pn.subject__c = 'Case created';
                pn.Notes__c = 'Vendor Case created. The prior case can be found here: https://' + Environment__c.getInstance().instance__c + '.salesforce.com/' + obj.id;
                pn.patient_case__c = newCase.id;
                insert pn;
            }
            else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result));
                miscMap.put('convertresult', 'false');
                return;
            }
        }catch (exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            miscMap.put('convertresult', 'false');
            return;
        }

        Provider__c[] providers = new Provider__c[] {};
        Provider__c clonedProvider;

        for (Provider__c p : [select City__c, Credentials__c, Email_Address__c, Fax_Number__c, First_Name__c, Full_Name__c, Home_Transition_Care__c, Last_Name__c, Medical_Record_Contact__c, MRC_Fax_Number__c, MRC_Phone__c, mrcRcvd__c, NPI__c, parentid__c, Patient_Case__c, Phone_Number__c, mrcFaxDate__c, ROI_Received_from_Patient__c, ROI_Sent_to_Patient__c, State__c, Street__c, Type__c, Zip_Postal_Code__c from Provider__c where patient_case__c = :obj.id]) {
            clonedProvider = p.clone();
            clonedProvider.patient_case__c = newCase.id;
            providers.add(clonedProvider);
        }
        if (providers.size() > 0) {
            insert providers;
        }

        ftpAttachment__c[] ftpAttachments = new ftpAttachment__c[] {};
        ftpAttachment__c clonedFTP;
        set<id> legacyAttachmentId = new set<id>();

        for (ftpAttachment__c ftpa : [select attachmentID__c, Comments__c, contentType__c, File_Description__c, fileName__c, parentID__c, Patient_Case__c, subDirectory__c from ftpAttachment__c where patient_case__c = :obj.id]) {
            clonedFTP = ftpa.Clone();
            clonedFTP.patient_case__c = newCase.id;
            ftpAttachments.add(clonedFTP);
        }
        insert ftpAttachments;

        Program_Notes__c authNote = new Program_Notes__c();
        try{
            authNote = [select attachment__c, attachID__c, Clinical_Note__c, Type__c, Communication_Type__c, Contact_Type__c, Legacy_Created_By__c, legacyCreatedDate__c, ftpAttachment__c, Initiated__c, isAuthEmailAttach__c, isContinineResults__c, Notes__c, Subject__c from Program_Notes__c where patient_case__c = :obj.id and isAuthEmailAttach__c = true];
            if (authNote.id != null) {
                Program_Notes__c foo = authNote.clone();
                foo.patient_case__c = newCase.id;
                insert foo;
            }
        }catch (exception e) {}

        attachment[] alist = new attachment[] {};
        attachment clonedAttachment;
        for (Attachment a : [select parentid, body, name, contentType from attachment where parentId = :obj.id]) {
            clonedAttachment = new attachment();
            clonedAttachment.body = a.body;
            clonedAttachment.name = a.name;
            clonedAttachment.contentType = a.contentType;
            clonedAttachment.parentId = newCase.id;
            aList.add(clonedAttachment);
        }

        insert alist;
    }

    public selectOption[] getvendorFacilityOpts(){
        selectOption[] referralOpts = new selectOption[] {};
        referralOpts.add(new selectOption('', '--None--'));
        for (client_facility__c cf : [select id, name, procedure__r.name, facility__r.name from client_facility__c where Active__c = true and procedure__r.name =:rtName]) {
            referralOpts.add(new selectOption(cf.id, cf.facility__r.name));
        }
        return referralOpts;
        //return coeSelfAdminPickList.selectOptionList(obj, obj.client__c, obj.ecen_procedure__c, null, coeSelfAdminPickList.referredFacility, obj.nReferred_facility__c, true);
    }
}
