public class coeLogicFields{

    public set<string> logicDisplayFields {get; private set;} 
    public set<string> clientFacilitySetupDisplayFields {get; private set;} 
    
    public coeLogicFields(string procedure){
        logicDisplayFields = new set<string>();
        clientFacilitySetupDisplayFields = new set<string>();
        
        if(procedure=='Oncology'){
            logicDisplayFields.add('Patient_Age__c');
            clientFacilitySetupDisplayFields.add('Patient_Age__c');
        
            logicDisplayFields.add('Carrier_Name__c');
            clientFacilitySetupDisplayFields.add('Carrier_Name__c');
            logicDisplayFields.add('Patient_City__c');
            clientFacilitySetupDisplayFields.add('Patient_City__c');
        
            logicDisplayFields.add('Patient_State__c');
            clientFacilitySetupDisplayFields.add('Patient_State__c');
        
            logicDisplayFields.add('Patient_Zip_Postal_Code__c');
            clientFacilitySetupDisplayFields.add('Patient_Zip_Postal_Code__c');   
            
            logicDisplayFields.add('Patient_Street__c');
            clientFacilitySetupDisplayFields.add('Patient_Street__c');
            
            logicDisplayFields.add('Client__c');
            clientFacilitySetupDisplayFields.add('Client__c');
            
        }else{
           
        
        logicDisplayFields.add('Ecen_Procedure__c');
        clientFacilitySetupDisplayFields.add('Ecen_Procedure__c');
        
        logicDisplayFields.add('Client__c');
        clientFacilitySetupDisplayFields.add('Client__c');
        
        logicDisplayFields.add('nProposed_Procedure__c');
        clientFacilitySetupDisplayFields.add('nProposed_Procedure__c');
        
        logicDisplayFields.add('Patient_Age__c');
        clientFacilitySetupDisplayFields.add('Patient_Age__c');
        
        logicDisplayFields.add('Carrier_Name__c');
        clientFacilitySetupDisplayFields.add('Carrier_Name__c');
        
        logicDisplayFields.add('Current_Nicotine_User__c');
        clientFacilitySetupDisplayFields.add('Current_Nicotine_User__c');
        
        logicDisplayFields.add('Mileage__c');
        clientFacilitySetupDisplayFields.add('Mileage__c');
        
        logicDisplayFields.add('Patient_BMI__c');
        clientFacilitySetupDisplayFields.add('Patient_BMI__c');        

        logicDisplayFields.add('Patient_City__c');
        clientFacilitySetupDisplayFields.add('Patient_City__c');
        
        logicDisplayFields.add('Patient_State__c');
        clientFacilitySetupDisplayFields.add('Patient_State__c');
        
        logicDisplayFields.add('Patient_Zip_Postal_Code__c');
        clientFacilitySetupDisplayFields.add('Patient_Zip_Postal_Code__c');   
        
        logicDisplayFields.add('Patient_has_Scoliosis__c');
        clientFacilitySetupDisplayFields.add('Patient_has_Scoliosis__c');      
        
        clientFacilitySetupDisplayFields.add('Patient_Street__c');
        
        }
         
        
    }     
    /*
    public set<string> getlogicDisplayFields(){
        return logicDisplayFields;
    }

    public set<string> getclientFacilitySetupDisplayFields(){
        return clientFacilitySetupDisplayFields;
    }
    */
}