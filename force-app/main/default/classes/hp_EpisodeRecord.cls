public with sharing class hp_EpisodeRecord extends hp_callWebApp{
    
    public static hpEpisodeStruct sendEpisode(string pln,string pfn,string pdob, string ssn, string uw, string grp, string insId, string sequence, string eventtype, string medcategory, string effectiveDate, string termDate, string admitDate, string thruDate, string approvedDays, string comments, string episodeNumber, string visits){
        
        hp_EpisodeRecord sendEpisode= new hp_EpisodeRecord();
        return sendEpisode.sendTheEpisode(pln,pfn,pdob, ssn, uw, grp, insId, sequence, eventtype, medcategory, effectiveDate, termDate, admitDate, thruDate, approvedDays, comments, episodeNumber, visits);
        
    }
    
    public hpEpisodeStruct sendTheEpisode(string pln,string pfn,string pdob, string ssn, string uw, string grp, string insId, string sequence, string eventtype, string medcategory, string effectiveDate, string termDate, string admitDate, string thruDate, string approvedDays,string comments,string episodeNumber, string visits){
        
        endpoint='episoderoute';
        //search.add('pln:GREEN');
        search.add('pln:'+pln.touppercase());
        
        //search.add('pfn:CYNTHIA');
        search.add('pfn:'+pfn.touppercase());
        
        //search.add('pdob:19600706');
        search.add('pdob:'+pdob);
        
        //search.add('essn:293622129');
        search.add('essn:'+ssn);
        
        //search.add('effdate:20190101');
        search.add('effdate:'+ effectiveDate);
        
        //search.add('termdate:20190110');
        search.add('termdate:'+termDate);
        
        //search.add('uw:034');
        search.add('uw:'+uw);
        
        //search.add('grp:HDP');
        search.add('grp:'+grp);
        
        //search.add('rel:00');
        search.add('rel:'+sequence);
        
        search.add('eventtype:'+ eventtype.touppercase());
        search.add('medcategory:'+ medcategory.touppercase());
        
        //search.add('pinsid:VUHS01167744');
        search.add('pinsid:'+ insId);
        
        search.add('admitDate:'+admitDate);
        
        search.add('thruDate:'+thruDate);
        
        search.add('approvedDays:'+approvedDays);
        
        search.add('comments:'+comments);
        
        search.add('episodeNumber:'+episodeNumber);
        
        search.add('visits:'+visits);
        
        
        search.add('id:EpisodeCall');
        
        jsonBody = jb.eligSearch(search);
        jsonBody = jsonBody.replaceAll('null','');
        if(test.IsRunningTest()){
            result ='{"ResultCode":"0","RecordNumber":"010286087                                                                                                                                                                                                                                                       "}';
        }else{
            result = callWebApp(null);
        }
        hpEpisodeStruct episodeResult = (hpEpisodeStruct)JSON.deserialize(result, hpEpisodeStruct.class); 
        
        return episodeResult;
    
    }
    
}