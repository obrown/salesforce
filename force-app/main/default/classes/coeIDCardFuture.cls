public with sharing class coeIDCardFuture{
    
    @future(callout=true) //Limited to ten records at time
    public static void emailIDCardRequests(set<id> idSet){
        
        Program_Notes__c[] notesList = new Program_Notes__c[]{};
        attachment[] attachList = new attachment[]{};
        patient_case__c[] pcList = new patient_case__c[]{};
        
        Messaging.EmailFileAttachment[] mailAttachments = new Messaging.EmailFileAttachment[]{};
        
        for(patient_case__c obj : [select attachEncrypt__c, 
                                          certID__c,
                                          ID_Card_Requested__c,
                                          patient_first_name__c,
                                          patient_last_name__c,
                                          patient_street__c,
                                          patient_city__c,
                                          patient_state__c,
                                          Patient_Zip_Postal_Code__c from patient_case__c where id in :idSet]){
        
            coeCaseEncryptedData foo = new coeCaseEncryptedData();
            foo.patientName= obj.patient_first_name__c+' '+obj.patient_last_name__c;
            foo.patientStreet= obj.patient_street__c;
            foo.patientCSZ = obj.patient_city__c+', '+obj.patient_state__c+' '+obj.Patient_Zip_Postal_Code__c;
            foo.BID = obj.certID__c;
            foo.id = obj.id;
        
            PageReference coeIDCard = page.coeIDCard;
            coeIDCard.getParameters().put('cases',JSON.serialize(foo));
            
            Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
            
            efa.setFileName(obj.Patient_Last_Name__c+', '+obj.Patient_First_Name__c+' ID Card.pdf');
            efa.setBody(coeIDCard.getContent());
            mailAttachments.add(efa);
            
            Attachment theIdCard = new Attachment(parentId = obj.ID, name=obj.Patient_Last_Name__c+', '+obj.Patient_First_Name__c+' ID Card.pdf', body = efa.getBody()); 
            encryptAttach ea = new encryptAttach(EncodingUtil.base64Decode(obj.attachEncrypt__c));
            theIDCard.body=ea.encryptAttachmentBlob(theIdCard.body);
            attachList.add(theIdCard);
            
            obj.ID_Card_Requested__c= date.today();
            pcList.add(obj);
            
        
        }
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        string[] sendemailto = new string[]{};
        if(utilities.IsRunninginSandbox()){
            sendemailto.add(userInfo.getUserEmail());
            
        }else{
            //sendemailto.add('jhabacko@hdplus.com'); Removed per ticket 62762
            sendemailto.add('memberadvocate@hdplus.com');
            //sendemailto.add('ngreene@hdplus.com');Removed per ticket 62762
        }
        mail.setToAddresses(sendemailto);
        mail.setSubject('ID Card Requests - COE');
        mail.setPlainTextBody('Hello,\n\nPlease print off the attached ID card(s) and return to Member Advocates for mailing.\n\nThank you');
        mail.setFileAttachments(mailAttachments);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});   
        
        insert attachList;
        for(Attachment a : attachList){
        
            Program_Notes__c newNote = new Program_Notes__c(); 
            newNote.patient_case__c = a.parentId;
            newNote.Subject__c = 'ID Card';
            newNote.Notes__c = 'ID Card has been created and sent to admin to print';
            newNote.Type__c = 'Other';
            newNote.Communication_Type__c= 'Email';
            newNote.attachment__c=true;
            newNote.attachID__c= a.id;
            
            notesList.add(newNote);
        
        }
        
        update pcList;
        insert notesList;
        
    }
    
    @future(callout=true) //Limited to ten records at time
    public static void emailOncologyIDCardRequests(set<id> idSet){
        
        for(oncology__c obj : [select Client_Name__c, 
                                      ID_Card_Requested__c,
                                      patient_first_name__c, patient_last_name__c, Patient_Medical_Plan_Number__c, Patient_Address__c, patient_city__c, patient_state__c, Patient_Zip_Code__c
                                      from oncology__c where id in :idSet]){
        
            oncologyWorkflow.requestIDCard(obj);
        
         }  
        
    }
    
    @future(callout=true) //Limited to ten records at time
    public static void emailBariatricIDCardRequests(set<id> idSet){
        
        //Bariatric_Stage__c[] bList = new Bariatric_Stage__c[]{};
        
        //Messaging.EmailFileAttachment[] mailAttachments = new Messaging.EmailFileAttachment[]{};
        system.debug('idSet size '+idSet.size());
        for(Bariatric_Stage__c obj : [select bariatric__r.certID__c, 
                                             bariatric__r.Client__r.name,
                                      Estimated_Arrival__c, Estimated_Departure__c, ID_Card_Requested__c,Patient_First_Name__c,Patient_Last_Name__c,bariatric__r.Referred_Facility__c
                                      from Bariatric_Stage__c where id in :idSet]){
        
            BariatricWorkflow.requestIDCard(obj);
        
         }  
        
    }
    
}