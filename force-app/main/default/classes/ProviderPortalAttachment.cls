@RestResource(urlMapping='/providerPortal/v1/attachment')
global with sharing class ProviderPortalAttachment {

    @HttpPost
    global static string doPost() {
        String caseNumber = RestContext.request.headers.get('CaseNumber');
        String fileName = RestContext.request.headers.get('FileName');
        String subDirectory = RestContext.request.headers.get('Subdirectory');
        string[] search = new string[]{};
        patient_case__c pc = [select id from patient_case__c where name = :caseNumber ];
        ftpAttachment__c ftpAttachment = new ftpAttachment__c(patient_case__c =pc.id);
        ftpAttachment.subDirectory__c= subDirectory ;
        ftpAttachment.fileName__c= fileName;
        fileName= fileName.replaceAll('-', '_');
        fileName= fileName.replaceAll(',', '_');
        fileName= fileName.replaceAll('#', '_');
        fileName= fileName.replaceAll('&', '_');
        ftpAttachment.subDirectory__c = subDirectory ;
        Map<String, String> resultMap = new Map<String, String>();
        try{
            boolean duplicateAttachment =false;
            for(ftpAttachment__c da : [select id,subDirectory__c from ftpAttachment__c where patient_case__c = :pc.id and fileName__c=:fileName]){
                if(da.subDirectory__c==subDirectory){
                    duplicateAttachment=true;
                    break;
                }
            }
            
            if(duplicateAttachment){
              return 'File Uploaded';  
            }
            
        }catch(exception e){
            system.debug(e.getmessage());
            //return 'File Uploaded';
        }    
        
        
        try{
            insert ftpAttachment;
            
            
        }catch(exception e){
            return e.getMessage();
        }
         return 'File Uploaded';
    }
    global class response{
       // blob file;
        string fileName;
        string errorMsg;
        boolean isError;
    }
    @HttpGet
    global static string doGet() {
        string filePath = RestContext.request.headers.get('Filepath');
        string[] rfp = filePath.split('/');
        string fileName = rfp[rfp.size()-1];
        system.debug('fileName: '+fileName);
        
        try{
            id a_id = [select attachmentId__c from ftpAttachment__c where attachmentId__c != null and filename__c =:fileName limit 1].attachmentId__c;
            attachment attachment = [select body, parentID from attachment where name = :fileName  order by createdDate desc limit 1];
            restcontext.response.addheader('Filename', fileName);
            
            patient_case__c pc = [select attachEncrypt__c from patient_case__c where id = :attachment.parentID];
            blob key = EncodingUtil.base64Decode(pc.attachEncrypt__c);
            encryptAttach ea = new encryptAttach(key);
            blob decryptedbody;
            decryptedbody= ea.decryptAttachmentBlob(attachment.body);
                
            return EncodingUtil.base64Encode(decryptedbody);
        }catch(queryexception e){
            //catch silently
        }
        
        if(fileName.contains('ID Card.pdf') || fileName.contains('_Referral.pdf')){
            attachment attachment = [select body, parentID from attachment where name = :fileName  order by createdDate desc limit 1];
            restcontext.response.addheader('Filename', fileName);
            
            patient_case__c pc = [select attachEncrypt__c from patient_case__c where id = :attachment.parentID];
            blob key = EncodingUtil.base64Decode(pc.attachEncrypt__c);
            encryptAttach ea = new encryptAttach(key);
            blob decryptedbody;
            decryptedbody= ea.decryptAttachmentBlob(attachment.body);
                
            return EncodingUtil.base64Encode(decryptedbody);
        }
        
        /*  */
        
        if(fileName.left(2)=='a0'){
            fileName=fileName.right(fileName.length()-19);
        }
        
        http h = new http();
        HttpRequest req = new HttpRequest();
        filepath= filepath.replaceAll(':','%3A').replaceAll(',','%2C').replaceAll(' ','%20');
        system.debug(filepath);
        string url = fileServer__c.getInstance().Ussscorpion__c+'openfile/?filepath='+filepath+'&oid='+ UserInfo.getOrganizationId() + '&sid=' + UserInfo.getSessionId() + '&uid='+ UserInfo.getUserId();
        
        req.setEndPoint(url );
        req.setmethod('GET');
        req.setTimeout(120000);
        HttpResponse res = h.send(req);
        
        try{       
             if(res.getStatusCode()!=200){ 
                 system.debug(res.getStatusCode()+' error');
                 insert new Api_error__c(Status_Code__c=string.valueof(res.getStatusCode()), Message__c=res.getBody().left(200));
                 return null;
             }
        }catch(exception e){
            system.debug(e.getMessage()+' error');
            return null;
        }
        
        string[] fnArray = filepath.split('/');
        
        //return res.getBodyAsBlob();
        restcontext.response.addheader('Filename', fnArray[fnArray.size()-1]);
        return EncodingUtil.base64Encode(res.getBodyAsBlob());
    }

}