public without sharing class ecenBariatricTask{
    
    
     ecenPatientInformation pinfo = new ecenPatientInformation();
     ecenDashBoardStruct dashboard = new ecenDashBoardStruct();
     ecenTaskList TaskList = new ecenTaskList();
        
     public ecenPatientInformation getPInfo(Bariatric_stage__c BariatricStage){
     
         pinfo.dashboard = dashboard;
         pinfo.dashboard.WelcomeMessage = 'Thank you for signing into the Contigo Health application.  In order to use this application, you must have an open case with Contigo Health created/open through your employer.    If you feel you have received this message in error, please call 888-463-3737.';
         pinfo.dashboard.ToDoItemsComplete='0%';
         pinfo.TaskList = TaskList;   
         
          // Patient Tasks ... 1 - complete, 0 - Incomplete, 2 - Not Applicable
            map<string, integer> tasksMap = new map<string, integer>();
            tasksMap.put('referral', 1);
            tasksMap.put('forms', 0);
            tasksMap.put('nicotine', 2);
            tasksMap.put('appointments', 0);
            tasksMap.put('booktravel', 0);
            integer isNicotine=0;
         
         if(BariatricStage.id==null){
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
           }
           return pinfo;
           
        }else{
           ecenDashBoardTimeline.timelineRibbonItem timelineRibbonItem= new ecenDashBoardTimeline.timelineRibbonItem();
           ecenDashBoardTimeline timelineRibbon= new ecenDashBoardTimeline();
           map<integer, ecenDashBoardTimeline.timelineRibbonItem> trMap = new map<integer, ecenDashBoardTimeline.timelineRibbonItem>();
            
            trMap.put(1, new ecenDashBoardTimeline.timelineRibbonItem(1, 0, 'Referral - 1st Step'));
            trMap.put(2, new ecenDashBoardTimeline.timelineRibbonItem(2, 0, 'Treatment Decision'));
            trMap.put(3, new ecenDashBoardTimeline.timelineRibbonItem(3, 0, 'Travel for Care'));
            
            ecenDashBoardStruct.milestone[] graphMilestones= new ecenDashBoardStruct.milestone[]{};
            
            //timelineRibbon && graphRibbon
            
            if(BariatricStage.Bariatric__r.Referral_Date__c!=null){
                timelineRibbonItem = trMap.get(1);
                timelineRibbonItem.Completed=1;
                trMap.put(1, timelineRibbonItem);
            }
            
            if(BariatricStage.recordtype.name=='Pre'){
                if(BariatricStage.Plan_of_Care_Accepted_at_HDP__c!=null){
                    timelineRibbonItem = trMap.get(2);
                    timelineRibbonItem.Completed=1;
                    trMap.put(2, timelineRibbonItem);
                    tasksMap.put('appointments', 1);
                }
            }else if(BariatricStage.recordtype.name=='Global'){
                if(BariatricStage.Plan_of_Care_Accepted_at_HDP__c!=null && 
                   BariatricStage.Caregiver_Form_Received__c!=null &&
                   BariatricStage.X2nd_Nutritional_Counseling_Date__c !=null &&
                   BariatricStage.Determination_Received_at_HDP__c !=null &&
                   BariatricStage.Patient_Accepted_for_Weight_Loss_Surgery__c != '' &&
                   BariatricStage.Caregiver_Form_Received__c !=null
                   ){
                    timelineRibbonItem = trMap.get(2); 
                    timelineRibbonItem.Completed=1;
                    trMap.put(2, timelineRibbonItem);
                    tasksMap.put('appointments', 1);
                }
            }
                        
            
            
            timelineRibbon.items.add(trMap.get(1));
            timelineRibbon.items.add(trMap.get(2));
            
            //Welcome Message
            if(BariatricStage.recordtype.name=='Global'){
                dashboard.WelcomeMessage= 'Welcome to the HDP Bariatric Application for your surgery!';
            }else{
                dashboard.WelcomeMessage= 'Welcome to the HDP Bariatric Application for your evaluation!';
            }
            
            
            if(BariatricStage.Program_Forms_Received_Date__c!=null && BariatricStage.Caregiver_Form_Received__c!=null){
                tasksMap.put('forms', 1); //forms completed
            }
               
            system.debug('nico '+ BariatricStage.bariatric__r.current_nicotine_user__c);   
            if(BariatricStage.bariatric__r.current_nicotine_user__c=='Yes'){
                tasksMap.put('nicotine', 0);
                isNicotine=1;
                Bariatric_Nicotine__c[] bsNico = new Bariatric_Nicotine__c[]{};
                bsNico = [select Estimated_Arrival__c,Date_of_Test__c,Test_Results__c from Bariatric_Nicotine__c where bariatric_stage__c = :BariatricStage.id order by createdDate desc limit 1];
                
                if(!bsNico.isEmpty()){
                    if(bsNico[0].Estimated_Arrival__c != null && bsNico[0].Date_of_Test__c != null && bsNico[0].Test_Results__c == 'Negative for Nicotine'){
                        tasksMap.put('nicotine', 1);
                    }
                }
                
            }
            
            if(isNicotine==1){
                
                if(tasksMap.get('nicotine')==0){
                    TaskList.Pending.Add(new ecenTaskList.Task('Complete Nicotine Testing', 3, BariatricStage.Bariatric__r.patient_first_name__c+' '+BariatricStage.Bariatric__r.patient_last_name__c));
                }else{
                    TaskList.Completed.Add(new ecenTaskList.Task('Nicotine Testing Completed', 3, BariatricStage.Bariatric__r.patient_first_name__c+' '+BariatricStage.Bariatric__r.patient_last_name__c + ' ('+utilities.formatUSdate(date.valueof(BariatricStage.bariatric__r.referral_date__c))+')'));
                    
                }
            
            }
            
            if(BariatricStage.booking_reference_number__c!=null){
               tasksMap.put('booktravel', 1);
            }
            
            if(tasksMap.get('referral')==0){
                TaskList.Pending.add(new ecenTaskList.Task('Facility Referral', 1, 'HDP'));         
            }else{
                TaskList.Completed.add(new ecenTaskList.Task('Referred to ' + BariatricStage.bariatric__r.client_facility__r.facility__r.name, 1, 'HDP ('+utilities.formatUSdate(date.valueof(BariatricStage.bariatric__r.referral_date__c))+')'));
                
            }          
          
            if(tasksMap.get('forms')==0){
                TaskList.Pending.add(new ecenTaskList.Task('Complete HDP Forms', 2, null)); 
            }else{
                date d = BariatricStage.Program_Forms_Received_Date__c;
                if(BariatricStage.Program_Forms_Received_Date__c>d){
                    d=BariatricStage.Caregiver_Form_Received__c;
                }
                TaskList.Completed.add(new ecenTaskList.Task('HDP Forms Completed', 2, BariatricStage.Bariatric__r.patient_first_name__c+' '+BariatricStage.Bariatric__r.patient_last_name__c + ' ('+utilities.formatUSdate(d)+')'));
            }
            
            
            if(tasksMap.get('appointments')==0){
                TaskList.Pending.Add(new ecenTaskList.Task('Confirm appointment Dates', 3, 'HDP')); 
            }else{
                TaskList.Completed.Add(new ecenTaskList.Task('Appointments Confirmed', 3, 'HDP ('+utilities.formatUSdate(date.valueof(BariatricStage.bariatric__r.referral_date__c))+')'));
                
            }
           
            if(tasksMap.get('booktravel')==0){
                TaskList.Pending.Add(new ecenTaskList.Task('Book Travel with American Express', 4, BariatricStage.Bariatric__r.patient_first_name__c+' '+BariatricStage.Bariatric__r.patient_last_name__c));  
            }else{
                TaskList.Completed.Add(new ecenTaskList.Task('Travel Booked with American Express', 4, BariatricStage.Bariatric__r.patient_first_name__c+' '+BariatricStage.Bariatric__r.patient_last_name__c+ ' ('+utilities.formatUSdate(date.valueof(BariatricStage.bariatric__r.referral_date__c))+')'));
                
                
            }
           
            integer toDoItemsComplete;
            
            if(TaskList.Pending.isEmpty()){
                toDoItemsComplete = 100;
                
                timelineRibbonItem = trMap.get(3);
                timelineRibbonItem.Completed=1;
                trMap.put(3, timelineRibbonItem);
                
            
                
            }else{
                decimal pnd = TaskList.Pending.size();
                toDoItemsComplete =  100-integer.valueof(pnd.divide(TaskList.Pending.size()+TaskList.Completed.size(),1)*100);
            }
            
            timelineRibbon.items.add(trMap.get(3));
            
            dashboard.ToDoItemsComplete = ToDoItemsComplete+'%';
            
            //End setting Dashboard
            
            //Set todo sortOrder list 
             
            if(!TaskList.Pending.isEmpty()){
                for(integer i=0; i<TaskList.Pending.size(); i++){
                    TaskList.pending[i].sortOrder = i+1;
                
                }
            
            }
            
            for(integer i=0; i<TaskList.Completed.size(); i++){
                    TaskList.Completed[i].sortOrder = i+1;
                
            }
            
            pinfo.dashboard = dashboard; 
            pinfo.TaskList = TaskList;
            pinfo.timelineRibbon = timelineRibbon;
            if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
            }
        }
            
         return pInfo;
     
     }
     
}