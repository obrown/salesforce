public without sharing class PopHealthTestData {
    public Client__c client;
    public phm_Patient__c patient;
    public Utilization_Management_Clinical_Code__c umCC;
    public Utilization_Management_Clinician__c umClinician;
    public Case_Management_Clinician__c cmClinician;
      //public Utilization_Management_Denial_Letter__c umDenialLetter;
    public Utilization_Management_Note__c umNote;
    public Utilization_Management_Provider__c umProvider;
    public Utilization_Management__c umRecord;
    public Client_Facility__c clientFacility;
    public Facility__c facility;
    public Procedure__c ecenProcedure;
    sObject[] sObjects = new sObject[] {};
    public boolean patientError;
    public Set<integer> fieldKeys;
    public Patient_Management_Note__c pmNote;
    public Case_Management_Clinical_Code__c cmclinicalCode;
    public boolean showCMHistory;
    public string patientCommunicationFilter;

    public PopHealthTestData(){
    }

    public void loadData(string clientName, string facilityName){
        client = new Client__c(name = clientName);
        sObjects.add(client);

        ecenProcedure = new Procedure__c(name = 'Oncology');
        sObjects.add(ecenProcedure);

        facility = new Facility__c(name = facilityName);
        facility.Facility_Address__c = '2500 S Power Rd';
        facility.Facility_City__c = 'Mesa';
        facility.Facility_State__c = 'AZ';
        facility.Facility_Zip__c = '85205';
        sObjects.add(facility);

        insert sObjects;
    }

    public Utilization_Management_Clinician__c  addClinician(id UtilizationManagement){
        Utilization_Management_Clinician__c umClinician = new Utilization_Management_Clinician__c(name = 'Unit.test2');

        umClinician.City__c = 'CA';
        umClinician.Contact_Name__c = 'Lenox';
        umClinician.Credentials__c = 'MD';
        umClinician.Contact_Phone_Number__c = '480-555-5050';
        umClinician.Credentials__c = 'MD';
        umClinician.First_Name__c = 'James';
        umClinician.Last_Name__c = 'Jones';
        umClinician.Network_Status__c = 'In network';
        umClinician.State__c = 'CA';
        umClinician.Street__c = '333 E Main';
        umClinician.Zip_Code__c = '85209';
        umClinician.Utilization_Management__c = UtilizationManagement;

        return umClinician;
    }

      //added 8-19-21 ---------------start--------------------------------------
    public Case_Management_Clinician__c addcmClinician(
        id caseManagement
        ) {
        Case_Management_Clinician__c cmClinician = new Case_Management_Clinician__c(
            name = 'Unit.test2'
            );

        cmClinician.City__c = 'CA';
        cmClinician.Contact_Name__c = 'Lenox';
        cmClinician.Credentials__c = 'MD';
        cmClinician.Contact_Phone_Number__c = '480-555-5050';
        cmClinician.Credentials__c = 'MD';
        cmClinician.First_Name__c = 'James';
        cmClinician.Last_Name__c = 'Jones';
        cmClinician.Network_Status__c = 'In network';
        cmClinician.State__c = 'CA';
        cmClinician.Street__c = '333 E Main';
        cmClinician.Zip_Code__c = '85209';
        cmClinician.Case_Management__c = caseManagement;

        return cmClinician;
    }
    //8-19-21 ---------------------end----------------------------------failed fiest run 8-19-21

    public phm_Patient__c completePHIntake(phm_Patient__c patient1){
        //phm_Patient__c p = patient;

        phm_Patient__c patient = new phm_Patient__c();

        //patient.Patient_Employer__c = [select id from client__c where underwriter__c = '502' limit 1].id;

        patient.Patient_Preferred_Name__c = 'Test';
        patient.Patient_First_Name__c = 'Unit';
        patient.Patient_Last_Name__c = 'Test';
        patient.effective_date__c = date.today().addDays(-90);

        patient.Patient_Email_Address__c = '123@gmail.com';
        patient.Cert__c = 'Unit.Test';
        patient.patient_date_of_birth__c = '01/01/1980';
        patient.Date_of_Death__c = null;
        patient.Source_of_Date_or_Death__c = 'N/A';
        patient.Phone__c = '480-909-9999';
        patient.Phone_Type__c = 'Work';
        patient.Patient_Time_Zone__c = 'Eastern';
        patient.Preferred_Call_Time__c = '8a-12n';
        patient.Address_Type__c = 'Home';
        patient.Address__c = '123 E Unit Test';
        patient.City__c = 'Mesa';
        patient.State__c = 'AZ';
        patient.Zip__c = '85205';

        patient.Address2__c = '123 E Unit Test';
        patient.City2__c = 'Mesa';
        patient.State2__c = 'AZ';
        patient.Zip2__c = '85205';

        patient.Address3__c = '123 E Unit Test';
        patient.City3__c = 'Mesa';
        patient.State3__c = 'AZ';
        patient.Zip3__c = '85205';

        patient.Address4__c = '123 E Unit Test';
        patient.City4__c = 'Mesa';
        patient.State4__c = 'AZ';
        patient.Zip4__c = '85205';

        patient.Preferred_Phone__c = true;
        patient.patientKey__c = '123';
        insert patient;

        return patient;
    }

    public Utilization_Management__c completeUMInpatient(Utilization_Management__c umRecord){
        umRecord.HealthPac_Case_Number__c = 'New Case';
        umRecord.Comment_Line_for_Claims__c = 'test';
        umRecord.Facility_Name__c = 'Banner';
        umRecord.Facility_Type__c = 'Hospital';
        umRecord.Facility_Network_Status__c = 'In-network';
        umRecord.Facility_Contact_Name__c = 'Sam Sneed';
        umRecord.Facility_Contact_Phone__c = '480-555-5959';
        umRecord.Facility_Street__c = '2500 S Power RD';
        umRecord.Facility_City__c = 'Mesa';
        umRecord.Facility_State__c = 'AZ';
        umRecord.Facility_Zip_Code__c = '85129';
        umRecord.Event_Type__c = 'MED';
        umRecord.Med_Cat__c = 'Transplant';
        umRecord.Admission_Source__c = 'Direct';
        umRecord.Admission_Date__c = system.today() + 1;
        umRecord.Admission_Date__c = system.today() + 1;
        umRecord.Discharge_Date__c = system.today() + 5;
        umRecord.Approved_Through_Date__c = system.today() + 6;
        umRecord.Approved_Days__c = 6;
        umRecord.Next_Review_Date__c = system.today() + 7;
        umRecord.Quality_Codes__c = 'DCL';
        umRecord.Nurses__c= 'Davidson Lauri';

        upsert umRecord;

        return umRecord;
    }

      //8-19-21 update ---------------------start-----------------------------------------
    public Case_Management__c completeCMPatient(
        Case_Management__c cmRecord
        ) {
        cmRecord.Engagement__c = 'Yes';
        cmRecord.Referral_Source__c = 'Account Manager';
        cmRecord.MDC__c = 'Accident / Injury';
        cmRecord.MDC_Sub_Category__c = 'NA';
        cmRecord.Cost_Driver__c = 'Complex Condition';
        cmRecord.HCC__c = 'Actual';
        cmRecord.Expected_Cost__c = 'Continued Costs';
        cmRecord.Identification_Date__c = system.today();
        cmRecord.Case_Manager__c = 'Alicia Deere';
        cmRecord.Case_Status__c = 'Open';
        cmRecord.Case_Sub_status__c = 'In Process';
        cmRecord.Benefit_Year_Plan_Year__c = '1/1/2021';
        cmRecord.Cost_Containment__c = 'Coordination of Provider Services;Education on Benefits / Network;Disease, Condition, TX Plan Education;Patient Coaching / Support;Referral for OON Negotiations;Referral to Other Programs / Benefit Service;Steerage to In-Network Provider';
        upsert cmRecord;

        return cmRecord;
    }

    //8-19-21 update --------------------end-------------------------------------------------

    /*
     * public Utilization_Management__c completeUMOutpatient(Utilization_Management__c umRecord){
     *
     *  umRecord.HealthPac_Case_Number__c ='New Case';
     *  umRecord.Comment_Line_for_Claims__c = 'test';
     *  umRecord.Provider_Name__c= 'Banner';
     *  umRecord.Provider_type__c= 'Hospital';
     *  umRecord.Provider_Network_Status__c= 'In-network';
     *  umRecord.Provider_Contact_Name__c= 'Sam Sneed';
     *  umRecord.Provider_Contact_Phone__c= '480-555-5959';
     *  umRecord.Provider_Street__c = '2500 S Power RD';
     *  umRecord.Facility_City__c= 'Mesa';
     *  umRecord.Facility_State__c = 'AZ';
     *  umRecord.Facility_Zip_Code__c= '85129';
     *  umRecord.Event_Type__c = 'MED';
     *  umRecord.Admission_Source__c = 'Direct';
     *  umRecord.Admission_Date__c = system.today()+1;
     *  umRecord.Discharge_Date__c = system.today()+5;
     *  umRecord.Approved_Through_Date__c = system.today()+6;
     *  umRecord.Approved_Days__c = 5;
     *  umRecord.Visits__c = 1;
     *  umRecord.Next_Review_Date__c = system.today()+7;
     *  umRecord.Med_Cat__c = 'Transplant';
     *  umRecord.Quality_Codes__c = 'DCL';
     *
     *  insert umRecord;
     *
     *  return umRecord;
     * }
     */
    public Client_facility__c addClientFacility(id clientid, id prodecure, id facility){
        clientFacility = new Client_facility__c(Client__c = clientid, Facility__c = Facility, Procedure__c = prodecure);
        insert clientFacility;

        return clientFacility;
    }

    public boolean addCPTCodes() {
        CPT_Code__c[] cptCodes = new List<CPT_Code__c> {};
        cptCodes.add(new CPT_Code__c(name = 'cpta - cpta', description__c = 'cpta'));
        cptCodes.add(new CPT_Code__c(name = 'cptb - cptb', description__c = 'cptb'));

        return true;
    }

    public boolean addICDCodes() {
        ICD10__c[] cptCodes = new List<ICD10__c> {};
        cptCodes.add(new ICD10__c(name = 'icda', description__c = 'icda'));
        cptCodes.add(new ICD10__c(name = 'icdb', description__c = 'icdb'));

        return true;
    }

    public boolean addcmICDCodes() {
        Case_Management_Clinical_Code__c [] cptCodes = new List<Case_Management_Clinical_Code__c > {};
        cptCodes.add(new Case_Management_Clinical_Code__c(name = 'icdc', description__c = 'icdc'));
        cptCodes.add(new Case_Management_Clinical_Code__c(name = 'icdd', description__c = 'icdd'));

        return true;
    }
}