@isTest()
private class PaginatedsObjectListTest{
    
    
    static testMethod void myUnitTest() {
    
        sObject[] foo = new sObject[]{};
    
        for(integer i=0;i<99;i++){
            sObject f;
            foo.add(f);
        }
        
        PaginatedsObjectList pol = new PaginatedsObjectList();
        pol.setSoList(foo);
        pol.setpageSize(5);
        ApexPages.CurrentPage().getParameters().put('pageSize','5');
        pol.setpageSizeVF();
        pol.setPageParam(1);
        sObject[] viewList = pol.getviewList();
        
        system.assert(pol.getPage()==1);
        pol.gethasnext();
        pol.gethasPrevious();
        
        ApexPages.CurrentPage().getParameters().put('pageNum',string.valueof(pol.getNumOfPages().size()));
        pol.setPageVF();
        
        viewList = pol.getviewList();
        pol.setSoList(new sObject[]{});
        pol.getNumOfPages();
        pol.seeAll();
        pol.getNumOfPages();
        
    }
    
}