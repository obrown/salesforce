public without sharing class coeSelfAdminPickList{
    
    public static final string referredFacility = 'Referred_Facility__c';
    public static final string hotelName = 'Hotel_Name__c';
    public static final string employeeHealthplanName = 'Employee_Primary_Health_Plan_Name__c';
    public static final string carrierName = 'Client_Carrier__c';
    
    public static final string[] selfAdminList = new string[]{coeSelfAdminPickList.referredFacility, coeSelfAdminPickList.hotelName,coeSelfAdminPickList.employeeHealthplanName, coeSelfAdminPickList.carrierName};
    
    public static selectOption[] selectOptionList(sObject patientCase, string clientId, string procedureId, string cfId, string fieldName, string fieldValue, boolean active){
        map<string, selectOption[]> testMap;
        coeSelfAdminPickList csap = new coeSelfAdminPickList();
        
        if(clientId==null && procedureId==null){
            return new selectOption[]{new selectOption('','--None--')};
        }
        
        if(fieldName==coeSelfAdminPickList.referredFacility){
            return coeClientFacilities.facilitySelectOptions(clientId, procedureId,fieldValue, active);
        
        }else if(fieldName==coeSelfAdminPickList.hotelName){
            return coeClientHotelOptions.hotelselectOptions(cfId, fieldValue);
            
        }else if(fieldName==coeSelfAdminPickList.carrierName && patientCase.get('patient_state__c')!=null){
           return coeCarrierAndHealthPlans.carriersPickList(clientId, fieldValue,string.valueof(patientCase.get('patient_state__c')));
            
        } else if(fieldName==coeSelfAdminPickList.employeeHealthplanName){
            return coeCarrierAndHealthPlans.employeeHealthPlanPickList(clientId, string.valueof(patientCase.get(coeSelfAdminPickList.carrierName)));
            
        }      
        
        return new selectOption[]{};
    }
    
    
}