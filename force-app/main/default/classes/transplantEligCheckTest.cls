/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class transplantEligCheckTest {

    static testMethod void testInit(){
        
        Patient_Case__c testP = new Patient_Case__c(
        client_name__c = 'Walmart',
        recordtypeid = [select id from recordtype where name like 'Walmart%' and isactive = true limit 1].id,
        employee_first_name__c = 'James',
        employee_last_name__c = 'Martin',
        employee_dobe__c = '1980-02-22',
        employee_street__c = '123 E Main St',
        employee_city__c = 'Mesa',
        Employee_State__c = 'AZ',
        employee_zip_postal_code__c = '85297',
        employee_SSN__c = '123456789',
        relationship_to_the_insured__c = 'Spouse',
        patient_First_Name__c = 'Jane',
        patient_Last_Name__c = 'Martin',
        patient_dobe__c = '1980-02-22',
        patient_SSN__c = '987654321',
        actual_departure__c = date.today(),
        createHP__c=true);
        
        insert testP; 
        
        Patient_Case__c testP1 = testP.clone();
        testP1.client_name__c = 'Lowes';
        testP1.recordtypeid = [select id from recordtype where name like 'Lowes%' and isactive = true limit 1].id;
        insert testP1;
        
    }
    
    
}