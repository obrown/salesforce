@isTest(seeAllData=true)
private class substanceDisorderTest{
    
     static EmployeeContacts__c employeeContact(boolean saveRecord){
         EmployeeContacts__c employeeContact = new EmployeeContacts__c();
         employeeContact.FirstName__c = 'John';
         employeeContact.LastName__c = 'Test';
         employeeContact.PrimaryLanguageSpoken__c='English';
         employeeContact.dob__c = date.today().addYears(-30);
         employeeContact.ssn__c = '123456789';
         employeeContact.gender__c = 'Male';
         
         employeeContact.StreetAddress__c='123 E Main St';
         employeeContact.City__c='Mesa';
         employeeContact.State__c='AZ';
         employeeContact.ZipCode__c ='85212';
         
         employeeContact.MobilePhoneNumber__c='(480) 555-1212';
         employeeContact.HomePhoneNumber__c ='(480) 555-1212';
         employeeContact.WorkPhoneNumber__c='(480) 555-1212';
            
         employeeContact.EmployeeRelationShip__c = [select id from EmployeeRelationShip__c where name='Employee'].id;
         if(saveRecord){
             insert employeeContact;
             system.assert(employeeContact.id!=null);
             
             RelatedtoEmployee__c rtc = new RelatedtoEmployee__c();
             rtc.EmployeeContactID__c = employeeContact.id;
             insert rtc;
         }
         
         return employeeContact;
     }
    
     static patient_case__c supCase(){
         
         /* This should be refactored so that we dont have to use the seeAllData parameter */
         
         recordType rt = [select id from RecordType where name = 'Substance Disorder' and SobjectType='Patient_Case__c'];
         client_facility__c cf = [select procedure__c,client__c from client_facility__c where client__r.name='Lowes' and procedure__r.name='Substance Disorder' limit 1];
         
         patient_case__c pc = new patient_case__c(recordTypeId=rt.id,Client__c=cf.Client__c,ecen_procedure__c=cf.procedure__c,Client_Facility__c=cf.id);
         pc.patient_first_name__c = 'John';
         pc.patient_last_name__c = 'Test';
         pc.employee_first_name__c = 'John';
         pc.employee_last_name__c = 'Test';
         pc.patient_gender__c='Male';
         pc.Employee_gender__c='Male';
         pc.Patient_DOBe__c = string.valueof(date.today().addYears(-30));
         pc.Caller_Name__c= 'Test';
         pc.Callback_Number__c= '(480) 555-1212';
         pc.Callback_Type__c= 'Home';
         pc.Language_Spoken__c= 'English';
         pc.Initial_Call_Discussion__c = 'Test';
         
         pc.Relationship_to_the_Insured__c = 'Patient is Insured';
         
         pc.employee_dob__c=date.today().addYears(-30);
         
         pc.employee_street__c='123 E Main St';
         pc.employee_City__c='Mesa';
         pc.employee_State__c='AZ';
         
         pc.patient_street__c='123 E Main St';
         pc.patient_City__c='Mesa';
         pc.patient_State__c='AZ';
         
         pc.patient_mobile__c='(480) 555-1212'; 
         pc.patient_email_address__c='none'; 
         
         insert pc;
         system.debug(pc.id);
         system.assert(pc.id!=null);
         return pc;
     }

     static testMethod void existingCase(){
         patient_case__c pc = substanceDisorderTest.supCase();
         pc.employeeID__c= substanceDisorderTest.employeeContact(true).id;
         ApexPages.CurrentPage().getParameters().put('id',pc.id);
         substanceDisorderController substanceDisorderController = new substanceDisorderController();
         
     }
     
     static testMethod void newCase(){
         
         
         recordType rt = [select id from RecordType where name = 'Substance Disorder' and SobjectType='Patient_Case__c'];
         client_facility__c cf = [select procedure__c,client__c from client_facility__c where client__r.name='Lowes' and procedure__r.name='Substance Disorder' and active__c = true limit 1];
         
         ApexPages.CurrentPage().getParameters().put('client',cf.client__c);
         ApexPages.CurrentPage().getParameters().put('procedure',cf.procedure__c);
         
         substanceDisorderController substanceDisorderController = new substanceDisorderController();
         substanceDisorderController.newEmployeeContact();
         substanceDisorderController.employeeContact = substanceDisorderTest.employeeContact(false);
         substanceDisorderController.saveEmployeeContactModal();
         
         system.assert(substanceDisorderController.obj.employeeid__c!=null);
         
     }
     
     static testMethod void flightTesting(){
         
         patient_case__c supCase = substanceDisorderTest.supCase();
         ApexPages.CurrentPage().getParameters().put('cid',supCase.id);
         memberTrips__c mt = new memberTrips__c();
         ApexPages.StandardController controller = new ApexPages.StandardController(mt);
         
         memberTripExt memberTripExt= new memberTripExt(controller); 
         memberTripExt.newFlight();
         
         /* Set the airLineTrip__c record data */
         memberTripExt.flightTrip.airLineTrip.ConfirmationNumber__c='123456';
         memberTripExt.saveFlight();
         
         ApexPages.CurrentPage().getParameters().put('airlineTripId', memberTripExt.flightTrip.airLineTrip.id);
         
         memberTripExt.getFlightInformation();
         memberTripExt.flightTrip.getFlightAdjInformation(null, memberTripExt.flightTrip.airLineTrip.id);
         memberTripExt.flightTrip.saveFlightAdjustment();
         
         system.assert(memberTripExt.flightTrip.airLineAdjTrip.id!=null);
         
         ApexPages.CurrentPage().getParameters().put('airlineAdjTripId',memberTripExt.flightTrip.airLineAdjTrip.id);
         
         memberTripExt.flightTrip.getFlightAdjInformation(memberTripExt.flightTrip.airLineAdjTrip.id, null);
         memberTripExt.deleteAirlineAdjTrip();
         memberTripExt.saveFlightandNew();
         memberTripExt.saveFlight();
         
         system.assert(memberTripExt.flightTrip.airLineTrip.id!=null);
         
         ApexPages.CurrentPage().getParameters().put('airlineTripId',memberTripExt.flightTrip.airLineTrip.id);
         
         memberTripExt.deleteAirlineTrip();
         memberTripExt.save();

     }
     
     static testMethod void hotelTesting(){
         
         patient_case__c supCase = substanceDisorderTest.supCase();
         supCase.employeeID__c= substanceDisorderTest.employeeContact(true).id;
         update supCase;
         ApexPages.CurrentPage().getParameters().put('cid',supCase.id);
         memberTrips__c mt = new memberTrips__c();
         ApexPages.StandardController controller = new ApexPages.StandardController(mt);
         memberTripExt memberTripExt= new memberTripExt(controller); 
         
          memberTripExt.newHotel();
         
         /* Set the hotelTrip__c record data */
         memberTripExt.hotelTrip.hotelTrip.CheckInDate__c=date.today().addDays(2);
         memberTripExt.hotelTrip.hotelContacts[0].isChecked=true;
         memberTripExt.saveHotel();
         memberTripExt.cancelHotel();
         memberTripExt.getHotelTrip();
         
         ApexPages.CurrentPage().getParameters().put('hotelTripId',memberTripExt.hotelTrip.hotelTrip.id);
         memberTripExt.deleteHotelTrip();
        
         
     }
     
     static testMethod void substanceDisorderControllerTesting(){
         patient_case__c supCase = substanceDisorderTest.supCase();
         
         ApexPages.CurrentPage().getParameters().put('cid',supCase.id);
         
         memberTrips__c mt = new memberTrips__c();
         ApexPages.StandardController controller = new ApexPages.StandardController(mt);
         
         memberTripExt memberTripExt= new memberTripExt(controller); 
         
         memberTripExt.memberTrip.Admit_to_Hospital__c = date.today();
         memberTripExt.memberTrip.Discharge_from_Hospital__c= date.today().addDays(2);
         
         memberTripExt.memberTrip.ActualArrivalDate__c = date.today();
         memberTripExt.memberTrip.ActualDepartureDate__c = date.today().addDays(2);
         
         memberTripExt.save();
         
         memberTripExt.newFlight();
         memberTripExt.flightTrip.airLineTrip.ConfirmationNumber__c='123';
         memberTripExt.flightTrip.airLineTrip.ArrivalFlightDate__c=date.today();
         memberTripExt.flightTrip.airLineTrip.ArrivalFlightDateTime__c=datetime.now();
         memberTripExt.flightTrip.airLineTrip.DepartureFlightDate__c=date.today().addDays(2);
         memberTripExt.flightTrip.airLineTrip.DepartureFlightDateTime__c=datetime.now().addDays(2);
         memberTripExt.flightTrip.airLineTrip.EmployeeContact__c=supCase.employeeId__c;
         memberTripExt.flightTrip.airLineTrip.BaggageFees__c=50;
         
         memberTripExt.saveFlight();
         
         memberTripExt.flightTrip.getFlightAdjInformation(null, memberTripExt.flightTrip.airLineTrip.id);
         
         memberTripExt.flightTrip.airLineAdjTrip.Flight_Type__c='Arriving';
         memberTripExt.flightTrip.airLineAdjTrip.Adjustment_Amount__c=10;
         memberTripExt.flightTrip.saveFlightAdjustment();
         
         ApexPages.CurrentPage().getParameters().put('client', supCase.client__c);
         ApexPages.CurrentPage().getParameters().put('procedure', supCase.ecen_procedure__c);
         
         substanceDisorderController sdController = new substanceDisorderController();
         
         
         supCase.employeeID__c= substanceDisorderTest.employeeContact(true).id;
         ApexPages.CurrentPage().getParameters().put('employeeId',supCase.employeeId__c);
         sdController.createCasefromIntake();
         
         ApexPages.CurrentPage().getParameters().put('client', supCase.client__c);
         ApexPages.CurrentPage().getParameters().put('procedure', supCase.ecen_procedure__c);
         ApexPages.CurrentPage().getParameters().put('id',supCase.id);
         sdController = new substanceDisorderController();
         
         sdController.checkdup();
         sdController.getemployeeHealthPlanOpts();
         sdController.getcarrierOpts();
         sdController.getreferredFacilityOpts();
         sdController.updatePatientInfo();
         
         //sdController.obj.employee_healthplan__c=sdController.getemployeeHealthPlanOpts()[0].getValue();
         
         
         sdController.inlineSave();
         sdController.obj.Carrier_Nurse_Name__c='Test';
         
         sdController.inlineSave();
         
         sdController.getnotes();
         sdController.setStatusAndStatusReason();
         sdController.getcTypeItemsPOC();
         sdController.getcodeOptionsPOC();
         sdController.transfertoCM();
         sdController.submitForEligibility();
         
         sdController.newEmployeeContact();
         sdController.employeeContact.FirstName__c ='John';
         sdController.employeeContact.LastName__c ='Test';
         sdController.employeeContact.gender__c='Male';
         sdController.employeeContact.SSN__c='111223333';
         sdController.employeeContact.DOB__c=date.today().addyears(-30);
         sdController.employeeContact.EmployeeRelationShip__c=[select id from EmployeeRelationShip__c where name = 'Employee' order by createdDate asc limit 1].id; 
         
         sdController.saveEmployeeContact();
         
         ApexPages.currentPage().getParameters().put('ecId', sdController.employeeContact.id);
         sdController.getEmployeeContact();
         
        
         
         sdController.lastnameSearch='Test';
         sdController.firstnameSearch='Test';
         sdController.searchForNewIntake();
         
         sdController.editPaycard();
         sdController.paycard .Load_Amount__c=100;
         sdController.savePaycard();
         
         ApexPages.currentPage().getParameters().put('paycardID', sdController.paycard.id);
         sdController.editPaycard();
         
         ApexPages.currentPage().getParameters().put('paycardID', sdController.paycard.id);
         sdController.deletePaycard();
         
         
         sdController.editHotelPaycard();
         //sdController.paycard .Load_Amount__c=100;
         sdController.saveHotelPaycard();
         
         ApexPages.currentPage().getParameters().put('hotelPaycardID', sdController.hotelpaycard.id);
         sdController.editHotelPaycard();
         
         ApexPages.currentPage().getParameters().put('hotelPaycardID', sdController.hotelpaycard.id);
         sdController.deleteHotelPaycard();
         
         ApexPages.currentPage().getParameters().put('flightId', memberTripExt.flightTrip.airlineTrip.id);
         ApexPages.currentPage().getParameters().put('memberExt', memberTripExt.memberTrip.id);
         
         sdController.addAdjustment();
         
         sdController.saveFlightAdjustment();
         
         ApexPages.currentPage().getParameters().put('flightId', memberTripExt.flightTrip.airlineTrip.id);
         ApexPages.currentPage().getParameters().put('memberExt', memberTripExt.memberTrip.id);
         ApexPages.currentPage().getParameters().put('adjId', memberTripExt.flightTrip.airLineAdjTrip.id);
         
         sdController.getFlightAdjInformation();
         
         sdController.getLanguage();
         sdController.getRecordtypeName();
         
         sdController.newAOC();
         sdController.addAOC();
         
         ApexPages.currentPage().getParameters().put('theAHid', sdController.theAddOnCharge.id);
         sdController.editAOC();
         
         ApexPages.currentPage().getParameters().put('theAHid', sdController.theAddOnCharge.id);
         sdController.deleteAOC();
         
         sdController.getlangaugeText();
         sdController.getCallerName();
         sdController.setCallerName('test');
         
         sdController.getIsErrorMessage();
         sdController.setMessage('Test');
         
         sdController.getshowcancel();
         sdController.recalculateOffsets();
         
         EmployeeContacts__c spouseContact = employeeContact(false);
         spouseContact.EmployeeRelationShip__c=[select id from EmployeeRelationShip__c where name = 'Spouse' order by createdDate asc limit 1].id;
         insert spouseContact; 
         system.debug(sdController.employeeContact.id);
         RelatedtoEmployee__c RelatedtoEmployee = new RelatedtoEmployee__c(RelatedEmployeeID__c=supCase.employeeID__c, EmployeeContactID__c= spouseContact.id);
         insert RelatedtoEmployee ;
         
         ApexPages.CurrentPage().getParameters().put('employeeId', spouseContact.id);
         sdController.createCasefromIntake();
         
         sdController.getobjJSON();
         sdController.getDeleteTask();
         sdController.getisDelete();
         sdController.getshowRecords();
         sdController.getmiscMap();
         sdController.getstatusReason();
         sdController.getstatusReasonSub();
         sdController.mycancel();
     }
     
       

}