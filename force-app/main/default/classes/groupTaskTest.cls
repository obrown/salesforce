/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class groupTaskTest {

    static testMethod void myUnitTest() {
    	ID recordtypeid = [select id from Recordtype where isActive = true and name = 'Walmart Spine' limit 1].id;
        Patient_Case__c pc = new Patient_Case__c();
        pc.bid__c = '12356789';
        pc.client_name__c = 'Walmart';
        pc.Caller_Name__c = 'John Smith';
        pc.RecordTypeId = recordtypeid;
        pc.Status__c = 'Open';
        pc.Relationship_to_the_Insured__c = 'Patient Is Insured';
        pc.employee_first_name__c = 'John';
        pc.employee_last_name__c = 'Test';
        
        insert pc;
        user[] ulist = new user[]{};
        set<id> uset = new set<id>();
             Group gp = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = 'Transplant'];
              for(GroupMember g: [select userOrGroupId from GroupMember where groupid = :gp.id]){
                  uset.add(g.userOrGroupId);
              }
        ulist = [select id from User where isActive = true and id IN :uset];
        Task[] temptlist = new task[]{};
        
        for(User u : ulist){
                
            Task task = new Task(
                          ownerid=u.id,
                          whatid=pc.id,
                          Subject= pc.Patient_last_name__c + ', ' + pc.Patient_first_name__c + ' Test');
                          
            temptlist.add(task);
                          
        }
         
        insert temptlist;
        Task task = temptlist[0];
        task.Subject = 'Test Change';
        update task;
        
        delete temptlist[1];
           
    }
}