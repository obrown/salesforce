/**
 * This class contains unit tests for validating the behavior of the Ecen Self-Administration Application
 * failed fiest run 8-19-21
 */

@isTest
private class EcenSelfAdminTest {
    static testMethod void testClientFacilityPage() {
        Client__c client = createEcenRecords.createClient('Walmart', '502', 'WAL');

        system.assert (client.id != null);

        Procedure__c procedure = createEcenRecords.createProcedure('Joint');

        system.assert (procedure.id != null);

        Facility__c Facility = createEcenRecords.createFacility('Scripps', '9888 Genesee Ave', 'La Jolla', 'CA', '92037', '32.8851544', '-117.2255383');

        system.assert (facility.id != null);
        facilityFuture.fetchLatLong(new set<id> { facility.id });

        Client_Facility__c CF = createEcenRecords.createClientFacility(client.id, procedure.id, facility.id);

        system.assert (CF.id != null);

        Carrier__c carrier = createEcenRecords.createCarrier('BCBS', '', '', '', '');

        system.assert (carrier.id != null);

        Client_Carrier__c clientCarrier = createEcenRecords.createClientCarrier(carrier.id, client.id);

        system.assert (clientCarrier.id != null);

        Client_Employee_Healthplan__c ceh = new Client_Employee_Healthplan__c(name = 'HRA', Client_Carrier__c = clientCarrier.id);
        insert ceh;

        ApexPages.StandardController clientFacilitySC = new ApexPages.StandardController(CF);
        clientFacilityOverrideExt cfext = new clientFacilityOverrideExt(clientFacilitySC);

        /* test adding removing authorization email addresses */

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emailcom');
        cfext.addAuthorizationEmail();
        system.assert (cfext.authContactList.isEmpty());

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@email.com');
        cfext.addAuthorizationEmail();
        system.assert (cfext.authContactList.contains('email@email.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.addAuthorizationEmail();

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.removeAuthorizationEmail();
        system.assert (!cfext.authContactList.contains('email@emaill.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.removeAuthorizationEmail();

        /* test adding removing referral email addresses */

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emailcom');
        cfext.addReferralEmail();
        system.assert (cfext.referralContactList.isEmpty());

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@email.com');
        cfext.addReferralEmail();
        system.assert (cfext.referralContactList.contains('email@email.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.addReferralEmail();

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.removeReferralEmail();
        system.assert (!cfext.referralContactList.contains('email@emaill.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.removeReferralEmail();

        /* test adding and deleting plan codes */

        cfext.newPlanCode();
        cfext.planCode.HealthPac_Plan_Code__c = 'UNITTEST';
        cfext.planCode.Case_Plan_Name__c = cfext.planSelectOptions [0].getValue();
        cfext.saveHealthpacPlanCode();
        system.assert (!cfext.getplanCodes().isEmpty());

        cfext.newPlanCode();
        cfext.planCode.HealthPac_Plan_Code__c = 'UNITTESA';
        cfext.planCode.Case_Plan_Name__c = cfext.planSelectOptions [0].getValue();
        cfext.saveHealthpacPlanCode();

        ApexPages.currentPage().getParameters().put('planCodeId', cfext.planCode.id);
        cfext.getPlanCode();
        cfext.planCode.HealthPac_Plan_Code__c = '124NotAVlideCode';
        cfext.saveHealthpacPlanCode();
        system.assert (cfext.isError == true);

        ApexPages.currentPage().getParameters().put('planCodeId', cfext.planCode.id);
        cfext.DeleteHealthpacPlanCode();
        system.assert (cfext.getplanCodes().size() == 1);

        /* test adding and deleting physicians */

        cfext.newProvider();
        cfext.provider.Physician__c = createEcenRecords.createPhysician().id;
        cfext.saveProvider();
        system.assert (cfext.physicians.size() == 1);

        ApexPages.CurrentPage().getParameters().put('providerId', cfext.physicians [0].id);
        cfext.deleteProvider();
        system.assert (cfext.physicians.isEmpty());

        cfext.newProvider();
        cfext.provider.Physician__c = createEcenRecords.createPhysician().id;
        cfext.saveProvider();

        ApexPages.CurrentPage().getParameters().put('providerId', cfext.physicians [0].id);
        cfext.getClientProvider();
        cfext.saveProvider();

        cfext.newEcenHotel();
        cfext.hotel.hotel__c = createEcenRecords.createHotel('Best Western').id;
        cfext.saveEcnHotel();
        system.assert (cfext.gethotels().size() == 1);

        ApexPages.CurrentPage().getParameters().put('ecenHotelId', cfext.hotel.id);
        cfext.getEcnHotel();

        cfext.hotel.hotel__c = createEcenRecords.createHotel('Days Inn').id;
        cfext.saveEcnHotel();

        ApexPages.CurrentPage().getParameters().put('ecenHotelId', cfext.hotel.id);
        cfext.deleteHotel();

        /* test adding and deleting recommened facility rules*/

        cfext.newRecommendedFacilityRule();

        Recommended_Facility_Rule__c rfr1 = new Recommended_Facility_Rule__c(name = 'Unit.Test', Client_Facility__c = CF.id, Priority__c = '1');
        insert rfr1;

        insert new Recommendation_Logic__c(Recommended_Facility_Rule__c = rfr1.id, name = 'whatever', Field_Label__c = 'Patient_State__c', Field_Value__c = 'AZ', Display_Value__c = 'AZ', Field_Logic__c = 'equals', Field_Type__c = 'PICKLIST');

        Recommended_Facility_Rule__c rfr2 = new Recommended_Facility_Rule__c(name = 'Unit.Test2', Client_Facility__c = CF.id, Priority__c = '2');
        insert rfr2;

        insert new Recommendation_Logic__c(Recommended_Facility_Rule__c = rfr2.id, name = 'whatever', Field_Label__c = 'Patient_State__c', Field_Value__c = 'NM', Display_Value__c = 'NMZ', Field_Logic__c = 'equals', Field_Type__c = 'PICKLIST');

          //Reload the page to get the loadRFR map to fire
        cfext = new clientFacilityOverrideExt(clientFacilitySC);
        system.assert (cfext.rfMapSize > 0);

        ApexPages.CurrentPage().getParameters().put('newPriority', '1');
        ApexPages.CurrentPage().getParameters().put('previousPriority', '2');
        ApexPages.CurrentPage().getParameters().put('ruleID', rfr2.id);
        cfext.updateRule();
        rfr2 = [select Priority__c from Recommended_Facility_Rule__c where id =:rfr2.id];
        system.assert (rfr2.Priority__c == '1');

        ApexPages.CurrentPage().getParameters().put('newPriority', 'z');
        ApexPages.CurrentPage().getParameters().put('previousPriority', '1');
        ApexPages.CurrentPage().getParameters().put('ruleID', rfr1.id);
        cfext.updateRule();
        rfr1 = [select Priority__c from Recommended_Facility_Rule__c where id =:rfr1.id];
        system.assert (rfr1.Priority__c == '2');
    }

    static testMethod void testclientCarrierExt() {
        Client__c client = createEcenRecords.createClient('Walmart', '502', 'WAL');

        system.assert (client.id != null);

        Procedure__c procedure = createEcenRecords.createProcedure('Joint');

        system.assert (procedure.id != null);

        Facility__c Facility = createEcenRecords.createFacility('Scripps', '9888 Genesee Ave', 'La Jolla', 'CA', '92037', '32.8851544', '-117.2255383');

        system.assert (facility.id != null);

        Client_Facility__c CF = createEcenRecords.createClientFacility(client.id, procedure.id, facility.id);

        system.assert (CF.id != null);

        Carrier__c carrier = createEcenRecords.createCarrier('BCBS', '', '', '', '');

        system.assert (carrier.id != null);

        Client_Carrier__c clientCarrier = createEcenRecords.createClientCarrier(carrier.id, client.id);

        system.assert (clientCarrier.id != null);

        ApexPages.StandardController clientCarrierSC = new ApexPages.StandardController(clientCarrier);

        clientCarrierExt clientCarrierExt = new clientCarrierExt(clientCarrierSC);

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@email.com');
        clientCarrierExt.addccEmail();
        system.assert (clientCarrierExt.carrierContactList.contains('email@email.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'emaill@email.com');
        clientCarrierExt.addccEmail();

        ApexPages.currentPage().getParameters().put('emailAddress', 'emaill@email.com');
        clientCarrierExt.removeccEmail();
        system.assert (!clientCarrierExt.carrierContactList.contains('email@emaill.com'));

        //8-13-21 update------------------start---------------------------------------

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@email.com');
        clientCarrierExt.addccaEmail();
        system.assert (clientCarrierExt.carrierContactList.contains('email@email.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'emaill@email.com');
        clientCarrierExt.addccaEmail();

        ApexPages.currentPage().getParameters().put('emailAddress', 'emaill@email.com');
        clientCarrierExt.removeccaEmail();
        system.assert (!clientCarrierExt.carrierContactList.contains('email@emaill.com'));
        //8-13-21 update------------------end---------------------------------------

        clientCarrierExt.newEmployeePlan();
        clientCarrierExt.employeePlan.name = 'deletingLater';
        clientCarrierExt.employeePlanSave();
        system.assert (clientCarrierExt.employeePlans.size() == 1);
          //8-13-21 added --------------start------------------------------------------------
        ApexPages.currentPage().getParameters().put('planId', clientCarrierExt.employeePlans [0].id);
        clientCarrierExt.editEmployeePlan();
          //8-13-21 update------------------end---------------------------------------
        ApexPages.currentPage().getParameters().put('planId', clientCarrierExt.employeePlans [0].id);
        clientCarrierExt.deleteEmployeePlan();
        system.assert (clientCarrierExt.employeePlans.isEmpty());

        clientCarrierExt.newEmployeePlan();
        clientCarrierExt.employeePlan.name = 'HRA';
        clientCarrierExt.employeePlanSave();
    }

    static testMethod void testclientSetupTesting() {
        Client__c client = createEcenRecords.createClient('Walmart', '502', 'WAL');

        system.assert (client.id != null);

        Procedure__c procedure = createEcenRecords.createProcedure('Joint');

        system.assert (procedure.id != null);

        Facility__c Facility = createEcenRecords.createFacility('Scripps', '9888 Genesee Ave', 'La Jolla', 'CA', '92037', '32.8851544', '-117.2255383');

        system.assert (facility.id != null);

        Client_Facility__c CF = createEcenRecords.createClientFacility(client.id, procedure.id, facility.id);

        system.assert (CF.id != null);

        Carrier__c carrier = createEcenRecords.createCarrier('BCBS', '', '', '', '');

        system.assert (carrier.id != null);

        Client_Carrier__c clientCarrier = createEcenRecords.createClientCarrier(carrier.id, client.id);

        system.assert (clientCarrier.id != null);

        Client_Employee_Healthplan__c ceh = new Client_Employee_Healthplan__c(name = 'HRA', Client_Carrier__c = clientCarrier.id);
        insert ceh;

        ApexPages.StandardController clientFacilitySC = new ApexPages.StandardController(CF);
        clientFacilityOverrideExt cfext = new clientFacilityOverrideExt(clientFacilitySC);

        /* test adding removing authorization email addresses */

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emailcom');
        cfext.addAuthorizationEmail();
        system.assert (cfext.authContactList.isEmpty());

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@email.com');
        cfext.addAuthorizationEmail();
        system.assert (cfext.authContactList.contains('email@email.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.addAuthorizationEmail();

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.removeAuthorizationEmail();
        system.assert (!cfext.authContactList.contains('email@emaill.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.removeAuthorizationEmail();

        /* test adding removing referral email addresses */

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emailcom');
        cfext.addReferralEmail();
        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.addReferralEmail();
        system.assert (!cfext.referralContactList.isEmpty());

        /* test adding and deleting plan codes */

        cfext.newPlanCode();
        cfext.planCode.HealthPac_Plan_Code__c = 'UNITTEST';
        cfext.planCode.Case_Plan_Name__c = 'HRA';
        cfext.saveHealthpacPlanCode();
        system.assert (!cfext.getplanCodes().isEmpty());

        /* test adding and deleting physicians */

        cfext.newProvider();
        cfext.provider.Physician__c = createEcenRecords.createPhysician().id;
        cfext.saveProvider();
        system.assert (cfext.physicians.size() == 1);

        cfext.newEcenHotel();
        cfext.hotel.hotel__c = createEcenRecords.createHotel('Best Western').id;
        cfext.saveEcnHotel();
        system.assert (cfext.gethotels().size() == 1);

        cfext.newEcenHotel();
        cfext.hotel.hotel__c = createEcenRecords.createHotel('Days Inn').id;
        cfext.saveEcnHotel();

        /* test adding and deleting recommened facility rules*/

        cfext.newRecommendedFacilityRule();

        Recommended_Facility_Rule__c rfr1 = new Recommended_Facility_Rule__c(name = 'Unit.Test', Client_Facility__c = CF.id, Priority__c = '1', L1Order__c = 'OR', L1Rule_Order__c = 'OR');
        insert rfr1;

        insert new Recommendation_Logic__c(Recommended_Facility_Rule__c = rfr1.id, name = 'whatever', Field_Label__c = 'Patient State', Field_Name__c = 'Patient_State__c', Field_Value__c = 'AZ', Display_Value__c = 'AZ', Field_Logic__c = 'equals', Field_Type__c = 'PICKLIST', Level__c = 1.0);
        insert new Recommendation_Logic__c(Recommended_Facility_Rule__c = rfr1.id, name = 'whatever', Field_Label__c = 'Patient State', Field_Name__c = 'Patient_State__c', Field_Value__c = 'AZ', Display_Value__c = 'NM', Field_Logic__c = 'equals', Field_Type__c = 'PICKLIST', Level__c = 1.0);

        Recommended_Facility_Rule__c rfr2 = new Recommended_Facility_Rule__c(name = 'Unit.Test2', Client_Facility__c = CF.id, Priority__c = '2');
        insert rfr2;

        insert new Recommendation_Logic__c(Recommended_Facility_Rule__c = rfr2.id, name = 'whatever', Field_Name__c = 'CaseLatitudeLongitude__c', Field_Value__c = '200', Field_Type__c = 'MILEAGEDISTANCE', Display_Value__c = 'Distance Less than', Field_Logic__c = 'less than', Level__c = 1.0);

        patient_case__c patientCase = createEcenRecords.createPatientCase('Walmart', 'Joint');

        patientCase = (patient_case__c)createEcenRecords.setFormulafield(patientCase, 'Referred_Facility__c', 'Scripps');

        coeClientProviders.providerSelectOptions(patientCase, cf.id);
        coeClientProviders.providerStringList(patientCase, cf.id);
        patientCase.referred_facility__c = null;
        coeClientProviders.providerSelectOptions(patientCase, cf.id);
        coeClientProviders.providerStringList(patientCase, cf.id);

        coeCarrierAndHealthPlans.carriersPickList(client.id, clientCarrier.id, 'OH');
        coeCarrierAndHealthPlans.employeeHealthPlanPickList(client.id, clientCarrier.id);

        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, null, coeSelfAdminPickList.hotelName, 'UNIT.TEST', false);
        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, coeSelfAdminPickList.hotelName, 'UNIT.TEST', false);

        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, coeSelfAdminPickList.referredFacility, 'UNIT.TEST', false);
        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, coeSelfAdminPickList.carrierName, 'UNIT.TEST', false);

        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, coeSelfAdminPickList.employeeHealthplanName, 'UNIT.TEST', false);
        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, 'not an option', 'UNIT.TEST', false);

        coeSelfAdminPickList.selectOptionList(patientCase, null, null, cf.id, coeSelfAdminPickList.employeeHealthplanName, 'UNIT.TEST', false);

        ApexPages.CurrentPage().getParameters().put('cf', CF.id);
        clientSetupTesting cst = new clientSetupTesting();

        cst.getfacilities();
        cst.client = '';
        cst.procedure = '';
    }

    static testMethod void testRecommendedFacilitySetupTesting() {
        Client__c client = createEcenRecords.createClient('Walmart', '502', 'WAL');

        system.assert (client.id != null);

        Procedure__c procedure = createEcenRecords.createProcedure('Joint');

        system.assert (procedure.id != null);

        Facility__c Facility = createEcenRecords.createFacility('Scripps', '9888 Genesee Ave', 'La Jolla', 'CA', '92037', '32.8851544', '-117.2255383');

        system.assert (facility.id != null);

        Client_Facility__c CF = createEcenRecords.createClientFacility(client.id, procedure.id, facility.id);

        system.assert (CF.id != null);

        Carrier__c carrier = createEcenRecords.createCarrier('BCBS', '', '', '', '');

        system.assert (carrier.id != null);

        Client_Carrier__c clientCarrier = createEcenRecords.createClientCarrier(carrier.id, client.id);

        system.assert (clientCarrier.id != null);

        Client_Employee_Healthplan__c ceh = new Client_Employee_Healthplan__c(name = 'HRA', Client_Carrier__c = clientCarrier.id);
        insert ceh;

        ApexPages.StandardController clientFacilitySC = new ApexPages.StandardController(CF);
        clientFacilityOverrideExt cfext = new clientFacilityOverrideExt(clientFacilitySC);

        /* test adding removing authorization email addresses */

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emailcom');
        cfext.addAuthorizationEmail();
        system.assert (cfext.authContactList.isEmpty());

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@email.com');
        cfext.addAuthorizationEmail();
        system.assert (cfext.authContactList.contains('email@email.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.addAuthorizationEmail();

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.removeAuthorizationEmail();
        system.assert (!cfext.authContactList.contains('email@emaill.com'));

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.removeAuthorizationEmail();

        /* test adding removing referral email addresses */

        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emailcom');
        cfext.addReferralEmail();
        ApexPages.currentPage().getParameters().put('emailAddress', 'email@emaill.com');
        cfext.addReferralEmail();
        system.assert (!cfext.referralContactList.isEmpty());

        /* test adding and deleting plan codes */

        cfext.newPlanCode();
        cfext.planCode.HealthPac_Plan_Code__c = 'UNITTEST';
        cfext.planCode.Case_Plan_Name__c = 'HRA';
        cfext.saveHealthpacPlanCode();
        system.assert (!cfext.getplanCodes().isEmpty());

        /* test adding and deleting physicians */

        cfext.newProvider();
        cfext.provider.Physician__c = createEcenRecords.createPhysician().id;
        cfext.saveProvider();
        system.assert (cfext.physicians.size() == 1);

        cfext.newEcenHotel();
        cfext.hotel.hotel__c = createEcenRecords.createHotel('Best Western').id;
        cfext.saveEcnHotel();
        system.assert (cfext.gethotels().size() == 1);

        cfext.newEcenHotel();
        cfext.hotel.hotel__c = createEcenRecords.createHotel('Days Inn').id;
        cfext.saveEcnHotel();

        /* test adding and deleting recommened facility rules*/

        cfext.newRecommendedFacilityRule();

        Recommended_Facility_Rule__c rfr1 = new Recommended_Facility_Rule__c(name = 'Unit.Test', Client_Facility__c = CF.id, Priority__c = '1', L1Order__c = 'OR', L1Rule_Order__c = 'OR');
        insert rfr1;

        insert new Recommendation_Logic__c(Recommended_Facility_Rule__c = rfr1.id, name = 'whatever', Field_Label__c = 'Patient State', Field_Name__c = 'Patient_State__c', Field_Value__c = 'AZ', Display_Value__c = 'AZ', Field_Logic__c = 'equals', Field_Type__c = 'PICKLIST', Level__c = 1.0);
        insert new Recommendation_Logic__c(Recommended_Facility_Rule__c = rfr1.id, name = 'whatever', Field_Label__c = 'Patient State', Field_Name__c = 'Patient_State__c', Field_Value__c = 'AZ', Display_Value__c = 'NM', Field_Logic__c = 'equals', Field_Type__c = 'PICKLIST', Level__c = 1.0);

        Recommended_Facility_Rule__c rfr2 = new Recommended_Facility_Rule__c(name = 'Unit.Test2', Client_Facility__c = CF.id, Priority__c = '2');
        insert rfr2;

        insert new Recommendation_Logic__c(Recommended_Facility_Rule__c = rfr2.id, name = 'whatever', Field_Name__c = 'CaseLatitudeLongitude__c', Field_Value__c = '200', Field_Type__c = 'MILEAGEDISTANCE', Display_Value__c = 'Distance Less than', Field_Logic__c = 'less than', Level__c = 1.0);

        ApexPages.currentPage().getParameters().put('cf', CF.id);
        coeRecommendedScenarioTesting cst = new coeRecommendedScenarioTesting();

        cst.searchActiveRulesOnly = false;
        cst.selectedField = 'Client__c';
        cst.setfieldValues();
        cst.getfieldValues();
        cst.getfieldType();
        cst.fieldValue = client.id;
        cst.addField();

        cst.selectedField = 'Ecen_Procedure__c';
        cst.setfieldValues();
        cst.getfieldValues();
        cst.getfieldType();
        cst.fieldValue = procedure.id;
        cst.addField();

        cst.selectedField = 'Patient_State__c';
        cst.setfieldValues();
        cst.getfieldValues();
        cst.getfieldType();
        cst.fieldValue = 'NY';
        cst.addField();

        cst.selectedField = 'CaseLatitudeLongitude__c';
        cst.setfieldValues();
        cst.getfieldValues();
        cst.getfieldType();
        cst.fieldValue = '40.9677777,-76.6053472';
        cst.addField();
        cst.searchActiveRulesOnly = false;
        cst.runScenario();

        delete rfr2;

        rfr2 = new Recommended_Facility_Rule__c(name = 'Unit.Test2', Client_Facility__c = CF.id, Priority__c = '2');
        insert rfr2;

        insert new Recommendation_Logic__c(Recommended_Facility_Rule__c = rfr2.id, name = 'whatever', Field_Name__c = 'CaseLatitudeLongitude__c', Field_Value__c = '200', Field_Type__c = 'MILEAGEDISTANCE', Display_Value__c = 'Distance Less than', Field_Logic__c = 'greater than', Level__c = 1.0);

        cst.runScenario();

        patient_case__c patientCase = createEcenRecords.createPatientCase('Walmart', 'Joint');

        patientCase = (patient_case__c)createEcenRecords.setFormulafield(patientCase, 'Referred_Facility__c', 'Scripps');

        cst.searchCaseName = patientCase.Name.right(3);
        cst.searchforCase();

        cst.searchCaseName = patientCase.Name.right(8);
        cst.searchforCase();

        cst.getcustomFieldList();
        cst.getpatientCase();

        coeClientProviders.providerSelectOptions(patientCase, cf.id);
        coeClientProviders.providerStringList(patientCase, cf.id);
        patientCase.referred_facility__c = null;
        coeClientProviders.providerSelectOptions(patientCase, cf.id);
        coeClientProviders.providerStringList(patientCase, cf.id);

        coeCarrierAndHealthPlans.carriersPickList(client.id, clientCarrier.id, 'OH');
        coeCarrierAndHealthPlans.employeeHealthPlanPickList(client.id, clientCarrier.id);

        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, null, coeSelfAdminPickList.hotelName, 'UNIT.TEST', false);
        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, coeSelfAdminPickList.hotelName, 'UNIT.TEST', false);

        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, coeSelfAdminPickList.referredFacility, 'UNIT.TEST', false);
        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, coeSelfAdminPickList.carrierName, 'UNIT.TEST', false);

        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, coeSelfAdminPickList.employeeHealthplanName, 'UNIT.TEST', false);
        coeSelfAdminPickList.selectOptionList(patientCase, client.id, procedure.id, cf.id, 'not an option', 'UNIT.TEST', false);

        coeSelfAdminPickList.selectOptionList(patientCase, null, null, cf.id, coeSelfAdminPickList.employeeHealthplanName, 'UNIT.TEST', false);
    }

    static testMethod void testRecommendedFacilityRuleExt() {
        Client__c client = createEcenRecords.createClient('Walmart', '502', 'WAL');

        system.assert (client.id != null);

        Procedure__c procedure = createEcenRecords.createProcedure('Joint');

        system.assert (procedure.id != null);

        Facility__c Facility = createEcenRecords.createFacility('Scripps', '9888 Genesee Ave', 'La Jolla', 'CA', '92037', '32.8851544', '-117.2255383');

        system.assert (facility.id != null);

        Client_Facility__c CF = createEcenRecords.createClientFacility(client.id, procedure.id, facility.id);

        system.assert (CF.id != null);

        Recommended_Facility_Rule__c rfr = new Recommended_Facility_Rule__c(Client_Facility__c = cf.id, name = 'State is AZ');
        insert rfr;

        ApexPages.StandardController rfrSC = new ApexPages.StandardController(rfr);
        recommendedFacilityRuleExt rfrext = new recommendedFacilityRuleExt(rfrSC);

        rfrext.setfieldType();
        rfrext.newLogicRule();
        rfrext.getPatientCaseFields();
        rfrext.setfieldType();

        rfrext.logic.Field_name__c = 'Patient_State__c';
        rfrext.logic.Field_Label__c = 'Patient State';
        rfrext.setfieldType();
        rfrext.getfieldType();
        rfrext.getfieldValues();
        rfrext.logic.Field_Value__c = 'AZ';
        rfrext.logic.Field_Logic__c = 'equals';
        rfrext.logic.level__c = 1;

        rfrext.saveLogic();
        rfrext.deleteLogic();

        rfrext.logic.Field_name__c = 'Referred_Facility__c';
        rfrext.logic.Field_Label__c = 'Referred Facility';
        rfrext.setfieldType();
        rfrext.getfieldType();
        rfrext.getfieldValues();

        rfrext.logic.Field_name__c = 'Mileage__c';
        rfrext.logic.Field_Label__c = 'Mileage';
        rfrext.logic.level__c = 1;
        rfrext.setfieldType();
        rfrext.getfieldType();
        rfrext.getfieldValues();
        rfrext.logic.Field_value__c = '200';
        rfrext.logic.Field_Logic__c = 'less than or equal';

        rfrext.saveLogic();
        rfrext.newLogicRule();

        rfrext.logic.Field_name__c = 'CaseLatitudeLongitude__c';
        rfrext.logic.Field_Label__c = 'Distance to Scripps';
        rfrext.setfieldType();
        rfrext.getfieldType();
        rfrext.getfieldValues();
        rfrext.logic.level__c = 1;
        rfrext.logic.Field_Logic__c = 'less than';
        rfrext.logic.Field_value__c = '32.8851544,-117.2255383';
        rfrext.saveLogic();

        rfrext = new recommendedFacilityRuleExt(rfrSC);

        ApexPages.CurrentPage().getParameters().put('logicId', rfrext.logicListMap.get('1') [0].id);
        rfrext.loadLogic();
        rfrext.cloneOverride();

        for (string s : rfrext.logicListMap.keyset()) {
            for (Recommendation_Logic__c rl: rfrext.logicListMap.get(s)) {
                if (rl.field_value__c == 'AZ') {
                    ApexPages.CurrentPage().getParameters().put('newLevel', 'z');
                    ApexPages.CurrentPage().getParameters().put('logicId', rl.id);
                    ApexPages.CurrentPage().getParameters().put('previousLevel', string.valueof(rl.level__c));
                    rfrext.updateLogicLevel();
                    break;
                }
            }
        }
    }
}