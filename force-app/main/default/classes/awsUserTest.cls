/**
* This class contains unit tests for validating the behavior of the AWS User Trigger
*/
@isTest()
private class awsUserTest {
    
    static testMethod void testAWSuser(){
    
        coeTestData coeTestData = new coeTestData();
        id facilityID = coeTestData.createFacility(null);
        AWS_User__c awsUser = new AWS_User__c();
        awsUser.Email__c = userInfo.getUserEmail(); 
        awsUser.Facility__c=facilityID ;
        awsUser.Auth_User__c =userInfo.getUserId(); 
        awsUser.name = awsUser.Email__c;
        awsUser.FirstName__c = 'Unit';
        awsUser.LastName__c = 'Test';
        awsUser.User_supplied_Facility__c ='Unit.Test';
        awsUser.active__c=true;
        
        insert awsUser;
        system.assert(awsUser.id!=null, true);
        
        awsUser.active__c=false;
        update awsUser;
        
        awsUser.active__c=true;
        update awsUser;
        
    }
}