public without sharing class claimsAuditorAssignment{

    static map<string, id> auditorMap = new map<string, id>();
    
    public static id getAssignment(id client, string type){
        
        if(auditorMap.keyset().size()==0){
            for(Claims_Auditor__c ca : [select Audit_Type__c,Claims_Client__c from Claims_Auditor__c where Audit_Type__c != '']){
                auditorMap.put(ca.Claims_Client__c+ca.Audit_Type__c, ca.id);
            }
        }
        
        return auditorMap.get(client+type);
    
    }

}