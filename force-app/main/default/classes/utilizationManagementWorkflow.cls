public with sharing class utilizationManagementWorkflow {
    @future
    public static void lettersPrinted(set<string> theIds){
        Utilization_Management_Denial_Letter__c[] umdl = [select Print_Date__c from Utilization_Management_Denial_Letter__c where id in :theIds];
        for (Utilization_Management_Denial_Letter__c u: umdl) {
            u.Print_Date__c = date.today();
        }

        update umdl;
    }

    @future(callout = true)
    public static void sendUM(set<id> theIds){
        string dummyDate = '11111111';
        date umDenyDate = date.valueof('1980-01-01');

        Utilization_Management__c[] umList = new Utilization_Management__c[] {};
        umList = [select patient__r.Effective_Date__c,
                  patient__r.Termination_Date__c,
                  patient__r.patient_date_of_birth__c,
                  patient__r.Patient_First_Name__c,
                  patient__r.Patient_Last_Name__c,
                  patient__r.Patient_Social_Security_Number__c,
                  patient__r.Patient_Employer__r.Underwriter__c,
                  patient__r.Employer_Group__c,
                  patient__r.Cert__c,
                  patient__r.Sequence__c,
                  name,
                  Admission_Date__c,
                  Approved_Through_Date__c,
                  Approved_Days__c,
                  Comment_Line_for_Claims__c,
                  Med_Cat__c,
                  event_type__c,
                  visits__c,
                  initial_hp_load__c,
                  HealthPac_Case_Number__c,
                  Case_Status__c,
                  softDelete__c,
                  recordtype.name from Utilization_Management__c where id in :theIds];

        Utilization_Management__c[] umUpdateList = new Utilization_Management__c[] {};
        Utilization_Management__c[] failedUMs = new Utilization_Management__c[] {};

        for (Utilization_Management__c um : umList) {
            boolean isApproved = populationHealthUtils.isUmCaseApproved(um.Case_status__c);
            boolean isDenied = populationHealthUtils.isUmCaseDenied(um.Case_status__c);

            date patientEffectiveDate;

            if (um.patient__r.Effective_Date__c != null) {
                patientEffectiveDate = um.patient__r.Effective_Date__c;
            }
            else if (um.Admission_Date__c != null) {
                patientEffectiveDate = um.Admission_Date__c.addDays(-1);
            }

            if (patientEffectiveDate != null && um.Med_Cat__c != null && um.event_type__c != null) {
                //if(um.patient__r.Effective_Date__c!=null && um.Med_Cat__c!=null && um.event_type__c!=null){

                string frmdate;
                string thrudate;
                boolean isDummy = false;

                if (um.initial_hp_load__c == date.today() && !isApproved && !isDenied) {
                    frmdate = dummyDate;
                    thrudate = dummyDate;
                    isDummy = true;
                }
                else if ((isApproved || isDenied || um.softDelete__c) && um.Admission_Date__c != null && um.Approved_Through_Date__c != null) {
                    if (isDenied) {
                        frmdate = string.valueof(umDenyDate.year() + '' + utilities.formatDate(umDenyDate.month()) + '' + utilities.formatDate(umDenyDate.day()));
                        thrudate = frmdate;
                    }
                    else{
                        frmdate = string.valueof(um.Admission_Date__c.year() + '' + utilities.formatDate(um.Admission_Date__c.month()) + '' + utilities.formatDate(um.Admission_Date__c.day()));
                        thrudate = string.valueof(um.Approved_Through_Date__c.year() + '' + utilities.formatDate(um.Approved_Through_Date__c.month()) + '' + utilities.formatDate(um.Approved_Through_Date__c.day()));
                    }
                }

                if (frmdate != null && thrudate != null) {
                      //string effdate = um.patient__r.Effective_Date__c.year()+''+utilities.formatDate(um.patient__r.Effective_Date__c.month())+''+utilities.formatDate(um.patient__r.Effective_Date__c.day());
                    string effdate = patientEffectiveDate.year() + '' + utilities.formatDate(patientEffectiveDate.month()) + '' + utilities.formatDate(patientEffectiveDate.day());
                    string termdate = '';

                    if (um.patient__r.Termination_Date__c != null) {
                        termdate = um.patient__r.Termination_Date__c.year() + '' + utilities.formatDate(um.patient__r.Termination_Date__c.month()) + '' + utilities.formatDate(um.patient__r.Termination_Date__c.day());
                    }

                    string[] pDobArr = um.patient__r.patient_date_of_birth__c.split('/');
                    string pdob = pDobArr [2] + '' + pDobArr [0] + '' + pDobArr [1];

                    string approvedDays, visits;
                    if (um.Approved_Days__c == null || um.Approved_Days__c == 0 || isDummy) {
                        approvedDays = '';
                    }
                    else{
                        approvedDays = string.valueof(um.Approved_Days__c);
                    }

                    if (um.visits__c == null || isDummy) {
                        visits = '';
                    }
                    else{
                        visits = string.valueof(um.visits__c);
                    }
                    string commentLine = '';
                    if (isDummy) {
                        commentLine = 'IP Review Pending';
                        if (um.recordtype.name == 'Outpatient') {
                            commentLine = 'OP Review Pending';
                        }
                    }
                    else{
                        if (um.Comment_Line_for_Claims__c != '' & um.Comment_Line_for_Claims__c != null) {
                            commentLine = um.Comment_Line_for_Claims__c;
                            if (commentLine.length() > 40) {
                                commentLine = commentLine.left(40);
                            }
                            commentLine = commentLine.capitalize().replaceAll('&', '\\u0026').replaceAll('\n', ' ').replaceAll('\t', ' ').trim();
                        }
                    }

                    hpEpisodeStruct episode = hp_EpisodeRecord.sendEpisode(um.patient__r.Patient_Last_Name__c, um.patient__r.Patient_first_Name__c, pdob, um.patient__r.Patient_Social_Security_Number__c, um.patient__r.Patient_Employer__r.Underwriter__c, um.patient__r.Employer_Group__c, um.patient__r.cert__c, um.patient__r.Sequence__c, um.Med_Cat__c, um.event_type__c, effdate, termDate, frmdate, thrudate, approvedDays, commentLine, um.HealthPac_Case_Number__c, visits);
                    system.debug(episode);
                    if (episode.resultCode == '0') {
                        try{
                            system.debug(episode.RecordNumber.substring(1, 9));
                        }catch (exception e) {}

                        if (um.HealthPac_Case_Number__c != null && um.HealthPac_Case_Number__c != '' && um.HealthPac_Case_Number__c != episode.RecordNumber.substring(1, 9)) {
                              //error email needed
                            Messaging.SingleEmailMessage mail = utilities.email('mmartin@hdplus.com', null, null, Userinfo.getFirstName() + ' ' + Userinfo.getLastName(), 'Error in episode record', 'Hello,\n\nError occured receiving episode record ' + um.id + ' returned episode number ' + episode.RecordNumber);
                            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        }
                        else{//validateCase__c=false
                            umUpdateList.add(new Utilization_Management__c(id = um.id, validateCase__c = false, HealthPac_Case_Number__c = episode.RecordNumber.substring(1, 9)));
                        }
                    }
                    else{
                        failedUMs.add(um);
                        //error email needed

                        string addr = 'sdsalesforce@hdplus.com';
                        if (utilities.isRUnningInSandbox()) {
                            addr = 'mmartin@hdplus.com';
                        }

                        Messaging.SingleEmailMessage mail = utilities.email(addr, null, null, Userinfo.getFirstName() + ' ' + Userinfo.getLastName(), 'Error in episode record', 'Hello,\n\nAn error occured while attempting to receive an episode record for ' + um.name + '\n\nExpected result code 0, received result code ' + episode.ResultCode + '.' + '\n\n' + 'If several tickets show up within a few minutes of each other, this could indicate a connection issue between Salesforce and WebApp2/Healthpac.\n\nYou can view the pre-cert here ' + URL.getOrgDomainUrl().toExternalForm() + '/' + um.id);
                        Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                        if (!isApproved && !isDenied) {
                            umUpdateList.add(new Utilization_Management__c(id = um.id, validateCase__c = false, initial_hp_load__c = null));
                        }
                    }
                }
            }
        }

        update umUpdateList;
        // utilizationManagementWorkflow.sendUM(failedSet);
    }

    public static void check_for_clinical_note_for_cm(set<id> noteids){
        map<id, Utilization_Management_Note__c> um_note_map = new map<id, Utilization_Management_Note__c> {};

        for (Utilization_Management_Note__c umn : [select Utilization_Management__r.patient__c, Utilization_Management__r.name, Body__c, createdDate from Utilization_Management_Note__c where id in :noteids]) {
            Utilization_Management_Note__c foo = um_note_map.get(umn.Utilization_Management__r.patient__c);
            if (foo == null) {
                um_note_map.put(umn.Utilization_Management__r.patient__c, umn);
            }
            else{
                if (foo.createdDate > umn.createdDate) {
                    um_note_map.put(umn.Utilization_Management__r.patient__c, foo);
                }
            }
        }

        Case_Management__c[] cm_update_list = new Case_Management__c[] {};
        for (Case_Management__c cm : [select lastModifiedDate, Benefit_Year__c, clinical_update__c, clinical_update_note__c, patient__c from Case_Management__c where patient__c in :um_note_map.keySet() and benefit_year__c > :date.today().addYears(-1).year()]) {
            if (um_note_map.get(cm.patient__c).createdDate > cm.lastModifiedDate) {
                cm.clinical_update__c = true;
                cm.clinical_update_note__c = um_note_map.get(cm.patient__c).Utilization_Management__r.name + ': ' + um_note_map.get(cm.patient__c).body__c;
                cm_update_list.add(cm);
            }
        }

        if (cm_update_list.size() > 0) {
            update cm_update_list;
        }
    }

    //returns an array of UM recods that conflict with the umRecords passed in as a param
    /*
     * public static Utilization_Management__c[] patientOverlapCheck(map<string, Utilization_Management__c> umRecordMap){
     *
     *  Utilization_Management__c[] responseArr = new Utilization_Management__c[]{};
     *
     *  map<id, Utilization_Management__c[]> umMap = new map<id, Utilization_Management__c[]>(); //in :umRecordMap.keyset()
     *  for(Utilization_Management__c um : [select name, Patient__c, Admission_Date__c,Approved_Through_Date__c, recordtype.name from Utilization_Management__c where patient__c in :umRecordMap.keyset() and recordtype.name = 'Inpatient' and Newborn__c=false]){
     *
     *      if(um.id==umRecordMap.get(um.patient__c).id){continue;}
     *
     *      Utilization_Management__c[] umList = umMap.get(um.patient__c);
     *      if(umList==  null){
     *          umList = new Utilization_Management__c[]{};
     *
     *      }
     *
     *      umList.add(um);
     *      umMap.put(um.patient__c, umList);
     *
     *  }
     *
     *  for(id patient : umMap.keyset()){
     *      Utilization_Management__c[] umList = umMap.get(patient);
     *      if(umList==null){continue;}
     *
     *      Utilization_Management__c currRecord = umRecordMap.get(patient);
     *      for(Utilization_Management__c um : umList){
     *          true== dates overlap
     *          if(utilizationManagementWorkflow.compareDates(currRecord, um)){
     *              responseArr.add(um);
     *              break;
     *          }
     *      }
     *
     *  }
     *  return responseArr;
     *
     * }
     *
     * public static boolean compareDates(Utilization_Management__c currRecord, Utilization_Management__c prevRecord){
     *  date umDenyDate = date.valueof('1980-01-01');
     *
     *  if(currRecord.Admission_Date__c == umDenyDate || currRecord.Approved_Through_Date__c ==umDenyDate){
     *      return false;
     *  }
     *
     *  (StartA <= EndB) and (EndA >= StartB)
     *  if(currRecord.Admission_Date__c <= prevRecord.Approved_Through_Date__c && currRecord.Approved_Through_Date__c>= prevRecord.Admission_Date__c  ){
     *      return true;
     *  }
     *
     *  return false;
     * }
     */
}