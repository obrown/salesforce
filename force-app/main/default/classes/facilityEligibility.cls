public with sharing class facilityEligibility{
    
    public string bid {get; set;}
    public boolean BID_Verified {get; set;}
    public boolean Carrier_and_Plan_Type_verified {get; set;}
    public boolean Patient_and_Employee_DOB_verified {get; set;}
    public boolean Patient_and_Employee_SSN_verified {get; set;}
    public string Carrier_Name {get; set;}
    public string Client_Carrier {get; set;}
    public string Client {get; set;}
    public string Client_Name {get; set;}
    public date Date_Care_Mgmt_Transfers_to_Eligibility {get; set;}
    public string recordTypeName {get; set;}
    public string employee_first_name {get; set;}
    public string Employee_Last_Name {get; set;}
    public string Employee_Dobe {get; set;}
    public string Employee_Dob {get; set;}
    public string Employee_SSN {get; set;}
    public string Employee_Home_Phone {get; set;}
    public string Employee_Mobile {get; set;}
    public string Employee_Email_Address {get; set;}
    public string Employee_Street {get; set;}
    public string Employee_State {get; set;}
    public string Employee_City {get; set;}
    public string Employee_Zip_Postal_Code {get; set;}
    public string Employee_Primary_Health_Plan_Name {get; set;}
    public string eNotes {get; set;}
    public string Eligible {get; set;}
    public boolean Family_Deductible {get; set;}
    public string name {get; set;}
    public string Patient_First_Name {get; set;}
    public string Patient_Last_Name {get; set;}
    public string Patient_Dobe {get; set;}
    public string Patient_Dob {get; set;}
    public string Patient_SSN {get; set;}
    public string Patient_Home_Phone {get; set;}
    public string Patient_Mobile {get; set;}
    public string Patient_Email_Address {get; set;}
    public string Patient_Street {get; set;}
    public string Patient_State {get; set;}
    public string Patient_City {get; set;}
    public string Patient_Zip_Postal_Code {get; set;}
    public boolean pendingEligCheck {get; set;}
    public date Effective_Date_of_Medical_Coverage {get; set;}
    public string Relationship_to_the_Insured {get; set;}
    public boolean Single_Deductible {get; set;}
    public id id {get; set;}
    public string program_type {get; set;}
    
    public facilityEligibility setBariatric(bariatric__c b){
        
        facilityEligibility fe = new facilityEligibility();
        fe.id = b.id;
        fe.program_type = 'Bariatric';
        fe.bid = b.bid__c;
        fe.BID_Verified = b.BID_Verified__c;
        fe.Patient_and_Employee_DOB_verified = b.Verified_Employee_DOB__c ;
        fe.Patient_and_Employee_SSN_verified = b.Verified_Employee_SSN__c ;
        
        
        fe.Carrier_Name = b.Carrier_Name__c;
        fe.Client_Carrier = b.Client_Carrier__c;
        fe.Client = b.Client__c;
        fe.Client_Name =b.Client_Name__c;
        fe.Date_Care_Mgmt_Transfers_to_Eligibility = b.Date_Eligibility_Submitted__c;
        fe.employee_first_name = b.employee_first_name__c;
        fe.Employee_Last_Name =b.Employee_Last_Name__c;
        
        if(b.Employee_Dob__c!=null){
            fe.Employee_Dob = date.valueof(b.Employee_Dob__c).month()+'/'+date.valueof(b.Employee_Dob__c).day()+'/'+date.valueof(b.Employee_Dob__c).year();
        }
        
        fe.Employee_SSN = b.Employee_SSN__c;
        fe.Employee_Home_Phone = b.Employee_Home_Phone__c;
        fe.Employee_Mobile =b.Employee_Mobile_Phone__c;
        fe.Employee_Email_Address =b.Employee_Email_Address__c;
        fe.Employee_Street =b.Employee_Street__c;
        fe.Employee_State =b.Employee_State__c;
        fe.Employee_City =b.Employee_City__c;
        fe.Employee_Zip_Postal_Code =b.Employee_Zip_Code__c;
        fe.Employee_Primary_Health_Plan_Name = b.Employee_Primary_Health_Plan_Name__c;
        //fe.eNotes =b.eNotes__c;
        fe.Eligible =b.Eligible__c;
        //fe.Family_Deductible =b.Family_Deductible__c;
        fe.name = b.name;
        fe.Patient_First_Name = b.Patient_First_Name__c;
        fe.Patient_Last_Name = b.Patient_Last_Name__c;
        
        if(b.Patient_Dob__c!=null){
            fe.Patient_Dob = date.valueof(b.Patient_Dob__c).month()+'/'+date.valueof(b.Patient_Dob__c).day()+'/'+date.valueof(b.Patient_Dob__c).year();
        }
        
        fe.Patient_SSN =b.Patient_SSN__c;
        fe.Patient_Home_Phone = b.Patient_Home_Phone__c; 
        fe.Patient_Mobile =b.Patient_Mobile_Phone__c;
        fe.Patient_Email_Address =b.Patient_Email_Address__c;
        fe.Patient_Street =b.Patient_Street__c;
        fe.Patient_State =b.Patient_State__c;
        fe.Patient_City = b.Patient_City__c;
        fe.Patient_Zip_Postal_Code = b.Patient_Zip_Code__c;
        //fe.pendingEligCheck =b.pendingEligCheck__c;
        fe.Effective_Date_of_Medical_Coverage =b.Effective_Date_of_Medical_Coverage__c;
        fe.Relationship_to_the_Insured = b.Relationship_to_the_Insured__c;
        fe.Single_Deductible = b.Single_Deductible__c;
        
        return fe;
    }
    
    public bariatric__c saveBariatric(facilityEligibility fe, bariatric__c b){
        b.bid__c =fe.bid;
        b.BID_Verified__c =fe.BID_Verified;
        b.Verified_Employee_DOB__c =fe.Patient_and_Employee_DOB_verified;
        b.Verified_Employee_SSN__c =fe.Patient_and_Employee_SSN_verified;
        
        b.Verified_Patient_DOB__c=fe.Patient_and_Employee_DOB_verified;
        b.Verified_Patient_SSN__c =fe.Patient_and_Employee_SSN_verified;
        
        
        b.Carrier_Name__c =fe.Carrier_Name;
        b.Client_Carrier__c =fe.Client_Carrier;
        b.employee_first_name__c =fe.employee_first_name;
        b.Employee_Last_Name__c =fe.Employee_Last_Name;
        b.Employee_Dob__c = fe.Employee_Dob;
        
        
        b.Employee_SSN__c=fe.Employee_SSN;
        b.Employee_Home_Phone__c =fe.Employee_Home_Phone; 
        b.Employee_Mobile_Phone__c =fe.Employee_Mobile;
        b.Employee_Email_Address__c =fe.Employee_Email_Address;
        b.Employee_Street__c =fe.Employee_Street;
        b.Employee_State__c =fe.Employee_State;
        b.Employee_City__c =fe.Employee_City;
        b.Employee_Zip_Code__c =fe.Employee_Zip_Postal_Code;
        b.Employee_Primary_Health_Plan_Name__c =fe.Employee_Primary_Health_Plan_Name;
        //fe.eNotes =b.eNotes__c;
        b.Eligible__c =fe.Eligible;
        b.Family_Deductible__c=fe.Family_Deductible;
        
        b.Patient_First_Name__c =fe.Patient_First_Name;
        b.Patient_Last_Name__c =fe.Patient_Last_Name;
        b.Patient_Dob__c =fe.Patient_Dob;
        
        b.Patient_SSN__c =fe.Patient_SSN;
        b.Patient_Home_Phone__c =fe.Patient_Home_Phone; 
        b.Patient_Mobile_Phone__c =fe.Patient_Mobile;
        b.Patient_Email_Address__c =fe.Patient_Email_Address;
        b.Patient_Street__c =fe.Patient_Street;
        b.Patient_State__c =fe.Patient_State;
        b.Patient_City__c =fe.Patient_City;
        b.Patient_Zip_Code__c =fe.Patient_Zip_Postal_Code;
        b.Effective_Date_of_Medical_Coverage__c =fe.Effective_Date_of_Medical_Coverage;
        b.Relationship_to_the_Insured__c =fe.Relationship_to_the_Insured;
        b.Single_Deductible__c =fe.Single_Deductible;
        
        if(b.Relationship_to_the_Insured__c=='Patient is Insured'){
            b.Patient_First_Name__c =fe.employee_first_name;
            b.Patient_Last_Name__c =fe.Employee_Last_Name;
            b.Patient_Dob__c =fe.Employee_SSN;
            b.Patient_SSN__c =fe.Employee_SSN;
            b.Patient_Home_Phone__c =fe.Employee_Home_Phone; 
            b.Patient_Mobile_Phone__c =fe.Employee_Mobile;
            b.Patient_Email_Address__c =fe.Employee_Email_Address;
            b.Patient_Street__c =fe.Employee_Street;
            b.Patient_State__c =fe.Employee_State;
            b.Patient_City__c =fe.Employee_City;
            b.Patient_Zip_Code__c =fe.Employee_Zip_Postal_Code;
        
        }
        
        return b;
    }
    
    public patient_case__c savePC(facilityEligibility fe, patient_case__c pc){
        system.debug('pc.id '+pc.id);
        pc.ecen_procedure__r.name=fe.program_type;
        pc.bid__c=fe.bid;
        pc.BID_Verified__c=fe.BID_Verified;
        pc.Carrier_and_Plan_Type_verified__c=fe.Carrier_and_Plan_Type_verified;
        pc.Patient_and_Employee_DOB_verified__c=fe.Patient_and_Employee_DOB_verified;
        pc.Patient_and_Employee_SSN_verified__c =fe.Patient_and_Employee_SSN_verified;
        pc.Carrier_Name__c=fe.Carrier_Name;
        pc.Client_Carrier__c=fe.Client_Carrier;
        pc.Client__c=fe.Client;
        pc.Client_Name__c=fe.Client_Name;
        pc.Date_Care_Mgmt_Transfers_to_Eligibility__c=fe.Date_Care_Mgmt_Transfers_to_Eligibility;
        //fe.recordTypeName = pc.recordType.Name ;
        pc.employee_first_name__c=fe.employee_first_name;
        pc.Employee_Last_Name__c=fe.Employee_Last_Name;
        system.debug(fe.Employee_Dobe);
        pc.Employee_Dobe__c = fe.Employee_Dobe;
      //  if(fe.Employee_Dobe!=null){
      //      string[] dob = fe.Employee_Dobe.split('/');
            //pc.Employee_Dobe__c=dob[2]+'-'+dob[0]+'-'+dob[1];
       // } 
        pc.Employee_SSN__c=fe.Employee_SSN;
        pc.Employee_Home_Phone__c =fe.Employee_Home_Phone;
        pc.Employee_Mobile__c =fe.Employee_Mobile;
        pc.Employee_Email_Address__c =fe.Employee_Email_Address;
        pc.Employee_Street__c =fe.Employee_Street;
        pc.Employee_State__c =fe.Employee_State;
        pc.Employee_City__c =fe.Employee_City;
        pc.Employee_Zip_Postal_Code__c =fe.Employee_Zip_Postal_Code;
        pc.Employee_Primary_Health_Plan_Name__c =fe.Employee_Primary_Health_Plan_Name;
        pc.eNotes__c =fe.eNotes;
        pc.Eligible__c =fe.Eligible;
        pc.Family_Deductible__c=fe.Family_Deductible;
        pc.name=fe.name;
        pc.Patient_First_Name__c =fe.Patient_First_Name;
        pc.Patient_Last_Name__c =fe.Patient_Last_Name;
        pc.Patient_Dobe__c= fe.Patient_Dobe;
        // if(fe.Patient_Dobe !=null){
        //    string[] dob = fe.Patient_Dobe.split('/');
        //    pc.Patient_Dobe__c =dob[2]+'-'+dob[0]+'-'+dob[1];
        //} 
        pc.Patient_SSN__c =fe.Patient_SSN;
        pc.Patient_Home_Phone__c =fe.Patient_Home_Phone; 
        pc.Patient_Mobile__c =fe.Patient_Mobile;
        pc.Patient_Email_Address__c =fe.Patient_Email_Address;
        pc.Patient_Street__c =fe.Patient_Street;
        pc.Patient_State__c =fe.Patient_State;
        pc.Patient_City__c =fe.Patient_City;
        pc.Patient_Zip_Postal_Code__c =fe.Patient_Zip_Postal_Code;
        
        pc.Effective_Date_of_Medical_Coverage__c =fe.Effective_Date_of_Medical_Coverage;
        pc.Relationship_to_the_Insured__c =fe.Relationship_to_the_Insured;
        pc.Single_Deductible__c =fe.Single_Deductible;
        
        return pc;
    }
    
    public facilityEligibility setPC(patient_case__c pc){
        facilityEligibility fe = new facilityEligibility();
        
        fe.id = pc.id;
        fe.program_type = pc.ecen_procedure__r.name;
        fe.bid = pc.bid__c;
        fe.BID_Verified = pc.BID_Verified__c;
        fe.Carrier_and_Plan_Type_verified = pc.Carrier_and_Plan_Type_verified__c;
        fe.Patient_and_Employee_DOB_verified = pc.Patient_and_Employee_DOB_verified__c;
        fe.Patient_and_Employee_SSN_verified = pc.Patient_and_Employee_SSN_verified__c;
        fe.Carrier_Name = pc.Carrier_Name__c;
        fe.Client_Carrier = pc.Client_Carrier__c;
        fe.Client = pc.Client__c;
        fe.Client_Name =pc.Client_Name__c;
        fe.Date_Care_Mgmt_Transfers_to_Eligibility = pc.Date_Care_Mgmt_Transfers_to_Eligibility__c;
        fe.recordTypeName = pc.recordType.Name ;
        fe.employee_first_name = pc.employee_first_name__c;
        fe.Employee_Last_Name =pc.Employee_Last_Name__c;
        fe.Employee_Dobe = pc.Employee_Dobe__c;
        fe.Employee_Dob = pc.Employee_Dobe__c;
        if(pc.Employee_Dobe__c!=null){
            fe.Employee_Dob = date.valueof(pc.Employee_Dobe__c).month()+'/'+date.valueof(pc.Employee_Dobe__c).day()+'/'+date.valueof(pc.Employee_Dobe__c).year();
        } 
        fe.Employee_SSN = pc.Employee_SSN__c;
        fe.Employee_Home_Phone = pc.Employee_Home_Phone__c;
        fe.Employee_Mobile =pc.Employee_Mobile__c;
        fe.Employee_Email_Address =pc.Employee_Email_Address__c;
        fe.Employee_Street =pc.Employee_Street__c;
        fe.Employee_State =pc.Employee_State__c;
        fe.Employee_City =pc.Employee_City__c;
        fe.Employee_Zip_Postal_Code =pc.Employee_Zip_Postal_Code__c;
        fe.Employee_Primary_Health_Plan_Name = pc.Employee_Primary_Health_Plan_Name__c;
        fe.eNotes =pc.eNotes__c;
        fe.Eligible =pc.Eligible__c;
        fe.Family_Deductible =pc.Family_Deductible__c;
        fe.name = pc.name;
        fe.Patient_First_Name = pc.Patient_First_Name__c;
        fe.Patient_Last_Name = pc.Patient_Last_Name__c;
        fe.Patient_Dobe  = pc.Patient_Dobe__c; 
        fe.Patient_Dob = pc.Patient_Dobe__c; 
        fe.Patient_SSN =pc.Patient_SSN__c;
        fe.Patient_Home_Phone = pc.Patient_Home_Phone__c; 
        fe.Patient_Mobile =pc.Patient_Mobile__c;
        fe.Patient_Email_Address =pc.Patient_Email_Address__c;
        fe.Patient_Street =pc.Patient_Street__c;
        fe.Patient_State =pc.Patient_State__c;
        fe.Patient_City = pc.Patient_City__c;
        fe.Patient_Zip_Postal_Code = pc.Patient_Zip_Postal_Code__c;
        fe.pendingEligCheck =pc.pendingEligCheck__c;
        fe.Effective_Date_of_Medical_Coverage =pc.Effective_Date_of_Medical_Coverage__c;
        fe.Relationship_to_the_Insured = pc.Relationship_to_the_Insured__c;
        fe.Single_Deductible = pc.Single_Deductible__c;
        
        return fe;
    }
}