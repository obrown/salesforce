public with sharing class facilityEligController {
    
    public Eligibilty__c elig {get; set;}
    public boolean empVerified {get;set;}
    public boolean patientVerified {get;set;}
    public boolean eligVerified {get;set;}
    public boolean isError {get; private set;}
    public string logo {get; private set;}
    
    string ct;
    
    transient selectOption[] carrierOpts;
    transient selectOption[] employeeHealthPlanOpts;
 
    public facilityEligibility obj {get; set;} 
    
    public string redirectClient {get; private set;}
    
    id thisID;
    
    void pageLoad(){
        isError = false;
        obj = new facilityEligibility();
        init(thisID);
    }
    
    public facilityEligController(id theID){
        thisID = theID;
        
        pageLoad();
    }
    
    public facilityEligController(){
    
        thisID = ApexPages.currentPage().getParameters().get('id');
        if(string.valueof(thisID.getSobjectType())=='Patient_Case__c'){   
            ct = 'pc';
        }else{
            ct = 'bariatric';
        } 
        
        pageLoad();
    
    }
    
    public selectOption[] getcarrierOpts(){
        if(obj .client==null){return null;}
        
        selectOption[] foo = coeSelfAdminPickList.selectOptionList(null, obj.client, null, null, coeSelfAdminPickList.carrierName, obj.Client_Carrier, true);
        return foo;
    }
    
    public selectOption[] getemployeeHealthPlanOpts(){
        
        if(obj.Client_Carrier==null){
            return new selectoption[]{new selectoption('','No carrier set')};
        }
        if(ct=='pc'){
        return coeSelfAdminPickList.selectOptionList(objPC, obj.client, null, null, coeSelfAdminPickList.employeeHealthplanName, obj.Employee_Primary_Health_Plan_Name, true);
        }
        
        return coeSelfAdminPickList.selectOptionList(objBar, obj.client, null, null, coeSelfAdminPickList.employeeHealthplanName, obj.Employee_Primary_Health_Plan_Name, true);
    }
    patient_case__c objPC = new patient_case__c();
    bariatric__c objBar = new bariatric__c();
    
    void init(id thisID){
    
    if(string.valueof(thisID.getSobjectType())=='Patient_Case__c'){    
        this.objPC = [select bid__c,
                             BID_Verified__c,
                             Carrier_and_Plan_Type_verified__c,
                             Patient_and_Employee_DOB_verified__c,
                             Patient_and_Employee_SSN_verified__c,
                             Carrier_Name__c,
                             Client_Carrier__c,
                             Client__c,
                             Client_Name__c,
                             Date_Care_Mgmt_Transfers_to_Eligibility__c,
                             recordType.Name,
                             employee_first_name__c, 
                             Employee_Last_Name__c,
                             Employee_Dobe__c,
                             Employee_Dob__c,
                             Employee_SSN__c,
                             Employee_Home_Phone__c,
                             Employee_Mobile__c,
                             Employee_Email_Address__c,
                             Employee_Street__c,
                             Employee_State__c,
                             Employee_City__c,
                             Employee_Zip_Postal_Code__c,
                             Employee_Primary_Health_Plan_Name__c,
                             Ecen_Procedure__r.name,
                             eNotes__c,
                             Eligible__c,
                             id,
                             Family_Deductible__c,
                             name,
                             Patient_First_Name__c,
                             Patient_Last_Name__c,
                             Patient_Dobe__c,
                             Patient_Dob__c,
                             Patient_SSN__c,
                             Patient_Home_Phone__c,
                             Patient_Mobile__c,
                             Patient_Email_Address__c,
                             Patient_Street__c,
                             Patient_State__c,
                             Patient_City__c,
                             Patient_Zip_Postal_Code__c,
                             pendingEligCheck__c,
                             Effective_Date_of_Medical_Coverage__c,
                             Relationship_to_the_Insured__c,
                             Status_reason__c,
                             Single_Deductible__c from patient_case__c where id = :thisID];
        
        try{
            this.objPC.Patient_Dob__c = date.valueof(this.objPC.Patient_Dobe__c);
        }catch(exception e){}
        
        try{
            this.objPC.Employee_Dob__c = date.valueof(this.objPC.Employee_Dobe__c);
        }catch(exception e){}
        
        if(this.objPC.client_name__c == 'Lowes'){
            logo = '/logos/LowesLogo.jpg';
            redirectClient = 'Lowes';
        }else if(this.objPC.client_name__c.toLowerCase() == 'jetblue'){
            logo = '/logos/jetBlue.jpg';
            redirectClient = 'jetBlue';
        }
        
        if(this.objPC.Patient_and_Employee_DOB_verified__c == true &&
           this.objPC.Patient_and_Employee_SSN_verified__c == true){
           empVerified = true;
           patientVerified = true;
           
        }else{
           empVerified = false;
           patientVerified = false;
        }
        
        if(this.objPC.BID_Verified__c == true && 
           this.objPC.Carrier_and_Plan_Type_verified__c == true){
           
            eligVerified = true;
            
        }else{
            eligVerified = false;
        }    
        
       
        
        try{
            elig = [select Status__c,
                           Status_Effective_Date__c,
                           Paid_through_Date__c,Termination_Date__c from Eligibilty__c where patient_case__c = :this.objPC.id and eligible__c ='' order by createdDate desc limit 1];
        
        }catch(exception e){
            elig = new Eligibilty__c(patient_case__c= this.objPC.id);
        } 
        
        obj = obj.setPC(objPC);
        
        }else{
            
          objBar  = [select bid__c,
                             BID_Verified__c,
                             Verified_Employee_DOB__c ,
                             Verified_Employee_SSN__c ,
                             Carrier_Name__c,
                             Client_Carrier__c,
                             Client__c,
                             Client_Name__c,
                             Date_Eligibility_Submitted__c,
                             employee_first_name__c, 
                             Employee_Last_Name__c,
                             Employee_Dob__c,
                             Employee_SSN__c,
                             Employee_Home_Phone__c,
                             Employee_Mobile_Phone__c,
                             Employee_Email_Address__c,
                             Employee_Street__c,
                             Employee_State__c,
                             Employee_City__c,
                             Employee_Zip_Code__c,
                             Employee_Primary_Health_Plan_Name__c,
                             Eligible__c,
                             Family_Deductible__c,
                             name,
                             Patient_First_Name__c,
                             Patient_Last_Name__c,
                             Patient_Dob__c,
                             Patient_SSN__c,
                             Patient_Home_Phone__c,
                             Patient_Mobile_Phone__c,
                             Patient_Email_Address__c,
                             Patient_Street__c,
                             Patient_State__c,
                             Patient_City__c,
                             Patient_Zip_Code__c,
                             Effective_Date_of_Medical_Coverage__c,
                             Relationship_to_the_Insured__c,
                             Single_Deductible__c from bariatric__c where id = :thisID];
            
            try{
            elig = [select Status__c,
                           Status_Effective_Date__c,
                           Paid_through_Date__c,Termination_Date__c from Eligibilty__c where bariatric__c = :thisID and eligible__c ='' order by createdDate desc limit 1];
        
            }catch(exception e){
                elig = new Eligibilty__c(patient_case__c= this.objPC.id);
            } 
            
             obj = obj.setBariatric(objBar);
        
        }
    }

    void saver(){
        isError=false;
        try{
        
        if(this.obj.Relationship_to_the_Insured  == 'Patient is Insured'){
            patientVerified = true;
        }
    
        if(empVerified && patientVerified){
            this.obj.Patient_and_Employee_DOB_verified = true;
            this.obj.Patient_and_Employee_SSN_verified = true;
        }else{
            this.obj.Patient_and_Employee_DOB_verified = false;
            this.obj.Patient_and_Employee_SSN_verified = false;
        }
    
        if(eligVerified){
            this.obj.BID_Verified = true;
            this.obj.Carrier_and_Plan_Type_verified = true;
        }else{
            this.obj.BID_Verified = false;
            this.obj.Carrier_and_Plan_Type_verified = false;
        }
        
        if(ct=='pc'){   
            objPC = obj.savePC(obj, objPC);
            update objPC;
        
        }else if(ct=='bariatric'){ 
            objBar= obj.saveBariatric(obj, objBar);
            update objBar;
        
        }
        
        }catch(dmlException e){
            isError=true;
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                if(i<10){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber()); 
                }else{
                    break;
                }
            }
        }    
    }

    public void mySave(){
        saver();
        
        if(!isError){
        
        try{
            upsert elig;
            if(string.valueof(thisID.getSobjectType())=='Patient_Case__c'){   
                update this.objPC;
            }
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Record Saved'));
        }catch(dmlException e){
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                if(i<10){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)); 
                }else{
                    break;
                }
            }
        }    
        
        } 
        
    }

    public void mySubmit(){
        isError = false;
        saver();
        system.debug('elig.status__c '+elig.status__c);
        if(!isError){
            if(empVerified && patientVerified && eligVerified ){
                
                if(this.objPC.eligible__c == ''){
                    isError=true;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please mark the eligible picklist as Yes or No'));
                }
                
                if(this.objPC.eligible__c=='Yes'){
                
                    if(elig.status__c == null || elig.status__c == ''){
                        isError=true;
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please populate the status'));
                    }
                    
                    if(elig.status__c == 'Active' && this.objPC.Eligible__c==''){
                        isError=true;
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please populate the eligible field'));
                    }
                    
                    if(this.objPC.Eligible__c=='Yes' && this.objPC.Effective_Date_of_Medical_Coverage__c ==null){
                        isError=true;
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please populate the Effective Date of Coverage'));
                    }
                
                
                }
                
                //Be sure the neccesary date fields are populated before moving forward
                if(isError){
                    return;
                }
                
                if(this.obj.eligible == 'Yes'){
                    
                    if(this.obj.Effective_Date_of_Medical_Coverage > date.today()){
                        isError=true;
                        ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The Effective Date of Coverage is in the future. Please correct this, or mark the patient as ineligible.'));
                    }
                    
                    //if(elig.Status_Effective_Date__c > date.today()){
                       // isError=true;
                        //ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The Effective Date is in the future. Please correct this, or mark the patient as ineligible.'));
                    //}
                    
                    //if((elig.status__c != 'Active' && elig.status__c != null && elig.status__c != '') && elig.Paid_through_Date__c < date.today()){
                    //    isError=true;
                     //   ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'The Paid through Date is in the past. Please correct this, or mark the patient as ineligible.'));
                    //}
                
                }
                
                
                if(isError){
                    return;
                }
                
                isError=false;
                
                try{
                    if(string.valueof(thisID.getSobjectType())=='Patient_Case__c'){   
                        this.objPC.pendingEligCheck__c = false;
                        this.objPC.Status_reason__c='In Process';
                        
                        update this.objPC;
                    
                    }else if(string.valueof(thisID.getSobjectType())=='Bariatric__c'){ 
                        objBar.Date_Eligibility_Check_with_Client__c = date.today();
                        update objBar; 
                    
                    }else{
                        return;
                    }
                    
                    upsert elig;
                    ApexPages.addMessage(new ApexPages.message(ApexPages.severity.CONFIRM, 'Record Saved'));
                    
                }catch(dmlException e){
                    isError=true;
                    
                    for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                        if(i<10){
                            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                            System.debug(e.getDmlMessage(i)); 
                        }else{
                            break;
                        }
                    }
                }    
            }else{
                isError=true;
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.Fatal, 'Please check all the "Verified" checkboxes before submitting.'));
                
            } 
        
        }
    }

    /*
    void setthelogourl(){
        
        if(this.objPC.Client_Name__c!=null)
        {
            if(this.objPC.Client_Name__c == 'HCR ManorCare'){
              logourl = '/logos/HCRLogo.jpg';
          }else if(this.objPC.Client_Name__c == 'Kohls'){
              logourl = '/logos/KohlsLogo.png';
          }else if(this.objPC.Client_Name__c == 'Walmart'){
              logourl = '/logos/WalmartLogo.png';
          }else if(this.objPC.Client_Name__c == 'PepsiCo'){
              logourl = '/logos/PepsicoLogo.png';
          }else if(this.objPC.Client_Name__c == 'Lowes'){
              logourl = '/logos/LowesLogo.jpg';
          }else if(this.objPC.Client_Name__c == 'McKesson'){
              logourl = '/logos/McKessonLogo.png';
          }else if(this.objPC.Client_Name__c == 'Alon'){
              logourl = '/logos/AlonLogo.gif';
          }
        }
    }
    */

    /*
    public pageReference msave(){
        try{
            
            if(objPC.Employee_Last_Name__c==null || objPC.Employee_Last_Name__c==''){
                ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, 'Please enter the employee\'s last name'));
                return null;
            }
            objPC.Patient_Home_Phone__c=objPC.PatientHomePhoneDisp__c;
            objPC.PatientHomePhoneDisp__c=null;
            update objPC;
            objPC.PatientHomePhoneDisp__c=objPC.Patient_Home_Phone__c;
            
        }catch(exception e){
            ApexPages.addMessage(new ApexPages.message(ApexPages.severity.ERROR, e.getMessage()));
        }
        //pageReference detailPage = obj.save();
        //system.debug(this.objPC.employee_last_name__c);
        //update this.objPC;
        return null;
    }
    */

}