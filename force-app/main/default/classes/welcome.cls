public with sharing class welcome{
    
    //public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician,string userName,string userTitle,string userPhone,string userEmail){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);

        string disclaimerPath;
        
        StaticResource logo;
        StaticResource[] srList = [SELECT Id,NamespacePrefix,SystemModstamp,Name FROM StaticResource WHERE Name = 'cmSurvey' order by Name asc];
        logo=srList[0];
        
        String prefix = logo.NamespacePrefix;
        if( String.isEmpty(prefix) ) {
            prefix = '';
        }else{
            //If has NamespacePrefix
            prefix += '__';
        }        

        disclaimerPath='/resource/' + logo.SystemModstamp.getTime() + '/' + prefix + 'ohyDisclaimer';//added 1-30-21 to set pull in the disclaimerPath
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=cm.patient__r.Patient_First_Name__c+' '+cm.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Address__c: +'<br/>';
        letterText+='<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.City__c+', ' : ''+'&nbsp';
        letterText+=hasPatientAddress ? cm.patient__r.State__c+' ' : ''+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Zip__c : ''+'<br/><br/>';
        letterText+='<br/><br/>';

        letterText+='</div>';
        letterText+='<p>';
        letterText+='Dear&nbsp;'+cm.patient__r.Patient_First_Name__c+',<br/>';
        letterText+='As a member of OhioHealthy, you have access to a care management team that includes a social worker, pharmacist, nurses, health coaches and dietitians. This team is here to support ';
        letterText+='and assist you in reaching your health goals by: <br/>';
        letterText+='</p>';

        letterText+='<ul style="list-style-type:none;">';
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='Providing you with education about your medical condition(s)';
        letterText+='</li>';
        letterText+='<li>';
        letterText+='Connecting you with resources to help you with your healthcare and medication costs';
        letterText+='</li>';
        letterText+='<li>';
        letterText+='Arranging your follow-up appointments and/or testing';
        letterText+='</li>';
        letterText+='<li>';
        letterText+='Supporting you in making healthy behavior changes to improve your health';
        letterText+='</li>';
        letterText+='<li>';
        letterText+='Connecting you to resources for transportation and financial assistance';
        letterText+='</li>';
        letterText+='<li>';
        letterText+='Answering questions about your medications helping you manage stress';
        letterText+='</li>';            
        letterText+='</ul>';

        letterText+='<p>';
        letterText+='One of the team members will call you soon to discuss these free services and programs available to you. Please know that your responses are confidential. <br/><br/>';
        letterText+='If you have any questions, or need to reach me sooner, please call the phone number below between 8:00 a.m. and 5:00 p.m., Monday through Friday. If I do not answer the phone, please leave your name';
        letterText+=' and phone number so that I can return your call. I look forward to speaking with you.';
        letterText+='</p>';
        
        letterText+='<p>';  
        letterText+='Sincerely,<br/><br/>';
        /*
        letterText+='Care Management Department<br/>';
        letterText+='Ohio Healthy<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';
        */
        letterText+=userName+'<br/>';
        letterText+=userTitle+'<br/>';
        letterText+=userPhone+'<br/>';
        letterText+=userEmail+'<br/>';        
        letterText+='</p>';
        
        letterText+='<div><img src="'+disclaimerPath+'" style="width:320px;margin-left:-3px"/></div><br/>';// pulls in the image of the surveyPath
        return letterText;
    }
    
}