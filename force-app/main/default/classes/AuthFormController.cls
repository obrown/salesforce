public with sharing class AuthFormController {

     public Patient_Case__c obj {get;set;}
     private list<Attachment> attachList = new list<Attachment>();
     transient Attachment endattachment;
     private list<Program_Notes__c> plist = new list<Program_Notes__c>();
     public string theattachviewID {get;set;}
     public string thetempID;
     public string finalattachID {get;set;}
     public string URL;
     public string retURL {get;set;}     
     public string theOpp;
     public string theTo {get;set;}        
     public string theCC{get;set;}
     public string thebody {get;set;}
     public string theSubject {get;set;}
     public string errorMessage {get;set;}
     public string error {get;set;}
     public boolean IsError {get;set;}
     public boolean renderpdf {get;set;}
     public id attachID;
     public string theID {get;set;}
     public boolean IsSuccess {get;set;}
     public string noteType {get;set;}
     string testCC ='';
     string testNote='';
     string testSubject;
     string testType;
     string theNotes {get;set;}
     public boolean ccNote {get;set;}
    
     public void updateInfo(){
      testSubject =theSubject;
      testNote=theBody;
      testCC = theCC;
      testType = noteType;
     }
     
     void pageLoad(id thisID){
        theID = thisID;
        obj = new Patient_Case__c(); 
       
        obj = [Select Referred_Facility__c,nReferred_Facility__c ,client__r.name,ecen_procedure__r.name,client_facility__r.Authorization_Email_Contact__c, status__c,displayAuthNumber__c,case_Type__c,Authorization_Number__c,authSent__c,RecordType.Name,Patient_DOBe__c,Patient_Email_Address__c,Employee_DOBe__c,Patient_DOB__c,Employee_DOB__c,Employee_Primary_Health_Plan_ID__c,Relationship_to_the_Insured__c,Patient_Preferred_Phone__c,Patient_Gender__c,Patient_City__c,Patient_State__c, recent_testing__c,caregiver_name__c,caregiver_home_phone__c,Caregiver_Mobile__c,diagnosis__c,proposed_procedure__c,patient_symptoms__c,Other_Pertinent_Medical_Info_History__c,procedure_complexity__c,eligible__c,
               Patient_Zip_Postal_Code__c,Patient_Country__c,attachEncrypt__c,Patient_Street__c,Patient_SSN__c,Patient_Work_Phone__c,Patient_Home_Phone__c,Patient_Mobile__c,isConverted__c,Medicare_As_Secondary_Coverage__c,Language_Spoken__c,Employee_Mobile__c,Employee_Preferred_Phone__c,Employee_Home_Phone__c,Employee_SSN__c,Employee_Work_Phone__c,Employee_Last_Name__c,Employee_First_Name__c,Employee_Street__c,Employee_City__c,Employee_State__c,Employee_Country__c,Employee_Zip_Postal_Code__c,Employee_gender__c,recordtypeID,BID__c,client_name__c,Referral_facility_contact__c,Patient_First_Name__c,Patient_Last_Name__c, Name,Employee_Primary_Health_Plan_Name__c, Expedited_Referral__c from Patient_Case__c where id = :theID];
               obj.displayAuthNumber__c = obj.Authorization_Number__c; //this is only called from the trigger, which has not had the auth number set yet...
               main(true);
             
             if(isError){
                 
                 string body ='Hello,\n\nAn error occurred sending the authorization form for ';
                 body += obj.Name + '.\n\nError message: ';
                 body += errorMessage + '\n\nPlease send the authorization number manually\n\n';
                 body += 'https://'+Environment__c.getInstance().instance__c+'.salesforce.com/'+obj.id;
                 body += '\n\nThank you';
                 Messaging.sendEmail(new List<Messaging.SingleEmailMessage> {utilities.email(UserInfo.getUserEmail(), null, null, null ,'Error sending the authorization number',body)});
                 obj.authSent__c = true;
                 update obj;
                 return;
             }
             
             formSelected();    
     }
     
     public AuthFormController(id thisID){
         pageLoad(thisID);

            
     }
     
     public AuthFormController(){
     
            obj = new Patient_Case__c();
            if(theID==null){
                theID = ApexPages.currentPage().getParameters().get('Id');
                theID = id.valueof(theID);
            }
            //Referred_Facility__c
            obj = [Select Referred_Facility__c,nReferred_Facility__c ,client__r.name,ecen_procedure__r.name,status__c,client_facility__r.Authorization_Email_Contact__c, case_Type__c, displayAuthNumber__c,authSent__c,RecordType.Name,Patient_DOBe__c,Patient_Email_Address__c,Employee_DOBe__c,Patient_DOB__c,Employee_DOB__c,Employee_Primary_Health_Plan_ID__c,Relationship_to_the_Insured__c,Patient_Preferred_Phone__c,Patient_Gender__c,Patient_City__c,Patient_State__c, recent_testing__c,caregiver_name__c,caregiver_home_phone__c,Caregiver_Mobile__c,diagnosis__c,proposed_procedure__c,patient_symptoms__c,Other_Pertinent_Medical_Info_History__c,procedure_complexity__c,eligible__c,
                  Patient_Zip_Postal_Code__c,Patient_Country__c,attachEncrypt__c,Patient_Street__c,Patient_SSN__c,Patient_Work_Phone__c,Patient_Home_Phone__c,Patient_Mobile__c,isConverted__c,Medicare_As_Secondary_Coverage__c,Language_Spoken__c,Employee_Mobile__c,Employee_Preferred_Phone__c,Employee_Home_Phone__c,Employee_SSN__c,Employee_Work_Phone__c,Employee_Last_Name__c,Employee_First_Name__c,Employee_Street__c,Employee_City__c,Employee_State__c,Employee_Country__c,Employee_Zip_Postal_Code__c,Employee_gender__c,recordtypeID,BID__c,client_name__c,Referral_facility_contact__c,Patient_First_Name__c,Patient_Last_Name__c, Name,Employee_Primary_Health_Plan_Name__c, Expedited_Referral__c from Patient_Case__c where id = :theID];
            
            main(false);
    }
    
    private void main(boolean isnico){        
            isError=false;
            plist = new list<Program_Notes__c>([select attachID__c,ftpAttachment__c from Program_Notes__c where patient_case__c=:obj.id and isAuthEmailAttach__c=true order by createdDate desc limit 1]);
            
            if(plist.isEmpty()){
                obj.authSent__c=false;
                update obj;
                isError = true;
                errorMessage = 'No authorization form found';
                return;
            
            }
            
            retURL = '/pcredirect?id=' + obj.id;

            string recordTypeName = obj.client__r.name+' '+obj.ecen_procedure__r.name;
            string casecontact ='';
            theCC = convertCase.hdpemail(obj.client__r.name);
            
            if(utilities.isRunningInSandbox()){
                theTo = userinfo.getuseremail();
            }else{
                theTo = obj.client_facility__r.Authorization_Email_Contact__c;
            
                if(theTo!=null){
                    theTo = theTo.removeEnd(',');
                }
            
            }
            if(recordTypeName == 'Walmart Cardiac'){
                URL = '/apex/wmcardiacreferral?id=' + obj.id;
                casecontact ='1-877-286-3551 or WMCOEClinical@hdplus.com.';
            }else if(recordTypeName == 'Walmart Spine'){
                URL = '/apex/wmspinereferral?id=' + obj.id;
                casecontact ='1-877-286-3551 or WMCOEClinical@hdplus.com.';
            }else if(recordTypeName == 'Walmart Joint'){
                    URL = '/apex/pbghreferral?id=' + obj.id;
                    casecontact ='1-877-286-355 or WMCOEClinical@HDPlus.com.';
            }else if(recordTypeName == 'Lowes Cardiac'){
                    URL = '/apex/lowescardiacreferral?id=' + obj.id;
                    casecontact ='1-877-885-0654 or HDPCOEClinical@HDPlus.com.';
            }else if(recordTypeName == 'Lowes Spine' || recordTypeName.toLowerCase() == 'jetblue spine'){
                    URL = '/apex/lowesspinereferral?id=' + obj.id;
                    casecontact ='1-877-885-0654 or HDPCOEClinical@HDPlus.com.';
            }else if(recordTypeName == 'Lowes Joint'){
                    URL = '/apex/pbghreferral?id=' + obj.id;
                    casecontact ='1-877-885-0654 or HDPCOEClinical@HDPlus.com.';
            }else if(recordTypeName == 'Kohls Cardiac'){
                    URL = '/apex/kohlscardiacreferral?id=' + obj.id;
                    casecontact ='1-877-885-0652 or HDPCOEClinical@hdplus.com.';
            }else if(recordTypeName == 'HCR ManorCare Cardiac'){
                    URL = '/apex/hcrCardiacreferral?id=' + obj.id;
                    casecontact ='1-877-885-0651 or HDPCOEClinical@hdplus.com.';
            }else if(recordTypeName == 'PepsiCo Cardiac'){
                    URL = '/apex/pepsicoCardiacreferral?id=' + obj.id;
                    casecontact ='1-877-286-3562 or HDPCOEClinical@hdplus.com.';   
           }else if(recordTypeName == 'PepsiCo Joint'){
                    URL = '/apex/pepsicoJointreferral?id=' + obj.id;
                    casecontact ='1-877-286-3562 or HDPCOEClinical@hdplus.com.';
           }else if(recordTypeName == 'McKesson Joint'){
                    URL = '/apex/pbghreferral?id=' + obj.id;
                    casecontact ='1-877-286-3562 or HDPCOEClinical@hdplus.com.';
           }


            if(obj.displayAuthNumber__c=='N/A')
            {
             theBody = 'Hello,\n\nAttached is the Contigo Health (HDP) Program Authorization Form for the ' + obj.RecordType.Name + ' Program. Please remember that this form alone does not constitute acceptance of the patient into the program. That acceptance is contingent on acceptance of the facility’s Plan of Care and all member forms returned.\n\nIf you have questions regarding this form or the process, please contact us at ' + casecontact +'\n\nThank you,\nCare Management Team\nContigo Health';
                theSubject = recordTypeName + ' COE Program Authorization Form - ' + obj.Patient_Last_Name__c +', '+obj.Patient_First_Name__c;
            }else{ 
                theBody = 'Hello, \n\nThe HDP Authorization Number for ' + obj.Patient_Last_Name__c +', '+obj.Patient_First_Name__c + ' is ' + obj.displayAuthNumber__c + '.';
                if(isnico){ //Checking for override
                    boolean foo=false;
                    for(program_notes__c pn : [select id,Notes__c from program_notes__c where patient_case__c = :obj.id])
                    {
                        if(pn.Notes__c!=null)
                        {
                            if(pn.Notes__c == 'The continine process has been overridden.' ||
                                pn.Notes__c.contains('The reason continine test is not required, but patient is cleared to travel'))
                            {
                                foo=true;
                                break;    
                            }
                        }
                    }
                    if(!foo)
                    {
                        theBody += ' Final approval pending cotinine testing and results.';       
                    }    
                }
             
                theBody += '\n\nIf you have questions regarding this form or the process, please contact us at '+ casecontact +'\n\nThank you,\nCare Management Team\nContigo Health';
                theSubject = recordTypeName + ' COE Program Authorization Number - ' + obj.Patient_Last_Name__c +', '+obj.Patient_First_Name__c;
            } 
          
     }
        
    public void runrenderpdf(){
        renderpdf=true;

    } 
    
    public void encryptAttach(){
        Attachment a = [select body from attachment where ID = :attachID];
        blob key = EncodingUtil.base64Decode(obj.attachEncrypt__c);
        encryptAttach ea = new encryptAttach(key);
        blob thebody= a.body;
        blob encryptedbody;
        encryptedbody=ea.encryptAttachmentBlob(thebody);
        a.body=encryptedbody;
        update a;         
    }
    
    public string getURL(){
        return URL;
    }
    
    public void addtheAttachment(){
        //cause server rerender
    }

    public string geterrorMessage(){
        return errorMessage;
    }
    
    public void formSelected(){

        isSuccess =false;
        isError = false;
        error='false';
        
        id aid = pList[0].attachID__c;
        id fid = plist[0].ftpAttachment__c;
        Attachment endattachment = new Attachment();
        
        if(fid!=null){
            
            ftpAttachment__c theForm = [select subDirectory__c,fileName__c from ftpAttachment__c where id =:fid ];
            string fileUrl = fileServer__c.getInstance().baseDirectory__c+''+theForm.subDirectory__c+'/'+theform.fileName__c;
            fileUrl = fileUrl.replaceAll(':','%3A').replaceAll(',','%2C').replaceAll(' ','%20');
            
            documentStorageAttachment dsa = new documentStorageAttachment();
            documentStorageAttachment.response response = dsa.getAttachment(fileUrl);
            
            if(response.isError){
                isError=true;
                error='true';
                //ErrorCode: 2A3467B
                errormessage = 'Unable to retrieve the attached program forms. Please confirm you can open the attached program files and try again.\n\nIf you are unable to open the program files, please submit a ticket to sdsalesforce@congtiohealth.com\n';
                return;
            }
            
            endattachment = new Attachment();
            endattachment.body = response.file;
            endattachment.name = theform.fileName__c;
        }
        
        if(aid !=null && fid==null){
            endattachment = [select body, name from Attachment where id = :pList[0].attachID__c]; 
        }
        
        
        if(endattachment.body !=null){
            updateInfo();
            Program_Notes__c pn = new Program_Notes__c();
                 
                if(obj.displayAuthNumber__c=='N/A'){
                    pn.patient_case__c=theID;pn.subject__c='Auth. Form E-mailed';pn.notes__c='Authorization form e-mailed to ' + obj.client_facility__r.Authorization_Email_Contact__c; pn.type__c='E-mail';
                }else{
                    pn.patient_case__c=theID;pn.subject__c='Auth. Number E-mailed';pn.notes__c='Authorization Number e-mailed to ' + obj.client_facility__r.Authorization_Email_Contact__c; pn.type__c='E-mail';
                
                }
                
            try{
            isSuccess =true;
            insert pn;
            
            if(theCC == '' || theCC == Null){
            }else{            
            
                if(!(theCC.contains('@') && theCC.contains('.'))){
                    isError=true;
                    errorMessage = 'There seems to be an issue with the CC address.';
                }
            }
            
            if(isError ==false){
            
                list<String> theCClist = new list<String>();
                
                if(testCC!=null&&testCC!=''){
                    if(testCC.contains(','))
                    {
                        testCC = testCC.replaceAll(',', ';');
                    }
                    theCClist = testCC.split(';', -2);
                    
                }
                
                theOpp = convertCase.sendAuthEmail(obj, theTo, testSubject , testNote, theCClist, endattachment);
                error='false';
                errormessage = 'Authorization Number sent.';
                IsSuccess =true;
            }
            
            }catch (exception e){
            isError=true;
            error='true';
            
            system.debug(e.getmessage());
            Messaging.Singleemailmessage mail = utilities.email('mmartin@hdplus.com', null, null, null, 'Auth E-mail Error in ' + obj.id, e.getMessage()+'\n\n'+e.getLineNumber());
            Messaging.sendEmail(new Messaging.Singleemailmessage[]{mail});
            if(e.getMessage().contains('_EXCEPTION')){errorMessage  = utilities.errorMessageText(e.getMessage());}else{errorMessage = 'REQUIRED FIELD MISSING: ' + utilities.errorMessageTextRF(e.getMessage());}
            }
            
        }else{
            isError=true;
            error='true';
            errormessage = 'Unable to locate the program files attachment. These files should be associated with a note where the communication type is \'Auth Form.\'';
            
                        
        }
    
    }
    
    public string gettheOpp(){
        return '/' + obj.id;
    
    }
    
}