public class AmsSearchController{

    public PageReference openCase() {
        return new PageReference('/'+ ApexPages.CurrentPage().getParameters().get('openID'));
    }

    
    public string searchTerm {get; set;}
    public AmsSearch search {get; private set;}
    
    
    public AmsSearchController(){
        search();
    }
    
    public PageReference search(){
        searchTerm = ApexPages.CurrentPage().getParameters().get('st');
        ApexPages.CurrentPage().getParameters().put('st',null);
        
        search = new AmsSearch(searchTerm);
        
        if(search.AmsList != null && search.AmsList.size()==1){
            return new pagereference('/'+search.AmsList[0].id);
        }
        
        if(search.AmsList==null){
            search.AmsList = new magpie_Error__c[]{};
        }
        
        return null;
        
    }
    
}