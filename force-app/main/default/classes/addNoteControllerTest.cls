/**
 * This class contains unit tests for validating the behavior of  the Apex addNoteController class
 */

@isTest
private class addNoteControllerTest {
    static testMethod void patientCaseNote() {
        string testString = 'Test';

        Patient_Case__c pc = new Patient_Case__c();

        pc.Program_Authorization_Received__c = date.today();
        pc.Plan_of_Care_accepted_at_HDP__c = date.today();
        pc.Financial_Responsibility_Waiver__c = date.today();
        pc.bid__c = '123456';
        pc.Employee_First_Name__c = 'Johnny';
        pc.Employee_Last_Name__c = 'Tester';
        pc.Patient_DOBe__c = string.valueof(date.today().addYears(-20));
        pc.Patient_Email_Address__c = 'something@something.com';
        pc.Employee_DOBe__c = string.valueof(date.today().addYears(-20));
        pc.Employee_Primary_Health_Plan_ID__c = '12345';
        pc.Relationship_to_the_Insured__c = 'Patient is Insured';
        pc.Patient_Preferred_Phone__c = 'Home';
        pc.Patient_Gender__c = 'Male';
        pc.Patient_City__c = 'Mesa';
        pc.Patient_State__c = 'AZ';
        pc.recent_testing__c = 'rt';
        pc.caregiver_name__c = 'Johnny Tester';
        pc.RecordTypeid = [select id from RecordType where name = 'Walmart Cardiac' and isActive = true and sObjectType = 'Patient_Case__c' limit 1].id;
        pc.Client_Name__c = 'Walmart';
        insert pc;

        system.assert (pc.id != null);

        ApexPages.currentPage().getParameters().put('Id', pc.id);
        addNoteController anc = new addNoteController();

        anc.mySave();
        anc.objPN.Patient_Case__c = pc.id;
        anc.objPN.Subject__c = testString;
        anc.objPN.Notes__c = testString;
        anc.objPN.Communication_Type__c = testString;
        anc.objPN.initiated__c = testString;

        Program_Notes__c programNote = new Program_Notes__c(Patient_Case__c = pc.id, Subject__c = testString, Notes__c = testString, Communication_Type__c = testString, initiated__c = testString);
        insert programNote;

        Attachment attach = new Attachment();

        attach.Name = 'Test Attach';
        attach.Body = blob.valueof('testbody');
        anc.attachment = attach;
        attach.Description = 'Test Desc';
        anc.testtheAttach();
        anc.addtheAttachment();
        anc.objPN.Patient_Case__c = pc.id;
        anc.objPN.Subject__c = testString;
        anc.objPN.Notes__c = testString;
        anc.objPN.Communication_Type__c = testString;
        anc.objPN.initiated__c = testString;
        anc.objPN.Contact_Type__c = testString;
        anc.mySave();

        Attachment attach1 = new Attachment();

        attach1.Name = 'Test Attach';
        attach1.Body = blob.valueof('testbody');
        anc.attachment = attach1;
        attach1.Description = 'Test Desc';

        pNoteNew pn = new pNoteNew(new ApexPages.StandardController(programNote));

        pn.getPNC();
        pn.mySave();
        pn.getisEdit();
        pn.mydelete();

        attachment a = new attachment(name = 'UNIT.TEST', parentID = pc.id, body = blob.valueof('UNIT.TEST'));
        insert a;

        pn.objPC = new patient_case__c(attachEncrypt__c = EncodingUtil.base64encode(Crypto.generateAesKey(128)));
        pn.attachID = a.id;
        pn.encryptAttach();
        pn.decryptAttach();

        pn = new pNoteNew(new ApexPages.StandardController(pc));
    }
}