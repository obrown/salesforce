public class caseManagementLetter{
  
    public Case_Management_Letter__c[] cmLetters {get; private set;}
    public Case_Management_Letter__c[] deletedcmLetters {get; private set;}//8-18-22 o.brown added HDP-7023 update
    public Case_Management_Letter__c cmLetter {get; set;}   
    public boolean error {get; private set;}
    public saveResponse saveResponse {get; private set;}
    id cmRecordId;
    id clientId;
    string cmid;
    final boolean delcml = Case_Management_Letter__c.sObjectType.getDescribe().isDeletable(); //8-18-22  o.brown added HDP-7023 update
    
    public caseManagementLetter(id cmRecordId, id clientId){//8-24-22 updated typo cmecordId
    system.debug('caseManagementLetter='+cmRecordId+'client= '+clientId);
        this.cmRecordId = cmRecordId;
        this.clientId = clientId;

        cmid=string.valueof(cmRecordId);
        cmid=cmid.left(15);
        loadCMLetters();
        loadDeletedCMLetters();//8-18-22  o.brown added HDP-7023 update
    }

    public class saveResponse{
        
        public string resultCode;
        public string errorMsg;
        
        saveResponse(string resultCode){
            this.resultCode = resultCode;
        }
    }
    
    public void cancel(){
        cmLetter=null;
    }
    
    public void save(){
        saveResponse = new saveResponse('0');
        error=false;
        try{
            upsert cmLetter;
            loadCMLetters();
        }catch(dmlException e){
            saveResponse.resultCode ='2';
            error=true;
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                saveResponse.errorMsg = e.getDmlMessage(i);
                System.debug(e.getDmlMessage(i)); 
            }
            
        }
        
    }
    
    public void loadCMLetters(){
    system.debug('cmid** ='+cmid);
        cmLetters = new Case_Management_Letter__c[]{};
        /*8-18-22 o.brown removed for new update HDP-7023
        cmLetters = [select Name,Type_of_Letter__c,Signed_Date__c,Signed_by__c,Print_Date__c,Printed_By__c,createdDate, createdBy.Name from Case_Management_Letter__c where Case_Management__c = :cmid order by createdDate desc];
        */
        cmLetters = [select Name,Type_of_Letter__c,Signed_Date__c,Signed_by__c,Print_Date__c,Printed_By__c,createdDate, createdBy.Name,Type_of_Provider_Letter__c,provider_letter__c,Deleted_Date__c   from Case_Management_Letter__c where Case_Management__c = :cmid and Deleted_Date__c=null order by createdDate desc];
    
    }

    //8-18-22 new method added o.brown HDP-7023
    public void loadDeletedCMLetters(){
    system.debug('cmid** ='+cmid);
        deletedcmLetters = new Case_Management_Letter__c[]{};
        deletedcmLetters = [select Name,Type_of_Letter__c,Type_of_Provider_Letter__c,Signed_Date__c,Signed_by__c,Print_Date__c,Printed_By__c,createdDate, createdBy.Name,Deleted_Date__c,Type_of_Letter_Deleted__c,Type_of_Provider_Letter_Deleted__c,Deleted_By__c from Case_Management_Letter__c where Case_Management__c = :cmid and Deleted_Date__c!=null order by createdDate desc];
    
    }
    
    public void setCMLetterPrintDate(){

        string passId = ApexPages.CurrentPage().getParameters().get('cmLetter');
        for(Case_Management_Letter__c  cml : cmLetters){        
            if(cml.id==passId ){
                cml.Print_Date__c=date.today();
                string uName='';
                if(userinfo.getfirstname()!=''){
                    uName=userinfo.getfirstname()+' ';
                }
                uName = uName+userinfo.getlastname();
                update cml;
                loadCMLetters();
                break;
            }
        }
    }

    //8-18-22 o.brown update HDP-7023
    public void deleteTheCML(){
        if(delcml){ 
            string cmlID = ApexPages.currentPage().getparameters().get('cmlID ');
            string deletedlt=ApexPages.currentPage().getparameters().get('dlt');
            string deletedplt=ApexPages.currentPage().getparameters().get('dplt');
            
            Case_Management_Letter__c cml = new Case_Management_Letter__c(Case_Management__c=cmid);
            
            cml.Type_of_Letter_Deleted__c=deletedlt;
            cml.Type_of_Provider_Letter_Deleted__c=deletedplt;
            cml.Deleted_Date__c=datetime.now();
            cml.Deleted_By__c=UserInfo.getFirstName()+' '+UserInfo.getLastName();
        try {
              insert cml;

        } catch (dmlException e) {
          system.debug(e.getDmlMessage(0));
          return;
        }
  
            system.debug('cmlID = '+cmlID );
            system.debug('Type of letter deleted= '+cml.Type_of_Letter_Deleted__c);
            system.debug('Type of provider letter deleted= '+cml.Type_of_Provider_Letter_Deleted__c);
            system.debug('Deleted Date= '+cml.Deleted_Date__c);
            system.debug('Deleted by= '+cml.Deleted_By__c);

        try {
              delete (new Case_Management_Letter__c (id=cmlID ));

        } catch (dmlException e) {
          system.debug(e.getDmlMessage(0));
          return;
        }
            loadCMLetters();
            loadDeletedCMLetters();
            
        }
    }  
    
}