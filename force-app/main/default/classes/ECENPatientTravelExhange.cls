@RestResource(urlMapping='/ecen/v1/patienttravel')
global class ECENPatientTravelExhange{

    @HttpGet
    global static ecenPatientTravelStruct doGet() {
        String ins_id = RestContext.request.headers.get('Ins_id');
        coeMobileMessaging mClass = new coeMobileMessaging();
        coeMobileMessaging.MainInfo mainInfo = new coeMobileMessaging.MainInfo();
        
        ecenPatientTravelStruct travelStruct = new ecenPatientTravelStruct();
        
        oncology__c oncology = new oncology__c();
        
        try{
        oncology = [select client_facility__r.facility__r.name,
                           Evaluation_Estimated_Arrival__c,
                           Evaluation_Estimated_Departure__c,
                           Evaluation_Travel_Type__c,
                           Evaluation_Hotel__c,
                           Evaluation_Hotel__r.name,
                           Evaluation_Hotel_Check_in_Date__c,
                           Evaluation_Hotel_Check_Out_Date__c,
                           Evaluation_Arriving_Flight_Number__c,
                           Evaluation_Departing_Flight_Number__c,
                           Evaluation_Arriving_Flight_Date_and_Time__c,
                           Evaluation_Departing_Flight_Date__c,
                           First_Appointment_Date__c,
                           First_Appointment_Time__c,
                           Last_Appointment_Date__c,
                           Patient_First_AppointmentTime__c,
                           Patient_Last_AppointmentTime__c from oncology__c where bid_id__c = :ins_id];
                     
        }catch(queryException e){
            if(test.isRunningTest()){
                RestContext.response.responseBody= blob.valueof(JSON.serialize(travelStruct));
                
            }
            
        
        
        }
        
        if(oncology.id!=null){ 
            return ECENOncologyCaseTravel.getOncologytavel(oncology);
        }
        
        patient_case__c pc = new patient_case__c();
        
        try{
        pc = [select authSent__c,AMEX_Trip_ID__c,Arriving_Flight_Number__c,Arriving_Flight_Time__c,
                     client_facility__c,client_facility__r.facility__r.name,
                     Departing_Flight_Number__c,Departing_Flight_Time__c,
                     Estimated_Arrival__c, Estimated_Departure__c,
                     nHotel_Name__c, Hotel_Name__c,Hotel_Check_In_Date__c,Hotel_Checkout_Date__c,
                     Program_Type__c,
                     Travel_Request__c,
                     Travel_Type__c from patient_case__c where bid_id__c = :ins_id order by createdDate desc limit 1];
                     
        }catch(queryException e){
            system.debug(e.getmEssage());
            if(test.isRunningTest()){
                RestContext.response.responseBody= blob.valueof(JSON.serialize(travelStruct));
                
            }
            
        }
        
        if(pc.id!=null){ 
            return ECENPatientCaseTravel.getPCtavel(pc);
        }
        
        Bariatric_Stage__c bs = new Bariatric_Stage__c();
        
        try{
        bs = [select bariatric__r.client_facility__r.facility__r.name,
                           Arriving_Date_Time__c,
                           Arriving_Flight_Number__c,
                           Departing_Date_Time__c,
                           Departing_Flight_Number__c,
                           Hotel_Check_in__c,
                           Hotel_Check_out__c,
                           Hotel__c,
                           Travel_Type__c,
                           Evaluation_Date__c,
                           Estimated_Pre_Op__c,
                           Estimated_Hospital_Admit__c,
                           Estimated_Hospital_Discharge__c,
                           Estimated_Procedure__c,
                           Estimated_Arrival__c,
                           Estimated_Departure__c,
                           RecordType.name from Bariatric_Stage__c where bariatric__r.bid_id__c = :ins_id and recordtype.name !='Post' order by createdDate desc limit 1];
                     
        }catch(queryException e){
            if(test.isRunningTest()){
                RestContext.response.responseBody= blob.valueof(JSON.serialize(travelStruct));
                
            }
            
        
        
        }
        system.debug('bs.id ' + bs.id + ' '+bs.recordtype.name);
        if(bs.id!=null){ 
            return ECENBariatricCaseTravel.getBariatricTavel(bs);
        }
        return travelStruct;
    }
}