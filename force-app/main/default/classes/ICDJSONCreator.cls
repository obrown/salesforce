public with sharing class ICDJSONCreator {

    public String getJSON(){
        String icdName = Apexpages.currentPage().getParameters().get('icdName');
        if(icdName==null){return null;}
        if(icdName.length()<2){return null;}
        List<IcdWrapper> wrp = new List<IcdWrapper>();
        for (ICD10__c a : [Select a.Description__c, a.Name From ICD10__c a WHERE Name Like : '%'+icdName+'%' or Description__c Like : '%'+icdName+'%' ]) {               
               IcdWrapper w = new IcdWrapper (a.Name, a.Description__c);
               wrp.add(w);
        }
        return JSON.serialize(wrp);
    }

    public class IcdWrapper{
        String icdName, icdDescription;

        public IcdWrapper(String iName, String iDescription){
            icdName = iName;
            icdDescription = iDescription;
        }
    }

}