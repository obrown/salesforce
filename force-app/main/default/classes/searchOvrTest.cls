/* 

    This class contains unit tests for validating the behavior of the Apex searchOvr class

*/

@isTest
private class searchOvrTest {
    
    static testMethod void searchOvr() {
    
        OVR_Control__c ovr = new OVR_Control__c();
        ovr.client__c = 'UNIT.TEST.CLIENT';
        ovr.Comments__c = 'TEST';
        ovr.Payee__c = 'TEST';
        ovr.Payor__c = 'Health Design Plus';
        ovr.Status__c = 'Open';
        ovr.Type__c = 'Refund';
        insert ovr;
        
        Ovr_Claim__c ovrClaim = new Ovr_Claim__c(ovr_control__c=ovr.id);
        ovrClaim .claim_number__c = '123456789';
        ovrClaim .ESSN__c = '1234';
        ovrClaim .Overpayment_Amount__c = 500.00;
        insert ovrClaim;
        
        ApexPages.CurrentPage().getParameters().put('st', 'UNIT.TEST.CLIENT');
        searchOvr controller = new searchOvr();
        
        ApexPages.CurrentPage().getParameters().put('st', '123456789');
        controller = new searchOvr();
    
    }   
    
}