/**
* This class contains unit tests for validating the behavior of  the Apex sales/crm application
*/

@isTest
private class salesCRMTest {

    Lead__c theLead {get;set;}
    Meeting__c theMeeting {get; set;}
    Channel_Partner__c cp {get; set;}
    Producer__c producer  {get; set;}
    Account__c account {get; set;}
    
    void createAccount(){
        if(account==null){
            account = new account__c(name='UNIT.TEST');
            insert account;
        }
    }
    
    void createChannelPartner(){
        
        if(cp==null){
            cp = new Channel_Partner__c(name='UNIT.TEST');
            insert CP;
        }
    
    }
    
    void createProducers(){
        
        if(producer==null){
            createChannelPartner();
            producer = new Producer__c(name='UNIT.TEST',First_Name__c='UNIT',Last_Name__c='TEST', Channel_Partner__c=cp.id);
            insert producer;
        }
         
        
    }
    
    void createMeeting(boolean withOpp, boolean withCP){
        
        if(theMeeting==null){
            theMeeting = new Meeting__c();
            theMeeting.name = 'UNIT.TEST';
            theMeeting.Description__c = 'UNIT.TEST';
            insert theMeeting;
        }
        
    }
    
    void createLead(){
    
        if(theLead==null){
            theLead = new Lead__c();
            theLead.Name = 'UNIT.TEST';
            insert theLead;        
        }
        
    
    }
    
    static testMethod void testMeeting() {
    
        salesCRMTest crm = new salesCRMTest();
        crm.createMeeting(false, false);
        system.assert(crm.theMeeting!=null);
        
        crm.createProducers();
        system.assert(crm.producer!=null);
        
        ApexPages.CurrentPage().getParameters().put('id',crm.theMeeting.id);
        meetingParticipants mp = new meetingParticipants();
        
        mp.searchTerm = 'UNIT';
        mp.searchType = 'All';
        mp.search();
        
        system.assert(mp.theParticipants.size()>0);
        
        mp.selected = '{"selectedParticipants":[{"ctype":"Producer","id":"' + crm.producer.id + '"}]}';
        mp.addToSelected();
        //mp.selectedParticipants =null;
        mp.init();
        
        ApexPages.CurrentPage().getParameters().put('id',crm.theMeeting.id);
        meetingDetailext mde = new meetingDetailext(new ApexPages.StandardController(crm.theMeeting));
        
        mde.sendMeetingNoticeEmail();
        mde.Meeting.meeting_date__c = date.today();
        
        mde.sendMeetingNoticeEmail();
        
        mde.Meeting.Start_Time_Hr__c = '09';
        mde.Meeting.Start_Time_Min__c = '00';
        mde.Meeting.Start_Time_AmPm__c= 'AM';
        
        mde.sendMeetingNoticeEmail();
         
        mde.Meeting.End_Time_Hr__c = '09';
        mde.Meeting.End_Time_Min__c = '00';
        mde.Meeting.End_Time_AmPm__c= 'PM';
        
        update mde.meeting;
        
        mde = new meetingDetailext(new ApexPages.StandardController(crm.theMeeting));
        
        mde.emailSubject ='UNIT.TEST';
        mde.emailBody ='UNIT.TEST';
        
        mde.sendMeetingNoticeEmail();
        
        meeting_producer__c mproducer;
        
        for(Participant p : mp.selectedParticipants){
            
            if(p.contactType =='Producer'){
                system.debug(p);
                mproducer = new meeting_producer__c(id=p.recordID);
                break;
            }
            
        }
        system.assert(mproducer != null);
        ApexPages.CurrentPage().getParameters().put('delID',mproducer.id);
        ApexPages.CurrentPage().getParameters().put('type','Producer');
        mde.deleteProducer();
        
        
    }
    
    static testMethod void convertLead() {
        
        salesCRMTest crm = new salesCRMTest();
        crm.createLead();
        crm.createAccount();
         
        ApexPages.CurrentPage().getParameters().put('id',crm.theLead.id);
        convertLead  cl = new convertLead();
        cl.searchTerm = 'UNIT';
        cl.searchAccounts();
        
        ApexPages.CurrentPage().getParameters().put('aid',crm.account.id);
        cl.selectAcct();
        
        cl.createOpportunity();
        
        cl.theAccount = new Account__c(name = 'UNIT.TEST2');
        cl.saveAccount();
        
    }
    
    
    
    
}