public class oncologyEmployeeContactExt {
    
    public EmployeeContacts__c  ec {get; set;}
    public EmployeeContacts__c newEmployeeContact {get; set;}
    public employeeContacts__c[] employeeContacts {get; private set;}
    public boolean isError {get; private set;}
    public Oncology__c o;
    public selectOption[] RelationshipTypes {get; private set;}
    public selectOption[] employeeRelationshipTypes  {get; private set;}
    public string pdob {get;set;} 
    
    map<string, id> RelationshipTypesMap = new map<string, id>();
    string employeeKey; 
    string oId= ApexPages.currentPage().getParameters().get('oid');
    
    public oncologyEmployeeContactExt () {}
    public oncologyEmployeeContactExt (ApexPages.StandardController controller) {
        
    ec = (EmployeeContacts__c )controller.getRecord(); 
    
       o= new Oncology__c ();
       o=[select Client__c,Carrier__c,Client_Name__c,Client_Facility__c,id,EmployeeID__c,Patient_DOB__c,Patient_First_Name__c,patient_last_name__c,Patient_Medical_Plan_Number__c,Patient_Address__c,Patient_City__c,Patient_State__c,Patient_Zip_Code__c,Additional_diagnostics_outside_bundle__c,Patient_Email_Address__c,Referral_Date__c,procedure__c from  Oncology__c where id= :oId];
       ec.EmployerID__c=o.Client__c;
       
       loadEmployeeRelationshipTypes();
       
     if(ec.id==null){
        ec.StreetAddress__c=o.Patient_Address__c;
        ec.City__c=o.Patient_City__c;
        ec.State__c=o.Patient_State__c;
        ec.ZipCode__c=o.Patient_Zip_Code__c;
        ec.EmployerID__c=o.Client__c;
        
     }
           
    }
   
    public void saveEmployeeContact(){
        isError=false;
        try{
            upsert ec;
            system.debug(ec.id);
            system.debug(o);
            system.debug(ec.EmployeeRelationShip__c);
            createRelatedToEmployee(ec.id, o.employeeID__c, ec.EmployeeRelationShip__c);
            
        }catch(dmlexception e){
            isError=true;
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getDmlMessage(i)));
            }              
            
        }
    }

    void createRelatedToEmployee(id employeeContactId, id relatedToEmployeeId, id EmployeeRelationID){
        RelatedToEmployee__c rte = new RelatedToEmployee__c();
        try{
            rte = [select id from RelatedToEmployee__c where employeeContactId__c=:employeeContactId and RelatedEmployeeID__c=:relatedToEmployeeId and EmployeeRelationID__c=:EmployeeRelationID];
        }catch(queryException qe){
            //catch silently
        }
        database.upsert(new RelatedToEmployee__c(id=rte.id,employeeContactId__c=employeeContactId, RelatedEmployeeID__c=relatedToEmployeeId , EmployeeRelationID__c=EmployeeRelationID));
        
    } 
    void loadEmployeeRelationshipTypes (){
        if(employeeRelationshipTypes ==null){
            RelationshipTypes = new selectOption[]{};
            RelationshipTypes .add(new selectOption('', '-- None --'));
            
            employeeRelationshipTypes = new selectOption[]{};
            employeeRelationshipTypes.add(new selectOption('', '-- None --'));
            for(EmployeeRelationShip__c er : [select id, name from EmployeeRelationShip__c]){
                RelationshipTypesMap.put(er.name, er.id);
                RelationshipTypes.add(new selectOption(er.name, er.name));
                if(er.name !='Employee'){
                    employeeRelationshipTypes.add(new selectOption(er.id, er.name));
                }
            
            }
        }    
    } 
    
   
}