public with sharing class coeDashboard{
    
    transient patient_case__c[] jointList = new patient_case__c[]{};
    transient patient_case__c[] spineList = new patient_case__c[]{};
    transient patient_case__c[] cardiacList = new patient_case__c[]{};
    
    transient coeDashboardOpenCase openClass;
    transient coeDashboardHospitalInventory hospitalInventory;
    
    public map<string, set<string>> caseTypeMap {get; private set;}
    public set<string> hospitalMap {get; private set;}
    
    public transient map<string, map<string, integer>> OpenHICount {get; private set;}
    public set<string> hospitalInventorySet {get; private set;}
    public transient map<string, integer> OpenHITotal {get; private set;}
    public map<string, map<string, patient_case__c[]>> OpenHI {get; private set;}
    
    public transient map<string, map<string, integer>> OpenJointCount {get; private set;}
    public transient map<string, integer> OpenJointTotal {get; private set;}
    public map<string, map<string, patient_case__c[]>> OpenJoint {get; private set;}
    
    public transient map<string, map<string, integer>> OpenSpineCount {get; private set;}
    public transient map<string, integer> OpenSpineTotal {get; private set;}
    public map<string, map<string, patient_case__c[]>> OpenSpine {get; private set;}
    
    public transient map<string, map<string, integer>> OpenCardiacCount {get; private set;}
    public transient map<string, integer> OpenCardiacTotal {get; private set;}
    public map<string, map<string, patient_case__c[]>> OpenCardiac {get; private set;}
    
    public string ojStart {get; set;}
    public string ojEnd {get; set;}
    
    //public ApexPages.StandardSetController sc {get; private set;}
    //public transient patient_case__c[]  detailList {get; private set;}
    public transient string tableType {get; private set;}
    public boolean showall {get; set;}
    
    Public Integer noOfRecords{get; set;}
    Public Integer size{get;set;}
    
    id coeID;
    
    public coeDashboard(){
        
        coeID = [select id from group where name = 'HDP Care Management COE'].id;
        caseTypeMap = new map<string, set<string>>();
        //sc = new ApexPages.StandardSetController(new patient_case__c[]{});
        
        for(RecordType r : [select name from recordtype where sObjectType = 'Patient_case__c' and isActive= true]){
            
            if(r.name.indexOf('Joint')>-1){
                if(r.name.indexof('PepsiCo')<0){
                set<string> foo = caseTypeMap.get('Joint');
                if(foo==null){foo=new set<string>();}
                foo.add(r.name.left(r.name.indexOf('Joint')-1));
                caseTypeMap.put('Joint',foo); 
                }
            }else if(r.name.indexOf('Spine')>-1){
                set<string> foo = caseTypeMap.get('Spine');
                if(foo==null){foo=new set<string>();}
                foo.add(r.name.left(r.name.indexOf('Spine')-1));
                caseTypeMap.put('Spine',foo); 
            
            }else if(r.name.indexOf('Cardiac')>-1){
                set<string> foo = caseTypeMap.get('Cardiac');
                if(foo==null){foo=new set<string>();}
                foo.add(r.name.left(r.name.indexOf('Cardiac')-1));
                caseTypeMap.put('Cardiac',foo); 
            
            }
        
        
        }

        
        
        setOpenSpine();
        setOpenCardic();
        setOpenJoint();
        hospitalInventory();
    }
    
    /* Pagination */ 

    
    public ApexPages.StandardSetController sc {
        get{
        string scType = ApexPages.CurrentPage().getParameters().get('scType');
        string rType = ApexPages.CurrentPage().getParameters().get('rType');
        string client = ApexPages.CurrentPage().getParameters().get('client');
        
        if(scType!=null && rType!=null){
            
            boolean oc = false;
            tableType = scType +' ' + rType;
            map<string, map<string, patient_case__c[]>> fooMapMap;
            Patient_Case__c[] fooList= new Patient_Case__c[]{} ;
            
            if(scType=='Joint'){
                fooMapMap = OpenJoint;
                oc = true;
            }else if(scType=='Spine'){
                fooMapMap = OpenSpine;
                oc = true;
            }else if(scType=='Cardiac'){
                fooMapMap = OpenCardiac;
                oc= true;
            }else if(scType=='In House' || scType=='Awaiting Claim' || scType=='Awaiting POC'){
                fooMapMap = OpenHI;
            }
            
            if(oc){
             
                for(string s: fooMapMap.get(rType).keySet()){
                    for(Patient_Case__c pc : fooMapMap.get(rType).get(s)){
                        fooList.add(pc);
                    }
                
                }
            
            }else{
            
                for(string s: fooMapMap.get(scType).keySet()){
                    for(Patient_Case__c pc : fooMapMap.get(scType).get(s)){
                        fooList.add(pc);
                    }
                }
            
            }
            
            if(client!=null && client!=''){
                patient_case__c[] fooBarList = new patient_case__c[]{};
                for(patient_case__c pc : fooList){
                    if(pc.client_name__c == client){
                        fooBarList.add(pc);
                    }
                }
                fooList = fooBarList;
            }
            
            sc = new ApexPages.StandardSetController(fooList);
            size=10;
            sc.setPageSize(size);
            noOfRecords = sc.getResultSize();
        
        
        }return sc;
        }set;
        
    }
    
    public pageReference setSC() {
        sc = null;
        getDetailList();
        sc.setPageNumber(1);
        return null;
    }
    
    public patient_case__c[] getDetailList(){
        patient_case__c[] pcList = new patient_case__c[]{};
        
        if(sc!=null){
        
        for(patient_case__c pc : (patient_case__c[])sc.getRecords())
            pcList.add(pc);
        }    
        return pcList;
    }
    
    public void showAll(){
        sc.setPageSize(sc.getResultSize());
    }
    
    public void showDefault(){
        size=10;
        sc.setPageSize(size);
        
    }
    
    /* End Pagination */
    
    /* Open Cardiac */

    void setOpenCardic(){
        
        for(patient_case__c p : [select case_type__c, ownerid, referred_facility__c, program_type__c, actual_arrival__c,converted_date__c,client_name__c,isConverted__c,Status__c,status_reason__c,CreatedDate,Name,lastModifiedDate, patient_first_name__c, patient_last_name__c from patient_case__c where program_type__c='Cardiac' and Status__c = 'Open' order by client_name__c]){
            cardiacList.add(p);
        }
        
        openClass= new coeDashboardOpenCase(cardiacList, coeID, caseTypeMap.get('Cardiac'));
        openClass.populateOpenCase();
        OpenCardiacCount = openClass.OpenCaseCount;
        OpenCardiacTotal = openClass.OpenCaseTotal;
        OpenCardiac = openClass.OpenCase;
        
    }
    
    public void setOpenCardiacDates(){
        string sojDate = ApexPages.currentPage().getParameters().get('sojDate');
        string eojDate = ApexPages.currentPage().getParameters().get('eojDate');
        
        ojStart = sojDate;
        ojEnd = eojDate;
        
        cardiacList= new patient_case__c[]{};
        
        for(patient_case__c p : [select case_type__c, ownerid,referred_facility__c,program_type__c, actual_arrival__c,converted_date__c,client_name__c,isConverted__c,Status__c,status_reason__c,CreatedDate,Name,lastModifiedDate, patient_first_name__c, patient_last_name__c from patient_case__c where program_type__c='Cardiac' and Status__c = 'Open' and createdDate >= :date.valueof(sojdate) and createdDate <= :date.valueof(eojdate) order by client_name__c]){
            cardiacList.add(p);
          
        }
        
        openClass= new coeDashboardOpenCase(cardiacList, coeID, caseTypeMap.get('Cardiac'));
        openClass.populateOpenCase();
        OpenCardiacCount = openClass.OpenCaseCount;
        OpenCardiacTotal = openClass.OpenCaseTotal;
        OpenCardiac = openClass.OpenCase;
        
    }
    
    /* End Open Cardiac */
    
    /* Open Spine */

    void setOpenSpine(){
        for(patient_case__c p : [select case_type__c, ownerid ,referred_facility__c,actual_arrival__c,converted_date__c, program_type__c, client_name__c,isConverted__c,Status__c,status_reason__c,CreatedDate,Name,lastModifiedDate, patient_first_name__c, patient_last_name__c from patient_case__c where program_type__c='Spine' and Status__c = 'Open' order by client_name__c]){
             spineList.add(p);
        }
        
        openClass= new coeDashboardOpenCase(spineList, coeID, caseTypeMap.get('Spine'));
        openClass.populateOpenCase();
        OpenSpineCount = openClass.OpenCaseCount;
        OpenSpineTotal = openClass.OpenCaseTotal;
        OpenSpine = openClass.OpenCase;
        
    }

    public void setOpenSpineDates(){
        string sojDate = ApexPages.currentPage().getParameters().get('sojDate');
        string eojDate = ApexPages.currentPage().getParameters().get('eojDate');
        
        ojStart = sojDate;
        ojEnd = eojDate;
        
        spineList= new patient_case__c[]{};
        
        for(patient_case__c p : [select case_type__c, ownerid ,referred_facility__c,actual_arrival__c,converted_date__c,program_type__c, client_name__c,isConverted__c,Status__c,status_reason__c,CreatedDate,Name,lastModifiedDate, patient_first_name__c, patient_last_name__c from patient_case__c where program_type__c='Spine' and Status__c = 'Open' and createdDate >= :date.valueof(sojdate) and createdDate <= :date.valueof(eojdate) order by client_name__c]){
            spineList.add(p);
        }
        
        openClass= new coeDashboardOpenCase(spineList, coeID, caseTypeMap.get('Spine'));
        openClass.populateOpenCase();
        OpenSpineCount = openClass.OpenCaseCount;
        OpenSpineTotal = openClass.OpenCaseTotal;
        OpenSpine = openClass.OpenCase;
        
    }
    
    /* End Open Spine */
    
    /* Open Joint */
    
    void setOpenJoint(){
        for(patient_case__c p : [select case_type__c, ownerid ,referred_facility__c,actual_arrival__c,converted_date__c,program_type__c, client_name__c,isConverted__c,Status__c,status_reason__c,CreatedDate,Name,lastModifiedDate, patient_first_name__c, patient_last_name__c from patient_case__c where program_type__c='Joint' and client_name__c != 'PepsiCo' and Status__c = 'Open' order by client_name__c]){
              jointList.add(p);
        
        }
        
        openClass= new coeDashboardOpenCase(jointList, coeID, caseTypeMap.get('Joint'));
        openClass.populateOpenCase();
        OpenJointCount = openClass.OpenCaseCount;
        OpenJointTotal = openClass.OpenCaseTotal;
        OpenJoint = openClass.OpenCase;
        
        
    }
    
    public void setOpenJointDates(){
        string sojDate = ApexPages.currentPage().getParameters().get('sojDate');
        string eojDate = ApexPages.currentPage().getParameters().get('eojDate');
        
        ojStart = sojDate;
        ojEnd = eojDate;
        
        jointList = new patient_case__c[]{};
        
        for(patient_case__c p :[select case_type__c,referred_facility__c, ownerid ,program_type__c,actual_arrival__c, converted_date__c,client_name__c,isConverted__c,Status__c,status_reason__c,CreatedDate,Name,lastModifiedDate, patient_first_name__c, patient_last_name__c from patient_case__c where program_type__c='Joint' and Status__c = 'Open' and client_name__c != 'PepsiCo' and createdDate >= :date.valueof(sojdate) and createdDate <= :date.valueof(eojdate) order by client_name__c]){
            jointList.add(p);
        }
        
        openClass= new coeDashboardOpenCase(jointList, coeID, caseTypeMap.get('Joint'));
        openClass.populateOpenCase();
        OpenJointCount = openClass.OpenCaseCount;
        OpenJointTotal = openClass.OpenCaseTotal;
        OpenJoint = openClass.OpenCase;
        
    }
    
    /*
    public void setOpenJointGrouping(){
        grouping = ApexPages.currentPage().getParameters().get('sortByOJ');
        populateOpenJoint();
    }
    */

    /* End Open Joint */
    
    /* Hospital Inventory */
    
    
    public void hospitalInventory(){
    
        hospitalInventory= new coeDashboardHospitalInventory();
        hospitalInventory.populateOpenCase();
        OpenHICount = hospitalInventory.OpenCaseCount;
        OpenHITotal = hospitalInventory.OpenCaseTotal;
        hospitalInventorySet = hospitalInventory.openCaseSet;
        hospitalMap = new set<string>();
        for(string s : hospitalInventorySet){
        
            hospitalMap.add(s);
        
        }
        OpenHI = hospitalInventory.OpenCase;
        
    
    
    }
    
    /* End Hospital Inventory */
}