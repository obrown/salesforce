public without sharing class cciSelectlist {
    //6-13-22 HDP-7340 o.brown added-----------------------start---------------------------
    public boolean wkDeptSet{
        get; private set;
    } 
    public string wkDeptValue{
        get; private set;
    }     
    //6-13-22 o.brown added-----------------------end---------------------------
    
    public cciSelectlist(ApexPages.StandardController controller) {
        id cci_id = ApexPages.CurrentPage().getParameters().get('icciId');

        system.debug('cci_id ' + cci_id);
        if (cci_id != null) {
            Customer_Care_Inquiry__c c = [select
                                          SA_InquiryClosed_Reason__c,
                                          SA_Inquiry_Reason__c,
                                          SA_Inquiry_Work_Department__c,
                                          SA_Inquiry_Work_Task__r.CC_Inquiry_Work_Department__r.name,//6-13-22 added SA_Inquiry_Work_Task__r.CC_Inquiry_Work_Department__r.name
                                          SA_Inquiry_Work_Task__c from Customer_Care_Inquiry__c where id = :cci_id];
            system.debug('c ' + c);

            //6-13-22 HDP-7340 o.brown added-----------------------start---------------------------
            wkDeptSet=false;
            
            If(c.SA_Inquiry_Work_Department__c!=null){
              wkDeptValue=c.SA_Inquiry_Work_Task__r.CC_Inquiry_Work_Department__r.name;
              system.debug('wkDeptValue='+wkDeptValue);
            }

            If(c.SA_Inquiry_Work_Task__r.CC_Inquiry_Work_Department__r.name!=null){
              wkDeptSet=true;
              system.debug('wkDeptSet-2='+wkDeptSet);
            }
            //6-13-22 o.brown added-----------------------end---------------------------
            
            ApexPages.currentPage().getparameters().put('inquiryReason', c.SA_Inquiry_Reason__c);
            ApexPages.currentPage().getparameters().put('closedReason', c.SA_InquiryClosed_Reason__c);
            ApexPages.currentPage().getparameters().put('workDepartment', c.SA_Inquiry_Work_Department__c);
            ApexPages.currentPage().getparameters().put('workTask', c.SA_Inquiry_Work_Task__c);
        }
        setup_cciSelectlist();
    }

      //public map<string , string> workTaskReasonMap {get;set;}
    public map<string, string> inquiryReasonMap {
        get; set;
    }
    public map<string, string> closedReasonMap {
        get; set;
    }
    public map<string, string> workDepartmentMap {
        get; set;
    }
    public map<string, string> workTaskMap {
        get; set;
    }
    public map<string, string> workTaskNameMap;

      //public selectOption[] worktaskReason {get; set;}
    public selectOption[] inquiryReason {
        get; set;
    }
    public selectOption[] closedReason {
        get; set;
    }
    public selectOption[] workDepartment {
        get; set;
    }
    public selectOption[] workTask {
        get; set;
    }

    boolean isRelated;
      //public integer workTaskReasonCount {get; private set;}
    public integer inquiryReasonCount {
        get; private set;
    }
      //public string worktaskReasonValue {get; set;}
    public string inquiryReasonValue {
        get; set;
    }
    public string closedReasonValue {
        get; set;
    }
    public string workDepartmentValue {
        get; set;
    }
    public string workTaskValue {
        get; set;
    }

    public cciSelectlist(string inquiryReasonValue, string inquiryWorkDepartmentValueID, string inquiryWorkTaskValueID){
        this.isRelated = isRelated;
        getTheInquiryReason();
        this.inquiryReasonValue = inquiryReasonValue;

        getTheClosedReason();
        this.closedReasonValue = closedReasonValue;

        getTheworkDepartment();
        this.workDepartmentValue = inquiryWorkDepartmentValueID;

        getTheworkTask();
        this.workTaskValue = inquiryWorkTaskValueID;
    }

    public void setup_cciSelectlist(){
        inquiryReasonValue = ApexPages.currentPage().getParameters().get('inquiryReason');
        ApexPages.currentPage().getParameters().put('inquiryReason', null);

        closedReasonValue = ApexPages.currentPage().getParameters().get('closedReason');
        ApexPages.currentPage().getParameters().put('closedReason', null);

        this.workDepartmentValue = ApexPages.currentPage().getParameters().get('workDepartment');
        system.debug('workDepartmentValue ' + this.workDepartmentValue);
        ApexPages.currentPage().getParameters().put('workDepartment', null);

        workTaskValue = ApexPages.currentPage().getParameters().get('workTask');
        ApexPages.currentPage().getParameters().put('workTask', null);

        this.isRelated = isRelated;
        getTheInquiryReason();

        getTheClosedReason();
        if (closedReasonValue != null) {
            for (selectOption so : closedReason) {
                if (so.getlabel() == closedReasonValue) {
                    closedReasonValue = so.getvalue(); break;
                }
            }
        }
        this.closedReasonValue = closedReasonValue;

        getTheworkDepartment();
        if (workDepartment != null) {
            for (selectOption so : workDepartment) {
                if (so.getlabel() == workDepartmentValue) {
                    workDepartmentValue = so.getvalue(); break;
                }
            }
        }

        getTheworkTask();
        if (workTask != null) {
            for (selectOption so : workTask) {
                if (so.getlabel() == workTaskValue) {
                    workTaskValue = so.getvalue(); break;
                }
            }
        }
    }

    public cciSelectlist(){
        isRelated = false;
        getTheInquiryReason();
        this.inquiryReasonValue = inquiryReasonValue;

        getTheClosedReason();
        this.closedReasonValue = closedReasonValue;

        getTheworkDepartment();
        this.workDepartmentValue = workDepartmentValue;
    }

    public string getInquiryReasonName(string val){
        return inquiryReasonMap.get(val);
    }

    public string getClosedReasonName(string val){
        return closedReasonMap.get(val);
    }

    public void setInquiryReason(){
        try{
            id idFoo = id.valueof(ApexPages.CurrentPage().getParameters().get('inquiryReasonId'));
            inquiryReasonValue = string.valueof(idFoo);
        }catch (exception e) {
        }
    }

    public void setWorkDepartment(){
        try{
            id idFoo = id.valueof(ApexPages.CurrentPage().getParameters().get('workDepartmentId'));
            workDepartmentValue = string.valueof(idFoo);
            getTheworkTask();
        }catch (exception e) {
            clearClosedReason();
            workDepartmentValue = null;
            getTheworkTask();
        }
    }

    public void setWorkTask(){
        try{
            id idFoo = id.valueof(ApexPages.CurrentPage().getParameters().get('workTasktId'));
            workTaskValue = string.valueof(idFoo);
        }catch (exception e) {
            clearClosedReason();
        }
    }
    public void setclosedReason(){
    }

    public void clearInquiryReason(){
        inquiryReason = new selectoption[] {
            new selectoption('', '--None--')
        };
        inquiryReasonValue = '';
    }

    public void clearClosedReason(){
        closedReason = new selectoption[] {
            new selectoption('', '--None--')
        };
        closedReasonValue = '';
    }

    /* Take in workTaskValue, query for ewt work task reasons
     * based on the parent worktask id which should already be set
     *
     * We could possibly store a map of the values
     *
     */

    public selectoption[] getTheInquiryReason(){
          //workTaskReasonMap = new map<string , string>();
        inquiryReasonMap = new map<string, string>();
        inquiryReason = new selectoption[] {
            new selectoption('', '--None--')
        };
        inquiryReasonCount = 0;
        set<SelectOption> optionsSet = new Set<SelectOption>();

        for (CC_Inquiry_Reason__c ccir : [select name from CC_Inquiry_Reason__c order by name asc]) {
            optionsSet.add(new selectoption(ccir.id, ccir.name));
            inquiryReasonMap.put(ccir.id, ccir.name);
            inquiryReasonCount++;
        }

        getTheClosedReason();

        inquiryReason.addAll(optionsSet);

        if (inquiryReason == null) {
            inquiryReason = new selectoption[] {
                new selectoption('', '--None--')
            };
        }

        return inquiryReason;
    }

    public selectoption[] getTheClosedReason(){
        closedReasonMap = new map<string, string>();
        closedReason = new selectoption[] {};
        closedReason.add(new selectoption('', '--None--'));

        for (CC_Closed_Reason__c ccicr : [select name from CC_Closed_Reason__c order by name asc]) {
            closedReason.add(new selectoption(ccicr.id, ccicr.name));
            closedReasonMap.put(ccicr.id, ccicr.name);
        }

        return closedReason;
    }

    public selectoption[] getTheworkDepartment(){
        workDepartmentMap = new map<string, string>();
        workDepartment = new selectoption[] {};
        workDepartment.add(new selectoption('', '--None--'));

        for (CC_Inquiry_Work_Department__c cciwd : [select name from CC_Inquiry_Work_Department__c order by name asc]) {
            workDepartment.add(new selectoption(cciwd.id, cciwd.name));
            workDepartmentMap.put(cciwd.id, cciwd.name);
        }

        return workDepartment;
    }

    public selectoption[] getTheworkTask(){
        workTaskMap = new map<string, string>();
        workTaskNameMap = new map<string, string>();
        workTask = new selectoption[] {};
        workTask.add(new selectoption('', '--None--'));
        system.debug('workDepartmentValue ' + workDepartmentValue);
        for (CC_Inquiry_Work_Task__c cciwt : [select name, CC_Inquiry_Work_Department__c from CC_Inquiry_Work_Task__c order by name asc]) {
            workTaskNameMap.put(cciwt.id, cciwt.name);
            if (workDepartmentValue != '' && cciwt.CC_Inquiry_Work_Department__c == workDepartmentValue) {
                workTask.add(new selectoption(cciwt.id, cciwt.name));
                workTaskMap.put(cciwt.id, cciwt.name);
            }
        }

        return workTask;
    }

    public void reset(){
        inquiryReasonValue = '';

        closedReason = new selectOption[] {
            new selectOption('', '--None--')
        };
        inquiryReason = new selectOption[] {
            new selectOption('', '--None--')
        };
    }
}