public with sharing class recentItemsLifestart{

set<id> pIDs = new set<id>();

public ri[] riList {get; private set;}

public recentItemsLifestart(){
    run();
}

void run(){
riList = new ri[]{};
integer count=0;
integer fcount=0;
for(RecentlyViewed x : [SELECT Id, Type FROM RecentlyViewed WHERE LastViewedDate != NULL and Type = 'Lifestart__c' ORDER BY LastReferencedDate DESC limit 50]){
                     
                     string foo = string.valueof(x.get('Id'));
                     pIDs.add(foo);
               
}

if(pIDs.size()>0){
    
    for(Lifestart__c p :[select patient_first_name__c,patient_last_name__c from Lifestart__c where id IN :pIDs and parent__c = null]){
        riList.add(new ri(p.id, p.patient_last_name__c+', '+ p.patient_first_name__c));
    }
}


}
public class ri{
    
    public id theID {get; private set;}
    public string Name {get; private set;}
    public string logoURL {get; private set;}
    
    ri(id i, string d){
        this.name = d;
        this.theID = i;
        this.logoURL = 'https://c.cs20.content.force.com/servlet/servlet.ImageServer?id=015m0000000Ytla&amp;oid=00Dm00000000VIz&amp;lastMod=1437152056000';
        
    }
    
}

}