public without sharing class oncolcogyAutoStatusing{

    
    public static void autoStatus(oncology__c[] oList){
        
        string[] reqtoReferred = new string[]{};
            
        for(oncology__c o :olist){
            
            reqtoReferred.clear();
            
            if(o.id==null){
                o.status__c = 'Open';
                o.status_reason__c = 'New';
                o.sub_status_reason__c= 'N/A';
                o.Case_Stage__c='Intake';
                o.Patient_Status_Color__c='Green';
                continue;
            }
            
            
            if(o.status__c == 'Closed' && o.status_reason__c == 'Infectious Disease' &&(o.sub_status_reason__c==null||o.sub_status_reason__c=='Infectious Disease')||
               o.status__c == 'Pending' && o.status_reason__c == 'Infectious Disease' &&(o.sub_status_reason__c==null||o.sub_status_reason__c=='Infectious Disease')){
               o.sub_status_reason__c= 'Infectious Disease';
                continue;
            }

            if((o.status__c == 'Closed'||o.status__c == 'Pending') && o.status_reason__c == 'Infectious Disease'&&o.sub_status_reason__c=='Disengaged'){
                o.sub_status_reason__c= 'Disengaged';
                continue;
            }             

            if((o.status__c == 'Closed'||o.status__c == 'Pending') && o.status_reason__c == 'Infectious Disease'&&o.sub_status_reason__c=='Local Hospital'){
                o.sub_status_reason__c= 'Local Hospital';
                continue;
            }                       
            
            if((o.status__c == 'Closed'||o.status__c == 'Pending') && o.status_reason__c == 'Infectious Disease'&&o.sub_status_reason__c=='Patient Declined'){
                o.sub_status_reason__c= 'Patient Declined';
                continue;
            } 

            if((o.status__c == 'Closed'||o.status__c == 'Pending') && o.status_reason__c == 'Infectious Disease'&&o.sub_status_reason__c=='Reduced Symptoms'){
                o.sub_status_reason__c= 'Reduced Symptoms';
                continue;
            }
                        
            if(o.status__c == 'Closed' && o.status_reason__c == 'Cancelled by Patient' ||
               o.status__c == 'Complete' && o.status_reason__c == 'Claim Paid' ||
               o.status__c == 'Closed' && o.status_reason__c == 'Benefit Eligibility'){
                continue;
            }
                        
            if(o.Referral_Date__c!=null && o.Evaluation_Estimated_Arrival__c>=date.today() && (o.Evaluation_Estimated_Departure__c==null || o.Evaluation_Estimated_Departure__c<=date.today().addDays(1)) ){
                o.status_reason__c = 'Patient at COE';
                o.sub_status_reason__c= 'N/A';
                continue;
            }
            
            if(o.Referral_Date__c!=null && o.Evaluation_Actual_Arrival__c>=date.today() && (o.Evaluation_Actual_Departure__c==null || o.Evaluation_Actual_Departure__c<=date.today().addDays(1)) ){
                o.status_reason__c = 'Patient at COE';
                o.sub_status_reason__c= 'N/A';
                continue;
            }
          
            if(o.Referral_Date__c!=null && o.Last_Claim_Received_Date__c!=null){
                o.status_reason__c = 'Claim Received';
                o.sub_status_reason__c= 'N/A';
                continue;
            }
            
            if(o.Referral_Date__c!=null && o.Evaluation_Estimated_Departure__c<=date.today().addDays(1) && o.Last_Claim_Paid_Date__c==null ){
                o.status_reason__c = 'Claim Pending';
                o.sub_status_reason__c= 'N/A';
                continue;
            }
            
            if(o.Patient_Agrees_to_Referral__c=='No' && o.Referral_Date__c==null&& o.status__c=='Open'){
                o.status__c = 'Closed';
                o.status_reason__c = 'Cancelled by Patient';
                o.sub_status_reason__c= 'N/A';
                o.Case_Stage__c='Intake';
                o.Patient_Status_Color__c='Red';
                continue;            
            } 

            if(o.hasLocalPCP__c==0 && o.status__c=='Open' && o.Patient_Agrees_to_Referral__c!='No' && o.Referral_Date__c==null){
                o.status_reason__c = 'Awaiting Referral';
                o.sub_status_reason__c= 'Local PCP';
                o.Case_Stage__c='Intake';
                continue;            
            }  
  
            if(o.hasLocalPCP__c>0 && o.status__c=='Open'&& o.Patient_Agrees_to_Referral__c!='No' && o.Referral_Date__c==null&&o.Date_Eligibility_Verified__c==null){
                o.status_reason__c = 'Awaiting Referral';
                o.sub_status_reason__c= 'Benefit Eligibility';
                o.Case_Stage__c='Intake';
                o.Patient_Status_Color__c='Green';
                continue;            
            }
            
            reqtoReferred = oncologyRequiredToRefer.checkRequirements(o);
            
            if(o.Referral_Date__c==null && o.status__c=='Open' && !reqtoReferred.isEmpty()){
                o.status_reason__c = 'Awaiting Referral';
                o.sub_status_reason__c= 'Intake Incomplete';
                o.Case_Stage__c='Intake';
                o.Patient_Status_Color__c='Green';
                continue;            
            }   
            
            if(reqtoReferred.isEmpty() && o.Patient_Agrees_to_Referral__c=='Yes' && o.status__c=='Open' && o.hasLocalPCP__c>0 && o.Referral_Date__c==null){
                o.status_reason__c = 'Awaiting Referral';
                o.sub_status_reason__c= 'Intake Complete';
                o.Case_Stage__c='Intake';
                o.Patient_Status_Color__c='Green';
                continue;            
            }           
            
            if(o.Eligibility_Verification__c =='Yes' && o.Referral_Date__c!=null && o.Determination_Outcome__c==null && o.Patient_Agrees_to_Referral__c!='No' && o.status__c=='Open'){
                o.status_reason__c = 'Awaiting Program Eligibility';
                o.sub_status_reason__c= 'N/A';
                o.Case_Stage__c='Referred';
                o.Patient_Status_Color__c='Green';
                continue;            
            }            
            
            if(o.Referral_Date__c!=null&& o.Determination_Outcome__c=='Program Eligibility Met' && o.Patient_Agrees_to_Referral__c!='No' && o.Facility_Plan_of_Care_Submission_Date__c==null && o.status__c=='Open'){
                o.status_reason__c = 'Awaiting Plan of Care';
                o.sub_status_reason__c= 'N/A';
                o.Case_Stage__c='Accepted by Facility';
                o.Patient_Status_Color__c='Green';
                continue;            
            }            
            
            if(o.Referral_Date__c!=null && o.Plan_of_Care_Accepted_Date__c!=null && o.Date_HDP_Forms_Completed__c==null && o.status__c=='Open'){
                o.status_reason__c = 'Awaiting Forms';
                o.sub_status_reason__c= 'N/A';
                continue;            
            }            
            
            if(o.Referral_Date__c!=null && o.Facility_Plan_of_Care_Submission_Date__c!=null && o.Plan_of_Care_Accepted_Date__c ==null && o.Patient_Agrees_to_Referral__c!='No' && o.Patient_FAT_Hours__c==null && o.Proposed_Service_Type__c!='Virtual Evaluation' && o.status__c=='Open'){
                o.status_reason__c = 'Awaiting Acceptance of Plan of Care';
                o.sub_status_reason__c= 'N/A';
                continue;            
            }
            
            
            
            if(o.Plan_of_Care_Accepted_Date__c  != null && o.Evaluation_Travel_Type__c ==null && o.Evaluation_Estimated_Arrival__c== null && o.Evaluation_Estimated_Departure__c== null && o.Evaluation_Actual_Arrival__c== null && o.Evaluation_Actual_Departure__c== null && o.Referral_Date__c!=null && o.Facility_Plan_of_Care_Submission_Date__c!=null && o.Patient_Agrees_to_Referral__c!='No' && o.Patient_FAT_Hours__c!=null && o.Proposed_Service_Type__c=='In-Person Evaluation'){
                o.status_reason__c = 'Awaiting Travel / InPerson Eval Confirmation';
                o.sub_status_reason__c= 'N/A';
                continue;            
            }            
            
            //if(o.Referral_Date__c!=null && o.Proposed_Service_Type__c=='Virtual Evaluation' && o.Plan_of_Care_Accepted_Date__c!=null && o.Evaluation_Date__c!=null && o.Patient_Agree_to_POC__c=='Yes' && o.status__c=='Open'){
            if(o.Referral_Date__c!=null && o.Proposed_Service_Type__c=='Virtual Evaluation' && o.Plan_of_Care_Accepted_Date__c!=null && o.Evaluation_Date_1__c!=null && o.Patient_Agree_to_POC__c=='Yes' && o.status__c=='Open'){
                o.status_reason__c = 'Ready for Virtual Eval';
                o.sub_status_reason__c= 'N/A';
                continue;            
            }
            
            if(o.Referral_Date__c!=null && o.Proposed_Service_Type__c=='In-Person Evaluation' && o.Patient_Agree_to_POC__c=='Yes' && (o.Evaluation_Estimated_Arrival__c!=null || o.Evaluation_estimated_departure__c!=null) && o.Evaluation_Travel_Type__c!='' && o.status__c=='Open'){
                o.status_reason__c = 'Ready for Arrival / InPerson Eval';
                o.sub_status_reason__c= 'N/A';
                continue;            
            }
            
            if(o.Referral_Date__c!=null && o.Proposed_Service_Type__c=='Virtual Evaluation' && o.Plan_of_Care_Accepted_Date__c!=null && o.Patient_FAT_Hours__c!=null && o.status__c=='Open'){
                o.status_reason__c = 'Awaiting Virtual Evaluation Confirmation';
                o.sub_status_reason__c= 'N/A';
                continue;            
            }            
            
            if(o.id!=null && o.Referral_Date__c!=null&& o.Determination_Outcome__c=='Program Eligibility NOT Met - Clinical' && o.Determination_Outcome_Reason__c =='Clinical - Retinoblastoma'){
                o.status__c = 'Closed';
                o.status_reason__c = 'Program Eligibility NOT Met - Clinical';
                o.sub_status_reason__c= 'Retinoblastoma';
                o.Case_Stage__c='Referred';
                o.Patient_Status_Color__c='Red';
                continue;            
            }            

            if(o.id!=null && o.Referral_Date__c!=null&& o.Determination_Outcome__c=='Program Eligibility NOT Met - Clinical' && o.Determination_Outcome_Reason__c =='Clinical - Ocular'){
                o.status__c = 'Closed';
                o.status_reason__c = 'Program Eligibility NOT Met - Clinical';
                o.sub_status_reason__c= 'Ocular';
                o.Case_Stage__c='Referred';
                o.Patient_Status_Color__c='Red';
                continue;            
            }            

            if(o.id!=null && o.Referral_Date__c!=null&& o.Determination_Outcome__c=='Program Eligibility NOT Met - Clinical' && o.Determination_Outcome_Reason__c =='Clinical – Base of the skull tumors'){
                o.status__c = 'Closed';
                o.status_reason__c = 'Program Eligibility NOT Met - Clinical';
                o.sub_status_reason__c= 'Base of the skull tumors';
                o.Case_Stage__c='Referred';
                o.Patient_Status_Color__c='Red';
                continue;            
            }            
            
            if(o.id!=null && o.Referral_Date__c!=null&& o.Determination_Outcome__c=='Program Eligibility NOT Met - Clinical' && o.Determination_Outcome_Reason__c =='Clinical - Intracardiac' && o.status__c=='Closed'){
                o.status__c = 'Closed';
                o.status_reason__c = 'Program Eligibility NOT Met - Clinical';
                o.sub_status_reason__c= 'Intracardiac';
                o.Case_Stage__c='Referred';
                o.Patient_Status_Color__c='Red';
                continue;            
            }            

            if(o.id!=null && o.Referral_Date__c!=null&& o.Determination_Outcome__c=='Program Eligibility NOT Met - NonClinical' && o.Determination_Outcome_Reason__c =='Nonclinical - Age less than 2 years'){
                o.status__c = 'Closed';
                o.status_reason__c = 'Program Eligibility NOT Met - NonClinical';
                o.sub_status_reason__c= 'Age less than 2 years';
                o.Case_Stage__c='Referred';
                o.Patient_Status_Color__c='Red';
                continue;            
            }            

            if(o.id!=null && o.Referral_Date__c!=null&& o.Determination_Outcome__c=='Program Eligibility NOT Met - NonClinical' && o.Determination_Outcome_Reason__c =='Nonclinical - No local physician' && o.status__c=='Closed'){
                o.status__c = 'Closed';
                o.status_reason__c = 'Program Eligibility NOT Met - NonClinical';
                o.sub_status_reason__c= 'No local physician';
                o.Case_Stage__c='Referred';
                o.Patient_Status_Color__c='Red';
                continue;            
            }            

            if(o.id!=null && o.Referral_Date__c!=null&& o.Determination_Outcome__c=='Program Eligibility NOT Met - NonClinical' && o.Determination_Outcome_Reason__c =='Nonclinical - No local physician visit' && o.status__c=='Closed'){
                o.status__c = 'Closed';
                o.status_reason__c = 'Program Eligibility NOT Met - NonClinical';
                o.sub_status_reason__c= 'No local physician visit';
                o.Case_Stage__c='Referred';
                o.Patient_Status_Color__c='Red';
                continue;            
            }            
            /*
            if(o.id!=null && o.Referral_Date__c!=null&& o.Date_contacted_speciality_provider_POC__c!=null && o.Date_informed_patient_of_the_referralPOC__c!=null && o.Transitioned_as_Closed_Plan_of_Care__c!=null && o.Patient_Agree_to_POC__c=='No' && o.status__c=='Open'){
                o.status__c = 'Closed';
                o.status_reason__c = 'Cancelled by Patient at Plan of Care';
                o.sub_status_reason__c= 'N/A';
                o.Case_Stage__c='Accepted by Facility';
                o.Patient_Status_Color__c='Red';
                continue;            
            } 
            */ 
            
        }        
    }
    
}