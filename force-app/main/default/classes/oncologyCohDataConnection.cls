public with sharing class oncologyCohDataConnection {
    public SalesoforceUserStruct cohUser { get; private set; }
    public String sentData { get; set; }
    public String rData { get; set; }
    public String responseString { get; set; }
    transient String username, pw, accessToken, instanceUrl, client_secret, client_id, recordtypeid;
    String clientName;

    public oncologyCohDataConnection(String clientName) {
        sObject os;
        this.clientName = clientName;

        switch on clientName {
            when 'Amazon' {
                os = cohAmzConnection__c.getInstance();
                recordtypeid = String.valueOf(os.get('RecordTypeId__c'));
            }

            when 'Facebook' {
                os = cohFBConnection__c.getInstance();
                recordtypeid = String.valueOf(os.get('RecordTypeId__c'));
            }

            when 'Lowes' {
                os = cohFBConnection__c.getInstance();
                recordtypeid = String.valueOf(os.get('RecordTypeId__c'));
            }

            when 'SISC' {
                os = cohFBConnection__c.getInstance();
                recordtypeid = String.valueOf(os.get('SISC_RecordTypeId__c'));
            }

            when else {
                return;
            }
        }

        System.debug('COH RecordID : ' + recordtypeid);
        pw = String.valueOf(os.get('password__c'));
        username = String.valueOf(os.get('username__c'));
        client_secret = String.valueOf(os.get('client_secret__c'));
        client_id = String.valueOf(os.get('client_id__c'));
    }

    Boolean setAccessToken(Boolean isSandbox) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();

        if (isSandbox) {
            req.setEndpoint(
                'https://test.salesforce.com/services/oauth2/token'
            );
        } else {
            req.setEndpoint(
                'https://login.salesforce.com/services/oauth2/token'
            );
        }

        String body = 'grant_type=password&client_id=' +
                        client_id +
                        '&client_secret=' +
                        client_secret +
                        '&username=' +
                        username +
                        '&password=' +
                        pw;
        sentData = body;
        System.debug(sentData);
        req.setBody(body);
        req.setHeader('Content-Type', 'application/x-www-form-urlencoded');
        req.setmethod('POST');

        if (Test.isRunningTest()) {
            rData ='{"access_token":"SESSION_ID_REMOVED","instance_url":"https://cityofhope--EmpIntake.cs20.my.salesforce.com","id":"https://test.salesforce.com/id/00Dm0000000D8lbEAC/005m0000002lonLAAQ","token_type":"Bearer","issued_at":"1550805789473","signature":"5aAUYdHr9DLe9uOxfITmUO74+wJQcSiA8ezRVxF8gwE="}';
        } else {
            HttpResponse res = h.send(req);
            rData = res.getBody();
        }

        Map<String, Object> m = (Map<String, Object>) JSON.deserializeUntyped(rData);
        accessToken = String.valueOf(m.get('access_token'));
        instanceUrl = String.valueOf(m.get('instance_url'));

        if (accessToken == null || accessToken == '') {
            return false;
        }

        if (instanceUrl == null || instanceUrl == '') {
            return false;
        }

        return true;
    }

    public HttpResponse updateRecord(
        Oncology__c oncologyRecord,
        Boolean isSandbox
    ) {
        if (accessToken == null) {
            if (!setAccessToken(isSandbox)) {
                return null;
            }
        }

        HttpRequest req = new HttpRequest();
        req.setEndpoint(
            instanceUrl +
            '/services/data/v42.0/sobjects/Account/' +
            oncologyRecord.external_id__c +
            '?_HttpMethod=PATCH'
        );
        req.setMethod('POST');
        req.setHeader('Authorization', 'OAuth ' + accessToken);
        req.setHeader('Content-Type', 'application/json');

        if (clientName != 'Amazon') {
            req.setbody(
                OncologyMapFbToCOH.getTheAcct(oncologyRecord, recordTypeId)
            );
        } else if (clientName == 'Amazon') {
            req.setbody(
                OncologyMapAmzToCOH.getTheAcct(oncologyRecord, recordTypeId)
            );
        } else {
            return null;
        }

        System.debug('Request for COH : ' + req.getBody());

        if (Test.isRunningTest()) {
            HttpResponse res = new HttpResponse();
            res.setBody('{"id":"01pQ0000000Jfgr"}');
            res.setStatusCode(200);

            return res;
        } else {
            Http h = new Http();

            return h.send(req);
        }
    }

    public HttpResponse sendRecord(
        Oncology__c oncologyRecord,
        Boolean isSandbox
    ) {
        if (accessToken == null) {
            if (!setAccessToken(isSandbox)) {
                return null;
            }
        }

        HttpRequest req = new HttpRequest();

        req.setMethod('POST');
        req.setHeader('Authorization', 'OAuth ' + accessToken);
        req.setHeader('Content-Type', 'application/json');
        System.debug('clientName ' + clientName);

        switch on clientName {
            when 'Amazon' {
                req.setbody(
                    OncologyMapAmzToCOH.getTheAcct(oncologyRecord, recordTypeId)
                );
                req.setEndpoint(
                    instanceUrl + '/services/data/v42.0/sobjects/Am_Account__c'
                );
            }

            when else {
                req.setbody(
                    OncologyMapFbToCOH.getTheAcct(oncologyRecord, recordTypeId)
                );
                req.setEndpoint(
                    instanceUrl + '/services/data/v42.0/sobjects/Account'
                );
            }
        }

        if (Test.isRunningTest()) {
            HttpResponse res = new HttpResponse();
            res.setBody('{"id":"01pQ0000000Jfgr"}');
            res.setStatusCode(200);

            return res;
        } else {
            Http h = new Http();

            return h.send(req);
        }
    }

    public String queryRecords(String recordId) {
        if (accessToken == null) {
            if (!setAccessToken(utilities.isRunningInSandbox())) {
                return null;
            }
        }

        HttpRequest req = new HttpRequest();
        String table = 'Account';

        if (clientName == 'Amazon') {
            table = 'am_account__c';
        }

        req.setEndpoint(
            instanceUrl +
            '/services/data/v42.0/query/?q=select+Evaluation_Request_ID__c+from+' +
            table +
            '+where+id=\'' +
            recordId +
            '\''
        );
        req.setMethod('GET');
        req.setHeader('Authorization', 'OAuth ' + accessToken);
        req.setHeader('Content-Type', 'application/json');
        Http h = new Http();
        HttpResponse res = h.send(req);
        System.debug(res.getbody());

        return res.getbody();
    }
}
