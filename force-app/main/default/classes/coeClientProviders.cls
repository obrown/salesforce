public class coeClientProviders{

    public static selectOption[] providerSelectOptions(patient_case__c patientCase, id clientFacilityId){
        
        selectOption[] foo = new selectOption[]{};
        if(patientCase.client_facility__c==null){
            foo.add( new selectoption('','No Facility Set'));
            return foo;
        }
        
        foo.add( new selectoption('','None'));
        
        for(Client_Facility_Physician__c p : [select Physician__r.name from Client_Facility_Physician__c where client_facility__c = :clientFacilityId]){
           foo.add( new selectoption(p.id, p.Physician__r.name)); 
        }
        
        return foo;
    }
    
    public static string[] providerStringList(patient_case__c patientCase, id clientFacilityId){
        
        string[] foo = new string[]{};
        if(patientCase.client_facility__c==null){
            foo.add('No Facility Set');
            return foo;
        }
        
        for(Client_Facility_Physician__c p : [select Physician__r.name from Client_Facility_Physician__c where client_facility__c = :clientFacilityId]){
           foo.add(p.Physician__r.name); 
        }
        
        if(foo.size()==0){
            foo.add('No Providers Found');
        }
        
        return foo;
    }

}