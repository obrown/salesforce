public with sharing class umEmployeeDenialMedicalNecessity_OHy{
    
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c  uml, Utilization_Management_Clinician__c clinician, Utilization_Management_Clinical_Code__c[] diagnosisCodes, Utilization_Management_Clinical_Code__c[] clinicalCodes){
        
        boolean hasClinician = (Clinician.Street__c!=null&&Clinician.City__c!=null&&Clinician.State__c!=null&&Clinician.Zip_Code__c!=null);
        boolean hasAddress = (um.patient__r.Address__c!=null&&um.patient__r.City__c!=null&&um.patient__r.State__c!=null&&um.patient__r.Zip__c!=null);
        
        string letterText='';
        letterText+=um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasAddress ? um.patient__r.Address__c + '<br/>': '' + '<br/>';
        letterText+=hasAddress ? um.patient__r.City__c +', ': ', ';
        letterText+=hasAddress ? um.patient__r.State__c  + ' ' : ' ';
        letterText+=hasAddress ? um.patient__r.Zip__c +'<br/><br/>': '' +'<br/><br/>';
        letterText+='Patient Name:&nbsp;'+um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c;
        letterText+='<br/>';
        letterText+='Date of Birth:&nbsp;'+um.patient__r.Patient_Date_of_Birth__c;
        letterText+='</div>';

        letterText+='<p>';
        letterText+='Dear&nbsp;'+um.patient__r.Patient_First_Name__c+',';//+'&nbsp;'+cm.patient__r.Patient_Last_Name__c+',';
        letterText+='</br></br>';
        letterText+='OhioHealthy, LLC (“OhioHealthy”) performs benefit administration services for your '+um.patient__r.Patient_Employer__r.name+'\'s, self-funded group health plan (the “Plan).&nbsp;You are receiving this letter because&nbsp';
        letterText+=+um.patient__r.Patient_First_Name__c+' '+um.patient__r.Patient_Last_Name__c+'\'s request for <b>[Service Name]</b> at '+um.Facility_Name__c+' on'+'&nbsp;'+um.Admission_Date__c.month()+'/'+um.Admission_Date__c.day()+'/'+um.Admission_Date__c.year();
        letterText+='&nbsp;for <b>[Treatment]</b> was not approved.';        
        letterText+='</p>';

        letterText+='<p>';             
        letterText+='As part of the review, I reviewed the information from your Plan, your care provider as well as information from Milliman care guidelines (MCG®): <b>[MCG number and description]</b> MCG are the criteria used to';
        letterText+='&nbspassist with decisions on care. ';
        letterText+='</p>';

        letterText+='<p>';  
        letterText+='This decision is based on the following:';
        letterText+='</br></br>';
        letterText+='<b><u>Clinical Rational:</b></u>'+'&nbsp;[explanation of scientific or clinical judgement for the determination]';
        letterText+='</br></br>';
        letterText+='<b><u>Denial Reason:</b></u>'+'&nbsp;[specific reason for the adverse determination]';
        letterText+='</br></br>';
        letterText+='<b><u>Health Plan Provision:</b></u>'+'&nbsp;The following language on page <b>[INSERT PAGE # or SECTION #]</b> ';
        letterText+='</br>';
        letterText+='supports this decision:'+'&nbsp;<b>[Include the language from the SPD that is the basis for denial]</b> ';           
        letterText+='</p>';

        letterText+='<p>';
        letterText+='Before doing <b>[Treatment]</b>, you need to <b>[Description of what need to do]</b>';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='You may get a copy of your benefit information by calling the number on your Medical ID/Insurance Card or by mailing a request to:';
        letterText+='</br></br>';
        letterText+='<div style="display:inline-block;margin-left:285px;">';
        letterText+='OhioHealthy';
        letterText+='</div>';
        letterText+='</br></br>';
        letterText+='<div style="display:inline-block;margin-left:250px;">';
        letterText+='Attn: Care Management';
        letterText+='</div>';
        letterText+='</br></br>'; 
        letterText+='<div style="display:inline-block;margin-left:275px;">';
        letterText+='P.O. Box 2582';
        letterText+='</div>';
        letterText+='</br></br>';
        letterText+='<div style="display:inline-block;margin-left:240px;">';
        letterText+='Hudson, Ohio 44236-2582';
        letterText+='</div>';                               
        letterText+='</p>';

        letterText+='<p>';
        letterText+='If your care provider would like to talk to me about why your request was not approved, please have your care provider call 1-888-845-3580 to schedule a time.';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='All choices on your care and treatment are the responsibility of you and your care provider. This letter only provides information about';
        letterText+='&nbsp; Plan coverage for your requested treatment.';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='Included in this mailing are:';
        letterText+='<ul>';
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='Your <b>[Treatment]</b> request information ';
        letterText+='</li>'; 
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='How to appeal this decision';
        letterText+='</li>';
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='How to file a complaint';
        letterText+='</li>';                        
        letterText+='</ul>';
        letterText+='</p>';

        letterText+='<br/><br/>';
        letterText+='Sincerely,';
        letterText+='<div ></div>';
        letterText+='<div style="display:none" >signhere</div>';
        letterText+='Eric M. Yasinow, M.D.<br/>';
        letterText+='Medical Director';
        letterText+='<br/><br/>';
        
        letterText+='<div>';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='CC:'; 
        letterText+='</div>';
        letterText+='<div style="display:inline-block;">';
        letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';        
        letterText+='<div style="display:inline-block;">';
        letterText+=um.Facility_Name__c;
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;"/>';
        letterText+=hasClinician ? Clinician.Street__c : '';
        letterText+='</div>';
        letterText+='<div >';
        letterText+='<div style="display:inline-block;width:40px">';
        letterText+='&nbsp;'; 
        letterText+='</div>';
        
        letterText+='<div style="display:inline-block;">';
        letterText+=hasClinician ? Clinician.City__c + ', ' : ', ';
        letterText+=hasClinician ? Clinician.State__c  +' ' : ' ';
        letterText+=hasClinician ? Clinician.Zip_Code__c : '';
        letterText+='</div>';
        letterText+='</div><br/>';   

        letterText+='<p>';
        letterText+='<b>[Service Name] Request</b>';
        letterText+='</p>';              

        letterText+='<div style="margin-top:10px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Case Number:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px;">';
        letterText+='&nbsp;'+um.HealthPac_Case_Number__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Physician Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c;
        letterText+='</div>';
        letterText+='</div>'; 
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Facility Name:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+um.Facility_Name__c;
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%">';
        letterText+='Dates of Service:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;Beginning '+um.Admission_Date__c.month()+'/'+um.Admission_Date__c.day()+'/'+um.Admission_Date__c.year();
        letterText+='</div>';
        letterText+='</div>';
        letterText+='<div style="margin-top:5px">';
        letterText+='<div style="display:inline-block;width:15%;vertical-align:top">';
        letterText+='Type of Request:';
        letterText+='</div>';
        letterText+='<div style="display:inline-block;margin-left:8px">';
        letterText+='&nbsp;'+uml.Type_of_Request__c;
        letterText+='</div>';
        letterText+='</div>';
        
        letterText+='<br/>';
        letterText+='Diagnoses: ';
        if(diagnosisCodes.isEmpty()){
            letterText+='N/A';
        }else{
            for(Utilization_Management_Clinical_Code__c dc :diagnosisCodes){
                letterText+=dc.name+' '+dc.Description__c+' ;';
            }
            letterText=letterText.removeEnd(';');
        }
        letterText+='<br/>';
        letterText+='Treatment:<b>[Treatment]</b> ';
        
        if(clinicalCodes.isEmpty()){
            //letterText+='N/A';
        }else{
            for(Utilization_Management_Clinical_Code__c dc :clinicalCodes){
                letterText+=dc.name+' '+dc.Description__c+' ;';
            }
             letterText=letterText.removeEnd(';');
        }
        letterText+='<br/><br/>';
        
        
        letterText+='<p>';
        letterText+='<b>How to Appeal This Decision</b>';
        letterText+='<br/>';
        letterText+='This decision has been processed consistent with benefit terms and conditions described in the Summary Plan Description and Plan Document. If you have further questions, you may contact Customer Service at the telephone number listed on your Medical ID/Insurance Card.';
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='You have a right to request, free of charge, a copy of all documents, records and other information, including any applicable internal guideline or protocol used in this determination.';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='You or your authorized representative have the right to appeal this decision. Your appeal must be sent in writing, along with any additional information such as written comments or documents that apply to the appeal, within 180 days of receipt of this letter to:';
        letterText+='</p>';
        letterText+='<br/><br/>';

        letterText+='<p>';
        letterText+='<div style="display:inline-block;margin-left:285px;">';
        letterText+='OhioHealthy';
        letterText+='</div>';
        letterText+='</br></br>';
        letterText+='<div style="display:inline-block;margin-left:250px;">';
        letterText+='Attn: Appeals Coordinator';
        letterText+='</div>';
        letterText+='</br></br>'; 
        letterText+='<div style="display:inline-block;margin-left:275px;">';
        letterText+='P.O. Box 2852';
        letterText+='</div>';
        letterText+='</br></br>';
        letterText+='<div style="display:inline-block;margin-left:240px;">';
        letterText+='Hudson, Ohio 44236-2582';
        letterText+='</div>';                               
        letterText+='</p>';
        
        letterText+='<p>';
        letterText+='OhioHealthy will review and make a decision on your appeal within 30 days.';
        letterText+='<br/><br/>';
        letterText+='If your request is urgent, you can also fax your appeal request to 1-877-891-2691 within 24 hours of receipt of this letter. OhioHealthy will process your appeal within 72 hours.';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='You may have other voluntary alternative dispute resolution options, such as mediation and the right to bring a civil action under section 502(a) of ERISA. Any legal action for benefits must be filed within 180 days after the final decision';
        letterText+='after the appeals process has been exhausted. One way to find out what may be available is to contact your local U.S. Department of Labor Office.';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='For questions about your appeal rights, this notice, or for assistance, you can contact:';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='<ul>';
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='Customer Service at the telephone number listed on your Medical ID/Insurance Card';
        letterText+='</li>'; 
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='Employee Benefits Security Administration at 1-866-444-EBSA (3272) or visit www.dol.gov/ebsa ';
        letterText+='</li>';
        letterText+='<li style="list-style-type:none;margin-bottom:5px">';
        letterText+='Ohio Department of Insurance at:';
        letterText+='</li>';                        
        letterText+='</ul>';
        letterText+='</p>';

        letterText+='<p style="text-align:center">'; 
        letterText+='Ohio Department of Insurance<br/>'; 
        letterText+='ATTN:  Consumer Affairs<br/>'; 
        letterText+='50 West Town Street, Suite 300, Columbus, OH  43215<br/>';
        letterText+='800-686-1526 / 614-644-2673<br/>';
        letterText+='614-644-3744 (fax)<br/>';
        letterText+='614-644-3745 (TDD)<br/>';         
        letterText+='</p>';   

        letterText+='<p style="text-align:center">'; 
        letterText+='Contact ODI Consumer Affairs:<br/>';
        letterText+='<span style="color:blue;text-decoration:underline;">https://secured.insurance.ohio.gov/ConsumServ/ConServComments.asp</span><br/>';
        letterText+='File a Consumer Complaint:<br/>';
        letterText+='<span style="color:blue;text-decoration:underline;">http://insurance.ohio.gov/Consumer/OCS/Pages/ConsCompl.aspx</span><br/>';
        letterText+='</p>'; 
        letterText+='</br>'; 

        letterText+='<p>'; 
        letterText+='<b>How to File a Complaint</b> - Please note that filing a complaint is NOT an appeal of this decision. To make an appeal, please follow the process outline in the section entitled “<b>How to Appeal This Decision</b>” above.';
        letterText+='&nbsp;Documentation representing material difference from information originally submitted.';
        letterText+='</p>'; 

        letterText+='<p>';  
        letterText+='OhioHealthy offers two ways to make a complaint:';
        letterText+='<br/><br/>';
        letterText+='1) Call our Customer Service Department at the number on the back of your insurance card and a representative will assist you with this process.';
        letterText+='<div style="display:inline-block;margin-left:350px;">';
        letterText+='OR';
        letterText+='</div>';
        letterText+='<br/><br/>';
        letterText+='2) Write to us. Our address is: OhioHealthy, Attn: Complaint Department, P.O Box 2582 Hudson, OH  44236-2582.';
        letterText+='</p>';      
                               
        letterText+='<p>';
        letterText+='SPANISH (Español):  Para obtener asistencia en Español, llame al 1-330-656-1072.<br/>';
        letterText+='TAGALOG (Tagalog):  Kung kailangan ninyo ang tulong sa Tagalog tumawag sa 1-330-656-1072.<br/>';
        letterText+='CHINESE <span style="font-family: Arial Unicode MS">(中文):  如果需要中文的帮助，请拨打这个号码 </span>1-330-656-1072.<br/>';
        letterText+='NAVAJO (Dine): Dinek\'ehgo  shika  at\'ohwol  ninisingo, kwiijigo  holne\' 1-330-656-1072.<br/>';
        letterText+='</p>';
        letterText+='<br/><br/>';

        return letterText;
    }
    
}