@isTest
private class ECENPatientInformationExhangeTest{
    
    static testMethod void main(){
        
        
        patientCaseTestData t = new patientCaseTestData();
        t.loadData('Walmart', 'Scripps', 'Joint');
        t.pc.bid__c= '123456';
        t.pc.bid_id__c= '123456';
        
        t.populateDemo();
        
        insert t.pc;
        
        t.pc.Plan_of_Care_accepted_at_HDP__c= date.today();
        t.pc.Caregiver_Responsibility_Received__c = date.today();
        t.pc.Program_Authorization_Received__c = date.today();
        
        t.pc.current_nicotine_user__c='Yes';
        t.pc.displayAuthNumber__c = t.pc.Authorization_Number__c;
        t.pc.isconverted__c = true;
        t.pc.Converted_Date__c = date.today();
        
        t.pc.estimated_arrival__c = date.today().addDays(4);
        t.pc.estimated_departure__c= date.today().addDays(8);
        t.pc.AMEX_Trip_ID__c = 'AF3448A'; 
        t.pc.Arriving_Flight_Number__c ='45678';
        t.pc.Arriving_Flight_Time__c=datetime.now();
        t.pc.Arriving_Flight_Number__c ='87489';
        t.pc.Departing_Flight_Time__c=datetime.now();
        t.pc.Hotel_Check_In_Date__c = date.today();
        t.pc.Hotel_Checkout_Date__c = date.today();
        t.pc.Travel_Request__c = date.today();
        t.pc.Travel_Type__c = 'Flying';
        
        update t.pc;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/ecen/v1/patientInfo';
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        ECENPatientInformationExhange.doGet();
        
        req.AddHeader('Ins_id', t.pc.bid_id__c);
        ECENPatientInformationExhange.doGet();
        
        ECEN_Message__c incomingMessage = new ECEN_Message__c(Insurance_Id__c=t.pc.bid_id__c);
        
        incomingMessage.subject__c = 'Unit.Test';
        incomingMessage.body__c = 'Unit.Test';
        incomingMessage.Sender_Name__c = 'Unit.Test';
        incomingMessage.Sender_Email__c= 'testig@test.com';
        incomingMessage.dateReceived__c= datetime.now();
        
        ECEN_Message__c sentMessage = incomingMessage.clone();
        sentMessage.sentMessage__c=true;
        sentMessage.dateSent__c= datetime.now();
        sentMessage.dateReceived__c=null;
        
        Ecen_Contact_Number__c ecn = new Ecen_Contact_Number__c();
        ecn.Contact_Number__c= '(480) 555-1212';
        ecn.name= 'unit.test';
        
        sObject[] solist = new sobject[]{incomingMessage, sentMessage};
        
        insert solist;
        
        req.requestURI = '/services/apexrest/ecen/v1/messages';
        ECENMessageExchange.doGet();
        
        req.AddHeader('Ins_id', t.pc.bid_id__c);
        req.AddHeader('Token', 'tokentesting');
        
        req.requestURI = '/services/apexrest/ecen/v1/devicetoken';
        ECENPatientDevice.doPost();
        
        
        req.requestURI = '/services/apexrest/ecen/v1/contacts';
        ECENContactsExchange.doGet();
        
        pc_poc__c poc = t.addPoc(t.pc.id);
        insert poc;
        
        req.requestURI = '/services/apexrest/ecen/v1/patienttravel';
        ECENPatientTravelExhange.doGet();
        
        ecenDashBoardStruct edb = new ecenDashBoardStruct();
        edb.ToDoItemsComplete = 'unit.test';
        edb.WelcomeMessage = 'unit.test';
        ecenDashBoardStruct.graph graph = new ecenDashBoardStruct.graph();
        ecenDashBoardStruct.milestone milestone = new ecenDashBoardStruct.milestone('test',4);
        Test.stopTest();
        
    }

    static testMethod void testBariatricMobileBackend(){
        Id RecordTypeIdPre = Schema.SObjectType.Bariatric_Stage__c.getRecordTypeInfosByName().get('Pre').getRecordTypeId();

            
        bariatric__c bariatric = new bariatric__c();
        
        bariatric = new bariatric__c(); 
            bariatric.bid__c = '12345678';
            bariatric.bid_id__c = '12345678';
            bariatric.status__c = 'Open';
            bariatric.status_reason__c = 'In Process';
            bariatric.Date_Provider_Verification_Form_Received__c = date.today().addDays(-8);
            bariatric.Date_Provider_Verification_Form_Signed__c = date.today();
            bariatric.Intake_Information_Complete__c = 'Yes';
            bariatric.Date_Intake_Information_is_Complete__c = date.today();
            bariatric.Referred_Facility__c = 'Geisinger';
            bariatric.employee_first_name__c = 'Unit';
            bariatric.employee_last_name__c = 'Test';
            bariatric.employee_gender__c = 'Male';
            date dob = date.today().addyears(-35);
            
            bariatric.employee_dob__c = dob.month()+'/'+dob.day()+'/'+dob.year();
            bariatric.employee_home_phone__c = '(480) 555-1212';
            bariatric.employee_email_Address__c = 'none@none.gmail.com';
            bariatric.employee_SSN__c ='1234';
            bariatric.Employee_Street__c ='123 E Main St';
            bariatric.Employee_City__c ='Mesa';
            bariatric.Employee_State__c ='AZ';
            bariatric.Employee_Zip_Code__c='12345';
            
            bariatric.patient_gender__c = 'Male';
            bariatric.patient_first_name__c = 'Unit';
            bariatric.patient_last_name__c = 'Test';
            bariatric.patient_dob__c = dob.month()+'/'+dob.day()+'/'+dob.year();
            bariatric.patient_home_phone__c = '(480) 555-1212';
            bariatric.patient_email_Address__c = 'none@none.gmail.com';    
            bariatric.patient_SSN__c ='1234';
            bariatric.patient_Street__c ='123 E Main St';
            bariatric.patient_City__c ='Mesa';
            bariatric.patient_State__c ='AZ';
            bariatric.patient_Zip_Code__c='12345';
            
            bariatric.Patient_Height_FT__c = '5';
            bariatric.Patient_Height_Inches__c= '6';
            bariatric.Patient_Weight__c = '300';
            bariatric.Relationship_to_the_Insured__c ='Patient is Insured';
            bariatric.Type_2_Diabtetic__c = 'Yes';
            bariatric.current_nicotine_user__c = 'No';
            
            bariatric.COB_Received__c = date.today(); 
            
            sObject[] sObjects = new sObject[]{};
            
            Client__c client = new Client__c(name='Walmart');
            sObjects.add(client);
        
            Procedure__c ecenProcedure = new Procedure__c(name='Bariatric');
            sObjects.add(ecenProcedure);
        
            Facility__c facility = new Facility__c(name='Testing');
            facility.Facility_Address__c = '2500 S Power Rd';
            facility.Facility_City__c= 'Mesa';
            facility.Facility_State__c= 'AZ';
            facility.Facility_Zip__c= '85205';
            sObjects.add(facility);
        
            insert sObjects;
            
            Client_facility__c  clientFacility= new Client_facility__c (Client__c=client.id, Facility__c=Facility.id,Procedure__c=ecenProcedure.id);
            insert clientFacility;
        
        
        bariatric.client_facility__c= clientFacility.id;
        
        bariatric.Eligible__c = 'Yes';
        bariatric.carrier_name__c = 'UHC';
        bariatric.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        bariatric.Verified_Employee_DOB__c = true;
        bariatric.Verified_Employee_SSN__c = true;
        bariatric.Verified_Patient_DOB__c = true;
        bariatric.Verified_Patient_SSN__c = true;
        bariatric.patient_ssn__c='123456789';
        bariatric.employee_ssn__c='123456789';
        bariatric.Referral_Date__c = date.today();
        insert bariatric;
        system.assert(bariatric.id!=null);
        
        bariatric_Stage__c preStage = new Bariatric_Stage__c(Bariatric__c=bariatric.id, recordtypeid=RecordTypeIdPre);
        preStage.Plan_of_Care_Accepted_at_HDP__c=date.today();
        preStage.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        preStage.Caregiver_Form_Received__c=date.today();
        preStage.Program_Forms_Received_Date__c=date.today();
        preStage.Plan_of_Care_Type__c ='Evaluation';
        preStage.booking_reference_number__c='1234522';
        preStage.Travel_Type__c='Flying';
        preStage.Estimated_Arrival__c=date.today();
        preStage.Estimated_Departure__c=date.today()+4;
        preStage.Travel_Type__c='Flying';
        preStage.Arriving_Flight_Number__c='1234';
        preStage.Departing_Flight_Number__c='1234';
        preStage.Arriving_Date_Time__c=datetime.now();
        preStage.Departing_Date_Time__c=datetime.now();
        preStage.Evaluation_Date__c=date.today();
        insert preStage;
        
         system.assert(preStage.id!=null);
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/ecen/v1/patientInfo';
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        ECENPatientInformationExhange.doGet();
        
        req.AddHeader('Ins_id', bariatric.bid_id__c);
        ECENPatientInformationExhange.doGet();
        
        ECEN_Message__c incomingMessage = new ECEN_Message__c(Insurance_Id__c=bariatric.bid_id__c);
        
        incomingMessage.subject__c = 'Unit.Test';
        incomingMessage.body__c = 'Unit.Test';
        incomingMessage.Sender_Name__c = 'Unit.Test';
        incomingMessage.Sender_Email__c= 'testig@test.com';
        incomingMessage.dateReceived__c= datetime.now();
        incomingMessage.Insurance_Id__c=bariatric.bid_id__c;
        
        ECEN_Message__c sentMessage = incomingMessage.clone();
        sentMessage.sentMessage__c=true;
        sentMessage.dateSent__c= datetime.now();
        sentMessage.dateReceived__c=null;
        sentMessage.Insurance_Id__c=bariatric.bid_id__c;
        Ecen_Contact_Number__c ecn = new Ecen_Contact_Number__c();
        ecn.Contact_Number__c= '(480) 555-1212';
        ecn.name= 'unit.test';
        
        sObject[] solist = new sobject[]{incomingMessage, sentMessage};
        
        insert solist;
        
        req.requestURI = '/services/apexrest/ecen/v1/messages';
        ECENMessageExchange.doGet();
        
        req.AddHeader('Ins_id', bariatric.bid_id__c);
        req.AddHeader('Token', 'tokentesting');
        
        req.requestURI = '/services/apexrest/ecen/v1/devicetoken';
        ECENPatientDevice.doPost();
        
        
        req.requestURI = '/services/apexrest/ecen/v1/contacts';
        ECENContactsExchange.doGet();
        
        req.requestURI = '/services/apexrest/ecen/v1/patienttravel';
        ECENPatientTravelExhange.doGet();
        
        delete preStage;
        
        ecenDashBoardStruct edb = new ecenDashBoardStruct();
        edb.ToDoItemsComplete = 'unit.test';
        edb.WelcomeMessage = 'unit.test';
        ecenDashBoardStruct.graph graph = new ecenDashBoardStruct.graph();
        ecenDashBoardStruct.milestone milestone = new ecenDashBoardStruct.milestone('test',4);
        
        Id RecordTypeIdGlobal = Schema.SObjectType.Bariatric_Stage__c.getRecordTypeInfosByName().get('Global').getRecordTypeId();
        system.debug('RecordTypeIdGlobal  '+RecordTypeIdGlobal);
        bariatric_Stage__c globalStage = new Bariatric_Stage__c(Bariatric__c=bariatric.id, recordtypeid=RecordTypeIdGlobal );
        globalStage.Plan_of_Care_Accepted_at_HDP__c=date.today();
        globalStage.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        globalStage.Caregiver_Form_Received__c=date.today();
        globalStage.Program_Forms_Received_Date__c=date.today();
        globalStage.Plan_of_Care_Type__c ='Evaluation';
        globalStage.booking_reference_number__c='1234522';
        globalStage.Estimated_Pre_Op__c=date.today();
        globalStage.Estimated_Hospital_Admit__c=date.today();
        globalStage.Estimated_Procedure__c=date.today();
        globalStage.Estimated_Hospital_Discharge__c=date.today();
        globalStage.Travel_Type__c='Flying';
        globalStage.Estimated_Arrival__c=date.today();
        globalStage.Estimated_Departure__c=date.today()+4;
        insert globalStage;
        
        
        system.debug(globalStage.id+' '+globalStage.RecordTypeId);
        req.AddHeader('Ins_id', bariatric.bid_id__c);
        req.AddHeader('Token', 'tokentesting');
        
        req.requestURI = '/services/apexrest/ecen/v1/patientInfo';
        req.httpMethod = 'GET';
        ECENPatientInformationExhange.doGet();
        
        req.requestURI = '/services/apexrest/ecen/v1/devicetoken';
        ECENPatientDevice.doPost();
        
        
        req.requestURI = '/services/apexrest/ecen/v1/contacts';
        ECENContactsExchange.doGet();
        
        req.requestURI = '/services/apexrest/ecen/v1/patienttravel';
        ECENPatientTravelExhange.doGet();
        
        Test.stopTest();
        
    }

    static testMethod void testOncologyMobileBackEnd(){
        
        OncologyTestData t = new OncologyTestData();
        t.loadData('Facebook','City of Hope');
        oncology__c oncology = t.oncology;
        oncolcogyAutoStatusing.autoStatus(new oncology__c[]{oncology}); //pass in a new oncology record with no id
        oncology = t.completeOncologyIntake(oncology); //inserts the record, populating all the required fields
        system.assert(oncology.id!=null);
         
        oncology.Patient_Agrees_to_Referral__c='Yes';
        oncology.status__c='Open';
         
        oncology.Referral_Date__c=date.today();
        oncology.Date_contacted_speciality_provider_POC__c=system.today();
        oncology.Date_informed_patient_of_the_referralPOC__c=system.today();
        oncology.Transitioned_as_Closed_Plan_of_Care__c=system.today();
        oncology.status__c='Open';
        oncology.Patient_Medical_Plan_Number__c='123456'; 
        oncology.bid_id__c= '123456';
       
        oncology.Booking_Reference_Number__c='123456789';
        oncology.Date_HDP_Forms_Completed__c = date.today();
        oncology.Determination_Date__c = date.today();
        oncology.Determination_Outcome__c='';
        oncology.HDP_Program_Forms_Completed__c=true;
        
        hotel__c hotel = new hotel__c(name='Hotel Test');
        insert hotel;
        
        ecen_hotel__c ehotel= new ecen_hotel__c(hotel__c=hotel.id, client_facility__c=oncology.client_facility__c);
        insert eHotel;
        
        oncology.Evaluation_Hotel__c=eHotel.id;
        oncology.Evaluation_Estimated_Arrival__c = date.today().addDays(4);
        oncology.Evaluation_Estimated_Departure__c= date.today().addDays(8);
        oncology.Evaluation_Travel_Type__c = 'AF3448A'; 
        oncology.Evaluation_Arriving_Flight_Number__c ='45678';
        oncology.Evaluation_Arriving_Flight_Date_and_Time__c=datetime.now();
        oncology.Evaluation_Departing_Flight_Number__c ='87489';
        oncology.Evaluation_Departing_Flight_Date__c=datetime.now();
        oncology.Evaluation_Hotel_Check_in_Date__c = date.today();
        oncology.Evaluation_Hotel_Check_Out_Date__c = date.today();
        oncology.Evaluation_Travel_Type__c= 'Flying';
        
        oncology.Translation_Services_Required__c='No';
        
        oncology.HR_Name__c = 'Unit.test';
        oncology.HR_Email__c= 'Unittest@unitTest.org';
        oncology.HR_Phone_Number__c= '(480) 555-1212';
        
        oncology.Client_HR__c= 'Facebook';
        oncology.Patient_Advocacy__c='Accolade';
        
        oncology.First_Appointment_Date__c=date.today();
        oncology.Patient_FAT__c='09';
        oncology.PatientFAT_Minutes__c= '00';
        oncology.PatientFAT_AMPM__c = 'AM';
        
        oncology.Last_Appointment_Date__c=date.today().addDays(2);
        oncology.Patient_LAT__c='09';
        oncology.PatientLAT_Minutes__c= '00';
        oncology.PatientLAT_AMPM__c = 'AM';
        oncology.Estimated_Arrival_Date__c=date.today();
        
        update oncology;
        
        RestRequest req = new RestRequest();
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/ecen/v1/patientInfo';
        req.httpMethod = 'GET';
        
        RestContext.request = req;
        RestContext.response = res;
        
        Test.startTest();
        ECENPatientInformationExhange.doGet();
        system.debug(oncology.bid_id__c);
        req.AddHeader('Ins_id', oncology.bid_id__c);
        ECENPatientInformationExhange.doGet();
        
        ECEN_Message__c incomingMessage = new ECEN_Message__c(Insurance_Id__c=oncology.bid_id__c);
        
        incomingMessage.subject__c = 'Unit.Test';
        incomingMessage.body__c = 'Unit.Test';
        incomingMessage.Sender_Name__c = 'Unit.Test';
        incomingMessage.Sender_Email__c= 'testig@test.com';
        incomingMessage.dateReceived__c= datetime.now();
        incomingMessage.Insurance_Id__c=oncology.bid_id__c;
        
        ECEN_Message__c sentMessage = incomingMessage.clone();
        sentMessage.sentMessage__c=true;
        sentMessage.dateSent__c= datetime.now();
        sentMessage.dateReceived__c=null;
        sentMessage.Insurance_Id__c=oncology.bid_id__c;
        
        Ecen_Contact_Number__c ecn = new Ecen_Contact_Number__c();
        ecn.Contact_Number__c= '(480) 555-1212';
        ecn.name= 'unit.test';
        
        sObject[] solist = new sobject[]{incomingMessage, sentMessage};
        
        insert solist;
        
        req.requestURI = '/services/apexrest/ecen/v1/messages';
        ECENMessageExchange.doGet();
        
        req.AddHeader('Ins_id', oncology.bid_id__c);
        req.AddHeader('Token', 'tokentesting');
        
        req.requestURI = '/services/apexrest/ecen/v1/devicetoken';
        ECENPatientDevice.doPost();
        
        req.AddHeader('Ins_id', oncology.bid_id__c);
        req.AddHeader('Token', 'tokentesting');
        
        req.requestURI = '/services/apexrest/ecen/v1/contacts';
        ECENContactsExchange.doGet();
        
        req.requestURI = '/services/apexrest/ecen/v1/patienttravel';
        ECENPatientTravelExhange.doGet();
        
        ecenDashBoardStruct edb = new ecenDashBoardStruct();
        edb.ToDoItemsComplete = 'unit.test';
        edb.WelcomeMessage = 'unit.test';
        ecenDashBoardStruct.graph graph = new ecenDashBoardStruct.graph();
        ecenDashBoardStruct.milestone milestone = new ecenDashBoardStruct.milestone('test',4);
        Test.stopTest();
        
    }
    
    static testMethod void cognitoTest(){

        patientCaseTestData t = new patientCaseTestData();
        t.loadData('Walmart', 'Scripps', 'Joint');
        t.pc.bid__c= '123456';
        t.pc.bid_id__c= '123456';
        
        t.populateDemo();
        
        insert t.pc;
        
        t.pc.Plan_of_Care_accepted_at_HDP__c= date.today();
        t.pc.Caregiver_Responsibility_Received__c = date.today();
        t.pc.Program_Authorization_Received__c = date.today();
        
        t.pc.current_nicotine_user__c='Yes';
        t.pc.displayAuthNumber__c = t.pc.Authorization_Number__c;
        t.pc.isconverted__c = true;
        t.pc.Converted_Date__c = date.today();
        
        t.pc.estimated_arrival__c = date.today().addDays(4);
        t.pc.estimated_departure__c= date.today().addDays(8);
        t.pc.AMEX_Trip_ID__c = 'AF3448A'; 
        t.pc.Arriving_Flight_Number__c ='45678';
        t.pc.Arriving_Flight_Time__c=datetime.now();
        t.pc.Arriving_Flight_Number__c ='87489';
        t.pc.Departing_Flight_Time__c=datetime.now();
        t.pc.Hotel_Check_In_Date__c = date.today();
        t.pc.Hotel_Checkout_Date__c = date.today();
        t.pc.Travel_Request__c = date.today();
        t.pc.Travel_Type__c = 'Flying';
        
        update t.pc;
        
        ECEN_Message__c incomingMessage = new ECEN_Message__c(Insurance_Id__c=t.pc.bid_id__c);
        
        incomingMessage.subject__c = 'Unit.Test';
        incomingMessage.body__c = 'Unit.Test';
        incomingMessage.Sender_Name__c = 'Unit.Test';
        incomingMessage.Sender_Email__c= 'testig@test.com';
        incomingMessage.dateReceived__c= datetime.now();
        
        ECEN_Message__c sentMessage = incomingMessage.clone();
        sentMessage.sentMessage__c=true;
        sentMessage.dateSent__c= datetime.now();
        sentMessage.dateReceived__c=null;
        
        Ecen_Contact_Number__c ecn = new Ecen_Contact_Number__c();
        ecn.Contact_Number__c= '(480) 555-1212';
        ecn.name= 'unit.test';
        
        sObject[] solist = new sobject[]{incomingMessage, sentMessage};
        
        insert solist;
        
        pc_poc__c poc = t.addPoc(t.pc.id);
        insert poc;
        Test.startTest();
        ApexPages.CurrentPage().getParameters().put('tok','132456798');
        cognitoTesting ct = new cognitoTesting();
        ct.getJsonString();
        
        Test.stopTest();
            
    }
}