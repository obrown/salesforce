/*

This class is used by the test class Transplant Test

*/


public with sharing class createTransplant {

	Transplant__c tr = new Transplant__c();  

	public Transplant__c newTransplant(boolean doCommit){
				
		tr.bid__c='123456789';
        tr.transplant_type__c='Kidney;Liver';
        tr.Relationship_to_the_Insured__c = 'Spouse';
        tr.Employee_First_Name__c = 'Johnny';
        tr.Employee_First_Name__c = 'Test';
        tr.Employee_DOB__c = date.today().addyears(-30);
        tr.Employee_DOBe__c = string.valueof(date.today().addyears(-30));
        tr.Employee_Gender__c = 'Male';
        tr.Employee_SSN__c = '123456789';
        tr.Patient_First_Name__c ='Jane';
        tr.Patient_Last_Name__c ='Test';
        tr.PatientHomePhoneDisp__c = '(480) 555-1212';
        tr.Patient_Home_Phone__c ='(480) 555-1212';
        tr.Patient_DOB__c = date.today().addyears(-29);
        tr.Patient_DOBe__c = string.valueof(date.today().addyears(-29));
        tr.patient_Gender__c = 'Female';
        tr.Patient_Email_Address__c = 'test@hdplus.com';
        tr.Same_as_Employee_Address__c = true;
        tr.Employee_Street__c = '123 E Main St';
        tr.Employee_City__c = 'Mesa';
        tr.Employee_State__c = 'AZ';
        tr.Employee_Zip_Postal_Code__c = '85206';
        tr.EmployeeHomePhoneDisp__c = '(480) 555-1212';
        tr.Carrier_Name__c= 'Aetna';
        tr.intake_status__c ='Open';
        tr.intake_status_reason__c ='Intake Incomplete';
        tr.RecordTypeId = [select id from recordtype where sObjectType = 'Transplant__c'].id;
        tr.referred_facility__c='Mayo Scottsdale';
        if(doCommit){insert tr;}
        
		return tr;
		
	}
	
	public void createStages(string stageType, Transcontroller tc, date stageeff, date stageTerm)
	{
		
		tc.obj.Stage_Effective_Date__c = stageeff;
        tc.obj.Stage_Termination_Date__c = stageterm;
        
		if(stageType=='Pre')
		{
			tc.addStagePre();
		}
		
		if(stageType=='Global')
		{
			tc.addStageGlobal();
		}
		
		if(stageType=='Post')
		{
			tc.addStagePost();
		}
		
		
	}
	
	public Caregiver__c addCG(id theID){
		
		caregiver__c cg = new caregiver__c(parentid__c = theID,
        								   HomePhone__c='(480) 555-1212',
        								   MobilePhone__c='(480) 555-1212',
        								   Relationship_to_the_Patient__c='Brother',
        								   Street__c='123 E Main St',
        								   City__c='Gilbert',
        								   State__c='AZ',
        								   isActive__c=true, 
        								   Zip_Postal_Code__c='85265', 
        								   caregiver_dobdisp__c= date.today().addyears(-35),
        								   Caregiver_First_Name__c='John', 
        								   Caregiver_Last_Name__c='Doe',
        								   caregiver_home_phone__c='(480) 555-1212', 
        								   caregiver_dob__c = '1980-01-02',
        								   Caregiver_Mobile_Phone__c = '(480) 555-1212',
        								   isPCM__c = false
        								   );
    
    	insert cg;
    	
    	return cg;
		
	}
	
	public void addPro(Transcontroller tc){
		
		tc.obj.Provider_Type__c='Doc';
		tc.obj.Provider_Credentials__c= 'MD';
		tc.profname ='John';
		tc.prolname='Doc';
		tc.obj.Provider_Phone__c='(480) 555-1212';
		tc.obj.Provider_Fax_Number__c='(480) 555-1212';
		tc.proemail='test@hdplus.com';
		tc.prostreet='123 E Main St';
		tc.procity='Mesa';
		tc.obj.Provider_State_Province__c='AZ';
		tc.prozip='85297'; 
		
	}
	
	public void addAppt(Transcontroller tc, string theStage){
		
        tc.obj.Caregiver_Needed__c = 'No';
        tc.Apptcg ='John Smith';
        tc.obj.appt_Notes__c = 'Test';
        tc.obj.Estimated_Appointment_Start__c = date.today().addDays(-2);
        tc.obj.Actual_Appointment_Start__c = date.today().addDays(-2);
        tc.obj.Estimated_Appointment_End__c = date.today();
        tc.obj.Actual_Appointment_End__c = date.today();
        tc.obj.Appt_Type__c = 'Evaluation';
        
        if(theStage=='Pre'){
        	tc.addAppt();
        }        
        
        if(theStage=='Global'){
        	tc.addApptGlobal();
        } 
        
        if(theStage=='Post'){
        	tc.addApptPost();
        } 
		
	}	
	
}