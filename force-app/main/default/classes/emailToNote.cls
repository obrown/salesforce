global with sharing class emailToNote implements Messaging.InboundEmailHandler {

    global Messaging.InboundEmailResult handleInboundEmail(Messaging.InboundEmail email, Messaging.InboundEnvelope envelope) {
        Messaging.InboundEmailResult emailresult = new Messaging.InboundEmailresult();
        
        if(email.subject.contains('PC-')){
            string pcname = email.subject.substring(email.subject.indexof('PC-'),email.subject.indexof('PC-')+11);
            string emailBody = email.plainTextBody;
            emailBody = emailBody.replace('THIS MESSAGE AND OR ANY ATTACHMENTS IS INTENDED ONLY FOR PERSONAL AND CONFIDENTIAL USE OF THE INDIVIDUAL OR ENTITY TO WHOM IT IS ADDRESSED AND MAY CONTAIN INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, AND EXEMPT FROM DISCLOSURE UNDER APPLICABLE LAW. If the reader of this message is not the intended recipient, or the employee or agent responsible for delivering the message to the intended recipient, you are hereby notified that you have received this message in error and that any review, dissemination, distribution, or copying of this message is strictly prohibited. If you have received this message in error, please notify the sender immediately by e-mail or telephone, and delete the original message immediately. Thank you.','');
        
            id pcId = [select id from patient_case__c where name = :pcname].id;
        
            Program_Notes__c note         = new Program_Notes__c();
            if(emailBody.left(20).contains('____________')){
                note.Initiated__c = 'Inbound';
                note.subject__c = 'E-mail Received';
            }else{
                note.Initiated__c = 'Outbound';
                note.subject__c = 'E-mail Update';
            }
        
            emailBody = emailBody.replaceAll('_','').trim();
            note.patient_case__c          = pcId;
            note.communication_type__c    = 'Email';
            note.type__c                  = 'Email';
            note.Contact_Type__c          = 'Email';
            note.Notes__c                 = emailBody;
            note.Legacy_Created_By__c     = userInfo.getName();
            note.Legacy_LastModifiedBy__c = userInfo.getName();
            note.legacyCreatedDate__c     = datetime.now();
            note.Legacy_LastModifiedDate__c = datetime.now();
            insert note;
        
            return emailresult;
        }else if(email.subject.contains('Oncology-')){
            string oncologyname = email.subject.substring(email.subject.indexof('Oncology-'),email.subject.indexof('Oncology-')+15);
            string emailBody = email.plainTextBody;
            emailBody = emailBody.replace('THIS MESSAGE AND OR ANY ATTACHMENTS IS INTENDED ONLY FOR PERSONAL AND CONFIDENTIAL USE OF THE INDIVIDUAL OR ENTITY TO WHOM IT IS ADDRESSED AND MAY CONTAIN INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, AND EXEMPT FROM DISCLOSURE UNDER APPLICABLE LAW. If the reader of this message is not the intended recipient, or the employee or agent responsible for delivering the message to the intended recipient, you are hereby notified that you have received this message in error and that any review, dissemination, distribution, or copying of this message is strictly prohibited. If you have received this message in error, please notify the sender immediately by e-mail or telephone, and delete the original message immediately. Thank you.','');
        
            id oncologyId = [select id from oncology__c where name = :oncologyname].id;
        
            Oncology_Case_Note__c note         = new Oncology_Case_Note__c(Oncology__c=oncologyId);
            if(emailBody.left(20).contains('____________')){
                note.Initiated__c = 'Inbound';
                note.subject__c = 'E-mail Received';
            }else{
                note.Initiated__c = 'Outbound';
                note.subject__c = 'E-mail Update';
            }
        
            emailBody = emailBody.replaceAll('_','').trim();
            note.communication_type__c    = 'Email';
            note.Contact_Type__c          = 'Email';
            note.Body__c                  = emailBody;
            insert note;
        
            return emailresult;
        }else if(email.subject.contains('Bariatric-')){
            string bariatricname = email.subject.substring(email.subject.indexof('Bariatric-'),email.subject.indexof('Bariatric-')+17);
            string emailBody = email.plainTextBody;
            emailBody = emailBody.replace('THIS MESSAGE AND OR ANY ATTACHMENTS IS INTENDED ONLY FOR PERSONAL AND CONFIDENTIAL USE OF THE INDIVIDUAL OR ENTITY TO WHOM IT IS ADDRESSED AND MAY CONTAIN INFORMATION THAT IS PRIVILEGED, CONFIDENTIAL, AND EXEMPT FROM DISCLOSURE UNDER APPLICABLE LAW. If the reader of this message is not the intended recipient, or the employee or agent responsible for delivering the message to the intended recipient, you are hereby notified that you have received this message in error and that any review, dissemination, distribution, or copying of this message is strictly prohibited. If you have received this message in error, please notify the sender immediately by e-mail or telephone, and delete the original message immediately. Thank you.','');
        
            id BariatricId = [select id from Bariatric__c where name = :bariatricname].id;
        
            Bariatric_Note__c note         = new Bariatric_Note__c(Bariatric__c=BariatricId );
            emailBody = emailBody.replaceAll('_','').trim();
            note.Action__c            = 'Email';
            note.Note__c                  = emailBody;
            insert note;
        
            return emailresult;
        }else{
            return emailresult;
        }
        
        
    
    }
}