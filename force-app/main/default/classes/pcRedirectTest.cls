/**
* This class contains unit tests for validating the behavior of  the Apex addNoteController class
*/

@isTest
private class pcRedirectTest {

    static testMethod void pcRedirect() {
        
        map<string, recordType> rtMap = new map<string, recordType>();
        for(recordType r : [select name, id, developerName from recordType where isActive = true]){
        
            rtMap.put(r.name, r);
        
        }
        
        patient_case__c pc = new patient_case__c();
        pc.employee_first_name__c = 'Unit';
        pc.employee_last_name__c = 'Unit';
        
        pc.recordType = rtMap.get('Lowes Joint');
        
        
        insert pc;        
        pc = [select recordtypeid, id,program_type__c,isConverted__c,case_type__c from patient_case__c where id = :pc.id];        
        
        system.debug(pc.case_type__c+' '+pc.program_type__c);
        
        pcRedirect pcr = new pcRedirect(new ApexPages.StandardController(pc));
        pcr.redirector();
        
        pc.recordtypeid=rtMap.get('Lowes Joint').id;
        update pc;
        pc = [select recordtypeid, id,program_type__c,isConverted__c,case_type__c from patient_case__c where id = :pc.id]; 
        pcr = new pcRedirect(new ApexPages.StandardController(pc));   
        pcr.redirector();
        
        ApexPages.CurrentPage().getParameters().put('RecordType',rtMap.get('Walmart Spine').id);
        pcr.redirector();
        
        ApexPages.CurrentPage().getParameters().put('RecordType',rtMap.get('Walmart Cardiac').id);
        pcr.redirector();
            
    }
    
}