public with sharing class populationHealthHeaderController {

    public integer unreadCount { get; set; }
    
    public populationHealthHeaderController(){
        unreadCount=0;
        set<string> certSet = new set<string>();
        set<string> idSet = new set<string>();
        
        for(phm_patient__c pat:  [select Cert__c from phm_Patient__c]){   
                certSet.add(pat.cert__c);
                idSet.add(pat.id);
            }
         try{   
         ECEN_Message__c [] unreadMessages = [select id from ECEN_Message__c where (phm_Patient__c in :idset) and Patient_Sent_Message__c= true and unread__c=true order by createdDate desc];
         unreadCount = unreadMessages.size();  
         }catch(queryException qe){}
    
    }
}