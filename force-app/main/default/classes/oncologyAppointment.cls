public with sharing class oncologyAppointment{
    
    id parentId;
    
    public Oncology_Appointment__c [] Appointments {get; set;}
    public Oncology_Appointment__c Appointment {get; set;}
    
    public oncologyAppointment(id parentId){
        setparentId(parentId);
        loadAppointments();
    }
    
    public void setparentId(id parentId){
        this.parentId=parentId;
    }
    
    public void loadAppointments(){
        Appointments = new Oncology_Appointment__c[]{};
        Appointments = [select Actual_End__c, Name, Actual_Start__c, Appointment_Notes__c, Appointment_Type__c, createdby.name, createdDate,Canceled_Notification_Date__c,Estimated_End__c,Estimated_Start__c from Oncology_Appointment__c where parentID__c = :parentId];
        
        
    }
    
    public void newAppointment(){
        Appointment = new Oncology_Appointment__c (parentID__c=parentId);
        
    }
    
    public void saveAppointment(){
        try{
           
           upsert Appointment;
           loadAppointments();
           
        }catch(dmlException e){
           for (Integer i = 0; i < e.getNumDml(); i++){
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)+' '+e.getLineNumber()); 
           } 
        }
    }
    
    public void cancelAppointment(){
        Appointment = null;
        
    }    
    
    public void openAppointment(){
        Oncology__c o;
        string apptId = ApexPages.CurrentPage().getParameters().get('apptId');
        if(apptId==null){
            appointment = new Oncology_Appointment__c (parentID__c=o.id);
            return;
        }else{
            for(Oncology_Appointment__c  a : Appointments){
                if(a.id==apptId){
                    appointment=a;
                    break;
                }
            }    
        
        }
                    
    }    
    
    public void deleteAppointment(){
        
        string apptId = ApexPages.CurrentPage().getParameters().get('apptId');
        if(apptId!=null){
             Oncology_Appointment__c a = new Oncology_Appointment__c(id=apptId);
             delete a;
             loadAppointments();
        }   
    } 

}