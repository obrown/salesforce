public without sharing class OncologyMapAmzToCOH{
    
    am_account_xx acct = new am_account_xx();
    
    public void setTheAcct(oncology__c o, string recordtypeid){
        acct.RecordTypeId=recordtypeid; // 
        system.debug(' acct.RecordTypeId: ' +acct.RecordTypeId);
        acct.HDP_Parent_ID_xx=o.id;
        acct.Referral_Date_and_Time_xx= o.Referral_Date__c;
        acct.Date_Eligibility_Verified_xx = o.Date_Eligibility_Verified__c;
        acct.First_Name_xx =o.Patient_First_Name__c;
        acct.Last_Name_xx =o.Patient_Last_Name__c;
         
        acct.Patient_Preferred_Name_xx = o.Patient_Preferred_Name__c;
        acct.Email_xx= o.Patient_Email_Address__c;
        acct.Gender_xx=o.Gender__c;
        acct.Patient_COE_Benefit_Identification_Numbe_xx= o.Patient_Medical_Plan_Number__c;
        acct.Date_of_birth_xx = date.valueof(o.Patient_DOB__c);
        
        acct.Mailing_Street_XX= o.Patient_Address__c;
        acct.Mailing_City_xx=o.Patient_City__c;
        acct.Mailing_State_Province_XX= oncolcogyStateFormat.stateFormal(o.Patient_State__c);
        acct.Mailing_Zip_Postal_Code_xx=o.Patient_Zip_Code__c;
        acct.Mailing_Country_xx='United States';
        
        acct.Mobile_Phone_xx= formatPhoneNumber(o.Patient_Phone2__c);
        acct.Other_Phone_xx= formatPhoneNumber(o.Patient_Phone3__c);
        acct.Home_Phone_xx= formatPhoneNumber(o.Patient_Phone__c);
        
        if(o.Patient_Preferred_Phone__c != null){
            acct.Preferred_phone_xx = o.Patient_Preferred_Phone__c +' Phone';
        }
        
        acct.Best_Time_to_Contact_xx = o.Patient_Best_Time_To_Contact__c;
        
        if(o.Relationship_to_Subscriber__c=='Employee'){
            acct.Relationship_to_Employee_xx = 'Patient is '+o.Relationship_to_Subscriber__c;
        }else{
            acct.Relationship_to_Employee_xx = o.Relationship_to_Subscriber__c;
        }
        
        acct.Employee_First_Name_xx = o.Employee_First_Name__c;
        acct.Employee_Last_Name_xx = o.Employee_Last_Name__c;
        acct.Employee_Date_of_Birth_xx = date.valueof(o.Employee_DOB__c);
        acct.Primary_Diagnosis_PatientReported_to_HDP_xx= o.Primary_Diagnosis_Patient_Reported__c;
        acct.Other_Pertinent_Medical_Information_xx = o.Other_Pertinent_Medical_Information__c;
        if(o.Treatment_Patient_Reported__c!='None'){
            acct.Treatment_Patient_Reported_to_HDP_xx = o.Treatment_Patient_Reported__c;
        }
        acct.Other_Pertinent_Medical_Information_xx = o.Other_Pertinent_Medical_Information__c;
        acct.Date_of_Diagnosis_PatientReportedto_HDP_xx= date.valueof(o.Date_of_Diagnosis_Patient_Reported__c);
        acct.COH_Nurse_Name_xx=o.Facility_Nurse_Name__c;
        acct.COH_Nurse_Phone_Number_xx=formatPhoneNumber(o.Facility_Nurse_Phone_Number__c);
        acct.COH_Nurse_Email_xx=o.Facility_Nurse_Email__c;
        acct.HDP_Nurse_Name_xx=o.HDP_Nurse__r.name;
        acct.HDP_Nurse_Email_xx=o.HDP_Nurse_Email__c;
        acct.HDP_Nurse_Phone_Number_xx=o.HDP_Nurse_Phone_Number__c;
        
        acct.Referral_Facility_xx = 'City of Hope';
        acct.Employer_Program_xx = 'Oncology';
        //acct.HDP_Nurse_Phone_Number__c=formatPhoneNumber(o.Date_of_Diagnosis_Patient_Reported__c;
        //acct.HDP_Nurse_Email__c=o.Date_of_Diagnosis_Patient_Reported__c;
        
        Oncology_Physician__c physician = new Oncology_Physician__c(); 
        try{
            physician = [select Local_Physician__c,Associated_Facilities__c, City__c, Credentials__c,First_Name__c, Fax_Number__c,Last_Name__c,Phone_Number__c,Specialty__c,State__c,Street_Address__c,Zip_Code__c from Oncology_Physician__c where  oncology__c  = :o.id and Local_PCP__c=true limit 1];
        
            if(physician !=null){
                acct.Local_Physician_First_Name_Last_Name_xx = physician.First_Name__c +' '+physician.Last_Name__c;//
                acct.Local_Physician_Credentials_xx = physician.Credentials__c;
                acct.Local_Physician_Specialty_xx = physician.Specialty__c;
                acct.Local_Physician_Street_Address_xx=physician.Street_Address__c;
                acct.Local_Physician_City_xx=physician.City__c;
                acct.Local_Physician_State_xx=string.valueof(oncolcogyStateFormat.stateFormal(physician.State__c));
                acct.Local_Physician_Zip_xx=physician.Zip_Code__c ;
                acct.Local_Physician_Country_xx='United States';
                acct.Local_Physician_Phone_Number_xx=formatPhoneNumber(physician.Phone_Number__c);
                acct.Local_Physician_Fax_Number_xx=formatPhoneNumber(physician.Fax_Number__c);
                acct.Associated_Facilities_xx=physician.Associated_Facilities__c;
   
            }
        
        }catch(queryException se){
        
        }
    }
    
    public static string getTheAcct(oncology__c o, string recordtypeid){
        OncologyMapAmzToCOH mapper = new OncologyMapAmzToCOH();
        mapper.setTheAcct(o, recordtypeid);
        string jsonAcct = JSON.serialize(mapper.acct, true);
        
        //order matters here
        jsonAcct = jsonAcct.replaceAll('_xxpc','__pc');
        jsonAcct = jsonAcct.replaceAll('_xx','__c');
        return jsonAcct;
    }
    
    string formatPhoneNumber(string original){
        
        if(original==null||original==''){return original;}
        
        string newPhone = original.replaceFirst('\\(','');
        newPhone = newPhone.replaceFirst('\\)','');
        newPhone = newPhone.replaceAll(' ','');
        newPhone = newPhone.replaceFirst('-','');
        return newPhone;
        
    }
    
    public static void mapIncomingOncologyId(Oncology_Clinical_Facility__c[] ocfList, set<id> extIds){
        
        map<string,id> extMap = new map<string, id>();
        
        for(Oncology__c o : [select External_ID__c from Oncology__c where External_ID__c in :extIds]){
            extmap.put(o.External_ID__c, o.id);
        }
        
        for(Oncology_Clinical_Facility__c ocf : ocfList){
            ocf.parentId__c = extMap.get(ocf.Facility_Record_ID__c);
            system.debug(ocf.parentId__c);
        }
        
    } 
    
}