public class FileChangeRequestResponseStruct{
    
    public string ResultCode {get; set;}
    public string Message {get; set;}
    public string ErrorReason {get; set;}
    public files[] Filelist {get; set;}
    
    
    public class files{
        public string Filename {get; set;}
        public datetime LastModified {get; set;}
        
    }
    
    
    
}