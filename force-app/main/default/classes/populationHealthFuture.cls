public with sharing class populationHealthFuture {
    @future(callout = true)
    public static void eligbility(set<id> theIDs){
        phm_Patient__c[] patientList = new phm_Patient__c[] {};
        for (phm_Patient__c patient : [select Patient_Social_Security_Number__c, Patient_Employer__r.Underwriter__c, Employer_Group__c, sequence__c from phm_Patient__c where id in :theIds]) {
            try{
                hpEligDetailStruct eligDetail = hp_eligSearch.searchHP(patient.Patient_Social_Security_Number__c,
                    patient.Patient_Employer__r.Underwriter__c,
                    patient.Employer_Group__c,
                    patient.sequence__c,
                    string.valueOf(date.today().addDays(-1)).remove('-'));
                eligDetail.cleanUp();

                patient.Medical_Status__c = utilities.decodeStatus(eligDetail.eligibilityDetail.Status);
                patient.Most_Recent_Plan_Number__c = eligDetail.eligibilityDetail.MedicalBenefitPlan;
                if (eligDetail.eligibilityDetail.EligDate != null) {
                    string[] eDate = eligDetail.eligibilityDetail.EligDate.split('/');
                    patient.Effective_Date__c = date.valueof(eDate [2] + '-' + eDate [0] + '-' + eDate [1]);
                }

                patient.Last_Date_of_Eligibility_Check__c = datetime.now();
                patientList.add(patient);
            }catch (exception e) {}
        }

        update patientList;
    }
    /*
     * @future(callout=true)
     * public static void eligbility(id theID){
     *  Utilization_Management__c umRecord = [select Patient__r.Effective_Date__c,
     *                                               Patient__r.Termination_Date__c,
     *                                               From_Date__c,
     *                                               Thru_Date__c,
     *                                               Approved_Days__c,
     *                                               Patient__r.Subscriber_Social_Security_Number__c,
     *                                               Patient__r.Patient_First_Name__c,
     *                                               Patient__r.Patient_Last_Name__c,
     *                                               Patient__r.Patient_Date_of_Birth__c,
     *                                               HealthPac_Case_Number__c,
     *                                               Comment_Line_for_Claims__c,
     *                                               Patient__r.Patient_Employer__r.Underwriter__c,
     *                                               Patient__r.Employer_Group__c,
     *                                               Patient__r.Sequence__c,
     *                                               Patient__r.Cert__c from Utilization_Management__c where id = :theId];
     *
     *
     * }
     *
     * public static string formatDate(Integer i){
     *  string s = string.valueof(i);
     *  if(s.length()==1){
     *      return '0'+s;
     *  }
     *
     *  return s;
     * }
     */

    @future(callout = true)
    public static void sendImagePacFileRoute(set<id> theIds){
        date t = date.today();

        Population_Health_Attachment__c[] alist = new Population_Health_Attachment__c[] {};
        for (Population_Health_Attachment__c owt : [select ImpagePacMessageSent__c, File_Name__c, name, Patient__r.Patient_Social_Security_Number__c, Patient__r.Employer_Group__c, Patient__r.Patient_Employer__r.underwriter__c from Population_Health_Attachment__c where id in: theIds]) {
            hpImpagePacStruct hpi = new     hpImpagePacStruct();
            hpi.Filename = owt.File_Name__c;

            hpi.Filename = hpi.Filename.replace('.pdf', '.tif').tolowercase();
            hpi.ReceivedDate = t.year() + '' + utilities.formatDate(t.month()) + '' + utilities.formatDate(t.day());
            hpi.ScanDate = t.year() + '' + utilities.formatDate(t.month()) + '' + utilities.formatDate(t.day());
            hpi.ServerDesc = '';
            hpi.UserDesc = ' ' + hpi.Filename;
            hpi.EntityType = 'E';
            hpi.ImageType = 'A';
            hpi.HpKeyPartA = owt.Patient__r.Patient_Employer__r.Underwriter__c;
            hpi.HpKeyPartB = owt.Patient__r.Employer_Group__c;
            hpi.HpKeyPartC = owt.Patient__r.Patient_Social_Security_Number__c;

            hp_ImagePacUpdate.sendImagePacUpdate(hpi);
            owt.ImpagePacMessageSent__c = date.today();
            alist.add(owt);
        }
        update alist;
    }

    @future(callout = true)
    public static void syncPopHealth(set<id> idset){
        phm_Patient__c[] patientList = new phm_Patient__c[] {};//added 7-9-21
        set<id> dupIds = new set<id>();

        for (phm_Patient__c p : [select id,
                                 Patient_Date_of_Birth__c
                                 from phm_Patient__c where id in :idset]) {
            phm_Patient__c patient = new phm_Patient__c(id = p.id);

            if (p.Patient_Date_of_Birth__c != null) {
                try{
                    string[] dobArray = p.Patient_Date_of_Birth__c.split('/');
                    date dob = date.valueof(dobArray [2] + '-' + dobArray [0] + '-' + dobArray [1]);
                    patient.Patient_Age__c = dob.daysBetween(date.today()) / 365;
                }catch (exception e) {}
            }

            if (!dupIds.contains(p.id)) {
                patientList.add(patient);
                dupIds.add(p.id);
            }
        }

        //End loop

        if (!patientList.isEmpty()) {
            database.update(patientList, false);
        }
    }
}