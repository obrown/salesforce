public with sharing class oncologyAllowance{
    
    id parentId;
    
    public Oncology_Allowance__c[] allowances {get; set;}
    public Oncology_Allowance__c   allowance {get; set;}
    
    public oncologyAllowance(id parentId){
        setparentId(parentId);
        loadAllowances();
    }
    
    public void setparentId(id parentId){
        this.parentId=parentId;
    }
    
    public void loadAllowances(){
        allowances = new Oncology_Allowance__c[]{};
        allowances = [select Amount__c, Name, Note__c, Paid_Date__c, Type__c, createdby.name, createdDate from Oncology_Allowance__c where oncology__c = :parentId order by paid_date__c desc];
        
        
    }
    
    public void newAllowance(){
        allowance = new Oncology_Allowance__c(oncology__c=parentId);
        
    }
    
    public void saveAllowance(){
        try{
           
           upsert allowance;
           loadAllowances();
           
        }catch(dmlException e){
           for (Integer i = 0; i < e.getNumDml(); i++){
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)+' '+e.getLineNumber()); 
           } 
        }
    }
    
    public void cancelAllowance(){
        allowance = null;
        
    }    
    
    public void openAllowance(){
        string allowanceID = ApexPages.CurrentPage().getParameters().get('allowanceId');
        if(allowanceId==null){newAllowance();return;}
        for(Oncology_Allowance__c oa : allowances){
            if(oa.id==allowanceID){
                allowance = oa;
                break;
            }
        }
        
    }    
    
    public void deleteAllowance(){
        string allowanceID = ApexPages.CurrentPage().getParameters().get('allowanceId');
        if(allowanceId==null){return;}
        Oncology_Allowance__c a = new Oncology_Allowance__c(id=allowanceId);
        delete a;
        loadAllowances();
    }

}