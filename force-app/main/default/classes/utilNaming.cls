public with sharing class utilNaming {

	public string run(list<string> theLetters){
	
		string updatedLetter = 'A';
		integer lastLetter = 0;
		list<integer> nList = new list<integer>();
		nLIst = encodeLettervalue(theLetters);
		
		for(integer n: nLIst){

			if(lastLetter < n){
				lastLetter = n;			
			}
			
		}


		map<integer,string> appendMap = populateMap();
		
		updatedLetter = appendMap.get(lastLetter);
		
		return updatedLetter;
	}
	

	
	public map<integer,string> populateMap(){
		
		map<integer,string> appendMap = new map<integer,string>();
		appendMap.put(1,'B');
		appendMap.put(2,'C');
		appendMap.put(3,'D');
		appendMap.put(4,'E');
		appendMap.put(5,'F');
		appendMap.put(6,'G');
		appendMap.put(7,'H');
		appendMap.put(8,'I');
		appendMap.put(9,'J');
		appendMap.put(10,'K');
		appendMap.put(11,'L');
		appendMap.put(12,'M');
		appendMap.put(13,'N');
		appendMap.put(14,'O');
		appendMap.put(15,'P');
		appendMap.put(16,'Q');
		appendMap.put(17,'R');
		appendMap.put(18,'S');
		appendMap.put(19,'T');
		appendMap.put(20,'U');
		appendMap.put(21,'V');
		appendMap.put(22,'W');
		appendMap.put(23,'X');
		appendMap.put(24,'Y');
		appendMap.put(25,'Z');
		appendMap.put(26,'AA');
		
		return appendMap;
	
	}

	public list<integer> encodeLettervalue(list<string> theList ){
	
		list<integer> theNumbers = new list<integer>();

		map<string, integer> lettertointeger = new map<string, integer>();
		lettertointeger.put('A',1);
		lettertointeger.put('B',2);
		lettertointeger.put('C',3);
		lettertointeger.put('D',4);
		lettertointeger.put('E',5);
		lettertointeger.put('F',6);
		lettertointeger.put('G',7);
		lettertointeger.put('H',8);
		lettertointeger.put('I',9);
		lettertointeger.put('J',10);
		lettertointeger.put('K',11);
		lettertointeger.put('L',12);
		lettertointeger.put('M',13);
		lettertointeger.put('N',14);
		lettertointeger.put('O',15);
		lettertointeger.put('P',16);
		lettertointeger.put('Q',17);
		lettertointeger.put('R',18);
		lettertointeger.put('S',19);
		lettertointeger.put('T',20);
		lettertointeger.put('U',21);
		lettertointeger.put('V',22);
		lettertointeger.put('W',23);
		lettertointeger.put('X',24);
		lettertointeger.put('Y',25);
		lettertointeger.put('Z',26);

			for(string l: thelist){
				theNumbers.add(lettertointeger.get(l));								


			}
		
		return theNumbers;

	}
	
	static testMethod void testUtilClass(){
		
		utilNaming un = new utilNaming();
		
		un.populateMap();
		list<String> theList = new list<String>();
		theList.add('A');
		un.encodeLettervalue(theList);
		
		theList.add('Z');
		un.run(thelist);
		
		
	}
	

}