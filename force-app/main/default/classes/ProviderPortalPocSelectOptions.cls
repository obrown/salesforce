public with sharing class ProviderPortalPocSelectOptions {
    class option {
        string label;
        string value;

        option(string label, string value) {
            this.label = label;
            this.value = value;
        }
    }

    public poc_selectoptions getSelectOptions(string case_type, string client_facility) {
        poc_selectoptions opts = new poc_selectoptions();

        switch on (case_type) {
            when 'Spine' {
                opts.candidateOptions = candidateOptions();
                opts.clinicallyVerifiedOutcome = spineClinicallyVerifiedOutcome();
                opts.conservativeTreatment = spineConservativeTreatment();
                opts.planOfCareType = spinePlanOfCareType();
                opts.proposedAddOnServices = spineProposedAddOnServices();
                opts.proposedServiceReason = spineProposedServiceReason();
                opts.proposedServiceTrip = spineProposedServiceTrip();
                opts.proposedServiceType = spineProposedServiceType();
                opts.servicesRequestedOutsideofContracted = servicesRequestedOutsideofContracted();
                opts.surgeons = getSurgeons(client_facility);
                opts.surgeryHistoryStatus = surgeryHistoryStatus();
            }

            when 'Joint' {
                opts.candidateOptions = candidateOptions();
                opts.clinicallyVerifiedOutcome = jointClinicallyVerifiedOutcome();
                opts.hipApproach = jointHipApproach();
                opts.planOfCareType = jointPlanOfCareType();
                opts.proposedAddOnServices = jointProposedAddOnServices();
                opts.proposedServiceTrip = jointProposedServiceTrip();
                opts.proposedServiceType = jointProposedServiceType();
                opts.proposedServiceReason = jointProposedServiceReason();
                opts.servicesRequestedOutsideofContracted = servicesRequestedOutsideofContracted();
                opts.surgeons = getSurgeons(client_facility);
            }
        }

        opts.allowedModeOfTransportation = jointAllowedModeOfTransportation();
        opts.patientPreferredModeOfTravel = jointPatientPreferredModeOfTravel();

        return opts;
    }

    public class poc_selectoptions {
        option[] allowedModeOfTransportation = new option[] {};
        option[] hipApproach = new option[] {};
        option[] patientPreferredModeOfTravel = new option[] {};
        option[] planOfCareType = new option[] {};
        option[] proposedAddOnServices = new option[] {};
        option[] surgeryHistoryStatus = new option[] {};
        option[] surgeons = new option[] {};

        map<string, option[]> candidateOptions = new map<string, option[]>();
        map<string, option[]> clinicallyVerifiedOutcome = new map<string, option[]>();
        map<string, option[]> conservativeTreatment = new map<string, option[]>();
        map<string, option[]> proposedServiceTrip = new map<string, option[]>();
        map<string, option[]> proposedServiceReason = new map<string, option[]>();
        map<string, option[]> proposedServiceType = new map<string, option[]>();
        map<string, option[]> servicesRequestedOutsideofContracted = new map<string, option[]>();

        poc_selectoptions(){
            proposedServiceReason = new map<string, option[]>();
        }
    }

    option[] spinePlanOfCareType(){
        option[] planOfCareType = new option[] {};
        planOfCareType.add(new option('Additional Trip', 'Additional Trip'));
        planOfCareType.add(new option('Initial', 'Initial'));
        planOfCareType.add(new option('Second Trip', 'Second Trip'));

        return planOfCareType;
    }

    option[] jointPlanOfCareType(){
        option[] planOfCareType = new option[] {};
        planOfCareType.add(new option('Complication', 'Complication'));
        planOfCareType.add(new option('Initial', 'Initial'));
        planOfCareType.add(
            new option('Reschedule - Travel', 'Reschedule - Travel')
        );

        return planOfCareType;
    }

    map<string, option[]> jointProposedServiceTripReason(){
        map<string, option[]> result = new map<string, option[]>();

        result.put('Initial', new option[] {
            new option('New', 'New')
        });

        return result;
    }

    option[] jointHipApproach(){
        return new option[] {
            new option('Anterior', 'Anterior'),
            new option('Posterior', 'Posterior'),
            new option('Unknown', 'Unknown')
        };
    }

    map<string, option[]> jointClinicallyVerifiedOutcome(){
        map<string, option[]> result = new map<string, option[]>();

        option[] opts = new option[] {
            new option(
                'Cancelled - No Program Surgery After Arrival',
                'Cancelled - No Program Surgery After Arrival'
            ),
            new option('Follow-up Services', 'Follow-up Services'),
            new option(
                'Proceed with Program Surgery',
                'Proceed with Program Surgery'
            )
        };
        result.put('Initial', opts);
        result.put('Second Trip', opts);
        result.put('', opts);

        return result;
    }

    map<string, option[]> jointProposedServiceTrip(){
        map<string, option[]> result = new map<string, option[]>();

        result.put(
            'Initial',
            new option[] {
                new option(
                    'Program Surgery - Inpatient',
                    'Program Surgery - Inpatient'
                ),
                new option(
                    'Program Surgery - Outpatient',
                    'Program Surgery - Outpatient'
                )
            }
        );
        result.put(
            'Reschedule - Travel',
            new option[] {
                new option(
                    'Program Surgery - Inpatient',
                    'Program Surgery - Inpatient'
                )
            }
        );
        result.put(
            'Complication',
            new option[] {
                new option('Inpatient', 'Inpatient'),
                new option('Outpatient', 'Outpatient')
            }
        );

        return result;
    }

    map<string, option[]> jointProposedServiceType(){
        map<string, option[]> result = new map<string, option[]>();

        option[] opts = new option[] {
            new option('Left Hip', 'Left Hip'),
            new option('Left Knee', 'Left Knee'),
            new option('Right Hip', 'Right Hip'),
            new option('Right Knee', 'Right Knee')
        };
        result.put('Initial', opts);
        result.put('Reschedule - Travel', opts);
        result.put(
            'ComplicationInpatient',
            new option[] {
                new option('Surgery Revision', 'Surgery Revision'),
                new option('Procedure', 'Procedure'),
                new option('Medical Management', 'Medical Management'),
                new option(
                    'Manipulation Under Anesthesia',
                    'Manipulation Under Anesthesia'
                )
            }
        );
        result.put(
            'ComplicationOutpatient',
            new option[] {
                new option(
                    'Manipulation Under Anesthesia',
                    'Manipulation Under Anesthesia'
                ),
                new option('Medical Management', 'Medical Management'),
                new option('Surgery Revision', 'Surgery Revision')
            }
        );

        return result;
    }

    map<string, option[]> spineProposedServiceReason(){
        map<string, option[]> result = new map<string, option[]>();

        option[] opts = new option[] {
            new option('New', 'New')
        };
        result.put('Initial', opts);

        opts = new option[] {
            new option(
                'New Symptoms Different Part of Spine',
                'New Symptoms Different Part of Spine'
            ),
            new option(
                'New Symptoms Previous Surgery',
                'New Symptoms Previous Surgery'
            ),
            new option('Planned Follow-up Visit', 'Planned Follow-up Visit'),
            new option(
                'Unplanned Follow-up Complication',
                'Unplanned Follow-up Complication'
            )
        };
        result.put('After Surgery', opts);

        opts = new option[] {
            new option('Complex Surgery Case', 'Complex Surgery Case'),
            new option(
                'First Surgery Cancelled (Reschedule Travel)',
                'First Surgery Cancelled (Reschedule Travel)'
            ),
            new option(
                'New Symptoms No Previous Surgery',
                'New Symptoms No Previous Surgery'
            )
        };
        result.put('No Prior Program Surgery', opts);

        return result;
    }

    map<string, option[]> jointProposedServiceReason(){
        map<string, option[]> result = new map<string, option[]>();

        option[] opts = new option[] {
            new option('Left Hip', 'Left Hip'),
            new option('Left Knee', 'Left Knee'),
            new option('Right Hip', 'Right Hip'),
            new option('Right Knee', 'Right Knee')
        };
        result.put('Initial', opts);
        result.put('Reschedule - Travel', opts);
        result.put('Complication', opts);
        return result;
    }

    option[] jointPatientPreferredModeOfTravel(){
        option[] result = new option[] {
            new option('Drive', 'Drive'),
            new option('Fly', 'Fly'),
            new option('Train', 'Train')
        };

        return result;
    }

    option[] jointAllowedModeOfTransportation(){
        option[] result = new option[] {
            new option(
                'Drive by car with commute',
                'Drive by car with commute'
            ),
            new option(
                'Drive by car with hotel stay',
                'Drive by car with hotel stay'
            ),
            new option('Fly', 'Fly'),
            new option('Train', 'Train')
        };

        return result;
    }

    map<string, option[]> servicesRequestedOutsideofContracted(){
        map<string, option[]> result = new map<string, option[]>();

        result.put(
            'Joint',
            new option[] {
                new option('Cardiac Workup', 'Cardiac Workup'),
                new option('DME', 'DME'),
                new option('Injections', 'Injections'),
                new option('MRI', 'MRI'),
                new option('Physical Therapy', 'Physical Therapy'),
                new option('X-ray', 'Xray'),
                new option('Other', 'Other')
            }
        );
        result.put('Spine', new option[] {
            new option('--None--', '')
        });

        return result;
    }

    map<string, option[]> spineProposedServiceTrip(){
        map<string, option[]> result = new map<string, option[]>();

        result.put(
            'Initial',
            new option[] {
                new option('Evaluation', 'Evaluation'),
                new option(
                    'Evaluation with Planned Inpatient Program Surgery',
                    'Evaluation with Planned Inpatient Program Surgery'
                ),
                new option(
                    'Evaluation with Planned Outpatient Program Surgery',
                    'Evaluation with Planned Outpatient Program Surgery'
                )
            }
        );
        result.put(
            'Second Trip',
            new option[] {
                new option('Evaluation', 'Evaluation'),
                new option(
                    'Evaluation with Planned Inpatient Program Surgery',
                    'Evaluation with Planned Inpatient Program Surgery'
                ),
                new option(
                    'Evaluation with Planned Outpatient Program Surgery',
                    'Evaluation with Planned Outpatient Program Surgery'
                ),
                new option(
                    'Planned Inpatient Non-Program Surgery',
                    'Planned Inpatient Non-Program Surgery'
                ),
                new option(
                    'Planned Outpatient Non-Program Surgery',
                    'Planned Outpatient Non-Program Surgery'
                ),
                new option('Post-Op Assessment', 'Post-Op Assessment')
            }
        );
        result.put(
            'Additional Trip',
            new option[] {
                new option('Evaluation', 'Evaluation'),
                new option(
                    'Evaluation with Planned Inpatient Program Surgery',
                    'Evaluation with Planned Inpatient Program Surgery'
                ),
                new option(
                    'Evaluation with Planned Outpatient Program Surgery',
                    'Evaluation with Planned Outpatient Program Surgery'
                ),
                new option(
                    'Planned Inpatient Non-Program Surgery',
                    'Planned Inpatient Non-Program Surgery'
                ),
                new option(
                    'Planned Outpatient Non-Program Surgery',
                    'Planned Outpatient Non-Program Surgery'
                ),
                new option('Post-Op Assessment', 'Post-Op Assessment')
            }
        );

        return result;
    }
    map<string, option[]> spineProposedServiceType(){
        map<string, option[]> result = new map<string, option[]>();

        result.put(
            '',
            new option[] {
                new option('N/A', 'N/A')
            }
        );
        result.put(
            'Evaluation',
            new option[] {
                new option('Spine Evaluation', 'Spine Evaluation'),
                new option('Virtual Spine Evaluation', 'Virtual Spine Evaluation')
            }
        );
        result.put(
            'Evaluation with Planned Inpatient Program Surgery',
            new option[] {
                new option(
                    'Combined Anterior/Posterior Spinal Fusion Except Cervical Fusion',
                    'Combined Anterior/Posterior Spinal Fusion Except Cervical Fusion'
                ),
                new option(
                    'Combined Anterior/Posterior Cervical Fusion',
                    'Combined Anterior/Posterior Cervical Fusion'
                ),
                new option('Cervical Fusion', 'Cervical Fusion'),
                new option(
                    'Spinal Fusion - Not Cervical/Scoliosis',
                    'Spinal Fusion - Not Cervical/Scoliosis'
                ),
                new option(
                    'Spinal Fusion with Scoliosis - Not Cervical or Anterior/Posterior',
                    'Spinal Fusion with Scoliosis - Not Cervical or Anterior/Posterior'
                ),
                new option('Other', 'Other')
            }
        );
        result.put(
            'Evaluation with Planned Outpatient Program Surgery',
            new option[] {
                new option('Anterior Cervical Discectomy and Fusion', 'ACDF'),
                new option(
                    'Lami with Decompression',
                    'Lami with Decompression'
                ),
                new option(
                    'Lami, Re-exploration, Lumbar',
                    'Lami, Re-exploration, Lumbar'
                ),
                new option(
                    'Lami, Facetectomy, Foraminotomy',
                    'Lami, Facetectomy, Foraminotomy'
                ),
                new option(
                    'Lami, Facetectomy, Foraminotomy - Cervical',
                    'Lami, Facetectomy, Foraminotomy - Cervical'
                ),
                new option('Other', 'Other')
            }
        );
        result.put(
            'Planned Inpatient Non-Program Surgery',
            new option[] {
                new option('Other', 'Other')
            }
        );
        result.put(
            'Planned Outpatient Non-Program Surgery',
            new option[] {
                new option('Other', 'Other')
            }
        );
        result.put(
            'Post-Op Assessment',
            new option[] {
                new option('Other', 'Other')
            }
        );

        return result;
    }
    map<string, option[]> spineClinicallyVerifiedOutcome(){
        map<string, option[]> result = new map<string, option[]>();

        option[] opts = new option[] {};
        opts.add(
            new option('Follow-up Services', 'Follow-up Services')
        );
        opts.add(
            new option('Proceed with Evaluation', 'Proceed with Evaluation')
        );
        opts.add(
            new option(
                'Proceed with Program Surgery',
                'Proceed with Program Surgery'
            )
        );

        result.put('Initial', opts);
          //result.put('Second Trip', new string[]{});
        opts = new option[] {
            new option(
                'Cancelled - No Program Surgery After Arrival',
                'Cancelled - No Program Surgery After Arrival'
            ),
            new option('Follow-up Services', 'Follow-up Services'),
            new option(
                'Proceed with Program Surgery',
                'Proceed with Program Surgery'
            )
        };
        result.put('Additional Trip', opts);

        return result;
    }

    map<string, option[]> spineConservativeTreatment(){
        map<string, option[]> result = new map<string, option[]>();

        option[] opts = new option[] {};
        opts.add(new option('Bone SPECT Scans', 'Bone SPECT Scans'));
        opts.add(new option('Botox Injection', 'Botox Injection'));
        opts.add(new option('CT Scans', 'CT Scans'));
        opts.add(
            new option(
                'Diagnostic Radiology w/ Evaluation X-Ray',
                'Diagnostic Radiology w/ Evaluation X-Ray'
            )
        );
        opts.add(
            new option(
                'EMG Muscle Test One Limb',
                'EMG Muscle Test One Limb'
            )
        );
        opts.add(
            new option(
                'Epidural Steroid Injection',
                'Epidural Steroid Injection'
            )
        );
        opts.add(new option('Facet Injection', 'Facet Injection'));
        opts.add(
            new option(
                'Injection Paravertebral C/T',
                'Injection Paravertebral C/T'
            )
        );
        opts.add(new option('IVC Filter', 'IVC Filter'));
        opts.add(new option('Medial Branch Block', 'Medial Branch Block'));
        opts.add(new option('Occupational Therapy', 'Occupational Therapy'));
        opts.add(new option('Peripheral Injection', 'Peripheral Injection'));
        opts.add(new option('Physical Therapy', 'Physical Therapy'));
        opts.add(new option('Radiofrequency Ablation', 'Radiofrequency Ablation'));
        opts.add(
            new option(
                'Sacroiliac Joint Injection',
                'Sacroiliac Joint Injection'
            )
        );
        opts.add(
            new option('Trigger Point Injection', 'Trigger Point Injection')
        );
        opts.add(
            new option(
                'Walker Folding Wheeled Adjustable',
                'Walker Folding Wheeled Adjustable'
            )
        );

        result.put('Evaluation', opts);

        return result;
    }

    option[] getSurgeons(string client_facility){
        option[] surgeons = new option[] {};
        surgeons.add(new option('None', ''));

        for (Client_Facility_Physician__c phy : [
            select id,
            Physician__r.First_Name__c,
            Physician__r.Last_Name__c
            from Client_Facility_Physician__c
            where client_facility__c = :client_facility
        ]) {
            surgeons.add(
                new option(
                    phy.Physician__r.last_Name__c + ', ' + phy.Physician__r.first_Name__c,
                    phy.id
                )
            );
        }

        return surgeons;
    }
    option[] spineProposedAddOnServices(){
        option[] opts = new option[] {
            new option(
                'Additional Diagnostics of Neuropsych Testing',
                'Additional Diagnostics of Neuropsych Testing'
            ),
            new option(
                'Additional Diagnostics of Ultrasound (Peripheral Muscle or Joint)',
                'Additional Diagnostics of Ultrasound'
            )
        };

        return opts;
    }
    option[] jointProposedAddOnServices(){
        option[] opts = new option[] {
            new option('Yes', 'Yes'),
            new option('No', 'No'),
            new option('Walker', 'Walker')
        };

        return opts;
    }

    map<string, option[]> candidateOptions(){
        map<string, option[]> result = new map<string, option[]>();

        option[] optsGC = new option[] {
            new option('--None--', ''),
            new option('BMI', 'BMI'),
            new option('Caregiver', 'Caregiver'),
            new option('Dental', 'Dental'),
            new option('Diagnostics', 'Diagnostics'),
            new option('Medical Clearance', 'Medical Clearance'),
            new option('Medical Condition', 'Medical Condition'),
            new option(
                'Needs Future Appointment Dates',
                'Needs future appointment dates'
            ),
            new option('Nicotine', 'Nicotine'),
            new option('No Home Physician', 'No Home Physician')
        };
        option[] optsNGC = new option[] {
            new option('--None--', ''),
            new option('Medical Clearance', 'Medical Clearance'),
            new option('Medical Condition', 'Medical Condition'),
            new option('Procedure Not Indicated', 'Procedure Not Indicated'),
            new option('Recommended Procedure', 'Recommended Procedure')
        };
        result.put('GoodCandidate', optsGC);
        result.put('NotGoodCandidate', optsNGC);

        return result;
    }
    option[] surgeryHistoryStatus() {
        option[] opts = new option[] {};
        opts.add(new option('After Surgery', 'After Surgery'));
        opts.add(
            new option('No Prior Program Surgery', 'No Prior Program Surgery')
        );

        return opts;
    }
}
