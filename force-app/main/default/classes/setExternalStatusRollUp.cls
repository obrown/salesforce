public with sharing class setExternalStatusRollUp {

    /*
        @params:
        status       = Status of the case (patient case is assumed)
        statusReason = Status Reason of the case (patient case is assumed)
        subList      = Status Reason sublist of the case (patient case is assumed)
        cgName       = Caregiver name of the case record. Will be used to determine if the case needs a caregiver
        isConverted  = Determines if the case has been referred to a hospital or not
        recordID     = Case record ID 
    */
    
    public static string theStatus(string status, string statusReason, string subList,string cgName, boolean isConverted, id recordID)
    {
        //system.debug('status '+ status+ 'sr '+statusReason+' subList'+subList+' cgName'+cgName+' isConverted'+isConverted);
        string foo ='';
        if(statusReason==null){statusReason='';}
        if(isConverted)
        { 
            
            //After referral 
            if(statusReason != null &&
              (statusReason=='Awaiting Appointment Date Confirmations' || statusReason=='Awaiting Travel Confirmation' || 
               statusReason=='Awaiting Forms'                          || statusReason=='Ready for Arrival'            ||
               statusReason=='Awaiting Nicotine Clearance'             || statusReason.contains('Pending Eligiblity Check')))
            
            { 
                foo = 'Accepted- Pre-Travel';
                return foo; 
            }else if(status=='Completed' || statusReason == 'Claim Paid'){
                foo = 'Paid Claims';
                return foo;
                
            }else if(statusReason=='Claim Pending'||statusReason=='Claim Received'){
                foo = 'Accepted- Post'; 
                return foo;
                
            }else if(statusReason=='Patient at COE'){
                foo = 'Accepted- At COE';
                return foo;
                
            }else if(statusReason=='Appointment Date' || statusReason=='Diagnostics' || statusReason=='BMI'){
                foo = 'Patient Deferred';
                return foo;
                
            }else if(statusReason=='No Eligibility' || statusReason=='Patient Expired'){
                foo = 'Other';
                return foo;
            }else if(sublist=='Patient Expired'){
                return sublist;
            
            }else if(sublist=='No Eligibility'){
                foo = 'Employer Plan ineligible';
                return foo;
                
            }else if(statusReason=='Medical Records'       || statusReason=='Nicotine'             ||
                     statusReason=='Cancelled by Facility' || statusReason=='Cancelled by Patient' ||
                     statusReason=='Caregiver'             || statusReason=='Dental'               || 
                     statusReason=='Other'                 || statusReason=='Medical Condition'||
                     statusReason=='Infectious Disease' )//added 9-11-20 infectious disease
            
            {     
                foo = statusReason;
                return foo;
            
            }else if(statusReason=='Awaiting Plan of Care'){
                foo = 'Pre-Determination';
                return foo;
            }else if(statusReason=='No Home Physician'){
                foo = 'Home Physician';
                return foo;
                
            }    
       }else{
        
            //Before referral
            if(statusReason!=null && (statusReason==('Call One')          || statusReason.contains('Call Two') ||
                                      statusReason=='New'                 || statusReason=='Nurse'             ||
                                      statusReason=='New - Web Intake'    || statusReason.contains('In Process') ||
                                      statusReason.contains('Pending')))
            {
               foo = 'In Process';
               return foo;
            }else if(statusReason=='Call Three')
            {
               foo = 'Disengaged';
               return foo;
            }else if(subList == 'Diagnostics'                || statusReason == 'Dental' || 
                     subList == 'Last 12 mos provider visit' || subList == 'Recommended Procedure' ||
                     statusReason == 'Diagnostics')
            {
                foo='Clinical Protocols';
                return foo;
            }else if(statusReason=='Invalid Information')
            { 
                foo = statusReason;
                return foo;
            }else if(subList =='Not Eligible - Procedure' ||  subList=='Patient Expired' || subList=='Inquiry'      ||
                     subList =='Incomplete'               ||  subList=='Local Hospital'  || subList=='Disengaged'   ||
                     subList =='Nicotine'                 ||  subList=='Caregiver'       || subList=='Patient Declined' || subList =='Not - Eligible Procedure' || 
                     subList =='Workers Compensation'     ||  subList=='Subrogation')
            { 
                foo = subList;
                return foo;
            }else if(statusReason =='Not Eligible - Procedure' ||  statusReason=='Inquiry'              || statusReason =='Incomplete' ||  
                     statusReason=='Patient Expired'           ||  statusReason=='Local Hospital'       || statusReason=='Caregiver'   ||
                     statusReason=='Clinical Protocols'        ||  statusReason=='Invalid Information'  || statusReason=='Program Compliance')
            { 
                foo = statusReason;
                return foo;
            }else if(subList=='No Home Physician')
            {
                foo ='Home Provider';
                return foo;
            }else if(subList=='No Eligibility')
            {
                foo = 'Employer Plan ineligible';
                return foo;
            }else if(subList=='Patient Choice' || statusReason=='Patient Choice' || statusReason=='Patient Declined' || statusReason=='Cancelled by Patient')
            {
                foo = 'Patient Choice to Defer';
                return foo;
            }else if(subList=='Infectious Disease'){//added 9-11-20
                foo = 'Infectious Disease'; 
                return foo;                    
                            
             }else if(statusReason=='Nurse-Nicotine' || statusReason=='Nurse-BMI' || statusReason=='Nurse- Ready'){
                foo = 'In Process';
                return foo;
            
            }else if(statusReason=='Nurse-Missing Info' || statusReason=='Missing Info'){
                if(cgName==null||cgName=='')
                {   
                    try{
                        Provider__c[] plist = [select home_transition_care__c from Provider__c where Patient_case__c = :recordID and home_transition_care__c='Yes'];
                        if(plist.size()>0){
                            foo = 'Patient Securing Caregiver'; 
                        }else{
                            foo = 'Patient Securing Provider & Caregiver';
                        }
                    }catch(exception e){
                        foo = 'Patients Securing Provider & Caregiver'; 
                    }
                    
                }else{
                
                    try{
                        Provider__c[] plist = [select home_transition_care__c from Provider__c where Patient_case__c = :recordID and home_transition_care__c='Yes'];
                        if(plist.size()>0){
                            foo = 'In Process';
                        }else{
                            foo = 'Patient Securing Provider'; 
                        }    
                    }catch(exception e){
                        foo = 'Patient Securing Provider'; 
                    }    
                }
            }
    
        }       
        return foo;
    }
    
    public static string theStage(string status, string statusReason, string sublist, string externalStatus, boolean isConverted)
    {
        
        string foo='';
        if(externalStatus=='In process'                                  || externalStatus=='Invalid Information'                ||
           externalStatus=='Patients Securing Both Provider & Caregiver' || externalStatus=='Clinical Protocols'                 ||
           externalStatus=='Patient Securing Caregiver'                  || externalStatus=='Nicotine'                           ||
           externalStatus=='Patient Choice to Defer'                     || externalStatus=='TBD- Patient Deferred'              ||
           externalStatus=='Accepted- Dental'                            || externalStatus=='TBD- Medical Records'               ||
           externalStatus=='TBD- Nicotine'                               || externalStatus=='TBD- Patient Needs Home Physician'  ||
          
          (status=='Open' && isConverted == false)                       ||     
          (externalStatus=='TBD' && statusReason != 'Awaiting Plan of Care')||externalStatus=='Infectious Disease'||
           externalStatus=='Disengaged'||externalStatus=='Patient Declined')//9-11-20
           
        {
            foo = 'PRE';
            return foo;
        }
        
        if(externalStatus=='Accepted')
        {
            if(statusReason=='Awaiting Appointment Date Confirmations' || statusReason=='Awaiting Forms' ||
               statusReason=='Awaiting Travel Confirmation'            || statusReason=='Ready for Arrival')
            {
                foo = 'Pre-Travel';
                return foo;
            }
        }       

        if(statusReason=='Claim Pending' || statusReason=='Claim Received')
        {
            foo = 'Post';
            return foo;
        }

        if(statusReason=='Patient at COE')
        {
            foo = 'In Town';
            return foo;
        }

        if(isConverted==true)
        {   
        
            if(sublist=='Medical Complications/Conditions' || sublist=='Medical Records' ||
               sublist=='Recommended Procedure'            || sublist=='Home Provider'   ||
               sublist=='No Surgery Indicated'             || sublist=='BMI'             ||
               sublist=='No Eval Indicated'                || sublist=='Patient Expired' ||
               sublist=='Program Compliance'               || sublist=='Local Hospital'  ||
               sublist=='Disengaged'                       || sublist=='Patient Declined' ||
               sublist=='Home Provider'                    || sublist=='CareGiver')
            {
                foo = sublist;
                return foo;
            }
            
            if(statusReason=='Appointment Date'){
                foo = '';
            }
            
            if(statusReason=='Pended')
            {
                foo = 'PRE';
                return foo;
            }

            if(statusReason=='No Eligibility')
            {
                foo = 'Employer Plan ineligible';
                return foo;
            }           

            if(statusReason=='Claim Paid')
            {
                foo = 'Complete';
                return foo;
            }
            
        }
        
        return foo;       
    }
    
}