public with sharing class UHHS_DenialNetworkVariance {
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c uml, Utilization_Management_Clinician__c clinician, string logoPath){
        StaticResource sr = [SELECT Id, NamespacePrefix, SystemModstamp FROM StaticResource WHERE Name = 'medicalDirectorSignature' LIMIT 1];

        String prefix = sr.NamespacePrefix;

        if (String.isEmpty(prefix)) {
            prefix = '';
        }
        else {
              //If has NamespacePrefix
            prefix += '__';
        }
        String srPath = '/resource/' + sr.SystemModstamp.getTime() + '/' + prefix + 'medicalDirectorSignature';

        string letterText = '';

        letterText += '<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText += um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c + '<br/>';
        letterText += um.patient__r.Address__c + '<br/>';
        letterText += um.patient__r.City__c + '&nbsp';
        letterText += um.patient__r.State__c + '&nbsp';
        letterText += um.patient__r.Zip__c + '<br/><br/>';
        letterText += '</div>';
        letterText += '<p>';
        letterText += 'Contigo Health, LLC is a third-party administrator that performs Care Management services for' + '</br>';
        letterText += 'the' + ' ' + um.patient__r.Patient_Employer__r.name + ' ' + 'Health System Group Benefit Plan which is a self-funded group health' + '</br>';
        letterText += 'plan (Plan), regulated by ERISA.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'Contigo Health has denied the Network Variance Request referenced below:';
        letterText += '</p>';

        letterText += '<div style="margin-top:10px">';
        letterText += '<div style="display:inline-block;width:25%">';
        letterText += 'Case Number:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px;">';
        letterText += '&nbsp;' + um.HealthPac_Case_Number__c;
        letterText += '</div>';
        letterText += '</div>';

        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:25%">';
        letterText += 'Patient Name:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px;">';
        letterText += '&nbsp;' + um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c;
        letterText += '</div>';
        letterText += '</div>';

        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:25%">';
        letterText += 'Patient Date of Birth:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px;">';
        letterText += '&nbsp;' + um.patient__r.Patient_Date_of_Birth__c;
        letterText += '</div>';
        letterText += '</div>';

        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:25%">';
        letterText += 'Provider Name:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;' + Clinician.First_Name__c + ' ' + Clinician.Last_Name__c + ', ' + Clinician.Credentials__c + '/' + um.Facility_Name__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:25%">';
        letterText += 'Date of Service:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;Beginning ' + um.Admission_Date__c.month() + '/' + um.Admission_Date__c.day() + '/' + um.Admission_Date__c.year();
        letterText += '</div>';
        letterText += '</div>';

        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:25%;">';
        letterText += 'Type of Service Request:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;' + uml.Type_of_Request__c;
        letterText += '</div>';
        letterText += '</div>';

        letterText += '<div style="margin-top:5px">';
        letterText += '<div style="display:inline-block;width:25%;">';
        letterText += 'Service Description:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;margin-left:8px">';
        letterText += '&nbsp;' + uml.Type_of_Request__c;
        letterText += '</div>';
        letterText += '</div>';

        letterText += '<br/>';

        letterText += 'Denial Reason: The request for&nbsp;_____________________________________, is denied because' + '</br>';
        letterText += 'service(s) is available from an In-Network Providers.' + '</br></br>';
        letterText += 'Rationale Used to Make Determination: University Hospital’s Summary Plan Document and Plan' + '</br>';
        letterText += 'Document, Network Variance Requests and Determinations, Network Provider Directory.' + '</br>';

        letterText += '<br/><br/>';
        letterText += '<div style="font-weight:bold">';
        letterText += 'Network Variance Requests and Determinations (please note, capitalized terms are defined in the Plan documents):';
          //letterText+='in the Plan documents): ';
        letterText += '</div>';
        letterText += 'To receive benefits under the Plan, you must use In-Network Providers as set forth herein. The Plan has contracted with the Network(s) to provide care to Covered Persons at discounted fees. Hospitals, Physicians and other Providers and facilities who have contracted with the Network are called "Network Providers." Those who have not contracted with the Network are referred to in this Plan as "Out-of-Network Providers."  This arrangement results in the following benefits to Covered Persons:';
        letterText += '<br/><br/>';
          //letterText+='<ol style="list-style-type:decimal">';
          //letterText+='<li>';
          //letterText+='The  Plan  provides  different  levels  of  benefits  based  on  whether  the  Covered  Persons  use ';
        letterText += '1. The  Plan  provides  different  levels  of  benefits  based  on  whether  the  Covered  Persons  use ';
        letterText += 'a Network or Out-of-Network Providers. Unless the exception shown below applies, if a Covered Person elects to receive medical care from the Out-of-Network Provider,</span>';
        letterText += ' services will not be covered by the Plan. The following exceptions apply:';
        //letterText+='</li>';

        letterText += '<ul style="list-style-type:none;">';
        letterText += '<li >';
        letterText += 'a. The Network Provider level of benefits is payable at the allowable amount when a Covered Person receives Emergency Care either as an outpatient or if admitted at an Out-of-Network facility or from an out-of-network provider, covered expenses will be ';
        letterText += 'applied to your in-network deductible, coinsurance and out-of-pocket maximum  up to the allowed amount Plus you will be responsible for paying any amounts above the allowed amount that the provider bills you. You also may need to file claim forms' + '</br></br>';
        letterText += '</li>';
        letterText += '<li>';
        letterText += 'b. The Network Provider level of benefits is payable at usual and customary charges when a Covered Person utilizes a Network Facility of Physician office, but that Network Facility of Physician office utilizes an Out-of-Network ancillary provider.' + '</br></br>';
        letterText += '</li>';
        letterText += '<li>';
        letterText += 'c. The member obtains a network variance because the service is not available from a network provider as described herein.';
        letterText += '</li>';
        letterText += '</ul>';

          //letterText+='<li>';
        letterText += '2. If the charge billed by an Out-of-Network Provider for any covered service is higher than the Reasonable and Customary Charges determined by the Plan, Covered Persons are responsible for the excess (Balance Billing). Since Network Providers have agreed to accept the negotiated discounted fee as full payment for their services, Covered Persons are not responsible for any billed amount that exceeds that fee when utilizing a network provider.';
          //letterText+='</li>';
        letterText += '<br/><br/>';
          //letterText+='<li>';
        letterText += '3. To receive benefit consideration, Covered Persons must submit claims for services provided by Out-of-Network Providers to the Claims Administrator. Network Providers have agreed to bill the Plan directly, so that Covered Persons do not have to submit claims.';
        //letterText+='</li>';
        //letterText+='</ol>';

        letterText += '<p>';
        letterText += 'Although additional information is not required to submit an appeal, all information provided will be considered when rendering an Appeal Decision.';
          //letterText+='will be considered when rendering an Appeal Decision.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'Additional Information Consideration:<br/>';
        letterText += '&nbsp;&nbsp;&nbsp;&nbsp;•&nbsp;&nbsp;&nbsp;&nbsp;Documentation representing material difference from information originally submitted.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'All choices regarding the care and treatment of the patient remain the responsibility of the attending physician. In no way does this letter attempt to dictate the care the patient ultimately' + '</br>';
        letterText += 'receives.';
          //letterText+='receives.';
        letterText += '</p>';
        /*
         * letterText+='<p>';
         * letterText+='Sincerely,<br/>';
         * letterText+='<div><img src="'+srPath+'" style="width:140px;"/></div>';
         * letterText+='<div>';
         * letterText+='Eric M. Yasinow, M.D.';
         * letterText+='</div>';
         * letterText+='<div>';
         * letterText+='Medical Director';
         * letterText+='</div>';
         * letterText+='</p>';
         */

        letterText += '<br/><br/>';
        letterText += 'Sincerely,<br/><br/>';
        letterText += '<div ></div>';
        letterText += '<div style="display:none" >signhere</div>';
        letterText += 'Eric M. Yasinow, M.D.<br/>';
        letterText += 'Medical Director<br/>';
        letterText += '<br/><br/>';

        boolean hasFacility = (um.Facility_Name__c != null && um.Facility_Street__c != null && um.Facility_City__c != null && um.Facility_State__c != null && um.Facility_Zip_Code__c != null);

        letterText += '<div style="display:inline-block;width:40px">';
        letterText += 'CC:';
        letterText += '</div>';
        letterText += hasFacility ? um.Facility_Name__c : '';

        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;">';
        letterText += hasFacility ? um.Facility_Street__c : '';
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;">';
        letterText += hasFacility ? um.Facility_City__c + ', ' : '';
        letterText += hasFacility ? um.Facility_State__c + ' ' : '';
        letterText += hasFacility ? um.Facility_Zip_Code__c : '';
        letterText += '</div>';
        letterText += '</div>';
        letterText += '</div>';
        letterText += '</div>';
        letterText += '&nbsp;';
        letterText += '&nbsp;';
        letterText += '&nbsp;';
        letterText += '&nbsp;';

        boolean hasClinician = (Clinician.Street__c != null && Clinician.City__c != null && Clinician.State__c != null && Clinician.Zip_Code__c != null);

        letterText += '<div>';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += 'CC:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;">';
        letterText += Clinician.First_Name__c + ' ' + Clinician.Last_Name__c + ', ' + Clinician.Credentials__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;"/>';
        letterText += hasClinician ? Clinician.Street__c : '';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;">';
        letterText += hasClinician ? Clinician.City__c + ', ' : ', ';
        letterText += hasClinician ? Clinician.State__c + ' ' : ' ';
        letterText += hasClinician ? Clinician.Zip_Code__c : '';
        letterText += '</div>';

        letterText += '<div style="margin-top:10px">';

        letterText += '<div style="display:inline-block;">';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '</div>';

        letterText += '<p>';
        letterText += 'This decision has been processed consistent with benefit terms and conditions described in the<br/>';
        letterText += 'Summary Plan Description and Plan Document. Contacting Customer Service at the telephone<br/>';
        letterText += 'number listed on your medical ID card may resolve your questions. You have a right to request,<br/>';
        letterText += 'free of charge, a copy of any internal rule, guideline, protocol, or similar criteria used in this<br/>';
        letterText += 'determination, an explanation of the scientific or clinical basis of the determination if the denial <br/>';
        letterText += 'involves a medical necessity or experimental treatment limitation or exclusion, the diagnosis and <br/>';
        letterText += 'treatment codes related to this claim and their meanings, and any documents, records or other <br/>';
        letterText += 'information relevant to this claim.<br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'You or your authorized representative have the right to appeal this decision. Your appeal must be<br/>';
        letterText += 'sent in writing, along with any additional information, within 180 days of receipt of the denial to <br/>';
        letterText += 'Contigo Health, LLC., Attention: Appeals Coordinator, 1755 Georgetown Road, Hudson Ohio <br/>';
        letterText += '44236.<br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'You may have the right to bring civil action under ERISA 502(a) following the Plan\'s final internal<br/>';
        letterText += 'appeal process. If your attending provider believes your situation is urgent, you may request an <br/>';
        letterText += 'expedited appeal by contacting Customer Service.  If your claim involves medical judgment, and <br/>';
        letterText += 'you have exhausted all of the Plan\'s internal appeal processes (except where your claim is urgent), <br/>';
        letterText += 'you may be able to request an external review of your claim by an independent third party who <br/>';
        letterText += 'will review the denial and issue a final decision.  Please refer to your Summary Plan Description and<br/>';
        letterText += 'Plan Document for details on the Plan\'s claims and appeals procedures.<br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'For questions about your appeal rights, this notice, or for assistance, you can contact Customer <br/>';
        letterText += 'Service at the telephone number listed on your medical ID card.  You also may contact the <br/>';
        letterText += 'Employee Benefits Security Administration at 1-866-444-EBSA(3272).';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'Your employer’s health plan complies with applicable Federal civil rights laws and does not<br/>';
        letterText += 'discriminate on the basis of race, color, national origin, age, disability or sex.<br/>';
        letterText += '</p>';

        letterText += '(SPANISH) ATENCIÓN: si habla español, tiene a su disposición servicios gratuitos de asistencia <br/>';
        letterText += 'lingüística.  Llame al 1-330-656-1072. (TTY: 711)<br/><br/>';
        letterText += '(TAGALOG) PAUNAWA: Kung nagsasalita ka ng Tagalog, maaari kang gumamit ng mga <br/>';
        letterText += 'serbisyo ng tulong sa wika nang walang bayad.  Tumawag sa 1-330-656-1072. (TTY: 711)<br/><br/>';
        letterText += '(CHINESE) <span style="font-family: Arial Unicode MS">注意：如果您使用繁體中文，您可以免費獲得語言援助服務。請致電  </span> <br/>';
        letterText += '1-330-656-1072. (TTY: 711)<br/><br/>';
        letterText += '(NAVAJO) Díí baa akó nínízin: Díí saad bee yáníłti’go Diné Bizaad, saad bee áká’ánída’áwo’dę́ę́’, <br/>';
        letterText += 't’áá jiik’eh, éí ná hólq̨́, kojį́ hǫ́dį́į́lnih 1-330-656-1072. (TTY: 711)<br/>';
        letterText += '<br/><br/>';
        return letterText;
    }
}