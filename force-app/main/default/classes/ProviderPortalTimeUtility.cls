/**
 * @description       : 
 * @author            : Michael Martin
 * @group             : 
 * @last modified on  : 02-04-2021
 * @last modified by  : Michael Martin
 * Modifications Log 
 * Ver   Date         Author           Modification
 * 1.0   02-04-2021   Michael Martin   Initial Version
**/
public with sharing class ProviderPortalTimeUtility{

    public string hour {get; set;}
    public string minute {get; set;}
    public string AMPM {get; set;}

    public static selectoption[] getMinuteOpts(){
        selectoption[] minuteOpts = new selectOption[]{};
        minuteOpts.add(new selectOption('',''));
        for(integer i=0;i<60;i++){
            string si='';
            if(i<10){
                si='0';
            }
            si += string.valueOf(i);
            minuteOpts.add(new selectOption(si,si));
        }
        return minuteOpts;
    }

    public static selectoption[] getAMPMOpts(){
        selectoption[] ampmOpts = new selectOption[]{};
        ampmOpts.add(new selectOption('',''));
        ampmOpts.add(new selectOption('AM','AM'));
        ampmOpts.add(new selectOption('PM','PM'));
        return ampmOpts;
    }

    public static selectoption[] getHourOpts(){
        selectoption[] hourOpts = new selectOption[]{};
        hourOpts.add(new selectOption('',''));
        hourOpts.add(new selectOption('01','01')); 
        hourOpts.add(new selectOption('02','02'));
        hourOpts.add(new selectOption('03','03'));
        hourOpts.add(new selectOption('04','04'));
        hourOpts.add(new selectOption('05','05'));
        hourOpts.add(new selectOption('06','06'));
        hourOpts.add(new selectOption('07','07'));
        hourOpts.add(new selectOption('08','08'));
        hourOpts.add(new selectOption('09','09'));
        hourOpts.add(new selectOption('10','10'));
        hourOpts.add(new selectOption('11','11'));
        hourOpts.add(new selectOption('12','12'));


        return hourOpts;
    }
}