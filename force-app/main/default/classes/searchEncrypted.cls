public virtual class searchEncrypted{

   public string searchTerm {get; private set;}
   
   public void setSearchTerm(string searchTerm){
       
       this.searchTerm = searchTerm;
   
   }
   
   public sObject[] search(string dbSearch, string[] searchFields){
   
       sObject[] soList;
       
       if(searchTerm==null || searchTerm==''){return null;}
       string st = searchTerm;
       st = searchTerm.toLowerCase().trim();
       
       string[] stFinal;
       stFinal = st.split(' ');
       
       set<sObject> soSet = new set<sObject>();
       
       string result='';
       Object fieldValue;
       
       for(sObject so : database.query(dbSearch)){
       
           fieldValue = '';
           result='';
           
           for(string sf: searchFields){
               
               try{
                   
                   try{
                       fieldValue = so.get(sf);
                   
                   }catch(sobjectexception e){
                       if(sf.indexof('.')>0){
                           string obj = sf.left(sf.indexof('.'));
                           string field = sf.right(sf.length() - sf.indexof('.')-1);
                           fieldValue = so.getsObject(obj).get(field );
                       }
                   }
                   
               }catch(exception e){
                   
                   system.debug(e.getMessage());
                   system.debug(e.getLinenumber());   
               
               }
               
               result = fieldValue + ';' + result;
           
           }
           
           result = result.toLowerCase();
           
           for(string s: stFinal){
               if(!soSet.contains(so) && result.indexOf(s)>-1){
                   soSet.add(so);
               }
           }
       
       }
       
       if(soSet.size()>0){
           try{
               soList = new list<sObject>(soSet);
           }catch(exception e){
               system.debug(e.getMessage());
               system.debug(e.getLinenumber()); 
                   
           }
       }
       if(soList==null){soList= new sobject[]{};}
       
       return soList;
   }
   
   public string setSearchString(string[] searchFields, string obj){
        
        string searchString ='';
        
        for(String s : searchFields){
            searchString = searchString+','+s;
        }
        
        return 'Select ' + searchString.right(searchString.length()-1) + ' from ' + obj.trim();
        
    }

}