@isTest(seealldata=true)
private class caseAttachmentsTest{
    static testMethod void myUnitTest() {
        
        client_facility__c cf = [select client__c, procedure__c from client_facility__c where Client__r.name = 'Walmart' and Procedure__r.name = 'Joint' limit 1];
        patient_case__c pc = new patient_case__c();
        pc.Employee_First_Name__c  ='John';
        pc.Employee_Last_Name__c  ='Smith'; 
        pc.bid__c  ='123457';
        pc.Language_Spoken__c  ='English';
        pc.Initial_Call_Discussion__c = 'Test';
        pc.status__c= 'Open';
        pc.status_reason__c= 'In Process';
        pc.attachEncrypt__c = EncodingUtil.base64encode(Crypto.generateAesKey(128));
        pc.client__c = cf.client__c;
        pc.ecen_procedure__c = cf.procedure__c ;
        insert pc;
         
        ApexPages.currentPage().getParameters().put('id',pc.id);
        //ApexPages.currentPage().getParameters().put('procedure',cf.procedure__c);
        caseController cc = new caseController();
        //cc.obj.Employee_First_Name__c  ='John';
        //cc.obj.Employee_Last_Name__c  ='Smith';
        //cc.obj.bid__c  ='123457';
        //cc.obj.Language_Spoken__c  ='English';
        //cc.obj.Initial_Call_Discussion__c = 'Test';
        
      //  cc.checkdup();
        
        //cc.obj.status__c= 'Open';
        //cc.obj.status_reason__c= 'In Process';
      //  cc.obj
                
        cc.inlineSave();
        
        attachment attach = new attachment();
        attach.parentid = cc.obj.id;
        attach.name = 'test';
        attach.body = blob.valueof('54a5s6454dadasd456asdasd');
        
        insert attach;
        caseAttachments ca = new caseAttachments(cc.obj.id);
        
        system.assert(ca.getAttachments().size()>0);
        blob key = EncodingUtil.base64Decode(cc.obj.attachEncrypt__c);
        ca.encryptAttach(attach.id, key);
        ca.decryptAttach(attach.id, key);
        ca.DeleteAttach(attach.id);
        
        
    }
}