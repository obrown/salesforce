public with sharing class searchWakeMedOE{

    public searchWakeMedOE(ApexPages.StandardController controller) {
        
        st = ApexPages.CurrentPage().getParameters().get('st');
        searchByName(st);
    }

    
    public string st {get; set;}
    public WakeMed_OE_Intake__c[]  results {get; private set;}
    
    void searchByName(string st){
        
        set<string> searchSet;
        
        if(st.indexOf(' ')>0){
            string[] stList = st.split(' ');
            
            for(string s : stList){
                s += '%';
            }        
            
            searchSet = new set<string>(stList);
        }else{
            searchSet = new set<string>();
            searchSet.add(st +'%');
        }
        
        try{
            results = [select CreatedDate, CreatedBy.id,CreatedBy.name, Name, Employee_First_Name__c, Employee_Last_Name__c, Caller_Name__c from WakeMed_OE_Intake__c where Employee_First_Name__c like :searchSet or Employee_Last_Name__c like :searchSet or Caller_Name__c like :searchSet];        
        }catch(dmlException e){
            results = new WakeMed_OE_Intake__c[]{};
        }
    }
    
}