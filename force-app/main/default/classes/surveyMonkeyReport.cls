public with sharing class surveyMonkeyReport{

    public void run(boolean isTest, date testDay){
    
        integer day = date.today().day();
        date tDate = date.today();
        
        if(isTest){
            day= testDay.day();
            tDate = testDay; 
        }
        
        if(day != 1 && day != 15){
            return;
        }
        
        /* Continue on 1st and 15th*/
        
        date startDate, endDate, oncologyStartDate , oncologyEndDate;
        
        integer mm= 0, yy = 0;

        if(tDate.month()==1){
            mm = 12;    
            yy = tDate.year()-1;
        }else{
            mm = tDate.month()-1;
            yy= tDate.year();
        }
        
        oncologyStartDate =tDate.addDays(-60);
            
        if(day==1){
            startDate = date.valueOf(yy + '-' + mm + '-' + '01');
            endDate = date.valueOf(yy + '-' + mm + '-' + '14');
            
            oncologyStartDate = date.newInstance(oncologyStartDate.year(), oncologyStartDate.month(), 1);
            oncologyEndDate = date.newInstance(oncologyStartDate.year(), oncologyStartDate.month(), 14);
        
        }else{
            startDate = date.valueOf(yy + '-' + mm + '-' + '15');
            endDate = date.valueOf(yy + '-' + mm + '-' + date.daysInMonth(yy, mm));
            oncologyStartDate = date.newInstance(oncologyStartDate.year(), oncologyStartDate.month(), 15);
            oncologyEndDate = date.newInstance(oncologyStartDate.year(), oncologyStartDate.month(), date.daysInMonth(oncologyStartDate.year(), oncologyStartDate.month())); 
            
    
        }
        
        string body='Email,First Name, Last Name, Client, Program, Referred Facility,Case\n';

        for(patient_case__c p : [select name, 
                                        Patient_Email_Address__c,
                                        patient_first_name__c, 
                                        patient_last_name__c, 
                                        case_type__c, 
                                        client__r.name,
                                        ecen_procedure__r.name,
                                        client_facility__r.facility__r.name,
                                        program_type__c,
                                        Inpatient_Program_Surgery__c,
                                        Inpatient_No_Program_Surgery__c,
                                        Initiated_Conservative_Treatment__c,
                                        Outpatient_evaluation_only__c,
                                        Outpatient_Extended_Evaluation__c,
                                        Outpatient_Program_Surgery__c,
                                        visit_type__c from patient_case__c where actual_departure__c >= :startDate and actual_departure__c <= :endDate]){
            
            if(p.program_type__c == 'Cardiac' && 
               p.Initiated_Conservative_Treatment__c == null &&
               p.Outpatient_evaluation_only__c == null &&
               p.Outpatient_Extended_Evaluation__c == null &&
               p.Outpatient_Program_Surgery__c == null &&
               p.Inpatient_Program_Surgery__c== null &&
               p.Inpatient_No_Program_Surgery__c == null){
              
               body += p.Patient_Email_Address__c +',' + p.patient_first_name__c +','+p.patient_last_name__c +','+p.client__r.name+','+p.ecen_procedure__r.name+','+p.client_facility__r.facility__r.name+','+p.name +'\n';
               
            }else if(p.program_type__c == 'Joint'){
               body += p.Patient_Email_Address__c +',' + p.patient_first_name__c +','+p.patient_last_name__c +','+p.client__r.name+','+p.ecen_procedure__r.name+','+p.client_facility__r.facility__r.name+','+ p.name +'\n';
               
            }else if(p.program_type__c == 'Spine' && (p.visit_type__c=='Inpatient'|| p.visit_type__c=='Outpatient')){
               body += p.Patient_Email_Address__c +',' + p.patient_first_name__c +','+p.patient_last_name__c +','+p.client__r.name+','+p.ecen_procedure__r.name+','+p.client_facility__r.facility__r.name+','+p.name +'\n';
               
            }
               
            
        }
        
        string oBody='Email,First Name, Last Name, Client, Program, Referred Facility,Case\n';
        string fbOBody='Email,First Name, Last Name, Client, Program, Referred Facility,Case\n';
        
        for(oncology__c p : [select name, 
                                    Patient_Email_Address__c,
                                    patient_first_name__c, 
                                    patient_last_name__c, 
                                    client__r.name,
                                    procedure__r.name,
                                    client_facility__r.client__r.name,
                                    client_facility__r.facility__r.name from oncology__c where Referral_Date__c!=null and Plan_of_Care_Accepted_Date__c != null and Determination_Outcome__c ='Program Eligibility Met' and (last_appointment_date__c>= :oncologyStartDate and last_appointment_date__c<= :oncologyEndDate)]){
                
                
               if(p.client_facility__r.client__r.name=='Facebook'){
                   fbOBody+= p.Patient_Email_Address__c +',' + p.patient_first_name__c +','+p.patient_last_name__c +','+p.client__r.name+','+p.procedure__r.name+','+p.client_facility__r.facility__r.name+','+p.name+'\n';
               }else{
                   oBody += p.Patient_Email_Address__c +',' + p.patient_first_name__c +','+p.patient_last_name__c +','+p.client__r.name+','+p.procedure__r.name+','+p.client_facility__r.facility__r.name+','+p.name+'\n';
               }
        }
        
        string s = startDate.month() + '/' + startDate.Day() + '/' + startDate.year();
        string e = endDate.month() + '/' + endDate.Day() + '/' + endDate.year();
        
        string os = oncologyStartDate.month() + '/' + oncologyStartDate.Day() + '/' + oncologyStartDate.year();
        string oe = oncologyEndDate.month() + '/' + oncologyEndDate.Day() + '/' + oncologyEndDate.year();
        
        
        string emailAddresses;
        if(utilities.isRunninginSandbox() || isTest){
            emailAddresses=userInfo.getUserEmail();
        }else{
            emailAddresses= 'mbina@hdplus.com,hkeyes@hdplus.com';
        }
        
        Messaging.SingleEmailMessage[] emails = new Messaging.SingleEmailMessage[]{};
        
        Messaging.SingleEmailMessage mail = utilities.email(emailAddresses,null,null,'System','Survey Money Report for ' + s + ' - ' + e,'Survey Money Report for ' + s + ' - ' + e+'.\n\nThanks!');
        Blob csvBlob = Blob.valueOf(body);
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName('Patient Satisfaction Survey Patients - Joint and Spine ' + s.replaceAll('/','.') + ' - ' + e.replaceAll('/','.') + '.csv');
        efa.setBody(csvBlob);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        mail.setReplyTo('noreply@hdplus.com');
        emails.add(mail);
        
        mail = utilities.email(emailAddresses,null,null,'System','Survey Money Report for ' + os + ' - ' + oe,'Survey Money Report for ' + os + ' - ' + oe+'.\n\nThanks!');
        csvBlob = Blob.valueOf(fbOBody);
        efa = new Messaging.EmailFileAttachment();
        efa.setFileName('Patient Satisfaction Survey Patients - Facebook Cancer Care Program ' + os.replaceAll('/','.') + ' - ' + oe.replaceAll('/','.') + '.csv');
        efa.setBody(csvBlob);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        mail.setReplyTo('noreply@hdplus.com');
        emails.add(mail);
        
        mail = utilities.email(emailAddresses,null,null,'System','Survey Money Report for ' + os + ' - ' + oe,'Survey Money Report for ' + os + ' - ' + oe+'.\n\nThanks!');
        csvBlob = Blob.valueOf(oBody);
        efa = new Messaging.EmailFileAttachment();
        efa.setFileName('Patient Satisfaction Survey Patients - Oncology Cancer Care Program ' + os.replaceAll('/','.') + ' - ' + oe.replaceAll('/','.') + '.csv');
        efa.setBody(csvBlob);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        mail.setReplyTo('noreply@hdplus.com');
        emails.add(mail);
        
        Messaging.sendEmail(emails); 
        
    }

}