public without sharing class hpImpagePacStruct{
    
    public string Filename;
    public string ReceivedDate;
    public string ScanDate;
    public string ServerDesc;
    public string ImageType;
    public string UserDesc;
    public string EntityType;
    public string HpKeyPartA;
    public string HpKeyPartB;
    public string HpKeyPartC;
}