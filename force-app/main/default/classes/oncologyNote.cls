public with sharing class oncologyNote{
    
    id parentId;
    
    public Oncology_Care_Coordination_Note__c note {get; set;}
    
    public Oncology_Care_Coordination_Note__c[] inotes {get; set;}
    public Oncology_Care_Coordination_Note__c[] rnotes {get; set;}
    public Oncology_Care_Coordination_Note__c[] pdnotes {get; set;}
    public Oncology_Care_Coordination_Note__c[] poconotes {get; set;}           
    public Oncology_Care_Coordination_Note__c[] poccnotes {get; set;}
    public Oncology_Care_Coordination_Note__c[] mrnotes {get; set;}  
    public Oncology_Care_Coordination_Note__c[] oyssnotes {get; set;}
    public Oncology_Care_Coordination_Note__c[] oysenotes {get; set;}
    public Oncology_Care_Coordination_Note__c[] cnotes {get; set;}
    
    public oncologyNote(id parentId){
        setparentId(parentId);
        loadNotes();
    }
    
    void setparentId(id parentId){
        this.parentId=parentId;
    }
    
    public void loadNotes(){
        
        inotes = new Oncology_Care_Coordination_Note__c[]{}; 
        rnotes= new Oncology_Care_Coordination_Note__c[]{};
        pdnotes= new Oncology_Care_Coordination_Note__c[]{};
        poconotes= new Oncology_Care_Coordination_Note__c[]{};
        poccnotes= new Oncology_Care_Coordination_Note__c[]{};
        mrnotes= new Oncology_Care_Coordination_Note__c[]{};
        oyssnotes= new Oncology_Care_Coordination_Note__c[]{};
        oysenotes= new Oncology_Care_Coordination_Note__c[]{};
        cnotes= new Oncology_Care_Coordination_Note__c[]{};
        
        for(Oncology_Care_Coordination_Note__c ont : [select  Communication_Type__c,Name,Contact_Name__c,Contact_Type__c,Initiated__c, createdby.name, createdDate,Notes__c,Activity_Type__c,Subject__c from Oncology_Care_Coordination_Note__c where parentID__c = :parentId]){
        
            if(ont.Activity_Type__c =='Intake Status Note'){
                inotes.add(ont);
                continue;
            }
            
            if(ont.Activity_Type__c =='Referral Status Note'){
                rnotes.add(ont);
                continue;
            }
            
            
            if(ont.Activity_Type__c =='Program Determination Activity Note'){
                pdnotes.add(ont);
                continue;
            }
            
            if(ont.Activity_Type__c =='Plan of Care Status Notes - Open'){
                poconotes.add(ont);
                continue;
            }
            
            if(ont.Activity_Type__c =='Plan of Care Status Notes - Closed'){
                poccnotes.add(ont);
                continue;
            }
            
            
            if(ont.Activity_Type__c =='Medical Record Collection Status Note'){
                mrnotes.add(ont);
                continue;
            }

            if(ont.Activity_Type__c =='One Year Support Status Notes'){
                oyssnotes.add(ont);
                continue;
            } 

            if(ont.Activity_Type__c =='One Year Support End'){
                oysenotes.add(ont);
                continue;
            }            

            if(ont.Activity_Type__c =='Clinical Facility Note'){
                cnotes.add(ont);
                continue;
            }                   
        
        }

    }
    
    
    public void newNote(){
        note= new Oncology_Care_Coordination_Note__c(parentID__c=parentId);
        
    }
    
    public void savenote(){
        try{
           
           upsert note;
           loadNotes();
           
        }catch(dmlException e){
           for (Integer i = 0; i < e.getNumDml(); i++){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)+' '+e.getLineNumber()); 
           } 
        }
    }
  
    public void cancelinote(){
        note = null;
        
    }    
    
    public void openNote(){
        string notesId= ApexPages.CurrentPage().getParameters().get('notesId');
        string notesType= ApexPages.CurrentPage().getParameters().get('notesType');
        system.debug(notesId);
        
        if(notesType=='rNote'){
            findNote(notesId,rNotes);
            return;
        }
        
        if(notesType=='iNote'){
            findNote(notesId,iNotes);
            return;
        }

        if(notesType=='mrNote'){
            findNote(notesId,mrNotes);
            return;
        }        

        if(notesType=='pdNote'){
            findNote(notesId,pdNotes);
            return;
        }        

        if(notesType=='poccNote'){
            findNote(notesId,poccNotes);
            return;
        }        

        if(notesType=='pocoNote'){
            findNote(notesId,pocoNotes);
            return;
        } 

        if(notesType=='oyssNote'){
            findNote(notesId,oyssnotes);
            return;
        }        

        if(notesType=='oysenote'){
            findNote(notesId,oysenotes);
            return;
        }               

        if(notesType=='cnote'){
            findNote(notesId,cnotes);
            return;
        }        
    }    
    
    boolean findNote(id theid, Oncology_Care_Coordination_Note__c[] notes){
        for(Oncology_Care_Coordination_Note__c  a : notes){
                if(a.id==theid){
                    note=a;
                    return true;
                    
                }
            }   
        return false;     
    }
    
}