/**
 * @description       :
 * @author            : Michael Martin
 * @group             :
 * @last modified on  : 02-10-2021
 * @last modified by  : Michael Martin
 * Modifications Log
 * Ver   Date         Author           Modification
 * 1.0   02-10-2021   Michael Martin   Initial Version
 **/
public class patientSearchVFController {
  final string sId = userinfo.getSessionid();
  final string userId = userinfo.getuserid();
  public string getsId() {
    return sid;
  }
  public string getuserId() {
    return userId;
  }

  public string file_description { get; set; }

  public string selectedFileName { get; set; }
  string originalFileName;

  public SelectOption[] client { get; set; }
  map<string, Client__c> clientmap = new Map<string, Client__c>();
  public hpPatientSearchStruct.PatSearchResultLogs[] results { get; set; }
  hpPatientSearchStruct.PatSearchResultLogs[] preFilterResults;
  public hpPatientSearchStruct.PatSearchResultLogs[] ediResults { get; set; }
  public hpEligDetailStruct.EligibilityInformation hpDetail { get; set; }

  public string clientLogo { get; private set; }
  public string[] servicesAvailable { get; private set; }
  public string patientRecordId { get; private set; }
  public boolean activeFlag { get; set; }

  public boolean openPatientFileInline { get; set; }

  map<string, hpPatientSearchStruct.PatSearchResultLogs> enrolledMap;
  map<string, hpPatientSearchStruct.PatSearchResultLogs> employeeMap = new Map<string, hpPatientSearchStruct.PatSearchResultLogs>();

  public map<string, hpPatientSearchStruct.PatSearchResultLogs> resultsMap {
    get;
    set;
  }

  public boolean hpSearchfound { get; private set; }
  public boolean hpSearchRun { get; private set; }
  public boolean hpEligSelected { get; private set; }
  public boolean hpEligFound { get; private set; }

  public boolean error { get; private set; }

  public string eSSN { get; set; }
  public string efn { get; set; }
  public string eln { get; set; }
  public string cert { get; set; }
  public string uw { get; set; }
  public string compPatientId { get; set; }
  public boolean searchHP { get; set; }
  public boolean searchEDI { get; set; }

  public boolean hasError { get; set; }
  public string umid { get; private set; }
  public string cmid { get; private set; }
  public string gender{ get; set; }

//added 12-02-21 ------------------------------------------start----------------------------
  id profID; 
  SystemID__c sid2 = SystemID__c.getInstance();
  boolean ohyClient;
  //added 12-02-21 ------------------------------------------end------------------------------

  public patientSearchVFController() {
    umid = apexpages.currentpage().getparameters().get('umid');
    cmid = apexpages.currentpage().getparameters().get('cmid');
    loadClientList();
    searchHP = true;
    searchEDI = true;
  }

  string fileSrcBaseDir;
  final string baseDir = fileServer__c.getInstance()
    .populationHealthDestinationDirectory__c; //fileServer__c.getInstance().baseDirectory__c +''+ 'population_health/utilization_management/'; //z:/FaxCM/FaxInboundCM/

  void loadClientList() {
    if (client == null) {
      client = new List<SelectOption>{};
      client.add(new SelectOption('', 'All'));
  
      profID = userInfo.getProfileID(); //12-02-21 added 
      for (Client__c c : [
        SELECT
          underwriter__c,
          active__c,
          logoDirectory__c,
          name,
          Care_Management__c,
          Disease_Management__c,
          Maternity_Management__c,
          Utilization_Management__c,
    OHY_Client__c//12-02-21 added new field to pull only OHY clients************************
        FROM Client__c
        WHERE underwriter__c != ''
        ORDER BY name ASC
      ]) {
    /*12-02-21 new logic added to set list based on logged in user SFprofile	
        if (c.active__c) {
          client.add(new SelectOption(c.underwriter__c, c.name));
          clientmap.put(c.underwriter__c, c);
        }
    */
      if (profid != sid2.Ohio_Healthy__c) {
        if (c.active__c) {
        client.add(new SelectOption(c.underwriter__c, c.name));
        clientmap.put(c.underwriter__c, c);
        }
      }else{
        ohyClient=true;
        if (c.active__c&&c.OHY_Client__c==true) {
           
        client.add(new SelectOption(c.underwriter__c, c.name));
        clientmap.put(c.underwriter__c, c);
        }        
      }		  
    
      }
    }
  }

  public void clear() {
    eSSN = null;
    efn = null;
    eln = null;
    cert = null;
    uw = null;
    hasError = false;
    hpsearchRun = false;
    results = new List<hpPatientSearchStruct.PatSearchResultLogs>{};
    resultsMap = new Map<string, hpPatientSearchStruct.PatSearchResultLogs>{};
  }

  public void searchHealthpac() {
    hpSearchRun = true;
    error = false;//5-12-21 added
    results = new List<hpPatientSearchStruct.PatSearchResultLogs>{};//5-12-21 added
    string localOnly = ApexPages.CurrentPage().getParameters().get('local');

    if (eSSN != null) {
      eSSN = eSSN.trim();
      eSSN = eSSN.replaceAll('-', '');
    }
    if (efn != null) {
      efn = efn.toUpperCase().trim();
    }
    if (eln != null) {
      eln = eln.toUpperCase().trim();
    }
    if (cert != null) {
      cert = cert.trim();
    }

    if(string.isBlank(uw)){
        //search by Member Fname
        if(efn!=''&& eln==''&& cert==''){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select a Client when searching by First name'));
          error=true;
          return;
          //search by Member Lname 
        }else if(eln!=''&&efn==''&& cert==''){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select a Client when searching by Last name'));
          error=true;
          return;
          
        //Search by First and Last Name  
        }else if(eln!=''&&efn!=''&& cert==''){
          ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select a Client when searching by First and Last Name'));
          error=true;
          return;        
        //12-01-21 o.brown Added update to not allow OHY to search healthpac without selecting a client
        }else if(cert != null&&ohyClient==true){
         ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please Select a Client when searching by Member ID'));
         error=true;
         return;          
        } 
  
  
  } 

    resultsMap = new Map<string, hpPatientSearchStruct.PatSearchResultLogs>{};
    enrolledMap = new Map<string, hpPatientSearchStruct.PatSearchResultLogs>();

    hpSearchfound = false;
    hpEligSelected = false;

    hpPatientSearchStruct foo = hp_patientSearch.searchHP(
      uw,
      essn,
      cert,
      efn,
      eln
    );
   
    if (foo.ResultCode == '1') {
      //no results found
      return;
    }

    if (foo == null || foo.ResultCode != '2') {
      ApexPages.addMessage(
        new ApexPages.Message(
          ApexPages.Severity.ERROR,
          'An error has occured in the healthpac API connection'
        )
      );
      error = true;
      return;
    }

    if (foo.PatSearchResultLogs == null) {
      return;
    }
    if (activeFlag == null) {
      activeFlag = false;
    }
    for (
      hpPatientSearchStruct.PatSearchResultLogs s : foo.PatSearchResultLogs
    ) {
      if (s.Grp == null || (s.Grp != null && s.Grp.trim() != 'HDP')) {
        hpPatientSearchStruct.PatSearchResultLogs psr = new hpPatientSearchStruct.PatSearchResultLogs(
          s.Underwriter,
          s.Grp,
          s.SSN,
          s.EmpFirstName,
          s.EmpLastName,
          s.EmpDateOfBirth,
          s.EmpAddress1,
          s.EmpAddress2,
          s.EmpCity,
          s.EmpState,
          s.EmpZip,
          s.Sequence,
          s.PatFirstName,
          s.PatLastName,
          s.PatDateOfBirth,
          decodeRelationship(s.Relationship),
          s.Gender,
          decodeStatus(s.Status),
          s.Dept,
          safeTrim(s.MemberNumber),
          safeClient(s.Underwriter)
        );

        if (
          (activeFlag && psr.status == 'Active') ||
          !activeFlag &&
          (uw == null ||
          uw == '' ||
          uw == s.Underwriter)
        ) {
          hpSearchfound = true;
          if (s.Relationship == '1') {
            employeeMap.put(s.SSN, psr);
          } else {
            hpPatientSearchStruct.PatSearchResultLogs ttt = employeeMap.get(
              s.SSN
            );
            if (ttt != null) {
              psr.EmpAddress1 = ttt.EmpAddress1;
              psr.EmpCity = ttt.EmpCity;
              psr.EmpState = ttt.EmpState;
              psr.EmpZip = ttt.EmpZip;
            }
          }
          psr.Enrolled = false;
          system.debug('s: '+s);  
          //Map of patient keys
          enrolledMap.put(
            populationHealthUtils.patientkey(
              new populationHealthUtils.patientKeyStruct(
                safeTrim(s.MemberNumber),
                s.SSN,
                s.Sequence,
                s.Underwriter,
                s.PatDateOfBirth,
                s.Grp
              )
            ),
            psr
          );
          
          string key =
            psr.PatFirstName.trim() +
            psr.PatLastName.trim() +
            psr.PatDateOfBirth.trim() +
            psr.Underwriter.trim() +
            psr.Sequence +
            safeTrim(s.MemberNumber)+
            psr.Grp;
          key = key.replaceAll(' ', '');
          resultsMap.put(key, psr);
          results.add(psr);
        }
      }
    }

    for (phm_Patient__c p : [
      SELECT patientKey__c
      FROM phm_Patient__c
      WHERE patientKey__c IN :enrolledMap.keySet()
    ]) {
      hpPatientSearchStruct.PatSearchResultLogs bar = enrolledMap.get(
        p.patientKey__c
      );
      bar.enrolled = true;
      bar.PatientRecordId = p.id;
    }

    hpPatientSearchStruct.PatSearchResultLogs[] enrolledResults = new List<hpPatientSearchStruct.PatSearchResultLogs>{};
    hpPatientSearchStruct.PatSearchResultLogs[] enrolledResultsNotEnrolled = new List<hpPatientSearchStruct.PatSearchResultLogs>{};

    for (hpPatientSearchStruct.PatSearchResultLogs psr : results) {
      if (psr.enrolled == true) {
        enrolledResults.add(psr);
      } else {
        enrolledResultsNotEnrolled.add(psr);
      }
    }

    results.clear();
    results.addAll(enrolledResults);
    results.addAll(enrolledResultsNotEnrolled);
  }

  public void eligDetail() {
    hpEligFound = false;
    string key = ApexPages.CurrentPage().getParameters().get('memberKey');
    system.debug('key: ' + key);
    system.debug('resultsMap: '+resultsMap.keyset());
    key = key.replaceAll(' ', '');

    hpPatientSearchStruct.PatSearchResultLogs member = resultsMap.get(key);
    hpEligDetailStruct eligDetail = hp_eligSearch.searchHP(
      member.Ssn,
      member.underwriter,
      member.grp,
      member.sequence,
      string.valueOf(date.today().addDays(-1)).remove('-')
    );
    eligDetail.cleanUp();

    hpDetail = eligDetail.eligibilityDetail;

    if (eligDetail.eligibilityDetail == null) {
      hpDetail = new hpEligDetailStruct.EligibilityInformation();
    } else {
      hpEligFound = true;
    }
    patientDetail = new patientDetail();

    patientDetail.patientRecordId = member.PatientRecordId;
    patientDetail.MemberNumber = member.MemberNumber;
    patientDetail.Grp = member.Grp;
    patientDetail.Underwriter = member.underwriter;
    patientDetail.sequence = member.sequence;

    patientDetail.EligDate = hpDetail.EligDate;
    patientDetail.MedicalBenefitPlan = hpDetail.MedicalBenefitPlan;
    patientDetail.Status = hpDetail.Status;
    patientDetail.ClientName = member.ClientName;

    patientDetail.MedicalCOBFlag = hpDetail.MedicalCOBFlag;
    patientDetail.Relationship = member.Relationship;
    patientDetail.MedicalFamilyCoverageCode = hpDetail.MedicalFamilyCoverageCode;
    patientDetail.EmpCity = member.EmpCity;
    patientDetail.EmpAddress1 = member.EmpAddress1;
    patientDetail.EmpState = member.EmpState;
    patientDetail.EmpZip = member.EmpZip;

    patientDetail.PatFirstName = member.PatFirstName;
    patientDetail.PatLastName = member.PatLastName;
    patientDetail.PatDateOfBirth = member.PatDateOfBirth;
    patientDetail.Ssn = member.Ssn;
    Gender = member.Gender; 
    client__c client = clientMap.get(member.underwriter);
    if (client == null) {
      client = new client__c(
        underwriter__c = member.underwriter,
        Group_Number__c = member.grp
      );
      insert client;
      clientMap.put(member.underwriter, client);
    }
    clientLogo = '/phmLogos/' + client.logoDirectory__c;
    servicesAvailable = new List<string>{};

    if (client.Utilization_Management__c) {
      servicesAvailable.add('Utilization Management');
    }

    if (client.Care_Management__c) {
      servicesAvailable.add('Disease Management');
    }

    if (client.Disease_Management__c) {
      servicesAvailable.add('Care Management');
    }

    if (client.Maternity_Management__c) {
      servicesAvailable.add('Maternity Management');
    }
    hpEligSelected = true;
  }

  public patientDetail patientDetail { get; set; }

  class patientDetail {
    public string ClientName { get; set; }
    public id patientRecordId { get; set; }

    public string MemberNumber { get; set; }
    public string Grp { get; set; }
    public string Underwriter { get; set; }
    public string Sequence { get; set; }
    public string DepSsn { get; set; }
    public string EligDate { get; set; }
    public string Salary { get; set; }
    public string Dept { get; set; }
    public string MedicalFamilyCoverageCode { get; set; }
    public string Status { get; set; }
    public string MedicalBenefitPlan { get; set; }
    public string MedicalCOBFlag { get; set; }
    public string Ssn { get; set; }

    public string EmpFirstName { get; private set; }
    public string EmpLastName { get; private set; }
    public string EmpDateOfBirth { get; private set; }
    public string EmpAddress1 { get; set; }
    public string EmpAddress2 { get; set; }
    public string EmpCity { get; set; }
    public string EmpState { get; set; }
    public string EmpZip { get; set; }

    public string PatFirstName { get; private set; }
    public string PatLastName { get; private set; }
    public string PatClientName { get; private set; }
    public string PatDateOfBirth { get; private set; }
    public string Relationship { get; private set; }
  }

  public void quickFilter() {
    if (preFilterResults == null) {
      preFilterResults = results.clone();
    }

    string filter = ApexPages.CurrentPage().getParameters().get('filter');
    results = preFilterResults.clone();

    if (filter == '') {
      return;
    }

    if (filter == 'activeFilter') {
      activeFilter();
    }

    ApexPages.CurrentPage().getParameters().put('filter', null);
  }

  void activeFilter() {
    hpPatientSearchStruct.PatSearchResultLogs[] foo = new List<hpPatientSearchStruct.PatSearchResultLogs>{};
    for (hpPatientSearchStruct.PatSearchResultLogs r : results) {
      if (r.Status == 'Active') {
        foo.add(r);
      }
    }

    results.clear();
    results = foo.clone();
  }

  public pageReference moveFile() {
    string fname = ApexPages.CurrentPage().getParameters().get('filename');
    system.debug('fname ' + fname);
    error = false;
    if (fname != null) {
      if (patientDetail.patientRecordId == null) {
        enroll();

        if (error) {
          return null;
        }
      }

      string safeFname = fname;

      try {
        fileServer.safename(safeFname.escapeHtml3());
      } catch (exception e) {
        error = true;
        ApexPages.addMessage(
          new ApexPages.Message(
            ApexPages.Severity.ERROR,
            'An error moving the file ' + e.getMessage()
          )
        );
        system.debug(e.getMessage());
      }

      //string fileSrc = fileSrcBaseDir+fname;
      string fileSrc = fileSrcBaseDir + originalFileName;
      string pFileDir =
        patientDetail.PatLastName.trim() +
        '_' +
        patientDetail.PatFirstName.trim() +
        patientDetail.PatDateOfBirth.replaceAll('/', '');
      pFileDir = pFileDir.replace('.', '');

      //string initFileDestName = patientDetail.PatLastName.trim()+'_'+patientDetail.PatFirstName.trim().left(1);
      //initFileDestName=initFileDestName.replace('.','').replaceAll(' ','_');

      if (fname.length() > 30) {
        fname = fname.left(30);
      }
      fname = fname.replaceAll('.pdf', '');
      string fileDestname = fname + '.pdf';
      fileDestname = fileDestname.replaceAll(' ', '_');
      fileDestname = fileDestname.replaceAll('-', '_');
      fileDestname = fileDestname.replaceAll(',', '_');
      fileDestname = fileDestname.replaceAll('#', '_');
      fileDestname = fileDestname.replaceAll('&', '_');

      string fd = baseDir + '' + pFileDir + '/' + fileDestname;
      system.debug('fileSrc ' + fileSrc);
      system.debug('fd ' + fd);

      FileChangeRequestResponseStruct result = fileServer.moveFile(
        fileSrc,
        fd
      );

      if (result != null && result.ResultCode == '0') {
        Population_Health_Attachment__c attachment = new Population_Health_Attachment__c(
          Patient__c = patientDetail.patientRecordId
        );
        attachment.File_Directory__c = pFileDir;
        attachment.File_Name__c = fileDestname;
        attachment.File_Description__c = file_description;

        insert attachment;
        selectedFileName = null;
        originalFileName = null;

        set<id> patientIds = new Set<id>();
        patientIds.add(attachment.id);
        populationHealthFuture.sendImagePacFileRoute(patientIds);
      } else {
        error = true;
        ApexPages.addMessage(
          new ApexPages.Message(
            ApexPages.Severity.ERROR,
            'An error moving the file ' + result.Message
          )
        );
        //ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'An error moving the file ' + 'result'));
        system.debug(result.Message);
      }
    }
    return null;
  }

  public void setSelectedFileName() {
    string fn = ApexPages.CurrentPage()
      .getParameters()
      .get('selectedFileName');
    string fsbd = ApexPages.CurrentPage()
      .getParameters()
      .get('fileSrcBaseDir');

    selectedFileName = fn.escapeHtml3();
    fileSrcBaseDir = fsbd.escapeHtml3();
    originalFileName = selectedFileName;
    selectedFileName = selectedFileName.substring(
      0,
      selectedFileName.indexOf('.')
    );
  }

  public pageReference openPatientRecord() {
    pageReference pageRef;
    if (openPatientFileInline == false) {
      pageRef = new pageReference(
        '/apex/populationHealthPatient?patientRecordId=' +
        patientDetail.patientRecordId
      );
      pageRef.setRedirect(false);
    } else {
      ApexPages.CurrentPage()
        .getParameters()
        .put('pid', patientDetail.patientRecordId);
      return null;
    }

    return pageRef;
  }
  public phm_Patient__c patient { get; private set; }

  public void enroll() {
    error = false;
    patient = new phm_Patient__c();

    if (patientDetail.PatLastName != null) {
      patient.Patient_First_Name__c =
        patientDetail.PatFirstName.subString(0, 1).ToUpperCase() +
        patientDetail.PatFirstName.subString(1).tolowercase();
    }

    if (patientDetail.PatLastName != null) {
      patient.Patient_Last_Name__c =
        patientDetail.PatLastName.subString(0, 1).ToUpperCase() +
        patientDetail.PatLastName.subString(1).tolowercase();
    }

    patient.Patient_Employer__c = clientmap.get(patientDetail.Underwriter)
      .id;
    patient.Employer_Group__c = patientDetail.Grp;
    patient.Patient_Date_of_Birth__c = patientDetail.PatDateOfBirth;

    patient.Most_Recent_Plan_Number__c = hpDetail.MedicalBenefitPlan;
    patient.Medical_Status__c = decodeStatus(hpDetail.Status);
    patient.Patient_Social_Security_Number__c = patientDetail.Ssn;
    patient.Subscriber_Social_Security_Number__c = patientDetail.Ssn;
    patient.cert__c = patientDetail.MemberNumber;

    If(gender=='F'){
      gender='Female';
   
    }else if(gender=='M'){
     gender='Male';
    }
    patient.gender__c = gender;//10-21-21 added o.brown

    if (hpDetail.EligDate != null) {
      string[] eDate = hpDetail.EligDate.split('/');
      patient.Effective_Date__c = date.valueof(
        eDate[2] +
        '-' +
        eDate[0] +
        '-' +
        eDate[1]
      );
    }

    if (patientDetail.EmpFirstName != null) {
      patient.subscriber_first_name__c =
        patientDetail.EmpFirstName.subString(0, 1).ToUpperCase() +
        patientDetail.EmpFirstName.subString(1).tolowercase();
    }

    if (patientDetail.EmpLastName != null) {
      patient.subscriber_last_name__c =
        patientDetail.EmpLastName.subString(0, 1).ToUpperCase() +
        patientDetail.EmpLastName.subString(1).tolowercase();
    }

    patient.Subscriber_Date_of_Birth__c = patientDetail.EmpDateOfBirth;

    patient.Relationship_to_Subscriber__c = patientDetail.Relationship;
    patient.Last_Date_of_Eligibility_Check__c = datetime.now();
    patient.Sequence__c = patientDetail.Sequence;
        
    patient.patientKey__c = populationHealthUtils.patientkey(
      new populationHealthUtils.patientKeyStruct(
        patientDetail.MemberNumber,
        patient.Patient_Social_Security_Number__c,
        patientDetail.Sequence,
        patientDetail.Underwriter,
        patient.Patient_Date_of_Birth__c,
        patient.Employer_Group__c
      )
    );

    try {
      patient.Address__c = employeeMap.get(
          patient.Patient_Social_Security_Number__c
        )
        .EmpAddress1;
      patient.City__c = employeeMap.get(
          patient.Patient_Social_Security_Number__c
        )
        .EmpCity;
      patient.State__c = employeeMap.get(
          patient.Patient_Social_Security_Number__c
        )
        .EmpState;
      patient.Zip__c = employeeMap.get(
          patient.Patient_Social_Security_Number__c
        )
        .EmpZip.left(5);
      patient.Address_Type__c = 'HealthPac';
    } catch (exception e) {
    }

    try {
      insert patient;
      patientDetail.patientRecordId = patient.id;
    } catch (dmlException e) {
      error = true;
      ApexPages.addMessage(
        new ApexPages.Message(
          ApexPages.Severity.ERROR,
          e.getDmlMessage(0)
        )
      );
      system.debug(e.getDmlMessage(0));
      return;
    }
  }

  public void transferUmCase() {
    error = false;
    try {
      Utilization_Management__c oldUmRecord = [
        SELECT
          Approved_Days__c,
          Admission_Date__c,
          Approved_Through_Date__c,
          case_status__c,
          Comment_Line_for_Claims__c,
          Discharge_Date__c,
          Event_Type__c,
          Facility_Name__c,
          Facility_Contact_Name__c,
          Facility_Contact_Phone__c,
          Facility_Provider_Fax_Number__c,
          Facility_Network_Status__c,
          Facility_State__c,
          Facility_Street__c,
          Facility_Type__c,
          Facility_Zip_Code__c,
          Med_Cat__c,
          patient__c,
          Provider_Name__c,
          Provider_Network_Status__c,
          Provider_State__c,
          Provider_Street__c,
          Provider_type__c,
          Provider_Zip_Code__c,
          Quality_Codes__c,
          RecordTypeId,
          Total_Approved_Days__c,
          Total_Days__c,
          Visits__c
        FROM Utilization_Management__c
        WHERE id = :umid
      ];

      phPatientTabsController phPatientTabs = new phPatientTabsController(
        oldUmRecord.patient__c,
        umId
      );
      //phPatientTabs.denyUM();
      phPatientTabs.sendDenial('Canceled');

      if (phPatientTabs.umError) {
        error = true;
        ApexPages.addMessage(
          new ApexPages.Message(
            ApexPages.Severity.ERROR,
            'Unable to deny original Pre-Cert: ' +
            phPatientTabs.floatingMessage
          )
        );
        return;
      }

      if (patientDetail.patientRecordId == null) {
        enroll();
      } else {
        patient = new phm_patient__c(
          id = patientDetail.patientRecordId
        );
      }

      Utilization_Management__c newUmRecord = oldUmRecord.clone();

      newUmRecord.patient__c = patient.id;
      //newUmRecord.case_status__c      ='Approved – Final';
      newUmRecord.case_status__c = oldUmRecord.case_status__c;
      newUmRecord.newBorn__c = true;
      newUmRecord.Newborn_Transfer__c = true;
      insert newUmRecord;
      umid = newUmRecord.id;

      Utilization_Management_Clinical_Code__c[] olddiagnosisCodes = [
        SELECT
          name,
          Code_Type__c,
          Description__c,
          createdDate,
          CreatedBy.name,
          Utilization_Management__c
        FROM Utilization_Management_Clinical_Code__c
        WHERE
          Utilization_Management__c = :oldUmRecord.id
          AND code_type__c = 'Diagnosis'
        ORDER BY name DESC
      ];
      Utilization_Management_Clinical_Code__c[] newDiagnosisCodes = new List<Utilization_Management_Clinical_Code__c>{};

      for (
        Utilization_Management_Clinical_Code__c nd : olddiagnosisCodes
      ) {
        Utilization_Management_Clinical_Code__c zzz = nd.clone();
        zzz.Utilization_Management__c = newUmRecord.id;
        newDiagnosisCodes.add(zzz);
      }

      insert newDiagnosisCodes;

      //copy um and cm notes from old case start
      Utilization_Management_Note__c[] oldNotes = [
        SELECT
          name,
          Body__c,
          Communication_Type__c,
          Subject__c,
          createdDate,
          CreatedBy.name,
          Utilization_Management__c
        FROM Utilization_Management_Note__c
        WHERE Utilization_Management__c = :oldUmRecord.id
        ORDER BY createdDate DESC
      ];
      Utilization_Management_Note__c[] newNotes = new List<Utilization_Management_Note__c>{};

      for (Utilization_Management_Note__c onote : oldNotes) {
        Utilization_Management_Note__c foo = onote.clone();
        foo.Utilization_Management__c = newUmRecord.id;
        newNotes.add(foo);
      }

      insert newNotes;

      Utilization_Management_Clinician__c[] oldCLinicianNotes = [
        SELECT
          name,
          City__c,
          Clinician_Fax__c,
          Contact_Name__c,
          Contact_Phone_Number__c,
          Credentials__c,
          First_Name__c,
          Last_Name__c,
          Network_Status__c,
          Ordering_Clinician__c,
          Salutation__c,
          State__c,
          Street__c,
          CreatedBy.name,
          Utilization_Management__c,
          Zip_Code__c
        FROM Utilization_Management_Clinician__c
        WHERE Utilization_Management__c = :oldUmRecord.id
        ORDER BY createdDate DESC
      ];
      Utilization_Management_Clinician__c[] newClinicianNotes = new List<Utilization_Management_Clinician__c>{};

      for (
        Utilization_Management_Clinician__c ocnote : oldCLinicianNotes
      ) {
        Utilization_Management_Clinician__c zam = ocnote.clone();
        zam.Utilization_Management__c = newUmRecord.id;
        newClinicianNotes.add(zam);
      }

      insert newClinicianNotes;
      //copy um and cm notes from old case end

      oldUmRecord.Comment_Line_for_Claims__c = 'CANCELED';
      oldUmRecord.case_status__c = 'Canceled';
      update oldUmRecord;
    } catch (exception e) {
      error = true;
      ApexPages.addMessage(
        new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage())
      );
      system.debug(e.getMessage() + ' ' + e.getLineNumber());
    }
  }

//5-07-21 aded start***************************************************************************************************************
  public void transferCmCase() {
    Error = false;
system.debug('made it to transferCMCase in patientSearchVFController');
    
    try {
      Case_Management__c oldCmRecord = [
        SELECT
              Benefit_Year_Plan_Year__c,
                  Case_Manager__c,
                  Case_Status__c,
                  Case_Sub_status__c,
                  Cost_Containment__c,
                  Cost_Driver__c,
                  CM_Notes__c,
                  CM_Referral_Denied__c,
                  CM_Risk_Rating__c,
                  CreatedDate,
                  Diagnosis__c,
                  Discharge_Date__c,
                  //Disease_Condition_TX_Plan_Education__c,
                  DuplicateCheck__c,
                  //Cost_Containment_Activity_Completion__c,
                  //Coordination_of_Provider_Services__c,
                  //Education_on_Benefits_Network__c, 
                  Engagement__c, Enrollment__c, 
                  Expected_Cost__c, HCC__c, 
                  Identification_Date__c, 
                  LastModifiedDate, MDC__c, 
                  MDC_Sub_Category__c, name,
                  Newborn__c, Patient__c, 
                  Referral_Source__c, 
                  Next_Review_Date__c, 
                  //Opened__c, 
                  //Patient_Coaching_Support__c,
                  Referral_for_OON_Negotiations__c,
                  //Ref_to_Other_Programs_Benefit_Services__c, 
                  softDelete__c 
                  //Steerage_to_In_Network_Provider__c
        FROM Case_Management__c 
        WHERE id = :cmid
      ];

      phPatientTabsController phPatientTabs = new phPatientTabsController(
        oldCmRecord.patient__c,
        cmId
      );

      if (patientDetail.patientRecordId == null) {
        enroll();
      } else {
        patient = new phm_patient__c(
          id = patientDetail.patientRecordId
        );
      }

      Case_Management__c newCmRecord = oldCmRecord.clone();
      system.debug('made it to oldCmRecord.clone in patientSearchVFController');

      newCmRecord.patient__c = patient.id;
      //newCmRecord.case_status__c = oldCmRecord.case_status__c;5-18-21 updated set newborn to status New
      newCmRecord.case_status__c =  'New';
      newCmRecord.newBorn__c = true;
      newCmRecord.Newborn_Transfer__c = true;
      insert newCmRecord;
      cmid = newCmRecord.id;

      Case_Management_Clinical_Code__c [] olddiagnosisCodes = [
        SELECT
          name,
          Code_Type__c,
          Description__c,
          createdDate,
          CreatedBy.name,
          Case_Management__c 
        FROM Case_Management_Clinical_Code__c 
        WHERE
          Case_Management__c  = :oldCmRecord.id
          AND code_type__c = 'Diagnosis'
        ORDER BY name DESC
      ];
      
      if(olddiagnosisCodes!=null ){
      Case_Management_Clinical_Code__c [] newDiagnosisCodes = new List<Case_Management_Clinical_Code__c >{};

      for (
        Case_Management_Clinical_Code__c  nd : olddiagnosisCodes
      ) {
        Case_Management_Clinical_Code__c  foo = nd.clone();
        foo.Case_Management__c  = newCmRecord.id;
        newDiagnosisCodes.add(foo);
      }
       system.debug('made it to cmDiagnosis codes in patientSearchVFController');
       
      insert newDiagnosisCodes;
      }
      //copy um and cm notes from old case start
      Case_Management_Note__c[] oldNotes = [
        SELECT
          name,
          Body__c,
          Communication_Type__c,
          Subject__c,
          createdDate,
          CreatedBy.name,
          Case_Management__c 
        FROM Case_Management_Note__c
        WHERE Case_Management__c  = :oldCmRecord.id
        ORDER BY createdDate DESC
      ];
      Case_Management_Note__c[] newNotes = new List<Case_Management_Note__c>{};

      for (Case_Management_Note__c onote : oldNotes) {
        Case_Management_Note__c foo = onote.clone();
        foo.Case_Management__c  = newCmRecord.id;
        newNotes.add(foo);
      }
       system.debug('made it to cmNotes.clone in patientSearchVFController');
      insert newNotes;

      Case_Management_Clinician__c[] oldCLinician = [
        SELECT
          name,
          City__c,
          Clinician_Fax__c,
          Contact_Name__c,
          Contact_Phone_Number__c,
          Credentials__c,
          First_Name__c,
          Last_Name__c,
          Network_Status__c,
          Salutation__c,
          State__c,
          Street__c,
          CreatedBy.name,
          Case_Management__c ,
          Zip_Code__c
        FROM Case_Management_Clinician__c
        WHERE Case_Management__c  = :oldCmRecord.id
        ORDER BY createdDate DESC
      ];
      Case_Management_Clinician__c[] newClinician = new List<Case_Management_Clinician__c>{};

      for (
        Case_Management_Clinician__c oclinician : oldCLinician
      ) {
        Case_Management_Clinician__c foo = oclinician.clone();
        foo.Case_Management__c  = newCmRecord.id;
        newClinician.add(foo);
      }
       system.debug('made it to cmClinician.clone in patientSearchVFController');
      insert newClinician;
      //copy um and cm notes from old case end
      
      /* 5-18-21 removed per operations keep old status
      oldCmRecord.CM_Notes__c = 'Closed-Case Transferred';
      oldCmRecord.case_status__c = 'Closed';
      oldCmRecord.Case_Sub_status__c='Termed before enrolled';
      */
      
      update oldCmRecord;
    } catch (exception e) {
      error = true;
      ApexPages.addMessage(
        new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage())
      );
      system.debug(e.getMessage() + ' ' + e.getLineNumber());
    }
  }



//5-07-21 end*********************************************************************************



  string decodeStatus(string status) {
    if (status == 'A') {
      return 'Active';
    } else if (status == 'T') {
      return 'Termed';
    }

    return 'NA';
  }

  string decodeRelationship(string rel) {
    if (rel == '1' || rel == '01') {
      return 'Self';
    } else if (rel == '2' || rel == '02') {
      return 'Spouse';
    } else if (rel == '3' || rel == '03') {
      return 'Son';
    } else if (rel == '4' || rel == '04') {
      return 'Daughter';
    } else if (rel == '5' || rel == '05') {
      return 'Other';
    }

    return 'NA';
  }

  string safeTrim(string xx) {
    string x = xx;
    if (x == null) {
      x = '';
    } else {
      x = x.trim();
    }
    return x;
  }

  string safeClient(string x) {
    string y = '';
    try {
      y = clientMap.get(x).name;
    } catch (exception e) {
      y = x;
    }

    return y;
  }
  
}