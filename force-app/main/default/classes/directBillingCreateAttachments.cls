public with sharing class directBillingCreateAttachments{
    
    public void createInvoiceBatchCalls(set<id> invoiceIds){
        
        id[] idList = new list<id>(invoiceIds);
        integer lSize = idList.size();
       
        for(integer i=0;i<=lSize ; i=i+75){
            
            integer cLower=i;
            
            integer cUpper = cLower+75;
            
            if(cUpper>=lSize) {
                cUpper=lSize-1 ;
            }
            
            if(lSize<75){
                i=76;
            }
            
            set<id> tempIdset = new set<id>();
            
            for(integer x=cLower ; x<=cUpper ; x++){
                tempIdset.add(idList[x]);
            }
            
            directBillingFuture.createPDFinvoice(tempIdset);
            //directBillingFuture.createPDFTerminationletter(tempIdset);//5-19.20 added
        }
        
    }
    
    public void createPastDueLetterBatchCalls(set<id> invoiceIds){
        
        id[] idList = new list<id>(invoiceIds);
        integer lSize = idList.size();
        
        for(integer i=0;i<=lSize ; i=i+75){
            
            integer cLower=i;
            integer cUpper = cLower+75;
            
            if(cUpper>=lSize) {
                cUpper=lSize-1 ;
            }
            
            if(lSize<75){
                i=76;
            }
            
            set<id> tempIdset = new set<id>();
            
            for(integer x=cLower ; x<=cUpper ; x++){
                tempIdset.add(idList[x]);
            }
            
            directBillingFuture.createPDFpastdueletter(tempIdset);
            //directBillingFuture.createPDFTerminationletter(tempIdset);//5-19.20 added
            
            
        }
        
    }

//5-11-20
    public void createTerminationLetterBatchCalls(set<id> invoiceIds){
        
        id[] idList = new list<id>(invoiceIds);
        integer lSize = idList.size();
        
        for(integer i=0;i<=lSize ; i=i+75){
            
            integer cLower=i;
            integer cUpper = cLower+75;
            
            if(cUpper>=lSize) {
                cUpper=lSize-1 ;
            }
            
            if(lSize<75){
                i=76;
            }
            
            set<id> tempIdset = new set<id>();
            
            for(integer x=cLower ; x<=cUpper ; x++){
                tempIdset.add(idList[x]);
            }
            
            directBillingFuture.createPDFTerminationletter(tempIdset);
            
        }
        
    }

    
    //This should called after batching the id's into future calls by the createInvoiceBatchCalls method in this class
    public void createInvoice(set<id> invoiceIds){
        set<direct_billing_leave__c> dbSet = new set<direct_billing_leave__c>();
        map<id, Direct_Billing_Invoice__c[]> invoiceMap = new map<id, Direct_Billing_Invoice__c[]>();
          
        Direct_Billing_Invoice__c[] invoices = [select db_leave__r.Name,
                                                       db_leave__r.DB_Member__r.Employee_Name__c,
                                                       db_leave__r.DB_Member__r.Address_1__c,
                                                       db_leave__r.DB_Member__r.Address_2__c,
                                                       db_leave__r.DB_Member__r.City__c,
                                                       db_leave__r.DB_Member__r.State__c,
                                                       db_leave__r.DB_Member__r.Zip_Code__c,
                                                       db_leave__r.Welcome_Letter_Sent__c,
                                                       db_leave__r.Identity_Theft_Election__c,
                                                       db_leave__r.Identity_Theft_Premium__c,
                                                       db_leave__r.Indemnity_Plan_Election__c,
                                                       db_leave__r.Indemnity_Plan_Premium__c,
                                                       db_leave__r.Legal_Plan_Election__c,
                                                       db_leave__r.Legal_Plan_Premium__c,
                                                       db_leave__r.Medical_Election__c,
                                                       db_leave__r.Medical_Premium__c,
                                                       db_leave__r.Medical_Parma_Election__c,
                                                       db_leave__r.Medical_Parma_Premium__c,
                                                       db_leave__r.Dental_Election__c,         
                                                       db_leave__r.Dental_Premium__c,
                                                       db_leave__r.vision_tier__c,         
                                                       db_leave__r.Vision_Premium__c,
                                                       db_leave__r.fsa_election__c,         
                                                       db_leave__r.fsa_premium__c,
                                                       db_leave__r.Personal_Accident_Election__c,         
                                                       db_leave__r.Personal_Accident_Amount_Per_Pay__c,
                                                       db_leave__r.Critical_Illness_Election__c,         
                                                       db_leave__r.Critical_Illness_Amount_Per_Pay__c,
                                                       db_leave__r.Long_Term_Disability_Election__c,         
                                                       db_leave__r.Long_Term_Disability_Premium__c,
                                                       db_leave__r.Life_and_AD_D_Election__c,         
                                                       db_leave__r.Life_and_AD_D_Premium__c,
                                                       db_leave__r.Spouse_Life_Election__c,         
                                                       db_leave__r.Spouse_Life_Premium__c,
                                                       db_leave__r.Dependent_Life_Election__c,         
                                                       db_leave__r.Dependent_Life_Premium__c,
                                                       db_leave__r.Premiums_Total__c,
                                                       due_date__c,
                                                       invoice_amount__c,
                                                       invoice_month__c,
                                                       invoice_charges__c,
                                                       Pay_Dates_included__c,
                                                       Physical_Invoice_Created__c,
                                                       welcomeLetterNeeded__c,
                                                       pastDueLetterNeeded__c,
                                                       terminationLetterNeeded__c,
                                                       name from Direct_Billing_Invoice__c where id in :invoiceIds and Physical_Invoice_Created__c=null];
                                                       
        ftpAttachment__c[] attachments = new ftpAttachment__c[]{};
        attachment[] inv_attachments = new attachment[]{};
        attachment[] wel_attachments = new attachment[]{};
        
        for(Direct_Billing_Invoice__c invoice : invoices){
            Direct_Billing_Leave__c dbl = new Direct_Billing_Leave__c(id=invoice.db_leave__c);
            dbSet.add(dbl);
            Direct_Billing_Invoice__c[] foo_invoices = invoiceMap.get(dbl.id);
            if(foo_invoices==null){
                foo_invoices= new Direct_Billing_Invoice__c[]{};
            }
            
            foo_invoices.add(invoice);
            invoiceMap.put(dbl.id, foo_invoices);
            
        }
        
        for(Direct_Billing_Invoice__c inv : invoices){
            
            string urlParams = inv.id;
            
            PageReference directBillingInvoice = page.directBillingInvoice;
            directBillingInvoice.getParameters().put('inv',urlParams);
            directBillingInvoice.getParameters().put('empname',inv.db_leave__r.DB_Member__r.Employee_Name__c);
            directBillingInvoice.getParameters().put('empaddy',inv.db_leave__r.DB_Member__r.Address_1__c);
            directBillingInvoice.getParameters().put('empaddy2',inv.db_leave__r.DB_Member__r.Address_2__c);
            
            blob invoiceContent;
            if(Test.isRunningTest()){
                invoiceContent=blob.valueof('unit.test');
            }else{
                invoiceContent=directBillingInvoice.getContent();
            }
               
           // multipartform.uploadFile(invoiceContent,'Invoice_'+string.valueof(inv.invoice_month__c.month())+''+string.valueof(inv.invoice_month__c.year())+'.pdf','Direct Billing/'+inv.db_leave__r.Name+'/',fileServer__c.getInstance().ussscorpion__c+'savefile/');
           attachment inv_attachment = new attachment();
           inv_attachment.body = invoiceContent;
           inv_attachment.parentId = inv.ID;
           inv_attachment.contentType = 'application/pdf';
           inv_attachment.name='Invoice_'+string.valueof(inv.invoice_month__c.month())+'-'+string.valueof(inv.invoice_month__c.year())+'.pdf';
           inv_attachments.add(inv_attachment);
                
           // if(res.getStatusCode() < 250){
                 ftpAttachment__c invoice_attach = new ftpAttachment__c();
                 invoice_attach.fileName__c = 'Invoice_'+string.valueof(inv.invoice_month__c.month())+''+string.valueof(inv.invoice_month__c.year())+'.pdf';
                 invoice_attach.contentType__c = 'application/pdf';
                 invoice_attach.name='Invoice_'+string.valueof(inv.invoice_month__c.month())+'-'+string.valueof(inv.invoice_month__c.year())+'.pdf';
                 invoice_attach.parentId__c = inv.ID;
                 invoice_attach.subDirectory__c='Direct Billing/'+inv.db_leave__r.Name+'/';
                 attachments.add(invoice_attach);
                 inv.Physical_Invoice_Created__c=date.today();
           // }
           
           if(inv.welcomeLetterNeeded__c){
               
               PageReference directBillingWelcome = new ApexPages.StandardController(inv).view();
               directBillingWelcome = page.directBillingWelcome ;
               directBillingWelcome.getParameters().put('id',inv.ID);
               directBillingWelcome.getParameters().put('empname',inv.db_leave__r.DB_Member__r.Employee_Name__c);
               directBillingWelcome.getParameters().put('empaddy',inv.db_leave__r.DB_Member__r.Address_1__c);
               directBillingWelcome.getParameters().put('empaddy2',inv.db_leave__r.DB_Member__r.Address_2__c);
               
               blob welcomeLetterContent;
               if(Test.isRunningTest()){
                   welcomeLetterContent=blob.valueof('unit.test');
               }else{
                   welcomeLetterContent=directBillingWelcome.getContent();
               }
               
               string fileName = inv.ID+'_WelcomeLetter_'+string.valueof(inv.invoice_month__c.month())+''+string.valueof(inv.invoice_month__c.year())+'.pdf';
               attachment wl_attachment = new attachment();
               wl_attachment.body = welcomeLetterContent;
               wl_attachment.parentId = inv.ID;
               wl_attachment.contentType = 'application/pdf';
               wl_attachment.name='Welcome Letter_'+string.valueof(inv.invoice_month__c.month())+'-'+string.valueof(inv.invoice_month__c.year())+'.pdf';
               wel_attachments.add(wl_attachment);
           
               //if(welcomeRes.getStatusCode() < 250){
                 ftpAttachment__c attach = new ftpAttachment__c();
                 attach.fileName__c = fileName;
                 attach.contentType__c = 'application/pdf';
                 attach.name='Welcome Letter_'+string.valueof(inv.invoice_month__c.month())+'-'+string.valueof(inv.invoice_month__c.year())+'.pdf';
                 attach.parentId__c = inv.ID;
                 attach.subDirectory__c='Direct Billing/'+inv.db_leave__r.Name+'/';
                 attachments.add(attach);
              //}    
           
           }                                               
        
        }
        
        insert inv_attachments;
        insert wel_attachments;
        map<id, id> inv_map = new map<id, id>();
        map<id, id> wel_map = new map<id, id>();
        
        for(Attachment a : inv_attachments){
            inv_map.put(a.parentId, a.id);
        }
        
        for(Attachment a : wel_attachments){
            wel_map.put(a.parentId, a.id);
        }
        
        
        for(ftpAttachment__c ftp_a : attachments){
            if(ftp_a.name.left(7)=='Invoice'){
                ftp_a.attachmentID__c= inv_map.get(ftp_a.parentId__c );
            
            }else{
                ftp_a.attachmentID__c= wel_map.get(ftp_a.parentId__c );
            
            }    
        }
        
        insert attachments;
        update invoices;
        
    }
    
    public void createPastDueLetter(set<id> invoiceIds){
        map<id, Direct_Billing_Invoice__c> currentInvoiceMap = new map<id, Direct_Billing_Invoice__c>();
        map<id, Direct_Billing_Invoice__c> invoiceDates = new map<id, Direct_Billing_Invoice__c>();
        ftpAttachment__c[] attachments = new ftpAttachment__c[]{};
        attachment[] pd_attachments = new attachment[]{};
        
        Direct_Billing_Invoice__c[] invoices = [select db_leave__r.DB_Member__r.Employee_Name__c,
                                                       db_leave__r.DB_Member__r.Address_1__c,
                                                       db_leave__r.DB_Member__r.Address_2__c,
                                                       db_leave__r.DB_Member__r.City__c,
                                                       db_leave__r.DB_Member__r.State__c,
                                                       db_leave__r.DB_Member__r.Zip_Code__c,
                                                       db_leave__r.Welcome_Letter_Sent__c,
                                                       db_leave__r.Identity_Theft_Election__c,
                                                       db_leave__r.Identity_Theft_Premium__c,
                                                       db_leave__r.Indemnity_Plan_Election__c,
                                                       db_leave__r.Indemnity_Plan_Premium__c,
                                                       db_leave__r.Legal_Plan_Election__c,
                                                       db_leave__r.Legal_Plan_Premium__c,                                                       
                                                       db_leave__r.Medical_Election__c,
                                                       db_leave__r.Medical_Premium__c,
                                                       db_leave__r.Medical_Parma_Election__c,
                                                       db_leave__r.Medical_Parma_Premium__c,
                                                       db_leave__r.Name,
                                                       db_leave__r.Dental_Election__c,         
                                                       db_leave__r.Dental_Premium__c,
                                                       db_leave__r.vision_tier__c,         
                                                       db_leave__r.Vision_Premium__c,
                                                       db_leave__r.fsa_election__c,         
                                                       db_leave__r.fsa_premium__c,
                                                       db_leave__r.Personal_Accident_Election__c,         
                                                       db_leave__r.Personal_Accident_Amount_Per_Pay__c,
                                                       db_leave__r.Critical_Illness_Election__c,         
                                                       db_leave__r.Critical_Illness_Amount_Per_Pay__c,
                                                       db_leave__r.Long_Term_Disability_Election__c,         
                                                       db_leave__r.Long_Term_Disability_Premium__c,
                                                       db_leave__r.Life_and_AD_D_Election__c,         
                                                       db_leave__r.Life_and_AD_D_Premium__c,
                                                       db_leave__r.Spouse_Life_Election__c,         
                                                       db_leave__r.Spouse_Life_Premium__c,
                                                       db_leave__r.Dependent_Life_Election__c,         
                                                       db_leave__r.Dependent_Life_Premium__c,
                                                       db_leave__r.Premiums_Total__c,
                                                       db_leave__r.Outstanding_Balance__c,
                                                       db_leave__r.Current_Balance__c,
                                                       new_charges__c,
                                                       due_date__c,
                                                       invoice_amount__c,
                                                       invoice_month__c,
                                                       invoice_charges__c,
                                                       Pay_Dates_included__c,
                                                       Previous_Invoice_Amount__c,
                                                       Previous_Invoice_Date__c,
                                                       Physical_Invoice_Created__c,
                                                       welcomeLetterNeeded__c,
                                                       pastDueLetterNeeded__c,
                                                       name from Direct_Billing_Invoice__c where id in: invoiceIds];
                                                       
         for(Direct_Billing_Invoice__c dbi :invoices){
             currentInvoiceMap.put(dbi.id, dbi);
             invoiceDates.put(dbi.db_leave__c, null);
         
         }
     
         for(Direct_Billing_Invoice__c dbi :[select id, invoice_date__c, db_leave__c,invoice_amount__c, invoice_month__c from Direct_Billing_Invoice__c where db_leave__c in :invoiceDates.keySet() and id not in: currentInvoiceMap.keySet() order by invoice_month__c asc]){
             invoiceDates.put(dbi.db_leave__c, dbi);
         }
        
         
         for(id i : currentInvoiceMap.keySet()){
             
             string previousInvoiceDate = invoiceDates.get(currentInvoiceMap.get(i).db_leave__c).invoice_month__c.month()+'/'+invoiceDates.get(currentInvoiceMap.get(i).db_leave__c).invoice_month__c.day() +'/'+ invoiceDates.get(currentInvoiceMap.get(i).db_leave__c).invoice_month__c.year();
             
             PageReference directBillingPastDueLetter = new ApexPages.StandardController(currentInvoiceMap.get(i)).view();
             directBillingPastDueLetter= page.directBillingPastDueLetter ;
             directBillingPastDueLetter.getParameters().put('id',currentInvoiceMap.get(i).ID);
             directBillingPastDueLetter.getParameters().put('empname',currentInvoiceMap.get(i).db_leave__r.DB_Member__r.Employee_Name__c);
             directBillingPastDueLetter.getParameters().put('empaddy',currentInvoiceMap.get(i).db_leave__r.DB_Member__r.Address_1__c);
             directBillingPastDueLetter.getParameters().put('empaddy2',currentInvoiceMap.get(i).db_leave__r.DB_Member__r.Address_2__c);
             directBillingPastDueLetter.getParameters().put('previousInvoiceDate', previousInvoiceDate);
             directBillingPastDueLetter.getParameters().put('outstandingBalance', string.valueof(currentInvoiceMap.get(i).db_leave__r.Current_Balance__c-currentInvoiceMap.get(i).new_charges__c));
             
             string fileName = currentInvoiceMap.get(i).ID+'_PastDueLetter_'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.month())+''+string.valueof(currentInvoiceMap.get(i).invoice_month__c.year())+'.pdf';
             blob PastDueLetterContent;
             if(Test.isRunningTest()){
                 PastDueLetterContent =blob.valueof('unit.test');
             }else{
                 PastDueLetterContent =directBillingPastDueLetter.getContent();
             }
             //HttpResponse pastDueRes = ussscorpionFileSharing.uploader(fileName , 'application/pdf' , PastDueLetterContent);
             attachment pd_attachment = new attachment();
             pd_attachment.body = PastDueLetterContent ;
             pd_attachment.parentId = i;
             pd_attachment.contentType = 'application/pdf';
             pd_attachment.name='PastDue_Letter_'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.month())+'-'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.year())+'.pdf';
             pd_attachments.add(pd_attachment);
            
           //  if(pastDueRes.getStatusCode() < 250){
                 ftpAttachment__c attach = new ftpAttachment__c();
                 attach.fileName__c = fileName;
                 attach.contentType__c = 'application/pdf';
                 attach.name='PastDue_Letter_'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.month())+'-'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.year())+'.pdf';
                 attach.parentId__c = i;
                 attach.subDirectory__c='Direct Billing/'+currentInvoiceMap.get(i).db_leave__r.Name+'/';
                 attachments.add(attach);
                 //currentInvoiceMap.get(i).pastDueLetterNeeded__c=false;
           //  }  
         }  
         
         insert pd_attachments;
         map<id, id> pd_map = new map<id, id>();
         for(Attachment pda : pd_attachments){
             pd_map.put(pda.parentId, pda.id);
         }
         for(ftpAttachment__c a : attachments){
             a.attachmentID__c= pd_map.get(a.parentId__c);
         }
         insert attachments;
         update invoices;
    
    }
    //5-11-20
    public void createTerminationLetter(set<id> invoiceIds){
        map<id, Direct_Billing_Invoice__c> currentInvoiceMap = new map<id, Direct_Billing_Invoice__c>();
        map<id, Direct_Billing_Invoice__c> invoiceDates = new map<id, Direct_Billing_Invoice__c>();
        ftpAttachment__c[] attachments = new ftpAttachment__c[]{};
        attachment[] term_attachments = new attachment[]{};
        
        Direct_Billing_Invoice__c[] invoices = [select db_leave__r.DB_Member__r.Employee_Name__c,
                                                       db_leave__r.DB_Member__r.Address_1__c,
                                                       db_leave__r.DB_Member__r.Address_2__c,
                                                       db_leave__r.DB_Member__r.City__c,
                                                       db_leave__r.DB_Member__r.State__c,
                                                       db_leave__r.DB_Member__r.Zip_Code__c,
                                                       db_leave__r.Welcome_Letter_Sent__c,
                                                       db_leave__r.Identity_Theft_Election__c,
                                                       db_leave__r.Identity_Theft_Premium__c,
                                                       db_leave__r.Indemnity_Plan_Election__c,
                                                       db_leave__r.Indemnity_Plan_Premium__c,
                                                       db_leave__r.Legal_Plan_Election__c,
                                                       db_leave__r.Legal_Plan_Premium__c,                                                       
                                                       db_leave__r.Medical_Election__c,
                                                       db_leave__r.Medical_Premium__c,
                                                       db_leave__r.Medical_Parma_Election__c,
                                                       db_leave__r.Medical_Parma_Premium__c,
                                                       db_leave__r.Name,
                                                       db_leave__r.Dental_Election__c,         
                                                       db_leave__r.Dental_Premium__c,
                                                       db_leave__r.vision_tier__c,         
                                                       db_leave__r.Vision_Premium__c,
                                                       db_leave__r.fsa_election__c,         
                                                       db_leave__r.fsa_premium__c,
                                                       db_leave__r.Personal_Accident_Election__c,         
                                                       db_leave__r.Personal_Accident_Amount_Per_Pay__c,
                                                       db_leave__r.Critical_Illness_Election__c,         
                                                       db_leave__r.Critical_Illness_Amount_Per_Pay__c,
                                                       db_leave__r.Long_Term_Disability_Election__c,         
                                                       db_leave__r.Long_Term_Disability_Premium__c,
                                                       db_leave__r.Life_and_AD_D_Election__c,         
                                                       db_leave__r.Life_and_AD_D_Premium__c,
                                                       db_leave__r.Spouse_Life_Election__c,         
                                                       db_leave__r.Spouse_Life_Premium__c,
                                                       db_leave__r.Dependent_Life_Election__c,         
                                                       db_leave__r.Dependent_Life_Premium__c,
                                                       db_leave__r.Premiums_Total__c,
                                                       db_leave__r.Outstanding_Balance__c,
                                                       db_leave__r.Current_Balance__c,
                                                       new_charges__c,
                                                       due_date__c,
                                                       invoice_amount__c,
                                                       invoice_month__c,
                                                       invoice_charges__c,
                                                       Pay_Dates_included__c,
                                                       Previous_Invoice_Amount__c,
                                                       Previous_Invoice_Date__c,
                                                       Physical_Invoice_Created__c,
                                                       welcomeLetterNeeded__c,
                                                       pastDueLetterNeeded__c,
                                                       terminationLetterNeeded__c,
                                                       Days_since_Due__c,
                                                       name from Direct_Billing_Invoice__c where Days_since_Due__c>89and id in: invoiceIds];
                                                       
         for(Direct_Billing_Invoice__c dbi :invoices){
             currentInvoiceMap.put(dbi.id, dbi);
             invoiceDates.put(dbi.db_leave__c, null);
         
         }
     
         for(Direct_Billing_Invoice__c dbi :[select id, invoice_date__c, db_leave__c,invoice_amount__c, invoice_month__c from Direct_Billing_Invoice__c where db_leave__c in :invoiceDates.keySet() and id not in: currentInvoiceMap.keySet() order by invoice_month__c asc]){
             invoiceDates.put(dbi.db_leave__c, dbi);
         }
        
         for(id i : currentInvoiceMap.keySet()){
             
             //string previousInvoiceDate = invoiceDates.get(currentInvoiceMap.get(i).db_leave__c).invoice_month__c.month()+'/'+invoiceDates.get(currentInvoiceMap.get(i).db_leave__c).invoice_month__c.day() +'/'+ invoiceDates.get(currentInvoiceMap.get(i).db_leave__c).invoice_month__c.year();
             
             PageReference directBillingTerminationLetter = new ApexPages.StandardController(currentInvoiceMap.get(i)).view();
             directBillingTerminationLetter= page.directBillingTerminationLetter ;
             directBillingTerminationLetter.getParameters().put('id',currentInvoiceMap.get(i).ID);
             directBillingTerminationLetter.getParameters().put('empname',currentInvoiceMap.get(i).db_leave__r.DB_Member__r.Employee_Name__c);
             directBillingTerminationLetter.getParameters().put('empaddy',currentInvoiceMap.get(i).db_leave__r.DB_Member__r.Address_1__c);
             directBillingTerminationLetter.getParameters().put('empaddy2',currentInvoiceMap.get(i).db_leave__r.DB_Member__r.Address_2__c);
             //directBillingTerminationLetter.getParameters().put('previousInvoiceDate', previousInvoiceDate);
             directBillingTerminationLetter.getParameters().put('outstandingBalance', string.valueof(currentInvoiceMap.get(i).db_leave__r.Current_Balance__c-currentInvoiceMap.get(i).new_charges__c));
             
             string fileName = currentInvoiceMap.get(i).ID+'_TerminationLetter_'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.month())+''+string.valueof(currentInvoiceMap.get(i).invoice_month__c.year())+'.pdf';
             blob TerminationLetterContent;
             if(Test.isRunningTest()){
                 TerminationLetterContent =blob.valueof('unit.test');
             }else{
                 TerminationLetterContent =directBillingTerminationLetter.getContent();
             }
             //HttpResponse termRes = ussscorpionFileSharing.uploader(fileName , 'application/pdf' , TerminationLetterContent);
             attachment term_attachment = new attachment();
             term_attachment.body = TerminationLetterContent;
             term_attachment.parentId = i;
             term_attachment.contentType = 'application/pdf';
             term_attachment.name='Termination_Letter_'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.month())+'-'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.year())+'.pdf';
             term_attachments.add(term_attachment);
               
             //if(termRes.getStatusCode() < 250){
                 ftpAttachment__c attach = new ftpAttachment__c();
                 attach.fileName__c = fileName;
                 attach.contentType__c = 'application/pdf';
                 attach.name='Termination_Letter_'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.month())+'-'+string.valueof(currentInvoiceMap.get(i).invoice_month__c.year())+'.pdf';
                 attach.parentId__c = currentInvoiceMap.get(i).ID;
                 attach.subDirectory__c='Direct Billing/'+currentInvoiceMap.get(i).db_leave__r.Name+'/';
                 attachments.add(attach);
                 //currentInvoiceMap.get(i).pastDueLetterNeeded__c=false;
             //}  
         }  
         insert term_attachments;
         map<id, id> term_map = new map<id, id>();
         for(Attachment tda : term_attachments){
             term_map.put(tda.parentId, tda.id);
         }
         for(ftpAttachment__c a : attachments){
             a.attachmentID__c= term_map.get(a.parentId__c);
         }
         
         insert attachments;
         
         update invoices;
    
    }    

}