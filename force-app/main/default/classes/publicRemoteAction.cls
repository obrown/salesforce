public with sharing class publicRemoteAction {

    public publicRemoteAction(caseController controller) {

    }


    public publicRemoteAction(ApexPages.StandardController controller) {

    }
    
    
   @RemoteAction
   public static string addForceNote(string id, string subject ,string body){
    
        try{
        
        Program_Notes__c pn = new Program_Notes__c(Patient_Case__c=id,Subject__c=subject,Notes__c=body,Communication_Type__c='Note');
        insert pn;              
            
        }catch (exception e){
            return e.getMessage();
        }
        
        return '200';
   } 

   @RemoteAction
   public static string getAddonCharges(string id){ //used for patient cases
        Clinical_Codes__c aoc = new Clinical_Codes__c();
        string jsonString;
        try{
            
            try{
                aoc = [select Add_On_Charges__c, Add_on_charges_approved__c, Billed_Add_On_Charges__c from Clinical_Codes__c  where id = :id];  
                jsonString = JSON.serialize(aoc);
            }catch (exception e){
            }
        }catch (exception e){
            return e.getMessage();
        }
        
        return jsonString;
   }
   
   @RemoteAction
   public static string setAddonCharges(string id, string values){ //used for patient cases
       
        Clinical_Codes__c aoc = new Clinical_Codes__c();
        
        try{
           aoc = (Clinical_Codes__c)JSON.deserialize(values, Clinical_Codes__c.class);           
         }catch (exception e){
            return e.getMessage();
        }
        
        return '200';
   }    

}