public with sharing virtual class ClaimsMatcher {
    string ccertid;
    formatID fid = new formatID();

    ClaimsReporting__c[] transplantClaims = new ClaimsReporting__c[] {};
    ClaimsReporting__c[] jointCardicSpineClaims = new ClaimsReporting__c[] {};
    ClaimsReporting__c[] wlsClaims = new ClaimsReporting__c[] {};

    final date min_claim_age = Date.newInstance(date.today().addYears(-7).Year(), 1, 1);

    public static void run_from_trigger(ClaimsReporting__c[] claims, set<string> claimsNewlyPaid) {
        claimsMatcher cm = new claimsMatcher();

        cm.sortClaimsList(claims);

        if (cm.wlsClaims.size() > 0) {
            ClaimsMatcherWLS.run(cm.wlsClaims, claimsNewlyPaid, false);
        }

        if (cm.transplantClaims.size() > 0) {
            ClaimsMatcherTransplant.run(cm.transplantClaims, claimsNewlyPaid, false);
        }

        if (cm.jointCardicSpineClaims.size() > 0) {
            ClaimsMatcherJointCardiacSpine.run(cm.jointCardicSpineClaims, claimsNewlyPaid, false);
        }
    }

    void sortClaim(ClaimsReporting__c c){
        switch on c.client_Name__c {
            when 'Walmart Weight Loss' {
                wlsClaims.add(c);
            }

            when 'Walmart Transplant' {
                transplantClaims.add(c);
            }

            when else{
                jointCardicSpineClaims.add(c);
            }
        }
    }

    public void sortClaimsList(ClaimsReporting__c[] claims) {
        for (ClaimsReporting__c c : claims) {
            if (c.client_name__c != null && min_claim_age < c.FDOS__c) { //Claims newer than ~3 years old
                sortClaim(c);
            }
        }
    }
}
