public without sharing class AmsFutureJob {

    @future(callout=true)
    public static void updatePatientNameFromHP(set<id> errorIDs){
        
        magpie_Error__c[] errorList = [select client_name__c, Member_Sequence__c,Underwriter__c,Employee_SSN__c from magpie_Error__c where id in: errorIDs];
        
        for(magpie_Error__c m : errorList){
            hpPatientSearchStruct foo = hp_patientSearch.searchHP(m.Underwriter__c, m.Employee_SSN__c , '', '', '');
            
            if(foo.PatSearchResultLogs==null){return;}
            
            for(hpPatientSearchStruct.PatSearchResultLogs hp : foo.PatSearchResultLogs ){
                if(hp.Sequence.trim()==m.Member_Sequence__c){
                    m.Member_Name__c = hp.PatFirstName.trim().toLowerCase().capitalize() +' '+hp.PatLastName.trim().toLowerCase().capitalize();
                    break;
                }
            }
         
        }
        
        update errorList;
    }

}