@RestResource(urlMapping='/ecen/v1/contacts')
global with sharing class ECENContactsExchange {
    
    @HttpGet
    global static ecenContactExchange doGet() {
        String ins_id = RestContext.request.headers.get('Ins_id');
        
        patient_case__c[] pcList = new patient_case__c[]{};
        oncology__c[] oncologyList = new oncology__c[]{};
        Bariatric_Stage__c[] bsList = new Bariatric_Stage__c[]{};
        
        ecenContactExchange ecx = new ecenContactExchange();
        try{
           pcList = [select id, client_facility__r.facility__r.name, client_facility__r.contact_number__c from patient_case__c where bid_id__c = :ins_id and Converted_Date__c != null and estimated_arrival__c != null];
        }catch(queryException q){
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(ecx));
           } 
           return ecx;
        }
        
        Ecen_Contact_Number__c[] ecenContacts = new Ecen_Contact_Number__c[]{};
        
        ecenContacts = [select name, Contact_Number__c from Ecen_Contact_Number__c where Travel_Only__c = false order by name];
        
        if(!pcList.isempty()){
            for(Ecen_Contact_Number__c ec : [select name, Contact_Number__c from Ecen_Contact_Number__c where Travel_Only__c = true order by name]){
                ecenContacts.add(ec);
            }
           
            ecenContacts.add(new Ecen_Contact_Number__c(name=pclist[0].client_facility__r.facility__r.name, contact_number__c=pclist[0].client_facility__r.contact_number__c));
            
        }
        
        
        try{
           oncologyList= [select id, client_facility__r.facility__r.name, client_facility__r.contact_number__c from oncology__c where bid_id__c = :ins_id and referral_Date__c != null and Estimated_Arrival_Date__c!= null];
        }catch(queryException q){
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(ecx));
           } 
           return ecx;
        }
        
        if(!oncologyList.isEmpty()){
            for(Ecen_Contact_Number__c ec : [select name, Contact_Number__c from Ecen_Contact_Number__c where Travel_Only__c = true order by name]){
                ecenContacts.add(ec);
            }
           
            ecenContacts.add(new Ecen_Contact_Number__c(name=oncologyList[0].client_facility__r.facility__r.name, contact_number__c=oncologyList[0].client_facility__r.contact_number__c));
            
        }
        
        try{
           oncologyList= [select id, client_facility__r.facility__r.name, client_facility__r.contact_number__c from oncology__c where bid_id__c = :ins_id and referral_Date__c != null and Estimated_Arrival_Date__c!= null];
        }catch(queryException q){
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(ecx));
           } 
           return ecx;
        }
        
        if(!oncologyList.isEmpty()){
            for(Ecen_Contact_Number__c ec : [select name, Contact_Number__c from Ecen_Contact_Number__c where Travel_Only__c = true order by name]){
                ecenContacts.add(ec);
            }
           
            ecenContacts.add(new Ecen_Contact_Number__c(name=oncologyList[0].client_facility__r.facility__r.name, contact_number__c=oncologyList[0].client_facility__r.contact_number__c));
            
        }
        
        try{
           bsList= [select id, bariatric__r.client_facility__r.facility__r.name, bariatric__r.client_facility__r.contact_number__c from bariatric_stage__c where bariatric__r.bid_id__c = :ins_id and referral_Date__c != null and Estimated_Arrival__c!= null order by createdDate asc];
        }catch(queryException q){
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(ecx));
           } 
           return ecx;
        }
        
        if(!bsList.isEmpty()){
            for(Ecen_Contact_Number__c ec : [select name, Contact_Number__c from Ecen_Contact_Number__c where Travel_Only__c = true order by name]){
                ecenContacts.add(ec);
            }
           
            ecenContacts.add(new Ecen_Contact_Number__c(name=bsList[0].bariatric__r.client_facility__r.facility__r.name, contact_number__c=bsList[0].bariatric__r.client_facility__r.contact_number__c));
            
        }
        
        ecenContactExchange.contact[] contacts = new ecenContactExchange.contact[]{};
        
        for(Ecen_Contact_Number__c ec : ecenContacts){
            
            ecenContactExchange.contact c = new ecenContactExchange.contact();
            c.name = ec.name;
            c.contactNumber = ec.Contact_Number__c;
            contacts.add(c);
        }
        
        ecx.contacts = contacts;
        
        if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(ecx));
        }
        
        return ecx;
    
    }
    
    
}