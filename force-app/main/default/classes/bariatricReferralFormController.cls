public with sharing class bariatricReferralFormController{
    
    public string pfName {get; private set;}
    public string plName {get; private set;}
    public string pSSN {get;private set;}
    public string pDOB {get; private set;}
    public string pStreet {get; private set;}
    public string pCity {get; private set;}  
    public string pZip {get; private set;}   
    public string pHomePhone {get; private set;}         
    public string pMobilePhone {get; private set;}   
    public string pBID {get; private set;}   
    public string eFname {get; private set;} 
    public string eLname {get; private set;} 
    public string empLabel {get; private set;} 
    
    Bariatric__c bariatric;
    
    public bariatricReferralFormController(ApexPages.StandardController stdController) { 
       empLabel ='Associate';
       bariatric = (Bariatric__c)stdController.getRecord();
        
       if(ApexPages.currentPage().getParameters().get('plName')!=null){
            plName =  ApexPages.currentPage().getParameters().get('plName');
            pfName =  ApexPages.currentPage().getParameters().get('pfName');
            eFname=  ApexPages.currentPage().getParameters().get('eFname');
            eLname = ApexPages.currentPage().getParameters().get('eLname');
            pSSN =    ApexPages.currentPage().getParameters().get('pSSN');
            pCity =   ApexPages.currentPage().getParameters().get('pCity');
            pStreet = ApexPages.currentPage().getParameters().get('pStreet');
            pZip =    ApexPages.currentPage().getParameters().get('pZip');
            pHomePhone = ApexPages.currentPage().getParameters().get('pHomePhone');
            pMobilePhone = ApexPages.currentPage().getParameters().get('pMobilePhone');
            pBID =   ApexPages.currentPage().getParameters().get('pBID');
            pDOB =   ApexPages.currentPage().getParameters().get('pDOB');
            system.debug(pDOB);
            if(pSSN!=null){
                if(pSSN.length()==9){
                    pSSN = pSSN.left(3) + '-' + pSSN.mid(3,2)+ '-' + pSSN.right(4);
                }
            }else{
                pSSN = 'N/A';
            }
            
            if(pDOB ==null){
                pDOB = 'N/A';
            } 
            
        }else{
            plName =  bariatric.patient_last_name__c;
            pfName =  bariatric.patient_first_name__c;
            
            pStreet = bariatric.Patient_Street__c;
            pCity =   bariatric.Patient_City__c;
            pZip =    bariatric.Patient_Zip_Code__c;
            pHomePhone = bariatric.Patient_Home_Phone__c;
            pMobilePhone = bariatric.Patient_Mobile_Phone__c;
            pBID =   bariatric.certID__c;
            
            pSSN =    bariatric.Patient_SSN__c;
            if(pSSN!=null){
                if(pSSN.length()==9){
                    pSSN = pSSN.left(3) + '-' + pSSN.mid(3,2)+ '-' + pSSN.right(4);
                }
            }else{
                pSSN = 'N/A';
            }
        
            if(bariatric.Patient_DOB__c!=null && bariatric.Patient_DOB__c!=''){
                if(bariatric.Patient_DOB__c.contains('-')){
                    string[] foo = bariatric.Patient_DOB__c.split('-');
                    pDOB = foo[1]+'/'+foo[2]+'/' + foo[0]; 
                }
            }else{
                pDOB = 'N/A';
            } 
        } 
        
        if(bariatric.client_name__c!='Walmart'){
            empLabel ='Employee';
        }
       

    }
    
}