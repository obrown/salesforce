public with sharing class ECENOncologyCaseTravel{
    
    
    public static ecenPatientTravelStruct getOncologytavel(Oncology__c Oncology){
    
        ecenPatientTravelStruct travelStruct = new ecenPatientTravelStruct();
        system.debug(Oncology.id);
        if(Oncology.id==null){
            if(test.isRunningTest()){
                RestContext.response.responseBody= blob.valueof(JSON.serialize(travelStruct));
            }
            return travelStruct;
        }       
        
        boolean displayTravel=false;
        
        if(oncology.Evaluation_Travel_Type__c!='' && oncology.Evaluation_Estimated_Arrival__c!=null && oncology.Evaluation_Estimated_Departure__c!=null){
            displayTravel=true;
        }
        
        if(displayTravel){
            travelStruct.travelType = oncology.Evaluation_Travel_Type__c;
            if(travelStruct.travelType !=null && travelStruct.travelType.contains('Driving')){
                travelStruct.travelType = 'Driving';
            }
            
            if(oncology.Evaluation_Hotel__c!=null){
            
                travelStruct.hotel = new ecenPatientTravelStruct.hotel();
                travelStruct.hotel.label=oncology.Evaluation_Hotel__r.name;
            }
            
            // Hotel Info
            if(oncology.Evaluation_Hotel_Check_in_Date__c!=null){
                if(travelStruct.hotel==null){
                    travelStruct.hotel = new ecenPatientTravelStruct.hotel();
                    
                }
                
                travelStruct.hotel.checkin= oncology.Evaluation_Hotel_Check_in_Date__c.month()+'/'+oncology.Evaluation_Hotel_Check_in_Date__c.day()+'/'+oncology.Evaluation_Hotel_Check_in_Date__c.year();
                
            }
            
            if(oncology.Evaluation_Hotel_Check_Out_Date__c!=null){
              if(travelStruct.hotel==null){
                  travelStruct.hotel = new ecenPatientTravelStruct.hotel();
                    
              }
                
              travelStruct.hotel.checkout= oncology.Evaluation_Hotel_Check_Out_Date__c.month()+'/'+oncology.Evaluation_Hotel_Check_Out_Date__c.day()+'/'+oncology.Evaluation_Hotel_Check_Out_Date__c.year();
            
            }    
            
            //flight Info //|| oncology.Evaluation_Travel_Type__c=='Train'
            if(oncology.Evaluation_Travel_Type__c=='Flying' ){
            
                travelStruct.arrival = new ecenPatientTravelStruct.arrival();
                travelStruct.departure = new ecenPatientTravelStruct.departure();
            
            
                if(oncology.Evaluation_Arriving_Flight_Number__c==null){
                    travelStruct.arrival.recordNumber='Pending';
                }else{
                    travelStruct.arrival.recordNumber='#'+oncology.Evaluation_Arriving_Flight_Number__c;
                    
                }
                
                if(oncology.Evaluation_Departing_Flight_Number__c==null){
                    travelStruct.departure.recordNumber='Pending';
                }else{
                    travelStruct.departure.recordNumber='#'+oncology.Evaluation_Departing_Flight_Number__c;
                    
                }
                            
                if(oncology.Evaluation_Arriving_Flight_Date_and_Time__c==null){
                    travelStruct.arrival.displayDate='Pending';
                    travelStruct.arrival.displayTime='';
                    
                }else{
                    travelStruct.arrival.displayDate=oncology.Evaluation_Arriving_Flight_Date_and_Time__c.format('EEE')+', '+oncology.Evaluation_Arriving_Flight_Date_and_Time__c.format('MMM')+' '+oncology.Evaluation_Arriving_Flight_Date_and_Time__c.day()+' '+oncology.Evaluation_Arriving_Flight_Date_and_Time__c.year();
                    travelStruct.arrival.displayTime=utilities.formatTime(oncology.Evaluation_Arriving_Flight_Date_and_Time__c);
                }
                
                if(oncology.Evaluation_Departing_Flight_Date__c==null){
                    travelStruct.departure.displayDate='Pending';
                    travelStruct.departure.displayTime='';
                    
                }else{
                    travelStruct.departure.displayDate=oncology.Evaluation_Departing_Flight_Date__c.format('EEE')+', '+oncology.Evaluation_Departing_Flight_Date__c.format('MMM')+' '+oncology.Evaluation_Departing_Flight_Date__c.day()+' '+oncology.Evaluation_Departing_Flight_Date__c.year();
                    travelStruct.departure.displayTime=utilities.formatTime(oncology.Evaluation_Departing_Flight_Date__c);
                }
                
            }
            
            travelStruct.facility.label=oncology.client_facility__r.facility__r.name;
        
            ecenPatientTravelStruct.travelItem appointment = new ecenPatientTravelStruct.travelItem();
            if(oncology.First_Appointment_Date__c!=null){
            
            appointment = new ecenPatientTravelStruct.travelItem();
            appointment.label= 'First Appointment';
            appointment.displayDate= utilities.formatUSdate(oncology.First_Appointment_Date__c);
            if(oncology.First_Appointment_Time__c!=null){
                appointment.displayTime= oncology.Patient_First_AppointmentTime__c;
            }else{
                appointment.displayTime= 'NA';
            }
        
            travelStruct.facility.Appointments.add(appointment );  
        
            
            }
        
        
        if(oncology.Last_Appointment_Date__c!=null){
            appointment = new ecenPatientTravelStruct.travelItem();
            appointment.label= 'Last Appointment';
            appointment.displayDate= utilities.formatUSdate(oncology.Last_Appointment_Date__c);
            if(oncology.Patient_Last_AppointmentTime__c==null){
                appointment.displayTime= 'NA';
            }else{
                appointment.displayTime= oncology.Patient_Last_AppointmentTime__c;
            
            }
        
            travelStruct.facility.Appointments.add(appointment ); 
        }
        
        
        }
        system.debug(travelStruct);
        return travelStruct;
    
    }
    
}