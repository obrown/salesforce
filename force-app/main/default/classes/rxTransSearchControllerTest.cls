@isTest
private class rxTransSearchControllerTest {

    static testMethod void UnitTest() {
    
        rxTransSearchController ctrl = new rxTransSearchController();
        ctrl.rxFrom = '01/01/2016';
        ctrl.rxTo = '01/31/2016';
        ctrl.eSSN = '555746607';
        ctrl.uw = '008';
        
        ctrl.rxSearch();
        ctrl.getRxErrors();
        
        ApexPages.CurrentPage().getParameters().put('rxFieldSort','ClaimNbr__c');
        ctrl.sortByFieldVF();
        
        ctrl.excelDownload();
        ApexPages.CurrentPage().getParameters().put('data', EncodingUtil.urlEncode(JSON.serialize(ctrl.rxSearchResults.RxTxns), 'UTF-8'));
        rxTransSearchDownload dl = new rxTransSearchDownload();
    }
        
}