public class owtFuture{
    
    @future(callout=true)
    public static void sendImagePacFileRoute(set<id> theIds){
        date t = date.today();
        //and ImpagePacMessageSent__c =null
        Work_Task_Attachment__c[] alist = new Work_Task_Attachment__c[]{};            
        for(Work_Task_Attachment__c owt : [select Operations_Work_Task__r.attachToClaim__c, ImpagePacMessageSent__c,name, Operations_Work_Task__r.Patient_Lname__c,Operations_Work_Task__r.EESSN__c,Operations_Work_Task__r.Group__c, Operations_Work_Task__r.underwriter__c,Operations_Work_Task__r.HP_Document_Type__c from Work_Task_Attachment__c where Operations_Work_Task__c in: theIds ]){
            hpImpagePacStruct hpi = new     hpImpagePacStruct();
            hpi.Filename = owt.name;
            if(hpi.Filename!=''){
                if(hpi.Filename.length()<=4){
                    hpi.Filename = owt.Operations_Work_Task__r.Patient_Lname__c+'_'+hpi.Filename; 
                }
                hpi.Filename=hpi.Filename.tolowercase();
            }
            
            hpi.Filename = hpi.Filename.replace('.pdf','.tif');
            hpi.ReceivedDate = t.year()+''+utilities.formatDate(t.month())+''+utilities.formatDate(t.day());
            hpi.ScanDate = t.year()+''+utilities.formatDate(t.month())+''+utilities.formatDate(t.day());
            hpi.ServerDesc='';
            hpi.UserDesc=' '+hpi.Filename;
            hpi.EntityType='E';
            if(owt.Operations_Work_Task__r.HP_Document_Type__c =='AI'){
                hpi.ImageType='A';
            }else{
                hpi.ImageType='C';
            }
                        
            hpi.HpKeyPartA = owt.Operations_Work_Task__r.Underwriter__c;
            hpi.HpKeyPartB = owt.Operations_Work_Task__r.Group__c;
            hpi.HpKeyPartC = owt.Operations_Work_Task__r.EESSN__c;
            
            hp_ImagePacUpdate.sendImagePacUpdate(hpi);
            owt.ImpagePacMessageSent__c=date.today();
            alist.add(owt);    
        
        }
        update alist;
    }
   
   @future(callout=true)
   public static void DemoUpdateFromParentTask(set<id> theIds){
        Operations_Work_Task__c[] alist = new Operations_Work_Task__c[]{};            
        for(Operations_Work_Task__c owt : [select OWT_Parent__r.EESSN__c, Patient_Fname__c,Patient_Lname__c,Patient_Dob__c,Group__c,OWT_Parent__r.Patient_Fname__c,OWT_Parent__r.Patient_Lname__c,OWT_Parent__r.Patient_Dob__c,OWT_Parent__r.Group__c,OWT_Parent__r.Relationship__c,OWT_Parent__r.Underwriter__c,OWT_Parent__r.Related_Claim_Number__c from Operations_Work_Task__c where id in: theIds]){
           if(owt.OWT_Parent__r.EESSN__c!=null){
                        owt.EESSN__c  = owt.OWT_Parent__r.EESSN__c;
                        owt.Patient_Fname__c= owt.OWT_Parent__r.Patient_Fname__c;
                        owt.Patient_Lname__c= owt.OWT_Parent__r.Patient_Lname__c;
                        owt.Patient_Dob__c= owt.OWT_Parent__r.Patient_Dob__c;
                        owt.Group__c= owt.OWT_Parent__r.Group__c;
                        owt.Relationship__c= owt.OWT_Parent__r.Relationship__c;   
                        owt.Underwriter__c = owt.OWT_Parent__r.Underwriter__c ;
                        owt.Related_Claim_Number__c=owt.OWT_Parent__r.Related_Claim_Number__c;
                        
                alist.add(owt);    
            }
        
        }
        update alist;
    }  
    
    @future(callout=true)
   public static void updateDependentTaskDemoInfo(set<id> theIds){
        Operations_Work_Task__c[] alist = new Operations_Work_Task__c[]{};            
        for(Operations_Work_Task__c owt : [select Member_Number__c, OWT_Parent__r.Member_Number__c, EESSN__c, OWT_Parent__r.EESSN__c, Patient_Fname__c,Patient_Lname__c,Patient_Dob__c,Group__c,OWT_Parent__r.Patient_Fname__c,OWT_Parent__r.Patient_Lname__c,OWT_Parent__r.Patient_Dob__c,OWT_Parent__r.Group__c,OWT_Parent__r.Relationship__c,OWT_Parent__r.Underwriter__c,OWT_Parent__r.Related_Claim_Number__c from Operations_Work_Task__c where OWT_Parent__c in: theIds]){
           if(owt.OWT_Parent__r.EESSN__c!=null){
                        owt.EESSN__c  = owt.OWT_Parent__r.EESSN__c;
                        owt.Patient_Fname__c= owt.OWT_Parent__r.Patient_Fname__c;
                        owt.Patient_Lname__c= owt.OWT_Parent__r.Patient_Lname__c;
                        owt.Patient_Dob__c= owt.OWT_Parent__r.Patient_Dob__c;
                        owt.Group__c= owt.OWT_Parent__r.Group__c;
                        owt.Relationship__c= owt.OWT_Parent__r.Relationship__c;   
                        owt.Underwriter__c = owt.OWT_Parent__r.Underwriter__c ;
                        owt.Member_Number__c = owt.OWT_Parent__r.Member_Number__c;
                        owt.Related_Claim_Number__c=owt.OWT_Parent__r.Related_Claim_Number__c;
               alist.add(owt);    
           }
        
        }
        update alist;
    }  

   @future() 
   public static void calcNonBusinessDays(set<id> theIds){
       
       Operations_Work_Task__c[] owtList = [select nonBusinessDaysOpen__c, createdDate,Closed_Date__c from Operations_Work_Task__c where id in :theIds];
       
       for(Operations_Work_Task__c owt : owtList){
           integer daysBetweenClosed = date.valueof(owt.CreatedDate).daysBetween(date.valueof(owt.Closed_Date__c));
           system.debug('daysBetweenClosed  '+daysBetweenClosed );
           
           if(daysBetweenClosed>2){
               integer nonBusinessOpen=0;
               for(integer i=0; i<daysBetweenClosed;i++){
                   if(owt.CreatedDate.addDays(i).format('u')=='6' || owt.CreatedDate.addDays(i).format('u')=='7') {
                       nonBusinessOpen++;
                   }
    
               }
               owt.nonBusinessDaysOpen__c=integer.valueof(nonBusinessOpen);
           }else{
               owt.nonBusinessDaysOpen__c=0;
           }
       }
       
       update owtList;
   
   }   
   
   @future() 
   public static void EWTworktaskstatus(set<id> theIds){
       
       Operations_Work_Task__c[] wtsList = [select Work_Task_status__c ,EWT_Work_Task_Status__r.name from Operations_Work_Task__c where id in :theIds];
       
        for(Operations_Work_Task__c o : wtsList){
            
                o.Work_Task_Status__c = o.EWT_Work_Task_Status__r.name;
       
        }
        
        update wtslist;
   
   }        
    
}