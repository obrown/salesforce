public with sharing class meetingDetailext{
    
    id meetingID;
    Public Meeting__c Meeting {get; set;}
    
    public Participant[] theParticipants {get; set;}
    
    
    public meetingDetailext(ApexPages.StandardController controller) {
        meetingID = ApexPages.CurrentPage().getParameters().get('id');
        meeting = (meeting__c)Controller.getRecord();
        loadParticipants();
    }
    
    void loadParticipants(){
    
        theParticipants = new Participant[]{};
        
        for(Meeting_User__c m : [select user__r.Name, user__r.firstName, user__r.lastName, user__r.email,user__r.phone, user__r.MobilePhone, createdDate from Meeting_User__c where Meeting__c = :meetingID]){
            theParticipants.add(new Participant(m.user__r.Name, m.user__r.email,m.user__r.phone,m.user__r.MobilePhone, m.createdDate, 'HDP', m.id, m.user__c)); 
            
        }
        
        for(Meeting_Producer__c m : [select producer__r.First_Name__c, producer__r.Last_Name__c, producer__r.Mobile__c, producer__r.Phone__c,producer__r.Email__c,createdDate from Meeting_Producer__c where Meeting__c = :meetingID]){
            theParticipants.add(new Participant(m.producer__r.First_Name__c+' '+isNull(m.producer__r.Last_Name__c), m.producer__r.Email__c,m.producer__r.Phone__c,m.producer__r.Mobile__c, m.createdDate, 'Producer', m.id, m.Producer__c)); 
            
        }
        
    }
    
    string isNull(string theVal){
            
            if(theVal==null){
                return '';
            }
            
            return theVal;            
    }
    
    public void deleteProducer(){
        
        string theid = ApexPages.CurrentPage().getParameters().get('delID');
        string type = ApexPages.CurrentPage().getParameters().get('type');
        
        
        ApexPages.CurrentPage().getParameters().put('id', meeting.id);
        meetingParticipants mp = new meetingParticipants();
        
        ApexPages.CurrentPage().getParameters().put('delID',theid);
        ApexPages.CurrentPage().getParameters().put('type',type);
        
        
        mp.deleteParticipants();
        
        ApexPages.CurrentPage().getParameters().put('delID',null);
        ApexPages.CurrentPage().getParameters().put('type',null);
        /*
        if(type=='Producer'){
            Meeting_Producer__c mp = new Meeting_Producer__c (id=theid);
            
            delete mp;
        }
        
        if(type=='HDP'){
            Meeting_User__c mu = new Meeting_User__c(id=theid);
            delete mu;
        }
        */
        loadParticipants();
    }
    
    public string emailSubject {get; set;}
    public string emailBody {get; set;}
    
    public void sendMeetingNoticeEmail(){
        
        try{
        
        if(Meeting.Meeting_Date__c== null){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please add a meeting date'));
            return;     
            
        }
        
        if(Meeting.Start_Time_Hr__c == null || Meeting.Start_Time_Min__c == null || Meeting.Start_Time_AmPm__c == null){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please correct the meeting start time'));
            return;     
            
        }
        
        if(Meeting.End_Time_Hr__c== null || Meeting.End_Time_Min__c == null || Meeting.End_Time_AmPm__c== null){
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Please correct the meeting end time'));
            return;     
            
        }
        
        Blob vcsBlob;

        string vcs ='BEGIN:VCALENDAR\n';
        vcs +='VERSION:2.0\n';
        vcs +='METHOD:PUBLISH\n';
        vcs +='BEGIN:VEVENT\n';
        vcs +='CLASS:PUBLIC\n';
                    
        string nowUTC = createUTC(dateTime.now().date(),string.valueof(dateTime.now().hour()), string.valueof(dateTime.now().minute()), null);
        
        vcs +='CREATED:' + nowUTC + '\n';
        vcs +='DESCRIPTION:' + meeting.description__c + '\n';
        vcs +='DTSTAMP:' + nowUTC + '\n';
        vcs +='DTSTART:' + createUTC(Meeting.Meeting_Date__c, Meeting.Start_Time_Hr__c, Meeting.Start_Time_Min__c, Meeting.Start_Time_AmPm__c) + '\n'; //20160405T193000Z\n';
        vcs +='DTEND:' + createUTC(Meeting.Meeting_Date__c, Meeting.End_Time_Hr__c, Meeting.End_Time_Min__c, Meeting.End_Time_AmPm__c) + '\n';
        vcs +='LOCATION:' + meeting.location__c + '\n';
        vcs +='PRIORITY:5\n';
        vcs +='SEQUENCE:0\n';
        vcs +='SUMMARY;LANGUAGE=en-us:' + Meeting.Name + '\n';
        vcs +='TRANSP:OPAQUE\n';
        vcs +='X-MICROSOFT-CDO-BUSYSTATUS:BUSY\n';
        vcs +='X-MICROSOFT-CDO-IMPORTANCE:1\n';
        vcs +='BEGIN:VALARM\n';
        vcs +='TRIGGER:-PT15M\n';
        vcs +='ACTION:DISPLAY\n';
        vcs +='DESCRIPTION:' + meeting.name + '\n';
        vcs +='END:VALARM\n';
        vcs +='END:VEVENT\n';
        vcs +='END:VCALENDAR\n';

        vcsBlob = blob.valueof(vcs);
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.subject = emailSubject; 
        Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
        efa.setFileName(meeting.Name +'.ics');
        efa.setBody(vcsBlob);
        mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
        mail.setPlainTextBody(emailBody);
        string[] mailingList = new string[]{};    
            
        if(!Utilities.isRunningInSandbox()){
            mailingList.add(userInfo.getUserEmail());
            for(Participant p : theParticipants){
                if(p.email!=null && p.email.contains('@')){
                    mailingList.add(p.email);
                }
            }
        }else{
            
            mailingList.add(userInfo.getUserEmail());
            
            
        }
        
        mail.setToAddresses(mailingList);
        Messaging.sendEmail(new Messaging.SingleEmailMessage[]{mail});
        
        Meeting.Meeting_Notice_Sent__c = date.today();
        
        update Meeting;
        
        }catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
    }
    
    integer add12(integer theval, string meridiem){
    
        if(meridiem=='PM'){
            if(theval!=12){
                theval += 12;
            }
            
            return theval;
        }
    
        return theval;
    }
    
    string addZero(integer val){
    
        if(val<10){
            
            return '0'+string.valueof(val);
        }
        
        return string.valueof(val);
    }
    
    string createUTC(date meetingDate, string h, string m, string meridiem){
    
        DateTime dtc;
        
        if(meridiem==null){
            dtc = DateTime.newInstance(meetingDate.year(), meetingDate.month(), meetingDate.day(), integer.valueof(h), integer.valueof(m),0);
            
        }else{
            dtc = DateTime.newInstance(meetingDate.year(), meetingDate.month(), meetingDate.day(), add12(integer.valueof(h),meridiem), integer.valueof(m),0);
        }
        
        return dtc.year()+''+ addZero(dtc.month()) +''+addZero(dtc.day()) +'T'+addZero(dtc.timeGMT().hour())+''+addZero(dtc.minute())+'00Z';
        
    }

}