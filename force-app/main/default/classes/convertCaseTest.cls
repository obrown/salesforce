/**
* This class contains unit tests for validating the behavior of  the Apex convertCase class
*/

@isTest(seealldata=true)
private class convertCaseTest {

    static testMethod void convertCaseTest () {
        
        PatientCase__c recordType = PatientCase__c.getInstance();  
        
        recordType rt = [select id, name, developerName from recordType where name = 'Walmart Joint'];
        system.debug(rt);
        
        patient_case__c pc = new patient_case__c();
        pc.Status__c= 'Open';
        pc.Status_Reason__c= 'In process';
        
        pc.Employee_Last_Name__c='Test';
        pc.Employee_First_Name__c='Unit';
        pc.Employee_Gender__c= 'Male';
        pc.Employee_DOBe__c= '1950-01-01';
        pc.Employee_Street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c= 'AZ';
        pc.Employee_Zip_Postal_Code__c= '85212';
        pc.Employee_Mobile__c= '(480) 555-1212';
        pc.Employee_Home_Phone__c= '(480) 555-1212';
        pc.Employee_Work_Phone__c= '(480) 555-1212';
        
        pc.Patient_Last_Name__c='Test';
        pc.Patient_First_Name__c='Unit';
        pc.Patient_Gender__c= 'Male';
        pc.Patient_DOBe__c= '1950-01-01';
        pc.Patient_Street__c = '123 E Main St';
        pc.Patient_City__c = 'Mesa';
        pc.Patient_State__c= 'AZ';
        pc.Patient_Zip_Postal_Code__c= '85212';
        pc.Patient_Mobile__c= '(480) 555-1212';
        pc.Patient_Home_Phone__c= '(480) 555-1212';
        pc.Patient_Work_Phone__c= '(480) 555-1212';
        //pc.Relationship_to_the_Insured__c = 'Patient is Insured';
        pc.recordtypeid = recordType.wmJoint__c;
        pc.recordtype =rt;
        pc.client_name__c ='Walmart';
        //convertCase convert = new convertCase();
        
        insert pc;
        system.debug(pc.case_type__c);
        convertCase.addEligibity(pc);
        
        pc.Relationship_to_the_Insured__c = 'Patient is Insured';
        
        convertCase.addEligibity(pc);
        pc.recordtypeid = recordType.jetBlueSpine__c;
        pc.client_name__c ='jetBlue';
        convertCase.addEligibity(pc);
        
        pc.recordtypeid = recordType.lowesJoint__c;
        pc.client_name__c ='Lowes';
        convertCase.addEligibity(pc);
        
        convertCase.walmartRequiredfields(pc);
        
        Provider__c provider = new Provider__c(patient_case__c=pc.id);
        provider.Home_Transition_Care__c='Yes';
        //provider.Zip_Postal_Code__c='85212';
        //provider.State__c='AZ';
        //provider.city__c='Mesa';
        //provider.city__c='Mesa';
        
        insert provider;
        
        
        pc.Employee_DOBe__c= null;
        pc.Employee_Gender__c=null;
        pc.Employee_first_Name__c= null;
        pc.Employee_Last_Name__c=null;
        pc.Relationship_to_the_Insured__c=null;
        pc.Patient_DOBe__c=null;
        pc.Patient_Gender__c=null;
        pc.Employee_Street__c=null;
        pc.Employee_City__c=null;
        pc.Employee_State__c=null;
        pc.Patient_City__c=null;
        pc.Patient_Street__c=null;
        pc.Patient_State__c=null;
        pc.Patient_Work_Phone__c=null;
        pc.Patient_First_Name__c= null;
        pc.Patient_Last_Name__c=null;
        pc.Patient_Home_Phone__c=null;
        pc.Patient_Work_Phone__c=null;
        pc.Patient_Mobile__c=null;
       
        convertCase.walmartRequiredfields(pc);
        
        convertCase.transfertoCM(pc);
        
        pc.Employee_Last_Name__c='Test';
        pc.Employee_First_Name__c='Unit';
        pc.Employee_Gender__c= 'Male';
        pc.Employee_DOBe__c= '1950-01-01';
        pc.Employee_Mobile__c= '(480) 555-1212';
        pc.Employee_Home_Phone__c= '(480) 555-1212';
        pc.Employee_Work_Phone__c= '(480) 555-1212';
        pc.Employee_Country__c='USA';
        pc.Employee_Street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c= 'AZ';
        pc.Employee_Zip_Postal_Code__c= '85212';
        
        pc.Patient_Last_Name__c='Test';
        pc.Patient_First_Name__c='Unit';
        pc.Patient_Gender__c= 'Male';
        pc.Patient_DOBe__c= '1950-01-01';
        pc.Patient_Mobile__c= '(480) 555-1212';
        pc.Patient_Home_Phone__c= '(480) 555-1212';
        pc.Patient_Work_Phone__c= '(480) 555-1212';
        pc.Patient_Country__c='USA';
        pc.Patient_Email_Address__c='unittest@unittest.com';
        pc.Patient_Street__c = '123 E Main St';
        pc.Patient_City__c = 'Mesa';
        pc.Patient_State__c= 'AZ';
        pc.Patient_Zip_Postal_Code__c= '85212';
        pc.Relationship_to_the_Insured__c = 'Patient is Insured';
        
        convertCase.transfertoCM(pc); 
        convertCase.walmartPreLoader(pc.id, 'Walmart');
        
        
        pc.Employee_SSN__c='555121212';
        update pc;
        
        pc = [select case_type__c, Employee_SSN__c, coeOverride__c from patient_case__c where id = :pc.id];
        
        convertCase convert = new convertCase();
        pc.coeOverride__c=true;
        
        convert.sendReferralEmail(pc, 'test','test','test@test.com', new string[]{'test@test.com'});
        
        attachment a = new attachment(parentid=pc.id, body=blob.valueof('UNIT.TEST'),name='UNIT.TEST');
        
        convertCase.sendAuthEmail(pc, 'test','test','test@test.com', new string[]{'test@test.com'}, a);
        
        Transplant__c trans = new Transplant__c();
        trans.Patient_First_Name__c= 'Unit';
        trans.Patient_Last_Name__c= 'Test';
        trans.Intake_Status__c= 'Open';
        trans.Intake_Status_Reason__c= 'In process';
        trans.transplant_type__c= 'Bone Marrow'; 
        
        insert trans; 
        
        provider.parentid__c = trans.id;
        
        update provider;
        
        convertCase.walmartPreLoader(trans.id, 'trans');
          
    }

}