public with sharing class coeRecommendedScenarioTesting{

    public string selectedField {get; set;}
    public string fieldValue {get; set;}
    public string searchCaseName {get; set;}
    public coeRecommendedFacility coe {get; set;}
    
    public boolean isSelfAdminField{get; private set;}
    
    final string[] customFieldList = coeSelfAdminPickList.selfAdminList ;
    public string[] getcustomFieldList(){return customFieldList;}
    
    public boolean searchActiveRulesOnly {get; set;}
    
    selectOption[] PatientCaseFields, fieldValues;
    Patient_Case__c patientCase;
    public Patient_Case__c  getpatientCase(){return patientCase; }
    
    public string procedure {get; private set;}
    public string clientName {get; private set;}
    
    public set<string> selectedFieldList {get; private set;}
    public selectedField[] selectedFields {get; private set;}
    
    public string[] hotels {get; private set;}
    public string[] providers {get; private set;}
    public selectOption[] carriers {get; private set;}
    public selectOption[] employeeHealthPlanNames {get; private set;}
    
    public map<string, string[]> carrierMap {get; private set;}
    public set<string> carrierSet {get; private set;}
    
    public string[] facilities {get; private set;}
    public string recommendedRule {get; private set;}
    public string healthPacPlanCode {get; private set;}
    
    public boolean isError {get; private set;}
    string noReferredFacility = 'No Referred Facility Set';
    public client_facility__c cf {get; private set;}
    
    coeRecommendedFacility coeRecommendedFacility;
    
    class selectedField {
        
        public string fieldName {get; set;}
        public string fieldLabel {get; set;}
        public string fieldValue {get; set;}
        public string displayValue {get; set;}
        
    }
    
    Map<String, Schema.SObjectField> fieldMap = Schema.SObjectType.Patient_Case__c.fields.getMap();
    public string clientFacilityId {get; private set;}
    
    public coeRecommendedScenarioTesting(){
        
        patientCase = new patient_Case__c();
        selectedFieldList = new set<string>();
        selectedFields = new selectedField[]{};
        searchActiveRulesOnly=true;     
    }
    
    public selectOption[] getfieldValues(){
        return fieldValues;
    }
    
    public void setfieldValues(){
        
        fieldValues = new selectOption[]{};
        isSelfAdminField=false;
    
        if(selectedField==null){
            return;
        }
        
        fieldValues.add(new SelectOption('', ''));
        Schema.SObjectField field;
        
        try{
            field = fieldMap.get(selectedField);
        }catch(exception e){
            //catch silently
        }
        
        if(selectedField=='Ecen_Procedure__c'){
            set<string> pSet = new set<string>();
            for(client_facility__c c : [select procedure__c, procedure__r.name from client_facility__c where active__c =:searchActiveRulesOnly]){
                if(!pSet.contains(c.procedure__r.name)){
                    fieldValues.add(new SelectOption(c.procedure__c, c.procedure__r.name));
                    pSet.add(c.procedure__r.name);
                }
            }
            
            SelectOptionSorter.doSort(fieldValues, SelectOptionSorter.FieldToSort.Label); 
            return;
        }
        
        if(selectedField=='Client__c'){
            set<string> cnSet = new set<string>();
            for(client_facility__c c : [select client__c, client__r.name from client_facility__c where active__c =:searchActiveRulesOnly order by client__r.name desc]){
                
                if(!cnSet.contains(c.client__r.name)){
                    fieldValues.add(new SelectOption(c.client__c, c.client__r.name));
                    cnSet.add(c.client__r.name);
                }
            }
            
            //SelectOptionSorter.doSort(fieldValues, SelectOptionSorter.FieldToSort.Label); 
            return;
        }
        
        if(field!=null && selectedField!='CaseLatitudeLongitude__c' && !customFieldList.contains(selectedField)){
        
            schema.describefieldresult dfield = field.getDescribe();
            List<Schema.PicklistEntry> ple = dfield.getPicklistValues();
            
            for( Schema.PicklistEntry f : ple){
              fieldValues.add(new SelectOption(f.getLabel(), f.getValue()));
            }
            
        }else if(customFieldList.contains(selectedField)){
            isSelfAdminField=true;
            fieldValues = coeSelfAdminPickList.selectOptionList(patientCase, cf.client__c, cf.procedure__c, cf.id, selectedField, null, searchActiveRulesOnly);
            
        }
        
        SelectOptionSorter.doSort(fieldValues, SelectOptionSorter.FieldToSort.Label); 
        return;
    }
    
    string fieldType;
    public string getfieldType(){
    
        if(selectedField==null){
            return 'STRING';
        }
        
        if(selectedField=='Client__c' || selectedField=='Ecen_Procedure__c'){
            return 'PICKLIST';
        }
          
        try{
            Schema.SObjectField field;
            field = fieldMap.get(selectedField);
            schema.describefieldresult dfield = field.getDescribe();
            fieldType = string.valueof(dfield.getType());
            
        }catch(exception e){
            //catch silently
            fieldType='PICKLIST';
        }
        
        return fieldType;
    }
    
    set<string> doNotIncludeinFieldList(){
        set<string> foo = new set<string>();
        foo.add('CaseLatitudeLongitude__c');
        foo.add('Mileage__c');
        return foo;
    }
    
    public selectOption[] getPatientCaseFields(){
        coeLogicFields clf = new coeLogicFields(procedure);
        
        if(PatientCaseFields!=null){return PatientCaseFields;}
        PatientCaseFields = new selectOption[]{};
        PatientCaseFields.add(new selectOption('','--None--'));
        for(string fieldName : clf.clientFacilitySetupDisplayFields){
            
            Schema.SObjectField field = fieldMap.get(fieldName);
            schema.describefieldresult dfield = field.getDescribe();
            string type = string.valueof(dfield.getType());
            string fn = dfield.getname();
            // && (type== 'ENCRYPTEDSTRING' || type=='PICKLIST' || type =='STRING'|| type =='DOUBLE');
            
            if(!doNotIncludeinFieldList().contains(fieldName)){
                selectOption f = new selectOption(fieldName, dfield.getLabel());
                PatientCaseFields.add(f);
            }
        }

        SelectOptionSorter.doSort(PatientCaseFields, SelectOptionSorter.FieldToSort.Label);
        return PatientCaseFields;
        
    }
    
    string[] listOfFieldsFromCaseSearch(){
        coeLogicFields clfc = new coeLogicFields(procedure);
        set<string> flds= new set<string>();
        for(string clf : clfc.logicDisplayFields ){
            flds.add(clf);
            
        }
        for(string clf : clfc.clientFacilitySetupDisplayFields){
            flds.add(clf);
            
        }
        return new list<string>(flds);
    }
    
    public void searchforCase(){
        string querys ='select ';
        
        for(string fld : listOfFieldsFromCaseSearch()){
            querys = querys + fld +',';
            
        }
        
        querys = querys + 'client__r.name'; 
        //querys = querys.removeEnd(',');
        querys = querys + ' from patient_case__c where name =\'PC-'+ searchCaseName+ '\'';
        
        try{
            patientCase = database.query(querys);
        }catch(QueryException e){
            cf = new Client_facility__c();
            system.debug(e.getMEssage());
            return;
        }
            
        selectedFields = new selectedField[]{};
        
        for(string f : listOfFieldsFromCaseSearch()){
            if(!doNotIncludeinFieldList().contains(f)){
            selectedField = f;
            fieldValue = string.valueof(patientCase.get(f));
            addField();
            }
            
        }
        
        clientName = string.valueOf(patientCase.get('Client_Name__c'));
        procedure  = string.valueOf(patientCase.get('Program_Type__c'));
        
        runScenario();
    }
    
    public void addField(){
        
        selectedField sf = new selectedField();
        
        sf.fieldName = selectedField;
        sf.fieldLabel = fieldMap.get(selectedField).getDescribe().getLabel();
        sf.fieldValue = fieldValue;
        sf.displayValue = fieldValue;
        
        patientCase = (Patient_Case__c)setFormulafield(patientCase, sf.fieldName, sf.fieldValue);
        
        if((fieldValue InstanceOf ID)){
            String sobjectType = id.valueof(fieldValue).getSObjectType().getDescribe().getName();
            
            String searchQuery= 'select name from ' + string.escapeSingleQuotes(sobjectType) + ' where id = \'' +string.escapeSingleQuotes(fieldValue) +'\''; 
            sObject[] searchList = database.query(searchQuery);
            if(!searchList.isEmpty()){
                sf.displayValue = string.valueOf(searchList[0].get('Name'));        
            }
        }    
        
        
        selectedFields.add(sf);
        selectedFieldList.add(selectedField);
        selectedField =null;
        fieldValue =null;
    
    }
    
    public void removeField(){
        
        string fieldLabel = ApexPages.CurrentPage().getParameters().get('fieldLabel');    
        selectedField[] newList = new selectedField[]{};
        
        for(selectedField sf : selectedFields){
            if(sf.FieldLabel!=fieldLabel){
                newList.add(sf);
            }
            
        }
        
        selectedFields.clear();
        selectedFields  = newList.clone();
        
    }
    
    public void runScenario(){
        
        isError=false;
        boolean hasClient=false, hasProcedure=false;
        if(patientCase.name==null){
            
            
            
            for(selectedField sf : selectedFields){
                if(sf.fieldName=='Client__c'){
                    hasClient=true;
                }
                
                if(sf.fieldName=='Ecen_Procedure__c'){
                    hasProcedure=true;
                }
                try{
                   patientCase = (Patient_Case__c)setFormulafield(patientCase, sf.fieldName, sf.fieldValue);
                   
                }catch(exception e){
                    system.debug(e.getMessage());
                }
            }
        
        }
        
        if(!hasClient || !hasProcedure){
            recommendedRule = 'Client and Procedure are required to begin testing';
            return;
        }
        
        if(patientCase.get('Patient_City__c')!=null && patientCase.get('Patient_State__c')!=null&&patientCase.get('Patient_Zip_Postal_Code__c')!=null){
            string returnValue = geocodioGeoCoding.lookupAddress('',string.valueof(patientCase.get('Patient_City__c')),string.valueof(patientCase.get('Patient_State__c')),string.valueof(patientCase.get('Patient_Zip_Postal_Code__c')));
            //string.valueof(patientCase.get('Patient_Street__c'))       
                            try{
                                geocodio gc = (geocodio) System.JSON.deserialize(returnValue, geocodio.class);
                                patientCase = (Patient_Case__c)setFormulafield(patientCase, 'CaseLatitudeLongitude__c',  gc.results[0].location.lat+','+gc.results[0].location.lng); 
                            }catch(exception e){
                              system.debug(e.getMessage());
                            }    
        }
        
        
        coe = new coeRecommendedFacility();
        recommendedRule= coe.getRecommendedFacility(patientCase, string.valueof(patientCase.get('Client__c')),string.valueof(patientCase.get('Ecen_Procedure__c')),searchActiveRulesOnly).name;        

        if(recommendedRule==null){
            recommendedRule='No Recommended Facility Found';
            
        }
        
        

    }
    
    SObject setFormulafield(SObject sObj, String fieldName, Object value){
        String jsonString = JSON.serialize(sObj);
        Map<String,Object> dataMap = (Map<String,Object>)JSON.deserializeUntyped(jsonString);
        dataMap.put(fieldName, value);
        jsonString = JSON.serialize(dataMap);
        return (SObject)JSON.deserialize(jsonString, SObject.class);
    }

}