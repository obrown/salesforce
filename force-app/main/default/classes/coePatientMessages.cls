public with sharing class coePatientMessages{
    
    public ECEN_Message__c[] inbox {get; set;}
    public ECEN_Message__c[] outbox {get; set;} 
    public ECEN_Message__c message {get; set;}
    public integer unread {get;set;}
    public boolean isError {get; private set;}
    string pname;
    string pemail;
    string ins_id;
    string PatientCaseId;
    string caseType;
    
    public coePatientMessages(string ins_id, id PatientCaseId, string pname, string pemail){}
    
    public coePatientMessages(string ins_id, id PatientCaseId, string pname, string pemail,string caseType){
        this.ins_id = ins_id;        
        this.PatientCaseId= PatientCaseId;        
        this.pname= pname;
        this.pemail= pemail;
        this.caseType= caseType;
        loadMessages();
        
    }
    
    public void cancel(){
        loadMessages();
    }
    
    void loadMessages(){
        outbox = new ECEN_Message__c[]{};
        inbox  = new ECEN_Message__c[]{};   
        unread=0;
        for(ECEN_Message__c m : [select body__c,bariatric__c,
                                        createdby.name,
                                        dateReceived__c,dateSent__c,Date_Received__c,Date_Sent__c,
                                        follow_up__c,
                                        Patient_Case__c,Patient_Sent_Message__c,
                                        read__c,Recipient_Name__c,Recipient_Email__c,
                                        Sender_Email__c,Sender_Name__c,subject__c,sentMessage__c ,
                                        Unread__c from ECEN_Message__c where Insurance_Id__c like :ins_id order by createdDate desc]){
            if(m.Patient_Sent_Message__c){
                inbox.add(m);
                if(m.unread__c){
                    unread++;
                }
            }else{
                outbox.add(m);
            }
            
        }    
    }
    
    public void loadMessageVF(){
        loadMessage(ApexPages.CurrentPage().getParameters().get('pmid'));
    }
    
    void loadMessage(string messageId){
        
        message=null;
        for(ECEN_Message__c m : inbox){
            if(messageId==m.id){
            message = m;
            message.read__c = true;
            update message;
            loadMessages();
            break;
            }
        }
        if(message==null){
            for(ECEN_Message__c m : outbox){
                if(messageId==m.id){
                    message = m;
                    break;
                }
            }
        }
    }
    
    public void replyToMessageVF(){
        loadMessage(ApexPages.CurrentPage().getParameters().get('pmid'));
        string newBody = '\n\n\n\n-----Original Message-----\n';
        newBody += 'From: '+ message.sender_name__c + '[' + message.sender_email__c + ']\n';
        newBody += 'Sent: '+ utilities.formatDateTime(message.dateSent__c) +'\n';
        newBody += 'To: Member Advocate [MemberAdvocate@hdplus.com]\n';
        newBody += 'Subject: ' + message.subject__c +'\n\n';
        
        newBody += message.body__c;
        message = new ECEN_Message__c(Insurance_Id__c =ins_id, subject__c='RE: '+ message.subject__c , body__c=newBody);
        message.put(caseType, PatientCaseId);
        message.Recipient_Name__c =message.sender_name__c;
        message.Recipient_Name__c =message.sender_email__c;
        message.sender_name__c ='Member Advocate';
        message.sender_email__c ='MemberAdvocate@hdplus.com';
        
        
    }
    
    public void newMessageVF(){
        message = new ECEN_Message__c(Insurance_Id__c =ins_id);
        message.put(caseType, PatientCaseId);
        message.Recipient_Name__c =message.sender_name__c;
        message.Recipient_Name__c =message.sender_email__c;
        message.sender_name__c =pName;
        message.sender_email__c =pEmail;
    }
    
    public void saveMessageVF(){
        isError=false;
        
        if(message.subject__c!='' && message.body__c!=''){
            if(caseType=='phm_Patient__c'){
                message.sender_name__c = 'Case Manager';
                message.Sender_Email__c= 'memberadvocate@hdplus.com';
            }else{
                message.sender_name__c = 'Member Advocate';
                message.Sender_Email__c= 'memberadvocate@hdplus.com';
            }
            
            message.dateReceived__c = datetime.now();
            insert message;
            loadMessages();
                        
        }else{
            isError=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Subject and Body are required to send a message'));
        }
        
        
    }
    
}