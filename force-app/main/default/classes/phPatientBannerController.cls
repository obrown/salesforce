public with sharing class phPatientBannerController{
    
    ApexPages.StandardController controller;
    public phm_Patient__c patient {get; set;}
    public boolean apiError {get; set;}
    public boolean lazyLoadDone {get; set;}
    public string preferredPhone {get; set;}
    public string compPatientId {get; set;}
    public Case_Management_Note__c cmNote {get; private set;}
    
    public phPatientBannerController(ApexPages.StandardController c) {
        controller = c;
        patient = (phm_Patient__c)c.getRecord();
        
    }
    
    public phPatientBannerController(){
        loadPatientData();
        lazyLoadDone=true;
        apiError=false;
    }
    
    public void eligDetail(){
        apiError=false;
        lazyLoadDone=true;
        
        try{   
        
        if(compPatientId!=null && patient ==null){
            loadPatientData();
            
        }
        
        apiError=false;
        
        hpEligDetailStruct eligDetail = hp_eligSearch.searchHP(patient.Patient_Social_Security_Number__c, patient.Patient_Employer__r.Underwriter__c,patient.Employer_Group__c, patient.sequence__c, string.valueOf(date.today().addDays(-1)).remove('-'));
        eligDetail.cleanUp();
        
        patient.Medical_Status__c = utilities.decodeStatus(eligDetail.eligibilityDetail.Status);
        patient.Most_Recent_Plan_Number__c= eligDetail.eligibilityDetail.MedicalBenefitPlan;
        if(eligDetail.eligibilityDetail.EligDate!=null){
            string[] eDate = eligDetail.eligibilityDetail.EligDate.split('/');
            patient.Effective_Date__c = date.valueof(eDate[2]+'-'+eDate[0]+'-'+eDate[1]);
        }
        
        patient.Last_Date_of_Eligibility_Check__c = datetime.now();
        
        if(lazyLoadDone){
            update patient;     
            loadPatientData();
        }          
        
        lazyLoadDone=true;
        }catch(exception e){
            system.debug(e.getMessage()+' '+e.getLineNumber());
            lazyLoadDone=true;
            apiError=true;
        }
        
    }
    
    public void loadPatientData(){
        string pId;
        
        if(ApexPages.CurrentPage()==null && (compPatientId==null|| compPatientId=='')){return;}
        
        if(compPatientId!=null){
            pId=compPatientId;
        }else{
            if(ApexPages.CurrentPage()==null){return;}
            pId = ApexPages.CurrentPage().getparameters().get('patientRecordId');
            if(pId==null){return;}
        }
        
        patient = [select Date_of_Death__c,
                          Effective_Date__c,
                          Employer_Group__c,
                          Cert__c,
                          Gender__c,
                          Interpreter_services_are_required__c,
                          Language__c,
                          Last_Date_of_Eligibility_Check__c,
                          Medical_Status__c,
                          Most_Recent_Medical_Status_Date__c,
                          Most_Recent_Plan_Number__c,
                          Paid_Through_Date__c,
                          Patient_Age__c,
                          Patient_Date_of_Birth__c,
                          Patient_First_Name__c,
                          Patient_Last_Name__c,
                          Patient_Preferred_Name__c,
                          Preferred_Contact_Time__c,
                          Patient_Time_Zone__c,
                          Patient_Social_Security_Number__c,
                          Patient_Employer__r.Underwriter__c,
                          Patient_Employer__r.name,
                          Phone__c,
                          Phone2__c,
                          Phone3__c,
                          Phone4__c,
                          Phone_Type__c,
                          Phone_Type2__c,
                          Phone_Type3__c,
                          Phone_Type4__c,
                          PM_Note__c,
                          Preferred_Phone__c,
                          Preferred_Phone2__c,
                          Preferred_Phone3__c,
                          Preferred_Phone4__c,
                          Preferred_Call_Time__c,
                          Primary_Language__c,
                          Relationship_to_Subscriber__c,
                          Sequence__c,
                          Source_of_Date_or_Death__c,
                          Subscriber__c,
                          Subscriber_Date_of_Birth__c,
                          Subscriber_First_Name__c,
                          Subscriber_Last_Name__c,
                          Termination_Date__c,
                          timezoneAddress__c from phm_Patient__c where id = :pid];
                          
        if(patient.Patient_Date_of_Birth__c!=null){
            
            try{
               string[] dobArray = patient.Patient_Date_of_Birth__c.split('/');
               date dob = date.valueof(dobArray[2]+'-'+dobArray[0]+'-'+dobArray[1]); 
               patient.Patient_Age__c = dob.daysBetween(date.today())/365;
            }catch(exception e){}
        } 
        preferredPhone='';
        
        if(patient.Preferred_Phone3__c){
             preferredPhone=patient.Phone3__c;
             
        }else if(patient.Preferred_Phone2__c){
            preferredPhone=patient.Phone2__c;
            
        }else if(patient.Preferred_Phone__c){
            preferredPhone=patient.Phone__c;
            
        }else{
            preferredPhone='N/A';
        }  
        
          
        Case_Management_Note__c[] cmNotes = new Case_Management_Note__c[]{};
        try{
        cmNotes = [select Last_CM_Contact__c,Patient__c from Case_Management_Note__c where Patient__c=:pid order by createdDate desc];
        }catch(QueryException se){
        }    
                      
        If(cmNotes.size() >0){
            cmNote= cmNotes [0];
        }
        
    }  
    
}