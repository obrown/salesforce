@RestResource(urlMapping='/ecen/v1/messageread')
global with sharing class ECENMessageRead {


    @HttpPost
    global static string doPost() {
        string message_id = RestContext.request.headers.get('Mid');
        system.debug('message_id '+message_id);
        ECEN_Message__c message = new ECEN_Message__c(id=message_id,User_Read__c=true);
        update message;
        return '';
    }
}