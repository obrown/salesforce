/***************************************************

Mike notes 9/30: 

For the longer filter descriptions added:
filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');

Added feature to the page to expand and close filter breadcrumb

****************************************************/

public with sharing class oncologyQueuesController {
    
    public oncology__c[] all {get; set;}
    oncology__c[] programDeterminations {get; set;}
    public string filterBreadCrumb {get; set;}
    
    public oncologyQueuesController(){
        loadAll();
    }
    
    void loadAll(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c order by createdDate desc];
      filterBreadCrumb ='None';
    }
    
    void loadAmazon(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where client__r.name = 'Amazon' order by createdDate desc];
      filterBreadCrumb ='Client=Amazon';
    }

    /* This is terrible, loading by specific client */
    
    void loadFacebook(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where client__r.name = 'Facebook' order by createdDate desc];
      filterBreadCrumb ='Client=Facebook';
    }

    void loadSISC(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where client__r.name = 'SISC' order by createdDate desc];
      filterBreadCrumb ='Client=SISC';
    }
    
    void loadNextEraEnergy(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where client__r.name = 'NextEra Energy' order by createdDate desc];
      filterBreadCrumb ='Client=Facebook';
    }

    
    void thisMonth(){
      all = new oncology__c[]{};
      date d = Date.today().toStartOfMonth();
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where createdDate >= :d order by createdDate desc];
      filterBreadCrumb ='Created=This Month';
    }
    
    void loadPoc(){
        all = new oncology__c[]{};
        filterBreadCrumb ='Facility POCs';
    }
    void loadawaitingReferral(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where Status__c= 'Open' and Referral_Date__c=null and createdDate>=2020-08-25T14:00:00.000Z order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date=Blank';
    }    
    
    void  loadInfoPacketToBeSent(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where Status__c= 'Open'and Referral_Date__c!=null and Info_Packet_Sent__c=null and createdDate>=2020-08-25T14:00:00.000Z order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Info Packet Sent=Blank';
    }    
    
    void  loadMissingForms(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where Status__c= 'Open' and Info_Packet_Sent__c!=null and Date_HDP_Forms_Completed__c=null and createdDate>=2020-08-25T14:00:00.000Z order by createdDate desc];
      filterBreadCrumb='Status=Open, HDP Program Forms Completed = Blank, Info Packet Sent Not Blank';
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }

    void  loadWeeklyTouchBase(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where Status__c= 'Open'and Referral_Date__c!=null and Last_Patient_Contact_Days__c>7 and Evaluation_Actual_Departure__c=null and createdDate>=2020-08-25T14:00:00.000Z order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Last Patient Contact >7 days, Evaluation Actual Departure Date = Blank';
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }

    void  loadStipendCardToSend(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where (Status__c= 'Open' and Referral_Date__c!=null and Stipend_Card_Sent__c=null and Determination_Evaluation_Type__c='In-Person Evaluation' and createdDate>=2020-08-25T14:00:00.000Z) or (Treatment_Estimated_Arrival__c!=null and Determination_Evaluation_Type__c='Virtual Evaluation' and Status__c= 'Open' and Referral_Date__c!=null and Stipend_Card_Sent__c=null and createdDate>=2020-08-25T14:00:00.000Z)order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Stipend Card Sent= Blank, Determinatoin Evaluation Type=(In-Person Evaluation) or Treatment Estimated Arrival Not Blank, Determination Evaluation Type=(Virtual Evaluation), Referral Date Not Blank, Stipend Card Sent = Blank';
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }

    void  loadTravelRequest(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where (Status__c= 'Open' and Referral_Date__c!=null and Evaluation_Estimated_Arrival__c!=null and Evaluation_Estimated_Departure__c!=null and Evaluation_Travel_Request__c=null and Determination_Evaluation_Type__c='In-Person Evaluation' and createdDate>=2020-08-25T14:00:00.000Z) or (Treatment_Estimated_Arrival__c!=null and Treatment_Estimated_Departure__c!=null and Treatment_Travel_Request__c=null and Status__c= 'Open' and Referral_Date__c!=null and createdDate>=2020-08-25T14:00:00.000Z) order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Evaluation Estimated Arrival Not Blank, Evaluation Estimated Departure Not Blank, Evaluation Travel request= Blank, Determination Evaluation Type=(In-Person Evaluation) or status=Open, Referral date Not Blank, Treatment Estimated Arrival Not Blank, Treatment Estimated Departure not Blank, Treatment Travel Request = Blank';
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }
    
    void  loadTravelToEnter(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where (Status__c= 'Open' and Referral_Date__c!=null and Evaluation_Travel_Request__c!=null and Evaluation_Hotel__r.name=null and createdDate>=2020-08-25T14:00:00.000Z) or (Treatment_Travel_Request__c!=null and Treatment_Hotel__r.name=null and Status__c= 'Open' and Referral_Date__c!=null and createdDate>=2020-08-25T14:00:00.000Z) order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Evaluation Travel request Not Blank, Evaluation Hotel Not Blank or Status=Open, Referral Date Not Blank, Treatment Travel Request Not Blank, Treatment Hotel=Blank ';      
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }    

    void  loadMemberIDCardToSend(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where (Status__c= 'Open' and Referral_Date__c!=null and Evaluation_Estimated_Arrival__c!=null and Determination_Evaluation_Type__c='In-Person Evaluation' and Evaluation_Member_ID_Sent__c=null and createdDate>=2020-08-25T14:00:00.000Z) or (Treatment_Estimated_Arrival__c!=null and Treatment_Estimated_Departure__c!=null and Treatment_Member_ID_Sent__c=null and Status__c= 'Open' and Referral_Date__c!=null and createdDate>=2020-08-25T14:00:00.000Z) order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Evaluation Estimated Arrival/Departure Not Blank, Determination Evaluation Type=(In-Person Evaluation), Evaluation Member ID Sent = Blank or Status=Open, Referral Date Not Blank,  Treatment Estimated Arrival/Departure Not Blank, Treatment Member ID Sent=Blank ';            
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }

    void  loadTravelItinerarySentToFacility(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where (Status__c= 'Open' and Referral_Date__c!=null and Evaluation_Hotel__r.name!=null and Evaluation_Travel_Request__c!=null and Evaluation_Facility_Travel_Notification__c=null and createdDate>=2020-08-25T14:00:00.000Z) or (Treatment_Hotel__r.name!=null and  Treatment_Facility_Travel_Notification__c=null and Status__c= 'Open' and Referral_Date__c!=null and createdDate>=2020-08-25T14:00:00.000Z) order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Evaluation Hotel Not Blank, Evaluation Travel Request Not Blank, Evaluation Facility Travel Notification = Blank or Status=Open, Referral Date Not Blank, Treatment Hotel Not Blank, Treatment Facility Travel Notification = Blank';                  
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }

    void  loadTravelItinerarySentToMember(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where (Status__c= 'Open' and Referral_Date__c!=null and Evaluation_Hotel__r.name!=null and Evaluation_Travel_Request__c!=null and Evaluation_Patient_Travel_Notification__c=null and createdDate>=2020-08-25T14:00:00.000Z) or (Treatment_Hotel__r.name!=null and Treatment_Patient_Travel_Notification__c=null and Status__c= 'Open' and Referral_Date__c!=null and createdDate>=2020-08-25T14:00:00.000Z) order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Evaluation Hotel Not Blank, Evaluation Travel Request Not Blank, Evaluation Patient Travel Notification = Blank or Status=Open, Referral Date Not Blank, Treatment Hotel Not Blank, Treatment Patient Travel Notification = Blank';      
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');  
    }
    
    void  loadActualArrival(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where (Status__c= 'Open' and Referral_Date__c!=null and Evaluation_estimated_Arrival__c!=null and Evaluation_Estimated_Arival_Days__c>=0 and Evaluation_Actual_Arrival__c=null and createdDate>=2020-08-25T14:00:00.000Z) or (Treatment_Estimated_Arrival__c!=null and Treatment_Estimated_Arrival_Days__c>=0 and Treatment_Actual_Arrival__c=null and Status__c= 'Open' and Referral_Date__c!=null and createdDate>=2020-08-25T14:00:00.000Z) order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Evaluation Estimated Arrival <=Today, Evaluation Actual Arrival=Blank or Status=Open, Referral Date Not Blank, Treatment Estimated Arrival<=Today, Treatment Actual Arrival=Blank';       
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }

    void  loadActualDeparture(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where (Status__c= 'Open' and Referral_Date__c!=null and Evaluation_Estimated_Departure__c!=null and Evaluation_Estimated_Departure_Days__c>=0 and Evaluation_Actual_Departure__c=null and createdDate>=2020-08-25T14:00:00.000Z) or (Treatment_Estimated_Departure__c!=null and Treatment_Estimated_Departure_Days__c>=0 and Treatment_Actual_Departure__c=null and Status__c= 'Open' and Referral_Date__c!=null and createdDate>=2020-08-25T14:00:00.000Z) order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Evaluation Estimated Departure<=Today, Evaluation Actual Departure=Blank or Status=Open, Referral Date Not Blank, Treatment Estimated Departure<=Today, Treatment Actual Departure=Blank';      
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }

    void  loadPatientAtCOE(){
      all = new oncology__c[]{};
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where (Status__c= 'Open' and Referral_Date__c!=null and Evaluation_Actual_Arrival__c!=null and Evaluation_Actual_Departure__c=null and createdDate>=2020-08-25T14:00:00.000Z) or (Treatment_Actual_Arrival__c!=null and Treatment_Actual_Departure__c=null and Status__c= 'Open' and Referral_Date__c!=null and createdDate>=2020-08-25T14:00:00.000Z) order by createdDate desc];
      filterBreadCrumb='Status=Open, Referral Date Not Blank, Evaluation Actual Arrival Not Blank,Evaluation Actual Departure Is Blank or Status=Open, Referral Date Not Blank, Treatment Actual Arrival Not Blank, Treatment Actual Departure Is Blank';      
      filterBreadCrumb=filterBreadCrumb.replaceAll(',','<br/>').replaceAll(' or ','<br/>or<br/>');
    }    
    public void changeListView(){
      string lv = ApexPages.Currentpage().getParameters().get('lv');
      switch on lv{
          when 'programDetermination'{
              programDeterminations();
              return;
          }
          when 'all'{
              loadAll();
              return;
          }
          when 'poc'{
              loadPoc();
              return;
          }
          when 'amazon'{
              loadAmazon();
              return;
          }
          when 'facebook'{
              loadFacebook();
              return;
          }
          when 'nextera energy'{
              loadNextEraEnergy();
              return;
          }
          
          
          when 'thisMonth'{
              thisMonth();
              return;
          }

          when 'sisc'{
              loadSISC();
              return;
          }when 'awaitingreferral'{
              loadawaitingReferral();
              return;          
          }when 'infopackettobesent'{
              loadInfoPacketToBeSent();
              return;           
          }when 'missingforms'{
              loadMissingForms();
              return;           
          }when 'weeklytouchbase'{
              loadWeeklyTouchBase();
              return;          
          }when 'stipendcardtosend'{
              loadStipendCardToSend();
              return;          
          }when 'travelrequest'{
              loadTravelRequest();
              return;            
          }when 'traveltoenter'{
              loadTravelToEnter();
              return;              
          }when 'memberidcardtosend'{
              loadMemberIDCardToSend();
              return;           
          }when 'travelitinerarysenttofacility'{
              loadTravelItinerarySentToFacility();
              return;           
          }when 'travelitinerarysenttomember'{
              loadTravelItinerarySentToMember();
              return;          
          }when 'actualarrival'{
              loadActualArrival();
              return;          
          }when 'actualdeparture'{
              loadActualDeparture();
              return;           
          }when 'patientatcoe'{
              loadPatientAtCOE();
              return;            
          }           
          
      }
     
   } 
   
   void programDeterminations(){
      all = new oncology__c[]{};
      set<id> pdSet = new set<id>();
      for(Oncology_Clinical_Facility__c ocf : [select id,ParentId__c from Oncology_Clinical_Facility__c where Accepted_Status__c = '' or Accepted_Status__c='Pending']){
        pdSet.add(ocf.ParentId__c);      
      }
      all=[select id, client__r.name, patient_first_name__c, patient_last_name__c, createdbyid,createdDate, lastModifiedDate,lastModifiedByID, status__c,status_reason__c,Sub_Status_Reason__c  from Oncology__c where id in :pdset order by createdDate desc];  
   }

}