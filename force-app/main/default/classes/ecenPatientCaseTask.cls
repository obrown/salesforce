/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 12-22-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   12-22-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
public without sharing class ecenPatientCaseTask{
    
    
     ecenPatientInformation pinfo = new ecenPatientInformation();
     ecenDashBoardStruct dashboard = new ecenDashBoardStruct();
     ecenTaskList TaskList = new ecenTaskList();
        
     public ecenPatientInformation getPInfo(patient_case__c pc){
         
        pinfo.dashboard = dashboard;
        pinfo.dashboard.WelcomeMessage = 'Thank you for signing into the Contigo Health application.  In order to use this application, you must have an open case with Contigo Health created/open through your employer.    If you feel you have received this message in error, please call 888-463-3737.';
        pinfo.dashboard.ToDoItemsComplete='0%';
        pinfo.TaskList = TaskList;   
        
       
        if(pc.id==null){
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
           }
           return pinfo;
        }else{
          
            if(pc.program_type__c=='Joint' || pc.program_type__c=='Spine'){
                jointSpine(pc);
            }else if(pc.program_type__c=='Cardiac'){
               // cardiac(pc);
            }
            
            
            return pinfo;     
            
            }
        
     }
     
     private void jointSpine(patient_case__c pc){
         ecenDashBoardTimeline.timelineRibbonItem timelineRibbonItem= new ecenDashBoardTimeline.timelineRibbonItem();
         ecenDashBoardTimeline timelineRibbon= new ecenDashBoardTimeline();
            map<integer, ecenDashBoardTimeline.timelineRibbonItem> trMap = new map<integer, ecenDashBoardTimeline.timelineRibbonItem>();
            
            trMap.put(1, new ecenDashBoardTimeline.timelineRibbonItem(1, 0, 'Referral - 1st Step'));
            trMap.put(2, new ecenDashBoardTimeline.timelineRibbonItem(2, 0, 'Treatment Decision'));
            trMap.put(3, new ecenDashBoardTimeline.timelineRibbonItem(3, 0, 'Travel for Care'));
            
            ecenDashBoardStruct.milestone[] graphMilestones= new ecenDashBoardStruct.milestone[]{};
            
            //timelineRibbon && graphRibbon
            
            if(pc.converted_date__c!=null){
                timelineRibbonItem = trMap.get(1);
                timelineRibbonItem.Completed=1;
                trMap.put(1, timelineRibbonItem);
            }
            
            
            if(pc.Plan_of_Care_accepted_at_HDP__c!=null && pc.displayAuthNumber__c!=''){
                timelineRibbonItem = trMap.get(2);
                timelineRibbonItem.Completed=1;
                trMap.put(2, timelineRibbonItem);
            }
            
            timelineRibbon.items.add(trMap.get(1));
            timelineRibbon.items.add(trMap.get(2));
            
            
            //Welcome Message
            if(pc.client_facility__r.client__r.name=='Walmart'){
                dashboard.WelcomeMessage= 'Welcome to the Walmart Travel Surgical Program!';
            }else{
                dashboard.WelcomeMessage= 'Welcome to the Contigo Health ECEN Application!';
            }
           
            // Patient Tasks
            
            map<string, integer> tasksMap = new map<string, integer>();
            tasksMap.put('referral', 0);
            if(pc.converted_date__c!=null){
                tasksMap.put('referral', 1);
            
            }
            tasksMap.put('forms', 0);
            tasksMap.put('nicotine', 2);
            tasksMap.put('appointments', 0);
            tasksMap.put('booktravel', 0);
            integer isNicotine=0;
            
            if(pc.current_nicotine_user__c=='Yes'){
                tasksMap.put('nicotine', 0);
                isNicotine=1;
                if(pc.Patient_Cleared_to_Travel__c=='Yes' && (pc.Reason__c=='No Test Required' || pc.Reason__c=='Pass')){
                    tasksMap.put('nicotine', 1);
                }
                
            }

            if(pc.Program_Authorization_Received__c!=null && pc.Caregiver_Responsibility_Received__c!=null){
                tasksMap.put('forms', 1); //forms completed
            
            }
            
            if(pc.Plan_of_Care_accepted_at_HDP__c!=null && pc.displayAuthNumber__c!=''){
                tasksMap.put('appointments', 1);
                
            }
            
            if(pc.amex_trip_id__c!=null){
               tasksMap.put('booktravel', 1);
                
            }
            
            if(tasksMap.get('referral')==0){
                TaskList.Pending.add(new ecenTaskList.Task('Facility Referral', 1, 'CH'));         
            }else{
                TaskList.Completed.add(new ecenTaskList.Task('Referred to ' + pc.referred_facility__c, 1, 'Contigo Health ('+utilities.formatUSdate(pc.converted_date__c)+')'));                
                
            }
            
            if(tasksMap.get('forms')==0){
                TaskList.Pending.add(new ecenTaskList.Task('Complete Contigo Health Forms', 2, pc.patient_first_name__c+' '+pc.patient_last_name__c)); 
            }else{
                
                if(pc.Program_Authorization_Received__c<pc.Caregiver_Responsibility_Received__c){
                    TaskList.Completed.add(new ecenTaskList.Task('Contigo Health Forms Completed', 2, pc.patient_first_name__c+' '+pc.patient_last_name__c + ' ('+utilities.formatUSdate(pc.Caregiver_Responsibility_Received__c)+')'));
                }else{
                     TaskList.Completed.add(new ecenTaskList.Task('Contigo Health Forms Completed', 2, pc.patient_first_name__c+' '+pc.patient_last_name__c + ' ('+utilities.formatUSdate(pc.Program_Authorization_Received__c)+')'));
                }
            }
            if(isNicotine==1){
                
                if(tasksMap.get('nicotine')==0){
                    TaskList.Pending.Add(new ecenTaskList.Task('Complete Nicotine Testing', 3, pc.patient_first_name__c+' '+pc.patient_last_name__c));
                }else{
                    TaskList.Completed.Add(new ecenTaskList.Task('Nicotine Testing Completed', 3, pc.patient_first_name__c+' '+pc.patient_last_name__c + ' ('+utilities.formatUSdate(pc.converted_date__c)+')'));
                    
                }
            
            }
            
            if(tasksMap.get('appointments')==0){
                TaskList.Pending.Add(new ecenTaskList.Task('Confirm appointment Dates', 3, 'CH')); 
            }else{
                TaskList.Completed.Add(new ecenTaskList.Task('Appointments Confirmed', 3, 'Contigo Health ('+utilities.formatUSdate(pc.converted_date__c)+')'));
                
            }
            
            if(tasksMap.get('booktravel')==0){
                TaskList.Pending.Add(new ecenTaskList.Task('Book Travel with American Express', 4, pc.patient_first_name__c+' '+pc.patient_last_name__c));  
            }else{
                TaskList.Completed.Add(new ecenTaskList.Task('Travel Booked with American Express', 4, pc.patient_first_name__c+' '+pc.patient_last_name__c+ ' ('+utilities.formatUSdate(pc.converted_date__c)+')'));
                
                
            }
            
            integer toDoItemsComplete;
            
            if(TaskList.Pending.isEmpty()){
                toDoItemsComplete = 100;
                
                timelineRibbonItem = trMap.get(3);
                timelineRibbonItem.Completed=1;
                trMap.put(3, timelineRibbonItem);
                
            
                
            }else{
                decimal pnd = TaskList.Pending.size();
                toDoItemsComplete =  100-integer.valueof(pnd.divide(TaskList.Pending.size()+TaskList.Completed.size(),1)*100);
            }
            
            timelineRibbon.items.add(trMap.get(3));
            
            dashboard.ToDoItemsComplete = ToDoItemsComplete+'%';
            //End setting Dashboard
            
            //Set todo sortOrder list 
            if(!TaskList.Pending.isEmpty()){
                for(integer i=0; i<TaskList.Pending.size(); i++){
                    TaskList.pending[i].sortOrder = i+1;
                
                }
            
            }
            
            for(integer i=0; i<TaskList.Completed.size(); i++){
                    TaskList.Completed[i].sortOrder = i+1;
                
            }
            
            pinfo.dashboard = dashboard; 
            pinfo.TaskList = TaskList;
            pinfo.timelineRibbon = timelineRibbon;
            if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
            }
     }
     /*
     private void cardiac(patient_case__c pc){
         ecenDashBoardTimeline.timelineRibbonItem timelineRibbonItem= new ecenDashBoardTimeline.timelineRibbonItem();
         ecenDashBoardTimeline timelineRibbon= new ecenDashBoardTimeline();
            map<integer, ecenDashBoardTimeline.timelineRibbonItem> trMap = new map<integer, ecenDashBoardTimeline.timelineRibbonItem>();
            
            trMap.put(1, new ecenDashBoardTimeline.timelineRibbonItem(1, 0, 'Referral - 1st Step'));
            trMap.put(2, new ecenDashBoardTimeline.timelineRibbonItem(2, 0, 'Treatment Decision'));
            trMap.put(3, new ecenDashBoardTimeline.timelineRibbonItem(3, 0, 'Travel for Care'));
            
            ecenDashBoardStruct.milestone[] graphMilestones= new ecenDashBoardStruct.milestone[]{};
            
            //timelineRibbon && graphRibbon
            
            if(pc.converted_date__c!=null){
                timelineRibbonItem = trMap.get(1);
                timelineRibbonItem.Completed=1;
                trMap.put(1, timelineRibbonItem);
            }
            
            
            if(pc.Plan_of_Care_accepted_at_HDP__c!=null && pc.displayAuthNumber__c!=''){
                timelineRibbonItem = trMap.get(2);
                timelineRibbonItem.Completed=1;
                trMap.put(2, timelineRibbonItem);
            }
            
            timelineRibbon.items.add(trMap.get(1));
            timelineRibbon.items.add(trMap.get(2));
            
            
            //Welcome Message
            if(pc.client_facility__r.client__r.name=='Walmart'){
                dashboard.WelcomeMessage= 'Welcome to the Walmart Travel Surgical Program!';
            }else{
                dashboard.WelcomeMessage= 'Welcome to the Contigo Health ECEN Application!';
            }
           
            // Patient Tasks
            
            map<string, integer> tasksMap = new map<string, integer>();
            tasksMap.put('referral', 1);
            tasksMap.put('forms', 0);
            tasksMap.put('appointments', 0);
            tasksMap.put('booktravel', 0);
            
            if(pc.Program_Authorization_Received__c!=null && pc.Caregiver_Responsibility_Received__c!=null && pc.Financial_Responsibility_Waiver__c!=null){
                tasksMap.put('forms', 1); //forms completed
            
            }
            
            if(pc.Plan_of_Care_accepted_at_HDP__c!=null && pc.displayAuthNumber__c!=''){
                tasksMap.put('appointments', 1);
                
            }
            
            if(pc.amex_trip_id__c!=null){
               tasksMap.put('booktravel', 1);
                
            }
            
            if(tasksMap.get('referral')==0){
                TaskList.Pending.add(new ecenTaskList.Task('Facility Referral', 1, 'HDP'));         
            }else{
                TaskList.Completed.add(new ecenTaskList.Task('Referred to ' + pc.referred_facility__c, 1, 'Contigo Health ('+utilities.formatUSdate(pc.converted_date__c)+')'));                
                
            }
            
            if(tasksMap.get('forms')==0){
                TaskList.Pending.add(new ecenTaskList.Task('Complete Contigo Health Forms', 2, null)); 
            }else{
                date formsDate =pc.Caregiver_Responsibility_Received__c;
                if(pc.Program_Authorization_Received__c<pc.Caregiver_Responsibility_Received__c){
                    
                    if(pc.Caregiver_Responsibility_Received__c < pc.Financial_Responsibility_Waiver__c){
                        formsDate = pc.Financial_Responsibility_Waiver__c;
                    }
                    
                }else{
                     if(pc.Caregiver_Responsibility_Received__c < pc.Program_Authorization_Received__c){
                        formsDate = pc.Program_Authorization_Received__c;
                    }
                }
                
                TaskList.Completed.add(new ecenTaskList.Task('Contigo Health Forms Completed', 2, pc.patient_first_name__c+' '+pc.patient_last_name__c + ' ('+utilities.formatUSdate(formsDate)+')'));
            }
                        
            if(tasksMap.get('appointments')==0){
                TaskList.Pending.Add(new ecenTaskList.Task('Confirm appointment Dates', 3, 'HDP')); 
            }else{
                TaskList.Completed.Add(new ecenTaskList.Task('Appointments Confirmed', 3, 'Contigo Health ('+utilities.formatUSdate(pc.converted_date__c)+')'));
                
            }
            
            if(tasksMap.get('booktravel')==0){
                TaskList.Pending.Add(new ecenTaskList.Task('Book Travel with American Express', 4, pc.patient_first_name__c+' '+pc.patient_last_name__c));  
            }else{
                TaskList.Completed.Add(new ecenTaskList.Task('Travel Booked with American Express', 4, pc.patient_first_name__c+' '+pc.patient_last_name__c+ ' ('+utilities.formatUSdate(pc.converted_date__c)+')'));
                
                
            }
            
            integer toDoItemsComplete;
            
            if(TaskList.Pending.isEmpty()){
                toDoItemsComplete = 100;
                
                timelineRibbonItem = trMap.get(3);
                timelineRibbonItem.Completed=1;
                trMap.put(3, timelineRibbonItem);
                
            
                
            }else{
                decimal pnd = TaskList.Pending.size();
                toDoItemsComplete =  100-integer.valueof(pnd.divide(TaskList.Pending.size()+TaskList.Completed.size(),1)*100);
            }
            
            timelineRibbon.items.add(trMap.get(3));
            
            dashboard.ToDoItemsComplete = ToDoItemsComplete+'%';
            //End setting Dashboard
            
            //Set todo sortOrder list 
            if(!TaskList.Pending.isEmpty()){
                for(integer i=0; i<TaskList.Pending.size(); i++){
                    TaskList.pending[i].sortOrder = i+1;
                
                }
            
            }
            
            for(integer i=0; i<TaskList.Completed.size(); i++){
                    TaskList.Completed[i].sortOrder = i+1;
                
            }
            
            pinfo.dashboard = dashboard; 
            pinfo.TaskList = TaskList;
            pinfo.timelineRibbon = timelineRibbon;
            if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
            }     
     }
     */
     
}