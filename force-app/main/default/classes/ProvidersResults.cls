public with sharing class ProvidersResults {
    
    public list<Provider__c> prlist {get;set;}
    string pcID='';
   
    public ProvidersResults() {
    
    }
    /*
    string setheSearchString(){
        string searchString='http://www.HIPAASpace.com/api/npi/search?q=';
        searchString += 'profnamehere' + '&rt=json&token=2401A05054644C45B5832D4C1C031B323BE936FF27F8441586FE22FF60AD11C9'; 
        return searchString;
    }
    */
    
    public list<Provider__c> getProviders(string profname, string prolname, string city, string state ){
    //    HttpRequest req = new HttpRequest();
    //    string searchString = setheSearchString();
    //    system.debug(profname + ' ' + prolname +  ' name');
    //    searchstring = searchstring.replace('profnamehere', profname + '+' + prolname); // + ' ' + city + ', ' + state
    //    system.debug(searchstring);
    //    req.setEndpoint(searchString);
    //    req.setMethod('GET');
        
    //    Http http = new Http();
    //    HTTPResponse res = http.send(req);  
    //    string jsonString = res.getbody();
    //    system.debug(res.getStatus());
    //    system.debug(res.getbody());
        
        Provider__c[] tempproList = new Provider__c[]{};
        Provider__c[] proList = new Provider__c[]{};
        set<string> alreadyadded = new set<string>();
        
        for(Provider__c p:[select First_Name__c, Last_Name__c, Credentials__c, Type__c, Street__c, City__c, Zip_Postal_Code__c, Phone_Number__c, Fax_Number__c from Provider__c where first_name__c != null and last_name__c != null]){
            
            if(p.First_Name__c.tolowercase() == profname.tolowercase() && p.Last_Name__c.tolowercase() == prolname.tolowercase() && !alreadyadded.contains(profname.tolowercase()+prolname.tolowercase())){
                
                p.npi__c = 'dd';
                Provider__c TempPro = new Provider__c();
                TempPro = p;
                prolist.add(TempPro);
                alreadyadded.add(profname.tolowercase()+prolname.tolowercase());
                    
            }
            
        }
        /*
        Map<String, Object> jsonMap = (Map<String, Object>) JSON.deserializeUntyped(jsonString);
        for(Object proMap : (List<Object>)jsonMap.get('NPI')){
                Map<String, Object> pro = (Map<String, Object>)proMap;
                if(pro.get('FirstName')!=null && string.valueof(pro.get('MailingAddressStateName')).tolowercase()==state.tolowercase()){
                    Provider__c TempPro = new Provider__c();
                    
                    
                    tempPro.First_Name__c = camelCase(string.valueof(pro.get('FirstName')));
                    tempPro.Last_Name__c = camelCase(string.valueof(pro.get('LastName')));
                    tempPro.Credentials__c = string.valueof(pro.get('Credential'));
                    tempPro.Type__c= string.valueof(pro.get('Taxonomy1'));
                    tempPro.Street__c = string.valueof(pro.get('FirstLineMailingAddress'));
                    tempPro.City__c = string.valueof(pro.get('MailingAddressCityName'));
                    tempPro.State__c = string.valueof(pro.get('MailingAddressStateName'));
                    tempPro.Zip_Postal_Code__c = string.valueof(pro.get('MailingAddressPostalCode'));
                    tempPro.Phone_Number__c = string.valueof(pro.get('MailingAddressTelephoneNumber'));
                    tempPro.Fax_Number__c = string.valueof(pro.get('MailingAddressFaxNumber'));
                    tempPro.npi__c = string.valueof(pro.get('NPI'));
                    tempproList.add(tempPro);
                    
                }
        }
        
        tempproList = sortbystate(tempproList, profname.toLowerCase()+prolname.toLowerCase(), state);
        for(Provider__c p : tempProlist){
            prolist.add(p);
        }
        */
        return proList;
    }  
    
    public List<Provider__c > sortbystate(List<Provider__c > sortingList, string term, string state) {
        string thisTerm='';
        for (Integer i =0; i < sortingList.size(); i++) {
            
            for (Integer j = i; j > 0; j--) {
             thisTerm= sortingList[j-1].first_name__c + sortingList[j-1].last_name__c;
             thisTerm=thisTerm.tolowercase();   
                if(thisTerm == term || (sortingList[j-1].state__c > sortingList[j].state__c)){
                    Provider__c temp = sortingList[j];
                    sortingList[j] = sortingList[j-1];
                    sortingList[j-1] = temp;
                }
            }
        }
    
    return sortingList;
    
    }
    
    string camelCase(string foo){
     foo = foo.left(1).toUpperCase() + foo.right(foo.length()-1).toLowerCase();
     return foo;

    }
    
    }