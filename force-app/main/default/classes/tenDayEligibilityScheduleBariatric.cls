global class tenDayEligibilityScheduleBariatric implements Schedulable{
    
    global void execute(SchedulableContext go){
        tenDayEligibilityCheckBariatric tdec = new tenDayEligibilityCheckBariatric();
        tdec.run();
        
    }
    
}