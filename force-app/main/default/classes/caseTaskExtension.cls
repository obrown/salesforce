public with sharing class caseTaskExtension {
    
    public selectOption[] programOptions {get; private set;}
    public selectOption[] clientOptions {get; private set;}
    public String[] programValues {get; set;}
    
    public String[] clientValues {get; set;}
    set<string> programOptionSet = new set<string>();
    
    map<string, set<string>> clientProgramMap = new map<string, set<string>>();
    
    public Case_Task__c task {get; set;}
    final Case_Task__c[] initial_tasklist;
    
    ApexPages.StandardController c;
    
    public selectOption[] fieldOptions {get; private set;}
    set<string> fieldOptionSet = new set<string>();
    //public caseTaskExtension(ApexPages.StandardController controller) {}
    //public void setfieldOptions(){}
    //public pagereference saveTask(){return null;}
    //public pagereference cancelTask(){return null;}
    
// /*    
    public caseTaskExtension(ApexPages.StandardController controller) {
        this.c = controller;
        task = (Case_Task__c )c.getRecord();
        
        programOptions = new selectOption[]{};
        programOptions.add(new selectOption('None', 'Please select a client'));
        
        clientOptions = new selectOption[]{};
        clientOptions .add(new selectOption('', '--None--'));
        
        for(client_facility__c cf : [select id, Client__r.name, Procedure__r.name from client_facility__c where active__c = true order by Client__r.name asc, Procedure__r.name asc]){
            
            set<string> programOptions = clientProgramMap.get(cf.Client__r.name);
            
            if(programOptions==null){
               programOptions = new set<string>();
            }
            programOptions.add(cf.Procedure__r.name);
            clientProgramMap.put(cf.Client__r.name, programOptions);
        }
        for(string s : clientProgramMap.keyset()){
            clientOptions.add(new selectOption(s, s));
        }
        
        setfieldOptions();
        
        try{
            if(ApexPages.CurrentPage().getParameters().get('clone')=='true'){
                task = task.clone();
                task.client__c='';
                task.program__c='';
            }
        }catch(exception e){}
        
        if(task.id!=null){
            programValues=new string[]{};
            setProgramOptions();
            system.debug(task.procedure__c);
            programValues.add(task.procedure__c);
            setfieldOptions();
        }
    }
    public pagereference saveTask(){
        try{
            
           // system.debug(programValues);
            upsert task;
            
        }catch(exception e){
            return null;
        }
        
        return new PageReference('/' + task.id);
    }    
    public pagereference saveNewTask(){
        Case_Task__c[] new_tasklist = new Case_Task__c[]{};
        try{
            
            system.debug('programValues '+programValues);    
            
            
            for(string s : programValues){
                new_tasklist.add(new Case_Task__c(name=task.name, Description__c=task.Description__c,client__c=task.client__c,procedure__c=s, program__c=task.client__c+' '+s, field__c=task.field__c, Visible_to_Facility__c=task.Visible_to_Facility__c));
            }
            upsert new_tasklist;
            
        }catch(exception e){
            return null;
        }
        
        return new PageReference('/' + new_tasklist[0].id);
    }
    public pagereference cancelTask(){
        return new PageReference('/' + Case_Task__c.SObjectType.getDescribe().getKeyPrefix() + '/o');
    } 
    public void setProgramOptions(){
        
        if(task.client__c!=null){
            
            programOptions = new selectOption[]{};
            programOptions.add(new selectOption('None', '--None--'));
            for(string program : clientProgramMap.get(task.client__c)){
                programOptions.add(new selectoption(program,program));
            }
        }
    }   
    public void setfieldOptions(){
        //system.debug(programValues.isEmpty());
        if(task.id!=null){
            programValues=new string[]{task.procedure__c};
        }
        system.debug(programValues);
        
        if(programValues==null||programValues.contains('None')){
            fieldOptions = new selectOption[]{};
            fieldOptions.add(new selectOption('', 'Please choose a Program Type'));
            for(selectoption so : programOptions){
                so.setDisabled(false);
            }
            return;
        }
        
        fieldOptions = new selectOption[]{};
        fieldOptions.add(new selectOption('', '--None--'));
        Schema.DescribeSObjectResult pc_desc;
        system.debug(programValues);
        if(programValues.contains('Spine') || programValues.contains('Joint') || programValues.contains('Cardiac')){
           pc_desc  = Patient_Case__c.sObjectType.getDescribe(); 
           for(selectoption so : programOptions){
               string foo = so.getValue();
               if(!foo.contains('None') && !foo.contains('Spine') && !foo.contains('Joint') && !foo.contains('Cardiac')){
                   so.setDisabled(true);
               }
           } 
        }
        
        if(programValues.contains('Oncology')){
           pc_desc  = Oncology__c.sObjectType.getDescribe();  
           for(selectoption so : programOptions){
               string foo = so.getValue();
               if(!foo.contains('None') && !foo.contains('Oncology')){
                   so.setDisabled(true);
               } 
           } 
        }
        
        if(programValues.contains('Weight Loss Surgery')){
           pc_desc  = Bariatric__c.sObjectType.getDescribe();  
            for(selectoption so : programOptions){
               string foo = so.getValue();
               system.debug(foo);
               if(!foo.contains('None') && !foo.contains('Weight Loss Surgery')){
                   so.setDisabled(true);
               }
            }         
        }
        if(pc_desc==null){
            return;
        }
        Map<String, Schema.SObjectField> pc_fields = pc_desc.fields.getMap();
        Map<String, Schema.SObjectField> sorted_pc_fields = new Map<String, Schema.SObjectField>();
            
            list<String> sortList = new list<String>(pc_fields.keyset());
            sortList.sort();
            
            for(string key : sortList){
                sorted_pc_fields.put(key, pc_fields.get(key));
            }
            
            for(Schema.sObjectField fld : sorted_pc_fields.values()){ 
               //system.debug(fld.getDescribe().getType());
               if(fld.getDescribe().getType()==DisplayType.Date||fld.getDescribe().getType()==DisplayType.DateTime){
                   fieldOptions.add(new selectOption(fld.getDescribe().getName(),fld.getDescribe().getLabel()));
               }
            }
    }
//    */
    
}