public with sharing class ECENBariatricCaseTravel{
    
    
    public static ecenPatientTravelStruct getBariatricTavel(Bariatric_Stage__c bs){
    
        ecenPatientTravelStruct travelStruct = new ecenPatientTravelStruct();
        
        if(bs.id==null){
            if(test.isRunningTest()){
                RestContext.response.responseBody= blob.valueof(JSON.serialize(travelStruct));
            }
            return travelStruct;
        }       
        
        boolean displayTravel=false;
        
        
        if(bs.Travel_Type__c!='' && bs.Estimated_Arrival__c!=null && bs.Estimated_Departure__c!=null){
            displayTravel=true;
        }
        
        if(displayTravel){
            travelStruct.travelType = bs.Travel_Type__c;
            if(travelStruct.travelType !=null && travelStruct.travelType.contains('Driving')){
                travelStruct.travelType = 'Driving';
            }
            
            if(bs.Hotel__c!=''){
               
                travelStruct.hotel = new ecenPatientTravelStruct.hotel();
                travelStruct.hotel.label=bs.Hotel__c;
            }
            
            // Hotel Info
            if(bs.Hotel_Check_in__c!=null){
                if(travelStruct.hotel==null){
                    travelStruct.hotel = new ecenPatientTravelStruct.hotel();
                    
                }
                
                travelStruct.hotel.checkin= bs.Hotel_Check_in__c.month()+'/'+bs.Hotel_Check_in__c.day()+'/'+bs.Hotel_Check_in__c.year();
                
            }
            
            if(bs.Hotel_Check_out__c!=null){
              if(travelStruct.hotel==null){
                  travelStruct.hotel = new ecenPatientTravelStruct.hotel();
                    
              }
                
              travelStruct.hotel.checkout= bs.Hotel_Check_out__c.month()+'/'+bs.Hotel_Check_out__c.day()+'/'+bs.Hotel_Check_out__c.year();
            
            }    
            
            //flight Info
            if(bs.Travel_Type__c=='Flying' || bs.Travel_Type__c=='Train'){
            
                travelStruct.arrival = new ecenPatientTravelStruct.arrival();
                travelStruct.departure = new ecenPatientTravelStruct.departure();
            
            
                if(bs.Arriving_Flight_Number__c==null){
                    travelStruct.arrival.recordNumber='Pending';
                }else{
                    travelStruct.arrival.recordNumber='#'+bs.Arriving_Flight_Number__c;
                    
                }
                
                if(bs.Departing_Flight_Number__c==null){
                    travelStruct.departure.recordNumber='Pending';
                }else{
                    travelStruct.departure.recordNumber='#'+bs.Departing_Flight_Number__c;
                    
                }
                            
                if(bs.Arriving_Date_Time__c==null){
                    travelStruct.arrival.displayDate='Pending';
                    travelStruct.arrival.displayTime='';
                    
                }else{
                    travelStruct.arrival.displayDate=bs.Arriving_Date_Time__c.format('EEE')+', '+bs.Arriving_Date_Time__c.format('MMM')+' '+bs.Arriving_Date_Time__c.day()+' '+bs.Arriving_Date_Time__c.year();
                    travelStruct.arrival.displayTime=utilities.formatTime(bs.Arriving_Date_Time__c);
                }
                
                if(bs.Departing_Date_Time__c==null){
                    travelStruct.departure.displayDate='Pending';
                    travelStruct.departure.displayTime='';
                    
                }else{
                    travelStruct.departure.displayDate=bs.Departing_Date_Time__c.format('EEE')+', '+bs.Departing_Date_Time__c.format('MMM')+' '+bs.Departing_Date_Time__c.day()+' '+bs.Departing_Date_Time__c.year();
                    travelStruct.departure.displayTime=utilities.formatTime(bs.Departing_Date_Time__c);
                }
                
            }
            
            travelStruct.facility.label=bs.bariatric__r.client_facility__r.facility__r.name;
        
            ecenPatientTravelStruct.travelItem appointment = new ecenPatientTravelStruct.travelItem();
            
             if(bs.recordtype.name=='Pre'){
                if(bs.Evaluation_Date__c!=null){
                    Integer d = bs.Evaluation_Date__c.day();
                    Integer mo = bs.Evaluation_Date__c.month();
                    Integer yr = bs.Evaluation_Date__c.year();
                    
                    dateTime goo = DateTime.newInstance(yr, mo, d);
                    system.debug(goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year());
                    appointment = new ecenPatientTravelStruct.travelItem();
                    appointment.label= 'Evaluation';
                    appointment.displayDate= goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year();
                    travelStruct.facility.Appointments.add(appointment ); 
                }
                
               
             }else if(bs.recordtype.name=='Global'){
                
                if(bs.Estimated_Pre_Op__c!=null){
                    
                    Integer d = bs.Estimated_Pre_Op__c.day();
                    Integer mo = bs.Estimated_Pre_Op__c.month();
                    Integer yr = bs.Estimated_Pre_Op__c.year();
                    
                    dateTime goo = DateTime.newInstance(yr, mo, d);
                    system.debug(goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year());
                    appointment = new ecenPatientTravelStruct.travelItem();
                    appointment.label= 'Pre-op';
                    appointment.displayDate= goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year();
                    travelStruct.facility.Appointments.add(appointment );
                }
                
                if(bs.Estimated_Hospital_Admit__c!=null){
                    Integer d = bs.Estimated_Hospital_Admit__c.day();
                    Integer mo = bs.Estimated_Hospital_Admit__c.month();
                    Integer yr = bs.Estimated_Hospital_Admit__c.year();
                    
                    dateTime goo = DateTime.newInstance(yr, mo, d);
                    system.debug(goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year());
                    appointment = new ecenPatientTravelStruct.travelItem();
                    appointment.label= 'Hospital Admission';
                    appointment.displayDate= goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year();
                    travelStruct.facility.Appointments.add(appointment );
                
                }
                
                if(bs.Estimated_Procedure__c!=null){
                    Integer d = bs.Estimated_Procedure__c.day();
                    Integer mo = bs.Estimated_Procedure__c.month();
                    Integer yr = bs.Estimated_Procedure__c.year();
                    
                    dateTime goo = DateTime.newInstance(yr, mo, d);
                    system.debug(goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year());
                    appointment = new ecenPatientTravelStruct.travelItem();
                    appointment.label= 'Procedure';
                    appointment.displayDate= goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year();
                    travelStruct.facility.Appointments.add(appointment );
                
                }
                
                if(bs.Estimated_Hospital_Discharge__c!=null){
                    Integer d = bs.Estimated_Hospital_Discharge__c.day();
                    Integer mo = bs.Estimated_Hospital_Discharge__c.month();
                    Integer yr = bs.Estimated_Hospital_Discharge__c.year();
                    
                    dateTime goo = DateTime.newInstance(yr, mo, d);
                    system.debug(goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year());
                    appointment = new ecenPatientTravelStruct.travelItem();
                    appointment.label= 'Hospital Discharge';
                    appointment.displayDate= goo.format('EEE')+', '+goo.format('MMM')+' '+goo.day()+' '+goo.year();
                    travelStruct.facility.Appointments.add(appointment );
                }
                
                //appointment = new ecenPatientTravelStruct.travelItem();
                //appointment.label= 'First Appointment';
                //appointment.displayDate= utilities.formatUSdate(bs.Evaluation_Date__c);
               // travelStruct.facility.Appointments.add(appointment );  
             }
           
        }
        
        return travelStruct;
    
    }
    
}