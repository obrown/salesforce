public class geocodioGeoCoding{
    
    public static string lookupAddress(string a, string c, string s, string z){
        string address = EncodingUtil.UrlEncode(a,'UTF-8');
        string city = EncodingUtil.UrlEncode(c,'UTF-8');
        string state = EncodingUtil.UrlEncode(s,'UTF-8');
        string zip = EncodingUtil.UrlEncode(z,'UTF-8');
        
        string myParams = 'q='+address+','+city+'+'+state+','+zip;
        string requiredParams = '&api_key=88dd8a70c98a48d8a58c9055980a5888000008c';
        
        string urlparams = myParams+requiredParams;
        Http endpoint = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint('https://api.geocod.io/v1.3/geocode?'+urlparams);
        req.setMethod('GET');
        string returnValue;
        try{
            HttpResponse res = new HttpResponse();
            
            if(!Test.IsRunningTest()){
                res = endpoint.send(req); 
                if(res.getStatusCode()!=200){
                    system.debug(res.getbody());
                    return 'error';    
                }
                returnValue=res.getbody();
            }else{
                return '';
            }
            
        }catch(System.CalloutException e){
            system.debug(e.getMessage());
            return null;
        }
               
        
        return returnValue;
    }
}