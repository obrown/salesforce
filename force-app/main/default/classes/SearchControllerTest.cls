/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
*
* Unit tests are class methods that verify whether a particular piece
* of code is working properly. Unit test methods take no arguments,
* commit no data to the database, and are flagged with the testMethod
* keyword in the method definition.
*
* All test methods in an organization are executed whenever Apex code is deployed
* to a production organization to confirm correctness, ensure code
* coverage, and prevent regressions. All Apex classes are
* required to have at least 75% code coverage in order to be deployed
* to a production organization. In addition, all triggers must have some code coverage.
* 
* The @isTest class annotation indicates this class only contains test
* methods. Classes defined with the @isTest annotation do not count against
* the organization size limit for all Apex scripts.
*
* See the Apex Language Reference for more information about Testing and Code Coverage.
*/
@isTest
private class SearchControllerTest {

    static testMethod void myUnitTest() {
        
        
        Patient_Case__c pc = new Patient_Case__c();
        pc.Relationship_to_the_Insured__c = 'Patient is Insured';
        pc.Employee_First_Name__c = 'Johnny';
        pc.Employee_Last_Name__c = 'Test';
        pc.Employee_DOBe__c = string.valueof(date.today().addyears(-30));
        pc.Patient_DOBe__c = string.valueof(date.today().addyears(-30));
        pc.Employee_Gender__c = 'Male';
        pc.Employee_SSN__c = '123456789';
        pc.Same_as_Employee_Address__c = true;
        pc.Employee_Street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85206';
        pc.Employee_Home_Phone__c = '(480) 555-1212';
        pc.bid__c = '123456';
        pc.Client_Name__c = 'Walmart';
        
        insert pc;
        
        Account a = new Account();
        a.Employee_First_Name__c = 'Johnny';
        a.Employee_Last_Name__c = 'Test';
        a.name = 'Johnny Test';
        insert a;
        
        Contact c = new Contact();
        c.First_Name__c = 'Johnny';
        c.Last_Name__c = 'Test';
        c.FirstName = 'Johnny';
        c.LastName = 'Test';
        c.AccountId = a.id;        
        
        insert c;
        
        ApexPages.currentPage().getParameters().put('st', 'Johnny Test');
        ApexPages.currentPage().getParameters().put('sel', 'SearchAll');
        
        SearchController sc = new SearchController();
        sc.getsearchItems();
        
        sc.recID = pc.id;
        sc.DeletePC();
        
        sc.getisDeletePC();
        
        sc.getfoundPC();
        sc.getfoundAccts();
        sc.getfoundMembers();
       
        ApexPages.currentPage().getParameters().put('sel', 'pc'); 
        SearchController scpc = new SearchController();

        ApexPages.currentPage().getParameters().put('sel', 'trans'); 
        SearchController sctrans = new SearchController();                

        ApexPages.currentPage().getParameters().put('sel', 'coverage'); 
        SearchController sccoverage = new SearchController();            
        
        ApexPages.currentPage().getParameters().put('sel', 'member');
        SearchController scmember = new SearchController();
        
        SearchController scDUP = new SearchController(true, 'Johnny', 'Test'); 
    }
}