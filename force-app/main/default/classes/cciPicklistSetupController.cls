public with sharing class cciPicklistSetupController {
    
    public cciSelectlist isl {get; set;}
    public customerCareController ccc {get; set;}
    public string inquiryReasonId { get; set; }
    public string inquiryReasonName {get; set;}
    public string closedReasonId {get; set;}
    public string closedReasonName {get; set;}
    public string workDepartmentId { get; set; }
    public string workDepartmentName {get; set;}
    public string workTaskId { get; set; }
    public string workTaskName {get; set;}           
    public boolean error {get; private set;}
    
    public cciPicklistSetupController(){
        isl = new cciSelectlist();

    }
    
    
    public void setClosedReasonToUpdate(){
        try{
            closedReasonId = id.valueof(ApexPages.CurrentPage().getParameters().get('closedReasonId'));
            closedReasonName = ApexPages.CurrentPage().getParameters().get('closedReasonName');
        }catch(exception e){
            closedReasonName ='';
            closedReasonId =null;
        } 
         
    }
    
    public void setInquiryReasonToUpdate(){
        try{
            inquiryReasonId  = id.valueof(ApexPages.CurrentPage().getParameters().get('inquiryReasonId'));
            inquiryReasonName = ApexPages.CurrentPage().getParameters().get('inquiryReasonName');
        }catch(exception e){
            
            inquiryReasonName='';
            inquiryReasonId =null;
        } 
         
    }


    public void setWorkDepartmentToUpdate(){
        try{
            workDepartmentId  = id.valueof(ApexPages.CurrentPage().getParameters().get('workDepartmentId'));
            workDepartmentName = ApexPages.CurrentPage().getParameters().get('workDepartmentName');
        }catch(exception e){
            system.debug(e.getMessage());
            workDepartmentName='';
            workDepartmentId =null;
        } 
         
    }

    public void setWorkTaskToUpdate(){
        try{
            workTaskId  = id.valueof(ApexPages.CurrentPage().getParameters().get('workTaskId'));
            workTaskName = ApexPages.CurrentPage().getParameters().get('workTaskName');
        }catch(exception e){
            system.debug(e.getMessage());
            workTaskName='';
            workTaskId =null;
        } 
         
    }
        
    public void saveInquiryReason(){        
        
        error=false;
        try{
            
            CC_Inquiry_Reason__c ccIR = new CC_Inquiry_Reason__c(name=inquiryReasonName);
            if(inquiryReasonId!=null){
                ccIR.id=inquiryReasonId;
                
            }else{

            }
            
            try{
                upsert ccIR;
                isl.getTheInquiryReason();
                isl.inquiryReasonValue=ccIR.id;
            }catch(dmlException e){
                error=true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
                }  
            }
            
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
        
    }

    
    CC_Inquiry_Work_Department__c cciwd;
    public void saveWorkDepartment(){        
        
        error=false;
        try{
            
            cciwd= new CC_Inquiry_Work_Department__c (name=workDepartmentName);
            if(workDepartmentId!=null){
                cciwd.id=workDepartmentId;
                
            }else{
                //ewtWTR.EWT_Work_Tasks__c=esl.workTaskValue;
                //ccIR.EWT_Work_Tasks__c=esl.workTaskValue;
            }
            
            try{
                //upsert ewtWTR;
                upsert cciwd;
                //esl.getTheWorkTaskReason(); 
                
            }catch(dmlException e){
                error=true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
                }  
            }
            
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
        isl = new cciSelectlist();
    }    

    public void saveWorkTask(){        
        
        error=false;
        
        try{
            
            CC_Inquiry_Work_Task__c cciwt= new CC_Inquiry_Work_Task__c(name=workTaskName,CC_Inquiry_Work_Department__c=isl.workDepartmentValue);
            if(workTaskId!=null){
                cciwt.id=workTaskId;
            }
            
            try{
                upsert cciwt;
                isl.getTheworkTask();
                
                system.debug(isl.workTask);
                isl.workTaskValue=cciwt.id;
                
            }catch(dmlException e){
                error=true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
                } 
            
            }
            
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
        //isl = new cciSelectlist();
    }
        
    public void saveClosedReason(){
        error=false;
        
        try{
        
            CC_Closed_Reason__c cciCR = new CC_Closed_Reason__c(name=closedReasonName);
            if(closedReasonId!=null){
                cciCR.id=closedReasonId;
            }
            
            //ewtCR.EWT_Document_Type__c=esl.documentTypeValue;
            //ewtCR.EWT_Work_Task__c=esl.workTaskValue;
            //ewtCR.EWT_Work_Task_Reason__c=isl.inquiryReasonValue;
            //ewtCR.EWT_Work_Department__c=esl.workDepartmentValue;
            
            try{
                upsert cciCR;
                isl.getTheClosedReason();
            }catch(dmlException e){
                error=true;
                for (Integer i = 0; i < e.getNumDml(); i++) {
                    // Process exception here
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                    System.debug(e.getDmlMessage(i)+' '+e.getLineNumber() + ' ********************************************************* '); 
                }  
            }
            
            
        }catch(exception e){
            error=true;
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
        
    }
    
    public void removeEWTSO(){
        error=false;
        try{
            id ewtSoId = id.valueof(ApexPages.CurrentPage().getParameters().get('ewtSoId'));
            sObject so = Schema.getGlobalDescribe().get(string.valueof(ewtSoId.getSObjectType())).newSObject();
            so.id=ewtSoId;
            delete so;
            
            system.debug(string.valueof(ewtSoId.getSObjectType()));
            if(string.valueof(ewtSoId.getSObjectType())=='CC_Inquiry_Work_Task__c'){
                isl.getTheworkTask(); 
            }else if(string.valueof(ewtSoId.getSObjectType())=='CC_Inquiry_Work_Department__c'){
                isl.getTheworkDepartment();
               
            }else{
                isl.getTheInquiryReason();
                isl.getTheClosedReason();
            }
            
        }catch(exception e){
            error=true;
            system.debug(e.getMessage());
            system.debug(e.getLineNumber());
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()+' '+e.getLineNumber()));
        }
    }
    
}