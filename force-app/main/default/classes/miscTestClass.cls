/**
Using this to test the smaller classes:

attachFTPcontroller
codeController
googlemaps
newCase
LetterController
publicRemoteAction
searchlist
cloneCase
pNoteNew

 */
@isTest(SeeAllData=true)
private class miscTestClass {

    static testMethod void attachFTPcontroller()
    {
       attachFTPcontroller aftp = new attachFTPcontroller();
    }
    
    static testMethod void codeController()
    {
        test.startTest();
        Clinical_Codes__c theCC = [select code_type__c, 
                                       code_lookup__c,
                                       Patient_Case__c,
                                       code_lookup__r.name from Clinical_Codes__c limit 1];
       
        ApexPages.StandardController sc = new ApexPages.standardController(theCC);                             
        codeController cc = new codeController(sc);                            
        cc.getcTypeItems();                            
        cc.getOptions();
        string codelist = cc.getcodeList();
        cc.setcodeList(codeList);
        string codeType = cc.getcodeType();
        cc.setcodeType(codeType);
        cc.refreshCodeList();
        cc.mySave();
        test.stopTest();
    }  
    
    static testMethod void googlemaps()
    { 
        Patient_Case__c p = [select mileage__c,patient_street__c, Patient_City__c, Patient_State__c, Patient_Zip_Postal_Code__c,patient_country__c, ReferredFacilityAddress__c from Patient_case__c where isCOnverted__c = true and referredfacilityaddress__c != 'na' and referredfacilityaddress__c != null limit 1];
        
        string[] rfaddy = p.referredfacilityaddress__c.split(',,');     
        googlemaps.returnDistance(EncodingUtil.urlEncode(
                                            + p.patient_street__c + ' '
                                            + p.patient_city__c + ', '
                                            + p.patient_state__c + ' '
                                            + p.Patient_Zip_Postal_Code__c + ' '
                                            + p.Patient_Country__c,
                                            'UTF-8'), 
                                            EncodingUtil.urlEncode(
                                            + rfaddy[0] + ' '
                                            + rfaddy[1] + ', '
                                            + rfaddy[2] + ' '
                                            + rfaddy[3] + ' '
                                            + 'USA',
                                            'UTF-8'));
        
        //gm.getDistance();
        
        //string foo = ', destination_addresses : ,  6640 Alton Parkway, Irvine, CA 92618, USA , ,   origin_addresses : ,  Las Cruces, NM 88012, USA , ,   rows : ,      ,         elements : ,            ,               distance : ,                  text : 777 mi,,                  value : 1250948               , ,               duration : ,                  text : 11 hours 51 mins,                  value : 42651               , ,               status : OK            ,         ,      ,   , ,   status : OK, ';
        //gm.respjson = foo.split(', ');  
        //gm.getDistance();
        
    }
    
    static testMethod void newCaseTest()
    {
        newCase nc = new newCase();
        nc.getempnames();
    }
    
    static testMethod void LetterControllerTest()
    {
        
        Patient_Case__c p = [select patient_case__c, name, id from patient_case__c where isCOnverted__c = true and referredfacilityaddress__c != 'na' and referredfacilityaddress__c != null limit 1];
        ApexPages.StandardController sc = new ApexPages.standardController(p);
        LetterController lc = new LetterController(sc);
        lc.cancel();
        lc.save();
        
        ApexPages.currentPage().getParameters().put('pcid', p.id);
        LetterController lc1 = new LetterController();
    }
    
    static testMethod void publicRemoteActionTest()
    {
        
        Patient_Case__c p = [select patient_case__c, name, id from patient_case__c where isCOnverted__c = true and referredfacilityaddress__c != 'na' and referredfacilityaddress__c != null limit 1];
        publicRemoteAction.addForceNote(p.id, 'Test', 'Test');
        Clinical_Codes__c theCC = [select id from Clinical_Codes__c limit 1];
                                       
        publicRemoteAction.getAddonCharges(theCC.id);
        Clinical_Codes__c aoc = [select Add_On_Charges__c, Add_on_charges_approved__c, Billed_Add_On_Charges__c from Clinical_Codes__c  where id = :theCC.id];  
        publicRemoteAction.setAddonCharges(theCC.id, JSON.serialize(aoc));  
        
        ApexPages.StandardController sc = new ApexPages.standardController(p);          
        publicRemoteAction pra = new publicRemoteAction(sc);
                                   
    }
    
    static testMethod void searchlistTest()
    {
        searchlist sl = new searchlist();       
    }
        
    static testMethod void cloneCaseTest()
    {
        test.startTest();
        Patient_Case__c testP = new Patient_Case__c(
        client_name__c = 'Walmart',
        bid__c = '1234578',
        referral_source__c = 'Testing',
        current_nicotine_user__c = 'No',
        referred_facility__c = 'Virginia Mason',
        language_spoken__c = 'English',
        translation_services_required__c = 'No',
        recordtypeid = [select id from recordtype where name = 'Walmart Spine' and isactive = true limit 1].id,
        employee_first_name__c = 'James',
        employee_last_name__c = 'Martin',
        employee_dobe__c = '1980-02-22',
        employee_street__c = '123 E Main St',
        employee_city__c = 'Mesa',
        Employee_State__c = 'AZ',
        employee_zip_postal_code__c = '85297',
        employee_SSN__c = '123456789',
        relationship_to_the_insured__c = 'Spouse',
        patient_First_Name__c = 'Jane',
        patient_Last_Name__c = 'Martin',
        patient_dobe__c = '1980-02-22',
        patient_SSN__c = '987654321',
        actual_departure__c = date.today(),
        isConverted__c=true);
      
        insert testP; 
      
        cloneCase cc = new cloneCase();
        cc.CloneCase(testP.id,'Walmart Spine');
        test.stopTest();        
    }
    
    static testMethod void pNoteNewTest()
    {
        
        Patient_Case__c testP = new Patient_Case__c(
        client_name__c = 'Walmart',
        bid__c = '1234578',
        referral_source__c = 'Testing',
        current_nicotine_user__c = 'No',
        referred_facility__c = 'Virginia Mason',
        language_spoken__c = 'English',
        translation_services_required__c = 'No',
        recordtypeid = [select id from recordtype where name = 'Walmart Spine' and isactive = true limit 1].id,
        employee_first_name__c = 'James',
        employee_last_name__c = 'Martin',
        employee_dobe__c = '1980-02-22',
        employee_street__c = '123 E Main St',
        employee_city__c = 'Mesa',
        Employee_State__c = 'AZ',
        employee_zip_postal_code__c = '85297',
        employee_SSN__c = '123456789',
        relationship_to_the_insured__c = 'Spouse',
        patient_First_Name__c = 'Jane',
        patient_Last_Name__c = 'Martin',
        patient_dobe__c = '1980-02-22',
        patient_SSN__c = '987654321',
        actual_departure__c = date.today(),
        isConverted__c=true);
        testP.attachEncrypt__c = EncodingUtil.base64encode(Crypto.generateAesKey(128));
        insert testP; 
        
        Attachment a = [select name,body,parentid from Attachment where bodyLength < 1048576 limit 1];
        Attachment a1 = a.clone();
        a1.parentid = testP.id;
        insert a1;
        
        Program_Notes__c pn = new Program_Notes__c(subject__c='Test',Notes__c='Test',Patient_Case__c=testP.id,Communication_Type__c='Call',Initiated__c='Outbound'); 
        insert pn;
        
        createTransplant ct = new createTransplant();
        Transplant__c trans = ct.newTransplant(true); 
        
        Transplant_Notes__c tn = new Transplant_Notes__c(subject__c='Test',Notes__c='Test',Transplant__c=trans.id,type__c='test',Initiated__c='Outbound');
        insert tn;
        
        test.startTest();
        ApexPages.currentPage().getParameters().put('Id',testP.id);
        pNoteNew pnn = new pNoteNew(new ApexPages.standardController(testP));
        pnn.attachID = a1.id;
        pnn.encryptAttach();
        pnn.decryptAttach();
        
        pnn.setType('Other');  
        pnn.getType();
        pnn.getTypeItems();     
        pnn.getPNC();
        pnn.sortbySubject();
        pnn.sortmyListbyDate();
        pnn.sortmyListbyName();
        pnn.sortmyListbyType();
        pnn.getisDelete();
        pnn.myCancel();
        pnn.sf = 'none';
        pnn.filtertnc();
        
        pnn.sf = 'System';
        pnn.value = 'Patient Info Change';
        pnn.filtertnc();
        
        pnn.sf = 'System';
        pnn.value = 'Employee Info Change';
        pnn.filtertnc();
        
        pnn.sf = 'System';
        pnn.value = 'Status Change';
        pnn.filtertnc();
        
        pnn.sf = 'System';
        pnn.value = 'Transplant Type Change';
        pnn.filtertnc();
        
        pnn.sf = 'System';
        pnn.value = 'Carrier Info Change';
        pnn.filtertnc();
        
        pnn = new pNoteNew(new ApexPages.standardController(pn));
        pnn.getisEdit();
        
        pnn.theSubject = 'Test';
        pnn.theNotes = 'Test';
        pnn.Type = 'Test';
        pnn.isCCnote = false;
        
        pnn.mySave();
        pnn.mydelete();
        
        pnn = new pNoteNew(new ApexPages.standardController(trans));
        pnn = new pNoteNew(new ApexPages.standardController(tn));
        pnn.getTNC();
        
        pnn.theSubject = 'Test';
        pnn.theNotes = 'Test';
        pnn.Type = 'Test';
        pnn.isCCnote = false;
        
        pnn.mySave();
        pnn.mydelete();
        
        ApexPages.currentPage().getParameters().put('Id',testP.id);
        pnn = new pNoteNew();
        
        ApexPages.currentPage().getParameters().put('Id',trans.id);
        pnn = new pNoteNew();
                
        test.stopTest();        
    }
}