public with sharing class directBillingInvoiceWorkflow{
    
    map<string, Direct_Billing_Invoice__c[]> invoiceMap;
    
    public directBillingInvoiceWorkflow(){}
   
    public static set<id> createInvoices(map<id, date> leaveIds){
        map<id, decimal> newCharges = new map<id, decimal>();    
        map<string, DB_Pay_Schedule__c[]> payScheduleMap = new map<string, DB_Pay_Schedule__c[]>{};
        map<id, set<date>> leaveInvoicedPayDatesMap = new map<id, set<date>>();
        map<string, Direct_Billing_Invoice_Pay_Date__c[]>  newInvoicePayDates = new map<string, Direct_Billing_Invoice_Pay_Date__c[]>();
        map<id, Direct_Billing_Leave__c> leaveMap = new map<id, Direct_Billing_Leave__c>();
        map<id, decimal>  outstandingBalanceMap = new map<id, decimal>();
        map<id, date>  outstandingDatesMap = new map<id, date>();
        map<string, decimal> rateMap = new map<string, decimal>();
        
        for(DB_Rate_Tier__c drt: [select rate__c,name, db_rates__r.name,
                                         DB_Rates__r.client__c,
                                         DB_Rates__r.Start__c
                                         from DB_Rate_Tier__c where name !='Waive']){
            rateMap.put(drt.name+''+drt.db_rates__r.name+''+drt.DB_Rates__r.client__c+''+drt.DB_Rates__r.start__c.year(),drt.rate__c);                                         
        }
        
        for(DB_Pay_Schedule__c p : [select Pay_End__c,
                                           Pay_Date__c, 
                                           FSA_Only__c, 
                                           Recordtype.name from DB_Pay_Schedule__c where Pay_Date__c>:date.valueof('2019-01-01') and Do_not_invoice__c=false and Client__r.name='University Hospitals' order by Client__r.name, recordtype.name, Pay_Date__c desc]){
            
            DB_Pay_Schedule__c[] payDates = payScheduleMap.get(p.recordtype.name);
            
            if(payDates==null){
                payDates = new DB_Pay_Schedule__c[]{};
                
            }
            
            payDates.add(p);
            payScheduleMap.put(p.recordtype.name, payDates);
            
        }
        
        for(Direct_Billing_Invoice_Pay_Date__c ipd : [select db_invoice__c, db_invoice__r.db_leave__c, pay_date__c,pay_end__c from Direct_Billing_Invoice_Pay_Date__c where db_invoice__r.db_leave__c in :leaveIds.keySet()]){
            set<date> foo = leaveInvoicedPayDatesMap.get(ipd.db_invoice__r.db_leave__c);
            if(foo==null){
                foo = new set<date>();
                
            }
            foo.add(ipd.pay_date__c);
            leaveInvoicedPayDatesMap.put(ipd.db_invoice__r.db_leave__c, foo);
        }
       
        for(Direct_Billing_Leave__c l : [select db_member__r.current_balance__c,
                                                FSA_Election__c,
                                                FSA_Amount_Per_Pay__c,
                                                Identity_Theft_Election__c,
                                                Identity_Theft_Premium__c,
                                                Identity_Theft_Tier__c,
                                                Indemnity_Plan_Election__c,
                                                Indemnity_Plan_Premium__c,
                                                Indemnity_Plan_Tier__c,
                                                Legal_Plan_Election__c,
                                                Legal_Plan_Premium__c,
                                                Legal_Plan_Tier__c,
                                                Last_invoiced_Date__c,
                                                Life_and_AD_D_Premium__c,
                                                Life_and_AD_D_Election__c,
                                                Long_Term_Disability_Premium__c,
                                                Long_Term_Disability_Election__c,
                                                Critical_Illness_Amount_Per_Pay__c,
                                                Critical_Illness_Election__c,
                                                Spouse_Life_Election__c,
                                                Spouse_Life_Premium__c,
                                                Spouse_Life_Tier__c,
                                                Dependent_Life_Election__c,
                                                Dependent_Life_Premium__c,
                                                Dependent_Life_Tier__c,
                                                vision_premium__c,
                                                vision_election__c,
                                                Vision_Tier__c,    
                                                Medical_Parma_Election__c,
                                                Medical_Parma_Premium__c,
                                                Medical_Parma_Tier__c,
                                                Personal_Accident_Tier__c,
                                                Personal_Accident_Election__c,
                                                Personal_Accident_Amount_Per_Pay__c,
                                                dental_premium__c,
                                                dental_election__c,
                                                dental_tier__c,    
                                                medical_tier__c,
                                                medical_election__c,
                                                Monthly_Premium__c,
                                                outstanding_balance__c,
                                                current_balance__c,
                                                medical_premium__c,id,name, pay_frequency__c, start_date__c, end_date__c,Premiums_Total__c,fsa_premium__c from Direct_Billing_Leave__c where id in :leaveIds.keySet() ]){
            
            date endDate = leaveIds.get(l.id);
            if(endDate==null){endDate = l.end_date__c;}
            if(endDate==null){endDate=date.today().addMonths(1).toStartofMonth().addDays(-1);}
            decimal newCharge=0.00;
            
            for(DB_Pay_Schedule__c ps : payScheduleMap.get(l.pay_frequency__c)){
                
                if(ps.Pay_End__c >= l.start_date__c &&  ps.Pay_End__c <= endDate){
                    
                    leaveMap.put(l.id, l);
                    if(leaveInvoicedPayDatesMap.get(l.id)==null || !leaveInvoicedPayDatesMap.get(l.id).contains(ps.Pay_Date__c)){
                    
                        Direct_Billing_Invoice_Pay_Date__c[] foo = newInvoicePayDates.get(l.id);
                        if(foo==null){
                            foo = new Direct_Billing_Invoice_Pay_Date__c[]{};
                        }
                        
                        Direct_Billing_Invoice_Pay_Date__c newIPD = new Direct_Billing_Invoice_Pay_Date__c(pay_date__c=ps.Pay_Date__c,pay_end__c=ps.pay_end__c);
                        
                        if(ps.fsa_only__c){
                            if(l.fsa_election__c=='Flex'){  
                                newIPD.FSA_Premium__c=l.fsa_premium__c;
                                newCharge+=l.fsa_premium__c;
                            }else{
                                newIPD.FSA_Premium__c=0.00;
                            }
                        }else{
                            
                            decimal rate = rateMap.get(l.medical_tier__c+''+l.medical_election__c+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Medical_Premium__c=0.00;
                            }else{
                                newIPD.Medical_Premium__c=rate;
                                newCharge+=rate;
                            }
                           
                            rate = rateMap.get(l.Medical_Parma_Tier__c+''+l.Medical_Parma_Election__c+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Medical_Parma_Premium__c=0.00;
                            }else{
                                newIPD.Medical_Parma_Premium__c=rate;
                                newCharge+=rate;
                            }
                            
                            rate = rateMap.get(l.Dental_Tier__c+''+l.dental_election__c+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Dental_Premium__c=0.00;
                            }else{
                                
                                newIPD.Dental_Premium__c=rate;
                                newCharge+=rate;
                            }
                            
                            rate = rateMap.get(l.Vision_election__c+'Vision'+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Vision_Premium__c=0.00;
                            }else{
                                newIPD.Vision_Premium__c=rate;
                                newCharge+=rate;
                            }
                            
                            rate = rateMap.get(l.Personal_Accident_Election__c+'Personal Accident'+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Personal_Accident_Amount_Per_Pay__c=0.00;
                            }else{
                                newIPD.Personal_Accident_Amount_Per_Pay__c=rate;
                                newCharge+=rate;
                            }
                            
                            rate = rateMap.get(l.Spouse_Life_Election__c+'Spouse Life'+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Spouse_Life_Premium__c=0.00;
                            }else{
                                newIPD.Spouse_Life_Premium__c=rate;
                                newCharge+=rate;
                            }
                            
                            rate = rateMap.get(l.Dependent_Life_Election__c+'Dependent Life'+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Dependent_Life_Premium__c=0.00;
                            }else{
                                newIPD.Dependent_Life_Premium__c=rate;
                                newCharge+=rate;
                            }

                            rate = rateMap.get(l.Identity_Theft_Tier__c+''+l.Identity_Theft_Election__c+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Identity_Theft_Premium__c=0.00;
                            }else{
                                newIPD.Identity_Theft_Premium__c=rate;
                                newCharge+=rate;
                            }

                            rate = rateMap.get(l.Indemnity_Plan_Tier__c+''+l.Indemnity_Plan_Election__c+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Indemnity_Plan_Premium__c=0.00;
                            }else{
                                newIPD.Indemnity_Plan_Premium__c=rate;
                                newCharge+=rate;
                            }

                            rate = rateMap.get(l.Legal_Plan_Tier__c+''+l.Legal_Plan_Election__c+'University Hospitals'+ps.pay_date__c.year());
                            if(rate==null){
                                newIPD.Legal_Plan_Premium__c=0.00;
                            }else{
                                newIPD.Legal_Plan_Premium__c=rate;
                                newCharge+=rate;
                            }
                            
                            if(l.FSA_Premium__c==null){
                                newIPD.FSA_Premium__c=0.00;
                            }else{
                                newIPD.FSA_Premium__c=l.FSA_Premium__c;
                                newCharge+=l.FSA_Premium__c;
                            }
                            
                            if(l.Critical_Illness_Amount_Per_Pay__c==null){
                                newIPD.Critical_Illness_Amount_Per_Pay__c=0.00;
                            }else{
                                newIPD.Critical_Illness_Amount_Per_Pay__c=l.Critical_Illness_Amount_Per_Pay__c;
                                newCharge+=l.Critical_Illness_Amount_Per_Pay__c;
                            }
                            
                            if(l.Long_Term_Disability_Premium__c==null){
                                newIPD.Long_Term_Disability_Premium__c=0.00;
                            }else{
                                newIPD.Long_Term_Disability_Premium__c=l.Long_Term_Disability_Premium__c;
                                newCharge+=l.Long_Term_Disability_Premium__c;
                            }
                            
                            if(l.Life_and_AD_D_Premium__c==null){
                                newIPD.Life_and_AD_D_Premium__c=0.00;
                            }else{
                                newIPD.Life_and_AD_D_Premium__c=l.Life_and_AD_D_Premium__c;
                                newCharge+=l.Life_and_AD_D_Premium__c;
                            }
                            
                        }
                        
                        foo.add(newIPD);
                        newInvoicePayDates.put(l.id, foo);
                        newCharges.put(l.id, newCharge);
                        
                        
                    }   
                    
                }
                
            }
            
            outstandingBalanceMap.put(l.id, l.current_balance__c);
            outstandingDatesMap.put(l.id, l.Last_invoiced_Date__c);
        
        }
        
        Direct_Billing_Invoice__c[] newInvoices = new Direct_Billing_Invoice__c[]{};
        Direct_Billing_Invoice_Pay_Date__c[] newPayDateInvoices = new Direct_Billing_Invoice_Pay_Date__c[]{};
        Direct_Billing_Leave__c[] updateLeave = new Direct_Billing_Leave__c[]{};
        set<id> dupSetId = new set<id>();
        decimal newCharge=0.00;
        for(string leaveid : newInvoicePayDates.keySet()){
            boolean welcomeLetter=false;
            Direct_Billing_Invoice__c invoice = new Direct_Billing_Invoice__c(db_leave__c=leaveid.left(18));
            date duedate;
            date invoiceMonth;
            string payDatesIncluded='';
            Direct_Billing_Leave__c dbl = leaveMap.get(leaveid.left(18));
            boolean isLate=false;
            
            if(leaveInvoicedPayDatesMap.get(leaveid.left(18))==null){
                welcomeLetter=true;
                invoice.welcomeLetterNeeded__c=true;
                invoiceMonth= date.valueof(date.today().year()+'-'+date.today().month()+'-01');
                duedate= invoiceMonth.addMonths(1).toStartofMonth().addDays(-1);
                
                
            }else{
            
                //newInvoicePayDates.get(leaveId).size()-1
                Direct_Billing_Invoice_Pay_Date__c invoicePayDateMin = newInvoicePayDates.get(leaveId)[0];
                invoiceMonth = date.valueof(invoicePayDateMin.pay_end__c.year()+'-'+invoicePayDateMin.pay_end__c.month()+'-01');
                duedate = invoiceMonth.addMonths(1).toStartofMonth().addDays(-1);
                integer inv_year;
                decimal invoiceAmount=0.00;
                
            
            }
            
            for(Integer i = newInvoicePayDates.get(leaveid).size() - 1; i >= 0; i--){
                payDatesIncluded = payDatesIncluded + ', ' + newInvoicePayDates.get(leaveid)[i].pay_date__c.month()+'/'+newInvoicePayDates.get(leaveid)[i].pay_date__c.day()+'/'+newInvoicePayDates.get(leaveid)[i].pay_date__c.year(); 
            }
            
            if(outstandingBalanceMap.get(leaveid)>0.00){
                isLate=true;
                dbl.outstanding_balance__c=outstandingBalanceMap.get(leaveid)+newCharges.get(leaveId);
                invoice.pastDueLetterNeeded__c=isLate;
                invoice.terminationLetterNeeded__c=isLate;

            }else{
                isLate=false;
                dbl.outstanding_balance__c=0.00;
                invoice.pastDueLetterNeeded__c=false;
            }
  
            
            payDatesIncluded = payDatesIncluded.removeStart(', ');
            invoice.db_leave__c=leaveid.left(18);
            invoice.pay_dates_included__c=payDatesIncluded;
            invoice.invoice_amount__c = newCharges.get(leaveid)+outstandingBalanceMap.get(leaveid);
            invoice.Due_Date__c=dueDate;
            invoice.Previous_Invoice_Date__c=outstandingDatesMap.get(leaveid.left(18));
            invoice.Previous_Invoice_Amount__c=outstandingBalanceMap.get(leaveid.left(18));
            invoice.invoice_date__c = date.today();
            invoice.invoice_month__c=invoiceMonth;
            newInvoices.add(invoice);
            updateLeave.add(dbl);
            
        }
        
        upsert newInvoices;
        set<id> invoiceSet = new set<id>();
        for(Direct_Billing_Invoice__c inv : newInvoices){
           invoiceSet.add(inv.id);
                
           for(Direct_Billing_Invoice_Pay_Date__c npd: newInvoicePayDates.get(inv.db_leave__c)){
                npd.db_invoice__c=inv.id;
                
                newPayDateInvoices.add(npd);
            }
        }
        
        upsert newPayDateInvoices;
        update updateLeave;
        directBillingCreateAttachments dbca = new directBillingCreateAttachments();
        dbca.createInvoiceBatchCalls(invoiceSet);
        
        return invoiceSet;
        
    }       
    
    public static directBillingInvoiceStruct pastDueInvoices(date invoiceMonth, Direct_Billing_Leave__c[] leaves){
        
        Direct_Billing_Leave__c[] updateLeaves = new Direct_Billing_Leave__c[]{};
        Direct_Billing_Invoice__c [] pastDueInvoices = new Direct_Billing_Invoice__c []{};
        date dueDate = invoiceMonth.addMonths(1).toStartofMonth().addDays(-1);
        for(Direct_Billing_Leave__c leave : leaves){
            //Previous_Invoice_Amount__c=leave.Last_Invoiced_Amount__c        
            pastDueInvoices.add(new Direct_Billing_Invoice__c(db_leave__c=leave.id,Invoice_Month__c=invoiceMonth, pay_dates_included__c='NA', pastDueLetterNeeded__c=true, invoice_amount__c = leave.current_balance__c, Due_Date__c=dueDate, Previous_Invoice_Date__c=leave.Last_invoiced_Date__c, invoice_date__c = date.today()));
            updateLeaves.add(new direct_billing_leave__c(id=leave.id, outstanding_balance__c=leave.current_balance__c, Last_Invoiced_Amount__c=leave.current_balance__c));
        }
        
        return new directBillingInvoiceStruct(pastDueInvoices, updateLeaves);
    }

//5-11-20 update
    public static directBillingTermInvoiceStruct termInvoices(date invoiceMonth, Direct_Billing_Leave__c[] leaves){

        if(Test.isRunningTest()){
        //do work add data for test class 5-21-20
        }          
        Direct_Billing_Leave__c[] updateLeaves = new Direct_Billing_Leave__c[]{};
        Direct_Billing_Invoice__c [] termInvoices= new Direct_Billing_Invoice__c []{};
        date dueDate = invoiceMonth.addMonths(1).toStartofMonth().addDays(-1);
        for(Direct_Billing_Leave__c leave : leaves){
            termInvoices.add(new Direct_Billing_Invoice__c(db_leave__c=leave.id,Invoice_Month__c=invoiceMonth, pay_dates_included__c='NA', terminationLetterNeeded__c=true, invoice_amount__c = leave.current_balance__c, Due_Date__c=dueDate, Previous_Invoice_Date__c=leave.Last_invoiced_Date__c, Previous_Invoice_Amount__c=leave.Last_Invoiced_Amount__c, invoice_date__c = date.today()));
           // termInvoices.add(new Direct_Billing_Invoice__c(db_leave__c=leave.id,Invoice_Month__c=invoiceMonth, pay_dates_included__c='NA',invoice_amount__c = leave.current_balance__c, Due_Date__c=dueDate, Previous_Invoice_Date__c=leave.Last_invoiced_Date__c, Previous_Invoice_Amount__c=leave.Last_Invoiced_Amount__c, invoice_date__c = date.today()));            
            updateLeaves.add(new direct_billing_leave__c(id=leave.id, outstanding_balance__c=leave.current_balance__c, Last_Invoiced_Amount__c=leave.current_balance__c));
        }
        
        return new directBillingTermInvoiceStruct (termInvoices, updateLeaves);


        
    }
    
}