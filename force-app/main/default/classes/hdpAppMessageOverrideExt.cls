public without sharing class hdpAppMessageOverrideExt {
    
    private ApexPages.StandardController c;
    public HDP_App_Message__c ma {get; set;}
    public string applicationType {get; set;}
    public string[] mobileAppAlertContactList {get; private set;}
    public string clientLogo {get; private set;}
    
    Pattern emailRegEx = Pattern.compile('^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$');
    
    public hdpAppMessageOverrideExt(ApexPages.StandardController controller){
        c = controller;
        this.ma = (HDP_App_Message__c)controller.getRecord();
        applicationType = ma.application__c;
        clientLogo= '/logos/'+ma.client__r.logoDirectory__c;
        mobileAppAlertContactList = setContactList(ma.Mobile_App_Alert_Email_Contact__c);

    }
    
    string[] setContactList(string theFieldValue){
        
        string[] foo = new string[]{};
        
        if(theFieldValue==null||theFieldValue==''){
            return foo;    
        }
        
        for(string s : theFieldValue.split(',')){
            s = s.replaceAll(',','');
            if(s!=''){
                foo.add(s);
            }
        }
        
        return foo;
    }
    
    public void removeMobileAppAlertEmail(){
        string emailAddress = ApexPages.CurrentPage().getParameters().get('emailAddress');
        ApexPages.CurrentPage().getParameters().put('emailAddress', null);
        string mobileAppContact='';
        
        for(string s : mobileAppAlertContactList ){
            if(s!=emailAddress){
                mobileAppContact= mobileAppContact+ s + ','; 
            }
        }
        ma.Mobile_App_Alert_Email_Contact__c= mobileAppContact;
        try{
                upsert ma;
                mobileAppAlertContactList = setContactList(mobileAppContact);
            }catch(exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()));
            }
        
    }

    public void addMobileAppAlertEmail(){
        string emailAddress = string.escapeSingleQuotes(ApexPages.CurrentPage().getParameters().get('emailAddress'));
        emailAddress=emailAddress.trim();
        
        if(emailAddress!=null && emailAddress!=''){
        
            Matcher MyMatcher = emailRegEx.matcher(emailAddress);
            if(!MyMatcher.matches()){
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,'Invalid email address: '+emailAddress));
                return;
            }
            
            if(ma.Mobile_App_Alert_Email_Contact__c==null){ma.Mobile_App_Alert_Email_Contact__c='';}
            
            if(ma.Mobile_App_Alert_Email_Contact__c!= null && ma.Mobile_App_Alert_Email_Contact__c!='' && ma.Mobile_App_Alert_Email_Contact__c.indexOf(',')>0 && ma.Mobile_App_Alert_Email_Contact__c.right(1)!=','){
                ma.Mobile_App_Alert_Email_Contact__c= ma.Mobile_App_Alert_Email_Contact__c+',';    
            }
            
            
            ma.Mobile_App_Alert_Email_Contact__c= ma.Mobile_App_Alert_Email_Contact__c+ emailAddress+',';
            
            try{
                upsert ma;
                mobileAppAlertContactList = setContactList(ma.Mobile_App_Alert_Email_Contact__c);
            }catch(exception e){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getMessage()));
            }
            
            
            
        }
    }
    
    public void setapplicationType(){
       
        applicationType = ApexPages.Currentpage().getParameters().get('applicationType');
        system.debug('applicationType '+applicationType);
    }

    /*
    public void myClone(){
        HDP_App_Message__c theClone = ma.clone(false,true,false,false);
        ApexPages.StandardController con = new ApexPages.StandardController(theClone);
        //system.debug(con.getRecord());
        //return con.view();
    }
    */
}