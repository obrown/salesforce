public without sharing class umRequiredToApprove{
    
    public static boolean passValidationRules(Utilization_Management__c um, Utilization_Management_Clinical_Code__c[] diagnosisCodes){
        
        boolean isApproved = populationHealthUtils.isUmCaseApproved(um.case_status__c);
        boolean isDenied = populationHealthUtils.isUmCaseDenied(um.case_status__c);
        system.debug(isApproved +' '+isDenied);
        if((isApproved || isDenied) && um.Admission_Date__c==null){
            system.debug('Admission_Date__c');
            return false;
        }
        
        if(um.Approved_Through_Date__c < um.Admission_Date__c){
            system.debug('Admission_Date__c');
            return false;
        }
           
        if(um.recordtype.name =='Inpatient'){
          
          if(isApproved && um.Approved_Days__c<1){
            system.debug('Approved_Days__c');
            return false;
        } 
          
          if(isApproved && um.Approved_Through_Date__c==null){
              system.debug('Approved_Through_Date__c');      
              return false;
 
          }
            
          if(isApproved && um.Approved_Through_Date__c!=null && um.Admission_Date__c!=null ){
              integer days=um.Admission_Date__c.daysBetween(um.Approved_Through_Date__c.addDays(1));
              if(um.approved_days__c!=(days)){
                  system.debug('Approved_Days__c');      
                  return false;
              }
          }  
            
        }
        
        if((isApproved  || isDenied) && (um.Comment_Line_for_Claims__c==''||um.Comment_Line_for_Claims__c==null)){
            system.debug('Comment_Line_for_Claims__c');
            return false;
        }
             
        if(um.Event_Type__c==''||um.Event_Type__c==null){
            system.debug('Event_Type__c');
            return false;
        }      
          
        if(um.Facility_Name__c==''||um.Facility_Name__c==null){
            system.debug('Facility_Name__c');
            return false;
        }
        
        if(um.Facility_Type__c==''||um.Facility_Type__c==null){
            system.debug('Facility_Type__c');
            return false;
        }   
             
        if(um.Med_Cat__c==''||um.Med_Cat__c==null){
            system.debug('Med_Cat__c');
            return false;
        }  
               
        if((isApproved || isDenied) && (um.Quality_Codes__c==''||um.Quality_Codes__c==null)){
            system.debug('Quality_Codes__c');
            return false;
        }      

        if(um.Event_Type__c== 'HHC (HOME HEALTHCARE)' && um.visits__c<1 && um.case_status__c!='Denied'){
            system.debug('Event_Type__c');
            return false;
        } 

        if((isApproved || isDenied) && diagnosisCodes==null||diagnosisCodes.size()<1){
            system.debug('diagnosisCodes');
            return false; 
        }    
        return true;
    }
}