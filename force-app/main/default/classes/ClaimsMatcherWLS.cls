public with sharing class ClaimsMatcherWLS {
    
    date min_wls_actual_arrival=date.today();
    ClaimsReporting__c[] wls_claims = new ClaimsReporting__c[]{};
    set<string> claims_newly_paid;
    
    public void run(){ }
    
    public static void run(ClaimsReporting__c[] wls_claims, set<string> claims_newly_paid, boolean updateRecords){
         
         ClaimsMatcherWLS cm_wls = new ClaimsMatcherWLS();
         cm_wls.wls_claims =wls_claims;
         cm_wls.claims_newly_paid =claims_newly_paid;
         cm_wls.setmin_wls_actual_arrival(cm_wls.wls_claims);
         cm_wls.associateWLSClaims();
         if(updateRecords){
             cm_wls.save_wls_claims();
         }
    }
    
    void setmin_wls_actual_arrival(ClaimsReporting__c[] wls_claims){
        for(claimsReporting__c c : wls_claims){
            if(c.fdos__c<min_wls_actual_arrival ){
                 min_wls_actual_arrival =c.fdos__c;
            }
         }   
    }
    
    void save_wls_claims(){
        update wls_claims;
    }
    
    void associateWLSClaims(){
    
        map<string, Bariatric_Stage__c[]> wls_map = set_WLS_map();
        map<id, Bariatric__c> bariatric_updates = new map<id, Bariatric__c>(); //bariatric records to update
        Bariatric_Stage__c[] stages; //bariatric stage records; returning based on bid+last_name of bariatric record
        Bariatric_Stage__c[] updateStages = new Bariatric_Stage__c[]{}; //bariatric stage records tp update
        
        for(ClaimsReporting__c cr : wls_claims){
            
            if(claims_newly_paid.contains(cr.claim_number__c)){
                stages=wls_map.get(cr.claim_number__c);
               
                if(stages==null){
                    continue;
                }
                
                for(Bariatric_Stage__c bs : stages){
                    if(matchFound(bs, cr)){
                        bs.Claim_Received__c= cr.Date_Received__c;
                        bs.Claim_Paid__c= cr.Date_HP_Paid__c;
                        updateStages.add(bs);
                        
                        bariatric__c b = new bariatric__c(id=bs.bariatric__c);
                        b.status__c='Completed';
                        
                        switch on bs.recordtype.name.tolowercase(){
                            when 'global'{
                                b.status_reason__c ='Claim Paid- Global';
                            }
                            when 'post'{
                                b.status_reason__c ='Claim Paid- Post';
                            }
                        }
                        
                        bariatric_updates.put(b.id, b);
                        cr.Bariatric__c=bs.bariatric__r.name;
                        
                    }
                }
            }
        }
        
        update bariatric_updates.values();
        update updateStages;
    }
    
    boolean matchFound(bariatric_stage__c bs, ClaimsReporting__c cr){
        boolean returnValue =false;
        // (bs.Bariatric__r.status_reason__c.contains('Claim Pending') || bs.Bariatric__r.status_reason__c.contains('Claim Received') || bs.Bariatric__r.status_reason__c.contains('Claim Paid'))
        if( bs.recordtype.name.tolowercase() != 'pre' && cr.Paid__c> 0.00 &&
            (bs.Actual_Arrival__c.addDays(-4) <= cr.fdos__c && bs.Actual_Departure__c.addDays(4) >=cr.TDOS__c)){
            returnValue=true;              
        }                  
        return returnValue;
    }
    
    map<string, Bariatric_Stage__c[]> set_WLS_map(){
        
        map<string, Bariatric_Stage__c[]> wls_map = new map<string, Bariatric_Stage__c[]>();
        date min_wls_actual_arrival = min_wls_actual_arrival.addDays(-30);
        list<Bariatric_Stage__c> wls_stages = [select claim_number__c, recordtype.name, bariatric__r.name,Actual_Arrival__c,Actual_Departure__c,Bariatric__c, Bariatric__r.status__c, Bariatric__r.status_reason__c,Bariatric__r.bid__c, Bariatric__r.Relationship_to_the_Insured__c, Bariatric__r.patient_last_name__c,Bariatric__r.employee_last_name__c from Bariatric_Stage__c where Claim_NUmber__c in :claims_newly_paid and recordtype.name != 'Pre' and actual_arrival__c > :min_wls_actual_arrival];
        
        for(Bariatric_Stage__c bs : wls_stages){
            if (bs.Bariatric__r.bid__c!= null) {
                Bariatric_Stage__c[] stages = wls_map.get(bs.claim_number__c);
                if(stages==null){
                    stages= new Bariatric_Stage__c[]{};
                }
                stages.add(bs);
                wls_map.put(bs.claim_number__c, stages);
               
            }
        
        }
        
        return wls_map;
    }                    

}