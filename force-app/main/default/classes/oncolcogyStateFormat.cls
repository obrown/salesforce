public without sharing class oncolcogyStateFormat{

    public static string stateAbbrv(string state){
        if (state==null){return '';}
        state = state.touppercase().trim();
            
            If(state=='Alabama'){
                return 'AL';
            }
                        
            If(state=='Alaska'){
                return 'AK';
            }

            If(state=='Arizona'){
                return 'AZ';
            }

            If(state=='Arkansas'){
                return 'AR';
            }
            
            If(state=='California'){
                return 'CA';
            }                         
            
            If(state=='Colorado'){
                return 'CO';
            }
            
            If(state=='Connecticut'){
                return 'CT';
            }
            If(state=='Delaware'){
                return 'DE';
            }
            
            If(state=='Florida'){
                return 'FL';
            }
            
            If(state=='Georgia'){
                return 'GA';
            }
            
            If(state=='Hawaii'){
                return 'HI';
            }                                                                        

            If(state=='Idaho'){
                return 'ID';
            }            

            If(state=='Illinois'){
                return 'IL';
            }            

            If(state=='Indiana'){
                return 'IN';
            }           
            
            If(state=='Iowa'){
                return 'IA';
            }           
            
            If(state=='Kansas'){
                return 'KS';
            }
            
            If(state=='Kentucky'){
                return 'KY';
            }
            
            If(state=='Louisiana'){
                return 'LA';
            }   
            
            If(state=='Maine'){
                return 'ME';
            }
            
            If(state=='Maryland'){
                return 'MD';
            }   
            
            If(state=='Massachusetts'){
                return 'MA';
            }           
            
            If(state=='Michigan'){
                return 'MI';
            }           

            If(state=='Minnesota'){
                return 'MN';
            }           
            
            If(state=='Mississippi'){
                return 'MS';
            }           
            
            If(state=='Missouri'){
                return 'MO';
            }           
            
            If(state=='Montana'){
                return 'MT';
            }
            
            If(state=='Nebraska'){
                return 'NE';
            }
            
            If(state=='Nevada'){
                return 'NV';
            }

            If(state=='New Hampshire'){
                return 'NH';
            }

            If(state=='New Jersey'){
                return 'NJ';
            }

            If(state=='New Mexico'){
                return 'NM';
            }

            If(state=='New York'){
                return 'NY';
            }           

            If(state=='North Carolina'){
                return 'NC';
            }

            If(state=='North Dakota'){
                return 'ND';
            }

            If(state=='Ohio'){
                return 'OH';
            }

            If(state=='Oklahoma'){
                return 'OK';
            }

            If(state=='Oregon'){
                return 'OR';
            }

            If(state=='Pennsylvania'){
                return 'PA';
            }

            If(state=='Rhode Island'){
                return 'RI';
            }

            If(state=='South Carolina'){
                return 'SC';
            }

            If(state=='South Dakota'){
                return 'SD';
            }

            If(state=='Tennessee'){
                return 'TN';
            }
            
            If(state=='Texas'){
                return 'TX';
            }           
            
            If(state=='Utah'){
                return 'UT';
            }           
            
            If(state=='Vermont'){
                return 'VT';
            }           
            
            If(state=='Virginia'){
                return 'VA';
            }           
            
            If(state=='Washington'){
                return 'WA';
            }           

            If(state=='West Virginia'){
                return 'WV';
            }

            If(state=='Wisconsin'){
                return 'WI';
            }

            If(state=='Wyoming'){
                return 'WY';
            }                                                                       
    
        return '';
    }
    

    public static string stateFormal(string state){
        
        if (state==null){return '';}
        state = state.touppercase().trim();
            
            If(state=='AL'){
                return 'Alabama';
            }
                        
            If(state=='AK'){
                return 'Alaska';
            }

            If(state=='AZ'){
                return 'Arizona';
            }

            If(state=='AR'){
                return 'Arkansas';
            }
            
            If(state=='CA'){
                return 'California';
            }                         
            
            If(state=='CO'){
                return 'Colorado';
            }
            
            If(state=='CT'){
                return 'Connecticut';
            }
            If(state=='DE'){
                return 'Delaware';
            }
            
            If(state=='FL'){
                return 'Florida';
            }
            
            If(state=='GA'){
                return 'Georgia';
            }
            
            If(state=='HI'){
                return 'Hawaii';
            }                                                                        

            If(state=='ID'){
                return 'Idaho';
            }            

            If(state=='IL'){
                return 'Illinois';
            }            

            If(state=='IN'){
                return 'Indiana';
            }           
            
            If(state=='IA'){
                return 'Iowa';
            }           
            
            If(state=='KS'){
                return 'Kansas';
            }
            
            If(state=='KY'){
                return 'Kentucky';
            }
            
            If(state=='LA'){
                return 'Louisiana';
            }   
            
            If(state=='ME'){
                return 'Maine';
            }
            
            If(state=='MD'){
                return 'Maryland';
            }   
            
            If(state=='MA'){
                return 'Massachusetts';
            }           
            
            If(state=='MI'){
                return 'Michigan';
            }           

            If(state=='MN'){
                return 'Minnesota';
            }           
            
            If(state=='MS'){
                return 'Mississippi';
            }           
            
            If(state=='MO'){
                return 'Missouri';
            }           
            
            If(state=='MT'){
                return 'Montana';
            }
            
            If(state=='NE'){
                return 'Nebraska';
            }
            
            If(state=='NV'){
                return 'Nevada';
            }

            If(state=='NH'){
                return 'New Hampshire';
            }

            If(state=='NJ'){
                return 'New Jersey';
            }

            If(state=='NM'){
                return 'New Mexico';
            }

            If(state=='NY'){
                return 'New York';
            }           

            If(state=='NC'){
                return 'North Carolina';
            }

            If(state=='ND'){
                return 'North Dakota';
            }

            If(state=='OH'){
                return 'Ohio';
            }

            If(state=='OK'){
                return 'Oklahoma';
            }

            If(state=='OR'){
                return 'Oregon';
            }

            If(state=='PA'){
                return 'Pennsylvania';
            }

            If(state=='RI'){
                return 'Rhode Island';
            }

            If(state=='SC'){
                return 'South Carolina';
            }

            If(state=='SD'){
                return 'South Dakota';
            }

            If(state=='TN'){
                return 'Tennessee';
            }
            
            If(state=='TX'){
                return 'Texas';
            }           
            
            If(state=='UT'){
                return 'Utah';
            }           
            
            If(state=='VT'){
                return 'Vermont';
            }           
            
            If(state=='VA'){
                return 'Virginia';
            }           
            
            If(state=='WA'){
                return 'Washington';
            }           

            If(state=='WV'){
                return 'West Virginia';
            }

            If(state=='WI'){
                return 'Wisconsin';
            }

            If(state=='WY'){
                return 'Wyoming';
            }                                                                       
    
        return '';
    }


}