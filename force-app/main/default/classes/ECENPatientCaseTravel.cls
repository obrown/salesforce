public with sharing class ECENPatientCaseTravel{
    
    
    public static ecenPatientTravelStruct getPCtavel(patient_case__c pc){
        ecenPatientTravelStruct travelStruct = new ecenPatientTravelStruct();
        
        if(pc.id==null){
            if(test.isRunningTest()){
                RestContext.response.responseBody= blob.valueof(JSON.serialize(travelStruct));
            }
            return travelStruct;
        }       
        
         if(pc.program_type__c=='Joint' || pc.program_type__c=='Spine'){
             ECENPatientCaseTravel pct = new ECENPatientCaseTravel();
                travelStruct = pct.jointSpine(pc);
            }else if(pc.program_type__c=='Cardiac'){
             ECENPatientCaseTravel pct = new ECENPatientCaseTravel();
             //   travelStruct = pct.cardiac(pc);
            }
         
          if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(travelStruct));
           }
    
        return travelStruct;
    
    }
    
    public ecenPatientTravelStruct jointSpine(patient_case__c pc){
       ecenPatientTravelStruct travelStruct = new ecenPatientTravelStruct(); 
       
       boolean displayTravel=false;
        
        pc_poc__c poc = new pc_poc__c();
        try{
        poc =[select First_Appointment_Date__c,First_Appointment_Time__c, 
                     Hospital_Admission_Date__c,Hospital_Discharge_Date__c,
                     Last_Appointment_Date__c,Last_Appointment_Time__c,
                     Surgical_Procedure_Date__c,
                     Patient_Arrival_Date__c, Physician_Follow_up_Appointment_Date__c,Physician_Appointment_Time__c,
                     Return_Home_Travel_Date__c from pc_poc__c where patient_case__c = :pc.id];
        }catch(queryException e){}
        
        if(pc.Travel_Request__c!=null && pc.Estimated_Arrival__c!=null && pc.Estimated_Departure__c!=null){
            displayTravel=true;
        }
        
        if(displayTravel){
            travelStruct.travelType = pc.Travel_Type__c;
            if(travelStruct.travelType !=null && travelStruct.travelType.contains('Driving')){
                travelStruct.travelType = 'Driving';
            }
            
            if(pc.nHotel_Name__c!=null){
               
                travelStruct.hotel = new ecenPatientTravelStruct.hotel();
                travelStruct.hotel.label=pc.nHotel_Name__c;
            }
            
            
            // Hotel Info
            if(pc.Hotel_Check_In_Date__c!=null){
                if(travelStruct.hotel==null){
                    travelStruct.hotel = new ecenPatientTravelStruct.hotel();
                    
                }
                
                travelStruct.hotel.checkin= pc.Hotel_Check_In_Date__c.month()+'/'+pc.Hotel_Check_In_Date__c.day()+'/'+pc.Hotel_Check_In_Date__c.year();
                
            }
            
            if(pc.Hotel_Checkout_Date__c!=null){
              if(travelStruct.hotel==null){
                  travelStruct.hotel = new ecenPatientTravelStruct.hotel();
                    
              }
                
              travelStruct.hotel.checkout= pc.Hotel_Checkout_Date__c.month()+'/'+pc.Hotel_Checkout_Date__c.day()+'/'+pc.Hotel_Checkout_Date__c.year();
            
            }    
           
                      
            //flight Info
            if(pc.Travel_Type__c=='Flying' || pc.Travel_Type__c=='Train'){
            
                travelStruct.arrival = new ecenPatientTravelStruct.arrival();
                travelStruct.departure = new ecenPatientTravelStruct.departure();
            
            
                if(pc.Arriving_Flight_Number__c==null){
                    travelStruct.arrival.recordNumber='Pending';
                }else{
                    travelStruct.arrival.recordNumber='#'+pc.Arriving_Flight_Number__c;
                    
                }
                
                if(pc.Departing_Flight_Number__c==null){
                    travelStruct.departure.recordNumber='Pending';
                }else{
                    travelStruct.departure.recordNumber='#'+pc.Departing_Flight_Number__c;
                    
                }
                            
                if(pc.Arriving_Flight_Time__c==null){
                    travelStruct.arrival.displayDate='Pending';
                    travelStruct.arrival.displayTime='';
                    
                }else{
                    travelStruct.arrival.displayDate=pc.Arriving_Flight_Time__c.format('EEE')+', '+pc.Arriving_Flight_Time__c.format('MMM')+' '+pc.Arriving_Flight_Time__c.day()+' '+pc.Arriving_Flight_Time__c.year();
                    travelStruct.arrival.displayTime=utilities.formatTime(pc.Arriving_Flight_Time__c);
                }
                
                if(pc.Departing_Flight_Time__c==null){
                    travelStruct.departure.displayDate='Pending';
                    travelStruct.departure.displayTime='';
                    
                }else{
                    travelStruct.departure.displayDate=pc.Departing_Flight_Time__c.format('EEE')+', '+pc.Departing_Flight_Time__c.format('MMM')+' '+pc.Departing_Flight_Time__c.day()+' '+pc.Departing_Flight_Time__c.year();
                    travelStruct.departure.displayTime=utilities.formatTime(pc.Departing_Flight_Time__c);
                }
                
            
                
        }
        
        travelStruct.facility.label=pc.client_facility__r.facility__r.name;
        
        ecenPatientTravelStruct.travelItem appointment = new ecenPatientTravelStruct.travelItem();
        appointment.label= 'Admission';
        appointment.displayDate= utilities.formatUSdate(poc.Hospital_Admission_Date__c);
        travelStruct.facility.Appointments.add(appointment);  
        
        appointment = new ecenPatientTravelStruct.travelItem();
        appointment.label= 'First Appointment';
        appointment.displayDate= utilities.formatUSdate(poc.First_Appointment_Date__c);
        if(poc.First_Appointment_Time__c!=null){
            appointment.displayTime= poc.First_Appointment_Time__c;
        }else{
            appointment.displayTime= 'NA';
        }
        
        travelStruct.facility.Appointments.add(appointment );  
        
        appointment = new ecenPatientTravelStruct.travelItem();
        appointment.label= 'Surgical Procedure';
        appointment.displayDate= utilities.formatUSdate(poc.Surgical_Procedure_Date__c);
        
        travelStruct.facility.Appointments.add(appointment );  
        
        if(poc.Physician_Follow_up_Appointment_Date__c!=null){
            appointment = new ecenPatientTravelStruct.travelItem();
            appointment.label= 'Physician Follow-up Appointment';
            appointment.displayDate= utilities.formatUSdate(poc.Physician_Follow_up_Appointment_Date__c);
            if(poc.Physician_Appointment_Time__c!=null){
            appointment.displayTime= poc.Physician_Appointment_Time__c;
        }else{
            appointment.displayTime= 'NA';
        }
            travelStruct.facility.Appointments.add(appointment ); 
        }
        
        if(poc.Last_Appointment_Date__c!=null){
            appointment = new ecenPatientTravelStruct.travelItem();
            appointment.label= 'Last Appointment';
            appointment.displayDate= utilities.formatUSdate(poc.Last_Appointment_Date__c);
            if(poc.Last_Appointment_Time__c==null){
                appointment.displayTime= 'NA';
            }else{
                appointment.displayTime= poc.Last_Appointment_Time__c;
            
            }
        
            travelStruct.facility.Appointments.add(appointment ); 
        }
        
        appointment = new ecenPatientTravelStruct.travelItem();
        appointment.label= 'Hospital Discharge';
        appointment.displayDate= utilities.formatUSdate(poc.Hospital_Discharge_Date__c);
        travelStruct.facility.Appointments.add(appointment );  
        
        }    
    
        return travelStruct;
    }

}