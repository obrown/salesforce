@isTest
private class Test_RandomAuditApi {

static {
// setup test data  
}

static testMethod void testRandomAuditClaims() {

RestRequest req = new RestRequest(); 

Claims_Processor_Crosswalk__c  cpc = new Claims_Processor_Crosswalk__c(Processor_Alias_Name__c='auditmanager', name='auditmanager', isActive__c =true);
insert cpc;

req.requestURI = 'https://cs3.salesforce.com/services/apexrest/goRandomAudits/';  
string[] testString = new string[]{};
testString.add('502,WMT,21472305002,CKOWIC,5/4/2017,5/10/2017,5/4/2017,860,MM,H,CKOWIC');
testString.add('502,WMT,21472305003,CKOWIC,5/4/2017,5/10/2017,5/4/2017,860,MM,H,CKOWIC');
testString.add('502,WMT,21472305004,CKOWIC,5/4/2017,5/10/2017,5/4/2017,860,MM,H,CKOWIC');
testString.add('502,WMT,21472305005,CKOWIC,5/4/2017,5/10/2017,5/4/2017,860,MM,H,CKOWIC');
string jsonString = JSON.serialize(testString);
req.requestBody = blob.valueof(jsonString );
req.httpMethod = 'POST';
RestContext.request = req;

randomAuditApi.randomAuditClaims();

}

}