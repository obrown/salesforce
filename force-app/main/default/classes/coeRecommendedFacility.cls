public class coeRecommendedFacility{
Map<String, Schema.SObjectField> M = Schema.SObjectType.Patient_Case__c.fields.getMap();
    public recommendedFacility recommendedFacility {get;set;}
    public class recommendedFacility{
        
        public recommendedFacility(string name, string id){
            this.name=name;
            this.id=id;
            this.recommendedingRule = recommendedingRule;
            
        }
        public string name {get; private set;}
        public string id {get; private set;}
        public string recommendedingRule {get; private set;}
        public string clientFacility {get; private set;}
        
    }
    sobject theCase;
    integer maxLevels =3;
    
    //map<client_faclity__c, map<Recommended_Facility_Rule__c, Recommended_Facility_Rule__c[]>() rulesByPriorityByCf
    map<integer, map<id, Recommended_Facility_Rule__c[]>> rulesByPriorityByCf = new map<integer, map<id, Recommended_Facility_Rule__c[]>>();
    //map<Recommended_Facility_Rule__c, Recommendation_Logic__c[]>() logicByRule = new map<Recommended_Facility_Rule__c, Recommendation_Logic__c[]>();
    map<id, map<decimal, Recommendation_Logic__c[]>> logicByRule = new map<id, map<decimal, Recommendation_Logic__c[]>>();
    
    public recommendedFacility getRecommendedFacility(sObject theCase,string client, string procedure, boolean activeRuleSearch){
        recommendedFacility = new recommendedFacility(null, null);
        this.theCase= theCase;
        string ruleid;
        Recommendation_Logic__c[] rfrRules;
        //string dbQuery='select Field_Logic__c,Field_name__c,Field_Value__c,Field_Type__c,Level__c,L1L2Rule_Logic__c,L2L3Rule_Logic__c,Recommended_Facility_Rule__c,Recommended_Facility_Rule__r.L1Order__c,Recommended_Facility_Rule__r.L2Order__c,Recommended_Facility_Rule__r.L3Order__c,Recommended_Facility_Rule__r.L2Rule_Order__c,Recommended_Facility_Rule__r.L1Rule_Order__c,Recommended_Facility_Rule__r.Priority__c,Recommended_Facility_Rule__r.client_facility__r.facility__r.name, Recommended_Facility_Rule__r.client_facility__r.facility__r.longitude__c,Recommended_Facility_Rule__r.client_facility__r.facility__r.latitude__c,Recommended_Facility_Rule__r.client_facility__c,Recommended_Facility_Rule__r.Name,Name from Recommendation_Logic__c where ';
        
        if(activeRuleSearch){
            //dbQuery = dbQuery +'Recommended_Facility_Rule__r.Client_Facility__r.active__c = true and';
            //dbQuery = dbQuery +' Recommended_Facility_Rule__r.active__c = true and Recommended_Facility_Rule__r.Client_Facility__r.client__r.name =' + string.escapeSingleQuotes(patientCase.client_name__c)+ 'and Recommended_Facility_Rule__r.Client_Facility__r.procedure__r.name =' +patientCase.program_Type__c+ 'order by Recommended_Facility_Rule__r.Priority__c asc, Level__c asc';
            //rfrRules = database.query(dbQuery);
      
                for(Recommendation_Logic__c rfrRule : [select Field_Logic__c,
                               Field_name__c,
                               Field_Value__c,
                               Field_Type__c,
                               Level__c,
                               L1L2Rule_Logic__c,
                               L2L3Rule_Logic__c,
                               Recommended_Facility_Rule__c,
                               Recommended_Facility_Rule__r.L1Order__c,
                               Recommended_Facility_Rule__r.L2Order__c,
                               Recommended_Facility_Rule__r.L3Order__c,
                               Recommended_Facility_Rule__r.L2Rule_Order__c,
                               Recommended_Facility_Rule__r.L1Rule_Order__c,
                               Recommended_Facility_Rule__r.Priority__c,
                               Recommended_Facility_Rule__r.client_facility__r.facility__r.name,
                               Recommended_Facility_Rule__r.client_facility__r.facility__r.longitude__c,
                               Recommended_Facility_Rule__r.client_facility__r.facility__r.latitude__c,
                               Recommended_Facility_Rule__r.client_facility__c,
                               Recommended_Facility_Rule__r.client_facility__r.name,
                               Recommended_Facility_Rule__r.L3Rule_Order__c,
                               Recommended_Facility_Rule__r.Name,
                               Name
                               from Recommendation_Logic__c where Recommended_Facility_Rule__r.Client_Facility__r.active__c = true and Recommended_Facility_Rule__r.active__c = true and Recommended_Facility_Rule__r.Client_Facility__r.client__c =:client and Recommended_Facility_Rule__r.Client_Facility__r.procedure__c =: procedure order by Recommended_Facility_Rule__r.Priority__c asc, Level__c asc]){
                               popRulesByPriorityByCf(rfrRule);
                
            
            }
        
        }else{
        
            for(Recommendation_Logic__c rfrRule : [select Field_Logic__c,
                               Field_name__c,
                               Field_Value__c,
                               Field_Type__c,
                               Level__c,
                               L1L2Rule_Logic__c,
                               L2L3Rule_Logic__c,
                               Recommended_Facility_Rule__c,
                               Recommended_Facility_Rule__r.L1Order__c,
                               Recommended_Facility_Rule__r.L2Order__c,
                               Recommended_Facility_Rule__r.L3Order__c,
                               Recommended_Facility_Rule__r.L2Rule_Order__c,
                               Recommended_Facility_Rule__r.L1Rule_Order__c,
                               Recommended_Facility_Rule__r.L3Rule_Order__c,
                               Recommended_Facility_Rule__r.Priority__c,
                               Recommended_Facility_Rule__r.client_facility__r.facility__r.name,
                               Recommended_Facility_Rule__r.client_facility__r.facility__r.longitude__c,
                               Recommended_Facility_Rule__r.client_facility__r.facility__r.latitude__c,
                               Recommended_Facility_Rule__r.client_facility__r.facility__c,
                               Recommended_Facility_Rule__r.client_facility__c,
                               Recommended_Facility_Rule__r.client_facility__r.name,
                               Recommended_Facility_Rule__r.Name,
                               
                               Name
                               from Recommendation_Logic__c where Recommended_Facility_Rule__r.active__c = true and Recommended_Facility_Rule__r.Client_Facility__r.client__c = :client and Recommended_Facility_Rule__r.Client_Facility__r.procedure__c = :procedure order by Recommended_Facility_Rule__c asc, Recommended_Facility_Rule__r.Priority__c asc, Level__c asc]){
        
                    popRulesByPriorityByCf(rfrRule);
        
            }
        }
        
        for(integer i=1; i<=maxLevels; i++){
            
            map<id, Recommended_Facility_Rule__c[]> cfRuleMap = rulesByPriorityByCf.get(i);
            if(cfRuleMap==null){continue;}
            
            for(id cfId : cfRuleMap.keySet()){
                integer numofrules = cfRuleMap.get(cfId).size();
                integer runCount = 0;
                //system.debug('numofrules ' +numofrules );
                
                for(Recommended_Facility_Rule__c rfrRule : cfRuleMap.get(cfId)){
                    
                    runCount ++;
                    
                    //breaking here will move to the next facility
                    boolean result = iterateLogicRules(logicByRule.get(rfrRule.id));
                    system.debug('runCount: '+ runCount);
                    system.debug('numofrules: '+ numofrules);
                    if(!result && runCount == numofrules ){
                        break;  
                    
                    }else if(result && runCount == numofrules){
                        return recommendedFacility;
                    }
                    
                    
                }
               
            }
            
        }
        
        return recommendedFacility;
    }

    
    void poprulesByPriorityByCf(recommendation_Logic__c rfrLogic){
        
        Recommended_Facility_Rule__c rfrRule = new Recommended_Facility_Rule__c(id=rfrLogic.Recommended_Facility_Rule__c);
        
        rfrRule.L1Order__c = rfrLogic.Recommended_Facility_Rule__r.L1Order__c;
        rfrRule.L2Order__c = rfrLogic.Recommended_Facility_Rule__r.L2Order__c;
        rfrRule.L3Order__c = rfrLogic.Recommended_Facility_Rule__r.L3Order__c;
        rfrRule.L1Rule_Order__c = rfrLogic.Recommended_Facility_Rule__r.L1Rule_Order__c;
        rfrRule.L2Rule_Order__c = rfrLogic.Recommended_Facility_Rule__r.L2Rule_Order__c;
        rfrRule.L3Rule_Order__c = rfrLogic.Recommended_Facility_Rule__r.L3Rule_Order__c;
        rfrRule.Priority__c = rfrLogic.Recommended_Facility_Rule__r.Priority__c;
        
        facility__c fac =  new facility__c(id=rfrLogic.Recommended_Facility_Rule__r.client_facility__r.facility__c);
        fac.longitude__c = rfrLogic.Recommended_Facility_Rule__r.client_facility__r.facility__r.longitude__c;
        fac.latitude__c = rfrLogic.Recommended_Facility_Rule__r.client_facility__r.facility__r.latitude__c;
        fac.name = rfrLogic.Recommended_Facility_Rule__r.client_facility__r.facility__r.name;
        
        client_facility__c clientFac = new client_facility__c(id=rfrLogic.Recommended_Facility_Rule__r.client_facility__c);
        clientFac.facility__r = fac;
        
        rfrRule.Name = rfrLogic.Recommended_Facility_Rule__r.Name;
        rfrRule.client_facility__c = rfrLogic.Recommended_Facility_Rule__r.client_facility__c;
        rfrRule.client_facility__r = clientFac;
        
        //rfrRule.client_facility__r.facility__r.name = rfrLogic.Recommended_Facility_Rule__r.client_facility__r.facility__r.name;
        //rfrRule.client_facility__r.facility__r.longitude__c = rfrLogic.Recommended_Facility_Rule__r.client_facility__r.facility__r.longitude__c;
        //rfrRule.client_facility__r.facility__r.latitude__c = rfrLogic.Recommended_Facility_Rule__r.client_facility__r.facility__r.latitude__c;
        
        
        //Client facility , facility rules
        map<id, Recommended_Facility_Rule__c[]> cfRulesMap = rulesByPriorityByCf.get(integer.valueof(rfrLogic.Recommended_Facility_Rule__r.Priority__c));
        
        if(cfRulesMap==null){
            cfRulesMap = new map<id, Recommended_Facility_Rule__c[]>();
        }
        
        Recommended_Facility_Rule__c[] rules = cfRulesMap.get(rfrLogic.Recommended_Facility_Rule__r.client_facility__c);
        if(rules==null){
            rules = new Recommended_Facility_Rule__c[]{};
        }
        
        rules.add(rfrRule);
        cfRulesMap.put(rfrLogic.Recommended_Facility_Rule__r.client_facility__c, rules);
        
        //logic level
        map<decimal, Recommendation_Logic__c[]>  logicRulesMap = logicByRule.get(rfrLogic.Recommended_Facility_Rule__c);
        if(logicRulesMap==null){
            logicRulesMap = new map<decimal, Recommendation_Logic__c[]>();
        }
        
        Recommendation_Logic__c[] logicRules = logicRulesMap.get(rfrLogic.level__c);
        
        if(logicRules==null){
            
            logicRules = new Recommendation_Logic__c[]{};
        }
         
        logicRules.add(rfrLogic);
        logicRulesMap.put(rfrLogic.level__c, logicRules);
        logicByRule.put(rfrLogic.Recommended_Facility_Rule__c, logicRulesMap);
        rulesByPriorityByCf.put(integer.valueof(rfrLogic.Recommended_Facility_Rule__r.Priority__c) ,cfRulesMap);
       
    }
    
    boolean iterateLogicRules(map<decimal, Recommendation_Logic__c[]> logicByLevel){
            string levelOperator;
            decimal numberOfLevels = findUpper(logicByLevel.keySet());
            for(decimal i=1; i<=numberOfLevels; i++){
                
                if(logicByLevel.get(i)==null){break;}
                
                integer numberOfLogicToMeet = logicByLevel.get(i).size(); 
                integer rulesMet=0;
                
                for(Recommendation_Logic__c rule : logicByLevel.get(i)){
                        //the inner group operator. where you would drop the logic onto 
                        string groupOperator = string.valueof(rule.getSObject('Recommended_Facility_Rule__r').get('L' + integer.valueof(i) + 'Order__c'));
                        //Loop though the rules of the logic records
                        rulesMet++;
                        try{
                            //operator for in-between levels
                            levelOperator = string.valueof(rule.getSObject('Recommended_Facility_Rule__r').get('L' + integer.valueof(i) + 'Rule_Order__c'));
                       
                            }catch(exception e){
                                system.debug(e.getMessage());
                            }
                            
                            system.debug(rule.id);
                            system.debug(rule.Recommended_Facility_Rule__r.client_facility__c);
                            system.debug(rule.Field_name__c);
                            system.debug(rule.Field_logic__c);
                            system.debug(rule.Field_value__c);
                            system.debug(rule.name);
                            system.debug('groupOperator '+groupOperator);
                            system.debug('levelOperator '+levelOperator);
                            system.debug('numberOfLevels '+ numberOfLevels);
                            system.debug('Current Level '+ i);
                            system.debug('numberOfLogicToMeet vs rulesMet '+ numberOfLogicToMeet +' '+rulesMet);
                                                      
                            Schema.SObjectField field = M.get(rule.Field_name__c);
                            
                            boolean testResult = testLogic(rule, theCase);
                            system.debug('testResult: '+ testResult);
                            //Logic passed and looped through all groups and all levels and all. Rule Passed
                            if(testResult && (numberOfLogicToMeet == rulesMet) && (numberOfLevels==i)){
                                system.debug('all goo');
                            
                                recommendedFacility.name = rule.Recommended_Facility_Rule__r.client_facility__r.facility__r.name;
                                recommendedFacility.id= rule.Recommended_Facility_Rule__c;
                                recommendedFacility.recommendedingRule  = rule.Recommended_Facility_Rule__r.client_facility__r.name+' '+rule.Recommended_Facility_Rule__r.name;   
                                recommendedFacility.clientFacility = rule.Recommended_Facility_Rule__r.client_facility__c;
                                return true;
                            }

                            //Logic passed and looped group is an Or and All levels. Rule Passed
                            if(testResult && (numberOfLevels==i && groupOperator =='OR')){
                                recommendedFacility.name = rule.Recommended_Facility_Rule__r.client_facility__r.facility__r.name;
                                recommendedFacility.id= rule.Recommended_Facility_Rule__c;
                                recommendedFacility.recommendedingRule =rule.Recommended_Facility_Rule__r.client_facility__r.name+' '+rule.Recommended_Facility_Rule__r.name;  
                                recommendedFacility.clientFacility = rule.Recommended_Facility_Rule__r.client_facility__c; 
                                return true;
                            }
                            
                            //logic passed and everything else about the query is OR. Rule Passed
                            if(testResult && groupOperator == 'OR'  && levelOperator =='OR'){
                                recommendedFacility.name = rule.Recommended_Facility_Rule__r.client_facility__r.facility__r.name;
                                recommendedFacility.id= rule.Recommended_Facility_Rule__c; 
                                recommendedFacility.recommendedingRule= rule.Recommended_Facility_Rule__r.client_facility__r.name+' '+rule.Recommended_Facility_Rule__r.name;   
                                recommendedFacility.clientFacility = rule.Recommended_Facility_Rule__r.client_facility__c;
                                return true;
                            }
                            
                            //logic failed but the group or level is an OR statement. Continue to the next rule
                            if(!testResult && (groupOperator =='OR')){
                                continue;
                            }
                            
                            //logic failed and the group and level is an AND/null statement. Rule failed
                            if(!testResult && groupOperator =='AND'  && levelOperator != 'OR'){
                                return false;
                            }
                            
                            if(!testResult && numberOfLogicToMeet == rulesMet  && levelOperator == 'AND'){
                                return false;
                            }
                            
                            //logic failed and the level is an AND statement. Rule failed
                            if(!testResult && levelOperator =='AND'){
                                return false;
                            }
                           
                            
                            
                                
                }
            }
                    
            return false;      
        
    }
    
    boolean testLogic(Recommendation_Logic__c rule, sObject theCase){
        
        
        if(rule.Field_Name__c==null){return false;}
        string recordValue =string.valueof(theCase.get(rule.Field_Name__c));
        string fieldType = rule.field_type__c;
        
        if(fieldType!='MILEAGEDISTANCE' && recordValue==null){return false;}
        
        string operator = rule.field_logic__c;
        string value = rule.field_value__c;
        
        system.debug('recordValue: '+recordValue);
        system.debug('fieldType: '+fieldType);
        system.debug('operator : '+operator );
        system.debug('value: '+value);
        
        
        if(fieldType=='MILEAGEDISTANCE'){
            
            string caseLatLong = string.valueof(theCase.get('CaseLatitudeLongitude__c'));
            
            if(caseLatLong==''||caseLatLong==null){return false;}
            
            string caseLongitude=caseLatLong.split(',')[1];
            string caseLatitude=caseLatLong.split(',')[0];
            
            string cfLongitude= rule.Recommended_Facility_Rule__r.client_facility__r.facility__r.longitude__c;
            string cfLatitude=rule.Recommended_Facility_Rule__r.client_facility__r.facility__r.latitude__c;
            
            Location ruleFacility = Location.newInstance(Decimal.valueof(rule.Recommended_Facility_Rule__r.client_facility__r.facility__r.latitude__c),Decimal.valueof(rule.Recommended_Facility_Rule__r.client_facility__r.facility__r.longitude__c));
            Location caseLocation = Location.newInstance(Decimal.valueof(caseLatitude),Decimal.valueof(caseLongitude));
            
            Double dist = Location.getDistance(caseLocation, ruleFacility , 'mi');
            recordValue=string.valueof(dist);
            
        }else if(fieldType=='FACILITYDISTANCE'){
             
            string caseLongitude=recordValue.split(',')[1];
            string caseLatitude=recordValue.split(',')[0];
            
            string cfLongitude=value.split(',')[1];
            string cfLatitude=value.split(',')[0];
            
            // Instantiate new Location objects and compute the distance between them in different ways.
            Location caseLocation = Location.newInstance(Decimal.valueof(caseLatitude),Decimal.valueof(caseLongitude));
            Location userChosenFacility = Location.newInstance(Decimal.valueof(cfLatitude), Decimal.valueof(cfLongitude));
            Location ruleFacility = Location.newInstance(Decimal.valueof(rule.Recommended_Facility_Rule__r.client_facility__r.facility__r.latitude__c),Decimal.valueof(rule.Recommended_Facility_Rule__r.client_facility__r.facility__r.longitude__c));
            
            //brings over lat and long
            Double dist = Location.getDistance(caseLocation, ruleFacility , 'mi');
            Double dist2 = Location.getDistance(caseLocation, userChosenFacility , 'mi');
            recordValue=string.valueof(dist);
            value=string.valueof(dist2);
        }
        
        if(operator=='equals'){
            return (value==recordValue) ? true : false;
        
        }else if(operator=='not equals'){
            return (value!=recordValue) ? true : false;
        
        }else if(operator=='contains'){
            if(recordValue==null){return false;}
            return (recordValue.contains(value)) ? true : false;
        
        }else if(operator=='does not contain'){
            if(recordValue==null){return true;}
            return (recordValue.contains(value)) ? false : true;
        
        }else if(operator=='less than or equal' || operator=='less than' || operator=='greater than' || operator=='greater than or equal'){
            decimal dValue;
            decimal rValue;
            
            try{
                dValue = decimal.valueof(value);
                rValue = decimal.valueof(recordValue);
                
            }catch(exception e){
                system.debug(e.getMessage());
                system.debug(e.getlineNumber());
                return false;
            }
            
            
            if(operator=='less than or equal'){
                return (rValue<=dValue) ? true : false;
            
            }else if(operator=='less than'){
                return (rValue<dValue) ? true : false;
                
            }else if(operator=='greater than'){
                return (rValue>dValue) ? true : false;
                
            }else if(operator=='greater than or equal'){
                return (rValue>=dValue) ? true : false;
                
            }
        
        }
        
        return false;
    }
    
    decimal findUpper(set<decimal> theSet){
       
       decimal[] fooList = new list<decimal>(theSet);
       fooList.sort();
       return fooList[foolist.size()-1];
        
        
    }
    
}