public with sharing class bariatricUtils{


    public static string dobPageEncode(string d){
        
        string[] foo = new string[]{};
        foo= d.split('-');
        
        if(foo[0].length()==2){
            foo[0] = '19' +foo[0];
        }
        
        return foo[1]+'/'+foo[2]+'/'+foo[0];
    }
    
    public static string dobDatabaseEncode(string d){
        
        string[] foo = new string[]{};
        try{
        foo = d.split('/');
        if(foo[2].length()==2){
            foo[2] = '19' +foo[2];
        }
        return foo[2]+'-'+foo[0]+'-'+foo[1];
        }catch(exception e){
            return 'fail';
        }
    }
    
    public static string coeDirectory(bariatric__c bariatric){
        string coeFileNamePrefix = bariatric.patient_last_name__c+', '+bariatric.patient_first_name__c+'_'+bariatric.patient_dob__c.replaceAll('/','_')+'/'+bariatric.name;
        string coeFileDirectory ='Bariatric/'+coeFileNamePrefix;
        return coeFileDirectory;
    }
}