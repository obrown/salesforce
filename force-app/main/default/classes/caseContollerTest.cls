@isTest(seealldata=true)
private class caseContollerTest {
    
    private static testMethod void testNewCase()
    {
        
        client_facility__c[] cfs = [select client__r.name, client__c, procedure__c, procedure__r.name from client_facility__c];
        
        map<string, string> cfIdMap = new map<string, string>();
        map<string, string> procedureIdMap = new map<string, string>();
        
        for(client_facility__c cf : cfs){
            cfIdMap.put(cf.client__r.name, cf.client__c);
            procedureIdMap.put(cf.procedure__r.name, cf.procedure__c);
        }
         
        Patient_Case__c pc = new Patient_Case__c();
        
        ApexPages.currentPage().getParameters().put('client',cfIdMap.get('Lowes'));
        ApexPages.currentPage().getParameters().put('procedure',procedureIdMap.get('Cardiac'));
        pc.Client_name__c = 'Lowes';
        caseController cc = new caseController();
        
        pc.client_name__c= 'Walmart';
        ApexPages.currentPage().getParameters().put('client',cfIdMap.get('Walmart'));
        ApexPages.currentPage().getParameters().put('procedure',procedureIdMap.get('Spine'));
        cc = new caseController();
        
        pc.client_name__c= 'Walmart';
        ApexPages.currentPage().getParameters().put('client',cfIdMap.get('Walmart'));
        ApexPages.currentPage().getParameters().put('procedure',procedureIdMap.get('Cardiac'));
        cc = new caseController();
        
        ApexPages.currentPage().getParameters().put('client',cfIdMap.get('Walmart'));
        ApexPages.currentPage().getParameters().put('procedure',procedureIdMap.get('Joint'));
        pc.Employee_First_Name__c  ='John';pc.Employee_Last_Name__c  ='Smith';
        cc = new caseController();
        cc.checkdup();
        
        cc.obj.status__c= 'Open';
        cc.obj.status_reason__c= 'In Process';
        
        cc.inlineSave();
        system.assert(cc.message!='');
        
        cc.obj.BID__c = '123456';
        cc.obj.Language_Spoken__c = 'English';
        cc.obj.Initial_Call_Discussion__c = 'The ICD';
        cc.obj.Relationship_to_the_Insured__c = 'Spouse';
        cc.obj.Caller_relationship_to_the_patient__c='Spouse';
        cc.obj.caller_name__c='John Doe';
        
        cc.obj.Employee_first_name__c = 'John';
        cc.obj.Employee_last_name__c = 'Smith';
        cc.obj.employee_dob__c = date.valueof('1980-01-01');
        cc.obj.Employee_Gender__c = 'Male';
        cc.obj.employee_ssn__c = '123456789';
        cc.obj.employee_street__c = '123 E Main St';
        cc.obj.Employee_City__c = 'Mesa';
        cc.obj.Employee_State__c = 'AZ';
        cc.obj.Employee_Zip_Postal_Code__c = '85297';
        cc.obj.Employee_Country__c = 'USA';
        
        cc.obj.Patient_First_Name__c = 'Jane';
        cc.obj.Patient_last_name__c = 'Smith';
        cc.obj.Patient_dob__c = date.valueof('1981-01-01');
        cc.obj.Patient_Gender__c = 'Female';
        cc.obj.Patient_ssn__c = '987654321';
        cc.obj.Patient_Email_Address__c = 'none';
        cc.obj.Patient_street__c = '123 E Main St';
        cc.obj.Patient_City__c = 'Mesa';
        cc.obj.Patient_State__c = 'AZ';
        cc.obj.Patient_Zip_Postal_Code__c = '85297';
        cc.obj.Patient_Country__c = 'USA';
        
        cc.obj.Same_as_Employee_Address__c = true;
        
        
        cc.obj.Employee_Home_Phone__c = '(480) 555-1212';
        cc.obj.Employee_Mobile__c = '(480) 555-1212';
        cc.obj.Employee_Work_Phone__c = '(480) 555-1212';
        
        cc.obj.Patient_Home_Phone__c = '(480) 555-1212';
        cc.obj.Patient_Mobile__c = '(480) 555-1212';
        cc.obj.Patient_Work_Phone__c = '(480) 555-1212';
        
        cc.obj.CallBack_Number__c = '(480) 555-1212';
        cc.obj.Caregiver_DOBe__c = '1982-01-01';
        
        cc.obj.Listed_as_Program_Covered_Procedure__c = 'Yes';
        cc.obj.What_procedure_was_recommended__c = 'Testing';
        cc.obj.Carrier_Name__c = 'Aetna';
        cc.obj.attachEncrypt__c = EncodingUtil.base64encode(Crypto.generateAesKey(128));
        
        cc.inlineSave();
        
        system.assert(cc.obj.id!=null);
        
        Attachment a = [select name,body,parentid from Attachment limit 1];
        Attachment a1 = a.clone();
        a1.parentid = cc.obj.id;
        insert a1;
        
        cc.LoadAttachmentData();
        ApexPages.currentPage().getParameters().put('attachID',a1.id);
        
        cc.encryptAttach();
        cc.decryptAttach();
        
        ApexPages.currentPage().getParameters().put('attachID',a1.id);
        cc.DeleteAttach();  
        
        cc.transfertoCM();
        system.debug('tcm message '+ cc.message);
        
        cc.prolnameS ='Martin';
        cc.profState='AZ';  
        
        cc.setsearchProviders();
        cc.processSelected();
             
    }
    
    private static testMethod void testTheRestWJ()
    {
        
        Patient_Case__c pc = new Patient_Case__c();
        
        pc.Status__c = 'Open';
        pc.BID__c = '123456';
        pc.Language_Spoken__c = 'English';
        pc.Initial_Call_Discussion__c = 'The ICD';
        pc.Relationship_to_the_Insured__c = 'Spouse';
        
        pc.Employee_first_name__c = 'John';
        pc.Employee_last_name__c = 'Smith';
        pc.employee_dob__c = date.valueof('1980-01-01');
        pc.Employee_Gender__c = 'Male';
        pc.employee_ssn__c = '123456789';
        
        pc.Patient_First_Name__c = 'Jane';
        pc.Patient_last_name__c = 'Smith';
        pc.Patient_dob__c = date.valueof('1981-01-01');
        pc.Patient_Gender__c = 'Female';
        pc.Patient_ssn__c = '987654321';
        
        pc.Same_as_Employee_Address__c = true;
        
        pc.employee_street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85297';
        
        pc.Employee_Home_Phone__c = '(480) 555-1212';
        pc.Employee_Mobile__c = '(480) 555-1212';
        pc.Employee_Work_Phone__c = '(480) 555-1212';
        
        pc.Patient_Home_Phone__c = '(480) 555-1212';
        pc.Patient_Mobile__c = '(480) 555-1212';
        pc.Patient_Work_Phone__c = '(480) 555-1212';
        
        pc.Callback_Number__c = '(480) 555-1212';
        
        pc.Caregiver_Home_Phone__c = '(480) 555-1212';
        pc.Caregiver_Mobile__c = '(480) 555-1212';
        
        pc.Caregiver_DOBe__c = '1982-01-01';
        
        pc.recordtypeid=[select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name like 'Walmart Joint' limit 1].id;
        pc.Client_name__c = 'Walmart';
        insert pc;
        
        Task newT = new Task(whatid = pc.id,
                             subject='Test'
                             );
        
        insert newT; //Will test the deleteOpenT function and the loadAH function
        
        Task existingT = newT.clone();
        insert existingT;
        
        existingT.Status = 'Completed';
        update existingT;
        
        ApexPages.currentPage().getParameters().put('id',pc.id); 
        caseController cc = new caseController();
        
        cc.DeleteAH();
        ApexPages.currentPage().getParameters().put('theAHid',existingT.id);    
        cc.DeleteAH();
        
        cc.DeleteOpenT();
        ApexPages.currentPage().getParameters().put('theAHid',newT.id);
        cc.DeleteOpenT();
        
        /* Misc */
        
        cc.getRecordtypeName();
        cc.getCallerName();
        cc.setCallerName('John Smith');
        
        cc.obj.Language_Spoken__c = 'English';
        cc.refreshLangauge();
        cc.updatePatientInfo();
        cc.getRF();
        cc.setRF('Virginia Mason');
        cc.mycancel();
        cc.getisdeleteprovider();
        cc.getisEditProvider();
        cc.getisDelete();
        cc.getDeleteTask();
        //cc.getshownotesRecord();
        cc.addRowsAH();
        //cc.getshowAHrecords();
        //cc.getshowmoreah();
        cc.getshowcancel();
        cc.addshowERecord();
        //cc.getshowERecord();
        //cc.getshowmoreERecord();
        //cc.getshownotesRecord();
        cc.addRowsNotes();
        //cc.getshowmorenotes();
        cc.getreferalresult();
        //cc.getiserror();
        cc.refreshCodeList();
        cc.setMessage('');
        cc.getIsErrorMessage();
        cc.setMessage('test');
        cc.getIsErrorMessage();
        cc.getEPHN();
        cc.getSCtext();
        
        ApexPages.currentPage().getParameters().put('thenicotravelreson','testing');
        cc.reasonniconotrequired();
        cc.continineOverride();
        cc.continineOverrideNo();
        cc.sendContinineEmail();
        /* End */
        
        //cc.getStatusList();
        //cc.getStatusReasonList();
            
    }
    
    private static testMethod void convertWJ()
    {
    
        Patient_Case__c pc = new Patient_Case__c();
        
        pc.client_name__c= 'Walmart';
        pc.recordtypeid = [select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name = 'Walmart Joint' limit 1].id;
        
        pc.Status__c = 'Open';
        pc.BID__c = '123456';
        pc.Language_Spoken__c = 'English';
        pc.Initial_Call_Discussion__c = 'The ICD';
        pc.Relationship_to_the_Insured__c = 'Spouse';
        pc.referral_source__c = 'Company Video';
        pc.Employee_Primary_Health_Plan_ID__c ='123456';
        pc.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        pc.Carrier_Name__c = 'Aetna';
        pc.Carrier_Nurse_Name__c = 'John Smith';
        pc.Carrier_Nurse_Phone__c = '(480) 555-1212';
        
        
        pc.Employee_first_name__c = 'John';
        pc.Employee_last_name__c = 'Smith';
        pc.employee_dobe__c = '1980-01-01';
        pc.Employee_Gender__c = 'Male';
        pc.employee_ssn__c = '123456789';
        
        pc.Patient_First_Name__c = 'Jane';
        pc.Patient_last_name__c = 'Smith';
        pc.Patient_dobe__c = '1981-01-01';
        pc.Patient_Gender__c = 'Female';
        pc.Patient_ssn__c = '987654321';
        pc.Patient_Email_Address__c = 'something@hdplus.com';
        
        pc.Same_as_Employee_Address__c = true;
        
        pc.employee_street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85297';
        
        pc.Employee_Home_Phone__c = '(480) 555-1212';
        pc.Employee_Mobile__c = '(480) 555-1212';
        pc.Employee_Work_Phone__c = '(480) 555-1212';
        
        pc.Employee_Preferred_Phone__c = 'Home';
        
        pc.Patient_Home_Phone__c = '(480) 555-1212';
        pc.Patient_Mobile__c = '(480) 555-1212';
        pc.Patient_Work_Phone__c = '(480) 555-1212';
        
        pc.Patient_Preferred_Phone__c = 'Home';
        pc.Callback_Number__c = '(480) 555-1212';
        
        pc.Caregiver_Home_Phone__c = '(480) 555-1212';
        pc.Caregiver_Mobile__c = '(480) 555-1212';
        
        pc.Caregiver_Name__c = 'John Smith';
        pc.Caregiver_DOBe__c = '1982-01-01';
        
        insert pc;
        
        Test.startTest();
        
        ApexPages.currentPage().getParameters().put('id',pc.id);
        caseController cc = new caseController();
        
        cc.obj.Provider_Type__c='Family';
        cc.profname='Jane';
        cc.prolname='Smith';
        cc.proemail = 'something@hdplus.com';
        cc.prostreet = '123 E Main St';
        cc.procity = 'Mesa';
        cc.prozip = '85212';
        
        cc.AddProvider();
        system.assert(cc.Providers.size()>0);
        
        Provider__c delPro = cc.Providers[0].clone();
        insert delPro;
        ApexPages.currentPage().getParameters().put('theProId',cc.Providers[0].id);
        cc.editPro();
        cc.profname='John';
        ApexPages.currentPage().getParameters().put('theProId',null);
        cc.DeletePro();
        ApexPages.currentPage().getParameters().put('theProId',delPro.id);
        cc.DeletePro();
        cc.closePro();
        
        cc.obj.Expedited_Referral__c = 'Yes';
        cc.obj.Medicare_As_Secondary_Coverage__c = 'No';
        cc.obj.Other_Pertinent_Medical_Info_History__c = 'Test';
        cc.obj.Patient_Symptoms__c = 'Testing';
        cc.obj.Proposed_Procedure__c = 'Testing';
        cc.obj.Recent_Testing__c = 'Testing';
      // cc.obj.Referred_facility__c = 'John Hopkins';
        cc.obj.Diagnosis__c = 'Testing';
        
        cc.inlineSave();
        
        cc.getcodeOptions();
        cc.getcTypeItems();
        
        cc.obj.Clinical_Code_Status__c = 'Proposed';
        cc.codeType = 'DRG';
        cc.codeList =  [select id from Code__c where type__c = 'DRG' limit 1].id;
        cc.addCC();
        
        system.assert(cc.cc.size()>0);
            
        cc.submitForEligibility();
        cc.obj.Eligible__c = 'Yes';
        cc.obj.Patient_and_Employee_DOB_verified__c = true;
        cc.obj.Patient_and_Employee_SSN_verified__c = true;
        cc.obj.BID__c = '123549';
        cc.obj.BID_Verified__c = true;
        cc.obj.Carrier_and_Plan_Type_verified__c = true;
        
        update cc.obj;
        
        cc.convertLead();
        system.debug(cc.message=='');
        ApexPages.currentPage().getParameters().put('Id',cc.obj.id);
        vendorReferralController vrc = new vendorReferralController ();
        vrc.referCase();
        
        ApexPages.currentPage().getParameters().put('Id',cc.obj.id);
        multiReferralFormsController mfc = new multiReferralFormsController();   
        mfc.formSelected();
        cc.obj.isConverted__c = true;
        update cc.obj;
            
        system.assert(cc.obj.isConverted__c);
        
        cc.referalForm();
        
        //cc.obj.Add_On_Charges_Approved__c = date.today();
        //cc.obj.Add_On_Charges__c = 'MRI';
        //cc.obj.Billed_Add_On_Charges__c = 'MRI';
        //cc.obj.Add_On_Charges_Note__c = 'Test';
        
        cc.addAOC();
        system.assert(cc.addoncharges.size()>0);
        ApexPages.currentPage().getParameters().put('theAHid',cc.addoncharges[0].id);
        cc.editAOC();
        cc.deleteAOC();
        
        ApexPages.currentPage().getParameters().put('relatedrecordtype','Walmart Spine');
        
        cc.createRelatedCase();
        cc.deductRequest();
        pc = cc.obj;
        
        ApexPages.currentPage().getParameters().put('id',pc.id);
        cc = new caseController();
        
        Attachment a = [select name,body,parentid from Attachment limit 1];
        Attachment a1 = a.clone();
        a1.parentid = cc.obj.id;
        insert a1;
        
        Program_Notes__c pn = new Program_Notes__c(subject__c='Test',
                                                   notes__c='Test',
                                                   patient_case__c=cc.obj.id);
                                                   
        cc.sendAuth();
        insert pn;                                         
        
        pn.isAuthEmailAttach__c = true;
        update pn;
        cc.sendAuth();
        pn.attachid__c = a1.id;
        update pn;
        
        cc.sendAuth();
        
        test.stopTest();
        
    }
    
    private static testMethod void testStatuses()
    {

        Patient_Case__c pc = new Patient_Case__c();
        
        pc.client_name__c= 'Walmart';
        pc.recordtypeid = [select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name = 'Walmart Joint' limit 1].id;
        
        pc.Status__c = 'Open';
        pc.BID__c = '123456';
        pc.Language_Spoken__c = 'English';
        pc.Initial_Call_Discussion__c = 'The ICD';
        pc.Relationship_to_the_Insured__c = 'Spouse';
        pc.referral_source__c = 'Company Video';
        pc.Employee_Primary_Health_Plan_ID__c ='123456';
        pc.Employee_Primary_Health_Plan_Name__c = 'HRA (PPO)';
        pc.Carrier_Name__c = 'Aetna';
        pc.Carrier_Nurse_Name__c = 'John Smith';
        pc.Carrier_Nurse_Phone__c = '(480) 555-1212';
     
        
        pc.Employee_first_name__c = 'John';
        pc.Employee_last_name__c = 'Smith';
        pc.employee_dobe__c = '1980-01-01';
        pc.Employee_Gender__c = 'Male';
        pc.employee_ssn__c = '123456789';
        
        pc.Patient_First_Name__c = 'Jane';
        pc.Patient_last_name__c = 'Smith';
        pc.Patient_dobe__c = '1981-01-01';
        pc.Patient_Gender__c = 'Female';
        pc.Patient_ssn__c = '987654321';
        pc.Patient_Email_Address__c = 'something@hdplus.com';
        
        pc.Same_as_Employee_Address__c = true;
        
        pc.employee_street__c = '123 E Main St';
        pc.Employee_City__c = 'Mesa';
        pc.Employee_State__c = 'AZ';
        pc.Employee_Zip_Postal_Code__c = '85297';
        
        pc.Employee_Home_Phone__c = '(480) 555-1212';
        pc.Employee_Mobile__c = '(480) 555-1212';
        pc.Employee_Work_Phone__c = '(480) 555-1212';
        
        pc.Employee_Preferred_Phone__c = 'Home';
        
        pc.Patient_Home_Phone__c = '(480) 555-1212';
        pc.Patient_Mobile__c = '(480) 555-1212';
        pc.Patient_Work_Phone__c = '(480) 555-1212';
        
        pc.Patient_Preferred_Phone__c = 'Home';
        
        pc.Callback_Number__c = '(480) 555-1212';
        
        pc.Caregiver_Home_Phone__c = '(480) 555-1212';
        pc.Caregiver_Mobile__c = '(480) 555-1212';
        
        pc.Caregiver_Name__c = 'John Smith';
        pc.Caregiver_DOBe__c = '1982-01-01';
        
        insert pc; 
        ApexPages.currentPage().getParameters().put('id',pc.id);     
        caseController cc = new caseController();
        
        //cc.Status = 'Open';
        //cc.status = 'Pended';
        //cc.status = 'Closed';
        
        cc.obj.isConverted__c = true;
        update cc.obj;
        /*
        cc.Status = 'Open';
        cc.getStatusReasonList();
        
        cc.status = 'Pended';
        cc.getStatusReasonList();
        
        cc.status = 'Closed';
        cc.getStatusReasonList();
        */
    }

    private static testMethod void convertCasetransfertoCM()
    {
        patient_case__c pc = new patient_case__c();
        pc.client_name__c = 'Walmart';
        pc.employee_last_name__c = 'Test';
        pc.RecordTypeId =[select id from RecordType where sObjectType='Patient_Case__c' and isActive = true and name = 'Walmart Spine' limit 1].id;
        insert pc;
        
        Provider__c pro = new Provider__c(patient_case__c=pc.id);
        insert pro;
        
        convertCase.transfertoCM(pc);
        convertcase.walmartRequiredfields(pc);
        
        
    }
    //    public static string theStatus(string statusReason, string subList,string cgName, boolean isConverted, id recordID)
    private static testMethod void testSetExternalStatusRollUp(){
        setExternalStatusRollUp.theStatus('','Awaiting Appointment Date Confirmations', '','',true, null);
        setExternalStatusRollUp.theStatus('','', 'Home Provider','',true, null);
        setExternalStatusRollUp.theStatus('','Appointment Date', '','',true, null);
        setExternalStatusRollUp.theStatus('','Nicotine', '','',true, null);
        setExternalStatusRollUp.theStatus('','No Home Physician', '','',true, null);
        setExternalStatusRollUp.theStatus('','Medical Records', '','',true, null);
        setExternalStatusRollUp.theStatus('','Dental', '','',true, null);
        setExternalStatusRollUp.theStatus('','Caregiver', '','',true, null);
        
        setExternalStatusRollUp.theStatus('','In Process', '','',false, null);
        setExternalStatusRollUp.theStatus('','', 'Diagnostics','',false, null);
        setExternalStatusRollUp.theStatus('','In Process', '','',false, null);
        setExternalStatusRollUp.theStatus('','', 'Patient Expired','', false, null);
        
        setExternalStatusRollUp.theStatus('','', 'Not Eligible - Procedure', '',false, null);
        setExternalStatusRollUp.theStatus('','Not Eligible - Procedure', '','',false, null);
        setExternalStatusRollUp.theStatus('','', 'No Home Physician', '',false, null);
        setExternalStatusRollUp.theStatus('','', 'No Eligibility', '',false, null);
        setExternalStatusRollUp.theStatus('','', 'Patient Choice', '',false, null);
        setExternalStatusRollUp.theStatus('Completed','Claim Paid', '', '',true, null);
        setExternalStatusRollUp.theStage('', '','','In process', false);
        setExternalStatusRollUp.theStage('', '','','Accepted', false);
        setExternalStatusRollUp.theStage('', 'Claim Pending','','', false);
        setExternalStatusRollUp.theStage('', 'Patient at COE','','',false);
        setExternalStatusRollUp.theStage('', '','No Surgery Indicated','',true);
        setExternalStatusRollUp.theStage('', 'Pended','','', true);
        setExternalStatusRollUp.theStage('', '','No Eligibility', '',true);
        setExternalStatusRollUp.theStage('', '','Claim Paid', '',true);
        
        formatID f = new formatID();
        f.formatID('l1223','Lowes');
        f.formatID('kk0000001223','Kohls');
        f.formatID('gg4562','HCR ManorCare');
        f.formatID('p4456456','PepsiCo');
        f.formatID('4562','WalMart');
        f.formatID('4562','WalMart');
        
        
    }
    
}