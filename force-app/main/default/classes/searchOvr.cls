public with sharing class searchOvr extends searchEncrypted{
    
    public Ovr_Control__c[] lstOvr {get; private set;}
    public OVR_Claim__c[] lstOvrClaims {get; private set;}
    public OVR_Claim__c[] lstOvrChecks {get; private set;}
    
    public searchOvr(string searchBy ){
        
        if(searchBy != null){
            search(searchBy );
        }        
    }
    
    public searchOvr(){
        //lstOvr = new Ovr_Control__c[]{};
        //lstOvrClaims  = new OVR_Claim__c[]{};
        
        string searchBy = ApexPages.currentPage().getParameters().get('st');
        
        if(searchBy !=null){
            search(searchBy );
        }
        
    }
    
    void search(string searchBy){
    
        string[] foo = new string[]{};
        
        setSearchTerm(searchBy);
        
        string[] searchFields = new string[]{};
        searchFields.add('Client__c');
        searchFields.add('Comments__c');
        searchFields.add('Payee__c');
        searchFields.add('Payor__c');
        searchFields.add('name');
        searchFields.add('Type__c');
        searchFields.add('createdDate');
        searchFields.add('createdby.name');
        searchFields.add('createdby.id');
        
        
        string searchString = '';
        
        for(String s : searchFields){
            searchString = searchString+','+s;
        }
        
        searchString = setSearchString(searchFields,'ovr_control__c');
        lstOvr = search(searchString, searchFields);
        
        searchFields = new string[]{};
        
        searchFields.add('name');
        searchFields.add('Claim_Number__c');
        searchFields.add('Comments__c');
        searchFields.add('ESSN__c');
        searchFields.add('createdDate');
        searchFields.add('createdby.name');
        searchFields.add('ovr_control__c');
        searchFields.add('ovr_control__r.name');
        
        for(String s : searchFields){
            searchString = searchString+','+s;
        }
        
        searchString = setSearchString(searchFields,'ovr_claim__c');
        lstOvrClaims = search(searchString, searchFields);
    
        searchFields = new string[]{};
        
        searchFields.add('name');
        searchFields.add('createdDate');
        searchFields.add('createdby.name');
        searchFields.add('ovr_control__c');
        searchFields.add('ovr_control__r.name');
        
        searchString = setSearchString(searchFields,'ovr_Check__c');
        lstOvrChecks = search(searchString, searchFields);
    
    }
    
}