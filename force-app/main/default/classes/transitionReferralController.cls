public with sharing class transitionReferralController {

   // public string orf {get; private set;}
    public patient_case__c obj {get; set;}
    public map<string,string> miscMap {get; private set;}
    transient selectOption[] referredFacilityOpts;
        
    public transitionReferralController () {//Referred_Facility__c,
        obj = [select id,client__c, Referred_Facility__c ,Client_Facility__c , ecen_procedure__c, Current_Nicotine_User__c , Previously_Referred_Facility__c,Converted_Date__c,Previously_Referred_Date__c,nReferred_facility__c,recordtype.name,Transition_Reason__c,isConverted__c  from patient_case__c where id = :apexpages.currentpage().getparameters().get('id')];
       // orf=obj.nReferred_facility__c;
        miscMap = new map<string,string>();
        miscMap.put('convertresult',''); 
    }

    public void referCase(){
        string result;
        miscMap.put('convertresult','false'); 
        try{
            result =convertCase.walmartPreLoader(obj.id,'pc');
            system.debug(result);
            if(result=='1'){ 
                //update obj;
                miscMap.put('convertresult','true');   
                
            }else{
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, result));
            }
            
        }catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
        }
        
    }
    
    public selectOption[] getreferredFacilityOpts(){
        return coeSelfAdminPickList.selectOptionList(obj, obj.client__c, obj.ecen_procedure__c, null, coeSelfAdminPickList.referredFacility, obj.nReferred_facility__c, true);
    }
}