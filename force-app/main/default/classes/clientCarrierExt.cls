public with sharing class clientCarrierExt {
    Client_Carrier__c cc;

    public string[] carrierContactList {
        get; private set;
    }
    public string[] carrierAccumContactList {
        get; private set;
    }                                                           //5-26-21 added failed fiest run 8-19-21
    public Client_Employee_Healthplan__c[] employeePlans {
        get; private set;
    }
    public Client_Employee_Healthplan__c employeePlan  {
        get; set;
    }

    public boolean isError {
        get; private set;
    }

    public clientCarrierExt(ApexPages.StandardController controller) {
        cc = (Client_Carrier__c)controller.getRecord();
        loadcarrierContactList();
        loadcarrierAccumContactList(); //5-26-21 added
        loademployeePlans();
    }

    void loadcarrierContactList(){
        if (cc.Carrier_Email__c != null) {
            carrierContactList = cc.Carrier_Email__c.split(',');
        }
    }

      //'5-26-21 added -------------start--------------------------------------->
    void loadcarrierAccumContactList(){
        if (cc.Accum_Email__c != null) {
            carrierAccumContactList = cc.Accum_Email__c.split(',');
        }
    }
    //'5-26-21 added -------------end--------------------------------------->

    void loademployeePlans(){
        employeePlans = new Client_Employee_Healthplan__c[] {};
        employeePlans = [select Annual_Out_of_Pocket_Max_Assc_and_Dep__c, Associate_and_Family_Deductible__c, Associate_and_Dependents_Deductible__c, Associate_Only_Deductible__c, Annual_Out_of_Pocket_Max_Individual__c, Annual_Out_of_Pocket_Max_Family__c, name, createdDate, createdBy.name from Client_Employee_Healthplan__c where client_carrier__c = :cc.id];
    }

    public void addccEmail(){
        string emailAddress = ApexPages.CurrentPage().getParameters().get('emailAddress');

        ApexPages.CurrentPage().getParameters().put('emailAddress', null);

        if (emailAddress != null && emailAddress != '') {
            if (cc.Carrier_Email__c != null && cc.Carrier_Email__c != '' && cc.Carrier_Email__c.indexOf(',') > 0 && cc.Carrier_Email__c.right(1) != ',') {
                cc.Carrier_Email__c = cc.Carrier_Email__c + ',';
            }

            if (cc.Carrier_Email__c == null) {
                cc.Carrier_Email__c = '';
            }

            cc.Carrier_Email__c = cc.Carrier_Email__c + emailAddress + ',';

            try{
                update cc;
            }catch (dmlexception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
                return;
            }

            loadcarrierContactList();
        }
        return;
    }

    public void removeccEmail(){
        string emailAddress = ApexPages.CurrentPage().getParameters().get('emailAddress');

        ApexPages.CurrentPage().getParameters().put('emailAddress', null);

        if (emailAddress != null && emailAddress != '') {
            if (cc.Carrier_Email__c != null && cc.Carrier_Email__c != '') {
                string carrierContact = '';

                for (string s : carrierContactList) {
                    if (s != emailAddress) {
                        carrierContact = carrierContact + s + ',';
                    }
                }
                cc.Carrier_Email__c = carrierContact;
                update cc;
                loadcarrierContactList();
            }
        }
    }
      //'5-26-21 added -------------start--------------------------------------->
    public void addccaEmail(){
        string emailAddress = ApexPages.CurrentPage().getParameters().get('emailAddress');

        ApexPages.CurrentPage().getParameters().put('emailAddress', null);

        if (emailAddress != null && emailAddress != '') {
            if (cc.Accum_Email__c != null && cc.Accum_Email__c != '' && cc.Accum_Email__c.indexOf(',') > 0 && cc.Accum_Email__c.right(1) != ',') {
                cc.Accum_Email__c = cc.Accum_Email__c + ',';
            }

            if (cc.Accum_Email__c == null) {
                cc.Accum_Email__c = '';
            }

            cc.Accum_Email__c = cc.Accum_Email__c + emailAddress + ',';

            try{
                update cc;
            }catch (dmlexception e) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
                return;
            }

            loadcarrierAccumContactList();
        }
        return;
    }

    public void removeccaEmail(){
        string emailAddress = ApexPages.CurrentPage().getParameters().get('emailAddress');

        ApexPages.CurrentPage().getParameters().put('emailAddress', null);

        if (emailAddress != null && emailAddress != '') {
            if (cc.Accum_Email__c != null && cc.Accum_Email__c != '') {
                string carrierContact = '';

                for (string s : carrierAccumContactList) {
                    if (s != emailAddress) {
                        carrierContact = carrierContact + s + ',';
                    }
                }
                cc.Accum_Email__c = carrierContact;
                update cc;
                loadcarrierAccumContactList();
            }
        }
    }

    //'5-26-21 added -------------end--------------------------------------->

    public void editEmployeePlan(){
        string planId = ApexPages.CurrentPage().getParameters().get('planId');

        ApexPages.CurrentPage().getParameters().put('planId', null);

        for (Client_Employee_Healthplan__c ep : employeePlans) {
            if (ep.id == planId) {
                employeePlan = ep;
                break;
            }
        }
    }

    public void deleteEmployeePlan(){
        string planId = ApexPages.CurrentPage().getParameters().get('planId');

        ApexPages.CurrentPage().getParameters().put('planId', null);

        employeePlan = new Client_Employee_Healthplan__c(id = planId);

        try{
            delete employeePlan;
        }catch (dmlexception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            return;
        }

        loademployeePlans();
    }

    public void newEmployeePlan(){
        employeePlan = new Client_Employee_Healthplan__c(client_carrier__c = cc.id);
    }

    public void employeePlanSave(){
        isError = false;
        try{
            upsert employeePlan;
            system.debug(employeePlan);
            loademployeePlans();
        }catch (dmlexception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(0)));
            isError = false;
            return;
        }
    }
}