public with sharing class intakeSearch{
    
    public string searchTerms {get; set;}   
    public string searchField {get; set;} 
    public string searchStartDate {get;set;}
    public string searchEndDate {get;set;}
    public string preSetDateSelection {get;set;}
    datetime ssd, esd;
    
    public customer_care__c cfoo {get; set;}
    
    public customer_care__c[] ccList {get; private set;} 
    public customer_care_inquiry__c[] cciList {get; private set;} 
    
    public map<string, string> clientMap {get; private set;}
    
    public intakeSearch(){
        this.searchTerms = ApexPages.CurrentPage().getParameters().get('st');
        this.searchField = ApexPages.CurrentPage().getParameters().get('searchfield');
        this.searchStartDate= ApexPages.CurrentPage().getParameters().get('searchStartDate');//9-5-18 o.brown
        this.searchEndDate= ApexPages.CurrentPage().getParameters().get('searchEndDate');//9-5-18 o.brown
      
        search();
    }

    //public intakeSearch(string searchTerms, string searchfield){ //9-5-18 o.brown update intakeSearch method to take in the search start and search end dates with the search terms and searchfield 
    
    public intakeSearch(string searchTerms, string searchfield){
    
        this.searchTerms = searchTerms;
        this.searchField = searchfield;
       
    }
    
    
    public intakeSearch(string searchTerms, string searchfield,string searchStartDate,string searchEndDate){
        this.searchTerms = searchTerms;
        this.searchField = searchfield;
        this.searchStartDate=searchStartDate;//9-5-18 o.brown 
        this.searchStartDate=searchEndDate;//9-5-18 o.brown
        search();
    }

    

    
    
    public void setDates(){
        system.debug('preSetDateSelection ' + preSetDateSelection);
        //searchEndDate=null;
        searchStartDate=null;

        if(preSetDateSelection=='1'){
           searchStartDate=utilities.formatUSdate(date.today().addDays(-1)); //returns date format MM/DD/YEAR
        }
        else if(preSetDateSelection=='7'){
            searchStartDate=utilities.formatUSdate(date.today().addDays(-7)); //returns date format MM/DD/YEAR
        }
        else if(preSetDateSelection=='14'){
            searchStartDate=utilities.formatUSdate(date.today().addDays(-14)); //returns date format MM/DD/YEAR
                
        } 
        else if(preSetDateSelection=='30'){
            searchStartDate = utilities.formatUSdate(date.today().addDays(-30)); //returns date format MM/DD/YEAR
                   
        }
        else if(preSetDateSelection=='6M'){
            searchStartDate = utilities.formatUSdate(date.today().addmonths(-6)); //returns date format MM/DD/YEAR
        }
        else if(preSetDateSelection=='1Y'){
            searchStartDate = utilities.formatUSdate(date.today().addyears(-1)); //returns date format MM/DD/YEAR        
        }
        
    }
    
    public void newSearch(){
        this.searchTerms = ApexPages.CurrentPage().getParameters().get('newst');
        this.searchField = ApexPages.CurrentPage().getParameters().get('searchfield');
        search();
    }
    
    public void search(){
        
        date fooDate=date.valueof('1900-01-01');
        
        if(searchStartDate!=null){
            try{
                string[] d = searchStartDate.split('/');
                fooDate = date.valueof(d[2]+ '-' + d[0] + '-' + d[1]);
                system.debug(foodate);
                ssd=datetime.newInstance(fooDate.year(),fooDate.month(),fooDate.day());
            
            }catch(exception e){
                //TODO:
                //Display caught error
            }
        }else{
           ssd=datetime.newInstance(fooDate.year(),fooDate.month(),fooDate.day()); 
            
        }
        
        if(searchEndDate==null){
            fooDate=date.valueof('2900-01-01');
            esd=datetime.newInstance(fooDate.year(),fooDate.month(),fooDate.day());
        }else{
            try{
                string[] d = searchEndDate.split('/');
                fooDate = date.valueof(d[2]+ '-' + d[0] + '-' + d[1]);
                
                esd=datetime.newInstance(fooDate.year(),fooDate.month(),fooDate.day());
                
            }catch(exception e){
                system.debug(e.getMessage());
            }
        } 

        ccList = new customer_care__c[]{};
        cciList = new customer_care_inquiry__c[]{};
        
        if(searchField=='intakeNumber'){
            searchIntakeNumber();
            return;
        
        }else if(searchField=='eSSN'){
            searchESSN();
            return;
        
        }else if(searchField=='lastName'){
            searchLastName();
            return;
            
        }else if(searchField=='claims'){
            if(searchTerms.contains('-')){
                searchTerms = searchTerms.replaceAll('-','');    
            }
            searchClaims();
            return;
            
        }else if(searchField== 'all' || searchField=='' || searchfield == null){
            predictiveSearch();
            return; 
        
        }
        
        if(searchTerms==null || searchTerms==''){return;}
        string st = searchTerms;
        st = searchTerms.toLowerCase().trim();
        
        if(st.indexOf('intake number-')>-1){
            st = st.right(st.indexOf('-'));
        }
        
        set<customer_care__c> ccSet = new set<customer_care__c>();
        set<customer_care_inquiry__c> cciSet = new set<customer_care_inquiry__c>();
        
        string[] stFinal;
        stFinal = st.split(' ');
        
        string result;
        
        for(customer_care__c[] cList : [select Caller_Name__c,
                                         Caller_Phone_Number__c,
                                        // EESSN__c,
                                         Client__c,
                                         Employee_Name__c,
                                         Provider_Name__c,
                                         underwriter__c,
                                         name,
                                         createdDate, 
                                         createdby.name,
                                         owner.firstName,
                                         Relationship_to_Caller__c,
                                         Last_modified_date__c,
                                         Last_modified_by__c,
                                         owner.lastName from customer_care__c where createdDate >= :ssd and createdDate <= :esd order by createdDate desc]){
                                         //owner.lastName from customer_care__c where createdDate >= 2018-08-30T00:00:00Z and createdDate <= 2018-08-31T00:00:00Z]){
                                         
            for(customer_care__c c :clist){                         
            result = c.Caller_Name__c+';'+c.Caller_Phone_Number__c+';'+c.EESSN__c+';'+c.Employee_Name__c+';'+c.Provider_Name__c+';'+c.name+';'+c.owner.firstName+';'+c.owner.lastName;
            result = result.toLowerCase();
            
                for(string s : stFinal){
                if(!ccSet.contains(c) && result.indexOf(s)>-1){
                    ccSet.add(c);
                    setClient(c.Underwriter__c);
                    }
                
                }
            
            }
        
        }
        
        if(ccSet.size()>0){
            cclist = new List<customer_care__c>(ccSet);

        }
        
        //Search Customer care inquires
        //9-2-18 o.brown added ssd and esd to filter the results if an option is selected.
        for(Customer_Care_Inquiry__c c : [select Patient_Name__c,
                                                 Customer_Care__r.EESSN__c,
                                                 Customer_Care__r.Employee_Name__c,
                                                 Customer_Care__r.Provider_Name__c,
                                                 Customer_Care__r.Underwriter__c,
                                                 Customer_Care__r.Client__c,
                                                 Customer_Care__r.Name,
                                                 createdDate, 
                                                 createdby.name,
                                                 Customer_Care__c,
                                                 Relationship_Friendly__c,//09-27-18 added
                                                 Work_Department__c,
                                                 Last_modified_date__c,
                                                 Last_modified_by__c,
                                                 Inquiry_Reason__c,
                                                 Inquiry_Status__c from Customer_Care_Inquiry__c where createdDate >= :ssd and createdDate <= :esd order by createdDate desc limit :Limits.getLimitQueryRows()-Limits.getQueryRows()]){
                                         
            result = c.Patient_Name__c+';'+c.Customer_Care__r.EESSN__c+';'+c.Customer_Care__r.Employee_Name__c+';'+c.Customer_Care__r.Provider_Name__c;
            result = result.toLowerCase();
            
            for(string s : stFinal){
                
                if(!cciSet.contains(c) && result.indexOf(s)>-1){
                    cciSet.add(c);
                    setClient(c.Customer_Care__r.Underwriter__c);
                }
                
            }
        
        }
        
        if(cciSet.size()>0){
            ccilist = new List<Customer_Care_Inquiry__c>(cciSet);

        }
    
    }
    
    void searchIntakeNumber(){
    
        set<id> ccSet = new set<id>();
        string oldsearchTerms = searchTerms;
        
        if(searchTerms.contains('-')){
            searchTerms = searchTerms.substringAfter('-');
        }
        
        searchTerms = '%'+searchTerms;
        
        for(customer_care__c[] cList : [select Caller_Name__c,
                                         Caller_Phone_Number__c,
                                         EESSN__c,
                                         Client__c,
                                         Employee_Name__c,
                                         Provider_Name__c,
                                         underwriter__c,
                                         name,
                                         createdDate, 
                                         createdby.name,
                                         owner.firstName,
                                         Last_modified_date__c,
                                         Last_modified_by__c,
                                         owner.lastName from customer_care__c where name like :searchTerms and createdDate >= :ssd and createdDate <= :esd order by createdDate desc]){
                                        
                                         
            for(customer_care__c c :clist){ 
                setClient(c.Underwriter__c);
                cclist.add(c);
                ccSet.add(c.id);
            }
        
        }

        for(Customer_Care_Inquiry__c c : [select Patient_Name__c,
                                                 Customer_Care__r.EESSN__c,
                                                 Customer_Care__r.Employee_Name__c,
                                                 Customer_Care__r.Provider_Name__c,
                                                 Customer_Care__r.Underwriter__c,
                                                 Customer_Care__r.Client__c,
                                                 Customer_Care__r.Name,
                                                 createdDate, 
                                                 createdby.name,
                                                 Last_modified_date__c,
                                                 Last_modified_by__c,
                                                 Customer_Care__c,
                                                 Work_Department__c,
                                                 Inquiry_Reason__c,
                                                 Inquiry_Status__c from Customer_Care_Inquiry__c where Customer_Care__c in :ccSet order by createdDate desc]){
        
            ccilist.add(c);
        
        }   
        
        searchTerms = oldsearchTerms;
        
    }
    
    void searchESSN(){
        
        if(searchTerms==null){
            return;
        }
        
        string searchFilter; 
        try{
          searchFilter= searchTerms.right(4);
        }catch(exception e){return;} 
        set<id> ccSet = new set<id>();
        searchTerms = searchTerms.replaceAll('-','');
       
        for(customer_care__c[] cList : [select Caller_Name__c,
                                         Caller_Phone_Number__c,
                                         EESSN__c,
                                         Client__c,
                                         Employee_Name__c,
                                         Provider_Name__c,
                                         underwriter__c,
                                         name,
                                         Intake_Notes__c,
                                         Last_modified_date__c,
                                         Last_modified_by__c,
                                         createdDate, 
                                         createdby.name,
                                         Relationship_to_Caller__c,
                                         owner.firstName,
                                         owner.lastName from customer_care__c where ESSN__c = :searchFilter and createdDate >= :ssd and createdDate <= :esd order by createdDate desc]){
                                         //
                                         
            for(customer_care__c c :clist){
                if(c.eeSSN__c==searchTerms){ 
                    setClient(c.Underwriter__c);
                    cclist.add(c);
                    ccSet.add(c.id);
                }
            }
        
        }
        
        for(Customer_Care_Inquiry__c c : [select Patient_Name__c,
                                                 Customer_Care__r.EESSN__c,
                                                 Customer_Care__r.Employee_Name__c,
                                                 Customer_Care__r.Provider_Name__c,
                                                 Customer_Care__r.Underwriter__c,
                                                 Customer_Care__r.Client__c,
                                                 Customer_Care__r.Name,
                                                 name,
                                                 createdDate,
                                                 Relationship_Friendly__c, 
                                                 createdby.name,
                                                 Customer_Care__c,
                                                 Work_Department__c,
                                                 Last_modified_date__c,
                                                 Last_modified_by__c,
                                                 Inquiry_Reason__c,
                                                 Inquiry_Number__c,
                                                 Inquiry_Status__c from Customer_Care_Inquiry__c where Customer_Care__c in :ccSet order by createdDate desc limit 50000]){
            
            ccilist.add(c);
        }
        
    }

    void searchClaims(){
        
        if(searchTerms==null || searchTerms.length( )!= 11){
            return;
        }
        
        set<id> ccSet = new set<id>();
        searchTerms = searchTerms.substring(0,3) + '-' + searchTerms.substring(3,9) + '-' + searchTerms.substring(9,11);
        
        for(Customer_Care_Inquiry__c[] cc : [select Patient_Name__c,
                                                 Customer_Care__r.EESSN__c,
                                                 Customer_Care__r.Employee_Name__c,
                                                 Customer_Care__r.Provider_Name__c,
                                                 Customer_Care__r.Underwriter__c,
                                                 Customer_Care__r.Client__c,
                                                 Customer_Care__r.Name,
                                                 createdDate, 
                                                 createdby.name,
                                                 Last_modified_date__c,
                                                 Last_modified_by__c,
                                                 Customer_Care__c,
                                                 Work_Department__c,
                                                 Inquiry_Note__c,
                                                 Inquiry_Reason__c,
                                                 Inquiry_Status__c from Customer_Care_Inquiry__c where Work_Department__c = 'Claims' or (Inquiry_Reason__c = 'Appeal' or Inquiry_Reason__c = 'Problem Claim' or Inquiry_Reason__c = 'Claim status or explanation') order by createdDate desc]){
                                                        
            
            for(Customer_Care_Inquiry__c c : cc){
                if(c.Inquiry_Note__c != null && (c.Inquiry_Note__c.contains(searchTerms) || c.Inquiry_Note__c.contains(searchTerms.replaceAll('-','')))){
                    ccilist.add(c);
                    ccSet.add(c.Customer_Care__c);
                }
            }
            
        
        }
        
        for(customer_care__c c : [select Caller_Name__c,
                                         Caller_Phone_Number__c,
                                         EESSN__c,
                                         Client__c,
                                         Employee_Name__c,
                                         Provider_Name__c,
                                         underwriter__c,
                                         name,
                                         createdDate, 
                                         createdby.name,
                                         Last_modified_date__c,
                                         Last_modified_by__c,
                                         owner.firstName,
                                         owner.lastName from customer_care__c where id in :ccSet order by createdDate desc]){
                                         
            cclist.add(c);
            
        
        }
        
    }
    
    void searchLastName(){
        
        if(searchTerms==null){
            return;
        }
        
        set<id> ccSet = new set<id>();
        string searchFilter = searchTerms.substring(0,1);
        
        for(customer_care__c[] cList : [select Caller_Name__c,
                                         Caller_Phone_Number__c,
                                         EESSN__c,
                                         Client__c,
                                         Employee_Name__c,
                                         Provider_Name__c,
                                         underwriter__c,
                                         Last_modified_date__c,
                                         Last_modified_by__c,
                                         name,
                                         createdDate, 
                                         createdby.name,
                                         owner.firstName,
                                         owner.lastName from customer_care__c where employee_last_name__c = :searchFilter and createdDate >= :ssd and createdDate <= :esd order by createdDate desc]){
                                         
            for(customer_care__c c :clist){
                if(c.Employee_Name__c.contains(searchTerms)){ 
                    setClient(c.Underwriter__c);
                    cclist.add(c);
                    ccSet.add(c.id);
                    
                }
            }
        
        }
        
        for(Customer_Care_Inquiry__c c : [select Patient_Name__c,
                                                 Customer_Care__r.EESSN__c,
                                                 Customer_Care__r.Employee_Name__c,
                                                 Customer_Care__r.Provider_Name__c,
                                                 Customer_Care__r.Underwriter__c,
                                                 Customer_Care__r.Client__c,
                                                 Customer_Care__r.Name,
                                                 createdDate, 
                                                 createdby.name,
                                                 Customer_Care__c,
                                                 Last_modified_date__c,
                                                 Last_modified_by__c,
                                                 Work_Department__c,
                                                 Inquiry_Reason__c,
                                                 Inquiry_Status__c from Customer_Care_Inquiry__c where (createdDate >= :ssd and createdDate <= :esd) and (Customer_Care__c in :ccSet or Patient_Last_Name__c = :searchFilter) order by createdDate desc]){ //9-2-18 o.brown update
                                                 
                                                 
                                                 
            if(ccSet.contains(c.customer_care__c) || c.Patient_Name__c.contains(searchTerms)){
                ccilist.add(c);
            }
        }
        
    }
    
    void predictiveSearch(){
        
        system.debug('searchTerms: ' +searchTerms);
        
        //if(searchTerms.contains('-')){
        //    searchTerms = searchTerms.substringAfter('-');
        //}
        
        if(searchTerms.contains('-')){
            searchTerms = searchTerms.replaceAll('-','');
        }
        
        if(searchTerms.isNumeric()){
            if(searchTerms.length()==9){
                searchESSN();
            
            }else if(searchTerms.length()==11){    
                searchClaims();
                
            }else{
                searchIntakeNumber();
            }
        }else{
            searchLastName();
        }
        
        if(ccList.size() ==0 || cciList.size() == 0){
            
            try{
                searchField = 'noresults';
                search();
            }catch(exception e){
                
            }
        }
    
    }
    
    void loadClientMap(){
    
        if(clientMap==null){
            clientMap = new map<string, string>();
            //client = new List<SelectOption>();
            //client.add(new SelectOption('', ''));
            for(Claims_Audit_Client__c  c : [select underwriter__c, name from Claims_Audit_Client__c where underwriter__c != '502' order by name asc]){
                clientMap.put(c.underwriter__c, c.name);
            //    client.add(new SelectOption(c.underwriter__c, c.name));
            }
            //system.debug(client);
            
        }
    
    }

    void setClient(string uw){
        loadClientMap();
        string foo = '';
        foo= clientMap.get(uw);
        if(foo==null){
            clientMap.put(uw, uw);
        }
    }
    
    void setupDatetimes(string start, string stop){
        string[] d = start.split('/');
        date fooDate = date.valueof(d[0]+ '-' + d[1] + '-' + d[2]);
        ssd=datetime.newInstance(fooDate.year(),fooDate.month(),fooDate.day());
        
        d = stop.split('/');
        fooDate = date.valueof(d[0]+ '-' + d[1] + '-' + d[2]);
        esd=datetime.newInstance(fooDate.year(),fooDate.month(),fooDate.day());
    }
}