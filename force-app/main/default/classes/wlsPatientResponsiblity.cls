public without sharing class wlsPatientResponsiblity{

    
    public static Decimal OopRemaining(string coverageLevel, Decimal individual, Decimal family){
        
        if(coverageLevel==null || (individual==null && family==null) ){
            return null;
            
        }else{
            if(coverageLevel=='Associate Only'){
                return individual;    
                
            }else{
                if(individual<family){
                    return individual;
                }else{
                    return family;
                }
            }
        }
        
    }
    
    public static Decimal DeductibleRemaining(Decimal AnnualDeductible, Decimal deductibleMet){
        
        if(AnnualDeductible ==null || deductibleMet ==null){
            return null;
        }else{
            return AnnualDeductible - deductibleMet;
        }
        
    }
    
    public static Decimal PreResponsibilityAmount(Decimal deductibleRemaining, Decimal oopRemaining, string Facility){
        
        decimal cost = 750.00;
        
        if(Facility=='Scripps Mercy'){
            cost = 775.00;
            
        }else if(Facility=='Geisinger'){
            cost = 864.00;
            
        }else if(Facility=='Northeast Baptist'){
            cost = 700.00;
            
        }
        
        if(deductibleRemaining <= 0.00){
            
            decimal twentyfivepercent = cost *0.25;
            
            if(oopRemaining > 0){
                
                if(oopRemaining < twentyfivepercent){
                    return oopRemaining;
                }else{
                    return twentyfivepercent;
                }    
            }else{
                return 0.00;    
            }
            
        }else if(deductibleRemaining >= cost){
        
            return cost;
            
        }else if(deductibleRemaining < cost){
            
            decimal potentialCost = deductibleRemaining + ((cost-deductibleRemaining)*0.25);
            
            if(potentialCost > oopRemaining){
                return oopRemaining;            
            }else{
                return potentialCost;
            }
            
        }
        
        return 0.00;
    }

    public static Decimal GlobalResponsibilityAmount(Decimal deductibleRemaining, Decimal oopRemaining, string facility, string procedure, string clinicalCode){
        
        decimal cost = 0.00;
        
        if(facility=='Scripps Mercy'){
            
            if(clinicalCode=='619'){
                cost = 30890.00;
                
            }else if(clinicalCode=='620'){
                cost = 23558.00;
                    
            }else if(clinicalCode=='621'){
                cost = 18000.00;
                
            }
            
        }else if(Facility=='Geisinger'){
            
            if(procedure=='Bypass'){
                
                if(clinicalCode=='620'){
                    cost = 16031.00;
                
                }else if(clinicalCode=='621'){
                    cost = 16999.00;    
                    
                }
            
            }else if(procedure=='Sleeve'){
                
                if(clinicalCode=='620'){
                    cost = 22511.00;
                
                }else if(clinicalCode=='621'){
                    cost = 19905.00;
                    
                }
            }
            
        }else if(facility=='Northeast Baptist'){
            
            if(clinicalCode=='619'){
                cost = 30260.00;
                
            }else if(clinicalCode=='620'){
                cost = 19760.00;
                    
            }else if(clinicalCode=='621'){
                cost = 18260.00;
                
            }
            
        }
        
        if(deductibleRemaining <= 0.00){
        
            decimal twentyfivepercent = cost *0.25;
            
            if(oopRemaining > 0){
                
                if(oopRemaining < twentyfivepercent){
                    return oopRemaining;
                }else{
                    return twentyfivepercent;
                }    
            }else{
                return 0.00;    
            }
            
        }else if(deductibleRemaining >= cost){
        
            return cost;
            
        }else if(deductibleRemaining < cost){
            
            decimal potentialCost = deductibleRemaining + ((cost-deductibleRemaining)*0.25);
            
            if(potentialCost > oopRemaining){
                return oopRemaining;            
            }else{
                return potentialCost;
            }
            
        }
        
        return 0.00;
    }   
    
}