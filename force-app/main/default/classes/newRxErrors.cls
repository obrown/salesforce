@RestResource(urlMapping='/newrxerrors')
global class newRxErrors {
    @HttpPost
    global static String newRxErrors() {
        RestRequest req = RestContext.request;
        string jsonString = req.requestBody.toString();
        jsonString= jsonString.replaceAll('":"','__c":"');
        jsonString = jsonString.replace('ResultCode__c','ResultCode');
        jsonString = jsonString.replace('ErrorReason__c','ErrorReason');
        
        hpRxSearchStruct hpRxSS = new hpRxSearchStruct();
        try{
        
            hpRxSS = (hpRxSearchStruct)JSON.deserialize(jsonString, hpRxSearchStruct.class);
        
        }catch(exception e){
            return jsonString;
        }
        
        magpie_Error__c[] newErrors = new magpie_Error__c[]{};
        map<string, client__c> clientMap = utilities.clientMap();
        system.debug('Batch Size: ' + hpRxSS.RxTxns);
        for(rxAudit__c r : hpRxSS.RxTxns){
            
            system.debug('\n'+r);
            
            if(r.Underwriter__c=='030' || r.Transtype__c !='I'){
                continue;
            }
            
            magpie_Error__c ne  = new magpie_Error__c();
            
            ne.Underwriter__c = r.Underwriter__c;
            ne.Group__c = r.Grpnbr__c;
            ne.Member_Sequence__c = r.Patseq__c;
            ne.Rx_Claim_Number__c = r.ClaimNbr__c;
            ne.Error_Message__c = r.ErrMsg__c;
            ne.Employee_SSN__c = r.Essn__c;
            ne.File_Name__c = r.Filename__c;
            ne.Transaction_Type__c = r.Transtype__c;
            ne.Source_Type__c = r.Transtype__c;
            ne.Status__c = 'Open';
            
            try{
                ne.Client__c = clientMap.get(r.Underwriter__c).id;
            }catch(exception e){
                ne.Client_Name__c= r.Underwriter__c;
            }
            
            if(r.Dedamount__c!=null){
                ne.Deductible_Amount__c = decimal.valueof(r.Dedamount__c.trim());
            }
            
            if(r.Oopamount__c!=null){
                ne.Out_of_Pocket_Amount__c = decimal.valueof(r.Oopamount__c.trim());
            }
            
            if(r.DOS__c!=null && r.Dos__c.length()==8){
                try{
                ne.Dispensed_Date__c = date.valueof(r.Dos__c.left(4) + '-' + r.Dos__c.mid(4,2) + '-' + r.Dos__c.right(2));
                }catch(TypeException e){}
            }
            
            
            if(r.Transdate__c!=null && r.Transdate__c.length()==8){
                try{
                ne.Transaction_Date__c= date.valueof(r.Transdate__c.left(4) + '-' + r.Transdate__c.mid(4,2) + '-' + r.Transdate__c.right(2));
                }catch(TypeException e){}
            }
            
            newErrors.add(ne);
            
        }
        try{
            upsert newErrors;
            
        }catch(exception e){
            system.debug(e.getMessage());
            return UserInfo.getUserName()+' '+e.getMessage();
        }
        return string.valueof(hpRxSS.RxTxns);
    }
}