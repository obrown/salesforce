public with sharing class transplantEligCheck {


        public void init(){
            
            string csvstringcoehp = createCOEhpcase();
            string csvbariatricHPcase = bariatricHPcase();
            
            if(csvstringcoehp.countMatches('\n')>1){
                mailthecsv(csvstringcoehp,'New COE Cases for HealthPAC','Please find a list of new COE Cases for HealthPAC'); //TODO do something with the boolean response
            }else{
                mailthecsv('1','COE Cases for HeatlhPAC - NONE','There are no new COE Cases for HealthPAC');
                
            }
            
            if(csvbariatricHPcase.countMatches('\n')>1){
                mailthecsv(csvbariatricHPcase,'Bariatric Cases for HealthPAC','Please find a list of Bariatric Cases for HealthPAC'); //TODO do something with the boolean response
            }else{
                mailthecsv('1','Bariatric Cases for HeatlhPAC - NONE','There are no Bariatric Cases for HealthPAC');
                
            }
            
        }
        
        private string isnull(string s){
            string foo =s;
            if(foo==null){
                foo='';
            }
            
            return foo;
        }
        
        private string createCOEhpcase(){
            
            String x='CertID, Employee First Name, Employee Last Name, Employee SSN, Employee DOB, Employee Gender, Relationship to the insured, Patient First Name, Patient Last Name, Patient SSN, Patient DOB, Patient Gender, Employee Street, Employee City, Employee State, Employee Zip, HP Group Number, HP Plan Name, HP Effective Date, HP Termination Date, Coverage level, Carrier \n';
        
            //Patient_Case__c tempP = new Patient_Case__c();
            Patient_Case__c[] plist = new Patient_Case__c[]{};
            
            set<id> cfSet = new set<id>();
            
            try{
            
                for(Patient_Case__c p :[select client_facility__c, carrier_name__c, Client_Carrier__r.name, Coverage_level__c, Employee_Primary_Health_Plan_Name__c,Healthpac_Group__c,Loaded_into_HealthPac__c, certID__c, client_name__c, case_type__c, actual_arrival__c, actual_departure__c, createHP__c, Relationship_to_the_insured__c,
                                           employee_first_name__c, employee_last_name__c, employee_ssn__c, Employee_DOBe__c, employee_street__c, employee_city__c, employee_state__c, Employee_Zip_Postal_Code__c, referred_facility__c,
                                           patient_first_name__c, patient_last_name__c, patient_ssn__c, patient_DOBe__c, patient_street__c, patient_city__c, patient_state__c, patient_Zip_Postal_Code__c, employee_gender__c, patient_gender__c 
                                           from patient_case__c where createHP__c = true]){
                                           
                    if(p.actual_departure__c!=null){
                       plist.add(p);
                       cfSet.add(p.client_facility__c);
             
                    }                         
                
                    
                
                }
            
            if(!plist.isEmpty()){
                
                map<string, string> hpMap = new map<string, string>();
                for(HealthPac_Plan_Code__c hpc : [select Case_Plan_Name__c, client_facility__c,HealthPac_Plan_Code__c from HealthPac_Plan_Code__c where client_facility__c in :cfSet]){
                    hpMap.put(hpc.client_facility__c+''+hpc.case_plan_name__c, hpc.HealthPac_Plan_Code__c);
                }
                for(Patient_Case__c p : plist){
                   
                         x += p.certID__c + ',' + p.employee_first_name__c + ',' + escapenullcsv(p.employee_last_name__c) + ',' + ((p.client_name__c=='PepsiCo') ? ('P' + p.certID__c.right(8)) : p.employee_ssn__c) + ',' + utilities.formatUSdate(p.employee_dobe__c) + ',' + p.employee_gender__c + ',' + p.Relationship_to_the_insured__c + ',' + p.patient_first_name__c + ',' + escapenullcsv(p.patient_last_name__c) + ',' + p.patient_ssn__c + ',' + utilities.formatUSdate(p.patient_dobe__c) + ',' + p.patient_gender__c + ',' +
                        escapenullcsv(p.employee_street__c) + ',' + p.employee_city__c + ',' + p.employee_state__c + ',' + p.Employee_Zip_Postal_Code__c + ',' + p.Healthpac_Group__c + ',' + hpMap.get(p.client_facility__c+''+p.Employee_Primary_Health_Plan_Name__c) + ',' + p.actual_arrival__c + ',' + p.actual_departure__c.addDays(1) + ',' + escapenullcsv(p.Coverage_level__c) + ',' + escapenullcsv(p.carrier_name__c) + '\n'; 
                        p.HealthPac_Plan_Name__c=hpMap.get(p.client_facility__c+''+p.Employee_Primary_Health_Plan_Name__c);
                        p.createHP__c = false;
                        p.Loaded_into_HealthPac__c = date.today();
                }
                
                update plist;
            }
                
            }catch(exception e){
                Messaging.SingleEmailMessage mail = utilities.email('mmartin@hdplus.com',null,null,'System','Error in transplantEligCheck.createCOEhpcase',e.getMessage()+'\n'+e.getLinenumber());
                mail.setReplyTo('noreply@hdplus.com');
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
                return e.getMessage();
                
                
            }   
            
            x = x.replaceAll(' 0:00', '');
            x = x.toUpperCase();
            return x;
            
        }
        
        private string bariatricHPcase(){
            
            String x='CertID, Employee First Name, Employee Last Name, Employee SSN, Employee DOB, Employee Gender, Relationship to the insured, Patient First Name, Patient Last Name, Patient SSN, Patient DOB, Patient Gender, Employee Street, Employee City, Employee State, Employee Zip, HP Group Number, HP Plan Name, HP Effective Date, HP Termination Date, Coverage Level, Carrier\n';
            Bariatric_Stage__c[] bsList = new Bariatric_Stage__c[]{};
            
            for(Bariatric_Stage__c p :[select Bariatric__r.HP_Plan_Name__c,
                                              Bariatric__r.certID__c, 
                                              Bariatric__r.referred_facility__c, 
                                              actual_arrival__c, 
                                              actual_departure__c, 
                                              Determination_Received_at_HDP__c, 
                                              Bariatric__r.carrier_name__c,
                                              Bariatric__r.Coverage_Level__c,
                                              Bariatric__r.Relationship_to_the_insured__c,
                                              Bariatric__r.employee_first_name__c,
                                              Bariatric__r.employee_last_name__c,
                                              Bariatric__r.employee_ssn__c, 
                                              Bariatric__r.Employee_DOB__c, 
                                              Bariatric__r.Employee_Gender__c,
                                              Bariatric__r.employee_street__c,
                                              Bariatric__r.employee_city__c,
                                              Bariatric__r.employee_state__c,
                                              Bariatric__r.Employee_Zip_Code__c,
                                              Bariatric__r.patient_first_name__c,
                                              Bariatric__r.patient_last_name__c,
                                              Bariatric__r.patient_ssn__c,
                                              Bariatric__r.patient_DOB__c,
                                              Bariatric__r.Patient_Gender__c,
                                              Bariatric__r.patient_street__c,
                                              Bariatric__r.patient_city__c,
                                              Bariatric__r.patient_state__c,
                                              Bariatric__r.patient_Zip_Code__c from Bariatric_Stage__c where createHP__c = true and create_HP_Notification__c = null]){
                                           
                if(p.actual_departure__c!=null){
                    x += p.Bariatric__r.certID__c + ',' + p.Bariatric__r.employee_first_name__c + ',' + escapenullcsv(p.Bariatric__r.employee_last_name__c) + ',' + p.Bariatric__r.employee_ssn__c + ',' + utilities.formatUSdate(p.Bariatric__r.employee_dob__c) + ',' +p.Bariatric__r.Employee_Gender__c +','+ p.Bariatric__r.Relationship_to_the_insured__c + ',' + p.Bariatric__r.patient_first_name__c + ',' + escapenullcsv(p.Bariatric__r.patient_last_name__c) + ',' + p.Bariatric__r.patient_ssn__c + ',' + utilities.formatUSdate(p.Bariatric__r.patient_dob__c) + ',' +
                    p.Bariatric__r.Patient_Gender__c +','+escapenullcsv(p.Bariatric__r.employee_street__c) + ',' + p.Bariatric__r.employee_city__c + ',' + p.Bariatric__r.employee_state__c + ',' + p.Bariatric__r.Employee_Zip_Code__c + ',' + 'WLS' + ',' + p.Bariatric__r.HP_Plan_Name__c + ',' + p.actual_arrival__c + ',' + p.actual_departure__c.addDays(1) + ',' + escapenullcsv(p.Bariatric__r.Coverage_Level__c) + ',' + escapenullcsv(p.Bariatric__r.carrier_name__c) + '\n'; 
                                         
                    p.create_HP_Notification__c = date.today();
                    bsList.add(p);
                
                }
            }
            
            update bsList;
            
            x = x.replaceAll(' 0:00', '');
            x = x.toUpperCase();
            return x;
        
        }
        
        public static boolean mailthecsv(string s, string subject, string body){
            boolean foo=true;
            try{
            //hdpeligibility
            Messaging.SingleEmailMessage mail = utilities.email('hdpeligibility@hdplus.com',null,'mmartin@hdplus.com','System',Subject,Body);
            
            if(s!='1'){
                Blob csvBlob = Blob.valueOf(s);
                 Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                 efa.setFileName(subject+'.csv');
                efa.setBody(csvBlob);
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            }
             
            mail.setReplyTo('noreply@hdplus.com');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            
            
            
            }catch (exception e){
                foo = false;    
            }
        
            return foo;
            
        }
                
        string escapenullcsv(string s){
            string foo = s; 
            if(foo!=null){
                foo = foo.escapeCsv();
            }else{
                foo = '';
            }
            return foo; 
        }
                    
}