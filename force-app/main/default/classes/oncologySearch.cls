public with sharing class oncologySearch extends searchEncrypted {

    
    public oncology__c[] searchOncology(string searchBy, string sf){
    
        oncology__c[] results = new oncology__c[]{};
        string[] foo = new string[]{};
        
        setSearchTerm(searchBy);
        
        string[] searchFields = new string[]{};
        searchFields.add('Status__c');
        searchFields.add('Patient_First_Name__c');
        searchFields.add('Patient_Last_Name__c');
        searchFields.add('Employee_First_Name__c');
        searchFields.add('Employee_Last_Name__c');    
        searchFields.add('Patient_DOB__c');
        searchFields.add('Employee_DOB__c');
        searchFields.add('CreatedDate');
            
        if(sf!=null){
            
            if(sf=='caseNumber'){
                searchFields.add('Name');
            }else if(sf=='phone'){
                searchFields.add('Employee_Phone__c');
                searchFields.add('Employee_Mobile_Phone__c');
                searchFields.add('Employee_Other_Phone__c');
                searchFields.add('Patient_Phone__c');
                searchFields.add('Patient_Phone2__c');
            }            
        }else{
            
            
            searchFields.add('Name');
            searchFields.add('Patient_City__c');
            searchFields.add('Patient_State__c');
        
        }
        
        string searchString = '';
        
        for(String s : searchFields){
            searchString = searchString+','+s;
        }
        
        searchString = setSearchString(searchFields,'oncology__c');
        results = search(searchString, searchFields);
        
        for(Oncology__c o : results){
            o.patient_dob__c=utilities.formatUSdate(date.valueof(o.patient_dob__c));
            o.Employee_DOB__c=utilities.formatUSdate(date.valueof(o.Employee_DOB__c));
        }
        
        return results;
    
    }
    
    
}