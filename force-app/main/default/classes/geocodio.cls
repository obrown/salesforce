public class geocodio {

    public class Address_components {
        public String number_Z {get;set;} // in json: number
        public String street {get;set;} 
        public String suffix {get;set;} 
        public String formatted_street {get;set;} 
        public String city {get;set;} 
        public String state {get;set;} 
        public String zip {get;set;} 
        public String country {get;set;} 

        public Address_components(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'number') {
                            number_Z = parser.getText();
                        } else if (text == 'street') {
                            street = parser.getText();
                        } else if (text == 'suffix') {
                            suffix = parser.getText();
                        } else if (text == 'formatted_street') {
                            formatted_street = parser.getText();
                        } else if (text == 'city') {
                            city = parser.getText();
                        } else if (text == 'state') {
                            state = parser.getText();
                        } else if (text == 'zip') {
                            zip = parser.getText();
                        } else if (text == 'country') {
                            country = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Address_components consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Input {
        public Address_components address_components {get;set;} 
        public String formatted_address {get;set;} 

        public Input(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'address_components') {
                            address_components = new Address_components(parser);
                        } else if (text == 'formatted_address') {
                            formatted_address = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Input consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public Input input {get;set;} 
    public List<Results> results {get;set;} 

    public geocodio(JSONParser parser) {
        while (parser.nextToken() != System.JSONToken.END_OBJECT) {
            if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                String text = parser.getText();
                if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                    if (text == 'input') {
                        input = new Input(parser);
                    } else if (text == 'results') {
                        results = arrayOfResults(parser);
                    } else {
                        System.debug(LoggingLevel.WARN, 'JSON2Apex consuming unrecognized property: '+text);
                        consumeObject(parser);
                    }
                }
            }
        }
    }
    
    public class Results {
        public Address_components_Z address_components {get;set;} 
        public String formatted_address {get;set;} 
        public Location location {get;set;} 
        public Double accuracy {get;set;} 
        public String accuracy_type {get;set;} 
        public String source {get;set;} 

        public Results(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'address_components') {
                            address_components = new Address_components_Z(parser);
                        } else if (text == 'formatted_address') {
                            formatted_address = parser.getText();
                        } else if (text == 'location') {
                            location = new Location(parser);
                        } else if (text == 'accuracy') {
                            accuracy = parser.getDoubleValue();
                        } else if (text == 'accuracy_type') {
                            accuracy_type = parser.getText();
                        } else if (text == 'source') {
                            source = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Results consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Address_components_Z {
        public String number_Z {get;set;} // in json: number
        public String predirectional {get;set;} 
        public String street {get;set;} 
        public String suffix {get;set;} 
        public String formatted_street {get;set;} 
        public String city {get;set;} 
        public String county {get;set;} 
        public String state {get;set;} 
        public String zip {get;set;} 
        public String country {get;set;} 

        public Address_components_Z(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'number') {
                            number_Z = parser.getText();
                        } else if (text == 'predirectional') {
                            predirectional = parser.getText();
                        } else if (text == 'street') {
                            street = parser.getText();
                        } else if (text == 'suffix') {
                            suffix = parser.getText();
                        } else if (text == 'formatted_street') {
                            formatted_street = parser.getText();
                        } else if (text == 'city') {
                            city = parser.getText();
                        } else if (text == 'county') {
                            county = parser.getText();
                        } else if (text == 'state') {
                            state = parser.getText();
                        } else if (text == 'zip') {
                            zip = parser.getText();
                        } else if (text == 'country') {
                            country = parser.getText();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Address_components_Z consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    public class Location {
        public Double lat {get;set;} 
        public Double lng {get;set;} 

        public Location(JSONParser parser) {
            while (parser.nextToken() != System.JSONToken.END_OBJECT) {
                if (parser.getCurrentToken() == System.JSONToken.FIELD_NAME) {
                    String text = parser.getText();
                    if (parser.nextToken() != System.JSONToken.VALUE_NULL) {
                        if (text == 'lat') {
                            lat = parser.getDoubleValue();
                        } else if (text == 'lng') {
                            lng = parser.getDoubleValue();
                        } else {
                            System.debug(LoggingLevel.WARN, 'Location consuming unrecognized property: '+text);
                            consumeObject(parser);
                        }
                    }
                }
            }
        }
    }
    
    
    public static geocodio parse(String json) {
        System.JSONParser parser = System.JSON.createParser(json);
        return new geocodio(parser);
    }
    
    public static void consumeObject(System.JSONParser parser) {
        Integer depth = 0;
        do {
            System.JSONToken curr = parser.getCurrentToken();
            if (curr == System.JSONToken.START_OBJECT || 
                curr == System.JSONToken.START_ARRAY) {
                depth++;
            } else if (curr == System.JSONToken.END_OBJECT ||
                curr == System.JSONToken.END_ARRAY) {
                depth--;
            }
        } while (depth > 0 && parser.nextToken() != null);
    }

    private static List<Results> arrayOfResults(System.JSONParser p) {
        List<Results> res = new List<Results>();
        if (p.getCurrentToken() == null) p.nextToken();
        while (p.nextToken() != System.JSONToken.END_ARRAY) {
            res.add(new Results(p));
        }
        return res;
    }

}