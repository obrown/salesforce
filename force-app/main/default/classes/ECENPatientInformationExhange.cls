/**
 * @description       : 
 * @author            : ChangeMeIn@UserSettingsUnder.SFDoc
 * @group             : 
 * @last modified on  : 12-22-2020
 * @last modified by  : ChangeMeIn@UserSettingsUnder.SFDoc
 * Modifications Log 
 * Ver   Date         Author                               Modification
 * 1.0   12-22-2020   ChangeMeIn@UserSettingsUnder.SFDoc   Initial Version
**/
@RestResource(urlMapping='/ecen/v2/patientInfo')
global class ECENPatientInformationExhange{
     
     @HttpGet   
     global static ecenPatientInformation doGet() {
        
        String ins_id = RestContext.request.headers.get('Ins_id');
        
         if(ins_id==null){
            return null;
        }
        
        ecenPatientInformation pinfo = new ecenPatientInformation();
        ecenDashBoardStruct dashboard = new ecenDashBoardStruct();
        ecenTaskList TaskList = new ecenTaskList();
        pinfo.dashboard = dashboard;
        pinfo.dashboard.WelcomeMessage = 'Thank you for signing into the Contigo Health application.  In order to use this application, you must have an open case with Contigo Health created/open through your employer.    If you feel you have received this message in error, please call 888-463-3737.';
        pinfo.dashboard.ToDoItemsComplete='0%';
        pinfo.TaskList = TaskList;   
        
        /* ************ Oncology ***************** */
        
        oncology__c oncology = new oncology__c();
        
        try{
           oncology = [select Booking_Reference_Number__c,
                              client_facility__r.facility__r.name,
                              Date_HDP_Forms_Completed__c,
                              Determination_Date__c,
                              Determination_Outcome__c,
                              HDP_Program_Forms_Completed__c,
                              patient_first_name__c,
                              patient_last_name__c,
                              referral_date__c,
                              Send_POC_Auth__c from oncology__c where bid_id__c = :ins_id order by createdDate desc limit 1];
           system.debug('calling oncology tasks');                   
           ecenOncologyTask ecenOncologyTask = new ecenOncologyTask();  
           pinfo = ecenOncologyTask.getPInfo(oncology);   
            return pinfo;
        }catch(queryException e){
           //catch silent 
           system.debug(e.getMessage());
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
           }
           
        }
        
        /* ************** Patient Case ************* */
        
        patient_case__c pc = new patient_case__c();
        
        try{
        
           pc = [select actual_arrival__c, amex_trip_id__c,
                        Caregiver_Responsibility_Received__c,converted_date__c, nClient_Name__c, client_facility__c,client_facility__r.client__r.name, client_facility__r.facility__r.name, client_facility__r.contact_number__c,current_nicotine_user__c,
                        displayAuthNumber__c,
                        Hotel_Check_In_Date__c,Hotel_Checkout_Date__c,
                        Estimated_Arrival__c,
                        Financial_Responsibility_Waiver__c,
                        Nicotine_Test_Scheduled__c,
                        Patient_First_Name__c, Patient_Last_Name__c, Patient_Cleared_to_Travel__c, Plan_of_Care_accepted_at_HDP__c,Program_Authorization_Received__c,program_type__c,
                        Reason__c,referred_facility__c,
                        status_reason__c from patient_case__c where bid_id__c = :ins_id order by createdDate desc limit 1];
        
        }catch(queryException e){
           //catch silent 
           system.debug(e.getMessage());
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
           }
           
        }
        
        if(pc.id!=null){
        
            ecenPatientCaseTask epct = new ecenPatientCaseTask();
            pinfo = epct.getPInfo(pc);
          
            return pinfo;
        }
        
        /* *************** Bariatric ****************** */
        
        //Bariatric
        
        bariatric_stage__c bariatric = new bariatric_stage__c();
        
        try{
           bariatric = [select Booking_Reference_Number__c,
                               displayAuthNumber__c,
                               recordtype.name,
                               Estimated_Hospital_Admit__c,
                               Program_Forms_Received_Date__c,
                               Determination_Received_at_HDP__c,
                               Patient_Accepted_for_Weight_Loss_Surgery__c,
                               Caregiver_Form_Received__c,
                               Plan_of_Care_Accepted_at_HDP__c,
                               X2nd_Nutritional_Counseling_Date__c,
                               Bariatric__r.patient_last_name__c,
                               Bariatric__r.patient_first_name__c,
                               bariatric__r.client_facility__r.facility__r.name,
                               bariatric__r.Referral_Date__c,
                               bariatric__r.Current_Nicotine_User__c from bariatric_stage__c where bariatric__r.bid_id__c = :ins_id and recordtype.name !='Post' order by createdDate desc limit 1];
                              
           ecenBariatricTask ecenBariatricTask = new ecenBariatricTask();  
           pinfo = ecenBariatricTask.getPInfo(bariatric);   
        
        }catch(queryException e){
           //catch silent 
           system.debug(e.getMessage());
           if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
           }
           
        }
        
        if(test.isRunningTest()){
               RestContext.response.responseBody= blob.valueof(JSON.serialize(pinfo));
        }
        
        return pinfo;
            
     }
    
}