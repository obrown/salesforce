public with sharing class directBillingBatchInvoiceController {

    
    public Direct_Billing_Invoice__c[] invoices {get;set;}
    public map<id, Direct_Billing_Invoice__c> invoicesMap {get; private set;}
    public map<string, Direct_Billing_Invoice_Pay_Date__c[]> invoicePayDateMap {get; private set;}
    public map<string, string> invoicePayDatesIncluded {get; private set;} 
    public set<string> years {get; private set;}
    public map<string, set<string>> yearsMap {get; private set;}
    public map<string, integer> yearsMapCount {get; private set;}
    public integer invCount {get; set;}
   
    
    public set<id> leaves {get; private set;}
    
    public directBillingBatchInvoiceController(){
        loadInvoices();
        
    }
    
    void loadInvoices(){
        
        date invoiceMonth = date.today();
        invoiceMonth = date.valueof(invoiceMonth.year()+'-'+invoiceMonth.month()+'-01');
         //leaveMap = new map<id, Direct_Billing_Leave__c>();
         leaves = new set<id>();
         set<id> invSet = new set<id>();
         invoicesMap = new map<id, Direct_Billing_Invoice__c>();
         invoicePayDateMap = new map<string, Direct_Billing_Invoice_Pay_Date__c[]>();
         invoicePayDatesIncluded = new map<string, string>();
         years = new set<string>();
         yearsMap = new map<string, set<string>>();
         yearsMapCount = new map<string, integer>();
         
         for(Direct_Billing_Invoice_Pay_Date__c invoicePayDate : [select db_leave__c,DB_Invoice__c,
                                                                         DB_Invoice__r.db_leave__r.DB_Member__r.Address_1__c,
                                                                         DB_Invoice__r.db_leave__r.DB_Member__r.Address_2__c,
                                                                         DB_Invoice__r.db_leave__r.DB_Member__r.City__c,
                                                                         DB_Invoice__r.db_leave__r.DB_Member__r.State__c,
                                                                         DB_Invoice__r.db_leave__r.DB_Member__r.Zip_Code__c,
                                                                         DB_Invoice__r.db_leave__r.Welcome_Letter_Sent__c,
                                                                         DB_Invoice__r.db_leave__r.Monthly_Premium__c,
                                                                         DB_Invoice__r.db_leave__r.DB_Member__r.Employee_Name__c,
                                                                         Medical_Premium__c,
                                                         Medical_Parma_Premium__c,
                                                         db_leave__r.Identity_Theft_Election__c,
                                                         db_leave__r.Identity_Theft_Tier__c,
                                                         Identity_Theft_Premium__c,
                                                         db_leave__r.Indemnity_Plan_Election__c,
                                                         db_leave__r.Indemnity_Plan_Tier__c,
                                                         Indemnity_Plan_Premium__c,  
                                                         db_leave__r.Legal_Plan_Election__c,
                                                         db_leave__r.Legal_Plan_Tier__c,
                                                         Legal_Plan_Premium__c,                                                                                                                
                                                         DB_Invoice__r.pastDueLetterNeeded__c ,
                                                         db_leave__r.Medical_Election__c,
                                                         db_leave__r.Medical_Tier__c,
                                                         db_leave__r.Medical_Parma_Election__c,
                                                         DB_Invoice__r.db_leave__r.Dental_Election__c,         
                                                         DB_Invoice__r.db_leave__r.Dental_Tier__c,   
                                                         db_invoice__r.pay_dates_included__c,      
                                                         Dental_Premium__c,
                                                         Premium_Amount__c,
                                                         DB_Invoice__r.db_leave__r.Vision_Election__c,
                                                         DB_Invoice__r.db_leave__r.vision_tier__c,         
                                                         Vision_Premium__c,
                                                         DB_Invoice__r.db_leave__r.fsa_election__c,         
                                                         fsa_premium__c,
                                                         DB_Invoice__r.db_leave__r.Personal_Accident_Election__c,         
                                                         Personal_Accident_Amount_Per_Pay__c,
                                                         DB_Invoice__r.db_leave__r.Critical_Illness_Election__c,         
                                                         Critical_Illness_Amount_Per_Pay__c,
                                                         DB_Invoice__r.db_leave__r.Long_Term_Disability_Election__c,         
                                                         Long_Term_Disability_Premium__c,
                                                         DB_Invoice__r.db_leave__r.Life_and_AD_D_Election__c,         
                                                         Life_and_AD_D_Premium__c,
                                                         DB_Invoice__r.db_leave__r.Spouse_Life_Election__c,         
                                                         Spouse_Life_Premium__c,
                                                         DB_Invoice__r.db_leave__r.Dependent_Life_Election__c,         
                                                         Dependent_Life_Premium__c,
                                                         year__c,
                                                         Pay_Date__c from Direct_Billing_Invoice_Pay_Date__c where DB_Invoice__r.Invoice_Month__c = :invoiceMonth order by Pay_Date__c asc]){
                                           //and DB_Invoice__r.Physical_Invoice_Created__c=null              
            leaves.add(invoicePayDate.DB_Invoice__r.db_leave__c);
            invSet.add(invoicePayDate.db_invoice__c);
            
            Direct_Billing_Leave__c dbl = new Direct_Billing_Leave__c(id=invoicePayDate.DB_Invoice__r.db_leave__c);
            
            Direct_Billing_Invoice__c invoice = new Direct_Billing_Invoice__c(db_leave__c=dbl.id, id=invoicePayDate.DB_Invoice__c);
            string y = string.valueof(invoicePayDate.pay_date__c.year());
            set<string> ySet = yearsMap.get(dbl.id);
            if(ySet==null){ySet = new set<string>();}
            ySet.add(y);
            yearsMap.put(dbl.id, ySet);
            //years.add(y);
            Direct_Billing_Invoice_Pay_Date__c[] ipdList = invoicePayDateMap.get(invoicePayDate.DB_Invoice__c+''+y);
            
            string payDates = invoicePayDatesIncluded.get(invoicePayDate.DB_Invoice__c+''+y);
            
            if(payDates ==null){payDates ='';}
            
            payDates += invoicePayDate.pay_date__c.month()+'/'+invoicePayDate.pay_date__c.day()+'/'+invoicePayDate.pay_date__c.year()+', ';
            
            invoicePayDatesIncluded.put(invoicePayDate.DB_Invoice__c+''+y, payDates);
            
            if(ipdList==null){
                ipdList = new Direct_Billing_Invoice_Pay_Date__c[]{};
                ipdList.add(invoicePayDate);
            
                invoicePayDateMap.put(invoicePayDate.DB_Invoice__c+''+y, ipdList);
            
            }
                                                       
        }
        
        for(string s : invoicePayDatesIncluded.keyset()){
                string payDates = invoicePayDatesIncluded.get(s);
                payDates = payDates.removeEnd(', ');
                invoicePayDatesIncluded.put(s, payDates);
        }
        
        for(Direct_Billing_Invoice__c invoice : [select id,db_leave__c,
                                                        due_date__c,
                                                        New_Charges__c,
                                                        Invoice_Date__c,
                                                        invoice_amount__c,
                                                        invoice_month__c,
                                                        invoice_charges__c,
                                                        Previous_Invoice_Date__c,
                                                        Pay_Dates_included__c,
                                                        Physical_Invoice_Created__c,
                                                        welcomeLetterNeeded__c,
                                                        pastDueLetterNeeded__c,
                                                        Invoice_Outstanding_Balance__c,
                                                        db_leave__r.Current_Balance__c,
                                                        db_leave__r.Medical_Parma_Premium__c,
                                                        db_leave__r.DB_Member__r.Employee_Name__c,
                                                        db_leave__r.DB_Member__r.Address_1__c,
                                                        db_leave__r.DB_Member__r.Address_2__c,
                                                        db_leave__r.DB_Member__r.City__c,
                                                        db_leave__r.DB_Member__r.State__c,
                                                        db_leave__r.DB_Member__r.Zip_Code__c,
                                                        db_leave__r.Identity_Theft_Election__c,
                                                        db_leave__r.Indemnity_Plan_Election__c,
                                                        db_leave__r.Legal_Plan_Election__c,
                                                        db_leave__r.Medical_Parma_Election__c,
                                                        db_leave__r.Medical_Election__c,
                                                        db_leave__r.Dental_Election__c,
                                                        db_leave__r.Vision_Election__c,
                                                        db_leave__r.Vision_Premium__c,
                                                        db_leave__r.FSA_Election__c,
                                                        db_leave__r.Personal_Accident_Election__c,
                                                        db_leave__r.Critical_Illness_Election__c,
                                                        db_leave__r.Long_Term_Disability_Election__c,
                                                        db_leave__r.Life_and_AD_D_Election__c,
                                                        db_leave__r.Spouse_Life_Election__c,
                                                        db_leave__r.Dependent_Life_Election__c,
                                                        db_leave__r.Outstanding_Balance__c,
                                                        name from Direct_Billing_Invoice__c where id in :invSet]){
            invoicesMap.put(invoice.db_leave__c, invoice);
            
        }
        
        for(string s : YearsMap.keyset() ){
            yearsMapCount.put(s, YearsMap.get(s).size());
        }
        
   }
    
    public Integer getSize(){
     Integer mysize = years.size();
     return mysize;
    } 
    
}