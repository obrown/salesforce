public class AmsSearch extends searchEncrypted{
    
    public magpie_Error__c[] AmsList {get; set;}
    string searchBy;
    
    public AmsSearch(){
       searchBy = ApexPages.CurrentPage().getParameters().get('st');
       AmsSearch();
    }
    
    public AmsSearch(string searchTerm){
        searchBy = searchTerm;
        AmsSearch();
    }
    
    void AmsSearch(){
        
        if(searchBy==null){
            return;
        }
        
        setSearchTerm(searchBy);
    
        string[] searchFields = new string[]{};
        searchFields.add('Transaction_Date__c');
        searchFields.add('Employee_SSN__c');
        
        searchFields.add('Member_Name__c');
        searchFields.add('Rx_Claim_Number__c');
        searchFields.add('Client__r.Name');
        searchFields.add('Source_Type__c');
        searchFields.add('Transaction_Type__c');
        searchFields.add('Dispensed_Date__c');
        searchFields.add('Open_Work_Tasks__c');
        searchFields.add('createddate');
        searchFields.add('lastmodifieddate');
        searchFields.add('name');
        
        string searchString = '';
        
        for(String s : searchFields){
            searchString = searchString+','+s;
        }
        
        searchString = 'Select ' + searchString.right(searchString.length()-1) + ' from magpie_Error__c';
        AmsList = search(searchString, searchFields);
        
    }
    

}