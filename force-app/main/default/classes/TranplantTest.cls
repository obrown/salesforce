/**
* This class contains unit tests for validating the behavior of Apex classes
* and triggers.
**/

@isTest(seeAlldata=true)
private class TranplantTest {
    
    private static testMethod void newTransplant() {
        
        createTransplant ct = new createTransplant();
        Transplant__c tr = new Transplant__c();
        test.startTest();
        
        ApexPages.StandardController sc = new Apexpages.Standardcontroller(tr);
        TransController tc = new Transcontroller(new ApexPages.StandardController(tr));
        
        //Test Saving with no information, save should fail
        tc.inlineSave();
        system.assert(tc.getisError());
        
        tc.obj = ct.newTransplant(false);
        
        //Save with information, save should succeed
        tc.checkdup();
        tc.inlineSave();
        system.assert(tc.obj.id!=null);
       
        Transplant_Notes__c tnote = new Transplant_Notes__c();
        tnote.subject__c='test';
        tnote.transplant__c = tc.obj.id;
        insert tnote;
        
        test.stopTest();
    }

    private static testMethod void existingTransplant() {
        
        test.startTest();
        
        createTransplant ct = new createTransplant();
        Transplant__c tr = ct.newTransplant(true);
        system.assert(tr.id!=null);

        caregiver__c cg = ct.addCG(tr.id);
        
        ApexPages.StandardController sc = new Apexpages.Standardcontroller(tr);
        TransController tc = new Transcontroller(new ApexPages.StandardController(tr));
        
        /* Test the querying of tasks and deleting of Tasks*/
        
        Task newT = new Task(whatid = tr.id,
                             subject='Test'
                             );
        
        insert newT; //Will test the deleteOpenT function and the loadAH function            
        
        Task existingT = newT.clone();
        insert existingT;
        
        existingT.Status = 'Completed';
        update existingT;
        
        tc.LoadOpenTData();
        tc.LoadAHData();
        
        tc.theAHid = newT.id;
        tc.DeleteOpenT();
        
        tc.ntid = newT.id;
        tc.DeleteAH();
        
        /* End */
        
        /* Add Provider */
        
        ct.addPro(tc);
        tc.AddProvider();
        system.assert(tc.Providers.size()>0);
        
        tc.theProid = tc.Providers[0].id;
        tc.editPro();
        tc.obj.Provider_Phone__c='(480) 555-1215';
        tc.updatePro();
                
        /* End */
        
        /* Convert */
        
        tc.convertLead();
        system.debug(tc.message);
        
        
        /* End */
        
        /*Create Pre Stage */
        ct.createStages('Pre', tc, date.valueof('2014-01-01'), date.valueof('2014-02-01'));
        
        
        /* End */
        
        /* Primary Case Manager*/
        
        tc.obj.TransitionStage__c = 'Pre';
        tc.obj.pcmModal__c=null;
        tc.addPCM();
        
        tc.obj.TransitionStage__c = 'Pre';
        tc.obj.pcmModal__c = 'HDP';
        tc.addPCM();
        
        system.assert(tc.pcmList.size()>0);
        
        tc.delpcmid=tc.pcmList[0].id;
        tc.editthePCM();
        tc.updatePCM();
        tc.cancelEditPCM();
        tc.delpcmid=tc.pcmList[0].id;
        tc.deletepcm();
        
        /* End */
        
        
        /* misc */
        
        tc.getEPHN();
        tc.gettlTypes(); 
        tc.getstageTypes(); 
        tc.addshowERecord();
        tc.getshowERecord();
        tc.addRowsNotes();
        tc.getshowmorenotes();
        tc.getshownotesRecord();
        tc.addRowsAH();
        tc.getshowmoreah();
        tc.getshowAHrecords();
        tc.getIsErrorMessage();
        tc.setMessage(null);
        tc.getshowcancel();
        tc.getRecordtypeName();
        tc.getlanguageSpoken();
        tc.getLanguage();
        tc.getlangaugeText();
        tc.getisdeleteprovider();
        tc.getisEditProvider();
        tc.getisDelete();
        tc.getiseditablelead();
        tc.getistestingfields();
        tc.getDeleteTask();
        tc.mycancel();
        tc.getSCtext();
        tc.updatePatientInfo();
        /* end */
      
        test.stopTest();
        
    }
    
    private static testMethod void testAppt() 
    {
        
        createTransplant ct = new createTransplant();
        Transplant__c tr = ct.newTransplant(true);
        system.assert(tr.id!=null);

        caregiver__c cg = ct.addCG(tr.id);
        
        ApexPages.StandardController sc = new Apexpages.Standardcontroller(tr);
        TransController tc = new Transcontroller(new ApexPages.StandardController(tr));
        
        ct.addPro(tc);
        tc.AddProvider();
        
        /* Convert */
        
        tc.convertLead();
        
        /* End */
        
        test.startTest();   
        
        /*Create Stages for Appt */
        
        ct.createStages('Pre', tc, date.valueof('2014-01-01'), date.valueof('2014-02-01'));
        ct.createStages('Global', tc, date.valueof('2014-02-02'), date.valueof('2014-03-01'));
        ct.createStages('Post', tc, date.valueof('2014-03-02'), date.valueof('2014-04-01'));
        
        /* End */   
        
        /* Add Pre-Appt, edit, update, cancel and delete */
        tc.settheapptID();
        ct.addAppt(tc, 'Pre');
        
        tc.ApptIDinpatient = [select id from Appointment__c where Transplant__c = :tc.obj.id and Stage__c ='Pre'].id;
        id apptID = tc.ApptIDinpatient; 
        tc.editAppointment();
        tc.updateAppt();
        tc.cancelApptEdit();
        tc.ApptIDinpatient = [select id from Appointment__c where Transplant__c = :tc.obj.id and Stage__c ='Pre'].id;
        
        /* End */

        /* Add Global-Appt, edit, update, cancel and delete */
        ct.addAppt(tc, 'Global');
        tc.ApptIDinpatient = [select id from Appointment__c where Transplant__c = :tc.obj.id and Stage__c ='Global'].id;
        tc.editAppointment();
        tc.updateAppt();
        tc.cancelApptEdit();
        tc.ApptIDinpatient = [select id from Appointment__c where Transplant__c = :tc.obj.id and Stage__c ='Global'].id;
        tc.delAppt();
        
        /* End */
        
        /* Add Post-Appt, edit, update, cancel and delete */
        ct.addAppt(tc, 'Post');
        tc.ApptIDinpatient = [select id from Appointment__c where Transplant__c = :tc.obj.id and Stage__c ='Post'].id;
        tc.editAppointment();
        tc.updateAppt();
        tc.cancelApptEdit();
        tc.ApptIDinpatient = [select id from Appointment__c where Transplant__c = :tc.obj.id and Stage__c ='Post'].id;
        tc.delAppt();
        
        /* End */
        
        /* Test in-patient */
        
        tc.ApptIDinpatient = apptID;
        tc.obj.inpatient_Comments__c = 'Test';
        tc.obj.inpatient_Hospital_Discharge__c = date.today();
        tc.obj.inpatient_reason__c = 'Test';
        tc.addInpatient();
        tc.ipID = [select id from Inpatient__c where Appointment__c = :apptID].id;
        tc.editInpatient();
        tc.deleteinpatient();
        tc.closeinpatient();
        tc.ApptIDinpatient = apptID;
        tc.delAppt();
        /* End */
        
        tc.theProid = tc.Providers[0].id;
        tc.DeletePro();
        tc.closePro();
        
        test.stopTest();
    }   

    private static testMethod void testLocation() 
    {
        
        createTransplant ct = new createTransplant();
        Transplant__c tr = ct.newTransplant(true);
        system.assert(tr.id!=null);
        
        ApexPages.StandardController sc = new Apexpages.Standardcontroller(tr);
        TransController tc = new Transcontroller(new ApexPages.StandardController(tr));
        
        ct.addPro(tc);
        tc.AddProvider();
        
        /* Test Caregiver */
        
        tc.obj.cgRelationship_to_the_Patient__c ='Brother';
        tc.obj.cgStreet__c                 ='123 E Main St';
        tc.obj.cgcity__c                   = 'Gilbert';
        tc.obj.cgState__c                  = 'AZ';
        tc.obj.cgisactive__c               = true; 
        tc.obj.cgZip_Postal_Code__c        = '85265';
        tc.obj.Caregiver_First_Name__c     = 'John'; 
        tc.obj.Caregiver_Last_Name__c      = 'Doe';
        tc.obj.CaregiverHomePhoneDisp__c   = '(480) 555-1212'; 
        tc.obj.Caregiver_DOB__c            = date.today().addyears(-35);
        tc.obj.CaregiverMobilePhoneDisp__c = '(480) 555-1212';
        tc.addCaregiver();
        
        system.assert(tc.caregivers.size()>0);
        
        /* End */
        
        /* Convert */
        
        tc.convertLead();
        tc.cgid = tc.caregivers[0].id;
        tc.cgChanged();
        tc.deletecg();
        /* End */
        
        
        /*Create Stages for Location */
        
        ct.createStages('Pre', tc, date.valueof('2014-01-01'), date.valueof('2014-02-01'));
        ct.createStages('Global', tc, date.valueof('2014-02-02'), date.valueof('2014-03-01'));
        ct.createStages('Post', tc, date.valueof('2014-03-02'), date.valueof('2014-04-01'));
        tc.loadStageRecords('4');
        
        
        /* End */   
        
        /* Add Location */
        
        tc.obj.LocationChangeDateModal__c = date.today();
        tc.obj.Patient_Lives_Local_to_Mayo__c = 'No';
        tc.obj.NewLocationModal__c = 'Mayo';
        tc.addlocation();
        
        system.assert(tc.relocation.size()>0);
        
        /* End */
        
        /* Edit & Update Location */
        
        tc.locID = 'loc0';
        tc.editlocation();
        tc.updatelocation();
        tc.deleteLocation();
        tc.cancelLocationEdit();
        
        /* End */
        
        test.startTest();
        
        /* Test the stages */
        
        tc.delprestageid = 'preid';
        tc.edittheStage();
        
        tc.obj.Stage_Effective_Date__c = tc.obj.Stage_Effective_Date__c.addDays(-5);
        tc.updateStage();
        
        tc.delprestageid = 'globalid';
        tc.edittheStage();
        tc.updateStage();
        
        tc.delprestageid = 'postid';
        tc.edittheStage();
        tc.updateStage();
        
        tc.delprestageid = 'postStage'; 
        tc.deleteprestage();
        
        tc.delprestageid = 'globalStage'; 
        tc.deleteprestage();
        
        tc.delprestageid = 'preStage'; 
        tc.deleteprestage();
        
        tc.cancelStageEdit();
        /* End */
        
        test.stopTest();
    }
    
    
    //See TransplantTest2   
        
    
}