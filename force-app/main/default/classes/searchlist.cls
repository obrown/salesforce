public with sharing class searchlist {

    public string sl {get;set;}
    
    public searchlist(){
    SystemID__c sid = SystemID__c.getInstance();  
    
        if(utilities.isInternal()){
            sl ='Search All|Patient Cases';
            id p = userInfo.getProfileID();
            if(p != sid.csrProfID__c && p != sid.csrSuperProfid__c && p != sid.CareManagementCOE__c && p != sid.IntakeSpecialist__c){
                sl += '|Transplants';
            }
            sl += '|Coverage|Members|Intakes';
            
            if(Schema.sObjectType.Bariatric__c.fields.id.isAccessible()) {
                sl += '|Bariatric';
            }
        }else{
            sl ='Patient Cases';
        }
    
    }
    
}