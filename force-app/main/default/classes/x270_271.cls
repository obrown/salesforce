public class x270_271{
    
    public string lname,fname,dob,ins_id;
    
    public string vendorName;
    public string vendorTaxId;
    
    public string userName; 
    public string password; 
    
    public string payorId;
    
    
    string shortDate = string.valueof(date.today().year()).right(2)+''+padDate(date.today().month())+''+padDate(date.today().day()); 
    string longDate = date.today().year()+''+padDate(date.today().month())+''+padDate(date.today().day());  
    string tx_id = string.valueof(math.abs(crypto.getRandomLong())).left(5);
        
    public string build270(){
        
        string[] fdob = dob.split('-');
        dob = fdob[0]+''+padDate(integer.valueof(fdob[1]))+''+padDate(integer.valueof(fdob[2]));
        string x270=buildISA();
        x270 += buildGS();
        x270 += buildST();
        x270 += buildBHT();
        x270 += buildNM1_ProviderInfo();
        x270 += buildNM1_Member();
        x270 += 'EQ*30~';
        x270 += buildSE();
        x270 += 'GE*1*1~';
        x270 += 'IEA*1*000005477~';
        
        return x270;
    }
     
    
    string buildISA(){
        return 'ISA*03*'+ this.userName +'*00*'+ this.password +'*ZZ*341593929      *ZZ*'+ this.vendorTaxId +'      *'+shortDate+'*'+ padDate(datetime.now().hour()) +''+ padDate(datetime.now().minute()) +'*^*00501*000005477*0*P*:~';
    }
    
    string buildGS(){
        return 'GS*HS*341593929*MTEXE*'+ longDate +'*'+ padDate(datetime.now().hour()) +''+ padDate(datetime.now().minute()) +'*1*X*005010X279A1~';
        
    }
    
    string buildST(){
        return 'ST*270*'+tx_id+'*005010X279A1~';
    }
    
    string buildBHT(){
        return 'BHT*0022*13*000000000*'+ longDate +'*1419~';
    }
    
    string buildNM1_ProviderInfo(){
        string pi ='';
        pi += 'HL*1**20*1~';
        pi += 'NM1*PR*2*'+this.vendorName+'*****PI*'+ this.payorId +'~';
        pi += 'HL*2*1*21*1~';
        pi += 'NM1*1P*2*Health Design Plus*****FI*341593929~';
        return pi;
    }
    
    string buildNM1_Member(){
        string mi ='HL*3*2*22*0~';
        mi += 'NM1*IL*1*'+this.lname+'*'+this.fname.left(1)+'****MI*'+this.ins_id+'~';
        mi += 'DMG*D8*'+ this.dob + '~';
        return mi;
        
    }
    
    string buildSE(){
        return 'SE*13*'+tx_id+'~';
        
    }
    
    map<string, string> segmentMap = new map<string, string>();
    
    class result{
        string eligible, notes, deductibleInformation, carrier;
        date beginDate;
        
    }
    
    result x271(string x271){
        result result = new result();
        
        string[] lines = x271.split('~');
        string[] ebSegments = new string[]{};
        
        for(string l : lines){
            string[] segments = l.split('\\*');
            string key = segments[0]+''+segments[1];
            string value='';
            system.debug(l);
            if(segments[0]=='EB'){
                ebSegments.add(l.right(l.length()-3));
            }
            
            for(integer i=2; i<segments.size();i++){
                value += '*'+segments[i];
            }
                        
            segmentMap.put(key, value);
        }
        
        string deductibleInformation=translateEB(ebSegments);
        deductibleInformation=deductibleInformation.replaceAll('\\*','');
        
        if(segmentMap.get('EB1')!=null ||
           segmentMap.get('EB2')!=null ||
           segmentMap.get('EB3')!=null ||
           segmentMap.get('EB4')!=null ||
           segmentMap.get('EB5')!=null ||
           segmentMap.get('DTP291')!=null){
            
            date beginDate, endDate, cobraDate;
            date eligFloor = date.today();
            
            string strBeginDate, strEndDate, strCobraDate;
            if(segmentMap.get('DTP346')!=null){
                strBeginDate = segmentMap.get('DTP346').split('\\*')[2];
            }else if(segmentMap.get('DTP291')!=null){
                strBeginDate = segmentMap.get('DTP291').split('\\*')[2];    
            }
            
            if(segmentMap.get('NM1PR')!=null){
                result.carrier = segmentMap.get('NM1PR').split('\\*')[2];
            }
            
            if(segmentMap.get('DTP347')!=null){
                strEndDate= segmentMap.get('DTP347').split('\\*')[2];
                endDate= date.newInstance(integer.valueof(strEndDate.mid(0,4)),integer.valueof(strEndDate.mid(4,2)),integer.valueof(strEndDate.right(2)));
            }
            
            if(segmentMap.get('DTP341')!=null){
                strCobraDate = segmentMap.get('DTP341').split('\\*')[2];
                cobraDate = date.newInstance(integer.valueof(strCobraDate.mid(0,4)),integer.valueof(strCobraDate.mid(4,2)),integer.valueof(strCobraDate.right(2)));
            }
            
            beginDate = date.newInstance(integer.valueof(strBeginDate.mid(0,4)),integer.valueof(strBeginDate.mid(4,2)),integer.valueof(strBeginDate.right(2)));
            result.beginDate = beginDate;
            
            if(eligFloor<beginDate){
                result.eligible='No';
                result.notes='Member is not eligible. Eligibility start date is '+beginDate.month()+'/'+beginDate.day()+'/'+beginDate.year()+'\n\n';
                
                
            }else{
                
                if(endDate!=null){
                    if(endDate<=date.today()){
                        result.eligible='No';
                        result.notes='Member is not eligible. Paid thru date is '+endDate.month()+'/'+endDate.day()+'/'+endDate.year();
                        result.deductibleInformation = deductibleInformation;
                        
                        
                    }else{
                        result.eligible='Yes';
                        result.notes='Member is eligible. Eligibility start date is '+beginDate.month()+'/'+beginDate.day()+'/'+beginDate.year();
                        result.deductibleInformation = deductibleInformation;
                        
                     }
                }else if(cobraDate!=null){
                    
                    if(cobraDate<=date.today()){
                        result.eligible='No';
                        result.notes='Member is not eligible. Cobra paid thru date is '+cobraDate.month()+'/'+cobraDate.day()+'/'+cobraDate.year();
                    
                    }else{
                        result.eligible='No';
                        result.notes='Member is eligible. Eligibility start date is '+beginDate.month()+'/'+beginDate.day()+'/'+beginDate.year();
                        result.deductibleInformation = deductibleInformation;
                    }
                    
                }else{
                
                    result.eligible='Yes';
                    result.notes='Member is eligible. Eligibility start date is '+beginDate.month()+'/'+beginDate.day()+'/'+beginDate.year();
                    result.deductibleInformation = deductibleInformation;
                }
            }
            
        }else{
            result.eligible ='No';
            result.notes='Member not Found';
            
        }
        
        return result;
    }
    
    /* Patient Case */
    
    public string read271(string x271, patient_case__c pc){
        
        result result = x271(x271);
        
        Eligibilty__c eligibility = new Eligibilty__c(patient_case__c=pc.id,Date_Care_Mgmt_Transfered_to_Eligibility__c=date.today(),Date_Eligibility_Checked_with_Client__c=date.today());
        
        pc.Date_Care_Mgmt_Transfers_to_Eligibility__c=date.today();
        pc.Date_Eligibility_is_Checked_with_Client__c=date.today();
        eligibility.Effective_Date_of_Medical_Coverage__c=result.beginDate;
        pc.Effective_Date_of_Medical_Coverage__c=result.beginDate;
        
        pc.eligible__c = result.eligible;
        pc.enotes__c = result.notes;
        
        eligibility.eligible__c = result.eligible;
        eligibility.notes__c = result.notes;
        eligibility.deductible_information__c= result.deductibleInformation;
        
        upsert eligibility;
        update pc;
        return null;
        
    }
    
    /* Oncology */
    
    public string read271(string x271, oncology__c o){
        result result = x271(x271);
        
        Oncology_Eligibility__c oe = new Oncology_Eligibility__c(oncology__c=o.id);
        oe.Eligible__c =result.eligible;
        oe.Note__c =result.notes; 
        oe.Deductible_Information__c= result.deductibleInformation;
        oe.carrier__c =  result.carrier;
        
        upsert oe;
        return null;
        
    }
    
    string padDate(integer val){
        if(val<10){
            return '0'+val;
        }
        
        return ''+val;
    }
    
    string translateEB(string[] ebList){
        
        map<string, string> deductible_amount = new map<string, string>();
        map<string, string> deductible_remaining = new map<string, string>();
        
        map<string, string> oop_amount = new map<string, string>();
        map<string, string> oop_remaining = new map<string, string>();
        
        ebSegment ebs = new ebSegment();
        map<string, map<string, string>> ebMap = ebs.ebMap;
       
        string deductibleInformation='';
        string[] ebSegments ;
        set<string> dSet = new set<string>();
        
        for(string eb : ebList){
            
            ebSegments= eb.split('\\*');
            if(ebSegments[0]=='C' || ebSegments[0]=='G'){
            
                deductibleInformation = ebMap.get('2').get(ebSegments[1])+', '; //Family or individual
                deductibleInformation += ebMap.get('3').get(ebSegments[2])+', '; //Medical Dental etc (Health benefit plan)
                deductibleInformation += ebMap.get('5').get(ebSegments[3])+'\n'; //PPO , HRA, HSA
                deductibleInformation += ebMap.get('12').get(ebSegments[11])+''; // In Network, Out of Network
               
                dSet.add(deductibleInformation);                
                if(ebMap.get('1').get(ebSegments[0])=='Deductible'){
                    
                    if(ebMap.get('6').get(ebSegments[5])=='Remaining'){
                        
                        deductible_remaining.put(deductibleInformation, '$'+ebSegments[6]);
                    }else{
                        deductible_amount.put(deductibleInformation, '$'+ebSegments[6]);
                    }
                
                }else{
                    if(ebMap.get('6').get(ebSegments[5])=='Remaining'){
                        oop_remaining.put(deductibleInformation, '$'+ebSegments[6]);
                    }else{
                        oop_amount.put(deductibleInformation, '$'+ebSegments[6]);
                    }
                }
            
            }
          
        }
        
        deductibleInformation='';
        
        for(string eb : dSet){
            deductibleInformation += eb+'\n';
            
            if(deductible_amount.get(eb)==null){
                deductibleInformation += 'Deductible: $0, ';
            }else{
                deductibleInformation += 'Deductible: '+deductible_amount.get(eb)+', ';
            }
            
            if(deductible_remaining.get(eb)==null){
                deductibleInformation += 'Remaining, $0';
            }else{
                deductibleInformation += 'Remaining, '+deductible_remaining.get(eb);
            }
            
            if(oop_amount.get(eb)==null){
                deductibleInformation += '\nOut of Pocket (Stop Loss): $0, ';
            }else{
                deductibleInformation += '\nOut of Pocket (Stop Loss): '+oop_amount.get(eb)+', ';
            }
            
            if(oop_remaining.get(eb)==null){
                deductibleInformation += 'Remaining, $0';
            }else{
                deductibleInformation += 'Remaining, '+oop_remaining.get(eb);
            }
            deductibleInformation += '\n\n';
        }
        
        deductibleInformation = deductibleInformation.replaceAll(', null', '');
        deductibleInformation = deductibleInformation.replaceAll('null', '');
        
        return deductibleInformation;
    }
    
}