public class tenDayEligibilityCheck{    

    messaging.Singleemailmessage[] finalMail = new messaging.Singleemailmessage[]{};
    Patient_Case__c[] theOpplist = new list<Patient_Case__c>();
    
    Eligibilty__c[]                eligList = new Eligibilty__c[]{};
    string[]                       addEligibility = new string[]{};
    string[]                       sendemailto = new string[]{'hdpeligibility@hdplus.com'};
    
    id queueGroupId;
    
    public void run(){
    
        queueGroupId= [Select Id from Group where type='Queue' and Name= 'HDP Eligibility'].Id;
       
        try{
            set<id> w_idSet = new set<id>();
            set<id> l_idSet = new set<id>();
            
            for(Patient_Case__c p : [select Client_Name__c, 
                                            Date_Eligibility_is_Checked_with_Client__c, 
                                            Date_Care_Mgmt_Transfers_to_Eligibility__c,
                                            Employee_SSN__c,    
                                            Employee_HealthPlan__r.name,
                                            Employee_Primary_Health_Plan_Name__c,
                                            Relationship_to_the_Insured__c,
                                            Estimated_Arrival__c,
                                            Actual_Arrival__c, 
                                            Eligible__c, 
                                            pendingEligCheck__c,
                                           name from Patient_Case__c where isConverted__c = true and status__c != 'Closed' and status__c != 'Completed' and pendingEligCheck__c = false and ((Estimated_Arrival__c > :date.today().addDays(1) or Actual_Arrival__c > :date.today().addDays(1)) or (estimated_arrival__c = null or Actual_Arrival__c = null)) and actual_departure__c = null]){

                if(p.Date_Eligibility_is_Checked_with_Client__c == null && p.Date_Care_Mgmt_Transfers_to_Eligibility__c != null){ //check in process
                    continue;
                }
                
                if(p.Client_Name__c=='Lowes'){
                    l_idSet.add(p.id);
                      
                }else if(p.Client_Name__c=='Walmart'){   
                    w_idSet.add(p.id);
                    
                }else{
                    if(p.Estimated_Arrival__c != null){
                        addToElig(p, p.Date_Eligibility_is_Checked_with_Client__c, p.Estimated_Arrival__c);
    
                    }else if(p.Actual_Arrival__c != null){
                        addToElig(p, p.Date_Eligibility_is_Checked_with_Client__c, p.Actual_Arrival__c);
                    }
                }
                
                
                if(w_idSet.size()>=50){
                  futureElig.coeEligibility(w_idSet);
                  w_idSet.clear();
                }
                             
                if(l_idSet.size()>=50){
                  futureElig.coeEligibility(l_idSet);
                  l_idSet.clear();
                }
                
            }
            
            futureElig.coeEligibility(w_idSet);
            futureElig.coeEligibility(l_idSet);
            
            if(!eligList.isempty()){
                insert eligList;
                update theOpplist;
            }
        
                
        }catch (exception e){
            batchProcess.batchException(e.getMessage(),e.getLineNumber());                 
        }  
            
    }
    
    void addToElig(patient_case__c p, date checkedWithClient, date arrival){
        integer daysToTravel = date.today().daysBetween(arrival);
        if(checkedWithClient !=null){
            if(daysToTravel <=10 && checkedWithClient.daysBetween(arrival) > 10){
            
                Eligibilty__c objE = new Eligibilty__c(patient_case__c=p.id,Date_Care_Mgmt_Transfered_to_Eligibility__c =date.today(),Plan__c=p.Employee_Primary_Health_Plan_Name__c,ownerID=queueGroupId);
                eligList.add(objE);
                p.pendingEligCheck__c=true;
                p.Date_Care_Mgmt_Transfers_to_Eligibility__c = date.today();
                p.Eligible__c = null;
                p.Date_Eligibility_is_Checked_with_Client__c = null;
                p.EligibiltyEmailSent__c = false;
            
                theOpplist.add(p);
            
            }
                                        
        }else{
            
            Eligibilty__c objE = new Eligibilty__c(patient_case__c=p.id,Date_Care_Mgmt_Transfered_to_Eligibility__c =date.today(),Plan__c=p.Employee_Primary_Health_Plan_Name__c,ownerID=queueGroupId);
            eligList.add(objE);
            p.pendingEligCheck__c=true;
            p.Date_Care_Mgmt_Transfers_to_Eligibility__c = date.today();
            p.Eligible__c = null;
            p.Date_Eligibility_is_Checked_with_Client__c = null;
            p.EligibiltyEmailSent__c = false;
            
            theOpplist.add(p);
        }
            
    }

}