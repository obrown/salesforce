public with sharing class ovrController{

    public string sessionID {get; private set;}
    
    public ovr_control__c ovr {get; set;}
    
    public ovr_request__c   req {get; set;}
    public ovr_request__c[] reqlist {get; set;}
    
    public integer reqFirst {get; private set;}
    
    public ovr_claim__c   clm {get; set;}
    public ovr_claim__c[] clmList {get; set;}
    
    public ovr_refund__c   refund {get; set;}
    public ovr_refund__c[] refundList {get; set;}
    
    public ovr_check__c chk {get; set;}
    public ovr_check__c[] chkList {get; set;}
    
    public ovr_claim__c   sbg     {get; set;}
    public ovr_claim__c[] sbgList {get; set;}
    
    public ftpAttachment__c attach {get; set;}
    public ftpAttachment__c[] attachments {get; set;}
    
    public boolean error {get ; private set;}
    public map<id, ovr_refund__c[]> refundMap {get; private set;}
    
    public List<SelectOption> client {get; set;}
    
    public decimal totalRefundPaid {get; private set;}
    public decimal totalOverpaymentAmount {get; private set;}
    public decimal totalBalanceDue {get; private set;}
    public decimal totalAmountWaived {get; private set;}

    public transient String fileName {get;set;}
    public transient String fileDescription {get;set;}
    public transient Blob fileBody {get;set;}
    public transient string fileContent {get; set;}
    
    public string fileDirectory {get; private set;} 
    
    id theID;
    id cloneClmID;
    id openID;
     
    public ovrController(){
        theID = ApexPages.currentPage().getparameters().get('id');
        if(theID != null && string.valueof(id.valueof(theID).getSObjectType()) != 'OVR_Control__c'){
            theID = [select OVR_Control__c from Ovr_Claim__c where id = :theID].OVR_Control__c;
        }
        
        init();
        reqPagination();
        
    }
    
    void init(){
        
        if(theID==null){
            ovr = new ovr_control__c(ownerID = userinfo.getuserid(), Status__c='Open');
            loadClientMap();
        }else{
        
            ovr = [select Comments__c,
                          createdby.name,
                          createdDate,
                          lastmodifiedby.name,
                          lastmodifiedDate,
                          name,
                          type__c,
                          Number_of_Claims__c,
                          Client__c,
                          payee__c,
                          Payor__c,
                          Status__c,
                          Owner.Name,OwnerID from ovr_control__c where id = :theID];
            fileDirectory = ovr.name;                
        }
        
        loadReq();
        loadClaims();
        loadRefunds();
        loadChecks();
        loadSbgClaims();
        loadAttachments();
        
        if(ovr.client__c==null){
            loadClientMap();    
        }
        
        
    }
    
    public void doNothing(){
        cloneClmID = ApexPages.currentPage().getparameters().get('CloneClmID');
    }
    
    public void newClaim(){
        
        clm = new ovr_claim__c(ovr_control__c = theID, Amount_Waived__c=0.00, recordtypeid=Schema.SObjectType.Ovr_Claim__c.getRecordTypeInfosByName().get('Claim').getRecordTypeId());
        
        if(cloneClmID != null){
            for(ovr_claim__c c :clmList){
                
                if(c.id == cloneClmID){
                    clm.essn__c= c.essn__c;
                    clm.patient_first_name__c = c.patient_first_name__c ;
                    clm.patient_last_name__c = c.patient_last_name__c;
                    break;
                }
            
            }
        }
        cloneClmID=null;
        refundList = new ovr_refund__c[]{};
    }
  
    /* Claims */
    
    void loadClaims(){
        clmList = new ovr_claim__c[]{};
        refundMap  = new map<id, ovr_refund__c[]>{};
        
        
        totalRefundPaid = 0.00;
        totalOverpaymentAmount = 0.00;
        totalBalanceDue = 0.00;
        totalAmountWaived = 0.00;
        
        for(ovr_claim__c c : [select Amt_Reimbursed__c ,
                                     Amount_Waived__c,
                                     Amount_Waived_Note__c,
                                     Auditor__c,
                                     Balance_Due__c, 
                                     Comments__c,
                                     clone__c,
                                     Date_of_Accident__c,
                                     Date_Paid__c,
                                     Error__c,essn__c,
                                     HDP_Contingency__c,
                                     Member_First_Name__c,
                                     Member_Last_Name__c, 
                                     Name, 
                                     Number_of_Refunds__c,
                                     Overpayment_Amount__c,
                                     OVR_Control__c,
                                     Patient_First_Name__c,
                                     Patient_Last_Name__c,
                                     Processor__c,
                                     Reason__c,
                                     
                                     Total_Refund__c from ovr_claim__c where ovr_control__c = :theID and recordtype.name = 'Claim' order by createdDate Desc]){
                          
            clmList.add(c);
            refundMap.put(c.id, null);  
            
            if(c.Total_Refund__c!=null){
                totalRefundPaid += c.Total_Refund__c;
            }
            
            if(c.Overpayment_Amount__c!=null){
                totalOverpaymentAmount += c.Overpayment_Amount__c;
            }
            
            if(c.Balance_Due__c!=null){
                totalBalanceDue += c.Balance_Due__c;
                
            }
            
            if(c.Amount_Waived__c!=null){
                totalAmountWaived += c.Amount_Waived__c;
                
            }
            
        }
        
        ovr_refund__c[] fooRefund;
        
        for(ovr_refund__c r : [select OVR_Claim__c,Refund_Amount__c, Refund_Date__c, createdDate, createdby.name,Check_Number__c from ovr_refund__c where OVR_Claim__c in :refundMap.keyset() order by Refund_Date__c desc]){
            
            fooRefund = refundMap.get(r.OVR_Claim__c);
            if(fooRefund == null){
                fooRefund = new ovr_refund__c[]{};
            }
            
            fooRefund.add(r);
            refundMap.put(r.OVR_Claim__c, fooRefund);
        }
    
    }

    
    public void getTheClaim(){
        string clmID = ApexPages.currentPage().getparameters().get('clmID');
        if(clmID !=''){
            for(ovr_claim__c c :clmList){
                
                if(c.id == clmID){
                    clm = c;
                    loadRefunds();
                    break;
                }
            
            }
        }
    }
    

    public pagereference cancelCLM(){
        clm = null;
        return null;
        
    }

    
    public void saveCLMNew(){
        try{
        
        try{
            
            upsert clm;
            loadClaims();
            loadRefunds();
            cloneClmID = clm.id;
            newClaim();            
        }catch(dmlexception e){
            error =true;    
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)); 
            }
        }
        
        }catch(exception e){
            error=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage() +' '+e.getlinenumber()); 
        }
    }
    
    public void saveCLM(){
        error=false;
        try{
        
        try{
            clm.recordtypeid = Schema.SObjectType.Ovr_Claim__c.getRecordTypeInfosByName().get('Claim').getRecordTypeId();
            upsert clm;
            loadClaims();
            loadRefunds();
            ApexPages.CurrentPage().getParameters().put('clmID',clm.id);
            getTheClaim();
                        
        }catch(dmlexception e){
            error =true;    
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)); 
            }
        }
        
        }catch(exception e){
            error=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage() +' '+e.getlinenumber()); 
        }
        
    }

    
    public void deleteCLM(){
        string clmID = ApexPages.currentPage().getparameters().get('clmID');
        if(clmID !=''){
            for(ovr_claim__c c :clmList){
                
                if(c.id == clmID){
                    clm = c;
                    delete clm;
                    clm=null;
                    loadRefunds();
                    loadClaims();
                    break;
                }
            
            }
        }
    }
    
    /* End Claim */
    
   /* Subrogation Claims */
   
    public void newSbg(){
        sbg = new ovr_claim__c(ovr_control__c=theID, recordtypeid=Schema.SObjectType.Ovr_Claim__c.getRecordTypeInfosByName().get('Subrogation').getRecordTypeId());
        
    }
   
    public void saveSBGNew(){
        try{
        
        try{
            
            upsert sbg;
            loadSbgClaims();
            newSbg();
       
        }catch(dmlexception e){
            error =true;    
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)); 
            }
        }
        
        }catch(exception e){
            error=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage() +' '+e.getlinenumber()); 
        }
    }
    

    void loadsbgClaims(){
        sbgList = new ovr_claim__c[]{};
          
        for(ovr_claim__c s : [select Member_First_Name__c,
                                     Member_Last_Name__c,
                                     Patient_First_Name__c,
                                     Patient_Last_Name__c, 
                                     Amt_Reimbursed__c,
                                     HDP_Contingency__c,
                                     Date_of_Accident__c,
                                     Date_Paid__c,
                                     OVR_Control__c,
                                     Lien_Amount__c,
                                     HDP_Contingency_Percentage__c,
                                     Comments__c from ovr_claim__c where ovr_control__c = :theID and recordtype.name = 'Subrogation' order by createdDate Desc]){
                          
            sbgList.add(s);
                          
        }
        
    
    }

    
    public void getThesbgClaim(){
        string sbgID = ApexPages.currentPage().getparameters().get('sbgID');
        ApexPages.currentPage().getparameters().put('sbgID', null);
        if(sbgID !=''){
            for(ovr_claim__c s :sbgList){
                
                if(s.id == sbgID){
                    sbg = s;
                    break;
                }
            
            }
        }
    }

    
    public pagereference cancelSBG(){
        sbg= null;
        return null;
        
    }

    
    public void saveSbgCLM(){
        error=false;
        try{
        
        try{
            sbg.recordtypeid=Schema.SObjectType.Ovr_Claim__c.getRecordTypeInfosByName().get('Subrogation').getRecordTypeId();
            upsert sbg;
            loadSbgClaims();
            
                       
                        
        }catch(dmlexception e){
            error =true;    
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)); 
            }
        }
        
        }catch(exception e){
            error=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage() +' '+e.getlinenumber()); 
        }
        
    }
    
    public void deleteSBG(){
        string sbgID = ApexPages.currentPage().getparameters().get('sbgID');
        if(sbgID !=''){
            for(ovr_claim__c s :sbgList){
                
                if(s.id == sbgID){
                    sbg = s;
                    delete sbg;
                    sbg=null;
                    loadSbgClaims();
                    break;
                }
            
            }
        }
    }
    
    /* End Subrogation */
  
  
    /* Attachments */
    
    void loadAttachments(){
        
        attachments = new ftpAttachment__c[]{};
        attachments = [select subDirectory__c, Control__c, comments__c, fileName__c, name, createdDate, createdby.name from ftpAttachment__c where control__c = :theID and control__c != ''];
        
    }
    
    public pageReference getTheAttachment(){
        
        String attachmentID= ApexPages.currentPage().getparameters().get('attachmentID');
        system.debug(attachmentID);
        ApexPages.currentPage().getparameters().put('attachmentID', null);
        for(ftpAttachment__c f : attachments){
            if(f.id==attachmentID){
                attach = f;
                break;
            }
        }
        
        return null;
        
    }
    
    public pageReference deleteAttachment(){
        String deleteID = ApexPages.currentPage().getparameters().get('attachmentID');
        String recordName = ApexPages.currentPage().getparameters().get('recordName');
        
        ApexPages.currentPage().getparameters().put('attachmentID', null);
        ApexPages.currentPage().getparameters().put('recordName', null);
        
        ussscorpionFileSharing.deleteAttachment(recordName);
        
        for(ftpAttachment__c f : attachments){
                if(f.id==deleteID){
                    delete f;
                    loadAttachments();
                    attach=null;
                    break;
                }
        }
        
        return null;
    }
    
    public pageReference cancelAttachment(){
        attach=null;
        init();
        return null;
        
    }
    
    public void uploader(){
         
        string fileName = ApexPages.CurrentPage().getParameters().get('fileName');
                
        fileName= fileName.replaceAll('-', '_');
        fileName= fileName.replaceAll(',', '_');
        fileName= fileName.replaceAll('#', '_');
        coeDocumentStorage ds = new coeDocumentStorage();
        ds.saveOVRDocument(theID, fileDirectory, fileName, fileDescription);
        
             /*
             
             HttpResponse res = ussscorpionFileSharing.uploader(cci.ID+'_'+ this.fileName, this.fileContent , this.fileBody);
             
             if(res.getStatusCode() < 250){
                 attach = new ftpAttachment__c();
                 attach.fileName__c = cci.ID+'_'+this.fileName;
                 attach.contentType__c = this.fileContent;
                 attach.name=this.fileName;
                 attach.inquiry__c = cci.ID;
                 upsert attach;
                 
             }else{
                 ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, res.getBody()));
                 attach = new ftpAttachment__c();
                 return;
             }
             
         }else{
             upsert attach;
             
         }
         attach=null;
         */
         fileDescription='';
         loadAttachments();
        
     }
     
     public PageReference ussscorpion(){ 
         
        string recordName = ApexPages.CurrentPage().getParameters().get('recordName');
        ApexPages.CurrentPage().getParameters().put('recordName', null);
        return ussscorpionFileSharing.getter(recordName);
     
     }
    
    /* End Attachments */
    
    /* Requests */
    
    public void reqPagination(){
    
        if(reqFirst==null){
            reqFirst =0;
        }
        
        string dir = ApexPages.currentPage().getparameters().get('reqPage');
        
        if(dir=='forward'){
            if(reqList.size()>(reqFirst+10)){
                reqFirst += 10;
            }
        }else{
            reqFirst -= 10;
        }
        
        if(reqFirst<0){reqFirst=0;}
        
    
    }
    
    void loadReq(){
        reqlist = new ovr_request__c[]{};
        reqlist= [select name, 
                          Letter_Type__c,
                          OVR_Control__c,
                          Request_Date__c,
                          Request_Type__c from ovr_request__c where ovr_control__c  = :theID order by Request_Date__c desc];
        
    
    }
    
    public void getTheReq(){
    
        string reqID = ApexPages.currentPage().getparameters().get('reqID');
        
        if(reqID!=''){
            for(ovr_request__c r : reqlist){
                if(r.id == reqID){
                    req=r;
                    break;
                }
            }
        }
        
    
    }
    
    public void newReq(){
        req = new ovr_request__c(ovr_control__c = theID, Request_Date__c=date.today());
    }
    
    public void cancelReq(){
        req = null;
    }    
    
    public void saveReq(){
        
        try{
        
        try{
            upsert req;
            loadReq();
            req= null;
        
        }catch(dmlexception e){
        
          for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)); 
          }
            
        }
        
        }catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage()); 
        }    
        
    }
    
    public void delReq(){
        
        string reqID = ApexPages.currentPage().getparameters().get('reqID');
        
        if(reqID!=''){
            for(ovr_request__c r : reqlist){
                if(r.id == reqID){
                    req=r;
                    break;
                }
            }
        }
        
        delete req;
        loadReq();
        req=null;
        
    }
    
    /* End Requests */
    
    /* Refund */
    
    public void newRefund(){
        
        if(clm.id != null){
            refund = new ovr_refund__c(OVR_Claim__c=clm.id, Refund_Date__c=date.today());
        }  
    }
    
    void loadRefunds(){
        
        if(clm != null && clm.id!=null){
            
            refundList = refundMap.get(clm.id);//[select Refund_Amount__c, Refund_Date__c from ovr_refund__c where OVR_Claim__c = :clm.id order by Refund_Date__c desc];
        }
        
        if(refundList==null){
            refundList = new ovr_refund__c[]{};
        }
    }
    
    public void saveRefund(){
        
        try{
        try{
        
            if(clm.id!=null){
                upsert refund;
                loadRefunds();
                loadClaims();
                ApexPages.CurrentPage().getParameters().put('clmID',clm.id);
                getTheClaim();
            
            }
        
        }catch(dmlexception e){
        
          for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)); 
          }
            
        }
        
        }catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage()); 
        }    
    
    }
    
    public void getTheRefund(){
    
        string refID = ApexPages.currentPage().getparameters().get('refID');
        if(refID !=''){
            for(ovr_refund__c r :refundList){
                
                if(r.id == refID){
                    refund = r;
                    break;
                }
            
            }
        }
    
    }
    
    public void deleteRefund(){
    
        string refID = ApexPages.currentPage().getparameters().get('refID');
        if(refID !=''){
            for(ovr_refund__c r :refundList){
                
                if(r.id == refID){
                    delete r;
                    loadClaims();
                    loadRefunds();
                    ApexPages.CurrentPage().getParameters().put('clmID',clm.id);
                    getTheClaim();
                    break;
                }
            
            }
        }
    
    }
    
    /* End Refund */
    
    public void saveOVR(){
        error = false;
        try{
        
        try{    
            
            /*
            if((clientName!=ovr.ovr_client__r.name)){
                       
                    if(ovr.ovr_client__r.name==null && ovr.ovr_client__c != null){
                        
                            
                    }else{
                    
                    ApexPages.CurrentPage().getParameters().put('clientName',clientName );
                    searchClient();
                    
                    if(clientResult!= null && clientResult.size()==1){
                        ovr.ovr_client__c = clientResult[0].id;
                    }else if(clientResult!=null && clientResult.size()>1){
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'More than one client found for '+clientName ));
                        error=true;
                        return;
                    }
                    
                    }
                    
                    
            }
            */
            
            upsert ovr;
        }catch(dmlException e){    
            error=true;
            
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i) + e.getLineNumber()); 
            }
        }
        
        }catch(exception e){
            error=true;
            
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage() + e.getLineNumber());
        }
        
    }
    
    /* Checks */
    
    public void newCheck(){
        chk = new ovr_check__c(ovr_control__c=ovr.id);
    }
    
    public void cancelChk(){
        chk = null;
    }
    
    public void saveChk(){
        try{
        try{
            upsert chk;
            loadChecks();
            chk=null;
        }catch(dmlexception e){
        
          for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getDmlMessage(i)));
                System.debug(e.getDmlMessage(i)); 
          }
            
        }
        
        }catch(exception e){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
            System.debug(e.getMessage()); 
        }    
            
    }
    
    public void getCheck(){
        
        string chkID = ApexPages.currentPage().getparameters().get('chkID');
        
        if(chkID!=''){
            for(ovr_check__c c : chkList){
            
                if(chkID==c.id){
                    chk = c;
                    break;
                }
            
            }
        }
        
    }
    
    public void deleteCheck(){
        string chkID = ApexPages.currentPage().getparameters().get('chkID');
        
        if(chkID!=''){
            for(ovr_check__c c : chkList){
            
                if(chkID==c.id){
                    delete c;
                    loadChecks();
                    break;
                }
            
            }
        }
    }
    
    void loadChecks(){
        chkList = new ovr_check__c[]{};
        chkList = [select name, id, Check_Type__c , check_amount__c,Check_Received__c,Tech_Received__c,Finance_Received__c, ovr_control__c from ovr_check__c where ovr_control__c = :theID];
    }
    
   
    /* End Checks */
     
     public List<SelectOption> getProcessesorPicklist() {
     
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',''));
        
        for(Claims_Processor_Crosswalk__c c :[select name from Claims_Processor_Crosswalk__c where isActive__c = true order by name asc]){
        
            options.add(new SelectOption(c.name,c.name));
        }
        
        return options;
     }
          
     void loadClientMap(){
    
        if(client==null){
            
            client = new List<SelectOption>();
            client.add(new SelectOption('', ''));
            for(Client__c  c : [select underwriter__c,active__c, name from Client__c where underwriter__c != '' and active__c = true order by name asc]){
                client.add(new SelectOption(c.name, c.name));
            }
            
        }
    
     }

}