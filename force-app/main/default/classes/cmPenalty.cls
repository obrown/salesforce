public with sharing class cmPenalty{
    //updated 2-8-22
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText+=cm.patient__r.Patient_First_Name__c+' '+cm.patient__r.Patient_Last_Name__c+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Address__c: +'<br/>';
        letterText+='<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.City__c+', ' : ''+'&nbsp';
        letterText+=hasPatientAddress ? cm.patient__r.State__c+' ' : ''+'<br/>';
        letterText+=hasPatientAddress ? cm.patient__r.Zip__c : ''+'<br/>';
        letterText+='<br/>';

        letterText+='</div>';
        letterText+='<p>';
        letterText+='Dear&nbsp;'+cm.patient__r.Patient_First_Name__c+'&nbsp;'+cm.patient__r.Patient_Last_Name__c+',';
        letterText+='</p>';
        letterText+='<p>';  
        letterText+='Contigo Health, LLC is the third-party administrator for the '+cm.patient__r.Patient_Employer__r.name+' benefit plan. '+cm.patient__r.Patient_Employer__r.name+' is committed to supporting associates in their health management by providing a';
        letterText+=' variety of clinical programs. Our Case Management program is is available and supported by a team of experienced and credentialed case managers at no additional cost to you.<br/>';
        letterText+='</p>';

        letterText+='As per our previous letter and calls to you, your medical condition/diagnosis indicates you are an eligible candidate for the Case Management program. It is very important that I speak with you about your participation';
        letterText+=' in the Case Management Program.<br/>';

        letterText+='<p><b>';  
        letterText+='Participation in the Case Management Program is a requirement of the '+cm.patient__r.Patient_Employer__r.name+' benefit plan as noted in your benefit enrollment information and summary plan description. A monetary penalty of';
        letterText+=' $2,500 will apply for services related to the identified Case Management medical condition for members who decline or are nonresponsive to Case Management telephone calls and/or letters.';
        letterText+=' Penalties do not apply toward deductibles or out-of-pocket maximums. Additionally, ANY claims received for the associate not participating in the Case Management program will be denied';
        letterText+=' until the monetary penalty fee has been paid.';        
        letterText+='</p></b>';        
        
        letterText+='<p><b>';  
        letterText+='Please call me, as soon as possible, at 1-877-891-2690, to avoid financial penalties.</b><br/>';
        letterText+='My office hours are Monday-Friday from 8:30AM – 5:00PM Eastern Time.  If I am unable to take your call personally, please leave a confidential voice mail message that includes the best time of day to';
        letterText+=' reach you along with your telephone number, including area code, so I can get back to you quickly.<b> If I do not receive a call from you or your representative by</b> <b><u>(10 days from mailing date),</u>';
        letterText+=' financial penalties will be applied related to this Case Management benefit.</b><br/>';
        letterText+='</p>';         
        
        letterText+='I encourage you to continue to see your physician as necessary and I look forward to hearing from you soon.<br/><br/>';

        letterText+='Sincerely,<br/><br/>';
        letterText+='Care Management Department<br/>';
        letterText+='Contigo Health<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';

        return letterText;
    }
    
}