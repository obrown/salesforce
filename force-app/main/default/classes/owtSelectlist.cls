public without sharing class owtSelectlist{
    
    //selectoption[] workSource;
    //final string workSourceVal = 'Work Source';
    
    selectoption[] documentType;
    final string documentTypeVal = 'documentType';
  
  
    selectoption[] workTask;
    final string workTaskVal = 'Work Task';
    
    selectoption[] firstTask;
    final string firstTaskVal = 'First Task';
    
    selectoption[] workDepartment;
    final string workDepartmentVal = 'Work Department';
    
    selectoption[] closedReason;
    final string closedReasonVal = 'Closed Reason';

    selectoption[] worktaskReason;
    final string worktaskReasonVal = 'Work Task Reason';    
    
    Operation_Work_Task_Question__c[] questions;
    public string workTaskValue {get; set;}
    public string documentTypeValue {get; set;}
    public boolean isRelated {get; set;}
    public string worktaskReasonValue {get; set;}
     
    //public void setwtValues(){
    //    workTaskValue = ApexPages.CurrentPage().getParameters().get('wtv');
    //    documentTypeValue = ApexPages.CurrentPage().getParameters().get('wttv');
    //}
    
    public void setWorkTaskType(string taskValue){
        system.debug(taskValue);
        this.workTaskValue = taskValue;
    }
    
    public owtSelectlist(boolean isRelated, string workTaskValue, string documentTypeValue, string worktaskReasonValue){
        this.isRelated=isRelated;
        this.workTaskValue=workTaskValue;
        this.documentTypeValue=documentTypeValue;   
        this.worktaskReasonValue=worktaskReasonValue;
       
        
    }
    
    public owtSelectlist(){
        isRelated = false;
        
    }
    /*
    public selectoption[] getworkSource(){
        
        workSource = new selectoption[]{};
        workSource.add(new selectoption('','--None--'));
        for(Operation_Work_Task_Question__c q : questions){
            if(q.recordType.Name==workSourceVal){
                workSource.add(new selectoption(q.Field_Value__c, q.Field_Value__c));
            }
        }
        return workSource;
    }
    */
    public selectoption[] getdocumentType(){
    
        documentType= new selectoption[]{};
        documentType.add(new selectoption('','--None--'));
        documentType.add(new selectoption('No Document', 'No Document'));
        documentType.add(new selectoption('Accident Questionnaire', 'Accident Questionnaire'));
        documentType.add(new selectoption('Attorney Letter', 'Attorney Letter')); 
        documentType.add(new selectoption('Authorized Release Form', 'Authorized Release Form'));
        documentType.add(new selectoption('COB Questionnaire', 'COB Questionnaire'));
        documentType.add(new selectoption('Hand Key Claim', 'Hand Key Claim'));
        documentType.add(new selectoption('Medical Records', 'Medical Records'));
        documentType.add(new selectoption('Medpay Ledger', 'Medpay Ledger'));
        documentType.add(new selectoption('Power of Attorney Review', 'Power of Attorney Review'));
        documentType.add(new selectoption('Refund/Void Check', 'Refund/Void Check'));
        documentType.add(new selectoption('Primary Carrier EOB - (EB)', 'Primary Carrier EOB - (EB)'));
        documentType.add(new selectoption('Workers Comp Letter (WC)', 'Workers Comp Letter (WC)'));        
        
        /*
        for(Operation_Work_Task_Question__c q : questions){
            if(q.recordType.Name==workTaskTypeVal){
                workTaskType.add(new selectoption(q.Field_Value__c, q.Field_Value__c));
            }
        }
        */
        
        return documentType;
    }
    
    public string gethpdocumentType(){
        
        string hpdocumentType= 'NA';
        
        if(documentTypeValue=='COB Questionnaire'){
            hpdocumentType='CB';
        
        }else if(documentTypeValue=='Accident Questionnaire'){
            hpdocumentType='AI';    
            
        }else if(documentTypeValue=='Medical Records'){
            hpdocumentType='MR';
        
        }else if(documentTypeValue=='Authorized Release Form'){
            hpdocumentType='AU';        

        }else if(documentTypeValue=='Power of Attorney Review'){
            hpdocumentType='PA';
        
        }else if(documentTypeValue=='Refund/Void Check'){
            hpdocumentType='RV';
        
        }else if(documentTypeValue=='Hand Key Claim'){
            hpdocumentType='CL';        
        
        }else if(documentTypeValue=='Attorney Letter'){
            hpdocumentType='AL'; 
            
        }else if(documentTypeValue=='Medpay Ledger'){
            hpdocumentType='MP';
            
        }else if(documentTypeValue=='Primary Carrier EOB - (EB)'){
            hpdocumentType='EB';
            
        }else if(documentTypeValue=='Workers Comp Letter (WC)'){
            hpdocumentType='WC';
            
        }else if(documentTypeValue=='No Document'){
            hpdocumentType='ND';
        }
       
        return hpdocumentType;
    }

    public string getdocumentDescription(){
        
        string documentDescription= '';
        
        if(documentTypeValue=='COB Questionnaire'){
            documentDescription='COB Information Received';
        
        }else if(documentTypeValue=='Accident Questionnaire'){
            documentDescription='Accident Information Received';
       
        }else if(documentTypeValue=='Medical Records'){
            documentDescription='Medical Records Review';

        }else if(documentTypeValue=='Authorized Release Form'){
            documentDescription='Authorization Form Received';

        }else if(documentTypeValue=='Power of Attorney Review'){
            documentDescription='Power of Attorney Received';

        }else if(documentTypeValue=='Refund/Void Check'){
            documentDescription='Refund/Void Check';

        }else if(documentTypeValue=='Hand Key Claim'){
            documentDescription='Hand Key Claim';
                       
        }else if(documentTypeValue=='Attorney Letter'){
            documentDescription='Attorney Letter Received';
            
        }else if(documentTypeValue=='Medpay Ledger'){
            documentDescription='Medpay Ledger Received';

        }else if(documentTypeValue=='Primary Carrier EOB - (EB)'){
            documentDescription='Primary Carrier EOB';
                    
        }else if(documentTypeValue=='Workers Comp Letter (WC)'){
            documentDescription='Workers Comp Letter';
            
        }else if(documentTypeValue=='No Document'){
            documentDescription='No Document Received';
                
        }
        
        return documentDescription;
    }
    
    public selectoption[] getworkTask(){
        workTask= new selectoption[]{};
        
        workTask.add(new selectoption('','--None--'));
        system.debug('documentTypeValue ' +documentTypeValue);
        if(documentTypeValue=='COB Questionnaire'){
                workTask.add(new selectoption('COB Review','COB Review'));
                workTaskValue='COB Review';
                
            }else if(documentTypeValue=='Accident Questionnaire'){
                workTask.add(new selectoption('Accident Review','Accident Review'));
                

            }else if(documentTypeValue=='Medical Records'){
                workTask.add(new selectoption('Medical Records Review','Medical Records Review'));
                workTaskValue='Medical Records Review';

            }else if(documentTypeValue=='Authorized Release Form'){
                workTask.add(new selectoption('Authorization Review','Authorization Review'));
                workTaskValue='Authorization Review';
                
            }else if(documentTypeValue=='Power of Attorney Review'){
                workTask.add(new selectoption('Power of Attorney Review','Power of Attorney Review'));
                workTaskValue='Power of Attorney Review';                

            }else if(documentTypeValue=='Refund/Void Check'){
                workTask.add(new selectoption('Refund/Void Check','Refund/Void Check'));
                workTaskValue='Refund/Void Check';

            }else if(documentTypeValue=='Hand Key Claim'){
                workTask.add(new selectoption('Hand Key Claim','Hand Key Claim'));
                workTaskValue='Hand Key Claim';
                                
            }else if(documentTypeValue=='Attorney Letter'||documentTypeValue=='Medpay Ledger'){
                workTask.add(new selectoption('Subro Activity','Subro Activity'));
                workTaskValue='Subro Activity';

            }else if(documentTypeValue=='Primary Carrier EOB - (EB)'){
                workTask.add(new selectoption('Primary Carrier EOB','Primary Carrier EOB'));
                workTaskValue='Primary Carrier EOB';                
                            
            }else if(documentTypeValue=='Workers Comp Letter (WC)'){
                workTask.add(new selectoption('Workers Comp Review','Workers Comp Review'));
                workTaskValue='Workers Comp Review';
                            
            }else if(documentTypeValue=='No Document'){
                workTask.add(new selectoption('Accumulator Review','Accumulator Review')); 
                workTask.add(new selectoption('Additional Information Needed','Additional Information Needed'));           
                workTask.add(new selectoption('Claim Adjustment','Claim Adjustment'));
                workTask.add(new selectoption('COB Flag','COB Flag'));
                workTask.add(new selectoption('Customer Service Outreach','Customer Service Outreach'));
                workTask.add(new selectoption('LCC Requests','LCC Requests'));
                workTask.add(new selectoption('Multiple EOB\'s','Multiple EOB\'s'));
                workTask.add(new selectoption('Network Exception Request (Approved)','Network Exception Request (Approved)'));
                workTask.add(new selectoption('Network Exception Request (Denied)','Network Exception Request (Denied)'));
                workTask.add(new selectoption('Network Exception Request (No Prior Contact)','Network Exception Request (No Prior Contact)'));
                workTask.add(new selectoption('Network Exception - Nurse Review','Network Exception - Nurse Review'));
                workTask.add(new selectoption('Network Exception - Records Only','Network Exception - Records Only'));
                workTask.add(new selectoption('Paid Claims Report','Paid Claims Report'));
                workTask.add(new selectoption('Plan Build','Plan Build'));
                workTask.add(new selectoption('Pursue Network Exception','Pursue Network Exception'));
                workTask.add(new selectoption('Remove Future Dated Letters','Remove Future Dated Letters'));
                workTask.add(new selectoption('Special Project','Special Project'));
                workTask.add(new selectoption('Subro Activity','Subro Activity'));
                workTask.add(new selectoption('Subrogation Initiation','Subrogation Initiation'));
                workTask.add(new selectoption('Supervisor Review','Supervisor Review'));
                workTask.add(new selectoption('Verify Active Litigation','Verify Active Litigation'));
                workTask.add(new selectoption('Verify Eligibility','Verify Eligibility'));
                workTask.add(new selectoption('Verify Workers Comp','Verify Workers Comp'));

                workTask.add(new selectoption('Other','Other'));  
                            
            } 
        
        return workTask;
    }
    
    public selectoption[] getworkDepartment(){
        workDepartment= new selectoption[]{};
        
        //system.debug('isRelated '+isRelated);
        //system.debug('workTaskValue '+workTaskValue);
        workDepartment.add(new selectoption('','--None--'));
            
            if(workTaskValue=='Claim Adjustment'&&worktaskReasonValue=='Appeal Overturn'){
                    workDepartment.add(new selectoption('Claims Technical','Claims Technical'));
                    workDepartment.add(new selectoption('Claims Processing','Claims Processing'));
  
                    return workDepartment;

                
                
            }else{
            
            if(workTaskValue=='COB Review'||workTaskvalue=='Refund/Void Check'||workTaskvalue=='Pursue Network Exception'||workTaskvalue=='COB Flag'){
                workDepartment.add(new selectoption('Claims Technical','Claims Technical'));
              
            
            }else if(workTaskValue=='Accident Review'){
                workDepartment.add(new selectoption('Claims Support','Claims Support'));
                
            }else if(workTaskValue=='Claim Adjustment'||workTaskValue=='Hand Key Claim'||workTaskValue=='Primary Carrier EOB'){
                workDepartment.add(new selectoption('Claims Processing','Claims Processing'));            
                //workDepartment.add(new selectoption('Claims Technical','Claims Technical'));
                
               
            }else if(workTaskValue=='Subrogation Initiation'||workTaskValue=='Subro Activity'){
                workDepartment.add(new selectoption('Claims Support','Claims Support'));  

            }else if(workTaskValue=='Customer Service Outreach'){
                workDepartment.add(new selectoption('Customer Service','Customer Service'));  

            }else if(workTaskValue=='Supervisor Review'){
                workDepartment.add(new selectoption('Claims Processing','Claims Processing')); 
                workDepartment.add(new selectoption('Claims Support','Claims Support'));
                workDepartment.add(new selectoption('Claims Technical','Claims Technical'));
                workDepartment.add(new selectoption('Customer service','Customer service'));
   


            }else if(workTaskValue=='LCC Requests'||workTaskValue=='Paid Claims Report'||workTaskValue=='Multiple EOB\'s'){
                workDepartment.add(new selectoption('Admin Support','Admin Support')); 
                
           }else if(workTaskValue=='Other'){
                    workDepartment.add(new selectoption('Account Management','Account Management'));
                    workDepartment.add(new selectoption('Analytics','Analytics'));
                    workDepartment.add(new selectoption('Benefit Services','Benefit Services'));
                    workDepartment.add(new selectoption('Care Management','Care Management'));
                    workDepartment.add(new selectoption('Claim Quality','Claim Quality'));
                    workDepartment.add(new selectoption('Claims Processing','Claims Processing'));
                    workDepartment.add(new selectoption('Claims Technical','Claims Technical'));
                    workDepartment.add(new selectoption('Claims Support','Claims Support'));
                    workDepartment.add(new selectoption('Compliance','Compliance'));
                    workDepartment.add(new selectoption('Customer Service','Customer Service'));
                    workDepartment.add(new selectoption('Eligibility','Eligibility'));
                    workDepartment.add(new selectoption('Finance','Finance'));
                    workDepartment.add(new selectoption('Patient Centered Programs (PCP)','Patient Centered Programs (PCP)'));
                    workDepartment.add(new selectoption('Plan Build','Plan Build'));
                    workDepartment.add(new selectoption('Provider Network','Provider Network'));
            
            }else if(workTaskValue=='Plan Build'){
                workDepartment.add(new selectoption('Plan Build','Plan Build'));  

            }else if(workTaskValue=='Network Exception - Nurse Review'){
                workDepartment.add(new selectoption('PCP Nurse','PCP Nurse'));
                
            }else if(workTaskValue=='Accumulator Review'||workTaskValue=='Pursue Network Exception'){
                workDepartment.add(new selectoption('Claims Technical','Claims Technical'));  

            }else if(workTaskValue=='Network Exception Request (No Prior Contact)'||workTaskValue=='Network Exception Request (Approved)'||
            workTaskValue=='Network Exception Request (Denied)'||workTaskValue=='Network Exception - Records Only'){
                workDepartment.add(new selectoption('Member Advocates','Member Advocates'));                
                                  
            //}else if(workTaskValue=='Verify Eligibility'||workTaskValue=='Verify Workers \' Comp'||workTaskValue=='Verify Active Litigation'){
            }else if(workTaskValue=='Verify Eligibility'||workTaskValue=='Verify Workers Comp'||workTaskValue=='Verify Active Litigation'){            
                workDepartment.add(new selectoption('Member Advocates','Member Advocates'));
               
            }else if(workTaskValue=='Special Project'){
                workDepartment.add(new selectoption('Claims Processing','Claims Processing'));
                workDepartment.add(new selectoption('Claims Support','Claims Support')); 
                workDepartment.add(new selectoption('Claims Technical','Claims Technical'));
                workDepartment.add(new selectoption('Customer Service','Customer Service')); 

            }else if(workTaskValue=='Medical Records Review'){
                
                workDepartment.add(new selectoption('Plan Build','Plan Build'));
                workDepartment.add(new selectoption('Care Management','Care Management')); 
                workDepartment.add(new selectoption('Claims Support','Claims Support')); 
                workDepartment.add(new selectoption('Claims Technical','Claims Technical'));


            }else if(workTaskValue=='Authorization Review'||workTaskValue=='Power of Attorney Review'){
               // workDepartment.add(new selectoption('','--None--'));
                workDepartment.add(new selectoption('Compliance','Compliance')); 
                               
            }else if(worktaskReasonValue=='Copy of Divorce Decree'||worktaskReasonValue=='Copy of Medicare Card'||
                    worktaskReasonValue=='Status of Policyholder'||worktaskReasonValue=='Verify Active or Retired date of coverage'){
                    
                workDepartment.add(new selectoption('Claims Technical','Claims Technical'));
            
            }else if(worktaskReasonValue=='Signature Needed on form'&&workTaskValue=='Additional Information Needed'||
                     worktaskReasonValue=='Other'&&workTaskValue=='Additional Information Needed'){
                    workDepartment.add(new selectoption('Account Management','Account Management'));
                    workDepartment.add(new selectoption('Analytics','Analytics'));
                    workDepartment.add(new selectoption('Benefit Services','Benefit Services'));
                    workDepartment.add(new selectoption('Care Management','Care Management'));
                    workDepartment.add(new selectoption('Claim Quality','Claim Quality'));
                    workDepartment.add(new selectoption('Claims Processing','Claims Processing'));
                    workDepartment.add(new selectoption('Claims Technical','Claims Technical'));
                    workDepartment.add(new selectoption('Claims Support','Claims Support'));
                    workDepartment.add(new selectoption('Compliance','Compliance'));
                    workDepartment.add(new selectoption('Customer Service','Customer Service'));
                    workDepartment.add(new selectoption('Eligibility','Eligibility'));
                    workDepartment.add(new selectoption('Finance','Finance'));
                    workDepartment.add(new selectoption('Patient Centered Programs (PCP)','Patient Centered Programs (PCP)'));
                    workDepartment.add(new selectoption('Plan Build','Plan Build'));
                    workDepartment.add(new selectoption('Provider Network','Provider Network'));
                    
           }else if(workTaskValue=='Remove Future Dated Letters'||workTaskValue=='Workers Comp Review'){      
                    workDepartment.add(new selectoption('Claims Support','Claims Support'));                    
            
            }else{
              //  workDepartment.add(new selectoption('','--None--'));
            }
            
            }
            return workDepartment;
        }
        
        /*
        if(workTaskValue==null||workTaskValue==''){
            workDepartment.add(new selectoption('','Please select a work task type'));
        }else{
            workDepartment.add(new selectoption('','--None--'));
        }
        
        
        for(Operation_Work_Task_Question__c q : questions){
            if(q.recordType.Name==workDepartmentVal && workTaskValue==q.Dependent_Value__c && isRelated==q.Related_Task_Definition__c){
                workDepartment.add(new selectoption(q.Field_Value__c, q.Field_Value__c));
                valFound=true;
            }
        }
        
        */
        
    
    public selectoption[] getclosedReason(){
        closedReason= new selectoption[]{};
        
        if(workTaskValue=='COB Review'){
            closedReason.add(new selectoption('', '--None--'));
            closedReason.add(new selectoption('HDP Primary','HDP Primary'));
            closedReason.add(new selectoption('HDP Secondary','HDP Secondary'));
            closedReason.add(new selectoption('No Other Ins.','No Other Ins.'));
            closedReason.add(new selectoption('Additional Info. Needed','Additional Info. Needed'));
            closedReason.add(new selectoption('Complete', 'Complete'));
            closedReason.add(new selectoption('Duplicate', 'Duplicate'));
            
        }else if(workTaskValue=='Accident Review'){
            closedReason.add(new selectoption('', '--None--'));
            closedReason.add(new selectoption('No TPL', 'No TPL'));
            closedReason.add(new selectoption('COB/MedPay','COB/MedPay'));
            closedReason.add(new selectoption('Subro Case Initiated','Subro Case Initiated'));
            closedReason.add(new selectoption('Workers Comp','Workers Comp'));
            closedReason.add(new selectoption('Additional Information Needed','Additional Information Needed'));
            closedReason.add(new selectoption('Complete', 'Complete'));
            closedReason.add(new selectoption('Duplicate', 'Duplicate'));

        }else if(workTaskValue=='Network Exception Request (No Prior Coverage)'){
            closedReason.add(new selectoption('', '--None--'));
            closedReason.add(new selectoption('Education Only', 'Education Only'));
            closedReason.add(new selectoption('Intake Created','Intake Created'));
            closedReason.add(new selectoption('Complete', 'Complete'));
            closedReason.add(new selectoption('Duplicate', 'Duplicate'));
           
            
        }else if(workTaskValue=='Network Exception Request (No Prior Contact)'){
            closedReason.add(new selectoption('', '--None--'));
            closedReason.add(new selectoption('Transitioned to Intake', 'Transitioned to Intake'));
            closedReason.add(new selectoption('Pursue Network Exception', 'Pursue Network Exception'));
            closedReason.add(new selectoption('Not Pursuing Surgery', 'Not Pursuing Surgery')); 
            closedReason.add(new selectoption('Unable to Contact/Sending Letter', 'Unable to Contact/Sending Letter'));  
            closedReason.add(new selectoption('Complete', 'Complete')); 
            closedReason.add(new selectoption('Duplicate', 'Duplicate'));
                             
        }else if(workTaskValue=='Medical Records Review'){
            closedReason.add(new selectoption('', '--None--'));
            closedReason.add(new selectoption('Duplicate Records', 'Duplicate Records'));
            closedReason.add(new selectoption('Records Unsolicited','Records Unsolicited')); 
            closedReason.add(new selectoption('Complete', 'Complete'));
            closedReason.add(new selectoption('Duplicate', 'Duplicate'));   
                 
        }else if(workTaskValue=='Plan Build'){
            closedReason.add(new selectoption('', '--None--'));
            closedReason.add(new selectoption('Added code(s) to system for benefit mapping', 'Added code(s) to system for benefit mapping'));
            closedReason.add(new selectoption('Added description of code', 'Added description of code'));
            closedReason.add(new selectoption('Audit inquiry', 'Audit inquiry'));
            closedReason.add(new selectoption('Changed b/c to apply correct benefits', 'Changed b/c to apply correct benefits'));
            closedReason.add(new selectoption('Fixed BAD benefit code', 'Fixed BAD benefit code'));
            closedReason.add(new selectoption('Fixed INT (interim) benefit code', 'Fixed INT (interim) benefit code'));
            closedReason.add(new selectoption('Fixed PND benefit code', 'Fixed PND benefit code'));
            closedReason.add(new selectoption('Identified ICD9 match for ICD10', 'Identified ICD9 match for ICD10'));
            closedReason.add(new selectoption('Workflow', 'Workflow'));
            closedReason.add(new selectoption('Other', 'Other'));
            closedReason.add(new selectoption('Complete', 'Complete'));
            closedReason.add(new selectoption('Duplicate', 'Duplicate')); 
            
        }else if(documentTypeValue=='Attorney Letter'){
               closedReason.add(new selectoption('', '--None--'));
               closedReason.add(new selectoption('Initiate In house Settlement', 'Initiate In house Settlement'));
               closedReason.add(new selectoption('Refer Case to Subro Vendor', 'Refer Case to Subro Vendor'));
               closedReason.add(new selectoption('Complete', 'Complete'));
               closedReason.add(new selectoption('Duplicate', 'Duplicate')); 
               
        }else if(documentTypeValue=='Medpay Ledger'){
               closedReason.add(new selectoption('', '--None--'));
               closedReason.add(new selectoption('Claim Adjustment Needed', 'Claim Adjustment Needed'));
               closedReason.add(new selectoption('No Adjustment Necessary', 'No Adjustment Necessary'));
               closedReason.add(new selectoption('Complete', 'Complete'));
               closedReason.add(new selectoption('Duplicate', 'Duplicate'));
                        
        
        }else if(documentTypeValue=='Workers Comp Letter (WC)'){
               closedReason.add(new selectoption('', '--None--'));
               closedReason.add(new selectoption('WC Approval - Plan Exclusion', 'WC Approval - Plan Exclusion'));
               closedReason.add(new selectoption('WC Denial - Plan Allowable', 'WC Denial - Plan Allowable'));
               closedReason.add(new selectoption('Complete', 'Complete'));
               closedReason.add(new selectoption('Duplicate', 'Duplicate'));         
        
        }else{
                closedReason.add(new selectoption('Complete','Complete'));
                closedReason.add(new selectoption('Duplicate', 'Duplicate')); 
                
        }
        
        
        return closedReason;
    }
    
    public selectoption[] getworktaskReason(){
     if(workTaskValue=='Claim Adjustment'){
        worktaskReason= new selectoption[]{};
        
        worktaskReason.add(new selectoption('','--None--'));
        worktaskReason.add(new selectoption('Appeal Overturn', 'Appeal Overturn'));
        worktaskReason.add(new selectoption('Retro Add/Termination', 'Retro Add/Termination'));
        worktaskReason.add(new selectoption('Accident Review', 'Accident Review'));
        worktaskReason.add(new selectoption('COB Review', 'COB Review'));
        worktaskReason.add(new selectoption('Corrected Pricing', 'Corrected Pricing'));
        worktaskReason.add(new selectoption('Processor Error', 'Processor Error'));
        worktaskReason.add(new selectoption('Incorrect NetCode', 'Incorrect NetCode'));
        worktaskReason.add(new selectoption('Care Management Review', 'Care Management Review'));
        worktaskReason.add(new selectoption('Quantum Retro Review', 'Quantum Retro Review'));
        worktaskReason.add(new selectoption('W9 Received', 'W9 Received'));
        worktaskReason.add(new selectoption('Other', 'Other'));
        
//4-11-19 obrown added work task reason        
    }else if(workTaskValue=='Accumulator Review'){
    
        worktaskReason= new selectoption[]{};
        
        worktaskReason.add(new selectoption('','--None--'));
        worktaskReason.add(new selectoption('Accum Review from Customer Service', 'Accum Review from Customer Service'));
        worktaskReason.add(new selectoption('Accum Review from Client Service', 'Accum Review from Client Service'));
        worktaskReason.add(new selectoption('Overage Reports', 'Overage Reports'));
        worktaskReason.add(new selectoption('Bridgestone Surviving Spouse', 'Bridgestone Surviving Spouse'));
        worktaskReason.add(new selectoption('PBMX Review', 'PBMX Review'));
        worktaskReason.add(new selectoption('Accum Error Review', 'Accum Error Review'));
        worktaskReason.add(new selectoption('Response File Review', 'Response File Review'));
        worktaskReason.add(new selectoption('Cobra Transfer Report', 'Cobra Transfer Report'));
        worktaskReason.add(new selectoption('Cooper Active to Retiree Report', 'Cooper Active to Retiree Report'));
        worktaskReason.add(new selectoption('Cooper Surviving Spouse Report', 'Cooper Surviving Spouse Report'));    
        worktaskReason.add(new selectoption('Patient Group Change Report', 'Patient Group Change Report'));
        worktaskReason.add(new selectoption('Dual Eligibility Report', 'Dual Eligibility Report'));  
        worktaskReason.add(new selectoption('Member Pend Report', 'Member Pend Report'));
   
    }else if(workTaskValue=='Additional Information Needed'){
        worktaskReason= new selectoption[]{};
        worktaskReason.add(new selectoption('','--None--'));
        worktaskReason.add(new selectoption('Copy of Divorce Decree', 'Copy of Divorce Decree'));
        worktaskReason.add(new selectoption('Copy of Medicare Card', 'Copy of Medicare Card'));
        worktaskReason.add(new selectoption('Status of Policyholder', 'Status of Policyholder'));
        worktaskReason.add(new selectoption('Verify Active or Retired date of coverage', 'Verify Active or Retired date of coverage'));
        worktaskReason.add(new selectoption('Signature Needed on form', 'Signature Needed on form'));
        worktaskReason.add(new selectoption('Other', 'Other'));
        
    }else if(documentTypeValue=='Attorney Letter'||workTaskValue=='Subro Activity'){
        worktaskReason= new selectoption[]{};
        worktaskReason.add(new selectoption('','--None--'));
        worktaskReason.add(new selectoption('Attorney Letter Review', 'Attorney Letter Review'));
        worktaskReason.add(new selectoption('Send SPD', 'Send SPD'));
        worktaskReason.add(new selectoption('Send 5500', 'Send 5500'));
        worktaskReason.add(new selectoption('Initial Lien Report', 'Initial Lien Report'));
        worktaskReason.add(new selectoption('Interim Lien Report', 'Interim Lien Report'));
        worktaskReason.add(new selectoption('Final Lien Report', 'Final Lien Report')); 
        worktaskReason.add(new selectoption('Inbound Call', 'Inbound Call')); 
        worktaskReason.add(new selectoption('Outbound Call', 'Outbound Call'));    
        
    }else if(documentTypeValue=='Medpay Ledger'){
        worktaskReason= new selectoption[]{};
        worktaskReason.add(new selectoption('','--None--'));  
        worktaskReason.add(new selectoption('Medpay Ledger Review', 'Medpay Ledger Review')); 
         
    }else if(documentTypeValue=='Primary Carrier EOB - (EB)'){
        worktaskReason= new selectoption[]{};
        worktaskReason.add(new selectoption('','--None--'));  
        worktaskReason.add(new selectoption('Claim Adjustment', 'Claim Adjustment'));
         
    }    
        return worktaskReason;
    }


   
}