//
// Used to transact COE patient/nurse messages with the mobile API
//

global class coeMobileMessaging {

    public class PageInfo {
        public Integer totalResults;
        public Integer unRead;
        public Integer resultsPerPage = 20;
    }

    public class MainInfo {
        public String announcement;
        public MainContact mainContact;
        
        public MainInfo(){
            mainContact = new MainContact();
        }
    }

    public class User {
        public String name;
        public String email;
    }
    
    public class apiMessageBody {
        public integer count;
        public string subject;
        public string body;
        
    }
    
    public class Message {
        public String mid;
        public User user;
        public String dateReceived;
        public String subject;
        public String body;
        public boolean isRead;
        public Message(){
            user = new User();
        }
    }

    public MainInfo mainInfo;
    public PageInfo pageInfo;
    public List<coeMobileMessaging.User> contactOptions;
    public List<Message> messages;
    public Message message;
    public List<MessagesSent> messagesSent;

    public class MainContact {
        public String phone;
        public String email;
    }

    public class MessagesSent {
        public String mid;
        public User user;
        public String dateSent;
        public String subject;
        public String body;
        public boolean isRead;
        public String dateReceived; //actually date received
    }

}