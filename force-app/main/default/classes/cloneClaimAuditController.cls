public with sharing class cloneClaimAuditController{

    public Claim_Audit__c obj {get; private set;}
    Claim_Audit__c caHD = new Claim_Audit__c();
    public boolean error {get; private set;}
    public string newID {get;set;}

    id theID;


    public cloneClaimAuditController(ApexPages.StandardController controller) {
        
        theID = controller.getId();
        loadObj();
        
    }
    
    public cloneClaimAuditController(){
        theID = ApexPages.currentPage().getParameters().get('id');
        loadObj();
    
    }

    void loadObj(){
        
        if(theID != null){
    
            obj = [select Adjusted_Claim__c,
                          Adjusted_Claim_Reason__c,
                          Claim_Processor__c,
                          claim_Auditor__c,
                          claim__r.Amount_to_be_paid__c, 
                          claim__r.Allowed_Amount__c, 
                          claim__r.Date_Received__c, 
                          claim__r.Total_Charge__c, 
                          claim__r.Claim_Number__c, 
                          Claim__c, 
                          claim__r.Diagnosis__c,
                          claim__r.Diagnosis_Description__c,
                          claim__r.Procedure__c,
                          claim__r.Procedure_Description__c,
                          Coding_Credits__c,
                          Coding_Errors__c,
                          Comments__c,
                          Does_COB_Apply__c,
                          Does_Subrogation_Apply__c,
                          DRG_Pricing__c,
                          recordtypeid,
                          Audit_Type__c,
                          Audit_Status__c,
                          Pricing_Method_if_not_DRG__c,
                          In_Network__c,
                          Network_Name__c,
                          Negotiation_Attempted__c,
                          Negotiated_Amount__c,
                          Employee_Name__c,
                          relatedClaimAudit__c,
                          Patient_Age__c,
                          Patient_Name__c,
                          Patient_Effective_Date__c,
                          Patient_Termination_Date__c,
                          Precert__c,
                          Precert_Number_of_Days__c,
                          Relationship_to_Member__c,
                          fDOS__c, 
                          tDOS__c, 
                          Client__c,
                          name,
                          Stop_Loss_Notification_Initiated__c,
                          Stop_Loss_Notification_Date__c,
                          Unresolved_Errors__c,
                          Provider_Name__c from Claim_Audit__c where id = :theID ];
           

        }          

        
    }

    public void Submit(){
        error=false;
        
        if(obj.Unresolved_Errors__c>0){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, 'Claim Audit still has unresolved errors'));
            return;
        
        }
        
        try{
            
                //Clone current claim Audit
                caHD= obj.clone();
                caHD.Audit_Type__c='';
                caHD.Audit_Status__c='';
                caHD.Claim_Processor__c = null;
                insert caHD;

                newID = string.valueof(caHD.id);
               
        }catch(exception e){
            error=true;
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()+', line number '+e.getlineNumber()));
            //Clean newly created record
            if(caHD.id!=null){
                delete caHD;
            }
        }
        
    }
    
    
    
   

}