/**
* This class contains unit tests for validating the behavior of the Electronic application
*/
@isTest()
private class electronicWorkTaskTest {
    
  static testMethod void TestInsert() {
      Operations_Work_Task__c owt = new Operations_Work_Task__c();
      ApexPages.StandardController controller = new ApexPages.StandardController(owt);
      
      owtExtension owtE = new owtExtension(controller);
      owtE.Save();
      system.assert(null==null);
  }
  
  static testMethod void openTask() {
      Operations_Work_Task__c owt = new Operations_Work_Task__c();
      ApexPages.StandardController controller = new ApexPages.StandardController(owt);
      
      owtExtension owtE = new owtExtension(controller);
      owtE.Save();
      
      owtE.updateMyAccounts();
      
     
      system.assert(null==null);
  }
  
  static testMethod void TestRelatedTask() {
      Operations_Work_Task__c owt = new Operations_Work_Task__c();
      insert owt;
      system.assert(owt.id!=null);
      ApexPages.CurrentPage().getParameters().put('owtid', owt.id);
      
      Operations_Work_Task__c owtA = new Operations_Work_Task__c();
      ApexPages.StandardController controller = new ApexPages.StandardController(owtA);
      owtExtension owtE = new owtExtension(controller);
      owtE.Save();
      owtE = new owtExtension(controller);
      
      
      
      searchHealthpacModalController shc = new searchHealthpacModalController();
      shc.iRecordId = owt.id;
      shc.searchHealthpac();
      
      ApexPages.currentPage().getParameters().put('pEssn', '555746607');
      ApexPages.currentPage().getParameters().put('pSeq', '00');
      ApexPages.currentPage().getParameters().put('pgrp', 'CP3');
      ApexPages.currentPage().getParameters().put('puw', '034');
      ApexPages.currentPage().getParameters().put('status', 'Termed');  
      
      shc.selectResult();
      shc.resetSearch();
      
      //Test claimsearch
      
      ApexPages.currentPage().getParameters().put('claimNumber', '21800012345');
      shc.iRecordId = owt.id;
      shc.searchHealthpac();
      
      ApexPages.currentPage().getParameters().put('pclaimNumber', '21800012345');
      ApexPages.currentPage().getParameters().put('pEssn', '269728973');
      ApexPages.currentPage().getParameters().put('pSeq', '00');
      ApexPages.currentPage().getParameters().put('status', 'Active');
      
      shc.selectClaimResult();
      
      ApexPages.CurrentPage().getParameters().put('owtiID', owt.id);
      owtE.deleteTheOWT();
      
    //  OwtSubTask.getDelOWT();
    //  OwtSubTask.myDelete();
  }
}