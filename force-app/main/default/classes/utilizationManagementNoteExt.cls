public class utilizationManagementNoteExt {
    
    public Utilization_Management_Note__c umNote {get; set;}
    public boolean isError {get; private set;}
    
    public utilizationManagementNoteExt(ApexPages.StandardController controller) {
        
        umNote = (Utilization_Management_Note__c)controller.getRecord(); //[select Initiated__c, Subject__c, Contact_Type__c, Communication_Type__c, body__c from Utilization_Management_Note__c where id = :noteId];
        
        if(umNote.id==null){
            string umParent = ApexPages.CurrentPage().getParameters().get('parentId');
            umNote.Utilization_Management__c = umParent;
            
        }
        
        string imd = ApexPages.CurrentPage().getParameters().get('imd');
        if(imd=='1'){
            umNote.medicalDirectorReview__c=true;
            umNote.Contact_Type__c='Other';
            umNote.Subject__c='Medical Director Note';
            umNote.Communication_Type__c='Review';
        }
            
    }
    
    public void saveUmNote(){
        isError=false;
        try{
            
            upsert umNote;
            
        }catch(exception e){
            isError=true;
            for (Integer i = 0; i < e.getNumDml(); i++) {
                // Process exception here
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING, e.getDmlMessage(i)));
            }              
            
        }
    }
    
}