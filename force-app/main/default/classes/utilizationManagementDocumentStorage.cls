public with sharing class utilizationManagementDocumentStorage{
    
    string patientAttachmentId;
    
    public class response{
        public boolean isError;
        public string resultMessage;
    }
    
    public utilizationManagementDocumentStorage(){}
    
    public response saveDocument(string umId, string patientId, string fileDirectory, string fileName, string fileDesc){
        
        response response = createPatientDocument(patientId, fileDirectory, fileName, fileDesc);
        if(response.isError){
            return response ;
        }
        
        string patientAttachmentId = response.resultMessage;
        return createUtilizationManagementDocument(umId, fileDesc, patientAttachmentId);
    }
    
    public response saveDocument(string umId, string fileDirectory, string fileName, string fileDesc){
        
        string patientId = [select Patient__c from Utilization_Management__c where id = :umId].Patient__c;
        response response = createPatientDocument(patientId, fileDirectory, fileName, fileDesc);
        if(response.isError){
            return response ;
        }
        
        string patientAttachmentId = response.resultMessage;
        return createUtilizationManagementDocument(umId, fileDesc, patientAttachmentId);
    }
    
    response createUtilizationManagementDocument(string umId, string fileDesc, string patientAttachmentId){
        
        response response = new response();
        response.isError=true;
        
        Utilization_Management_Attachment__c attachment = new Utilization_Management_Attachment__c(Utilization_Management__c=umid);
        try{
            attachment.Population_Health_Attachment__c = patientAttachmentId;
            attachment.file_description__c=fileDesc;
            insert attachment;
            
            Population_Health_Attachment__c p_attachment = new Population_Health_Attachment__c(id=patientAttachmentId);
            p_attachment.UM_Record_List__c=[select Utilization_Management__r.name from Utilization_Management_Attachment__c where id =:attachment.id].Utilization_Management__r.name;
            
            response.isError=false;
            response.resultMessage =  attachment.id;
            Utilization_Management__c um = new Utilization_Management__c(id=umid);
            um.needs_additional_clinical__c=false;
            update um;
            
        }catch(exception e){
            response.resultMessage = e.getMessage();
            return response;
        }
        
        return response;
        
    }
    
    response createPatientDocument(id patientId, string fileDirectory,  string fileName, string fileDesc){
        
        response response = new response();
        response.isError=true;
        Population_Health_Attachment__c attachment = new Population_Health_Attachment__c(Patient__c=patientId);
        try{
            
            attachment.File_Directory__c=fileDirectory;
            attachment.File_Name__c=fileName;
            attachment.File_Description__c= fileDesc;
            attachment.ImpagePacMessageSent__c = attachment.Document_Reviewed__c = date.today();
            insert attachment;
            response.isError=false;
            response.resultMessage =  attachment.id;
        
        }catch(exception e){
            response.resultMessage = e.getMessage();
            return response;
        }
        
        return response;
    }
    
}