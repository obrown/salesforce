public with sharing class umDenialBenefitExclusion {
    public static string letterText(Utilization_Management__c um, Utilization_Management_Denial_Letter__c uml, Utilization_Management_Clinician__c clinician, Utilization_Management_Clinical_Code__c[] diagnosisCodes, Utilization_Management_Clinical_Code__c[] clinicalCodes, string logoPath){
        StaticResource sr = [SELECT Id, NamespacePrefix, SystemModstamp FROM StaticResource WHERE Name = 'medicalDirectorSignature' LIMIT 1];

        boolean hasAddress = (um.patient__r.Address__c != null && um.patient__r.City__c != null && um.patient__r.State__c != null && um.patient__r.Zip__c != null);

        string letterText = '';

        letterText += '<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        letterText += um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c + '<br/>';
        letterText += hasAddress ? um.patient__r.Address__c + '<br/>': '' + '<br/>';
        letterText += hasAddress ? um.patient__r.City__c + ', ': ', ';
        letterText += hasAddress ? um.patient__r.State__c + ' ' : ' ';
        letterText += hasAddress ? um.patient__r.Zip__c + '<br/><br/>': '' + '<br/><br/>';
        letterText += 'RE:&nbsp;&nbsp;Patient Name:&nbsp;' + um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c;
        letterText += '<br/>';
        letterText += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Date of Birth:&nbsp;' + um.patient__r.Patient_Date_of_Birth__c;
        letterText += '</div>';
        letterText += '<p>';
        letterText += 'Dear&nbsp;';
        if (um.patient__r.Gender__c == 'Female') {
            letterText += 'Ms. ';
        }
        else if (um.patient__r.Gender__c == 'Male') {
            letterText += 'Mr. ';
        }

        letterText += um.patient__r.Patient_First_Name__c + ' ' + um.patient__r.Patient_Last_Name__c + ',';

        letterText += '</p>';
        letterText += '<p>';
        letterText += 'Contigo Health is a third-party administrator (TPA) that performs Benefit Administration Services for the self-funded group health plan of ' + um.patient__r.Patient_Employer__r.name + ', which is regulated by ERISA under the US Department of Labor. ';
        letterText += 'This letter is in response to the request for the *** [service] *** at ' + um.facility_name__c + ' for ' + um.patient__r.patient_first_name__c + ' ' + um.patient__r.Patient_Last_Name__c + ' beginning on ' + um.admission_date__c.format() + '. *** [services denied] ***.';
        letterText += '</p>';

        letterText += '<br/>';
        letterText += 'Diagnoses: ';
        if (diagnosisCodes.isEmpty()) {
            letterText += 'N/A';
        }
        else{
            for (Utilization_Management_Clinical_Code__c dc :diagnosisCodes) {
                letterText += dc.name + ' ' + dc.Description__c + ' ;';
            }
            letterText = letterText.removeEnd(';');
        }
        letterText += '<br/><br/>';
        letterText += 'Treatment: ';

        if (clinicalCodes.isEmpty()) {
            letterText += 'N/A';
        }
        else{
            for (Utilization_Management_Clinical_Code__c dc :clinicalCodes) {
                letterText += dc.name + ' ' + dc.Description__c + ' ;';
            }
            letterText = letterText.removeEnd(';');
        }
        letterText += '<br/><br/>';

        letterText += 'As per ' + um.patient__r.Patient_Employer__r.name + ' Health Care Benefits Plan Document:&nbsp;';
        letterText += '<br/><br/>';

        letterText += 'Medical Benefit your Plan covers:&nbsp;';
        letterText += 'Please refer to the section titled *** [section title] ***';
        letterText += '<br/><br/>';
        letterText += '*** [SPD language quote] ***';
        letterText += '<br/><br/>';

        letterText += 'As per ' + um.patient__r.Patient_Employer__r.name + ' Health Care Benefits Plan Document:&nbsp;';
        letterText += '<br/>';
        letterText += 'Specific Plan Exclusions:&nbsp;';
        letterText += '<br/><br/>';
        letterText += '*** [exclusion language quote] ***';
        letterText += '<br/><br/>';

        letterText += '<div style="font-style:italic">';
        letterText += 'All choices regarding the care and treatment of the patient remain the responsibility of the member and the member’s provider. In no way does this letter attempt to dictate the care the patient receives.';
        letterText += '</div>';

        letterText += '<p>';
        letterText += 'SPANISH (Español): Para obtener asistencia en Español, llame al 330-656-1072<br/>';
        letterText += 'TAGALOG (Tagalog):  Kung kailangan ninyo ang tulong sa Tagalog tumawag sa 330-656-1072<br/>';
        letterText += 'CHINESE <span style="font-family: Arial Unicode MS">(中文):  如果需要中文的帮助，请拨打这个号码</span> 330-656-1072<br/>';
        letterText += 'NAVAJO (Dinek\'ehgo  shika  at\'ohwol  ninisingo, kwiijigo  holne\' 330-656-1072<br/>';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'The plan complies with applicable Federal civil rights laws and does not discriminate on the basis of race, color, national origin, sex, age or disability above the appeal right section of the denial letters.';
        letterText += '</p>';

        letterText += '<div style="page-break-after:always;"/>';
        letterText += '<p style="text-align:center;text-decoration:underline">';
        letterText += 'RIGHT TO APPEAL';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'The claimant has the right to initiate a formal appeal concerning any denied or partially denied service verbally or in writing. The claimant has the right to request, verbally or in writing and free of charge, any internal rule, guideline, protocol, scientific or clinical rationale, other similar criteria, or plan provision used to make the original determination. Submit requests to Contigo Health, ATTENTION: APPEALS COORDINATOR at 1755 Georgetown Rd, Hudson, OH 44236 or call Customer Service at the number on your Medical ID Card. The appeal should include the reason(s) for the ';
        letterText += 'request, any additional facts or documentation and/or a copy of the Explanation of Benefits that supports the appeal.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'First appeal level: Send to Contigo Health within 180 days of receipt of this letter or call Customer Service at the number on your Medical Card. If the claimant does not submit an appeal on time, he or she will lose the right to the appeal and the right to file suit in court.';
        letterText += '</p>';

        letterText += '<p>';
        letterText += 'Contigo Health<br/>';
        letterText += '1755 Georgetown Rd<br/>';
        letterText += 'Hudson, OH 44236<br/>';
        letterText += '</p>';

        boolean hasClinician = (Clinician.Street__c != null && Clinician.City__c != null && Clinician.State__c != null && Clinician.Zip_Code__c != null);

        letterText += '<div>';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += 'CC:';
        letterText += '</div>';
        letterText += '<div style="display:inline-block;">';
        letterText += Clinician.First_Name__c + ' ' + Clinician.Last_Name__c + ', ' + Clinician.Credentials__c;
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;"/>';
        letterText += hasClinician ? Clinician.Street__c : '';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;">';
        letterText += hasClinician ? Clinician.City__c + ', ' : ', ';
        letterText += hasClinician ? Clinician.State__c + ' ' : ' ';
        letterText += hasClinician ? Clinician.Zip_Code__c : '';
        letterText += '</div>';
        letterText += '</div><br/>';

        letterText += '<div style="margin-top:10px">';

        letterText += '<div style="display:inline-block;">';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        boolean hasFacility = (um.Facility_Name__c != null && um.Facility_Street__c != null && um.Facility_City__c != null && um.Facility_State__c != null && um.Facility_Zip_Code__c != null);

        letterText += hasFacility ? um.Facility_Name__c : '';

        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;">';
        letterText += hasFacility ? um.Facility_Street__c : '';
        letterText += '</div>';
        letterText += '</div>';
        letterText += '<div >';
        letterText += '<div style="display:inline-block;width:40px">';
        letterText += '&nbsp;';
        letterText += '</div>';

        letterText += '<div style="display:inline-block;">';
        letterText += hasFacility ? um.Facility_City__c + ', ' : '';
        letterText += hasFacility ? um.Facility_State__c + ' ' : '';
        letterText += hasFacility ? um.Facility_Zip_Code__c : '';
        letterText += '</div>';
        letterText += '</div>';
        letterText += '</div>';

        letterText += '</div>';
        return letterText;
    }
}