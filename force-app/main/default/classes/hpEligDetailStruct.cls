public with sharing class hpEligDetailStruct{

    
    public string ResultCode {get; set;}
    public map<string, EligibilityInformation> EligibilityInformation {get; set;}
    public EligibilityInformation eligibilityDetail {get; set;}
     
    public hpEligDetailStruct(){
    
    }
    
    public void cleanUp(){
        
        if(this.EligibilityInformation==null){return;}
        
        for(string s : this.EligibilityInformation.keySet()){
            if(this.EligibilityInformation.get(s) !=null){
                system.debug(EligibilityInformation);
                if(this.EligibilityInformation.get(s).Grp == 'HDP' || this.EligibilityInformation.get(s).Grp == '000'){
                    this.EligibilityInformation.put(s, null);
                    return;
                }
                if(this.EligibilityInformation.get(s).EligDate != null){
                    this.EligibilityInformation.get(s).EligDate = this.EligibilityInformation.get(s).EligDate.trim();
                    this.EligibilityInformation.get(s).EligDate = this.EligibilityInformation.get(s).EligDate.mid(4,2) +'/'+ this.EligibilityInformation.get(s).EligDate.right(2) +'/'+ this.EligibilityInformation.get(s).EligDate.left(4);
                }
            }
            this.eligibilityDetail =this.EligibilityInformation.get(s);
        }
        
    }
    
    public class EligibilityInformation{
        
        public hpPatientSearchStruct.PatSearchResultLogs patientDetail {get; set;}
        public string Grp  {get;set;}
        public string DepSsn {get;set;}
        public string EligDate {get;set;}
        public string Salary {get; set;}
        public string Dept {get; set;}
        public string MedicalFamilyCoverageCode {get;set;}
        public string Status {get; set;}
        public string MedicalBenefitPlan {get; set;}
        public string MedicalCOBFlag {get; set;}
        public string Ssn {get; set;}
        public string gender {get; set;}
    }
}