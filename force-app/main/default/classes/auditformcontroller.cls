public with sharing class auditformcontroller {

    public Claim_Audit__c         obj {get;set;}
    public Claim_Audit_Error__c[] cErrors {get;set;} 
    public string                 theDate {get;set;}
    public string                 theTo   {get; private set;}
    public boolean                isComplete {get; private set;}
    
    public auditformcontroller (ApexPages.StandardController controller) {
        obj = (Claim_Audit__c)controller.getRecord();
        obj.Audit_Form_Date__c = date.today();
        theDate = string.valueOf(obj.Audit_Form_Date__c.day())+'/'+string.valueOf(obj.Audit_Form_Date__c.month())+'/'+string.valueOf(obj.Audit_Form_Date__c.year());
        setTheCerrors(obj.id);
    }
    
    public auditformcontroller (){
    
       cErrors = new Claim_Audit_Error__c[]{};
       
       if(ApexPages.currentPage().getParameters().get('id') != null){
           
           obj = [select Audit_Form_Date__c, 
                         Audit_Status__c,
                         Comments__c,  
                         Claim__r.Group_Number__c,
                         Claim__r.Claim_Number__c,
                         Claim__r.Source_Type__c,
                         Claim_Auditor__r.Name,
                         Claim_Auditor__r.User__r.firstName,
                         Claim_Auditor__r.User__r.lastName,
                         Claim_Auditor__r.User__r.name,
                         Claim_Processor__r.name,
                         Claim_Processor__r.Email_Address__c,
                         Client__c,
                         wa__r.name,
                         Name from Claim_Audit__c where id = :ApexPages.currentPage().getParameters().get('id')];
                         
           obj.Audit_Form_Date__c = date.today();
           theDate = string.valueOf(obj.Audit_Form_Date__c.month())+'/'+string.valueOf(obj.Audit_Form_Date__c.day())+'/'+string.valueOf(obj.Audit_Form_Date__c.year());
           setTheCerrors(obj.id);
           
          
       }
        
    }
    
    void setTheCerrors(id theID){
        isComplete = false;
        if(ApexPages.currentPage().getParameters().get('isPlan') == 'false' || ApexPages.currentPage().getParameters().get('isPlan') == null){
            cErrors = [select Comments__c,Error_Source__c,Error_Type__c,Error_Reason__c from Claim_Audit_Error__c where claim_audit__c = :theID and resolved__c = false and Error_Source__c != 'Plan Build'];
        }else{
            cErrors = [select Comments__c,Error_Source__c,Error_Type__c,Error_Reason__c from Claim_Audit_Error__c where claim_audit__c = :theID and resolved__c = false and Error_Source__c = 'Plan Build'];
        }
    }
    
    public String getWordPrintViewXML(){
    return   '<!--[if gte mso 9]>' +
        '<xml>' +
        '<w:WordDocument>' +
        '<w:View>Print</w:View>' +
        '<w:Zoom>100</w:Zoom>' +
        '<w:DoNotOptimizeForBrowser/>' +
        '</w:WordDocument>' +
        '</xml>' +
        '<![endif]>';
        
    }

    public String getCheckBoxXML(){
    return '<!--[if supportFields]><span><span style="mso-element:field-begin"></span><span style="mso-bookmark:Check1"><span style="mso-spacerun:yes">&nbsp;</span>FORMCHECKBOX </span></span><![endif]--><span style="mso-bookmark:Check1"><span><![if !supportNestedAnchors]><a name=Check1></a><![endif]><!--[if gte mso 9]><xml> <w:data>FFFFFFFF650000001400060043006800650063006B003100000000000000000000000000000000000000000000000000</w:data></xml><![endif]--></span></span><!--[if supportFields]><span style="mso-bookmark:Check1"></span><span style="mso-element:field-end"></span><![endif]--><span style="mso-bookmark:Check1"></span>';        
    }

}