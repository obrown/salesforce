/**
* This class contains unit tests for validating the behavior of  the Apex claimAuditTrigger class
*/

@isTest()
private class claimAuditTriggerTest {

    static testMethod void claimAuditTrigger() {
            
        Claims_Audit_Client__c client = createClient();
        auditor(client);
        caClaim__c claim = createClaim(client);
        Claim_Audit__c audit = createAudit(claim);
        audit.Number_Of_lines__c = 3;
        audit.Audit_Status__c= 'Completed';
        system.debug('audit.Claim_Processor__c: '+audit.Claim_Processor__c);
        system.debug('audit.Audit_Type__c: '+audit.Audit_Type__c);
        update audit;
        system.assert(audit.id!=null);
        caClaims_Processed__c cp = new caClaims_Processed__c(Client__c=client.id ,Claims_Processed__c=100,Month_Paid__c=date.today());
        insert cp;
        system.assert(cp.id!=null);    
    }
    
    static Claims_Audit_Client__c createClient(){
        Claims_Audit_Client__c client = new Claims_Audit_Client__c();
        client.Name = 'Cooper';
        client.Underwriter__c = '008';
        client.group_number__c = 'SC1';
        client.Percent_to_Pull__c = 2;
        insert client;
        
        return client;
    }
    
    static caClaim__c createClaim(Claims_Audit_Client__c client){
        caClaim__c claim = new caClaim__c();
        claim.Name = '12345678900';
        claim.Allowed_Amount__c = 1000.00;
        claim.Total_Charge__c = 1000.00;
        claim.Amount_to_be_paid__c = 500.00;
        claim.Claim_Processed_Date__c = date.today().addDays(-30);
        claim.Claim_Received_Date__c = date.today().addDays(-25);
        claim.FDOS__c = date.today().addDays(-30);
        claim.TDOS__c = date.today().addDays(-25);
        claim.Employee_First_Name__c = 'John';
        claim.Employee_Last_Name__c = 'Test';
        claim.Patient_First_Name__c = 'John';
        claim.Patient_Last_Name__c = 'Test';
        claim.Relationship_to_Member__c = '00';
        claim.Diagnosis__c ='Test';
        claim.Diagnosis_Description__c ='Test';
        claim.Product__c = 'IV';
        claim.Source_Type__c = 'Test';
        claim.Patient_Age__c = '30';
        claim.client__c = client.id;
        claim.Claims_Processor_Crosswalk__c = createProcessor().id;
        insert claim;
        
        return claim;
    }
    
    static Claim_Audit__c createAudit(caClaim__c claim){
        Claim_Audit__c audit = new Claim_Audit__c();
        audit.claim__c = claim.id;
        audit.Audit_Type__c = 'Random Selection';
        audit.Audit_Status__c = 'To Be Audited';
        audit.comments__c = 'Test';
        audit.Claim_Auditor__c = [select id from Claims_Auditor__c limit 1].id;
        audit.Claim_Processor__c = claim.Claims_Processor_Crosswalk__c;
        system.debug('audit.Claim_Processor__c: '+audit.Claim_Processor__c);
        insert audit;
        
        return audit;
    }
    
    static Claims_Processor_Crosswalk__c createProcessor(){
    
        Claims_Processor_Crosswalk__c processor = new Claims_Processor_Crosswalk__c();
        processor.isActive__c = true;
        processor.Email_Address__c = 'email@test.com'; 
        processor.Processor_Alias_Name__c ='UnitTest'; 
        insert processor;
        
        return processor;
    }
    
    static Claims_Auditor__c auditor(Claims_Audit_Client__c client){
        
        Claims_Auditor__c auditor = new Claims_Auditor__c();
        auditor.Audit_Type__c = 'High Dollar';
        auditor.claims_client__c = client.id;
        insert auditor;
        
        return auditor;
    }
}