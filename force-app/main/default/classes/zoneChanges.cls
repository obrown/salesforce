/*
Author: Michael Martin
Date: Oct. 25, 2013
Refactored: Refactored May 14, 2013
Purpose: Gets all stage record add/changes and pushes records to the zoneChangesforHP table so a batch file can send a .csv to HealthPAC
*/

public with sharing class zoneChanges {

    string histbody = 'Employee SSN, Patient Last Name, Patient First Name, Zone Affected,Previous Effective Date,New Effective Date,Previous Termination Date, New Termination Date\n'; // body of email 
    map<id,string> stageeffMap = new map<id,string>();
    map<id,string> stagetermMap = new map<id,string>();

    
    //List to upload
    list<zoneChangesforHP__c> zippyList = new list<zoneChangesforHP__c>();
    
    public void setchanges(){

        //need a map to compare the transplant__c values to the hp_eligibility__c values
        map<id,Transplant__c> tmap = new map<id,Transplant__c>();

        //Maps for records to be sent to healthpac
        map<id,Stage__c> stageMap = new map<id,Stage__c>();
        map<id,Stage__c> stagepreMap = new map<id,Stage__c>();
        map<id,Stage__c> stageglobalMap = new map<id,Stage__c>();
        map<id,Stage__c> stagepostMap = new map<id,Stage__c>();  


        //Maps for changes sent to claims
        
        map<id,Stage__c> deletedMap = new map<id,Stage__c>();
        map<string,string> deletedexpmap = new map<string,string>();
        map<string,list<Claimsreporting__c>> fdos = new map<string,list<Claimsreporting__c>>(); //Map of lists 
        map<id, date> stageEffOld = new map<id, date>(); //Previous stage eff values
        map<id, date> stageTermOld = new map<id, date>(); //Previous stage term values  
        map<id,Stage__c> stagepreMapOld = new map<id,Stage__c>(); //Maps previous pre stage values from stageEffOld and stageTermOld map  
        map<id,Stage__c> stageglobalMapOld = new map<id,Stage__c>(); //Maps previous trp stage term values from stageEffOld and stageTermOld map 
        map<id,Stage__c> stagepostMapOld = new map<id,Stage__c>(); //Maps previous pst stage term values from stageEffOld and stageTermOld map    

        list<ClaimsReporting__c> pclaimexp = new list<ClaimsReporting__c>();
        set<id> stageIDs = new set<id>();

        set<id> tids = new set<id>(); //transplant ids for the exception process
        list<Stage__c> stagetobeupdated = new list<Stage__c>();

        if(Test.isRunningTest()){
            
            for(Stage__History s: [select field, oldvalue, newvalue, parentid from Stage__History where (field = 'created' or field = 'stageeff__c' or field = 'stageterm__c')]){
                stageIDs.add(s.parentid);  
            }

        }else{

            for(Stage__History s: [select field, oldvalue, newvalue, parentid from Stage__History where createdDate > :date.today().addDays(-1) and createdDate < :date.today() and (field = 'created' or field = 'stageeff__c' or field = 'stageterm__c')]){
                
                stageIDs.add(s.parentid);
                
                if(s.field != 'created'){
                    if(s.field == 'stageeff__c'){
                        stageeffMap.put(s.parentid,s.oldvalue+','+s.newvalue);
                        stageEffOld.put(s.parentid,date.valueof(s.oldvalue));       
                    }
                    
                    if(s.field == 'stageterm__c'){
                        stagetermMap.put(s.parentid,s.oldvalue+','+s.newvalue);
                        stageTermOld.put(s.parentid,date.valueof(s.oldvalue));      
                    }
                }
                
            }  

        }

        for(Stage__c s: [select id from Stage__c where isDeleted = true and lastmodifieddate >:date.today().addDays(-1) All Rows]){
            
            stageIDs.add(s.id);
            
        }
        
        for(Stage__c s: [select stage__c,transplant__c, isDeleted, StageEff__c,StageTerm__c,outlier__c,createdDate from Stage__c where (id in :stageIDs or outlier__c = true) and stage__c != null All Rows]){
            tids.add(s.transplant__c); //Used to limit number of transplant records
            if(s.outlier__c == true && !s.isDeleted){s.outlier__c = false;stagetobeupdated.add(s);}   
            
            if(s.isdeleted == true && !(s.createdDate > date.today().addDays(-1))){
                deletedMap.put(s.transplant__c,s);
                deletedexpmap.put(s.transplant__c,s.stage__c+','+s.StageEff__c + ',NA,'+ s.StageTerm__c+',NA,Deleted Record\n');
            }else{
                
                if(s.stage__c=='Pre'){
                    stagePreMap.put(s.transplant__c,s);     
                    stagePreMapOld.put(s.transplant__c, populateOldMap(s, 'Pre',stageEffOld, stageTermOld));
                    
                }else if(s.stage__c=='Global'){
                    stageGlobalMap.put(s.transplant__c,s);
                    stageGlobalMapOld.put(s.transplant__c, populateOldMap(s, 'Global',stageEffOld, stageTermOld));
                    
                }else if(s.stage__c=='Post'){
                    stagePostMap.put(s.transplant__c,s);
                    stagePostMapOld.put(s.transplant__c, populateOldMap(s, 'Post',stageEffOld, stageTermOld));
                    
                }
            }

        }

        for(Claimsreporting__c c :[select Pay_Status__c,Affected__c,transplantID__c,fdos__c,tdos__c,Employee_First_Name__c,Employee_Last_Name__c,Patient_First_Name__c,Patient_Last_Name__c,certid__c,claim_number__c from Claimsreporting__c where transplantID__c IN :tids ]){
            list<Claimsreporting__c> crlist = new list<Claimsreporting__c>();
            
            if(fdos.containsKey(c.transplantID__c)){
                crlist = fdos.get(c.transplantID__c).clone();
                crlist.add(c);
                fdos.put(c.transplantID__c,crlist);
            }else{
                crlist.add(c);
                fdos.put(c.transplantID__c, crlist);
            }  

        }  

        //Delete existing records, though we should never have records that persist as the nightly process deletes them as soon as the are exported
        list<zoneChangesforHP__c> delzippyList = new list<zoneChangesforHP__c>([select id from zoneChangesforHP__c]);
        delete delzippyList;
        
        for(Transplant__c t : [select employee_ssn__c, patient_ssn__c, certid__c,bid__c,isDeleted from Transplant__c where id IN :tids]){
            if(t.isDeleted==false){
                tmap.put(t.id,t);
            }
        }
        
        string body = 'Exception Report'; // body of email
        string csvbody = 'Patient Last Name, Patient First Name, Employee SSN (SF),Employee SSN (HP),Patient SSN (SF),Patient SSN (HP),CertID (SF),CertID (HP)\n'; // body of email
        boolean exp = false; // is there an actual exception?
        set<id> hpids = new set<id>(); //ids of HP_elibility_table__c, used to compare the list of patient in Hp and SF

        //Main loop....set's the claims reporting list, looks for differences between hp table and tranplant records
        for(hp_eligibility__c h : [select certID__c, SEQ__c,transplantID__c, PUNBR__c, GRNBR__c, Employee_SSN__c, patient_ssn__c, Patient_Last_Name__c, Patient_first_Name__c  from hp_eligibility__c where transplantID__c IN :tids]){

            hpids.add(h.transplantID__c);
            //Affected claims ,maybe 
            if(fdos.containsKey(h.transplantID__c)){
                
                for(claimsReporting__c cr: fdos.get(h.transplantID__c)){
                    
                    if(stagepreMap.containsKey(h.transplantID__c)){
                        cr.affected__c = claimAffected(stagepreMapOld, stagepreMap, cr.tdos__c, cr.fdos__c, h.transplantID__c);
                        
                    }else if(stageglobalMap.containsKey(h.transplantID__c)){
                        cr.affected__c = claimAffected(stageglobalMapOld, stageglobalMap, cr.tdos__c, cr.fdos__c, h.transplantID__c);
                        
                    }else if(stagepostMap.containsKey(h.transplantID__c)){
                        cr.affected__c = claimAffected(stagepostMapOld, stagepostMap, cr.tdos__c, cr.fdos__c, h.transplantID__c);   
                        
                    }
                    pclaimexp.add(cr);
                }
                
            }
            
            //Looking for differences between the transplant record and the hgp eligibility record...SSN's and cert id's
            if(tmap.containsKey(h.transplantID__c)){      
                csvbody+=findExceptions(tmap, h);
                if(csvbody.countMatches('\n')>1){exp=true;}
            }
            
            theZoneChanges(stagepreMap, 'PRT', h, tmap);
            theZoneChanges(stageglobalMap, 'TRP', h, tmap);
            theZoneChanges(stagepostMap, 'PST', h, tmap);
            
            if(deletedexpmap.containsKey(h.transplantID__c)){
                histbody += h.employee_ssn__c + ',' + h.Patient_Last_Name__c + ','  + h.Patient_First_Name__c + ',' + deletedexpmap.get(h.transplantID__c);  
            }
            
        }
        
        set<id> outliersID = new set<id>();
        list<Stage__c> outlierStages = new list<Stage__c>();
        
        if(hpids != tids){
            outliersID = tids.clone();
            outliersID.removeall(hpids);
            outlierStages = setOutliers(outliersID, stagepreMap,stageglobalMap,stagePostMap);
        }
        
        update stagetobeupdated; //TODO: updated only the stage that need to be updated to outlier__c false. This updates all stages to false. 
        //The next line then updates only the stages needed to true
        update outlierStages;    
        populateClaimsReport(pclaimexp);
        
        if(exp){
            //Exception email
            mailthecsv(csvbody,'mmartin@hdplus.com','Salesforce Exception Report for Zone Changes','Please find the zones exception report attached');  
        }
        
        if(tids.size()>0){
            
            histbody = histbodyFomat(histbody);
            
            if(histbody.countMatches('\n')>1){
                mailthecsv(histbody,'awashington@hdplus.com,SChiara2@hdplus.com,tfisher@hdplus.com','Zone Changes','Please find the zone changes attached');
                //mailthecsv(histbody,'mmartin@hdplus.com','Zone Changes','Please find the zone changes attached');
            }else{
                mailthecsv('1','awashington@hdplus.com,SChiara2@hdplus.com,tfisher@hdplus.com','Zone Changes','There are no zone changes');
                
            }

        }
        
        insert zippyList;
        string zonechanges = 'PUNBR__C,QUALPUNBR__C,GRNBR__C,QUALGRNBR__C,EMPLOYEE_SSN__C,QUALESSN__C,SEQ__C,QUALSEQ__C,ZONE__C,QUALZONE__C,EFFDATE__C,QUALEFFDATE__C,TERMDATE__C,QUALTERMDATE__C,PATIENT_LAST_NAME__C,QUALPATIENTLASTNAME__C,PATIENT_FIRST_NAME__C,QUALPATIENTFIRSTNAME__C,CERTID__C,QUALCERTID__C,STAGEID__C,SFID__C,HPISDELETED__C\n';
        for(zoneChangesforHP__c z : zippylist){
            zonechanges += z.PUNBR__C + ',PUNBR.' + z.PUNBR__C + ',' + z.GRNBR__C + ',GRNBR.' + z.GRNBR__C + ',' + z.EMPLOYEE_SSN__C + ',ESSN.' + z.EMPLOYEE_SSN__C + ',' + z.SEQ__C + ',SEQ.' + z.SEQ__C + ',' + z.ZONE__C + ',ZONE.' + z.ZONE__C + ',' + z.EFFDATE__C + ',EFFDATE.' + z.EFFDATE__C + ',' + z.TERMDATE__C + ',TERMDATE.' + z.TERMDATE__C + ',' + z.PATIENT_LAST_NAME__C + ',PATIENT_LAST_NAME.' + z.PATIENT_LAST_NAME__C + ',' + z.PATIENT_FIRST_NAME__C + ',PATIENT_FIRST_NAME.' + z.PATIENT_FIRST_NAME__C + ',' + z.CERTID__C + ',CERTID.' + z.CERTID__C + ',' + z.STAGEID__C + ',SDIF.' + z.STAGEID__C + ',' + hpisdeleted(z.ISDELETED__C) + '\n'; 
        }
        
        if(zonechanges.countMatches('\n')>1){
            mailthecsv(zonechanges, 'cgreen@hdplus.com', 'Zone Changes to HP', 'Please find the zone changes for HealthPac attached');
            //mailthecsv(zonechanges, 'mmartin@hdplus.com', 'Zone Changes to HP', 'Please find the zone changes for HealthPac attached');
        }else{
            mailthecsv('1','cgreen@hdplus.com','Zone Changes to HP','There are no zone changes');
        }
    }  
    
    
    
    /*      ------------------ These are the functions ------------------     */
    
    private string findExceptions(map<id,transplant__c> thisTmap, hp_eligibility__c thisH){
        string csvbody='';
        
        if(thisTmap.get(thisH.transplantID__c).employee_ssn__c != thisH.Employee_SSN__c || thisTmap.get(thisH.transplantID__c).certID__c != thisH.certID__c || thisTmap.get(thisH.transplantID__c).patient_ssn__c != thisH.patient_ssn__c){
            
            csvbody += thisH.Patient_Last_Name__c +','+ thisH.Patient_First_Name__c+','+
            thisTmap.get(thisH.transplantID__c).employee_ssn__c +','+ thisH.Employee_SSN__c +','+
            thisTmap.get(thisH.transplantID__c).patient_ssn__c +','+thisH.patient_ssn__c+','+
            thisTmap.get(thisH.transplantID__c).certid__c + ',' + thisH.certid__c +'\n';
        }  
        
        return csvbody; 
        
    }
    
    private void theZoneChanges(map<id, stage__c> stageMap, string theZone, hp_eligibility__c thisH, map<id,transplant__c> tMap ){
        
        if(stageMap.containsKey(thisH.transplantID__c)){
            
            zoneChangesforHP__c tempzippy = setzippy(thisH, stageMap);
            
            tempzippy.zone__c =theZone;
            zippyList.add(tempzippy);
            
            histbodychanges(stageMap, thisH.transplantID__c, tempzippy, thisH, tMap);
            
        }
        
    }
    
    private void populateClaimsReport(ClaimsReporting__c[] thispcClaimexp){
        
        if(thispcClaimexp.size()>0){
            String csvclaimsbody = 'Claim Number, CertID, Associate Last Name, Associate First Name, Patient Last Name, Patient First Name,FDOS,TDOS,Pay Status,Affected\n'; // body of emailail
            
            for(claimsReporting__c cr : thispcClaimexp){
                csvclaimsbody += cr.Claim_Number__c + ',' + cr.certID__c + ',' + cr.Employee_Last_Name__c + ',' + cr.Employee_First_Name__c + ',' + cr.Patient_Last_Name__c + ',' + cr.Patient_First_Name__c + ',' + cr.FDOS__c + ',' + cr.TDOS__c +  ',' +  cr.pay_status__c + ','+ cr.Affected__c + '\n';
            }
            
            csvclaimsbody = csvclaimsbody.replace(' 00:00:00','');
            csvclaimsbody = csvclaimsbody.replaceAll('null','');
            if(csvclaimsbody.countMatches('\n')>1){
                //mailthecsv(csvclaimsbody,'awashington@hdplus.com,SChiara2@hdplus.com,mmartin@hdplus.com','Salesforce Exception Report of claims affected','Please find the claims affected by zone changes');
                mailthecsv(csvclaimsbody,'mmartin@hdplus.com','Salesforce Exception Report of claims affected','Please find the claims affected by zone changes');
            }else{
                mailthecsv('1','mmartin@hdplus.com','Salesforce Exception Report of claims affected','There are no affected claims.');
            }
        }
        
    }
    
    
    private string hpisdeleted(boolean foo){
        
        if(foo){
            return 'D';
        }else{
            return '';
        }
        
    }
    
    private String addzero(String x){
        
        if(decimal.valueof(x)<10){
            x = '0' + x;    
            
        }
        
        return x;

    }
    
    private void histbodychanges(map<id,Stage__c> stageMap, string tid, zoneChangesforHP__c tempzippy, hp_eligibility__c h, map<id, transplant__c> tMap){
        
        if(stageeffMap.containskey(stageMap.get(tid).id) || stagetermMap.containskey(stageMap.get(tid).id)){
            histbody += tmap.get(tid).employee_ssn__c + ',' + h.Patient_Last_Name__c + ','  + h.Patient_First_Name__c + ',' + stageMap.get(tid).stage__c + ',' 
            + checknull(stageeffMap.get(stageMap.get(tid).id)) + ',' + checknull(stagetermMap.get(stageMap.get(tid).id))+'\n'; 
        }else{
            histbody += tempZippy.EMPLOYEE_SSN__C + ',' + tempZippy.Patient_Last_Name__c + ',' + tempZippy.Patient_First_Name__c + ',' + tempzippy.zone__c + ',NA,' + tempZippy.effdate__c + ',NA,' + tempZippy.termDate__c + '\n';
        }
    }
    
    private zoneChangesforHP__c setzippy(hp_eligibility__c h, map<id,Stage__c> stageMap){
        Date effDate;
        Date termDate;
        
        zoneChangesforHP__c tempzippy = new zoneChangesforHP__c();
        tempzippy.SEQ__c = string.valueof(h.seq__c);
        tempzippy.certID__c = h.certID__c;
        tempzippy.PUNBR__c = h.PUNBR__c;
        tempzippy.GRNBR__c = h.GRNBR__c;
        tempzippy.Employee_SSN__c = h.Employee_SSN__c;
        tempzippy.Patient_Last_Name__c = h.Patient_Last_Name__c;
        tempzippy.Patient_first_Name__c = h.Patient_first_Name__c;
        
        if(stageMap.get(h.transplantID__c).StageEff__c == null){
            
            tempzippy.effdate__c = '20501225'; // Date HP uses for null value 
            
        }else{
            effDate = stageMap.get(h.transplantID__c).StageEff__c;
            tempzippy.effdate__c = string.valueof(effDate.year()) + addzero(string.valueof(effDate.Month())) + addzero(string.valueof(effDate.day()));
            
        }
        
        
        if(stageMap.get(h.transplantID__c).StageTerm__c == null){
            tempzippy.termdate__c = '20501225';   // Date HP uses for null value    
        }else{
            termDate = stageMap.get(h.transplantid__c).StageTerm__c;
            tempzippy.termdate__c = string.valueof(termDate.year()) + addzero(string.valueof(termDate.Month())) + addzero(string.valueof(termDate.day()));
        }
        
        tempzippy.stageid__c = stageMap.get(h.transplantID__c).id;   
        
        return tempzippy;      
        
    }
    
    private string checkcontains(map<string,Stage__c> themap, string theid, string stage, boolean iseff){
        
        String returnstring = '';
        Date tempdate;
        try{

            if(iseff){
                tempdate = themap.get(theid).StageEff__c;
                returnstring = tempdate.month() + '/' + tempdate.day() + '/' + tempdate.year(); 
            }else{
                tempdate = themap.get(theid).StageTerm__c;
                returnstring = tempdate.month() + '/' + tempdate.day() + '/' + tempdate.year(); 
            }
            
        }catch(exception e){
            returnstring ='nullrr';
        }
        
        return returnstring;
        
    }
    
    private string checknull(string x){
        
        
        if(x==null){
            x=',nullxx';
        }
        
        return x;
    }
    
    private boolean mailthecsv(string s, string theto, string subject, string body){
        boolean foo=true;
        try{
            Messaging.SingleEmailMessage mail = utilities.email(theto,null,'mmartin@hdplus.com','System',Subject,Body);
            //Messaging.SingleEmailMessage mail = utilities.email(theto,null,'mmartin@hdplus.com','System',Subject,Body);
            
            if(s!='1'){
                Blob csvBlob = Blob.valueOf(s);
                Messaging.EmailFileAttachment efa = new Messaging.EmailFileAttachment();
                efa.setFileName(subject+'.csv');
                efa.setBody(csvBlob);
                mail.setFileAttachments(new Messaging.EmailFileAttachment[] {efa});
            }
            
            mail.setReplyTo('noreply@hdplus.com');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            
        }catch (exception e){
            system.debug(e.getMessage());
            foo = false;    
        }
        
        return foo;
        
    }
    
    private string claimAffected(map<id, stage__c>oldMap, map<id, stage__c>newMap,date tdos, date fdos,id transID ){
        string foo='';
        
        if(oldMap.get(transID).stageeff__c <= fdos && oldMap.get(transID).stageterm__c >= tdos){ //Claim was inside old pre stage eff and term date's
            
            if(newMap.get(transID).stageeff__c > fdos || newMap.get(transID).stageterm__c < tdos){ //Claim is outside new pre stage value's
                foo = 'Y';
            }
            
        }
        
        return foo;
    }
    
    private stage__c populateOldMap(stage__c theStage, string stage, map<id,Date> oldEffMap, map<id,Date> oldTermMap){
        
        stage__c foo = new stage__c(stage__c=stage, transplant__c=theStage.transplant__c);  
        if(oldEffMap.get(theStage.id)==null){foo.stageeff__c = theStage.stageeff__c;}else{foo.stageeff__c=oldEffMap.get(theStage.id);} //If the dates didnt change, assign the current value
        if(oldTermMap.get(theStage.id)==null){foo.stageterm__c =theStage.stageterm__c;}else{foo.stageterm__c=oldTermMap.get(theStage.id);}
        
        return foo;
    }
    
    /*                          Testing                             */
    
    
    private Stage__c[] setOutliers(set<id> outliersID, map<id,stage__c> stagepreMap, map<id,stage__c> stageglobalMap, map<id,stage__c> stagepostMap){
        
        Stage__c[] outlierStages = new Stage__c[]{};
        Stage__c stage = new Stage__c();
        Set<id> preventDUPs = new set<id>();
        
        for(ID s: outliersID){
            if(stagepreMap.containsKey(s)){
                stage = stagepreMap.get(s);
            }  
            
            if(stageglobalMap.containsKey(s)){
                stage = stageglobalMap.get(s);
            }
            
            if(stagepostMap.containsKey(s)){
                stage = stagepostMap.get(s);
            }
            
            if(stage.id!=null){
                stage.outlier__c = true;
                if(!stage.isDeleted && !preventDUPs.contains(stage.id)){
                    preventDUPs.add(stage.id);
                    outlierStages.add(stage);
                }
            }
        }
        return outlierStages;
        
    }
    
    private string histbodyFomat(string histBody){
        histbody = histbody.replace('null-null','');
        histbody = histbody.replace('-null','-NA');
        histbody = histbody.replace('null-','NA-');
        histbody = histbody.replace('null','');
        histbody = histbody.replace(' 00:00:00','');
        histbody = histbody.replace(',,xx,,',',NA,NA,NA,');
        histbody = histbody.replace(',xx','No Change,No Change');
        histbody = histbody.replace(',Pre,',',PRT,');
        histbody = histbody.replace(',Global,',',TRP,');
        histbody = histbody.replace(',Post,',',PST,');
        histbody = histbody.replace(',12/25/2050,',',NA,');
        histbody = histbody.replace(',,',',NA,');       
        
        return histbody;
    }
    
    static testMethod void myUnitTest() {
        
        Transplant__c newT = new Transplant__c();
        newT.Relationship_to_the_Insured__c = 'Patient is Insured';
        newT.Employee_First_Name__c = 'John';
        newT.Employee_Last_Name__c = 'Tester';
        newT.BID__c = '456321';
        newT.Carrier_Name__c = 'Blue Cross Arkansas';     
        newT.Transplant_Type__c = 'Heart';
        newT.Employee_DOB__c = date.today().addyears(-30);
        newT.Employee_Gender__c = 'Male';
        newT.Employee_SSN__c = '123456789';
        newT.Same_as_Employee_Address__c = true;
        newT.Employee_Street__c = '123 E Main St';
        newT.Employee_City__c = 'Mesa';
        newT.Employee_State__c = 'AZ';
        newT.Employee_Zip_Postal_Code__c = '85206';
        newT.EmployeeHomePhoneDisp__c = '(480) 555-1212';
        
        insert newT;
        
        Stage__c preS = new Stage__c();
        preS.Transplant__c = newT.id;
        preS.Stage__c = 'Pre';
        preS.Type__c = 'Heart';
        preS.StageEff__c = date.today().addDays(-30);
        preS.StageTerm__c = date.today().addDays(-25);
        
        insert preS;
        
        Stage__c globalS = new Stage__c();
        globalS.Transplant__c = newT.id;
        globalS.Stage__c = 'Global';
        globalS.StageEff__c = date.today().addDays(-22);
        globalS.StageTerm__c = date.today().addDays(-18);
        insert globalS;                        

        Stage__c postS = new Stage__c();
        postS.Transplant__c = newT.id;
        postS.Stage__c = 'Post';
        postS.StageEff__c = date.today().addDays(-22);
        postS.StageTerm__c = date.today().addDays(-18);
        insert postS; 
        
        zoneChanges zc = new zoneChanges();
        zc.setchanges();
        
        delete globalS;
        preS.Stage__c = 'Global';
        update preS;
        zc.setchanges();
        
        
        
        zoneChangesforHP__c z = new zoneChangesforHP__c();
        z.name='test';
        upsert z;
        update z;       
        
    }

}