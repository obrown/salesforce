public with sharing class hpEdiElgStruct{
    
    public string ResultCode {get; set;}
    public string ErrorMsg {get; set;}
    public hpEdiElg[] Results {get;set;}
    
    public void cleanUp(){
        
        if(Results==null||Results.isEmpty()){
            return;
        }
        
        for(hpEdiElg hpe : Results){
            
            if(hpe.Dob != null && hpe.Dob.length()==8){
                hpe.Dob = hpe.Dob.mid(4,2) +'/'+hpe.Dob.right(2)+'/'+hpe.Dob.left(4);
            }
            
            if(hpe.EffectiveDate!= null && hpe.EffectiveDate.length()==8){
                hpe.EffectiveDate= hpe.EffectiveDate.mid(4,2) +'/'+hpe.EffectiveDate.right(2)+'/'+hpe.EffectiveDate.left(4);
            }
            
            if(hpe.PaidThroughDate!= null && hpe.PaidThroughDate.length()==8){
                hpe.PaidThroughDate= hpe.PaidThroughDate.mid(4,2) +'/'+hpe.PaidThroughDate.right(2)+'/'+hpe.PaidThroughDate.left(4);
            }
            
            if(hpe.TermDate!= null && hpe.TermDate.length()==8){
                hpe.TermDate= hpe.TermDate.mid(4,2) +'/'+hpe.TermDate.right(2)+'/'+hpe.TermDate.left(4);
            }
            
            if(hpe.CobraEffDate!= null && hpe.CobraEffDate.length()==8){
                hpe.CobraEffDate= hpe.CobraEffDate.mid(4,2) +'/'+hpe.CobraEffDate.right(2)+'/'+hpe.CobraEffDate.left(4);
            }
            
            if(hpe.CobraEndDate != null && hpe.CobraEndDate .length()==8){
                hpe.CobraEndDate = hpe.CobraEndDate.mid(4,2) +'/'+hpe.CobraEndDate.right(2)+'/'+hpe.CobraEndDate.left(4);
            }
            
            if(hpe.LastUpdateDate!= null && hpe.LastUpdateDate.length()==20){
                hpe.LastUpdateDate= hpe.LastUpdateDate.left(10);
                hpe.LastUpdateDate =hpe.LastUpdateDate.replaceAll('-','');
                hpe.LastUpdateDate= hpe.LastUpdateDate.mid(4,2) +'/'+hpe.LastUpdateDate.right(2)+'/'+hpe.LastUpdateDate.left(4);
                
            }
            
        }
        
    }
    
    public class hpEdiElg{
    public string PatientType {get; set;}
    public string Gender {get; set;}
    public string Ssn {get; set;}
    public string Essn {get; set;}
    public string Dob {get; set;}
    public string Firstname {get; set;}
    public string Lastname {get; set;}
    public string Address1 {get; set;}
    public string Address2 {get; set;}
    public string City {get; set;}
    public string State {get; set;}
    public string Zip {get; set;}
    public string Phone {get; set;}
    public string EffectiveDate {get; set;}
    public string PaidThroughDate {get; set;}
    public string TermDate {get; set;}
    public string CoverageType {get; set;}
    public string EmploymentStatus {get; set;}
    public string HireDate {get; set;}
    public string Bid {get; set;}
    public string PatientId {get; set;}
    public string FamilyStatus {get; set;}
    public string Relationship {get; set;}
    public string PlanCode {get; set;}
    public string Plncode {get; set;}
    public string CoverageLevel {get; set;}
    public string LastUpdateDate {get; set;} 
    public datetime LastUpdateDateField {get; set;} 
    public string CobraStatus {get; set;}
    public string CobraEffDate {get; set;}
    public string CobraEndDate {get; set;}
    public string CobraQualEvent {get; set;}
    public string YearWaitInd {get; set;}
    }
    
}