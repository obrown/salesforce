public with sharing class coeExceptionNetwork{

    public Walmart_COE_Network_Exception__c network_exception {get; set;}
    patient_case__c pc;
    string caseType;
    public coeExceptionNetwork(patient_case__c pc, string caseType){
        this.caseType = caseType;
        this.pc = pc;
    }
    
    public void loadExceptions(){
        if(pc==null){return;}
        
        try{
        network_exception = [select COE_Program_Education_Completed__c,
                                    CreatedDate,
                                    Emergent_Network_Exception_Reason__c,
                                    Emergent_Network_Exception_Reason_Other__c,
                                    Expedited_Network_Exception_Request__c,
                                    Emergent_Network_Exception_Requested__c,
                                    HDP_Clinical_Determination__c,
                                    HDP_Clinical_Determination_Date__c,
                                    HDP_Clinical_Review_Status__c,
                                    HDP_Clinical_Determination_Letter_Sent__c,
                                    HDP_Clinical_Determination_Provider__c,
                                    IRO_Determination__c,
                                    IRO_Determination_Date__c,
                                    IRO_Determination_Letter_Sent_to_Patient__c,
                                    IRO_Determination_Notification_Provided__c,
                                    IRO_Review_Status__c,
                                    Medical_Records_Received__c,
                                    Medical_Records_Requested__c,
                                    Network_Exception_Reason__c,
                                    Network_Exception_Reason_Other__c,
                                    Network_Exception_Reason_Clinical__c,
                                    Network_Exception_Reason_NonClinical__c,
                                    Network_Exception_Request_Date_Initiated__c,
                                    Network_Exception_Requested_By__c,
                                    Network_Exception_Type__c,
                                    RecordType.Name,
                                    Sent_for_IRO_Review__c,
                                    Sent_for_HDP_Clinical_Review__c,
                                    Status__c ,
                                    Status_Reason__c,
                                    Substatus__c,
                                    Were_Medical_Records_Received__c,
                                    Written_Request_for_Network_Exception_Re__c from Walmart_COE_Network_Exception__c where patient_case__c = :pc.id and RecordType.Name = :pc.ecen_procedure__r.name order by createdDate desc limit 1] ;                                     
        }catch(queryException e){
            //catch quietly in the case no records are found
        }
        
        if(network_exception==null){
            try{
                id recordtypeid = [select id from recordtype where sobjecttype = 'Walmart_COE_Network_Exception__c' and name = :caseType].id;
                network_exception = new Walmart_COE_Network_Exception__c(patient_case__c=pc.id, recordtypeid=recordtypeid, status__c='Open');
            
            }catch(queryException e){
                network_exception = new Walmart_COE_Network_Exception__c();
                return;
            
            }
        }
    }
    
    public string saveException(){
        try{
                upsert network_exception;
                
        }catch(dmlException e){
                return e.getDMLMessage(0);
        }
        
        return null;
    }
}