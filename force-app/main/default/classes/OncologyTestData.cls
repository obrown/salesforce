public without sharing class OncologyTestData{
    
    public Client__c client;
    public Client_Facility__c clientFacility;
    public Facility__c facility;
    public Oncology_Physician__c Physician;
    public Procedure__c ecenProcedure;
    public Oncology__c oncology;
    sObject[] sObjects = new sObject[]{};
    
    
    public OncologyTestData(){
        
    }
    
    public void loadData(string clientName, string facilityName){
        client = new Client__c(name=clientName);
        sObjects.add(client);
        
        ecenProcedure = new Procedure__c(name='Oncology');
        sObjects.add(ecenProcedure);
        
        facility = new Facility__c(name=facilityName);
        facility.Facility_Address__c = '2500 S Power Rd';
        facility.Facility_City__c= 'Mesa';
        facility.Facility_State__c= 'AZ';
        facility.Facility_Zip__c= '85205';
        sObjects.add(facility);
        
        insert sObjects;
        
        oncology = new oncology__c(Procedure__c=ecenProcedure.id, client__c=client.id);
        
        oncology.Employee_First_Name__c ='John';
        oncology.Employee_Last_Name__c ='Smith';
        oncology.client__c = client.id;
        
        system.debug('oncology.client__c: ' +oncology.client__c);
    }
    
    public Oncology__c setFormulafield(Oncology__c sObj, String fieldName, Object value){
        String jsonString = JSON.serialize(sObj);
        Map<String,Object> dataMap = (Map<String,Object>)JSON.deserializeUntyped(jsonString);
        dataMap.put(fieldName, value);
        jsonString = JSON.serialize(dataMap);
        return (Oncology__c)JSON.deserialize(jsonString, Oncology__c.class);
    }
    
    public Oncology_Physician__c  addPcp(oncology__c oncology){
        
        Oncology_Physician__c  Physician=new Oncology_Physician__c (name='Unit.test2');
        
        Physician.Associated_Facilities__c='na';
        Physician.City__c='Mesa';
        Physician.Credentials__c='MD';
        Physician.Facility_PCP__c=false;
        Physician.Facility_Reported_Physician__c=false;
        Physician.Fax_Number__c='480-555-5555';
        Physician.First_Name__c='James';
        Physician.Last_Name__c='Jones';
        Physician.Local_PCP__c=true;
        Physician.Network_Status__c='In Network';
        Physician.Network_Status_Verification_Date__c=system.today()+5;
        Physician.Phone_Number__c='480-555-5555';
        Physician.Provider_Type__c='COE Managing Physician';
        Physician.Specialty__c='Medical Oncologist';
        Physician.State__c='AZ';
        Physician.Street_Address__c='2500 S Power Rd';
        Physician.Zip_Code__c='85205';
        Physician.Oncology__c = oncology.id;
        
        
        upsert Physician;
        system.assert(Physician.id!=null);
        return Physician;
    }
    
    public oncology__c completeOncologyIntake(oncology__c oncology){
        oncology__c o = oncology;
        
        o.employee_gender__c ='Male';
        o.gender__c='Male';
        o.Employee_Cert__c ='Unit.Test';
        o.Employee_DOB__c = '1980-01-01';
        o.Patient_Medical_Plan_Number__c= 'BC1234';
        o.Patient_First_Name__c= 'Mario';
        o.Patient_Last_Name__c= 'Brother';
        o.Relationship_to_Subscriber__c= 'Spouse';
        o.Preferred_Language__c= 'English';
        o.Delegate_Status__c = 'Unit test';
        o.Translation_Services_Required__c= 'No';
        o.Referral_Source__c= 'Self Referral';
        o.Employee_First_Name__c = 'Kobe';
        o.Employee_Last_Name__c = 'Bryant';
        o.Patient_DOB__c= '1980-01-01';
        o.Patient_Phone__c= '480-909-9999';
        
        insert o;
        
        return o;
    }
    
    public Client_facility__c addClientFacility(id clientid, id prodecure, id facility){
    
        clientFacility= new Client_facility__c (Client__c=clientid, Facility__c=Facility,Procedure__c=prodecure);
        insert clientFacility;
        
        return clientFacility;
    }
    
    public Carrier__c addCarrier(){
    
        Carrier__c carrier = new Carrier__c(name='Unit.Test');
        insert carrier;
        
        return carrier;
    }
    
    public Hotel__c addHotel(){
    
        Hotel__c Hotel = new Hotel__c(name='Unit.Test');
        insert Hotel;
        
        return Hotel;
    }
    
    public oncology__c ReadytoRefer(oncology__c oncology){
         
         oncology.Employee_First_Name__c = 'Kobe';
         oncology.Employee_Last_Name__c = 'Bryant';
         oncology.Patient_Agrees_to_Referral__c='Yes';
         oncology.Date_Eligibility_Verified__c = date.today();
         oncology.Patient_First_Name__c='Unit';
         oncology.Patient_Last_Name__c='Test';
         date pdob = date.today().addYears(-30);
         oncology.Patient_DOB__c = pdob.year()+'-'+pdob.month()+'-'+pdob.day();
         oncology.Employee_Gender__c='Male';
         oncology.Gender__c='Male';
         oncology.Patient_Medical_Plan_Number__c = 'TXN1234567';
         oncology.Plan_Type__c = 'Unit Test';
         oncology.COBRA_Status__c = 'No';
         oncology.LOA_Status__c =  'No';
         oncology.Referral_Source__c = 'Accolade';
         oncology.Patient_Address__c = '2500 S Power Rd';
         oncology.Patient_City__c= 'Mesa';
         oncology.Patient_State__c= 'AZ';
         oncology.Patient_Zip_Code__c ='85206';
         oncology.Patient_Country__c = 'US';
         oncology.Patient_Preferred_Phone__c = 'Mobile';
         oncology.Preferred_Language__c= 'English';
         oncology.Translation_Services_Required__c = 'No';
         oncology.Relationship_to_Subscriber__c = 'Employee';
         oncology.Delegate_Status__c = 'unit test';
         oncology.Patient_Advocacy__c='unit test';
         oncology.Accolade_Nurse_Name__c='unit test';
         oncology.Accolade_Nurse_Number__c ='(480) 555-1212';
         oncology.Advocacy_Nurse_Email__c='unit@test.com';
         oncology.Facility_Nurse_Name__c='unit test';
         oncology.Facility_Nurse_Phone_Number__c ='(480) 555-1212';
         oncology.Facility_Nurse_Email__c ='unit@test.com';
         oncology.Client_HR__c='Facebook';
         oncology.HR_Name__c='unit test';
         oncology.HR_Phone_Number__c ='(480) 555-1212';
         oncology.HR_Email__c='unit@test.com';
         oncology.Carrier_UM_Nurse__c='Meritain Pre-Cert';
         oncology.Carrier_Nurse__c='unit test';
         oncology.Carrier_Nurse_Phone_Number__c ='(480) 555-1212';
         oncology.Carrier_Nurse_Email__c='unit@test.com';
         oncology.HDP_Nurse__c=userinfo.getuserid();
         oncology.HDP_Nurse_Phone_Number__c ='(480) 555-1212';
         oncology.HDP_Nurse_Email__c='unit@test.com';
         return oncology;
    }
    
    public static string cohx271(){
        string x271 =  'ISA*00*          *00*          *ZZ*770545013      *ZZ*341593929      *190225*1419*^*00501*000005477*0*P*:~';
               x271 += 'GS*HB*MTEXE*341593929*20190225*1908*1*X*005010X279A1~';
               x271 += 'ST*271*0001*005010X279A1~';
               x271 += 'BHT*0022*11*000000015*20190225*1908~';
               x271 += 'HL*1**20*1~';
               x271 += 'NM1*PR*2*MERITAIN HEALTH*****PI*MTAIN~';
               x271 += 'HL*2*1*21*1~';
               x271 += 'NM1*1P*2*Health Design Plus*****FI*341593929~';
               x271 += 'HL*3*2*22*0~';
               x271 += 'TRN*1*583249000*9EMDEON999~';
               x271 += 'NM1*IL*1*FXEL*KE M****MI*999999999~';
               x271 += 'REF*6P*16404~';
               x271 += 'REF*18*001~';
               x271 += 'N3*214 OLIVE HILL LN~';
               x271 += 'N4*WOODSIDE*CA*94062~';
               x271 += 'DMG*D8*19801217~';
               x271 += 'INS*Y*18*001*25~';
               x271 += 'DTP*346*D8*20190101~';
               x271 += 'EB*1*FAM*30*GP*CROSSOVER XO~';
               x271 += 'LS*2120';
        return x271;
    
    }
        
}