@isTest
private class newClaimErrorExtTest {

    static testMethod void newClaimErrorExt() {
        
        
        caClaim__c cc = new caClaim__c();
        cc.name = '1234567890';
        
        Claim_audit__c ca = new Claim_audit__c();
        newClaimErrorExt ceca = new newClaimErrorExt(new ApexPages.StandardController(ca));
        
        ceca.claim = cc;
        ceca.newClaimSave();
        
        cc.name = '12345678900';
        ceca.claim = cc;
        ceca.newClaimSave();
        
        ceca.mySave();
        
        Claim_Audit_Error__c ce = new Claim_Audit_Error__c();
        newClaimErrorExt cece = new newClaimErrorExt(new ApexPages.StandardController(ce));
        
            
    }

}