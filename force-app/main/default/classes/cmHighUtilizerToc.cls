public with sharing class cmHighUtilizerToc{
    
    public static string letterText(Case_Management__c cm, Case_Management_Letter__c cml, Case_Management_Clinician__c clinician){
        string letterText='';
        boolean hasPatientAddress = (cm.patient__r.Address__c!=null && cm.patient__r.City__c!=null && cm.patient__r.State__c!=null && cm.patient__r.Zip__c!=null);
        
        letterText+='<div style="width:100%; margin-left:auto; margin-right:auto;margin-bottom:2em;text-align:left">';
        /*
        letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c+'<br/>';
        letterText+=Clinician.Street__c+'<br/>';
        letterText+=Clinician.City__c+', '+'<nbsp/>';
        letterText+=Clinician.State__c+' ';
        letterText+=Clinician.Zip_Code__c+'<br/>';
        letterText+='<br/>';
        */

        //not pass null clinician values on the letter
        if(Clinician.Credentials__c!=null){
           letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+', '+Clinician.Credentials__c+'<br/>';
        }else{
           letterText+=Clinician.First_Name__c+' '+Clinician.Last_Name__c+'<br/>';
        }        
    
        //not pass null values on the letter
        if(Clinician.Street__c!=null){
           letterText+=Clinician.Street__c+', '+'<nbsp/>';
        }

        if(Clinician.State__c!=null){
           letterText+=Clinician.State__c+', '+'<nbsp/>';
        }

        if(Clinician.Zip_Code__c!=null){
           letterText+=Clinician.Zip_Code__c+'<br/>';
        }
        
        letterText+='RE:'+cm.patient__r.Patient_First_Name__c+'&nbsp;'+cm.patient__r.Patient_Last_Name__c+'&nbsp'+cm.patient__r.Patient_Date_of_Birth__c+'<br/>';
        letterText+='</div>';
        
        letterText+='<p>';
        letterText+='Dear&nbsp;'+Clinician.First_Name__c+' '+Clinician.Last_Name__c+',';
        letterText+='</p>';
        letterText+='<p>';  
        letterText+='We are soliciting your assistance to help us assist your patient in managing their health care costs and expenses. <br/> ';
        letterText+='</p>';

        letterText+='<p>';
        letterText+='Contigo Health is the administrator for  '+cm.patient__r.Patient_First_Name__c+'\'s' +' health plan. Our records indicate that '+cm.patient__r.Patient_First_Name__c+ ' has been recently hospitalized, had emergency care, or has' ;
        letterText+=' been identified as a high cost claimant. <br/>';
        letterText+='</p>';  
 
        letterText+='<p>';                
        letterText+='Our goal is to assist '+ cm.patient__r.Patient_First_Name__c+' with managing transitions of care, pharmacy issues, and/or home health care needs, such as nursing or therapy, to try to reduce  readmissions';
        letterText+='  or acute preventable exacerbation of illness.<br/>';
        letterText+='</p>';  

        letterText+='<p>';  
        letterText+='We would appreciate any assistance you could provide in helping us engage your patient in dialogue. Our experienced Registered Nurse Case Managers have';
        letterText+=' proven to be invaluable resources to patients. Most patients, once engaged, truly recognize the benefit these nurses add to their care team.';
        letterText+='  The nurses help patients, and their families manage the complex maze of today’s healthcare system and the health insurance coverage landscape.<br/>';
        letterText+='</p>';         

        letterText+='<p>';  
        letterText+='I look forward to talking with you soon. Please feel free to call me at 1-877-891-2690, Monday-Friday from 8:30AM – 5:00PM Eastern Time. If I am not able to take your call,';
        letterText+=' please leave a message on my confidential voicemail and I will return your call as soon as possible.';
        letterText+='</p>';   

        letterText+='Thank you for any assistance you can provide to us and your patient.<br/><br/>';

        letterText+='Sincerely,<br/><br/>';
        letterText+='Care Management Department<br/>';
        letterText+='Contigo Health<br/>';
        letterText+='P.O. Box 2584<br/>';
        letterText+='Hudson, OH 44236<br/><br/>';

        return letterText;
    }
    
}