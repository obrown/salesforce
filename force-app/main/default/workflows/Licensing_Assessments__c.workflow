<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Staff_Licensing_Assessments_Due_Email</fullName>
        <description>Staff &amp; Assessments Due Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>jvolanti@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>laualexander@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/StaffLicensing_Assessments_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>Staff_Licensing_Assessments_Medical_NursingDue_Email</fullName>
        <description>Staff &amp; Assessments-Medical/Nursing Due Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>bfarone@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>jvolanti@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>laualexander@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/StaffLicensing_Assessments_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>State_Licensing_Assessments_Due_Email</fullName>
        <description>State-Licensing &amp; Assessments Due Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>jvolanti@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>laualexander@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/State_Licensing_Assessments_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>Vaccines_Licensing_Assessments_Due_Email</fullName>
        <description>Vaccines &amp; Assessments Due Email</description>
        <protected>false</protected>
        <recipients>
            <recipient>jvolanti@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>laualexander@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Vaccine_Licensing_Assessments_Due_Date</template>
    </alerts>
    <alerts>
        <fullName>stateLicenseEvaluation30</fullName>
        <description>stateLicenseEvaluation30</description>
        <protected>false</protected>
        <recipients>
            <recipient>jvolanti@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <recipients>
            <recipient>laualexander@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/stateLicenseEvaluation30</template>
    </alerts>
    <rules>
        <fullName>Staff-Licensing-Medical%2FNursing Expiring30Days</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3 and 4</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Expiration_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Staff Licensing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.Expiration30__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.License_Class__c</field>
            <operation>equals</operation>
            <value>Medical/Nursing</value>
        </criteriaItems>
        <description>Send email at 30days prior to due date Licence class Medical/Nursing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Staff_Licensing_Assessments_Medical_NursingDue_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Expiration_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Staff-Licensing-Medical%2FNursing Expiring60Days</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3 and 4</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Expiration_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Staff Licensing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.Expiration60__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.License_Class__c</field>
            <operation>equals</operation>
            <value>Medical/Nursing</value>
        </criteriaItems>
        <description>Send email at 60 days prior to due date Licence class Medical/Nursing</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Staff_Licensing_Assessments_Medical_NursingDue_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Expiration_Date__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Staff-LicensingExpiring30Days</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Staff Licensing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.Expiration30__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email at 30 days prior to due date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Staff_Licensing_Assessments_Due_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Expiration_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Staff-LicensingExpiring60Days</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3 and 4</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Expiration_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Staff Licensing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.Expiration60__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.License_Class__c</field>
            <operation>notEqual</operation>
            <value>Medical/Nursing</value>
        </criteriaItems>
        <description>Send email at 60 days prior to due date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Staff_Licensing_Assessments_Due_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Expiration_Date__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>State-LicensingExpiring30days</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>State Licensing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date_30__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email at  30 days prior to due date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>State_Licensing_Assessments_Due_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Due_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>State-LicensingExpiring60Days</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>State Licensing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date_60__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email at 60 prior to due date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>State_Licensing_Assessments_Due_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Due_Date__c</offsetFromField>
            <timeLength>-60</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>State-LicensingExpiring7days</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>State Licensing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date_7__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email at  7 days prior to due date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>State_Licensing_Assessments_Due_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Due_Date__c</offsetFromField>
            <timeLength>-7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Vaccines %26 Assessments-LicensingExpiring15Days</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vaccines &amp; Assessments</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date_15__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email at 15 days prior to due date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Vaccines_Licensing_Assessments_Due_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Due_Date__c</offsetFromField>
            <timeLength>-15</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Vaccines %26 Assessments-LicensingExpiring45Days</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Vaccines &amp; Assessments</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.Due_Date_45__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email at 45 days prior to due date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Vaccines_Licensing_Assessments_Due_Email</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Due_Date__c</offsetFromField>
            <timeLength>-45</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>stateLicenseEvaluation30</fullName>
        <active>true</active>
        <booleanFilter>1 and 2 and 3</booleanFilter>
        <criteriaItems>
            <field>Licensing_Assessments__c.Evaluation_Date__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>State Licensing</value>
        </criteriaItems>
        <criteriaItems>
            <field>Licensing_Assessments__c.sendStateEDEmail__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Send email 30 days prior to Evaluation date</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>stateLicenseEvaluation30</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Licensing_Assessments__c.Evaluation_Date__c</offsetFromField>
            <timeLength>-30</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
