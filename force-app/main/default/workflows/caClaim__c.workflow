<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>UpdateGroupCaClaim</fullName>
        <field>Group_Number__c</field>
        <formula>Client__r.Group_Number__c</formula>
        <name>UpdateGroupCaClaim</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>UpdateUnderwritercaClaim</fullName>
        <field>Underwriter__c</field>
        <formula>Client__r.Underwriter__c</formula>
        <name>UpdateUnderwritercaClaim</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setClaimAuditClaimNumber</fullName>
        <field>Claim_Number__c</field>
        <formula>Name</formula>
        <name>setClaimAuditClaimNumber</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>SetUnderWriterCaClaim</fullName>
        <actions>
            <name>UpdateGroupCaClaim</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>UpdateUnderwritercaClaim</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>caClaim__c.Group_Number__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>caClaimName</fullName>
        <actions>
            <name>setClaimAuditClaimNumber</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the claim number field to the value of the name field</description>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
