<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setInfectiousDiseaseOncology</fullName>
        <field>Sub_Status_Reason__c</field>
        <literalValue>Infectious Disease</literalValue>
        <name>setInfectiousDiseaseOncology</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>setInfectiousDiseaseOncology</fullName>
        <actions>
            <name>setInfectiousDiseaseOncology</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Oncology__c.Status_Reason__c</field>
            <operation>equals</operation>
            <value>Infectious Disease</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
