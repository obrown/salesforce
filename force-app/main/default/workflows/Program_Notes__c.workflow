<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setCreatedByNurseTrue</fullName>
        <field>created_by_nurse__c</field>
        <literalValue>1</literalValue>
        <name>setCreatedByNurseTrue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setFacilityTravelConfirmation</fullName>
        <field>Facility_Travel_Confirmation__c</field>
        <formula>today()</formula>
        <name>setFacilityTravelConfirmation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Patient_Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setPatientTravelConfirmation</fullName>
        <field>Patient_Travel_Confirmation__c</field>
        <formula>today()</formula>
        <name>setPatientTravelConfirmation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Patient_Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateLegacyCreatedDate</fullName>
        <field>legacyCreatedDate__c</field>
        <formula>now()</formula>
        <name>updateLegacyCreatedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updatePCmodified</fullName>
        <field>legacyLastModifiedDate__c</field>
        <formula>now()</formula>
        <name>updatePCmodified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Patient_Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updatePCupdateField</fullName>
        <field>legacyLastModifiedDate__c</field>
        <formula>now()</formula>
        <name>updatePCupdateField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Patient_Case__c</targetObject>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updatelegacyCreatedBy</fullName>
        <field>Legacy_Created_By__c</field>
        <formula>$User.FirstName + &apos; &apos; +  $User.LastName</formula>
        <name>updatelegacyCreatedBy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>FacilityTravelConfirmation</fullName>
        <actions>
            <name>setFacilityTravelConfirmation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Program_Notes__c.Contact_Type__c</field>
            <operation>equals</operation>
            <value>COE Facility</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Notes__c.Communication_Type__c</field>
            <operation>equals</operation>
            <value>Travel Confirmation</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>LegacyCreatedDate</fullName>
        <actions>
            <name>updateLegacyCreatedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>updatelegacyCreatedBy</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Populate the legacy created date of a note</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PatientTravelConfirmation</fullName>
        <actions>
            <name>setPatientTravelConfirmation</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Program_Notes__c.Contact_Type__c</field>
            <operation>equals</operation>
            <value>Patient,Caregiver</value>
        </criteriaItems>
        <criteriaItems>
            <field>Program_Notes__c.Communication_Type__c</field>
            <operation>equals</operation>
            <value>Travel Confirmation</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>noteCreatedByaNurse</fullName>
        <actions>
            <name>setCreatedByNurseTrue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>CreatedBy.UserRole.Name ==&quot;HDP Care Management&quot;</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>noteUpdatePC</fullName>
        <actions>
            <name>updatePCupdateField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Updated last modified date on a case when a note is created</description>
        <formula>if(and(not(contains(Notes__c,&apos;Patient is eligible&apos;)),
       not(contains(Notes__c,&apos;Patient is not eligible&apos;))),
	   if($User.Alias!=&apos;System&apos;,
	   
	   if(and(not($Profile.Name=&apos;Care Management COE&apos;), Patient_Case__r.Owner:Queue.QueueName =&apos;HDP Care Management COE&apos;),false,true),false),false)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
