<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>updatelegacyLastModifiedDatefromStage</fullName>
        <field>legacyLastModifiedDate__c</field>
        <formula>now()</formula>
        <name>updatelegacyLastModifiedDatefromStage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Transplant__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>isStageModifed</fullName>
        <actions>
            <name>updatelegacyLastModifiedDatefromStage</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
