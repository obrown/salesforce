<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setDuplicateFieldEwtWT</fullName>
        <field>preventDuplicate__c</field>
        <formula>Name+&apos;&apos;+Document_Type__c</formula>
        <name>setDuplicateFieldEwtWT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>preventDuplicateEWTWorkTask</fullName>
        <actions>
            <name>setDuplicateFieldEwtWT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
