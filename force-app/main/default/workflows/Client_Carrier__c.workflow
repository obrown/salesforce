<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setEcenCarrierName</fullName>
        <field>Name</field>
        <formula>Carrier__r.Name</formula>
        <name>setEcenCarrierName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>setEcenCarrierName</fullName>
        <actions>
            <name>setEcenCarrierName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>not(isNull(Carrier__c ))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
