<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setPMNote</fullName>
        <description>set PMNote to true if note is added</description>
        <field>PM_Note__c</field>
        <literalValue>1</literalValue>
        <name>setPMNote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Patient__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>PMNote</fullName>
        <actions>
            <name>setPMNote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Patient_Management_Note__c.Body__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Set to true if patient note entered</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
