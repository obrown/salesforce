<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setClaimErrorClaimAuditStatus</fullName>
        <field>Audit_Status__c</field>
        <literalValue>Error Review</literalValue>
        <name>setClaimErrorClaimAuditStatus</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Claim_Audit__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>newClaimError</fullName>
        <actions>
            <name>setClaimErrorClaimAuditStatus</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the parent claim audit status to Error Review</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
