<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>addlPayCardLoadFieldUpdate</fullName>
        <field>Prepaid_Card_Additional_Load_Date__c</field>
        <formula>Load_Date__c</formula>
        <name>addlPayCardLoad</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Patient_Case__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>addlPayCardLoadLogic</fullName>
        <actions>
            <name>addlPayCardLoadFieldUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>COE_Expenses__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Paycard</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
