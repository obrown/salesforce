<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>jetBlueEligibiltyReminder</fullName>
        <description>jetBlueEligibiltyReminder</description>
        <protected>false</protected>
        <recipients>
            <recipient>mmartin@hdplus.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/CustomerPortalSelfRegUserEmail</template>
    </alerts>
    <rules>
        <fullName>externalNewEligibilty</fullName>
        <active>true</active>
        <formula>Patient_Case__r.nClient_Name__c == &apos;jetBlue&apos; &amp;&amp;
isNull(Date_Eligibility_Checked_with_Client__c )</formula>
        <triggerType>onCreateOnly</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>jetBlueEligibiltyReminder</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Eligibilty__c.CreatedDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
