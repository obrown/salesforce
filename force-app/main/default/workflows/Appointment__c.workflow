<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>updateLastModifiedDateFromAppointment</fullName>
        <field>legacyLastModifiedDate__c</field>
        <formula>now()</formula>
        <name>updateLastModifiedDateFromAppointment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
        <targetObject>Transplant__c</targetObject>
    </fieldUpdates>
    <rules>
        <fullName>isAppoinmentModified</fullName>
        <actions>
            <name>updateLastModifiedDateFromAppointment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>TRUE</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
