<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>addClosedDate</fullName>
        <field>Closed_Date__c</field>
        <formula>today()</formula>
        <name>addClosedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setTransplantRecordTypeID</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Walmart_Transplant</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>setTransplantRecordTypeID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>transplantClearExpired</fullName>
        <field>Patient_Expired_Date__c</field>
        <name>transplantClearExpired</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>upateOwner</fullName>
        <field>OwnerId</field>
        <lookupValue>HDP_Care_Management_Transplant</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>upateOwner</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>isClosed</fullName>
        <actions>
            <name>addClosedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Transplant__c.Intake_Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>setownertohdpQueue</fullName>
        <actions>
            <name>upateOwner</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Set the owner to the correct queue</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>transplantRecordTypeID</fullName>
        <actions>
            <name>setTransplantRecordTypeID</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>set the recordID to Walmart Transplant</description>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>transplantStatsReasonChangedfromExpired</fullName>
        <actions>
            <name>transplantClearExpired</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Transplant__c.Intake_Status_Reason__c</field>
            <operation>notEqual</operation>
            <value>Expired</value>
        </criteriaItems>
        <description>Clear the expired value if the status reason is changed from expired</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <tasks>
        <fullName>Benefit_Quote_Response</fullName>
        <assignedToType>owner</assignedToType>
        <description>Please follow up with the Walmart Benefits Team regarding the request for the benefit quote.

Thank you</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Benefit Quote Response</subject>
    </tasks>
</Workflow>
