<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setEWTWorkTaskReasonDupField</fullName>
        <field>preventDuplicate__c</field>
        <formula>Name+&apos;&apos;+ EWT_Work_Tasks__c</formula>
        <name>setEWTWorkTaskReasonDupField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>preventDuplicateEWTWTR</fullName>
        <actions>
            <name>setEWTWorkTaskReasonDupField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
