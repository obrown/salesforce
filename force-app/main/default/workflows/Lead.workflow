<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Company_Name</fullName>
        <field>Company</field>
        <formula>$RecordType.DeveloperName</formula>
        <name>Company Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_transferred_to_eligibility</fullName>
        <field>Date_Care_Mgmt_Transfers_to_Eligibility__c</field>
        <formula>TODAY()</formula>
        <name>Date transferred to eligibility</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Employee_City</fullName>
        <field>Patient_City__c</field>
        <formula>City</formula>
        <name>Employee City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Employee_Country</fullName>
        <field>Country</field>
        <formula>Country</formula>
        <name>Employee Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Employee_Country1</fullName>
        <field>Country</field>
        <formula>Patient_Country__c</formula>
        <name>Employee Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Employee_Country2</fullName>
        <field>Patient_Country__c</field>
        <formula>Country</formula>
        <name>Employee Country</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Employee_Home_Phone</fullName>
        <field>Patient_Home_Phone__c</field>
        <formula>Phone</formula>
        <name>Employee Home Phone</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Employee_Street</fullName>
        <field>Patient_Street__c</field>
        <formula>Street</formula>
        <name>Employee Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Employee_Zip_Postal_Code</fullName>
        <field>Patient_Zip_Postal_Code__c</field>
        <formula>PostalCode</formula>
        <name>Employee Zip/Postal Code</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Home_Number</fullName>
        <field>Patient_Home_Phone__c</field>
        <formula>Callback_Number__c</formula>
        <name>Home Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Mobile_Number</fullName>
        <field>Patient_Mobile__c</field>
        <formula>Callback_Number__c</formula>
        <name>Mobile Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_City</fullName>
        <field>Patient_City__c</field>
        <formula>City</formula>
        <name>Patient City</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_DOB</fullName>
        <field>Patient_DOB__c</field>
        <formula>Employee_DOB__c</formula>
        <name>Patient DOB</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_First_Name</fullName>
        <field>Patient_First_Name__c</field>
        <formula>FirstName</formula>
        <name>Patient First Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_Last_Name</fullName>
        <field>Patient_Last_Name__c</field>
        <formula>LastName</formula>
        <name>Patient Last Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_SSN</fullName>
        <field>Patient_SSN__c</field>
        <formula>Employee_SSN__c</formula>
        <name>Patient SSN</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_Sex_Female</fullName>
        <field>Patient_Sex__c</field>
        <literalValue>Female</literalValue>
        <name>Patient Sex Female</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_Sex_Male</fullName>
        <field>Patient_Sex__c</field>
        <literalValue>Male</literalValue>
        <name>Patient Sex Male</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_Street</fullName>
        <field>Patient_Street__c</field>
        <formula>Street</formula>
        <name>Patient Street</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Patient_Zip</fullName>
        <field>Patient_Zip_Postal_Code__c</field>
        <formula>PostalCode</formula>
        <name>Patient Zip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Proper_Case_for_EEFirst_Name</fullName>
        <field>FirstName</field>
        <formula>UPPER(LEFT( FirstName ,1))&amp;LOWER(Mid(FirstName, 2, Len(FirstName)-1))</formula>
        <name>Proper Case for EEFirst Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Proper_Case_for_PFirst_name</fullName>
        <field>Patient_First_Name__c</field>
        <formula>UPPER(LEFT( Patient_First_Name__c ,1))&amp;LOWER(Mid( Patient_First_Name__c , 2, Len( Patient_First_Name__c )-1))</formula>
        <name>Proper Case for PFirst name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Propoer_Case_EELast_Name</fullName>
        <field>LastName</field>
        <formula>UPPER(LEFT( LastName ,1))&amp;LOWER(Mid( LastName , 2, Len( LastName )-1))</formula>
        <name>Propoer Case Last Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Propoer_Case_for_PLast_Name</fullName>
        <field>Patient_Last_Name__c</field>
        <formula>UPPER(LEFT( Patient_Last_Name__c ,1))&amp;LOWER(Mid( Patient_Last_Name__c , 2, Len( Patient_Last_Name__c )-1))</formula>
        <name>Propoer Case for PLast Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SpineComplexityUpdate</fullName>
        <field>Procedure_Complexity__c</field>
        <literalValue>Non-Complex</literalValue>
        <name>SpineComplexityUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Work_Number</fullName>
        <field>Patient_Work_Phone__c</field>
        <formula>Callback_Number__c</formula>
        <name>Work Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <tasks>
        <fullName>Initial_Call_Notes</fullName>
        <assignedToType>owner</assignedToType>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Lead.CreatedDate</offsetFromField>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Completed</status>
        <subject>Initial Call Notes</subject>
    </tasks>
</Workflow>
