<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setRecommendationLogicName</fullName>
        <field>Name</field>
        <formula>Field_Label__c +&apos; &apos;+ text(Field_Logic__c) +&apos; &apos;+ Field_Value__c</formula>
        <name>setRecommendationLogicName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setRecommendationLogicNameValue</fullName>
        <field>Name</field>
        <formula>Field_Label__c +&apos; &apos;+ text(Field_Logic__c) +&apos; &apos;+ Display_Value__c</formula>
        <name>setRecommendationLogicNameVaule</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>setRecommendationLogic</fullName>
        <actions>
            <name>setRecommendationLogicNameValue</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Recommendation_Logic__c.Field_Logic__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Recommendation_Logic__c.Field_name__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Recommendation_Logic__c.Field_Value__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
