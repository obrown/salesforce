<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setCCIInquiryReasonDupField</fullName>
        <field>preventDuplicate__c</field>
        <formula>Name</formula>
        <name>setCCIInquiryReasonDupField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>preventDuplicateCCIInquiryReason</fullName>
        <actions>
            <name>setCCIInquiryReasonDupField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
