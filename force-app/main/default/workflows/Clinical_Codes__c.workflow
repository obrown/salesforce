<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Clinical_Code_Page_Layout</fullName>
        <description>Changes the page layout to Program page layout when the program field is populated</description>
        <field>RecordTypeId</field>
        <lookupValue>Program_Record_Type</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Clinical Code Page Layout</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
</Workflow>
