<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setCCIWorkTaskDupField</fullName>
        <field>preventDuplicate__c</field>
        <formula>Name+&apos;&apos;+
CC_Inquiry_Work_Department__c</formula>
        <name>setCCIWorkTaskDupField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>preventDuplicateCCIWorkTask</fullName>
        <actions>
            <name>setCCIWorkTaskDupField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
