<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setEWTWorkDeptDupField</fullName>
        <field>preventDuplicate__c</field>
        <formula>Name +&apos;&apos;+ Work_Task__r.Document_Type__c+&apos;&apos;+Work_Task__c +&apos;&apos;+ Work_Task_Reason__c</formula>
        <name>setEWTWorkDeptDupField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>preventDuplicateEWTWorkDept</fullName>
        <actions>
            <name>setEWTWorkDeptDupField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
