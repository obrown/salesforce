<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>AppealCase_NameUpdate</fullName>
        <description>Set the Appeal case name to the case number</description>
        <field>Name</field>
        <formula>Case_Number__c</formula>
        <name>AppealCase NameUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setAppealOrigDateReceived</fullName>
        <field>Orig_Date_Received__c</field>
        <formula>DateReceived__c</formula>
        <name>setAppealOrigDateReceived</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>AppealCase NameUpdate</fullName>
        <actions>
            <name>AppealCase_NameUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Update the Appeal Case Name with the value of the case number</description>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>setAppealOrigDateReceived</fullName>
        <actions>
            <name>setAppealOrigDateReceived</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>isnull( Orig_Date_Received__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
