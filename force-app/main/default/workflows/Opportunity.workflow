<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Actual_Arrival_Date</fullName>
        <field>Actual_Arrival__c</field>
        <formula>Arrival_Date__c</formula>
        <name>Actual Arrival Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Actual_Departure_Date</fullName>
        <field>Actual_Departure__c</field>
        <formula>Departure_Date__c</formula>
        <name>Actual Departure Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Address_for_Stipend_Check</fullName>
        <field>Address_for_Stipend_Check__c</field>
        <formula>Account.BillingStreet &amp; BR() &amp;
 Account.BillingCity &amp;&quot;,&quot;&amp; Account.BillingState &amp;&quot; &quot;&amp; Account.BillingPostalCode &amp; BR() &amp;
 Account.BillingCountry</formula>
        <name>Address for Stipend Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Address_for_Stipend_Check_Retro</fullName>
        <field>Address_for_Retrospective_Payment__c</field>
        <formula>Account.BillingStreet &amp; BR() &amp;
 Account.BillingCity &amp;&quot;,&quot;&amp; Account.BillingState &amp;&quot; &quot;&amp; Account.BillingPostalCode &amp; BR() &amp;
 Account.BillingCountry</formula>
        <name>Address for Stipend Check (Retro)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Program_Name</fullName>
        <field>Name</field>
        <formula>if(letterAppend__c = null,
Account.Name+&quot; &quot;+ Patient_First_Name__c+&quot; &quot;+ Patient_Last_Name__c,
Account.Name+&quot;-&quot; +letterAppend__c + &quot; &quot;+ Patient_First_Name__c+&quot; &quot;+ Patient_Last_Name__c)</formula>
        <name>Program Name</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rec_Baggage</fullName>
        <field>Rec_Baggage__c</field>
        <formula>IF( ISPICKVAL( Travel_Type__c , &quot;Flying&quot;),100,0)</formula>
        <name>Rec Baggage</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rec_In_Town</fullName>
        <field>Rec_In_Town_Travel_Allowance__c</field>
        <formula>IF( 
(ISPICKVAL( Travel_Type__c , &quot;Flying&quot;) || ISPICKVAL( Travel_Type__c , &quot;Driving Greater than 60 miles&quot;)), 
(CASE(Client_Name__c
, &quot;PepsiCo&quot;, ((Hotel_Checkout_Date__c - Hotel_Check_In_Date__c) +1)*75
, &quot;Lowes&quot;, ((Hotel_Checkout_Date__c - Hotel_Check_In_Date__c) +1)*65
, &quot;Kohl&apos;s&quot;, ((Hotel_Checkout_Date__c - Hotel_Check_In_Date__c) +1)*65
,0)),
0)</formula>
        <name>Rec In Town Travel Allowance</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rec_Total_Stipend_Amount</fullName>
        <field>Rec_Travel_Amount__c</field>
        <formula>Rec_Airport_Parking__c + Rec_Baggage__c + Rec_Travel_Amount__c + Rec_In_Town_Travel_Allowance__c</formula>
        <name>Rec Total Stipend Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rec_Travel_Amount</fullName>
        <field>Rec_Travel_Amount__c</field>
        <formula>IF(NOT( ISPICKVAL( Travel_Type__c , &quot;Flying&quot;)),330,0)</formula>
        <name>Rec Travel Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_the_Rec_Airport_Parking</fullName>
        <field>Rec_Airport_Parking__c</field>
        <formula>IF( 
ISPICKVAL( Travel_Type__c , &quot;Flying&quot;), 
((Hotel_Checkout_Date__c - Hotel_Check_In_Date__c) +1)*15,
0)</formula>
        <name>Update the Rec Airport Parking</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>updateToOpen</fullName>
        <field>Program_Status__c</field>
        <literalValue>Open</literalValue>
        <name>updateToOpen</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <tasks>
        <fullName>Reminder_3wks_Prior_to_Procedure_Date</fullName>
        <assignedTo>HDPMemberAdvocate</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Submit to Finance</description>
        <dueDateOffset>0</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <priority>Normal</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Reminder:  3wks Prior to Procedure Date</subject>
    </tasks>
    <tasks>
        <fullName>Stipend_Request_Notification</fullName>
        <assignedTo>HDPMemberAdvocate</assignedTo>
        <assignedToType>role</assignedToType>
        <description>Please request a stipend check if applicable.</description>
        <dueDateOffset>-14</dueDateOffset>
        <notifyAssignee>false</notifyAssignee>
        <offsetFromField>Opportunity.Estimated_Arrival__c</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Stipend Request Notification</subject>
    </tasks>
</Workflow>
