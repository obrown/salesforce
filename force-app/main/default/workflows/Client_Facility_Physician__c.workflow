<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setCFPhysName</fullName>
        <field>Name</field>
        <formula>if(isBlank(text(Physician__r.Credentials__c)), 
Physician__r.First_Name__c +&apos; &apos;+Physician__r.Last_Name__c, 
Physician__r.First_Name__c +&apos; &apos;+Physician__r.Last_Name__c+&apos;, &apos;+ text(Physician__r.Credentials__c))</formula>
        <name>setCFhysName</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>setCFPhysName</fullName>
        <actions>
            <name>setCFPhysName</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 or 2</booleanFilter>
        <criteriaItems>
            <field>User.FirstName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
