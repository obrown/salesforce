<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>amsClosedBy</fullName>
        <field>Error_Closed_By__c</field>
        <formula>$User.FirstName +&apos; &apos;+$User.LastName</formula>
        <name>amsClosedBy</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>amsClosedDate</fullName>
        <field>Closed_Date__c</field>
        <formula>today()</formula>
        <name>amsClosedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>errorClearClosedDate</fullName>
        <field>Closed_Date__c</field>
        <name>errorClearClosedDate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setAmsErrorReasonNull</fullName>
        <field>Error_Reason__c</field>
        <name>setAmsErrorReasonNull</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setAmsOutcomeNull</fullName>
        <field>Outcome__c</field>
        <name>setAmsOutcomeNull</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>setAmsStatusClosed</fullName>
        <field>Status__c</field>
        <literalValue>Closed</literalValue>
        <name>setAmsStatusClosed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>All AMS Worktasks closed</fullName>
        <actions>
            <name>amsClosedBy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>amsClosedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>magpie_Error__c.Number_of_Work_Tasks__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>magpie_Error__c.Open_Work_Tasks__c</field>
            <operation>equals</operation>
            <value>0</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ValidErrorNo</fullName>
        <actions>
            <name>setAmsErrorReasonNull</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>magpie_Error__c.Error_Validated__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>ValidErrorYes</fullName>
        <actions>
            <name>setAmsOutcomeNull</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>magpie_Error__c.Error_Validated__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>isErrorClosed</fullName>
        <actions>
            <name>amsClosedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>magpie_Error__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>isErrorClosedWithNoDate</fullName>
        <actions>
            <name>amsClosedBy</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>amsClosedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>magpie_Error__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>magpie_Error__c.Closed_Date__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>isErrorOpen</fullName>
        <actions>
            <name>errorClearClosedDate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>magpie_Error__c.Status__c</field>
            <operation>notEqual</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
