<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>setEWTClosedReasonDupField</fullName>
        <field>preventDuplicate__c</field>
        <formula>Name+&apos;&apos;+
EWT_Document_Type__c
+&apos;&apos;+
EWT_Work_Department__c
+&apos;&apos;+
EWT_Work_Task__c
+&apos;&apos;+
EWT_Work_Task_Reason__c</formula>
        <name>setEWTClosedReasonDupField</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>false</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>preventDuplicateEWTClosedReason</fullName>
        <actions>
            <name>setEWTClosedReasonDupField</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
