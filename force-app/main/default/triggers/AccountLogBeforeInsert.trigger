trigger AccountLogBeforeInsert on Appeal_Case__c(before Insert) {
    for (Appeal_Case__c ac:Trigger.new) {
        ac.Orig_Date_Received__c = ac.DateReceived__c;
        ac.preventDuplicates__c = string.valueof(datetime.now().getTime());
    }
}