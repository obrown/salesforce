trigger beforeCustomerCareDependent on Customer_Care_Dependent__c (before insert, before update) {

    for(Customer_Care_Dependent__c c : trigger.new){
    
        if(c.Employee_First_Name__c!=null){
            try{
                c.Employee_First_Name_Search__c = c.Employee_First_Name__c.left(1);
            }catch(exception e){}
        
        }
        
        if(c.Employee_Last_Name__c!=null){
            try{
                c.Employee_Last_Name_Search__c = c.Employee_Last_Name__c.left(1);
            }catch(exception e){}
        }
        
        if(c.Patient_First_Name__c!=null){
            try{
                c.Patient_First_Name_Search__c = c.Patient_First_Name__c.left(1);
            }catch(exception e){}
        }
        
        if(c.Patient_Last_Name__c!=null){
            try{
                c.Patient_Last_Name_Search__c = c.Patient_Last_Name__c.left(1);
            }catch(exception e){}
        }
    
    }

}