trigger patientCaseTaskAfterUpdate on Patient_Case_Task__c (after update) {
    
    patient_case__c[] updated_cases = new patient_case__c[]{};
    set<id> pcId = new set<id>();
    
    for(Patient_Case_Task__c pct : trigger.new){
        
        if(pct.status__c=='Complete' && pct.status__c != trigger.oldMap.get(pct.id).status__c){
            if(pct.field__c!=null && pct.field__c!=''){
                patient_case__c pc = new patient_case__c(id=pct.patient_case__c); 
                pc.put(pct.field__c, pct.completedDate__c);   
                if(!pcId.contains(pc.id)){
                    pcId.add(pc.id);
                    updated_cases.add(pc);
                }
            }
        }
        
    }
    
    if(!updated_cases.isEmpty()){
        update updated_cases;
    }

}