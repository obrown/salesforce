trigger claimBefore on Claim__c (before insert) {

    map<id,patient_case__c> pcmap = new map<id,patient_case__c>();
    set<id> pcIDs = new set<id>();
    set<id> claimRecIDs = new set<id>();
    patient_case__c[] pcList = new patient_case__c[]{};
    
    for(Claim__c c : trigger.new){
        
        if(c.patient_case__c != null && c.Date_HP_Paid__c==null){
            pcIDs.add(c.patient_case__c);
            claimRecIDs.add(c.patient_case__c);
        }else{
            pcIDs.add(c.patient_case__c);
        }
        
    }
    
    for(patient_case__c p :[select employee_first_name__c,employee_last_name__c,
                            patient_first_name__c,patient_last_name__c, status_reason__c 
                            from patient_case__c where id in : pcids]){
        if(claimRecIDs.contains(p.id)){                    
            p.status_reason__c = 'Claim Received';
        }else{
            p.status_reason__c = 'Claim Paid';
        }
        pcList.add(p);
        pcmap.put(p.id,p);
    }
    
    for(Claim__c c : trigger.new){
        if((c.employee_name__c=='' || c.employee_name__c==null) && c.patient_case__c != null){
          
            c.employee_name__c = pcmap.get(c.patient_case__c).employee_first_name__c + ' ' + pcmap.get(c.patient_case__c).employee_last_name__c;
            c.patient_name__c = pcmap.get(c.patient_case__c).patient_first_name__c + ' ' + pcmap.get(c.patient_case__c).patient_last_name__c;
        }
        
    }
    
    update pcList;
}