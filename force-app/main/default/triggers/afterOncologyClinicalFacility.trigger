trigger afterOncologyClinicalFacility on Oncology_Clinical_Facility__c (after insert, after update){

    map<id, Oncology_Clinical_Facility__c> determinationMap = new map<id, Oncology_Clinical_Facility__c>();
    map<id, Oncology_Clinical_Facility__c> planOfCareMap = new map<id, Oncology_Clinical_Facility__c>();
    
    for(Oncology_Clinical_Facility__c  ocf : trigger.new){
        
        if(ocf.plan_of_care_submission_date__c!=null){
            planOfCareMap.put(ocf.parentid__c, ocf);
            
        }
        
        if(ocf.determination_date__c!=null){
            determinationMap.put(ocf.parentid__c, ocf);
        }
    
    }
    
    oncologyWorkflow.receiveRecordsFromCoH(determinationMap , planOfCareMap, trigger.isInsert);
   
}