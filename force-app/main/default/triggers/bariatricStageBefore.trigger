trigger bariatricStageBefore on Bariatric_Stage__c (before update) {
    
    map<id, Bariatric_Stage__c> bAuthMap = new map<id, Bariatric_Stage__c>();
    Map<ID,Schema.RecordTypeInfo> rt_Map = Bariatric_Stage__c.sObjectType.getDescribe().getRecordTypeInfosById();
    
    Bariatric_Stage__c[] bsList = new Bariatric_Stage__c[]{};
    map<id, Bariatric__c> bariatricMap = new map<id, Bariatric__c>();
      
    set<id> sendCarrierEmail = new set<id>();
    set<id> sendCarrierDischargeNotice = new set<id>();
    //set<id> sendNotificationOfSecheduledSurgery = new set<id>();
    map<id, Bariatric_Stage__c> sendNotificationOfSecheduledSurgery = new map<id, Bariatric_Stage__c>();
    set<id> followUpRecords = new set<id>();
    
    map<id, Bariatric_Stage__c> bariatricRelatedValues = new map<id,Bariatric_Stage__c> ([select id, bariatric__r.coverage_level__c, bariatric__r.referred_facility__c, procedure__c, recordtype.name from Bariatric_Stage__c where id IN: trigger.new]);
    set<id> syncProcedureDate = new set<id>();
    
    bariatric__c[] bariatricStatusUpdate = new bariatric__c[]{};
    
    for(Bariatric_Stage__c bs : trigger.new){
       
       bs.referred_facility__c = bariatricRelatedValues.get(bs.id).bariatric__r.referred_facility__c;
       
       string rt = rt_map.get(bs.recordTypeid).getName();
       
       //Pre Stage Records
       
       if(rt.containsIgnoreCase('pre')){
           
           if(bs.Auth_Number_Emailed__c==null){
            
               if(bs.Plan_of_Care_Accepted_at_HDP__c != null){
                   bAuthMap.put(bs.bariatric__c, bs);
                   bs.Auth_Number_Emailed__c=date.today(); 
               }
            
           }
           
           if(bs.actual_departure__c != null && (bs.actual_departure__c != trigger.oldMap.get(bs.id).actual_departure__c)){
               bsList.add(bs);
               bs.createHP__c = true;
           }
           
           if(bs.Evaluation_Date__c != trigger.oldMap.get(bs.id).Evaluation_Date__c){
               
               Bariatric__c bariatric = getBariatric(bs.bariatric__c);
               bariatric.Evaluation_Date__c = bs.Evaluation_Date__c;
               bariatricMap.put(bs.bariatric__c, bariatric);
           }
           
           if(bs.patientPaymentCarrierEmail__c == null && 
              bs.Patient_Accepted_for_Evaluation_Visit__c == 'Accepted' && trigger.oldMap.get(bs.id).Patient_Accepted_for_Evaluation_Visit__c != 'Accepted'){ 
               
              sendCarrierEmail.add(bs.id);
              bs.patientPaymentCarrierEmail__c = date.today();
                
           }
           
           //Set the patient responsibilty
           //bs.Patient_Responsibility__c = wlsPatientResponsiblity.PreResponsibilityAmount(wlsPatientResponsiblity.deductibleRemaining(bs.annual_deductible__c, bs.deductible_met__c), wlsPatientResponsiblity.oopRemaining(bariatricRelatedValues.get(bs.id).bariatric__r.coverage_level__c, bs.OOP_Remaining_Individual__c, bs.OOP_Remaining_Family__c), bs.referred_facility__c);
               
       }
       
       //Global Records
       
       if(rt.containsIgnoreCase('global')){
           
           if(bs.Auth_Number_Emailed__c==null && bs.displayAuthNumber__c){
               bAuthMap.put(bs.bariatric__c, bs);
               bs.Auth_Number_Emailed__c=date.today(); 
           
           }
           
           if(bs.actual_departure__c != null && (bs.actual_departure__c != trigger.oldMap.get(bs.id).actual_departure__c)){
               bs.createHP__c = true;
               sendCarrierDischargeNotice.add(bs.id);
               followUpRecords.add(bs.id);
           }
           
           if(bs.Estimated_Procedure__c != trigger.oldMap.get(bs.id).Estimated_Procedure__c ){
               Bariatric__c bariatric = getBariatric(bs.bariatric__c);
               bariatric.Estimated_Procedure__c = bs.Estimated_Procedure__c ;
               bariatricMap.put(bs.bariatric__c, bariatric);
           }
            
           if(bs.Proposed_Procedure__c != trigger.oldMap.get(bs.id).Proposed_Procedure__c){
               Bariatric__c bariatric = getBariatric(bs.bariatric__c);
               bariatric.Proposed_Procedure__c = bs.Proposed_Procedure__c;
               bariatricMap.put(bs.bariatric__c, bariatric);
           }
           
           if(bs.Surgery_Date__c != trigger.oldMap.get(bs.id).Surgery_Date__c){
               
               Bariatric__c bariatric = getBariatric(bs.bariatric__c);
               bariatric.Surgery_Date__c= bs.Surgery_Date__c;
               bariatricMap.put(bs.bariatric__c, bariatric);
           }
           
           if(bs.patientPaymentCarrierEmail__c == null && 
              bs.Patient_Accepted_for_Weight_Loss_Surgery__c == 'Accepted' && trigger.oldMap.get(bs.id).Patient_Accepted_for_Weight_Loss_Surgery__c != 'Accepted'){
              
              sendCarrierEmail.add(bs.id);
              bs.patientPaymentCarrierEmail__c = date.today();
           
           }
           
           if(bs.Procedure__c != trigger.oldmap.get(bs.id).procedure__c){
               syncProcedureDate.add(bs.bariatric__c);
           }
           /*
           system.debug(bs.Notification_of_Scheduled_Surgery__c);
           system.debug(bs.Plan_of_Care_Accepted_at_HDP__c );
           system.debug(bs.Estimated_Arrival__c  );
           system.debug(bs.Estimated_Hospital_Admit__c  );
           system.debug(bs.Estimated_Procedure__c );
           system.debug(bs.Estimated_Hospital_Discharge__c );
           system.debug(bs.Estimated_Cleared_to_Travel__c );
           system.debug(bs.Estimated_Departure__c );
           */
           if(bs.Notification_of_Scheduled_Surgery__c == null && 
              bs.Plan_of_Care_Accepted_at_HDP__c != null &&
              bs.Estimated_Arrival__c  != null &&
              bs.Estimated_Hospital_Admit__c  != null &&
              bs.Estimated_Procedure__c  != null &&
              bs.Estimated_Hospital_Discharge__c != null &&
              bs.Estimated_Cleared_to_Travel__c != null &&
              bs.Estimated_Departure__c != null){ 
              
              //sendNotificationOfSecheduledSurgery.add(bs.id); 
              sendNotificationOfSecheduledSurgery.put(bs.id, bs);  
              bs.Notification_of_Scheduled_Surgery__c = date.today();
           }
           
           //Set the patient responsibilty
           //bs.Patient_Responsibility__c = wlsPatientResponsiblity.GlobalResponsibilityAmount(wlsPatientResponsiblity.DeductibleRemaining(bs.annual_deductible__c, bs.deductible_met__c), wlsPatientResponsiblity.oopRemaining(bariatricRelatedValues.get(bs.id).bariatric__r.coverage_level__c, bs.OOP_Remaining_Individual__c, bs.OOP_Remaining_Family__c ), bs.referred_facility__c, bs.Proposed_Procedure__c, bs.Proposed_Clinical_Code__c);
       }
       
       //Post Records
       
       if(rt.containsIgnoreCase('post')){
           
           if(bs.actual_departure__c != null && (bs.actual_departure__c != trigger.oldMap.get(bs.id).actual_departure__c)){
               bsList.add(bs);
               bs.createHP__c = true;
           }
           
       }
       
       addTobariatricStatusUpdate(bs, rt); 
    } 
    
    if(bAuthMap.keyset().size()>0){
        
        map<id, string> errorMap = BariatricWorkflow.sendAuthEmail(bAuthMap);
        for(id i : errorMap.keySet()){
            trigger.newmap.get(i).Auth_Number_Emailed__c=null;
            
        }
        
    }  
    
    if(sendCarrierEmail.size()>0){
        string ppeResult = BariatricWorkflow.sendCarrierEmail(null, sendCarrierEmail);
        
        if(ppeResult != null){
            
            for(Bariatric_Stage__c bs : trigger.new){
                if(sendCarrierEmail.contains(bs.id)){
                    bs.patientPaymentCarrierEmail__c = null;
                    bs.addError(ppeResult);
                }    
            }
            
        }
        
    }
    
    if(sendNotificationOfSecheduledSurgery.keyset().size()>0){
        map<id, string> result = BariatricWorkflow.sendNotificationOfSecheduledSurgery(sendNotificationOfSecheduledSurgery);
        
        if(result.keySet().size() > 0){
            for(Bariatric_Stage__c bs : trigger.new){
                string foo = result.get(bs.id);
                if(foo != null){
                    bs.Notification_of_Scheduled_Surgery__c = null;
                    system.debug(foo);
                    bs.addError(foo);
                }    
            }   
        }
    
    }
    
    if(bsList.size()>0){
        bariatricWorkflow.createPreClaim(bsList);
    }
    
    bariatricWorkflow.createFollowUpRecord(followUpRecords); 
    
    if(sendCarrierDischargeNotice.size()>0){
        bariatricFuture.sendCarrierDischargeNotice(sendCarrierDischargeNotice);
    }
    
    if(bariatricMap.keySet().size()>0){
        
        Bariatric__c[] bariatricList = new Bariatric__c[]{};
        for(id b : bariatricMap.keyset()){
            bariatricList.add(bariatricMap.get(b));
        }
        update bariatricList;
    }
    
    Bariatric__c getBariatric(id theID){
        
        Bariatric__c b= bariatricMap.get(theID);
        if(b==null){
           b= new Bariatric__c(id=theID);
        }   
           
        return b;
    }
    
    if(!System.isFuture() && syncProcedureDate.size()>0){
        bariatricFuture.syncPostProcedureDate(syncProcedureDate);
    }
    
    if(bariatricStatusUpdate.size()>0){
        update bariatricStatusUpdate;
    }
    
    void addTobariatricStatusUpdate(bariatric_stage__c bs, string rt){
        
        if(bs.Status__c == 'Closed' || bs.Status__c == 'Completed'){
            return;
            
        }
        
        if(rt.containsIgnoreCase('global')){
            
            for(Bariatric__c b : bariatricStatusUpdate){
                if(bs.bariatric__c == b.id){
                    b =  BariatricWorkflow.autoStatus(null, bs, rt);
                    return;
                }    
            }
            
            bariatricStatusUpdate.add(BariatricWorkflow.autoStatus(null, bs, rt));
            
        }else if(rt.containsIgnoreCase('post')){
            
            for(Bariatric__c b : bariatricStatusUpdate){
                if(bs.bariatric__c == b.id){
                    b =  BariatricWorkflow.autoStatus(null, bs, rt);
                    return;
                }    
            }
            
            bariatricStatusUpdate.add(BariatricWorkflow.autoStatus(null, bs, rt));
        
        }else{
            
            for(Bariatric__c b : bariatricStatusUpdate){
                if(bs.bariatric__c == b.id){
                    return;
                }    
            }
            
            bariatricStatusUpdate.add(BariatricWorkflow.autoStatus(null, bs, rt));    
        
        }    
        
        
    }
    
}