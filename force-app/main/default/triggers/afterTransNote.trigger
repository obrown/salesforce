trigger afterTransNote on Transplant_Notes__c (after insert) {
set<id> theids = new set<id>();
set<id> thefaxids = new set<id>();
//Patient_Last_Contact__c
    for(Transplant_Notes__c tn: trigger.new){
        
        if(tn.Contact_Type__c=='Mayo' && tn.Type__c =='Fax'){
        
            thefaxids.add(tn.Transplant__c);
        }
        
        if(tn.Contact_Type__c=='Patient' || tn.Contact_Type__c=='Caregiver'){
        
            if(tn.Type__c =='Call'){
                theids.add(tn.Transplant__c);
            }
            
            if(tn.Initiated__c == 'Inbound' && (tn.Type__c =='Voicemail' || tn.Type__c =='Email' || tn.Type__c =='Fax')){
                theids.add(tn.Transplant__c);
            }
            
        }
    }

    if(theids.size()>0 || thefaxids.size()>0){
        Transplant__c[] tlist = new Transplant__c[]{};
        Task[] tlistTask = new Task[]{};
        Task[] tlistTask1 = new Task[]{};
        list<messaging.Singleemailmessage> finalMail = new list<messaging.Singleemailmessage>();
        
        Group gp = [SELECT (select userOrGroupId from groupMembers) FROM group WHERE name = 'Transplant'];
        user[] ulist = new user[]{};
        set<id> uset = new set<id>();
    
        for(GroupMember g: [select userOrGroupId from GroupMember where groupid = :gp.id]){
            uset.add(g.userOrGroupId);
        }
        ulist = [select id from User where isActive = true and id IN :uset];
        
        for(Transplant__c t: [select Patient_first_name__c, Patient_last_name__c, Patient_Last_Contact__c from Transplant__c where id in :theids or id in :thefaxids]){
            
            if(thefaxids.contains(t.id)){
            Messaging.SingleEmailMessage mail = utilities.email('WMTRNSPCLINICAL@hdplus.com',null,'mmartin@hdplus.com',userinfo.getName(),'A new fax from Mayo has been attached for '+ t.Patient_first_name__c + ' ' + t.Patient_last_name__c + '\'s case','Hello,\n\nA new fax from Mayo has been attached to ' + t.Patient_first_name__c + ' ' + t.Patient_last_name__c + '\'s Transplant Case.\n\nYou can view the case by clicking this link: ' + url.getSalesforceBaseUrl().toExternalForm() + '/apex/transDetail?id=' + t.id + '\n\nThank You,\n\n' + Userinfo.getFirstName() + ' ' + Userinfo.getLastName()); 
            finalMail.add(mail);
                     
                for(User u : ulist){
                    Task task = new Task(
                    ownerid=u.id,
                    whatid=t.id,
                    Subject= t.Patient_last_name__c + ', ' + t.Patient_first_name__c + ' - Notify Carrier of Mayo Clinical Update');
                    tlistTask.add(task);
                    
                    Task task1 = new Task(
                    ownerid=u.id,
                    whatid=t.id,
                    Subject= t.Patient_last_name__c + ', ' + t.Patient_first_name__c + ' - Notify Walmart of Mayo Clinical Update');
                    tlistTask1.add(task1);
                }
            
            }else{
                t.Patient_Last_Contact__c = datetime.now();
                tlist.add(t);
            }
            
        }
        if(tlist.size()>0)
        update tlist;
        
        if(tlistTask.size()>0)
        insert tlistTask;
        
        if(tlistTask1.size()>0)
        insert tlistTask1;
        
        if(finalMail.size()>0)
        messaging.sendEmail(finalmail);
    }
    
    

}