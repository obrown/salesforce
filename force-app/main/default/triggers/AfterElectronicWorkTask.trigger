trigger AfterElectronicWorkTask on Operations_Work_Task__c (after insert, after update) {
    
    map<id, datetime> datetimeMap = new map<id, datetime>();
    map<id, datetime> createdDatetimeMap = new map<id, datetime>();
    set<id> setToWaitingSet = new set<id>();
    set<id> updateImagePacSet = new set<id>();
    set<id> updateDependentTaskDemoInfo = new set<id>();
    set<id> pullDemoUpdateFromParentTask = new set<id>();
    set<id> calcNonBusinessDays = new set<id>();
    set<id> EWTworktaskstatus= new set<id>();  
        
    string depstatus;    
    for(Operations_Work_Task__c owt : trigger.new){
  
 //2-12-20 new code checking for trigger type then take action
        if(trigger.isInsert){
            EWTworktaskstatus.add(owt.id );
       
        }else{
            if(Trigger.oldMap.get(owt.Id).EWT_Work_Task_Status__c!= owt.EWT_Work_Task_Status__c) {

                System.debug('--*status is changed*--');
                System.debug('**Old status :'+Trigger.oldMap.get(owt.Id).EWT_Work_Task_Status__c);
                System.debug('**New status :'+owt.EWT_Work_Task_Status__c);
                EWTworktaskstatus.add(owt.id );

            }
       
       }
       
       if(owt.Hp_document_type__c != '' && owt.Hp_document_type__c != 'ND'){
            updateImagePacSet.add(owt.id);
       }
        
        if(trigger.isUpdate && owt.EESSN__c != null && trigger.oldMap.get(owt.id).EESSN__c == null ||
           trigger.isUpdate && owt.Related_Claim_Number__c != null && trigger.oldMap.get(owt.id).Related_Claim_Number__c == null){
            if(owt.owt_parent__c==null){
                updateDependentTaskDemoInfo.add(owt.id);
            }
            
        }
        
        if(owt.owt_parent__c!=null){
           if(trigger.isinsert || trigger.isupdate){
              pullDemoUpdateFromParentTask.add(owt.id);
              
          }
          
        }
        
        if(owt.owt_parent__c!=null && owt.dependent_task__c){  
          depstatus=owt.Work_Task_status__c;
          
          if(trigger.isinsert){
              pullDemoUpdateFromParentTask.add(owt.id);
          }
          if(owt.closed_date__c!=null){
              calcNonBusinessDays.add(owt.id);
          }
         
            
            datetime foo = createdDatetimeMap.get(owt.owt_parent__c);
            if(foo==null || foo < owt.createdDate){ 
              createdDatetimeMap.put(owt.owt_parent__c, owt.createdDate);
            }
          
            if(owt.Work_Task_status__c=='Open' || owt.Work_Task_status__c=='Waiting'||owt.Work_Task_status__c=='Closed'){
           
                setToWaitingSet.add(owt.owt_parent__c);
                system.debug('10-31-18 record in set to waiting set '+setToWaitingSet);
            }
        
        }else{
          //TODO: Delete 
          system.debug('owt.closed_date__c '+owt.closed_date__c);
          if(owt.closed_date__c!=null){
              calcNonBusinessDays.add(owt.id);
          }
          
          if((trigger.isinsert && owt.closed_date__c!=null) || (trigger.isUpdate && owt.closed_date__c!= trigger.oldMap.get(owt.id).closed_date__c)){
              calcNonBusinessDays.add(owt.id);
                
          }
        }

    }
    
        
    
    if(!datetimeMap.keyset().isEmpty()){
        
        Operations_Work_Task__c[] oList = [select Sub_Task_Max_Close_Date__c,Closed_Date__c  from Operations_Work_Task__c where id in :datetimeMap.keySet() order by owt_parent__c asc];
        Operations_Work_Task__c[] oUpdateList = new Operations_Work_Task__c[]{};
        for(Operations_Work_Task__c o : oList){
            datetime foo = datetimeMap.get(o.id);
            if(o.Sub_Task_Max_Close_Date__c ==null || 
               (o.Sub_Task_Max_Close_Date__c < foo)
               
            ){
                o.Sub_Task_Max_Close_Date__c = foo;
                oUpdateList.add(o);
            }
        
        }
        
        update oUpdateList;
        
                
    }

    if(!createdDatetimeMap.keyset().isEmpty()){
        
        Operations_Work_Task__c[] oList = [select Sub_Task_Min_Created_Date__c,createdDate  from Operations_Work_Task__c where id in :createddatetimeMap.keySet() order by owt_parent__c asc];
        Operations_Work_Task__c[] oUpdateList = new Operations_Work_Task__c[]{};
        for(Operations_Work_Task__c o : oList){
            datetime foo = createddatetimeMap.get(o.id);
            if(o.Sub_Task_Min_Created_Date__c == null ||foo < o.Sub_Task_Min_Created_Date__c)
            {
                o.Sub_Task_Min_Created_Date__c = foo;
                oUpdateList.add(o);
            }
        
        }
        
        update oUpdateList;
        
                
    }


    if(setToWaitingSet!=null){
        Operations_Work_Task__c[] oList = [select closed_date__c, Work_Task_status__c from Operations_Work_Task__c where id in :setToWaitingSet order by owt_parent__c asc];
        
        for(Operations_Work_Task__c o : oList){
            
            if(depstatus== 'Waiting'||depstatus== 'Open'){
                o.Work_Task_status__c = 'Waiting';
                o.closed_date__c = null;
            }
        
            if(depstatus== 'Closed'){
              o.Work_Task_status__c = 'Open';
            } 
        
        }
        
        update olist;
        
    }
    
    if(!system.isFuture()){
        if(!updateImagePacSet.isempty()){
            owtFuture.sendImagePacFileRoute(updateImagePacSet);
       
        }
    
        if(!updateDependentTaskDemoInfo.isEmpty()){
            owtFuture.updateDependentTaskDemoInfo(updateDependentTaskDemoInfo);
        }
    
        if(!pullDemoUpdateFromParentTask.isEmpty()){
           owtFuture.DemoUpdateFromParentTask(pullDemoUpdateFromParentTask);
        }

        if(!calcNonBusinessDays.isEmpty()){
            owtFuture.calcNonBusinessDays(calcNonBusinessDays);
        } 
     
        if(!EWTworktaskstatus.isEmpty()){
            owtFuture.EWTworktaskstatus(EWTworktaskstatus);
        }       
    }
    
}