trigger beforeTransplant on Transplant__c (before insert, before update, after update) {

map<id,Stage__c> stageMap = new map<id,Stage__c>();
list<Transplant_Notes__c> notesList = new list<Transplant_Notes__c>(); 
boolean stageFoo=false;
list<Stage__c> newStageList = new list<Stage__c>();
list<messaging.Singleemailmessage> finalMail = new list<messaging.Singleemailmessage>();   

    if(trigger.isBefore){    
        for(Transplant__c t: trigger.new){

        if(trigger.isInsert){
                t.Primary_Case_Manager__c = 'Carrier';
                 t.Intake_Status_Reason__c = 'Intake Incomplete';

            if(t.bid__c != null){  
                formatID nf = new formatID();   
                t.certID__c = nf.formatID(t.bid__c,'Walmarttransplant' + t.Carrier_Name__c);

            }
            
        }else{
        
        if(t.transplant_type__c!=null){
            if(t.transplant_type__c.contains('Bone Marrow')){
                t.transplant_type__c='Bone Marrow';
            }
        }
        
        if(t.Patient_Last_Name__c!=null){
           t.Patient_Last_Name_Sort__c=t.Patient_Last_Name__c.left(1);
        }
                    
        if(t.Employee_Last_Name__c!=null){
           t.Employee_Last_Name_Sort__c = t.Employee_Last_Name__c.left(1);
        }
            
        if(t.Relationship_to_the_Insured__c == 'Patient is Insured'){
                            
        if(t.Employee_Street__c != null){
            t.Patient_City__c = t.Employee_City__c;
            t.Patient_Country__c = t.Employee_Country__c;
            t.Patient_Street__c = t.Employee_Street__c;
            t.Patient_State__c = t.Employee_State__c;
            t.Patient_Zip_Postal_Code__c = t.Employee_Zip_Postal_Code__c;
        }
                            
        if(t.Employee_First_Name__c != null){
            t.Patient_First_Name__c = t.Employee_First_Name__c;
        }

        if(t.Employee_Last_Name__c != null){
            t.Patient_Last_Name__c = t.Employee_Last_Name__c;
        }

        if(t.Employee_Gender__c != null){
            t.Patient_Gender__c = t.Employee_Gender__c;
        }

        if(t.Employee_DOBe__c != null){
            t.Patient_DOBe__c = t.Employee_DOBe__c;
        }

        if(t.Employee_SSN__c != null){
            t.Patient_SSN__c = t.Employee_SSN__c;
        }
                            
        }           
                    
            if(t.bid__c != null){  
                formatID nf = new formatID();   
                t.certID__c = nf.formatID(t.bid__c,'Walmarttransplant' + t.Carrier_Name__c);

            }

            if(t.Relationship_to_the_Insured__c == 'Patient is Insured'){
                            
            if(t.Employee_First_Name__c != null){               
                t.Patient_First_Name__c = t.Employee_First_Name__c;
            }

            if(t.Patient_First_Name__c != null && t.Employee_First_Name__c ==null){               
                t.Employee_First_Name__c = t.Patient_First_Name__c;
            }

            if(t.Employee_Last_Name__c != null){
               t.Patient_Last_Name__c = t.Employee_Last_Name__c;
            }

            if(t.Patient_Last_Name__c != null && t.Employee_Last_Name__c ==null){               
                t.Employee_Last_Name__c = t.Patient_Last_Name__c;
            }

            if(t.Employee_Gender__c != null){
               t.Patient_Gender__c = t.Employee_Gender__c;
            }

            if(t.Patient_Gender__c != null && t.Employee_Gender__c == null){
               t.Employee_Gender__c = t.Patient_Gender__c;
            }

            if(t.Employee_DOBe__c != null){
               t.Patient_DOBe__c = t.Employee_DOBe__c;
            }

            if(t.Patient_DOBe__c != null && t.Employee_DOBe__c == null){
               t.Employee_DOBe__c = t.Patient_DOBe__c;
            }

            if(t.Employee_SSN__c != null){
              t.Patient_SSN__c = t.Employee_SSN__c;
            }

            if(t.Patient_SSN__c != null && t.Employee_SSN__c == null){
              t.Employee_SSN__c = t.Patient_SSN__c;
            }
                            
            }
            
            if(t.Same_as_Employee_Address__c==true){
                if(t.Patient_Street__c==null){
                    t.Patient_City__c = t.Employee_City__c;                   
                    t.Patient_Country__c = t.Employee_Country__c;
                    t.Patient_Street__c = t.Employee_Street__c;
                    t.Patient_State__c = t.Employee_State__c;
                    t.Patient_Zip_Postal_Code__c = t.Employee_Zip_Postal_Code__c;
                }else{
                    t.Employee_City__c = t.Patient_City__c;                   
                    t.Employee_Country__c = t.Patient_Country__c;
                    t.Employee_Street__c = t.Patient_Street__c;
                    t.Employee_State__c = t.Patient_State__c ;
                    t.Employee_Zip_Postal_Code__c = t.Patient_Zip_Postal_Code__c;                   
                }            
            }
            
               if(t.transplant_type__c != null && (t.transplant_type__c != trigger.oldmap.get(t.id).transplant_type__c)){

                //Reset values for multiple transplant types changing to multiple organs
                //Transfers the Transplant Dates for the orignal organ to a related list of Transplant dates
                //since mulitple organs use a related list to track Transplant Dates                
                if(t.transplant_type__c.contains(';') && (trigger.oldmap.get(t.id).transplant_type__c != null && !trigger.oldmap.get(t.id).transplant_type__c.contains(';'))){
                    
                    if(t.transplant_date__c!=null || t.Transplant_Admission_Date__c != null || t.Transplant_Discharge_Date__c!=null){
                    Stage__c newTransDate = new Stage__c();
                     newTransDate.isTransAdmit__c=true;
                      newTransDate.Transplant__c=t.id;
                       newTransDate.Transplant_Date__c=t.transplant_date__c;
                        newTransDate.Transplant_Admission_Date__c=t.Transplant_Admission_Date__c;
                         newTransDate.Transplant_Discharge_Date__c=t.Transplant_Discharge_Date__c;
                          newTransDate.Type__c = trigger.oldmap.get(t.id).transplant_type__c;
                           newStageList.add(newTransDate);
                            t.transplant_date__c=null;
                             t.Transplant_Admission_Date__c=null;
                              t.Transplant_Discharge_Date__c=null;
                    }        
                }
                
                //Reverse of above, take related list value and move to the Transplant record
                /*
                if(t.transplant_type__c.contains(';') && !trigger.oldmap.get(t.id).transplant_type__c.contains(';')){
                    
                    if(t.transplant_date__c!=null || t.Transplant_Admission_Date__c != null || t.Transplant_Discharge_Date__c!=null){
                        
                    Stage__c newTransDate = new Stage__c();
                     newTransDate.isTransAdmit__c=true;
                      newTransDate.Transplant__c=t.id;
                       newTransDate.Transplant_Date__c=t.transplant_date__c;
                        newTransDate.Transplant_Admission_Date__c=t.Transplant_Admission_Date__c;
                         newTransDate.Transplant_Discharge_Date__c=t.Transplant_Discharge_Date__c;
                          newTransDate.Type_global__c = trigger.oldmap.get(t.id).transplant_type__c;
                           newStageList.add(newTransDate);
                            t.transplant_date__c=null;
                             t.Transplant_Admission_Date__c=null;
                              t.Transplant_Discharge_Date__c=null;
                    }        
                } */            
                
            }               
                    
            if(t.Patient_Last_Name__c!=null){
               t.Patient_Last_Name_Sort__c=t.Patient_Last_Name__c.left(1);
            }
                    
            if(t.Employee_Last_Name__c!=null){
               t.Employee_Last_Name_Sort__c = t.Employee_Last_Name__c.left(1);
            }
            
            if(!test.isrunningtest() && trigger.size<3){
            
              utilities ute = new utilities();
            stageFoo = false;
            Date minpreeff = ute.minEffectiveDate(t.id,'pre',t.id);
            Date minglobaleff = ute.minEffectiveDate(t.id,'global',t.id);
            Date maxtermglobal = ute.maxTermDate(t.id, 'global', t.id);
            
            Date minposteff = ute.minEffectiveDate(t.id,'post',t.id);
            Date maxpostterm = ute.maxTermDate(t.id,'post',t.id);
              Date theDay = date.today();
            
            if(theDay >= minpreeff && minpreeff == null||(theDay >= minpreeff && theDay >= minpreeff)){
                stageFoo = true;
                t.Stage__c ='Pre';
                if(t.Intake_Status__c == 'Open'){
                    t.Intake_Status_Reason__c = 'Pre-Transplant';
                }
            }
            
            if(theDay <= maxtermglobal && theDay >= minglobaleff){
                stageFoo = true;
                t.Stage__c ='Global';
                if(t.Intake_Status__c == 'Open'){
                    t.Intake_Status_Reason__c = 'Transplant Global';
                }
            }
            
            //if(date.today() <= maxpostterm && date.today() >= minposteff){
            if(theDay >= minposteff){
                stageFoo = true;
                t.Stage__c ='Post';
                if(t.Intake_Status__c == 'Open'){
                    t.Intake_Status_Reason__c = 'Post-Transplant';
                }
            }
            
            if(stageFoo == false){
                t.stage__c = 'Undetermined';
                
            }

                t.Stage_Type_Post__c =null;
                 t.Stage_Termination_Date_post__c =null;
                  t.Stage_Effective_Date_post__c =null;
                
                t.Stage_Type_Global__c =null;
                 t.Stage_Termination_Date_Global__c =null;
                  t.Stage_Effective_Date_Global__c =null;
                
                t.Stage_Type_pre__c =null;
                 t.Stage_Termination_Date_pre__c =null;
                  t.Stage_Effective_Date_pre__c =null; 
                  
            }
            
            /*eligibility checks*/
            
                if(t.Eligibility_Verification_Received__c == null){
                    if(t.Employee_First_Name_verified__c ==true && t.Employee_Last_Name_verified__c ==true && t.Patient_First_Name_verified__c ==true && t.Patient_Last_Name_verified__c ==true && t.BID_verified__c ==true && t.Carrier_verified__c==true && t.Plan_verified__c==true && t.Employee_SSN_verified__c==true && t.Employee_DOB_verified__c==true && t.Patient_SSN_verified__c==true && t.Patient_SSN_verified__c==true){
                        t.Eligibility_Verification_Received__c = date.today();
                        
                                      Transplant_Notes__c tempNote = new Transplant_Notes__c(
                        Transplant__c=t.id,
                        Type__c='Note',
                        Contact_Type__c='N/A',
                        Initiated__c='System',
                        Subject__c='Eligibility information verified',
                        Notes__c='BID has been verified\nCarrier Name and Employee Health Plan Name have been verified\nEmployee Name, SSN and DOB have been verified\nPatient Name, DOB and SSN have been verified\n');
                    
                    notesList.add(tempNote);
                    }
                }
            
            /*end eligibility checks*/
       
    }
    
    }
    
    }
    
    //************************************           Begin After           *******************************************************
    if(trigger.isAfter){
                string paddtlcontact='';
                string eaddtlcontact='';
                string carrierinfo='';
                string status='';
        for(Transplant__c t: trigger.new){
               
                paddtlcontact='';
                eaddtlcontact='';
                carrierinfo='';
                status='';
        
                
            /*
            if(t.All_Medical_Records_Received__c != null && (t.All_Medical_Records_Received__c != trigger.oldmap.get(t.id).All_Medical_Records_Received__c)){
                
                 Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                 mail.setReplyTo(string.valueof(t.owner.email));
                 list<string> sendemailto = new list<string>();
                 sendemailto.add(t.Referral_facility_contact__c);
                 mail.setToAddresses(sendemailto);
                 mail.setSenderDisplayName(string.valueof(t.owner.name));
                 mail.setSubject('Walmart Transplant - ' + t.transplant_type__c + ' - ' + t.patient_last_name__c + ', ' + t.patient_first_name__c + ' - ' + date.today().month() + '/' + date.today().day() + '/' + date.today().year());
                 mail.setPlainTextBody('Please note that all known medical records for this patient have been sent.\n\nThank you,\n\nHealth Design Plus');
                 finalMail.add(mail);
                
            }
            */
            
            if(t.Referred_Facility__c != null && ((t.Referred_Facility__c != trigger.oldmap.get(t.id).Referred_Facility__c) && trigger.oldmap.get(t.id).Referred_Facility__c!=null)){
                
                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Referred Facility Change',
                    Notes__c='Referred Facility changed from '+ trigger.oldmap.get(t.id).Referred_Facility__c +' to '+ t.Referred_Facility__c);
                    
                    notesList.add(tempNote);
            
                
            }           
            
            //Document Status Changes
            
            if(t.Intake_Status__c != null && ((t.Intake_Status__c != trigger.oldmap.get(t.id).Intake_Status__c) && trigger.oldmap.get(t.id).Intake_Status__c!=null)){
                
                status += 'Status changed from '+ trigger.oldmap.get(t.id).Intake_Status__c +' to '+ t.Intake_Status__c + '\n';
                
            }

            if(t.Intake_Status_Reason__c != null && ((t.Intake_Status_Reason__c != trigger.oldmap.get(t.id).Intake_Status_Reason__c) && trigger.oldmap.get(t.id).Intake_Status_Reason__c!=null)){
                
                status += 'Status Reason changed from '+ trigger.oldmap.get(t.id).Intake_Status_Reason__c +' to '+ t.Intake_Status_Reason__c + '\n';
                
            }

            if(status!=''){
              Transplant_Notes__c tempNote = new Transplant_Notes__c(
               Transplant__c=t.id,
                Type__c='Note',
                 Contact_Type__c='Field Change',
                  Initiated__c='System',
                   Subject__c='Status Change',
                    Notes__c=status);
                    
                    notesList.add(tempNote);
            }
            
            //Document Carrier information changes
            
            if(t.Carrier_Name__c != null && ((t.Carrier_Name__c != trigger.oldmap.get(t.id).Carrier_Name__c) && trigger.oldmap.get(t.id).Carrier_Name__c!=null)){
                
                carrierinfo += 'Carrier Name changed from '+ trigger.oldmap.get(t.id).Carrier_Name__c +' to '+ t.Carrier_Name__c + '\n';
                
            }

            if(t.Carrier_Nurse_Name__c != null && ((t.Carrier_Nurse_Name__c != trigger.oldmap.get(t.id).Carrier_Nurse_Name__c) && trigger.oldmap.get(t.id).Carrier_Nurse_Name__c!=null)){
                
                carrierinfo += 'Carrier Nurse changed from '+ trigger.oldmap.get(t.id).Carrier_Nurse_Name__c +' to '+ t.Carrier_Nurse_Name__c + '\n';
                
            }

            if(t.Carrier_Nurse_Phone__c != null && ((t.Carrier_Nurse_Phone__c != trigger.oldmap.get(t.id).Carrier_Nurse_Phone__c) && trigger.oldmap.get(t.id).Carrier_Nurse_Phone__c!=null)){
                
                carrierinfo += 'Carrier Nurse Phone changed from '+ trigger.oldmap.get(t.id).Carrier_Nurse_Phone__c +' to '+ t.Carrier_Nurse_Phone__c + '\n';
                
            }

            if(t.Carrier_E_mail__c != null && ((t.Carrier_E_mail__c != trigger.oldmap.get(t.id).Carrier_E_mail__c) && trigger.oldmap.get(t.id).Carrier_E_mail__c!=null)){
                
                carrierinfo += 'Carrier E-mail changed from '+ trigger.oldmap.get(t.id).Carrier_E_mail__c +' to '+ t.Carrier_E_mail__c + '\n';
                
            }

            if(carrierinfo!=''){
                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Carrier Info Change',
                    Notes__c=carrierinfo);
                    
                    notesList.add(tempNote);
            }   
            
            if(t.bid__c != null && ((t.bid__c != trigger.oldmap.get(t.id).bid__c) && trigger.oldmap.get(t.id).bid__c!=null)){

                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='BID number changed',
                    Notes__c='BID number changed from '+ trigger.oldmap.get(t.id).bid__c +' to '+ t.bid__c + '\n');
                    
                    notesList.add(tempNote);                
                
                carrierinfo += 'Carrier E-mail changed from '+ trigger.oldmap.get(t.id).Carrier_E_mail__c +' to '+ t.Carrier_E_mail__c + '\n';
                
            }
             
            /* Walmart Eligiblity and Paid Thru Date Changes */
            String edatechange ='';            
            if(t.Walmart_Eligibility_Effective__c != null && (t.Walmart_Eligibility_Effective__c != trigger.oldmap.get(t.id).Walmart_Eligibility_Effective__c)){
                    edatechange +='Walmart Eligibility Effective changed from '+ utilities.formatUSdate(trigger.oldmap.get(t.id).Walmart_Eligibility_Effective__c) + ' to ' + utilities.formatUSdate(t.Walmart_Eligibility_Effective__c)+'\n';
            }
            
            if((t.Walmart_Eligibility_Effective__c != trigger.oldmap.get(t.id).Walmart_Eligibility_Effective__c) && t.Walmart_Eligibility_Effective__c==null){
                    edatechange +='Walmart Eligibility Effective changed from '+ utilities.formatUSdate(trigger.oldmap.get(t.id).Walmart_Eligibility_Effective__c) + ' to null\n';
            }
            
            if(t.Walmart_Eligibility_Termination__c != null && (t.Walmart_Eligibility_Termination__c != trigger.oldmap.get(t.id).Walmart_Eligibility_Termination__c)){
                    edatechange+='Walmart Eligibility Termination changed from '+ utilities.formatUSdate(trigger.oldmap.get(t.id).Walmart_Eligibility_Termination__c) +' to '+ utilities.formatUSdate(t.Walmart_Eligibility_Termination__c)+'\n';
            }
                
            if((t.Walmart_Eligibility_Termination__c != trigger.oldmap.get(t.id).Walmart_Eligibility_Termination__c) && t.Walmart_Eligibility_Termination__c==null){
                    edatechange+='Walmart Eligibility Termination changed from '+ utilities.formatUSdate(trigger.oldmap.get(t.id).Walmart_Eligibility_Termination__c) + ' to null\n';
            }
            
            if(edatechange!=''){
                
                Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Walmart Eligibility Date change',
                    Notes__c=edatechange);
                    notesList.add(tempNote); 
            }

            if(t.Premium_Paid_Thru__c != null && (t.Premium_Paid_Thru__c != trigger.oldmap.get(t.id).Premium_Paid_Thru__c)){
                    
                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Premium Paid Thru Date changed',
                    Notes__c='Premium Paid Thru Date changed from '+ utilities.formatUSdate(trigger.oldmap.get(t.id).Premium_Paid_Thru__c) +' to '+ utilities.formatUSdate(t.Premium_Paid_Thru__c));
                    
                    notesList.add(tempNote);                
                    
                
            }
            
            if((t.Premium_Paid_Thru__c != trigger.oldmap.get(t.id).Premium_Paid_Thru__c) && t.Premium_Paid_Thru__c==null){
                    
                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Premium Paid Thru Date Deleted',
                    Notes__c='Premium Paid Thru changed from '+ utilities.formatUSdate(trigger.oldmap.get(t.id).Premium_Paid_Thru__c) + ' to null');
                    
                    notesList.add(tempNote);                
                
            }
            
                        
            /* END Walmart Eligiblity and Paid Thru Date Changes */
                        
            if(t.Employee_Home_Phone__c != null && ((t.Employee_Home_Phone__c != trigger.oldmap.get(t.id).Employee_Home_Phone__c) && trigger.oldmap.get(t.id).Employee_Home_Phone__c!=null)){
                
                eaddtlcontact += 'Employee Home Phone changed from '+ trigger.oldmap.get(t.id).Employee_Home_Phone__c +' to '+ t.Employee_Home_Phone__c + '\n';
                
            }

            if(t.Employee_Mobile__c != null && ((t.Employee_Mobile__c != trigger.oldmap.get(t.id).Employee_Mobile__c) && trigger.oldmap.get(t.id).Employee_Mobile__c!=null)){
                
                eaddtlcontact += 'Employee Mobile Phone changed from '+ trigger.oldmap.get(t.id).Employee_Mobile__c +' to '+ t.Employee_Mobile__c + '\n';
                
            }

            if(t.Employee_Work_Phone__c != null && ((t.Employee_Work_Phone__c != trigger.oldmap.get(t.id).Employee_Work_Phone__c) && trigger.oldmap.get(t.id).Employee_Work_Phone__c!=null)){
                
                eaddtlcontact += 'Employee Work Phone changed from '+ trigger.oldmap.get(t.id).Employee_Work_Phone__c +' to '+ t.Employee_Work_Phone__c + '\n';
                
            }
                
            if(eaddtlcontact!=''){
                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Employee Phone Change',
                    Notes__c=eaddtlcontact);
                    notesList.add(tempNote);
            }                   

            if(t.Patient_Home_Phone__c != null && ((t.Patient_Home_Phone__c != trigger.oldmap.get(t.id).Patient_Home_Phone__c) && trigger.oldmap.get(t.id).Patient_Home_Phone__c!=null)){
                
                paddtlcontact += 'Patient Home Phone changed from '+ trigger.oldmap.get(t.id).Patient_Home_Phone__c +' to '+ t.Patient_Home_Phone__c + '\n';
                
            }

            if(t.Patient_Mobile__c != null && ((t.Patient_Mobile__c != trigger.oldmap.get(t.id).Patient_Mobile__c) && trigger.oldmap.get(t.id).Patient_Mobile__c!=null)){
                
                paddtlcontact += 'Patient Mobile Phone changed from '+ trigger.oldmap.get(t.id).Patient_Mobile__c +' to '+ t.Patient_Mobile__c + '\n';
                
            }

            if(t.Patient_Work_Phone__c != null && ((t.Patient_Work_Phone__c != trigger.oldmap.get(t.id).Patient_Work_Phone__c) && trigger.oldmap.get(t.id).Patient_Work_Phone__c!=null)){
                
                paddtlcontact += 'Patient Work Phone changed from '+ trigger.oldmap.get(t.id).Patient_Work_Phone__c +' to '+ t.Patient_Work_Phone__c + '\n';
                
            }
                
            if(paddtlcontact!=''){
                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Patient Phone Change',
                    Notes__c=paddtlcontact);
                    notesList.add(tempNote);
            }   
            
            //Begin Employee Address change checks
            
            string eaddybody=''; // body of the note for employee address changes
                        
            if(t.employee_Street__c != null && ((t.employee_Street__c != trigger.oldmap.get(t.id).employee_Street__c) && trigger.oldmap.get(t.id).employee_Street__c!=null)){
                
                eaddybody += 'Employee Street Changed from '+ trigger.oldmap.get(t.id).employee_Street__c +' to '+ t.employee_Street__c + '\n';
                
            }

            if(t.employee_City__c != null && ((t.employee_City__c != trigger.oldmap.get(t.id).employee_City__c) && trigger.oldmap.get(t.id).employee_City__c!=null)){
                
                eaddybody += 'Employee City Changed from '+ trigger.oldmap.get(t.id).employee_City__c +' to '+ t.employee_City__c + '\n';
                
            }
                
            if(t.employee_State__c != null && ((t.employee_State__c != trigger.oldmap.get(t.id).employee_State__c) && trigger.oldmap.get(t.id).employee_State__c!=null)){
                
                eaddybody += 'Employee State Changed from '+ trigger.oldmap.get(t.id).employee_State__c +' to '+ t.employee_State__c + '\n';
                
            }               

            if(t.employee_Zip_Postal_Code__c != null && ((t.employee_Zip_Postal_Code__c != trigger.oldmap.get(t.id).employee_Zip_Postal_Code__c) && trigger.oldmap.get(t.id).employee_Zip_Postal_Code__c!=null)){
                
                eaddybody += 'Employee Zip/Postal Code Changed from '+ trigger.oldmap.get(t.id).employee_Zip_Postal_Code__c +' to '+ t.employee_Zip_Postal_Code__c + '\n';
                
            }
            
            if(eaddybody!=''){
                    
                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                     Transplant__c=t.id,
                      Type__c='Note',
                       Contact_Type__c='Field Change',
                        Initiated__c='System',
                         Subject__c='Employee Address Change',
                          Notes__c=eaddybody);
                    
                    notesList.add(tempNote);
            }               
            
            //End Employee Address change checks
            
            string paddybody=''; // body of the note for patient address changes 
            
            //Begin Patient Address change checks
                               
            if(t.Patient_Street__c != null && ((t.Patient_Street__c != trigger.oldmap.get(t.id).Patient_Street__c) && trigger.oldmap.get(t.id).Patient_Street__c!=null)){
                
                paddybody += 'Patient Street Changed from '+ trigger.oldmap.get(t.id).Patient_Street__c +' to '+ t.Patient_Street__c + '\n';
                
            }

            if(t.Patient_City__c != null && ((t.Patient_City__c != trigger.oldmap.get(t.id).Patient_City__c) && trigger.oldmap.get(t.id).Patient_City__c!=null)){
                
                paddybody += 'Patient City Changed from '+ trigger.oldmap.get(t.id).Patient_City__c +' to '+ t.Patient_City__c + '\n';
                
            }
                
            if(t.Patient_State__c != null && ((t.Patient_State__c != trigger.oldmap.get(t.id).Patient_State__c) && trigger.oldmap.get(t.id).Patient_State__c!=null)){
                
                paddybody += 'Patient State Changed from '+ trigger.oldmap.get(t.id).Patient_State__c +' to '+ t.Patient_State__c + '\n';
                
            }               

            if(t.Patient_Zip_Postal_Code__c != null && ((t.Patient_Zip_Postal_Code__c != trigger.oldmap.get(t.id).Patient_Zip_Postal_Code__c) && trigger.oldmap.get(t.id).Patient_Zip_Postal_Code__c!=null)){
                
                paddybody += 'Patient Zip/Postal Code Changed from '+ trigger.oldmap.get(t.id).Patient_Zip_Postal_Code__c +' to '+ t.Patient_Zip_Postal_Code__c + '\n';
                
            }
            
            if(paddybody!=''){

                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                     Transplant__c=t.id,
                      Type__c='Note',
                       Contact_Type__c='Field Change',
                        Initiated__c='System',
                         Subject__c='Patient Address Change',
                          Notes__c=paddybody);
                    notesList.add(tempNote);
            }               
            
            //End Patient Address change checks
            
            //Note Patient DOB changes, but not for new values
            
            if(t.Patient_DOBe__c != null && ((t.Patient_DOBe__c != trigger.oldmap.get(t.id).Patient_DOBe__c) && trigger.oldmap.get(t.id).Patient_DOBe__c!=null)){
                
                Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Patient DOB Change',
                    Notes__c='Patient DOB Changed from: ' + date.valueOf(trigger.oldMap.get(t.id).Patient_DOBe__c).month() + '/' + date.valueOf(trigger.oldMap.get(t.id).Patient_DOBe__c).day() + '/' + date.valueOf(trigger.oldMap.get(t.id).Patient_DOBe__c).year() 
                                                            + ' to ' + date.valueOf(t.Patient_DOBe__c).month() + '/' + date.valueOf(t.Patient_DOBe__c).day() + '/' + date.valueOf(t.Patient_DOBe__c).year());
                    
                    notesList.add(tempNote);
                
            }
            
            //Note Employee DOB changes, but not for new values
            
            if(t.Employee_DOBe__c != null && ((t.Employee_DOBe__c != trigger.oldmap.get(t.id).Employee_DOBe__c) && trigger.oldmap.get(t.id).Employee_DOBe__c!=null)){
                
                Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Employee DOB Change',
                    Notes__c='Employee DOB Changed from: ' + date.valueOf(trigger.oldMap.get(t.id).Employee_DOBe__c).month() + '/' + date.valueOf(trigger.oldMap.get(t.id).Employee_DOBe__c).day() + '/' + date.valueOf(trigger.oldMap.get(t.id).Employee_DOBe__c).year() 
                                        + ' to ' + date.valueOf(t.Employee_DOBe__c).month() + '/' + date.valueOf(t.Employee_DOBe__c).day() + '/' + date.valueOf(t.Employee_DOBe__c).year());
                    
                    notesList.add(tempNote);
                
            }
            
            //Note Employee SSN changes, but not for new values
            
            if(t.Employee_SSN__c != null && ((t.Employee_SSN__c != trigger.oldmap.get(t.id).Employee_SSN__c) && trigger.oldmap.get(t.id).Employee_SSN__c!=null)){
                
                Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Employee SSN Change',
                    Notes__c='Employee SSN Changed from: ' + trigger.oldMap.get(t.id).Employee_SSN__c + ' to ' + t.Employee_SSN__c);
                    
                    notesList.add(tempNote);
                
            }
            
            //Note Patient SSN changes, but not for new values
            
            if(t.Patient_SSN__c != null && ((t.Patient_SSN__c != trigger.oldmap.get(t.id).Patient_SSN__c) && trigger.oldmap.get(t.id).Patient_SSN__c!=null)){
                
                Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Patient SSN Change',
                    Notes__c='Patient SSN Changed from: ' + trigger.oldMap.get(t.id).Patient_SSN__c + ' to ' + t.Patient_SSN__c);
                    
                    notesList.add(tempNote);
                
            }
            
            //Begin Stage setup for Bone Marrow Transplant types
            
            //Based on the transplant start date not null, and has changed from the prior value
             
            if((t.transplant_start__c!=null  && t.transplant_type__c=='Bone Marrow') && (t.transplant_start__c != trigger.oldmap.get(t.id).transplant_start__c)){
             Date transplantDate = t.transplant_start__c;
             Date StageTERM;   
                list<Stage__c> stagecheck = new list<Stage__c>([select Stage__c, StageTerm__c, StageEff__c from Stage__c where Transplant__c=:t.id and Stage__c = 'Global' order by StageTerm__c]); 
                //A global stage already exists, so we will update it.
                if(stagecheck.size()>0){

                    stageCheck[0].StageEff__c = t.transplant_start__c.addDays(-10);
                    
                    if(t.bmtmodal__c=='Auto'){
                        stageCheck[0].StageTerm__c = stageCheck[0].StageEff__c.addDays(49);
                        StageTERM =stageCheck[0].StageTerm__c ;
                    }else if(t.bmtmodal__c=='Allo'){
                            stageCheck[0].StageTerm__c = stageCheck[0].StageEff__c.addDays(99);
                            StageTERM =stageCheck[0].StageTerm__c ;
                    }
                    update stagecheck;                  
                    
                }else{
                //A global stage does not exists, so we will create it.
                    Stage__c globalStage = new Stage__c();
                    globalStage.StageEff__c = t.transplant_start__c.addDays(-10);
                    
                    if(t.bmtmodal__c=='Auto'){
                            globalStage.StageTerm__c = globalStage.StageEff__c.addDays(49);
                            StageTERM =globalStage.StageTerm__c ;
                        }else if(t.bmtmodal__c=='Allo'){
                            globalStage.StageTerm__c = globalStage.StageEff__c.addDays(99);
                            StageTERM =globalStage.StageTerm__c ;
                    }
                    
                    globalStage.Stage__c = 'Global';
                     globalStage.Transplant__c = t.id;
                      globalStage.type__c = 'Bone Marrow';
                    
                    insert globalStage; 
                
                }
                
                list<Stage__c> prolist = new list<Stage__c>([select StageEff__c,StageTerm__c,Stage__c from Stage__c where Transplant__c=:t.id and Stage__c = 'Post' order by StageTerm__c ]);
                        
                        //A post stage already exists, so we will update it.
                        if(prolist.size()>0){

                            prolist[0].StageEff__c =StageTERM.addDays(1);
                            prolist[0].StageTerm__c =transplantDate.addyears(1);
                            prolist[0].StageTerm__c =  prolist[0].StageTerm__c.adddays(1);
                            update prolist;
                            
                        }else{
                            //A post stage does not exists, so we will create it
                            Stage__c postStage = new Stage__c();
                            
                              postStage.StageEff__c =StageTERM.addDays(1);
                                postStage.StageTerm__c =transplantDate.addyears(1);
                                postStage.StageTerm__c =postStage.StageTerm__c.adddays(1);
                                postStage.Stage__c = 'Post';
                                postStage.Transplant__c = t.id;
                                postStage.type__c = 'Bone Marrow';
                            
                            insert postStage;


                        }
                        
                        list<Stage__c> prelist = new list<Stage__c>([select StageEff__c,StageTerm__c,Stage__c from Stage__c where Transplant__c=:t.id and Stage__c = 'Pre' order by StageEff__c ]);        
                
                        //A pre stage already exists, so we will update it
                        if(prelist.size()>0){
                        
                        prelist[0].StageTerm__c =t.transplant_start__c.addDays(-11);
                        update prelist;
                        
                        }else{
                        //A pre stage does not exist, so we will create it
                        Stage__c preStage = new Stage__c();
                            preStage.StageTerm__c =t.transplant_start__c.addDays(-11);
                                preStage.Stage__c = 'Pre';
                                    preStage.Transplant__c = t.id;
                                        preStage.type__c = 'Bone Marrow';       

                        insert preStage;    
                        
                        }
                            
            }
            
            //End Bone Marrow Stage Setup
            
            //Begin non-bone marrow organ stage setup, only for a single organ....multiple organs are handled in the TransController class
                //Multiple or Single Organ is controlled by the TransDetail and TransEdit form, so not checking here
                    //Also it should be noted that a global stage cannot be created by the user without a pre stage already existing
                        //and a post stage cannot be created without global stage already existing
            if((t.Transplant_Discharge_Date__c!=null  && t.transplant_type__c!='Bone Marrow') && (t.Transplant_Discharge_Date__c!= trigger.oldmap.get(t.id).Transplant_Discharge_Date__c)){
                
                list<Stage__c> stagecheck = new list<Stage__c>([select Stage__c, StageTerm__c, StageEff__c from Stage__c where Transplant__c=:t.id and Stage__c = 'Global' order by StageTerm__c]); 
                Date globalEff;
                Date transplantDate = t.transplant_date__c;  
                if(stagecheck.size()>0){
                //A global stage already exists, so we will update it   
                    if(t.transplant_date__c !=null){
                        
                        if(t.Transplant_Admission_Date__c!=null){
                            Integer daystill = t.Transplant_Admission_Date__c.daysBetween(t.transplant_date__c);
                            
                            if(daystill>1){
                                stageCheck[0].StageEff__c = t.transplant_date__c;
                                 globalEff = t.transplant_date__c;
                            }else{
                                 stageCheck[0].StageEff__c = t.Transplant_Admission_Date__c;
                                  globalEff = t.Transplant_Admission_Date__c;
                            }
                            
                        }else{
                          stageCheck[0].StageEff__c = t.transplant_date__c;
                           globalEff = t.transplant_date__c;
                        }
                        
                    }else{
                        if(t.Transplant_Admission_Date__c!=null){
                            stageCheck[0].StageEff__c = t.Transplant_Admission_Date__c;
                             globalEff = t.Transplant_Admission_Date__c;
                        }
                    }
                    
                    stageCheck[0].StageEff__c = t.Transplant_Discharge_Date__c;
                    update stagecheck;
                
                }else{
                
                    //A global stage does not exist, so we will create it       
                    Stage__c newStage = new Stage__c(Transplant__c=t.id,Stage__c='Global',StageTerm__c=t.Transplant_Discharge_Date__c);
                    
                    if(!t.transplant_type__c.contains(';')){
                        newStage.Type__c = t.transplant_type__c;
                    }
                    
                    if(t.transplant_date__c !=null){
                        
                        if(t.Transplant_Admission_Date__c!=null){
                            Integer daystill = t.Transplant_Admission_Date__c.daysBetween(t.transplant_date__c);
                            system.debug(daystill + ' daystill');
                            if(daystill>1){
                                newStage.StageEff__c = t.transplant_date__c;
                                 globalEff = t.transplant_date__c;
                            }else{
                                 newStage.StageEff__c = t.Transplant_Admission_Date__c;
                                  globalEff = t.Transplant_Admission_Date__c;
                            }
                            
                        }else{
                          newStage.StageEff__c = t.transplant_date__c;
                           globalEff = t.transplant_date__c;
                        }
                        
                    }else{
                        if(t.Transplant_Admission_Date__c!=null){
                            newStage.StageEff__c = t.Transplant_Admission_Date__c;
                             globalEff = t.Transplant_Admission_Date__c;
                        }
                    }
                    
                    newStageList.add(newStage);
                    
                }
                
                //Check for Pre-Stage
                   list<Stage__c> preStage = new list<Stage__c>([select StageEff__c, StageTerm__c from Stage__c where Transplant__c = :t.id and Stage__c = 'Pre' order by StageEff__c desc]);
                    
                    //Need to update the pre-stasge, if one exists
                    if(preStage.size()>0){
                        
                        preStage[0].StageTerm__c = globalEff.addDays(-1);
                        update preStage;
                    }else{
                        
                        Stage__c newStage = new Stage__c(Transplant__c=t.id,Stage__c='Pre',StageTerm__c=globalEff.addDays(-1),Type__c=t.transplant_type__c);
                        newStageList.add(newStage);
                    }
                
                //Check for Post-Stage
                list<Stage__c> postStage = new list<Stage__c>([select StageEff__c, StageTerm__c from Stage__c where Transplant__c = :t.id and Stage__c = 'Post' order by StageEff__c desc]);
                
                if(postStage.size()>0){
                    //post stage exists so update it    
                    postStage[0].StageEff__c=t.Transplant_Discharge_Date__c.addDays(1);
                     postStage[0].StageTerm__c=transplantDate.addyears(1);
                      postStage[0].StageTerm__c=postStage[0].StageTerm__c.adddays(1);
                      update postStage[0];
                
                }else{
                    //Need to create the post-stasge
                    Stage__c newStage = new Stage__c(Transplant__c=t.id,Stage__c='Post',StageEff__c=t.Transplant_Discharge_Date__c.addDays(1));
                            newstage.stageterm__c = transplantDate.addyears(1);
                            newstage.stageterm__c = newstage.stageterm__c.adddays(1);
                            newStage.Type__c = t.transplant_type__c;
                newStageList.add(newStage);
                }
            }
                
            if(t.Transplant_Type__c != null && ((t.Transplant_Type__c != trigger.oldmap.get(t.id).transplant_type__c) && trigger.oldmap.get(t.id).transplant_type__c!=null)){
                
                Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=t.id,
                    Type__c='Note',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Transplant Type Change',
                    Notes__c='The Transplant Type changed from ' + trigger.oldmap.get(t.id).transplant_type__c+ ' to '+ t.Transplant_Type__c
                );
                notesList.add(tempNote);
            } 
        }
    }
    
    if(notesList.size()>0){
        insert notesList;   
    }
    
    if(newStageList.size()>0){
        try{
        insert newStageList;
        }catch(exception e){}
    }
    
    if(finalMail.size()>0){
        messaging.sendEmail(finalmail);
    }
    
}