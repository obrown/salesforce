trigger memberTripDeleted on MemberTrips__c (after delete) {
    
    set<id> id_set = new set<id>();
    
    for(MemberTrips__c mt : trigger.old){
        id_set.add(mt.patient_case__c);
    }
    
    if(! id_set.isempty()){
        memberTripWorkflow.deletedTrip(id_set);
    }
}