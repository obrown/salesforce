trigger BariatricAfter on Bariatric__c (after insert) {
    
    //Submit for eligbility after creating a new case
    bariatricFuture.submitForEligibility(trigger.newmap.keyset());
    
}