trigger oncologyEligibilityAfter on Oncology_Eligibility__c (after insert, after update) {

    oncology__c[] updateList = new oncology__c[]{};
    
    for(Oncology_Eligibility__c oe : trigger.new){
        
        if(trigger.isInsert && oe.eligible__c!=null || trigger.isUpdate && oe.eligible__c != trigger.oldMap.get(oe.id).eligible__c){ 
            updateList.add(new oncology__c(id=oe.oncology__c,Eligibility_Verification__c=oe.eligible__c,Date_Eligibility_Verified__c=date.today()));
        
        }
    }
    
    try{
        upsert updateList;
    }catch(dmlexception e){
         for (Integer i = 0; i < e.getNumDml(); i++){
             ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, e.getDmlMessage(i)));
             System.debug(e.getDmlMessage(i)+' '+e.getLineNumber()); 
         }
    }
}