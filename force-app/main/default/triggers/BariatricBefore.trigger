trigger BariatricBefore on Bariatric__c (before update, before insert){

    Bariatric__c[] ageList = new Bariatric__c[]{};
    Bariatric__c[] bidList = new Bariatric__c[]{};
    Bariatric__c[] eligList = new Bariatric__c[]{};
    Bariatric__c[] lastNameList = new Bariatric__c[]{};
    Bariatric__c[] recommendedFacility = new Bariatric__c[]{};
    
    set<ID> demoID =new set<ID>();
    set<ID> syncReferredFacilty = new set<id>();
     
    if(trigger.isUpdate){
    
        for(Bariatric__c b : trigger.new){
            b.bid_id__c=b.bid__c;
            demoID.add(b.id);
            dobFormat(b);
            
            if(b.Referral_Date__c==null || b.eligible__c!=trigger.oldMap.get(b.id).eligible__c){
                BariatricWorkflow.autoStatus(b, null, null);
            }
            
            if(b.patient_dob__c!=trigger.oldMap.get(b.id).patient_dob__c){
                ageList.add(b);
            }
            
            if(b.bid__c!=trigger.oldMap.get(b.id).bid__c){
                bidList.add(b);
            }
            
            if(b.CaseLatitudeLongitude__c!=null && (b.CaseLatitudeLongitude__c!= trigger.oldmap.get(b.id).CaseLatitudeLongitude__c)){
               recommendedFacility.add(b); 
            }
            
            if(b.Patient_Street__c!=null && b.Patient_City__c!=null && b.Patient_State__c!=null && b.Patient_Zip_Code__c!=null && b.referred_facility__c != null && b.referred_facility__c != trigger.oldmap.get(b.id).referred_facility__c){
                  recommendedFacility.add(b); 
            }
            
            if(b.eligible__c != null && (b.eligible__c != trigger.oldmap.get(b.id).eligible__c)){
                eligList.add(b);
                b.Eligibility_Verified_By__c = userinfo.getuserid();
                b.Date_Eligibility_Check_with_Client__c  = date.today();
            }
            
            if(b.patient_last_name__c != trigger.oldmap.get(b.id).patient_last_name__c  && b.patient_last_name__c != null){
                lastNameList.add(b);
            }
            
            if(b.referred_facility__c != trigger.oldMap.get(b.id).referred_facility__c){
                syncReferredFacilty.add(b.id);
            }
            
        }
    
    }else{
        ageList = trigger.new;
        bidList = trigger.new;
        
        for(Bariatric__c b : trigger.new){
            b.bid_id__c=b.bid__c;
            if(b.encyptionKey__c==null){
                b.encyptionKey__c = EncodingUtil.base64encode(Crypto.generateAesKey(128));
            }
            
            dobFormat(b);
            BariatricWorkflow.autoStatus(b, null, null);
            
            if(b.CaseLatitudeLongitude__c!=null){
                  recommendedFacility.add(b); 
            }
            
            if(b.patient_last_name__c != null)
            {
                lastNameList.add(b);
            }
            
            
        }
    }
    
    if(lastNameList.size()>0){
        //Update patient age
        BariatricWorkflow.updateLastNameSort(lastNameList);
    
    }
    
    if(ageList.size()>0){
        //Update patient age
        BariatricWorkflow.updateBariatricAges(ageList);
    
    }
    
    if(bidList.size()>0){
        //Update patient age
        BariatricWorkflow.updateBariatricCert(bidList);
    }
    
    if(eligList.size()>0){
        //Update Eligibility
        BariatricWorkflow.updateEligibility(eligList);
    }
    
    if(recommendedFacility.size()>0){
        BariatricWorkflow.recommendedFacility(recommendedFacility);
    }
     
    if(!syncReferredFacilty.isEmpty()){
        bariatricFuture.syncReferredFacilty(syncReferredFacilty);
    }
    
    if(!System.isFuture()){
        bariatricFuture.setDemoBariatricStages(demoID);
        
        if(trigger.newMap!=null){
            bariatricFuture.setStatusRollUp(trigger.newMap.keyset());
        }
    }

    

    void dobFormat(bariatric__c b){
    
        if(b.employee_dob__c!=null && b.employee_dob__c.contains('/')){
            b.employee_dob__c = bariatricUtils.dobDatabaseEncode(b.employee_dob__c);
                
        }
            
        if(b.patient_dob__c!=null && b.patient_dob__c.contains('/')){
            b.patient_dob__c = bariatricUtils.dobDatabaseEncode(b.patient_dob__c);
        }
    }
    
}