trigger lifestartAfterUpdate on Lifestart__c (after update) {

    set<id> theIDs = new set<id>();
    
    for(lifestart__c l : trigger.new){
    
        if((l.Delivered_Outcome__c != trigger.oldMap.get(l.id).Delivered_Outcome__c) && l.Delivered_Outcome__c == 'Single Liveborn'){
            if(l.parent__c ==null){
                theIDs.add(l.id);
            }
        }
    
    }
    
    if(theIDs.size()>0){
        futureLifestart.deletePregnancyOutcome(theIDs);
    }

}