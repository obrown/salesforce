trigger afterInpatient on Inpatient__c (after insert, after update) {

    map<id,dates> dateMapper = new map<id, dates>();
    boolean foo=false;
    
    map<id,id> apptToTransMap = new map<id,id>();
    for(inpatient__c a: [select appointment__c,appointment__r.transplant__c from inpatient__c where id IN :trigger.newMap.keyset()]){
        apptToTransMap.put(a.appointment__c, a.appointment__r.transplant__c); 
    }
    
    
    for(inpatient__c i : trigger.new){
    foo=false;    
        if(trigger.isInsert)
        {
            if(i.Hospital_Admit__c !=null || i.Hospital_Discharge__c != null)
            {
                foo=true;
            }
            
        }else{
            if((i.Hospital_Admit__c != trigger.oldMap.get(i.id).Hospital_Admit__c) || 
               (i.Hospital_Discharge__c != trigger.oldMap.get(i.id).Hospital_Discharge__c))
            {
                foo=true;
            }
        }
        
        if(foo)
        {
            dateMapper.put(apptToTransMap.get(i.appointment__c), new dates(i.Hospital_Admit__c, i.Hospital_Discharge__c ));
        
        }
    
    }
    
    
    transplant__c[] tlist = new transplant__c[]{};
    dates theDates;
    for(transplant__c t :[select id, inpatientAdmit__c, inpatientDischarge__c from transplant__c where id in :dateMapper.keyset()]){
        
        theDates = dateMapper.get(t.id);
        t.inpatientAdmit__c = theDates.admit;
        t.inpatientDischarge__c = theDates.discharge;
        tlist.add(t);
    }
    
    if(tlist.size()>0)
    {
        update tlist;
    }
        
    class dates
    {
    
        public date admit {get;set;}
        public date discharge {get;set;}
        
        public dates(date admit, date discharge)
        {
        
            this.admit= admit;
            this.discharge= discharge; 
        
        }
    
    }

}