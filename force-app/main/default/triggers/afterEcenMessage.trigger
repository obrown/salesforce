trigger afterEcenMessage on ECEN_Message__c (after insert) {
    
    set<string> ids = new set<string>();
    
    for(ECEN_Message__c em : trigger.new){
        if(!em.Patient_Sent_Message__c){
            ids.add(em.id);        
        }
    }
    
    if(!test.isrunningtest() && !ids.isEmpty()){
        coePatientMessageFuture.sendPushNotification(ids);
    }
    
}