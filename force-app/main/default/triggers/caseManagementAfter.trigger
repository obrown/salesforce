trigger caseManagementAfter on Case_Management__c (after insert,after update) {
    public Case_Management_History__c cmAuditTrailRecord {
       get; private set;
   }    
         
   if(trigger.isInsert){
   //set audit history initial save record
    cmAuditTrailRecord = new Case_Management_History__c();
    for (Case_Management__c cm : Trigger.new) {

         ID id = cm.Id;
         cmAuditTrailRecord.Case_Management__c= ID;
         cmAuditTrailRecord.Initial_History__c= true;
         cmAuditTrailRecord.Benefit_Year_Plan_Year__c= cm.Benefit_Year_Plan_Year__c;
         cmAuditTrailRecord.Benefit_Year_Text__c= cm.Benefit_Year_Text__c;
         cmAuditTrailRecord.Care_Navigator__c= cm.Care_Navigator__c;
         cmAuditTrailRecord.Case_Manager__c= cm.Case_Manager__c;
         cmAuditTrailRecord.Case_Status__c= cm.Case_Status__c;
         cmAuditTrailRecord.Case_Sub_status__c= cm.Case_Sub_status__c;
         cmAuditTrailRecord.Clinical_Update__c= cm.Clinical_Update__c;
         cmAuditTrailRecord.Clinical_Update_Note__c= cm.Clinical_Update_Note__c;
         cmAuditTrailRecord.CM_Casepac_ID__c= cm.CM_Casepac_ID__c;
         cmAuditTrailRecord.CM_Referral_Denied__c= cm.CM_Referral_Denied__c;
         cmAuditTrailRecord.CM_Risk_Rating__c= cm.CM_Risk_Rating__c;
         
         If(cm.Coordination_of_Provider_Services__c==null){
             cmAuditTrailRecord.Coordination_of_Provider_Services__c= DateTime.newInstance(1986, 2, 21, 23, 0, 0);
         }else{
             cmAuditTrailRecord.Coordination_of_Provider_Services__c= cm.Coordination_of_Provider_Services__c;
         }

         If(cm.Disease_Condition_TX_Plan_Education__c==null){
             cmAuditTrailRecord.Disease_Condition_TX_Plan_Education__c= DateTime.newInstance(1986, 2, 21, 23, 0, 0);
         }else{
             cmAuditTrailRecord.Disease_Condition_TX_Plan_Education__c= cm.Disease_Condition_TX_Plan_Education__c;
         }  

         If(cm.Education_on_Benefits_Network__c==null){
             cmAuditTrailRecord.Education_on_Benefits_Network__c= DateTime.newInstance(1986, 2, 21, 23, 0, 0);
         }else{
             cmAuditTrailRecord.Education_on_Benefits_Network__c= cm.Education_on_Benefits_Network__c;
         } 
         
         If(cm.Patient_Coaching_Support__c==null){
             cmAuditTrailRecord.Patient_Coaching_Support__c= DateTime.newInstance(1986, 2, 21, 23, 0, 0);
         }else{
             cmAuditTrailRecord.Patient_Coaching_Support__c= cm.Patient_Coaching_Support__c;
         } 

         If(cm.Referral_for_OON_Negotiations__c==null){
             cmAuditTrailRecord.Referral_for_OON_Negotiations__c= DateTime.newInstance(1986, 2, 21, 23, 0, 0);
         }else{
             cmAuditTrailRecord.Referral_for_OON_Negotiations__c= cm.Referral_for_OON_Negotiations__c;
         }
                   
         If(cm.Ref_to_Other_Programs_Benefit_Services__c==null){
             cmAuditTrailRecord.Ref_to_Other_Programs_Benefit_Services__c= DateTime.newInstance(1986, 2, 21, 23, 0, 0);
         }else{
             cmAuditTrailRecord.Ref_to_Other_Programs_Benefit_Services__c= cm.Ref_to_Other_Programs_Benefit_Services__c;
         }

         If(cm.Steerage_to_In_Network_Provider__c==null){
             cmAuditTrailRecord.Steerage_to_In_Network_Provider__c= DateTime.newInstance(1986, 2, 21, 23, 0, 0);
         }else{
             cmAuditTrailRecord.Steerage_to_In_Network_Provider__c= cm.Steerage_to_In_Network_Provider__c;
         }

         cmAuditTrailRecord.Cost_Containment_Activity_Completion_T__c= cm.Cost_Containment_Activity_Completion_T__c;
         cmAuditTrailRecord.Cost_Containment__c= cm.Cost_Containment__c;
         cmAuditTrailRecord.Cost_Containment_Activity_Completion__c= cm.Cost_Containment_Activity_Completion__c;
         cmAuditTrailRecord.Cost_Driver__c= cm.Cost_Driver__c;
         cmAuditTrailRecord.Coverage_Status__c= cm.Coverage_Status__c;
         cmAuditTrailRecord.Diagnosis__c= cm.Diagnosis__c;
         cmAuditTrailRecord.Discharge_Date__c= cm.Discharge_Date__c;
         cmAuditTrailRecord.Disease_Condition_TX_Text__c= cm.Disease_Condition_TX_Text__c;
         cmAuditTrailRecord.Education_on_Benefits_Network_Text__c= cm.Education_on_Benefits_Network_Text__c;
         cmAuditTrailRecord.Engagement__c= cm.Engagement__c;
         cmAuditTrailRecord.Enrollment__c= cm.Enrollment__c;
         cmAuditTrailRecord.Expected_Cost__c= cm.Expected_Cost__c;
         cmAuditTrailRecord.HCC__c= cm.HCC__c;
         cmAuditTrailRecord.Identification_Date__c= cm.Identification_Date__c;
         cmAuditTrailRecord.Lifestart__c= cm.Lifestart__c;
         cmAuditTrailRecord.MDC__c= cm.MDC__c;
         cmAuditTrailRecord.MDC_Sub_Category__c= cm.MDC_Sub_Category__c;
         cmAuditTrailRecord.Newborn__c= cm.Newborn__c;
         cmAuditTrailRecord.Lifestart__c= cm.Lifestart__c;
         cmAuditTrailRecord.Newborn_Transfer__c= cm.Newborn_Transfer__c;
         cmAuditTrailRecord.Next_Review_Date__c= cm.Next_Review_Date__c;
         cmAuditTrailRecord.Opened_Text__c= cm.Opened_Text__c;
         cmAuditTrailRecord.Referral_Source__c= cm.Referral_Source__c;

    }
    insert(cmAuditTrailRecord);
   
   }
   
   if(trigger.isUpdate){
   fieldAuditHistory.recordHistory(trigger.newMap, trigger.oldMap, 'Case_Management__c', 'Case_Management_History__c');
   system.debug('****field History called*******');
   }
}
