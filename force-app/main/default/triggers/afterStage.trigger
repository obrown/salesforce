/*

    Purpose: Set up max term dates. If the the transplant record has post stage with the term date populated, 
    and another stage with no term date populated, then the max date should be null

*/

trigger afterStage on Stage__c (after insert, after update, after delete){

    set<id> tID = new set<id>();
    
    if(trigger.IsDelete){
        for(Stage__c s : trigger.old){
            if(s.stage__c != null && s.type__c != null){
                tID.add(s.transplant__c);
            }
        }
    }else{
        for(Stage__c s : trigger.new){
            if(s.stage__c != null && s.type__c != null){
                tID.add(s.transplant__c);
            }
        }
    }
    
    
    futureElig.setMaxTerm(tID);
   
}