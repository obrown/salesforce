trigger ClaimsProcessorCrosswalkBefore on Claims_Processor_Crosswalk__c (before insert, before update) {

    
    for(Claims_Processor_Crosswalk__c  cpc : trigger.new){
    
        if(cpc.processor_alias_name__c!=null){
    
            for(string s : cpc.processor_alias_name__c.split(',')){
    
                if(!s.isAlphaNumeric()){
    
                    cpc.addError('Only A Comma Can Be Used To Seperate Multiple Processor Alias Names. Example Jack,Adam,Tina');
                }
            
            }
        }
    
    }
            


}