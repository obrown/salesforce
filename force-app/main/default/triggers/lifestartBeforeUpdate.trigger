trigger lifestartBeforeUpdate on Lifestart__c (before update, before insert) {

    for(Lifestart__c ls : trigger.New){
        
        if(ls.patient_last_name__c!=''){
           ls.patient_last_name_sort__c = ls.patient_last_name__c.left(2); 
        }
        
    }

}