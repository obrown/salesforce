trigger UtilizationManagementNote on Utilization_Management_Note__c(after insert, after update) {
    set<id> umNoteIds = new set<id>();

    for (Utilization_Management_Note__c umn : trigger.new) {
        umNoteIds.add(umn.id);
    }

    utilizationManagementWorkflow.check_for_clinical_note_for_cm(umNoteIds);
}