trigger afterClaimsProcessed on caClaims_Processed__c (after Insert) {
    set<id> tSet = new set<id>();
    
    for(caClaims_Processed__c t : trigger.new){
        tSet.add(t.id);
    }
    
    claimAuditFuture.setClaimProcessedCount(tSet);

}