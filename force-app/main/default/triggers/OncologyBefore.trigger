trigger OncologyBefore on Oncology__c (before update, before insert ) {
    set<Oncology__c> selfSubcriber = new set<Oncology__c>();
    set<Oncology__c> sameAddress = new set<Oncology__c>();
    set<Oncology__c> formatPDOB = new set<Oncology__c>();
    set<Oncology__c> pocAuth = new set<Oncology__c>();
    
    for(Oncology__c o : trigger.new){
        
        o.bid_id__c = o.Patient_Medical_Plan_Number__c;
        
        if(o.Relationship_to_Subscriber__c=='Self'){
            selfSubcriber.add(o);
        }
        
        if(o.Same_as_Patient_Address__c){
            sameAddress.add(o);
        }

        if(o.Patient_DOB__c != null){
            if(date.valueOf(o.Patient_DOB__c)<date.today()){
               // if(o.Patient_Age__c==null||o.Patient_Age__c<=0){
                    formatPDOB.add(o);
              //  }
                         
           }else{
               
               o.AddError('The patient cannot be from the future');
               o.Patient_Age__c=null;
               
           }         

        }
        
        if(o.Employee_DOB__c != null){
            if(date.valueOf(o.Employee_DOB__c)>date.today()){
                o.AddError('The employee cannot be from the future');
            }
        }
        
        if(o.Facility_Plan_of_Care_Submission_Date__c!=null && o.status__c != 'Closed' && o.status__c != 'Completed'){
            pocAuth.add(o);
        }
        
        if(trigger.isUpdate){
            if(o.Evaluation_Actual_Departure__c!=null && o.Evaluation_Actual_Departure__c!= trigger.oldMap.get(o.id).Evaluation_Actual_Departure__c){
                o.createHP__c=true;
                o.Loaded_into_HealthPac__c =null;
            }
            
            if(o.Evaluation_Actual_Departure__c==null && o.createHP__c){
                o.createHP__c=false;
            }
        }
        
    }
    
    oncologyWorkflow.selfSubcriber(selfSubcriber );
    oncologyWorkflow.sameAddress(sameAddress);
    oncologyWorkflow.formatPDOB(formatPDOB);
    oncologyWorkflow.pocAuth(pocAuth);
    oncolcogyAutoStatusing.autoStatus(trigger.new);

}