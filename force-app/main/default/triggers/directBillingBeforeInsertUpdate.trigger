trigger directBillingBeforeInsertUpdate on Direct_Billing_Leave__c (before insert, before update) {
    
    directBillingWorkflow dbWF = new directBillingWorkflow();
    map<id, Direct_Billing_Leave__c > startDate = new map<id, Direct_Billing_Leave__c >();
    map<id, Direct_Billing_Leave__c > endDate = new map<id, Direct_Billing_Leave__c >();
    set<id> leaveDateChanges = new set<id>();
    
    for(Direct_Billing_Leave__c db : trigger.new){
        
        if(db.Start_Date__c != null){
            
            if(db.pay_frequency__c == '' || db.pay_frequency__c == null){
                db.AddError('Please add the pay frequency');
            
            }else{
                
                if(db.pay_frequency__c=='Bi-Weekly'){
                    
                    if(!dbWF.biWeeklyPayDates.contains(db.Start_Date__c.addDays(-1))){
                        db.AddError('Start Date is invalid');
                    }
                    
                }else if(db.pay_frequency__c=='Semi-Monthly'){
                    
                    if(!dbWF.semiMonthlyPayDates.contains(db.Start_Date__c.addDays(-1))){
                        db.AddError('Start Date is invalid');
                    }
                    
                }
                
            }
            
            if(trigger.isInsert || db.Start_Date__c != trigger.oldMap.get(db.id).Start_Date__c){
                startDate.put(db.db_member__c , db);
                leaveDateChanges.add(db.id);
                
            }
            
        }
        
        if(db.end_date__c !=null){
            if(trigger.isInsert || db.End_Date__c != trigger.oldMap.get(db.id).End_Date__c){
                startDate.put(db.db_member__c , db);
                leaveDateChanges.add(db.id);
            }
                
        }
        
    }
    
    if(startDate.size()>0){
        
        for(Direct_Billing_Leave__c db : directBillingWorkflow.checkStartDate(startDate)){
            
            Direct_Billing_Leave__c dbl = startDate.get(db.db_member__c);
            system.debug(dbl.id+' '+db.id);
            if(trigger.isInsert){
                if(dbl.id==null){
                    dbl.addError('The leave start and/or end date conflict another leave');
                }
                
            }else{
                if(dbl.id==db.id){
                    dbl.addError('The leave start and end date conflict another leave');
                    
                }
            }
            
            //if(db.id ==null || dblid) ==null){
               // if(db.end_date__c!=null){
               //     dbl.addError('The leave start and end date conflict another leave');
                
              //  }
                
              //  if(trigger.isInsert){
                   // dbl.start_date__c =null;
                 //   dbl.end_date__c =null;
            
              //  }else{
                
                   // dbl.start_date__c = trigger.oldMap.get(dbl.id).start_date__c;
                   // dbl.end_date__c = trigger.oldMap.get(dbl.id).end_date__c;
               // }
            
            //}
        
        }
        
    }
    
    if(leaveDateChanges.size()>0){
        //directBillingFuture.updateInsertOnLeaveDates(leaveDateChanges);
        
    }
    
}