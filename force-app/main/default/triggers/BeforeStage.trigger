trigger BeforeStage on Stage__c (before insert, before update, before delete) {
list<Transplant_Notes__c> addNotes = new list<Transplant_Notes__c>();
list<Stage__c> stageList = new list<Stage__c>();
map<string,id> tmap = new map<string,id>();


	if(!trigger.isDelete){
		
	for(transplant__c t: [select employee_ssn__c from transplant__c]){
	
		tmap.put(t.employee_ssn__c, t.id);

	}
		
	for(Stage__c s: trigger.new){
		
		if(s.isTransplantDates__c!=null){
			
			if(trigger.isInsert){
				
				if(s.Type__c!=null){
				
					if(s.Type__c.left(1)=='['){
						s.Type__c = s.Type__c.right(s.Type__c.length()-1);
					}
					
					if(s.Type__c.right(1)==']'){
						s.Type__c = s.Type__c.left(s.Type__c.length()-1);
					}
				
				}

				string body='';
				
				if(s.Transplant_Listing__c!=null){
					body+='Transplant Listing Date added: ' + s.Transplant_Listing__c.month() + '/' + s.Transplant_Listing__c.day() + '/' + s.Transplant_Listing__c.year() +'\n';
				}
				
				if(s.Transplant_Listing_Verification__c!=null){
					body+='Transplant Listing Verification Date added: ' + s.Transplant_Listing_Verification__c.month() + '/' + s.Transplant_Listing_Verification__c.day() + '/' + s.Transplant_Listing_Verification__c.year() +'\n';
				}
				
				if(s.Status_7_Date__c!=null){
					body+='Transplant Listing Status 7 Date added: ' + s.Status_7_Date__c.month() + '/' + s.Status_7_Date__c.day() + '/' + s.Status_7_Date__c.year() +'\n';
				}
				
				if(body!=''){
                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=s.Transplant__c,
                    Type__c='None',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Transplant Listing Date',
                    Notes__c=body);					
					
					addNotes.add(tempNote);
				}
				
			}else if(trigger.isUpdate){
				
				if(s.Type__c!=null){
				
					if(s.Type__c.left(1)=='['){
						s.Type__c = s.Type__c.right(s.Type__c.length()-1);
					}
					
					if(s.Type__c.right(1)==']'){
						s.Type__c = s.Type__c.left(s.Type__c.length()-1);
					}
				
				}
				
				string body='';
				
				if((s.Transplant_Listing__c!=null && trigger.oldmap.get(s.id).Transplant_Listing__c!=null) && (s.Transplant_Listing__c!= trigger.oldmap.get(s.id).Transplant_Listing__c)){
					body+='Transplant Listing changed from : ' + trigger.oldmap.get(s.id).Transplant_Listing__c.month() + '/' + trigger.oldmap.get(s.id).Transplant_Listing__c.day() + '/' + trigger.oldmap.get(s.id).Transplant_Listing__c.year() + ' to ' + s.Transplant_Listing__c.month() + '/' + s.Transplant_Listing__c.day() + '/' + s.Transplant_Listing__c.year() +'\n';
				}
				
				if((s.Transplant_Listing_Verification__c!=null && trigger.oldmap.get(s.id).Transplant_Listing_Verification__c!=null) && (s.Transplant_Listing_Verification__c!= trigger.oldmap.get(s.id).Transplant_Listing_Verification__c)){
					try{body+='Transplant Listing Verification changed from: ' + trigger.oldmap.get(s.id).Transplant_Listing_Verification__c.month() + '/' + trigger.oldmap.get(s.id).Transplant_Listing_Verification__c.day() + '/' + trigger.oldmap.get(s.id).Transplant_Listing_Verification__c.year() + ' to ' + s.Transplant_Listing_Verification__c.month() + '/' + s.Transplant_Listing_Verification__c.day() + '/' + s.Transplant_Listing_Verification__c.year() + '\n';
					}catch(exception e){}
				}
				
				if((s.Status_7_Date__c!=null && trigger.oldmap.get(s.id).Status_7_Date__c!=null) && (s.Status_7_Date__c!= trigger.oldmap.get(s.id).Status_7_Date__c)){
					try{body+='Transplant Listing Status 7 Date changed from : '+ trigger.oldmap.get(s.id).Status_7_Date__c.month() + '/' + trigger.oldmap.get(s.id).Status_7_Date__c.day() + '/' + trigger.oldmap.get(s.id).Status_7_Date__c.year() + ' to ' + s.Status_7_Date__c.month() + '/' + s.Status_7_Date__c.day() + '/' + s.Status_7_Date__c.year() + '\n';
					}catch(exception e){}
				}
				
				if(body!=''){
                    Transplant_Notes__c tempNote = new Transplant_Notes__c(
                    Transplant__c=s.Transplant__c,
                    Type__c='None',
                    Contact_Type__c='Field Change',
                    Initiated__c='System',
                    Subject__c='Transplant Listing Date',
                    Notes__c=body);					
					
					addNotes.add(tempNote);
				}
				
				
			}
			
		}

		if(s.isRelo__c!=true && s.isTransplantDates__c!=true && s.isTransAdmit__c!=true){

		    string noteSubject='';
     		string noteBody='';
			
			if(trigger.isInsert){
				
					if(s.StageEff__c!=null){
						noteSubject =  s.Stage__c + ' Stage Added';
						noteBody += 'Stage effective date: ' + s.StageEff__c.month() + '/' + s.StageEff__c.day() + '/' + s.StageEff__c.year() +'\n';
					}
					
					if(s.StageTerm__c!=null){
						noteSubject =  s.Stage__c + ' Stage Added';
						noteBody += 'Stage termination date ' + s.StageTerm__c.month() + '/' + s.StageTerm__c.day() + '/' + s.StageTerm__c.year() +'\n';
					}
				
					if(noteSubject!=''){
						Transplant_Notes__c tNote = new Transplant_Notes__c(Transplant__c=s.Transplant__c,
							Type__c='None',
								Contact_Type__c='Field Change',
									Initiated__c='System',
										Subject__c=noteSubject,
											Notes__c=noteBody
						);
						addNotes.add(tNote);
					}
					
		    		noteSubject='';
     				noteBody='';
     				
			}else{

					if(s.StageEff__c!=null && (s.StageEff__c!= trigger.oldmap.get(s.id).StageEff__c)){
						noteSubject = s.Stage__c + ' Stage Changed';
						
						if(trigger.oldmap.get(s.id).StageEff__c!=null){
							noteBody += 'Effective date changed from ' + trigger.oldmap.get(s.id).StageEff__c.month()+ '/' + trigger.oldmap.get(s.id).StageEff__c.day()+ '/' + trigger.oldmap.get(s.id).StageEff__c.year() + ' to ' + s.StageEff__c.month() + '/' + s.StageEff__c.day() + '/' + s.StageEff__c.year() +'\n';
						}else{
							noteBody += 'Effective date changed from null to ' + s.StageEff__c.month() + '/' + s.StageEff__c.day() + '/' + s.StageEff__c.year() +'\n';
							
						}
					}
					
					if(s.StageEff__c==null && (s.StageEff__c!= trigger.oldmap.get(s.id).StageEff__c)){
						noteSubject = s.Stage__c + ' Stage Changed';
						noteBody += 'Effective date changed from ' + trigger.oldmap.get(s.id).StageEff__c.month()+ '/' + trigger.oldmap.get(s.id).StageEff__c.day()+ '/' + trigger.oldmap.get(s.id).StageEff__c.year() + ' to null.\n';	
					}
					
					if(s.StageTerm__c!=null && (s.StageTerm__c!= trigger.oldmap.get(s.id).StageTerm__c)){
						noteSubject = s.Stage__c +' Stage Changed';
						
						if(trigger.oldmap.get(s.id).StageTerm__c!=null){
						noteBody += 'Termination date changed from ' + trigger.oldmap.get(s.id).StageTerm__c.month()+ '/' + trigger.oldmap.get(s.id).StageTerm__c.day()+ '/' + trigger.oldmap.get(s.id).StageTerm__c.year() + ' to ' + s.StageTerm__c.month() + '/' + s.StageTerm__c.day() + '/' + s.StageTerm__c.year() +'\n';
						}else{
						noteBody += 'Termination date changed from null to ' + s.StageTerm__c.month() + '/' + s.StageTerm__c.day() + '/' + s.StageTerm__c.year() +'\n';
						}
					}
					
					if(s.StageTerm__c==null && (s.StageTerm__c!= trigger.oldmap.get(s.id).StageTerm__c)){
						noteSubject = s.Stage__c + ' Stage Changed';
						noteBody += 'Termination date changed from ' + trigger.oldmap.get(s.id).StageTerm__c.month()+ '/' + trigger.oldmap.get(s.id).StageTerm__c.day()+ '/' + trigger.oldmap.get(s.id).StageTerm__c.year() + ' to null.\n';
						
					}

					if(noteSubject!=''){
						Transplant_Notes__c tNote = new Transplant_Notes__c(Transplant__c=s.Transplant__c,
							Type__c='None',
								Contact_Type__c='Field Change',
									Initiated__c='System',
										Subject__c=noteSubject,
											Notes__c=noteBody
						);
						addNotes.add(tNote);
					}
					
			}
			
		}
		
	}
	
	}else{
		
		for(Stage__c s: trigger.old){
		   
		   string noteSubject='';
     	   string noteBody='';
		   	
		   			if(trigger.oldmap.get(s.id).StageEff__c!=null){
						noteSubject = trigger.oldmap.get(s.id).Stage__c + ' Stage Deleted';
						noteBody += 'Deleted Effective Date value ' + trigger.oldmap.get(s.id).StageEff__c.month()+ '/' + trigger.oldmap.get(s.id).StageEff__c.day()+ '/' + trigger.oldmap.get(s.id).StageEff__c.year() +'\n';
					}
					
					if(trigger.oldmap.get(s.id).StageTerm__c !=null){
						noteSubject = trigger.oldmap.get(s.id).Stage__c + ' Stage Deleted';
						noteBody += 'Deleted Termination Date value ' + trigger.oldmap.get(s.id).StageTerm__c.month()+ '/' + trigger.oldmap.get(s.id).StageTerm__c.day()+ '/' + trigger.oldmap.get(s.id).StageTerm__c.year() +'\n';
					}
			
					if(noteSubject!=''){
						Transplant_Notes__c tNote = new Transplant_Notes__c(Transplant__c=s.Transplant__c,
							Type__c='None',
								Contact_Type__c='Field Change',
									Initiated__c='System',
										Subject__c=noteSubject,
											Notes__c=noteBody
						);
						addNotes.add(tNote);
					}	
	
		}
	}		
	
	if(addNotes.size()>0){
	
		insert addNotes;	
	
	}

}