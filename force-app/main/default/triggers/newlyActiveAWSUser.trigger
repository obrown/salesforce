trigger newlyActiveAWSUser on AWS_User__c (after update, after insert) {

    Messaging.SingleEmailMessage[] mailList = new Messaging.SingleEmailMessage[]{};
    
    for(AWS_User__c awsUser : trigger.new){
        
        if(trigger.isInsert){
            if(awsUser.active__c){
                mailList.add(newNotification(awsUSer));
            }
            
        }else{
            if(trigger.oldMap.get(awsUser.id).active__c == false && awsUser.active__c){
                mailList.add(newNotification(awsUSer));
            }
            
        }
    }
    
    if(mailList.size()>0){
        Messaging.sendEmail(mailList);
    }
    
    Messaging.SingleEmailMessage newNotification(AWS_User__c awsUSer){
        
        string[] sendEmailTo = new string[]{};
        sendEmailTo.add(awsUSer.Email__c);
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        mail.setToAddresses(sendEmailTo);
        
        mail.subject = 'Contigo Health Provider Portal login';
        string emailBody = 'Hello '+ AWSUser.FirstName__c +' '+ AWSUser.LastName__c +',\n\n';
        emailBody += 'Your Provider Portal login has been approved. You can login in by going to the link below and using the credentials you used when you signed up.\n\n';
        emailBody += 'The username provided when you registered is ' +AWSUser.Email__c +'\n\n';
        emailBody += 'https://coeproviderportal.contigohealth.com\n\n';
        emailBody += 'Thank you,\n\nContigo Health';
        mail.setPlainTextBody(emailBody);
        
        mail.setSenderDisplayName('Contigo Health');
        mail.setReplyTo('noreply@ContigoHealth.com');
        return mail;
    }
    

}