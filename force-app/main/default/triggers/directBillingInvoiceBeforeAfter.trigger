trigger directBillingInvoiceBeforeAfter on Direct_Billing_Invoice__c (before insert, before update){
    
    set<id> leaveIds = new set<id>();
    for(direct_billing_invoice__c dbi : trigger.new){
        leaveIds.add(dbi.db_leave__c);    
        if(trigger.isUpdate){
            if((dbi.payment_amount__c != trigger.oldmap.get(dbi.id).payment_amount__c) || 
               (dbi.payment_amount_2__c != trigger.oldmap.get(dbi.id).payment_amount_2__c)) {
                if(dbi.payment_amount__c==null){dbi.payment_amount__c=0.00;}
                if(dbi.payment_amount_2__c==null){dbi.payment_amount_2__c=0.00;}
            }
        }else{
            if(dbi.payment_amount__c >0.00 || dbi.payment_amount_2__c >0.00){
                if(dbi.payment_amount__c==null){dbi.payment_amount__c=0.00;}
                if(dbi.payment_amount_2__c==null){dbi.payment_amount_2__c=0.00;}
                
            }
        }
            
    }
    if(!System.isFuture()){
        directBillingFuture.updateOutstandingBalance(leaveIds);
    }
    /*
    if(trigger.isInsert){
        for(direct_billing_invoice__c dbi : trigger.new){
            
            if(dbi.payment_amount__c >0.00 || dbi.payment_amount_2__c >0.00){
                if(dbi.payment_amount__c==null){dbi.payment_amount__c=0.00;}
                if(dbi.payment_amount_2__c==null){dbi.payment_amount_2__c=0.00;}
                
            }
            
            
        }
        
    }else if(trigger.isUpdate){
    
        for(direct_billing_invoice__c dbi : trigger.new){
            
            if((dbi.payment_amount__c != trigger.oldmap.get(dbi.id).payment_amount__c) || 
               (dbi.payment_amount_2__c != trigger.oldmap.get(dbi.id).payment_amount_2__c)) {
                if(dbi.payment_amount__c==null){dbi.payment_amount__c=0.00;}
                if(dbi.payment_amount_2__c==null){dbi.payment_amount_2__c=0.00;}
            }
            
        }
    
    }
    */
}