trigger beforeClientFacility on Client_Facility__c (before insert, before update) {
    
    Pattern emailRegEx = Pattern.compile('^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$');
    Matcher MyMatcher;
    
    for(Client_Facility__c c : trigger.new){
        c.copyOfNameField__c = c.name;
        if(c.Authorization_Email_Contact__c!=null && c.Authorization_Email_Contact__c!=''){
            string[] alist = c.Authorization_Email_Contact__c.split(',');
            
            if(alist.size()>5){
                c.addError('Max number of emails is 5');
            }
            
            for(string s : alist){
                MyMatcher = emailRegEx.matcher(s);
                if(!MyMatcher.matches()){
                    c.addError('Invalid Email Address');
                    break;
                }
                
            }
            
        }
        
        if(c.Referral_Email_Contact__c!=null && c.Referral_Email_Contact__c!=''){
            string[] relist = c.Referral_Email_Contact__c.split(',');
            
            if(relist.size()>5){
                c.addError('Max number of emails is 5');
            }
            
            for(string s : relist){
                MyMatcher = emailRegEx.matcher(s);
                if(!MyMatcher.matches()){
                    c.addError('invalid Email Address');
                }
            }
            
        }
    }

}