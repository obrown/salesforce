trigger beforeClientCarrier on Client_Carrier__c (before insert, before update) {
    
    Pattern emailRegEx = Pattern.compile('^[a-zA-Z0-9._|\\\\%#~`=?&/$^*!}{+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}$');
    Matcher MyMatcher;
    
    for(Client_Carrier__c c : trigger.new){
        if(c.Carrier_Email__c!=null && c.Carrier_Email__c!=''){
            string[] alist = c.Carrier_Email__c.split(',');
            
            if(alist.size()>5){
                c.addError('Max number of emails is 5');
            }
            
            for(string s : alist){
                MyMatcher = emailRegEx.matcher(s);
                if(!MyMatcher.matches()){
                    c.addError('Invalid Email Address');
                    break;
                }
                
            }
            
        }
        
    }

}