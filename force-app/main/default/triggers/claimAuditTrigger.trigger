trigger claimAuditTrigger on Claim_Audit__c (before update, before insert, after Insert, after update, after Delete) {
    
    system.debug('isBefore: ' + trigger.isBefore);
    system.debug('isAfter: ' + trigger.isAfter);
    
    if(Trigger.isAfter){
    
        if(!system.isFuture() && !trigger.isDelete){
        
            //set<id> theClaims = new set<id>();
            set<id> theClaimsAuditor = new set<id>();
            
                for(Claim_Audit__c ca : trigger.new){
                    
                    if(trigger.isUpdate){
                    
                    if((ca.Audit_Type__c != 'High Dollar' && ca.Audit_Type__c != null && (ca.audit_type__c != trigger.oldmap.get(ca.id).audit_type__c)) || 
                       (ca.client__c != null && (ca.client__c != trigger.oldmap.get(ca.id).client__c))){
                        theClaimsAuditor.add(ca.id);
                    }
                    
                    }
                    
                    //theClaims.add(ca.claim__c);
                }    
                //claimAuditFuture.updateClaimAuditName(theClaims);
                
                if(theClaimsAuditor.size()>0){
                    claimAuditFuture.setClaimAuditAuditor(theClaimsAuditor, new set<id>());
                }
        }
        
        if(!system.isFuture() && trigger.isInsert){
            set<id> theClaimAudits = new set<id>();
            set<id> claimAudits = new set<id>();
            set<id> theClaimClients = new set<id>();
                
                for(Claim_Audit__c ca : trigger.new){
                    
                    if(ca.Claim_Auditor__c==null){
                        theClaimAudits.add(ca.id);
                    }
                    
                    if(ca.claim_processor__c==null){
                        claimAudits.add(ca.id);
                    }
                    
                    
                }
                
            claimAuditFuture.setClaimAuditAuditor(theClaimAudits, theClaimClients );
            claimAuditFuture.setClaimAuditProcessor(claimAudits);
            
        }
            
    }else{
        
        system.debug('isInsert: ' + trigger.isInsert);
        system.debug('isUpdate: ' + trigger.isUpdate);
        
        if(trigger.isUpdate){
            
            string completeRecordID;
            string newRecordID;
            string hdRecordID;
            string hdcReviewRecordID;
            string hdcCompleteRecordID;
            
            for(Claim_Audit__c c : trigger.new){
                system.debug('Audit_Status__c: '+ c.Audit_Status__c);
                system.debug('Audit_Type__c: '+ c.Audit_Type__c );
                if(c.Audit_Status__c != trigger.oldmap.get(c.id).Audit_Status__c){
                    if(c.Audit_Status__c=='Completed'){
                        
                        if(trigger.oldmap.get(c.id).Audit_Type__c == 'High Dollar'){
                            
                            if(hdcCompleteRecordID==null){
                                hdcCompleteRecordID= [select id from RecordType where sobjectType = 'Claim_Audit__c' and isActive = true and name = 'High Dollar Completed'].id;
                            } 
                            
                            c.recordtypeid = hdcCompleteRecordID;
                            
                        }else{
                        
                            if(completeRecordID==null){
                                completeRecordID = [select id from RecordType where sobjectType = 'Claim_Audit__c' and isActive = true and name = 'Completed'].id;
                            }
                            
                            c.recordtypeid = completeRecordID;
                        }
                        
                        c.Released_By__c = userInfo.getuserID();
                            
                    }else if(c.Audit_Status__c=='High Dollar Committee'){
                        
                        if(hdRecordID==null){
                            hdRecordID= [select id from RecordType where sobjectType = 'Claim_Audit__c' and isActive = true and name = 'High Dollar'].id;
                        }
                        
                        c.recordtypeid = hdRecordID;
                        
                    }else if(c.Audit_Status__c=='High Dollar Committee Review'){
                        
                        if(hdcReviewRecordID==null){
                            hdcReviewRecordID= [select id from RecordType where sobjectType = 'Claim_Audit__c' and isActive = true and name = 'High Dollar Review'].id;
                        }
                        
                        c.recordtypeid = hdcReviewRecordID;
                        
                    }else{
                        
                        if(newRecordID==null){
                            newRecordID= [select id from RecordType where sobjectType = 'Claim_Audit__c' and isActive = true and name = 'New Audit'].id;
                        }
                        
                        c.recordtypeid = newRecordID;
                    }
                    
                }
                
                if(c.claim_processor__c==null){
                    c.claim_processor__c = c.claim__r.Claims_Processor_Crosswalk__c;
                }
                
                if(c.Committee_Outcome__c != trigger.oldmap.get(c.id).Committee_Outcome__c){
                        
                        if(hdcCompleteRecordID==null){
                            hdcCompleteRecordID= [select id from RecordType where sobjectType = 'Claim_Audit__c' and isActive = true and name = 'High Dollar Completed'].id;
                        }         
                        c.Audit_Status__c = 'Completed';
                        c.Released_By__c = userInfo.getuserID();
                        c.recordTypeid = hdcCompleteRecordID;
                }
                
                if(c.Claim_Auditor__c != null && c.WA__c == null){
                   c.WA__c = c.Claim_Auditor__c;
                }
                
                system.debug(completeRecordID+' '+newRecordID+' '+hdRecordID+' '+hdcReviewRecordID+' '+hdcReviewRecordID+' '+hdcCompleteRecordID);
            
           
                
            }            
        
            }else if(trigger.isInsert){
            
            for(Claim_Audit__c c : trigger.new){
            
                if(c.clientID__c != '' && c.audit_type__c !='' && c.audit_type__c !='High Dollar'){
                    c.Claim_Auditor__c = claimsAuditorAssignment.getAssignment(c.clientID__c,c.audit_type__c);
                    system.debug('claimsAuditorAssignment.getAssignment: '+c.Claim_Auditor__c);
                }
                
                if(c.claimProcessorID__c != '' && c.audit_type__c !='' && c.audit_type__c !='High Dollar'){
                    c.Claim_Processor__c = c.claimProcessorID__c;
                }
                
                if(c.Claim_Auditor__c != null && c.WA__c == null){
                   c.WA__c = c.Claim_Auditor__c;
                }
                
            }
        
            }
        
        
    }
    
}