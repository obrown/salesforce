trigger UtilizationManagementBefore on Utilization_Management__c (before insert, before update) {
    
    set<id> umInitalHpLoad = new set<id>();
    boolean firstRun = cheapstatichack.firstRun();
    
    for(Utilization_Management__c um : trigger.new){
        
        if(um.initial_hp_load__c == null && um.event_type__c!='' && um.event_type__c!=null && um.Med_Cat__c!='' && um.Med_Cat__c!=null){
            if(trigger.isUpdate){
                um.initial_hp_load__c=date.today();
                umInitalHpLoad.add(um.id);
            }else{
                um.initial_hp_load__c=date.today();
            }
            
        }
        
        if(firstRun ){
            if(trigger.isUpdate && um.HealthPac_Case_Number__c==null && !umInitalHpLoad.contains(um.id) && um.Admission_Date__c!=null && um.Admission_Date__c!= trigger.oldMap.get(um.id).Admission_Date__c){
                um.initial_hp_load__c=date.today();
                umInitalHpLoad.add(um.id);
            }
        }
        
        if(um.Newborn__c==true && um.Comment_Line_for_Claims__c!=null && !um.Comment_Line_for_Claims__c.contains('NEWBORN-')){
           um.Comment_Line_for_Claims__c='NEWBORN- '+um.Comment_Line_for_Claims__c;
        }else if(um.Newborn__c==true && um.Comment_Line_for_Claims__c==null){
           um.Comment_Line_for_Claims__c='NEWBORN- '+'';
        }       
    }   
    
    if(!system.isFuture() && !umInitalHpLoad.isEmpty()){
        utilizationManagementWorkflow.sendUM(umInitalHpLoad);
    }
    
}