trigger beforeFacility on Facility__c (before insert, before update){
    
    facilityWorkflow fw = new facilityWorkflow();
    
    for(Facility__c fac : trigger.new){
        //Just check for an address change or an address populated on insert
        if(!system.isFuture()&&(trigger.isUpdate && 
            (fac.Facility_Address__c !=  trigger.oldmap.get(fac.id).Facility_Address__c || 
             fac.Facility_City__c !=  trigger.oldmap.get(fac.id).Facility_City__c||
             fac.Facility_State__c !=  trigger.oldmap.get(fac.id).Facility_State__c ||
             fac.Facility_Zip__c!=  trigger.oldmap.get(fac.id).Facility_Zip__c))
             ||
             (fac.Facility_Address__c !=  null || 
             fac.Facility_City__c !=  null ||
             fac.Facility_State__c !=  null ||
             fac.Facility_Zip__c!= null)
             ){
        
            fw.fetchLatLong.add(fac.id);
        
        }
    }
       
    if(!system.isFuture()){
        fw.fetchLatLong();
    }
    
}