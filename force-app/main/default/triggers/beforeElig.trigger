trigger beforeElig on hp_eligibility__c (after insert,after update) {


    //search by certID
    list<Transplant__c> certList = new list<Transplant__c>();
    list<Patient_Case__c> pccertList = new list<Patient_Case__c>();
    set<string> coeGrpNbr = new set<string>();
    set<string> transGrpNbr = new set<string>();
    
    coeGrpNbr.add('WAL');
    transGrpNbr.add('WMT');
    
    map<string,Transplant__c> essnMap = new map<string,Transplant__c>();
    for(Transplant__c[] transplantCases : [select Loaded_into_HealthPac__c,Updated_in_HealthPac__c, Employee_SSN__c from Transplant__c]){
        for(Transplant__c t: transplantCases){
            if(t.Employee_SSN__c!=null){
                essnMap.put(t.Employee_SSN__c,t);
            }
        }
        
    }
    
    map<string,Patient_Case__c> pcessnMap = new map<string,Patient_Case__c>();
    for(Patient_Case__c[] patientCases : [select Loaded_into_HealthPac__c, Employee_SSN__c, legacyCreatedDate__c from Patient_Case__c]){
      for(Patient_Case__c p:patientCases){
        if(p.Employee_SSN__c!=null){
          pcessnMap.put(p.Employee_SSN__c,p);
        }
      }
        
    }
    
    certList.clear();
    Transplant__c tempt = new Transplant__c();
    Patient_Case__c pctempt = new Patient_Case__c();
    boolean foo = false;
    for(hp_eligibility__c t: trigger.new){
        
        if(t.GRNBR__c!= null && t.Loaded_into_HealthPac__c!=null){
              
              if(coeGrpNbr.contains(t.GRNBR__c)){
                  if(pcessnMap.containsKey(t.Employee_SSN__c)){
                  pctempt = pcessnMap.get(t.Employee_SSN__c);
                    
                  if(pctempt.legacyCreatedDate__c >= t.Loaded_into_HealthPac__c){
                      pctempt.Loaded_into_HealthPac__c = t.Loaded_into_HealthPac__c;
                  }
                    
                  pccertList.add(pctempt);
                  
                  }
              }else if(transGrpNbr.contains(t.GRNBR__c)){
                  if(essnMap.containsKey(t.Employee_SSN__c)){
                    tempT = essnMap.get(t.Employee_SSN__c);
                    
                    if(tempT.Loaded_into_HealthPac__c != t.Loaded_into_HealthPac__c){
                    tempT.Loaded_into_HealthPac__c = t.Loaded_into_HealthPac__c;
                        foo = true;
                    }
                    
                    if(tempT.Updated_in_HealthPac__c != t.Updated_in_HealthPac__c){
                    tempT.Updated_in_HealthPac__c = t.Updated_in_HealthPac__c;
                        foo = true;
                    }
                        
                        if(foo)
                        certList.add(tempT);
                        
                   }
              }
                
              
                        
        }
        
        
    }   
    
    
        //futureElig.deletehpelig(tobedeleted);
        if(certList.size()>0){
            update certList;
        }
        
        if(pccertList.size()>0){
          update pccertList;
        }
        

}