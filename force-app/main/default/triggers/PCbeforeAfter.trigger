trigger PCbeforeAfter on Patient_Case__c(after update, before update, before insert, after insert) {
    list<Patient_Case__c> pcListToUpdate = new list<Patient_Case__c>();
    set<id> carrierNotification = new set<id>();

    boolean isFirstRun = true;

    if (cheapstatichack.firstrun) {
        cheapstatichack.firstrun = false;
    }
    else{
        isFirstRun = false;
    }

    SystemID__c sid = SystemID__c.getInstance();
    PatientCase__c rid = PatientCase__c.getInstance();
      //set<id> carrierCaseClosedNotification = new set<id>();
    set<id> carrierDepartureNotification = new set<id>();
    set<id> hpPlanNameChanged = new set<id>();
    // set<id> employeeContactToUpdate = new set<id>();

    if (trigger.isAfter) {
        Patient_Case__c[] pcListTravel = new Patient_Case__c[] {};
        set<Patient_Case__c> pcListLOS = new set<Patient_Case__c>();
        set<Patient_Case__c> pcListaddl = new set<Patient_Case__c>();

        list<Program_Notes__c> pnList = new list<Program_Notes__c>();
        for (Patient_Case__c l: trigger.new) {
            if (trigger.isInsert) {
                  //Copy initial call description into the notes
                if (l.Initial_Call_Discussion__c != '' && l.Initial_Call_Discussion__c != null && l.Initial_Call_Discussion__c != 'Related Case created') {
                    Program_Notes__c newPN = new Program_Notes__c(patient_case__c = l.id, communication_type__c = 'Call', subject__c = 'Initial Call Discussion', notes__c = l.Initial_Call_Discussion__c);
                    pnList.add(newPN);
                }
            }
            else if (trigger.isUpdate) {
                if (l.isConverted__c) {
                    if (l.travel_type__c != trigger.oldMap.get(l.id).travel_type__c) {
                        pcListTravel.add(l);
                    }

                    if (l.Length_of_Episode__c != trigger.oldMap.get(l.id).Length_of_Episode__c && l.length_of_episode__c != null) {
                        pcListLOS.add(l);
                    }

                    if (l.Length_of_Episode_Additional_Days__c != trigger.oldMap.get(l.id).Length_of_Episode_Additional_Days__c && l.Length_of_Episode_Additional_Days__c != null) {
                        pcListaddl.add(l);
                    }

                    if ((l.client_name__c == 'Walmart' || l.client_name__c == 'Lowes') && !system.isFuture() && l.Actual_Departure__c != null && trigger.oldMap.get(l.id).Actual_Departure__c != l.Actual_Departure__c && l.carrierDepartureNotification__c == null) {
                        carrierDepartureNotification.add(l.id);
                    }

                    if ((l.client_name__c == 'Walmart' || l.client_name__c == 'Lowes') && l.case_type__c.contains('Cardiac') && l.OP_Pre_op_Evaluation__c != null && l.Proposed_Procedure__c != '' && l.estimated_departure__c != null &&
                        (l.OP_Pre_op_Evaluation__c != trigger.OldMap.get(l.id).OP_Pre_op_Evaluation__c || l.Proposed_Procedure__c != trigger.OldMap.get(l.id).Proposed_Procedure__c || l.estimated_departure__c != trigger.OldMap.get(l.id).estimated_departure__c)
                        ) {
                        carrierNotification.add(l.id);
                    }
                }
            }
        }

        COE_Expenses__c[] coeList = new COE_Expenses__c[] {};

        if (!pcListLOS.isEmpty()) {
            coeExpenses ce = new coeExpenses();
            integer amount = 0;
            ce.dailyStipendHandler(pcListLOS, 60);
            coeList.addAll(ce.getCoeList());
        }

        if (!pcListaddl.isEmpty()) {
            coeExpenses ce = new coeExpenses();
            ce.addlDailyStipendHandler(pcListaddl, 60);
            coeList.addAll(ce.getCoeList());
        }

        if (!coeList.isEmpty()) {
            try{
                upsert coeList;
                coeExpenses.expenseTotalUpdate(coeList);
            }catch (exception e) {
            }
        }

        if (!pnList.isEmpty()) {
            try{
                insert pnList;
            }catch (exception e) {
            }
        }

        if (!carrierDepartureNotification.isEmpty()) {
            futureElig.carrierDepartureNotification(carrierDepartureNotification);
        }

        if (!carrierNotification.isEmpty()) {
            futureElig.sendCardiacCarrierNotification(carrierNotification);
        }
    }

    if (isFirstRun) {
        set<ID> theIDs = new set<ID>();
        set<ID> eligIDs = new set<ID>();
        set<ID> authid = new set<ID>();
        set<ID> pocID = new set<ID>();
        set<id> fastTrack = new set<id>();

        list<Program_Notes__c> notesList = new list<Program_Notes__c>();
        list<Patient_Case__c> addElig = new list<Patient_Case__c>();

        //End Trigger After

        if (trigger.isBefore) {
            list<messaging.Singleemailmessage> finalMail = new list<messaging.Singleemailmessage>();
            //Begin Trigger For loop

            for (Patient_Case__c l: trigger.new) {
                if (!trigger.isInsert) {
                    if (l.Actual_Departure__c != null && (l.Actual_Departure__c != trigger.oldmap.get(l.id).Actual_Departure__c)) {
                        l.status_reason__c = 'Claim Pending';
                        l.createHP__c = true;
                        if (l.case_type__c.contains('Joint') && l.client_name__c != 'PepsiCo') {
                            Messaging.SingleEmailMessage mail = utilities.email(UpdateEligibilityRecords.hdpemail(l.client_name__c), null, null, 'System', 'Populate plan of care for ' + l.Patient_Case__c, 'Hello,\n\nPlease populate the plan of care section for Patient Case ' + l.Patient_Case__c + '\n\n' + url.getSalesforceBaseUrl().toExternalForm() + '/apex/caseDetail?id=' + l.id + '&tab=Tab4' + '\n\nThank You,\n\n' + Userinfo.getFirstName() + ' ' + Userinfo.getLastName());
                            finalMail.add(mail);
                        }
                    }

                      //Set Cotinine To Completed
                    if (l.Nicotine_Test_Scheduled__c != null && (l.Nicotine_Test_Scheduled__c != trigger.oldmap.get(l.id).Nicotine_Test_Scheduled__c)) {
                        l.ContinineCompleted__c = true;
                    }

                      //Continine Complete process
                    if (l.Continineemailsent__c != true && l.Nicotine_Test_Results__c != null && l.Nicotine_Test_Results__c != 'Not Completed' && (l.Nicotine_Test_Results__c != trigger.oldmap.get(l.id).Nicotine_Test_Results__c)) {
                        Messaging.SingleEmailMessage mail = utilities.email(null, null, null, string.valueof(l.owner.name), 'Cotinine Results for ' + l.Patient_Last_Name__c + ', ' + l.Patient_First_Name__c, 'Hello,\n\nThe Cotinine results for Patient Case ' + l.Patient_Case__c + ' have been entered as ' + l.Nicotine_Test_Results__c + '.\n\nYou can view the results by clicking this link: ' + url.getSalesforceBaseUrl().toExternalForm() + '/apex/caseDetail?id=' + l.id + '&tab=Tab12' + '\n\nThank You,\n\n' + Userinfo.getFirstName() + ' ' + Userinfo.getLastName());
                        list<String> sendemailto = new list<String>();
                        sendemailto.add(UpdateEligibilityRecords.hdpemail(l.client_name__c));
                        mail.setToAddresses(sendemailto); //CM
                        l.Continineemailsent__c = true;
                        finalMail.add(mail);
                    }

                      //Continine Scheduled, auth number can be sent
                    if (l.Nicotine_Test_Scheduled__c != null && trigger.oldmap.get(l.id).Nicotine_Test_Scheduled__c == null && (l.Nicotine_Test_Scheduled__c != trigger.oldmap.get(l.id).Nicotine_Test_Scheduled__c)) {
                        Messaging.SingleEmailMessage mail = utilities.email(null, null, null, string.valueof(l.owner.name), 'Cotinine test scheduled for ' + l.Patient_Case__c, 'Hello,\n\nThe Cotinine test for Patient Case ' + l.Patient_Case__c + ' has been scheduled.\n\nYou can view the case by clicking this link: ' + url.getSalesforceBaseUrl().toExternalForm() + '/apex/caseDetail?id=' + l.id + '&tab=Tab12' + '\n\nThank You,\n\n' + Userinfo.getFirstName() + ' ' + Userinfo.getLastName());
                        list<String> sendemailto = new list<String>();
                        sendemailto.add(UpdateEligibilityRecords.hdpemail(l.client_name__c));
                        mail.setToAddresses(sendemailto); //CM
                        finalMail.add(mail);
                    }

                      //Custom notification for Care Management
                    if (userInfo.getLastName() != 'System') {
                        if (l.ownerid == sid.coeQueue__c && (l.OwnerId != trigger.oldmap.get(l.id).ownerid)) {
                            list<String> sendemailto = new list<String>();

                            sendemailto.add(UpdateEligibilityRecords.hdpemail(l.client_name__c));
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setReplyTo(string.valueof(l.owner.email));
                            mail.setToAddresses(sendemailto);

                            mail.setSenderDisplayName('Customer Service');
                              //mail.setSenderDisplayName(string.valueof(l.owner.name));
                            mail.setSubject('Patient Case ' + l.Patient_Case__c + ', a ' + l.case_type__c + ', has been transferred to the HDP Care Management COE queue.');

                            if (l.Patient_Last_Name__c != null) {
                                mail.setPlainTextBody('Hello,\n\nPatient Case for ' + l.Patient_Last_Name__c + ', ' + l.Patient_First_Name__c + ' has been transferred to the HDP Care Management COE queue.\n\nYou can view the case here: ' + url.getSalesforceBaseUrl().toExternalForm() + '/' + l.id);
                            }
                            else{
                                mail.setPlainTextBody('Hello,\n\nPatient Case for ' + l.Employee_Last_Name__c + ', ' + l.Employee_First_Name__c + ' has been transferred to the HDP Care Management COE queue.\n\nYou can view the case here: ' + url.getSalesforceBaseUrl().toExternalForm() + '/' + l.id);
                            }
                            finalMail.add(mail);
                            //l.cmNotified__c = true;
                        }
                    }
                }

                if (l.isConverted__c) {
                    if (!trigger.isInsert) { //We know the trigger is before, but check to be sure it is not an insert
                        if (l.Actual_Departure__c != null && ((l.Employee_Primary_Health_Plan_Name__c != trigger.oldMap.get(l.id).Employee_Primary_Health_Plan_Name__c) ||
                                                              (l.client_Facility__c != trigger.oldMap.get(l.id).client_Facility__c) ||
                                                              (l.case_type__c != trigger.oldMap.get(l.id).case_type__c))) {
                            if (l.Employee_Primary_Health_Plan_Name__c != null && l.client_Facility__c != null && l.case_type__c != null) {
                                hpPlanNameChanged.add(l.id);
                                //Messaging.SingleEmailMessage mail = utilities.email('hdpeligibility@hdplus.com',null,null,string.valueof(l.owner.name),'HealthPAC plan name change for ' + l.Patient_Case__c,'Hello,\n\nPlease update the healthPAC plan name to ' + l.HealthPac_Plan_Name__c + ' for Patient case ' + url.getSalesforceBaseUrl().toExternalForm() + '/apex/caseDetail?id=' + l.id + '&tab=Tab12' + '\n\nThank You,\n\n' + Userinfo.getFirstName() + ' ' + Userinfo.getLastName());

                                //finalMail.add(mail);
                            }
                        }

                        if (l.hotel_rate__c == null && l.hotel_name__c != null) {
                            string h;
                            if (l.hotel_name__c != null) {
                                h = l.hotel_name__c;
                            }
                            if (h != null) {
                                map<string, double> hMap = new map<string, double> {
                                    'Radisson' = > 99.03,
                                    'Wyndham' = > 116.99,
                                    'Best Western' = > 111.63,
                                    'Days Inn Inner Harbor' = > 114.35,
                                    'Fairfield Inn & Suites' = > 124.26,
                                    'Sheraton' = > 114.35,
                                    'Candlewood Suites' = > 137.58,
                                    'Inn at Virginia Mason' = > 99.68,
                                    'The Baronness Hotel' = > 99.68
                                };
                                l.hotel_rate__c = hMap.get(h);
                            }
                        }

                        if (l.hospital_admit__c != null && l.hospital_discharge__c != null) {
                            l.Days_in_Hospital__c = l.hospital_admit__c.daysBetween(l.hospital_discharge__c) - 1;

                            if (l.Days_in_Hospital__c <= 0) {
                                l.Days_in_Hospital__c = 1;
                            }
                        }

                        if (l.travel_type__c == 'Driving Greater than 60 miles') {
                            if (l.mileage__c != null && l.mileage__c != 'Calculating...') {
                                l.Driving_Stipend_Mileage_Offset__c = double.valueof(l.mileage__c) * .23;
                            }
                        }

                        if (l.travel_type__c != 'Flying') {
                            l.Airfare_Offset__c = 0;
                        }

                        if (l.Number_of_Hotel_Days__c != null && l.hotel_offset__c == null && l.hotel_rate__c != null) {
                            if (l.hotel_rate__c > 100.00) {
                                l.hotel_offset__c = l.Number_of_Hotel_Days__c * 100.00;
                            }
                            else{
                                l.hotel_offset__c = l.Number_of_Hotel_Days__c * l.hotel_rate__c;
                            }

                            if (l.Days_in_Hospital__c != null) {
                                l.hotel_offset__c = l.hotel_offset__c - (l.Days_in_Hospital__c * 50);
                            }
                        }

                        if (l.Total_Taxable_Offset__c == null) {
                            if (l.hotel_offset__c == null) {
                                l.hotel_offset__c = 0;
                            }
                            if (l.Driving_Stipend_Mileage_Offset__c == null) {
                                l.Driving_Stipend_Mileage_Offset__c = 0;
                            }
                            if (l.other_offset__c == null) {
                                l.other_offset__c = 0;
                            }
                            if (l.Airfare_Offset__c == null) {
                                l.Airfare_Offset__c = 0;
                            }

                            l.Total_Taxable_Offset__c = l.Airfare_Offset__c + l.hotel_offset__c + l.Driving_Stipend_Mileage_Offset__c + l.other_offset__c;
                        }

                        if (l.Total_taxable_income__c == null) {
                            if (l.Total_Expenses_Paid__c == null) {
                                l.Total_Expenses_Paid__c = 0;
                            }
                            l.Total_taxable_income__c = l.Total_Expenses_Paid__c - l.Total_Taxable_Offset__c;
                        }

                          //Look for changes that would affect the auth number for a poc, only for isPBGH__c joints
                        if (l.isPBGH__c && l.case_type__c != 'Joint') {
                            if (l.Financial_Responsibility_Waiver__c != null && (l.Financial_Responsibility_Waiver__c != trigger.oldMap.get(l.id).Financial_Responsibility_Waiver__c) ||
                                l.Caregiver_Responsibility_Received__c != null && (l.Caregiver_Responsibility_Received__c != trigger.oldMap.get(l.id).Caregiver_Responsibility_Received__c) ||
                                l.Program_Authorization_Received__c != null && (l.Program_Authorization_Received__c != trigger.oldMap.get(l.id).Program_Authorization_Received__c)) {
                                pocID.add(l.id);
                            }
                        }
                    }

                    /* Set display auth number
                     * l.displayAuthNumber__c = 'N/A';
                     * if(l.Program_Authorization_Received__c !=null && l.Plan_of_Care_accepted_at_HDP__c !=null){
                     *  if((l.Client_Name__c=='Walmart' && l.Financial_Responsibility_Waiver__c!=null && l.Caregiver_Responsibility_Received__c != null && !l.case_type__c.contains('Joint')) ||
                     *     (l.Client_Name__c != 'Walmart') ||
                     *     (l.Client_Name__c == 'Walmart' && l.case_type__c.contains('Joint') && l.Caregiver_Responsibility_Received__c != null)){
                     *
                     *       if(l.case_type__c.contains('Cardiac')){
                     *           l.displayAuthNumber__c = l.Authorization_Number__c;
                     *       }else if(l.current_nicotine_user__c=='Yes'){
                     *
                     *           if(l.case_type__c.contains('Spine') && l.referred_facility__c== 'Cleveland Clinic'){
                     *               l.displayAuthNumber__c = l.Authorization_Number__c;
                     *           }else{
                     *
                     *           if(l.continineCompleted__c==true){
                     *
                     *               l.displayAuthNumber__c = l.Authorization_Number__c;
                     *
                     *               if(l.authsent__c == false || l.authsent__c == null){
                     *                   //l.authsent__c = true;
                     *                   authid.add(l.id);
                     *               }
                     *           }
                     *
                     *           }
                     *       }else{
                     *           l.displayAuthNumber__c = l.Authorization_Number__c;
                     *       }
                     *
                     *   }
                     * }
                     */
                }

                  //New records
                if (l.Status__c == 'Open') {
                    if (l.createdDate == null && l.caller_name__c != 'Web Intake') { //Set new records to status reason open
                        l.status_reason__c = 'New';
                    }
                    else{
                        if (l.createdDate != null && date.valueof(l.createdDate).daysbetween(date.today()) > 10) {
                            if (l.status_reason__c == 'New') {
                                l.status_reason__c = 'In Process';
                            }
                        }
                    }
                }

                if (!trigger.isInsert) { //We know the trigger is before, but check to be sure it is not an insert
                      //Ensure that the eligiblity is ALWAYS checked, even if it did not get checked in the batch process
                    if (l.Actual_Arrival__c != null && (l.Actual_Arrival__c != trigger.oldMap.get(l.id).Actual_Arrival__c)) {
                        l.Date_range_of_testing_begin__c = l.Actual_Arrival__c.addDays(-12);
                        l.Date_range_of_testing_end__c = l.Actual_Arrival__c.addDays(-9);
                        Integer daystill = date.today().daysBetween(l.Actual_Arrival__c);

                        if (daystill < 10 && !(date.today() > l.Actual_Arrival__c)) {
                            try{
                                if (l.Date_Care_Mgmt_Transfers_to_Eligibility__c == null || l.Date_Eligibility_is_Checked_with_Client__c.daysBetween(date.today()) > 10) {
                                    if (l.EligibiltyEmailSent__c == false) {
                                        l.EligibiltyEmailSent__c = true;
                                        eligIDs.add(l.id);
                                    }
                                }
                            }catch (exception e) {}
                        }
                    }

                    if (l.Estimated_Arrival__c != null && (l.Estimated_Arrival__c != trigger.oldMap.get(l.id).Estimated_Arrival__c)) {
                        Integer daystill = date.today().daysBetween(l.Estimated_Arrival__c);

                        if (daystill < 10 && !(date.today() > l.Estimated_Arrival__c)) {
                            if (l.Date_Care_Mgmt_Transfers_to_Eligibility__c == null || (l.Date_Eligibility_is_Checked_with_Client__c != null && l.Date_Eligibility_is_Checked_with_Client__c.daysBetween(date.today()) > 10)) {
                                if (l.EligibiltyEmailSent__c == false) {
                                    l.EligibiltyEmailSent__c = true;
                                    eligIDs.add(l.id);
                                }
                            }
                        }
                    }
                    //End 10 day eligibility check

                      //if eligibilty has changed then set date eligibility checked to todays date
                    if ((l.Eligible__c != null && l.Eligible__c != trigger.oldmap.get(l.id).Eligible__c) || (trigger.oldmap.get(l.id).Eligible__c == 'Cobra' && l.Eligible__c != 'Cobra' && l.Eligible__c != '')) {
                        l.Date_Eligibility_is_Checked_with_Client__c = date.today();
                        if (l.Eligibility_Manual_Override__c || (l.client_name__c != 'Walmart')) {
                            pcListToUpdate.add(l); //Add to a list to exeucte after the for loop has finished
                              //l.Status__c = 'Open';
                            l.pendingEligCheck__c = false;
                        }

                        if (l.Eligible__c == 'No' && l.isConverted__c == true) {
                            list<string> sendemailto = new list<string>();

                            if (utilities.isRunningInSandbox()) {
                                sendemailto.add(userInfo.getUserEmail());
                            }
                            else{
                                sendemailto.add('memberadvocate@hdplus.com');
                                sendemailto.add(UpdateEligibilityRecords.hdpemail(l.client_name__c));
                            }

                            string BodyofEmail = 'Hello, \n\n Patient Case ' + l.Patient_Case__c + ' for patient ' + l.Patient_First_Name__c + ' ' + l.Patient_Last_Name__c + ' has been checked for eligibility and is listed as not eligible.\n\n' + ' You can click the following link to access the record https://login.salesforce.com/' + l.id + '\n\nThanks!';
                            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                            mail.setSubject('Patient Case ' + l.Patient_Case__c + ' is no longer eligible.');
                            mail.setSenderDisplayName('HDP Eligibility');
                            mail.setToAddresses(sendemailto);
                            mail.setPlainTextBody(BodyofEmail);

                            finalMail.add(mail);
                        }

                        if (l.client_name__c != 'Lowes') {
                            //Eligibility Verification process

                            try{
                                l.patient_dob__c = date.valueof(l.patient_dobe__c); //Here for encryption
                                l.employee_dob__c = date.valueof(l.employee_dobe__c); //Here for encryption
                            }catch (exception e) {}

                            string body = l.eNotes__c;
                            if (body == null) {
                                body = '';
                            }
                            else{
                                body = body + '\n';
                            }

                            Program_Notes__c newNote = new Program_Notes__c();

                            if (l.BID_Verified__c == true && (l.BID_Verified__c != trigger.oldmap.get(l.id).BID_Verified__c)) {
                                body = 'The employee issued id has been verified\n';
                                if (l.bid__c != trigger.oldmap.get(l.id).bid__c) {  //Bid has changed
                                    body = body + 'The employee issued id was changed from ' + trigger.oldmap.get(l.id).bid__c + ' to ' + l.bid__c + '\n';
                                }
                            }

                              //HDP Carrier has been verified
                            if (l.Carrier_and_Plan_Type_verified__c == true && (l.Carrier_and_Plan_Type_verified__c != trigger.oldmap.get(l.id).Carrier_and_Plan_Type_verified__c)) {
                                body += 'The carrier and plan type have been verified\n';

                                if (l.Carrier_Name__c != trigger.oldmap.get(l.id).Carrier_Name__c) {
                                    body += 'The carrier name was changed from ' + trigger.oldmap.get(l.id).Carrier_Name__c + ' to ' + l.Carrier_Name__c + '\n';
                                }

                                if (l.Employee_Primary_Health_Plan_Name__c != trigger.oldmap.get(l.id).Employee_Primary_Health_Plan_Name__c) {
                                    body += 'The plan type was changed from ' + trigger.oldmap.get(l.id).Employee_Primary_Health_Plan_Name__c + ' to ' + l.Employee_Primary_Health_Plan_Name__c + '\n';
                                }
                            }

                              //Patient and Employee SSN have been verified
                            if (l.Patient_and_Employee_SSN_verified__c == true && (l.Patient_and_Employee_SSN_verified__c != trigger.oldmap.get(l.id).Patient_and_Employee_SSN_verified__c)) {
                                body += 'The Patient SSN and Employee SSN have been verified\n';

                                if (l.Patient_SSN__c != trigger.oldmap.get(l.id).patient_ssn__c) {
                                    body += 'The Patient SSN was changed from ' + trigger.oldmap.get(l.id).patient_ssn__c + ' to ' + l.patient_ssn__c + '\n';
                                }

                                if (l.Employee_SSN__c != trigger.oldmap.get(l.id).Employee_ssn__c) {
                                    body += 'The Employee SSN was changed from ' + trigger.oldmap.get(l.id).Employee_SSN__c + ' to ' + l.Employee_SSN__c + '\n';
                                }
                            }
                            //End SSN verified

                              //Patient and Employee DOB have been verified
                            if (l.Patient_and_Employee_DOB_verified__c && (l.Patient_and_Employee_DOB_verified__c != trigger.oldmap.get(l.id).Patient_and_Employee_DOB_verified__c)) {
                                body += 'Patient DOB and Employee DOB have been verified\n';

                                if (l.Patient_DOBe__c != null && (l.Patient_DOBe__c != trigger.oldmap.get(l.id).patient_dobe__c)) {
                                    l.Eligible__c = 'No';

                                    if (l.Patient_DOBe__c != '') {
                                        Date oldbday = date.valueof(trigger.oldmap.get(l.id).Patient_DOBe__c);
                                        body += 'The Patient DOB was changed from ' + oldbday.month() + '/' + oldbday.day() + '/' + oldbday.year() + ' to ' + l.patient_dob__c.month() + '/' + l.patient_dob__c.day() + '/' + l.patient_dob__c.year() + '\n';
                                    }
                                    else{
                                        body += 'The Patient DOB was changed from null to ' + l.patient_dob__c.month() + '/' + l.patient_dob__c.day() + '/' + l.patient_dob__c.year() + '\n';
                                    }
                                }

                                if (l.Employee_DOBe__c != null && (l.Employee_DOBe__c != trigger.oldmap.get(l.id).Employee_dobe__c)) {
                                    l.Eligible__c = 'No';

                                    if (l.employee_dobe__c != '') {
                                        Date oldbday = date.valueof(trigger.oldmap.get(l.id).employee_dobe__c);
                                        body += 'The Employee DOB was changed from ' + oldbday.month() + '/' + oldbday.day() + '/' + oldbday.year() + ' to ' + l.employee_dob__c.month() + '/' + l.employee_dob__c.day() + '/' + l.employee_dob__c.year() + '\n';
                                    }
                                    else{
                                        body += 'The Employee DOB was changed from null to ' + l.employee_dob__c.month() + '/' + l.employee_dob__c.day() + '/' + l.employee_dob__c.year() + '\n';
                                    }
                                }

                                l.patient_dob__c = null; l.employee_dob__c = null;
                            }
                            //END Patient and Employee DOB verfifed

                              //Add note to the case with verification information
                            if (body != null) {
                                newNote.patient_case__c = l.id;
                                if (l.Eligible__c == 'Yes') {
                                    if (body == null) {
                                        body = 'Patient is eligible';
                                    }
                                    else{
                                        body = 'Patient is eligible' + '\n' + body;
                                    }
                                }
                                else if (l.Eligible__c == 'No') {
                                    if (body == null) {
                                        body = 'Patient is not eligible';
                                    }
                                    else{
                                        body = 'Patient is not eligible' + '\n' + body;
                                    }
                                }

                                body = body.replace('The paid thru date needs to be determined. Once this have been done the eligibility check will be complete.', '');
                                newNote.notes__c = body;
                                newNote.subject__c = 'Eligibility Checked';
                                newNote.type__c = 'Note';
                                notesList.add(newNote);
                                  //l.Status__c = 'Open';
                                l.eNotes__c = body;
                            }

                            // End Eligibility Verification process
                        }
                    }
                }
                //End trigger is not an insert and eligibility check

                  //Always push address info from employee to patient address fields, if checkbox true
                if (l.Same_as_Employee_Address__c) {
                    if (l.Employee_City__c == null || l.Employee_Street__c == null || l.Employee_State__c == null || l.Employee_Zip_Postal_Code__c == null) {
                    }
                    else{
                        l.Patient_City__c = l.Employee_City__c;
                        l.Patient_Country__c = l.Employee_Country__c;
                        l.Patient_Street__c = l.Employee_Street__c;
                        l.Patient_State__c = l.Employee_State__c;
                        l.Patient_Zip_Postal_Code__c = l.Employee_Zip_Postal_Code__c;
                    }
                }

                  //Always push employee info to patient fields, if Relationship is Patient is Insured
                if (l.Relationship_to_the_Insured__c == 'Patient is Insured') {
                    if (l.Employee_Street__c != null) {
                        l.Patient_City__c = l.Employee_City__c;
                        l.Patient_Country__c = l.Employee_Country__c;
                        l.Patient_Street__c = l.Employee_Street__c;
                        l.Patient_State__c = l.Employee_State__c;
                        l.Patient_Zip_Postal_Code__c = l.Employee_Zip_Postal_Code__c;
                    }

                    if (l.Employee_First_Name__c != null) {
                        l.Patient_First_Name__c = l.Employee_First_Name__c;
                    }

                    if (l.Employee_Last_Name__c != null) {
                        l.Patient_Last_Name__c = l.Employee_Last_Name__c;
                    }

                    if (l.Employee_Gender__c != null) {
                        l.Patient_Gender__c = l.Employee_Gender__c;
                    }

                    if (l.Employee_DOBe__c != null) {
                        l.Patient_DOBe__c = l.Employee_DOBe__c;
                    }

                    if (l.Employee_SSN__c != null) {
                        l.Patient_SSN__c = l.Employee_SSN__c;
                    }
                }

                if (l.Patient_Last_Name__c != null) {
                    l.Patient_Last_Name_Sort__c = l.Patient_Last_Name__c.left(1);
                }

                if (l.Employee_Last_Name__c != null) {
                    l.Employee_Last_Name_Sort__c = l.Employee_Last_Name__c.left(1);
                }

                  //formart certid id
                if (trigger.isInsert) {
                    if (l.Patient_DOBe__c != null) {
                        l.patient_age__c = date.today().year() - date.valueOf(l.Patient_DOBe__c).year();

                        if (date.valueOf(l.Patient_DOBe__c).month() > date.today().month()) {
                            l.patient_age__c = l.patient_age__c - 1;
                        }
                        else if (date.valueOf(l.Patient_DOBe__c).month() == date.today().month()) {
                            if (date.valueOf(l.Patient_DOBe__c).day() > date.today().day()) {
                                l.patient_age__c = l.patient_age__c - 1;
                            }
                        }
                    }

                      //on insert, format if bid is not null
                    if (l.bid__c != null) {
                        l.bid_id__c = l.bid__c;
                        formatID nf = new formatID();
                        l.certID__c = nf.formatID(l.bid__c, l.Client_Name__c);
                        // l.travelcertID__c = l.certID__c.substring(4);
                    }

                    if (l.Status__c != null && l.isConverted__c) {
                        l.Program_Status_Last_Updated__c = date.today();
                    }
                }
                else{
                      //Mark case as re-opened
                    if (l.status__c != trigger.oldMap.get(l.id).status__c) {
                        if (((trigger.oldMap.get(l.id).status__c == 'Closed' || trigger.oldMap.get(l.id).status__c == 'Pended') && l.status__c == 'Open') || l.status__c == 'Pended') {
                            l.Reopened__c = true;
                        }
                    }

                    if (l.Patient_DOBe__c != null) {
                        l.patient_age__c = date.today().year() - date.valueOf(l.Patient_DOBe__c).year();
                        if (date.valueOf(l.Patient_DOBe__c).month() > date.today().month()) {
                            l.patient_age__c = l.patient_age__c - 1;
                        }
                        else if (date.valueOf(l.Patient_DOBe__c).month() == date.today().month()) {
                            if (date.valueOf(l.Patient_DOBe__c).day() > date.today().day()) {
                                l.patient_age__c = l.patient_age__c - 1;
                            }
                        }
                    }

                      //else, format if bid is not null AND has changed
                    if (l.bid__c != trigger.oldmap.get(l.id).bid__c) {
                        if (l.bid__c != null) {
                            formatID nf = new formatID();
                            l.certID__c = nf.formatID(l.bid__c, l.Client_Name__c);
                            l.bid_id__c = l.bid__c;
                            //l.travelcertID__c = l.certID__c.substring(4);
                        }
                    }

                    if (l.isConverted__c && l.Status__c != null && (l.Status__c != trigger.oldMap.get(l.id).Status__c)) {
                        l.Program_Status_Last_Updated__c = date.today();
                    }
                }

                l = caseStatuses.autoStatus(l);

                // if(l.Nicotine_Quit_Date__c != null && l.Plan_of_Care_accepted_at_HDP__c==null && l.Nicotine_Test_Scheduled__c!=null && l.Nicotine_Test_Results__c==null){
                //l.Status__c = 'Open';
                //     l.Status_Reason__c = 'Awaiting Nicotine Clearance';
                // }

                  //Entering Walmart only processes
                if (l.Client_Name__c == 'Walmart') {
                      //if Walmart spine set complexity to Non-Complex
                    if (l.RecordTypeId == rid.wmSpine__c) {
                        l.Procedure_Complexity__c = 'Non-Complex';
                    }
                }
                //End Walmart only processes
            }
            //End Trigger For loop

            if (!notesList.isEmpty()) {
                try{
                    insert notesList;
                }catch (dmlException e) {}
            }

            if (!system.isfuture() && !pcListToUpdate.isEmpty()) {
                UpdateEligibilityRecords uer = new UpdateEligibilityRecords();
                uer.UpdateRecordsForOpp(pcListToUpdate);
            }

            if (!fastTrack.isEmpty()) {
                futureElig.fastTrackRefer(fastTrack);
            }

            if (!finalMail.isEmpty()) {
                messaging.sendEmail(finalmail);
            }

            if (!pocID.isEmpty()) {
                futureElig.pocUpdate(pocID);
            }

            if (!eligIDs.isEmpty()) {
                futureElig.createElig(eligIDs);
            }

            if (!system.isFuture() && !authid.isEmpty()) { //send auth e-mail
                futureElig.sendAuthNumberforCotinine(authid);
            }

            //if(!carrierCaseClosedNotification.isEmpty()){
            //    futureElig.carrierCaseClosedNotification(carrierCaseClosedNotification);
            //}

            if (!hpPlanNameChanged.isEmpty()) {
                futureElig.hpPlanNameChanged(hpPlanNameChanged);
            }
        }
    }
}