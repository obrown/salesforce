trigger beforeAMSError on magpie_Error__c (before update, after update, before insert, after insert) {
    list<id> updatePatientNameFromHP  = new list<id>();
    
    if(trigger.isBefore){
    
    for(magpie_Error__c mp : trigger.new){
        
        try{
            mp.hprecordID__c = mp.Employee_SSN__c.right(4)+''+mp.underwriter__c+''+mp.member_name__c.left(2)+''+mp.member_name__c.right(2);
        }catch(exception e){
            mp.hprecordID__c = mp.Employee_SSN__c.right(4)+''+mp.underwriter__c;
        }
    }
    
    }
    
    
    if(trigger.isAfter){
        for(magpie_Error__c mp : trigger.new){
        
        try{
            if(!system.isFuture()){
                if(mp.Employee_SSN__c!= null && mp.underwriter__c != null && mp.member_name__c==null){
                    updatePatientNameFromHP.add(mp.id);
                }
            }
            
        }catch(exception e){
        }
        }
    }
    
    if(updatePatientNameFromHP.size()>0){
    //TODO: Update for size
    if(updatePatientNameFromHP.size() > 100){ 
        set<id> foo = new set<id>();
        for(integer i =0; i<100; i++){
            foo.add(updatePatientNameFromHP[i]);
        }
        
        AmsFutureJob.updatePatientNameFromHP(foo);
        foo.clear();
        
        for(integer i =100; i<updatePatientNameFromHP.size(); i++){
            foo.add(updatePatientNameFromHP[i]);
        }
        
        AmsFutureJob.updatePatientNameFromHP(foo);
            
    }else{
        AmsFutureJob.updatePatientNameFromHP(new set<id>(updatePatientNameFromHP));  
    }
    }
}