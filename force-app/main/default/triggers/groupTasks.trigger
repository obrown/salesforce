trigger groupTasks on Task (before insert, before update, before delete) {

    system.debug(trigger.size);
    map<id, list<Task>> taskMap = new map<id, list<Task>>();
    
    if(trigger.size>1 && Trigger.isInsert){ //Grouped task
    
        for(Task t: trigger.new){ //Group all tasks into one common list
            
            if(taskMap.containsKey(t.WhatId)){
               taskMap.put(t.WhatId,updateList(taskMap.get(t.WhatId),t)); 
            
            }else{
                taskMap.put(t.WhatId,createlist(t));
            }
            
        }
        
        for(Task t: trigger.new){
        
           if(taskmap.containsKey(t.whatID)){ 
            string rndid = rnd();
            for(Task tt: taskMap.get(t.whatId)){
                tt.rndTaskID__c = rndid;
            }
            
            taskMap.remove(t.whatID);
            }
            
        }
    
    }else{
        if(!trigger.isInsert && trigger.size==1){
        set<id> tuids = new set<id>();
        set<string> rnds = new set<string>();
        if(trigger.isUpdate){
          
            for(Task t: trigger.new){
                
                if(t.rndTaskID__c!=null){
                    rnds.add(t.rndTaskID__c);
                    tuids.add(t.id);
                   
                }
                
            }
           	 
           	 try{
             	futureElig.updateRelatedTask(tuids,rnds);
             }catch (exception e){
             	//Can't detect if the trigger is being fired from a future call...so catch the exception
             }
            
        }else{
        
            for(Task t: trigger.old){
                    
                    if(t.rndTaskID__c!=null){
                        rnds.add(t.rndTaskID__c);
                        tuids.add(t.id);
                       
                    }
                    
                }
                try{
             	
             	futureElig.deleteRelatedTask(tuids,rnds); 
             		}catch (exception e){
             	//Can't detect if the trigger is being fired from a future call...so catch the exception
             	}
                
            }
            
        
        }
    
    }
    
    private string rnd(){
         string foo = string.valueof(datetime.now());
         foo = foo.replaceAll('-','');
         foo = foo.replaceAll(':','');
         foo = foo.replaceAll(' ','');
         double rndID = double.valueof(foo) * math.random();
         
         return string.valueof(rndID);
    }
    
    private list<Task> createlist(Task t){
    
        Task[] foo = new Task[]{};
        foo.add(t);
        
        return foo;
    
    }
    
    private list<Task> updateList(list<Task> tlist, Task t){
        tlist.add(t);
        return tlist;
    }
    
}