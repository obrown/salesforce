trigger cciUpdateInsert on Customer_Care_Inquiry__c(before insert, before update) {
    customer_care_inquiry__c[] cciList = new customer_care_inquiry__c[] {};
    customer_care_inquiry__c[] eligList = new customer_care_inquiry__c[] {};
    Set<String> ClaimsDepts = new Set<String> {
        'Claims', 'Claims Technical', 'Claims Supervisor', 'Quality/Rx', 'Quality/Audit', 'Compliance', 'Provider', 'Claims Support', 'Finance', 'Plan Build', 'Admin'
    };
    Set<String> EligDepts = new Set<String> {
        'Eligibility'
    };
    string userlastname = userinfo.getlastname();
    id uid = userinfo.getuserid();
    if (trigger.isInsert) {
        for (customer_care_inquiry__c c : trigger.new) {
            c.last_modified_date__c = datetime.now();
            c.last_modified_by__c = uid;
            c.preventDuplicates__c = string.valueof(datetime.now().getTime());

            if (ClaimsDepts.contains(c.work_department__c)) {
                cciList.add(c);
            }

            if (EligDepts.contains(c.work_department__c)) {
                eligList.add(c);
            }
        }
    }
    else{
        for (customer_care_inquiry__c c : trigger.new) {
            if (userlastname != 'System') {
                c.last_modified_date__c = datetime.now();
                c.last_modified_by__c = userinfo.getuserid();
            }

            if (c.work_department__c != trigger.oldmap.get(c.id).work_department__c) {
                if (ClaimsDepts.contains(c.work_department__c)) {
                    cciList.add(c);
                }
                else if (EligDepts.contains(c.work_department__c)) {
                    eligList.add(c);
                }
            }
            else if (c.anthem_aged_claim__c == true && (c.anthem_aged_claim__c != trigger.oldmap.get(c.id).anthem_aged_claim__c)) {
                cciList.add(c);
            }
        }
    }

    if (cciList.size() > 0) {
        map<string, customer_care_inquiry__c[]> cciMap = new map<string, customer_care_inquiry__c[]>();

        for (customer_care_inquiry__c cci : cciList) {
            customer_care_inquiry__c[] foo = cciMap.get(cci.work_department__c);

            if (foo == null) {
                foo = new customer_care_inquiry__c[] {};
            }

            foo.add(cci);
            cciMap.put(cci.work_department__c, foo);
        }

        for (string key : cciMap.keyset()) {
            claimsAssignment ca = new claimsAssignment(key, 'Claims');
            ca.setList(cciList, key, 'Claims');
        }
    }

    if (eligList.size() > 0) {
        claimsAssignment ca = new claimsAssignment('Eligibility', 'Eligibility');
        ca.setList(eligList, 'Eligibility', 'Eligibility');
    }
}