trigger customerCareBefore on Customer_Care__c(before insert, before update) {
    string userlastname = userinfo.getlastname();
    id uid = userinfo.getuserid();

    if (trigger.isInsert) {
        for (customer_care__c c : trigger.new) {
            c.last_modified_date__c = datetime.now();
            c.last_modified_by__c = uid;
            c.preventDuplicates__c = string.valueof(datetime.now().getTime());
        }
    }
    else{
        for (customer_care__c c : trigger.new) {
            if (userlastname != 'System') {
                c.last_modified_date__c = datetime.now();
                c.last_modified_by__c = uid;
            }
        }
    }
}