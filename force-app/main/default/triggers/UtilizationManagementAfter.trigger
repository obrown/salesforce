trigger UtilizationManagementAfter on Utilization_Management__c (after update, after insert) {
    public Utilization_Management_History__c umAuditTrailRecord {//12-28-21 added o.brown hdp-6719
        get; private set;
    }
        
    set<id> theIds = new set<id>();
    set<id> finalStatusChanged= new set<id>();//12-20-21 added o.brown hdp-6624
    if(trigger.isInsert){
   
    //set umAudit history initial save record   
    umAuditTrailRecord = new Utilization_Management_History__c();
        for(Utilization_Management__c  um : trigger.new){
            
            if(um.event_type__c!='' && um.event_type__c!=null && um.Med_Cat__c!='' && um.Med_Cat__c!=null){
                theIds.add(um.id);
            }
           //12-28-21 set um initial history record  
           system.debug('um ='+um);
           ID id = um.Id;
           umAuditTrailRecord.Utilization_Management__c = ID;
           umAuditTrailRecord.Initial_History__c= true;
           umAuditTrailRecord.Admission_Date__c= um.Admission_Date__c;
           umAuditTrailRecord.Approved_Through_Date__c= um.Approved_Through_Date__c;
           umAuditTrailRecord.Quality_Codes__c= um.Quality_Codes__c;
           umAuditTrailRecord.Nurses__c= um.Nurses__c;
           umAuditTrailRecord.Facility_Network_Status__c= um.Facility_Network_Status__c; 
           umAuditTrailRecord.Comment_Line_for_Claims__c= um.Comment_Line_for_Claims__c;        
        
        } 
        insert(umAuditTrailRecord);   
    }
    
    if(trigger.isUpdate){
    fieldAuditHistory.recordHistory(trigger.newMap, trigger.oldMap, 'Utilization_Management__c', 'Utilization_Management_History__c');//12-28-21 added
      for(Utilization_Management__c  um : trigger.new){
      
        boolean isApproved = populationHealthUtils.isUmCaseApproved(um.case_status__c);
        boolean isDenied   = populationHealthUtils.isUmCaseDenied(um.case_status__c);
        
        if((isApproved || isDenied || um.softDelete__c) && um.HealthPac_Case_Number__c!='' && um.HealthPac_Case_Number__c!=null) {
            
            if(um.Admission_Date__c!=null && (um.Admission_Date__c != trigger.oldMap.get(um.id).Admission_Date__c)){
                   theIds.add(um.id);
            
            }else if(um.Approved_Through_Date__c!=null && (um.Approved_Through_Date__c!= trigger.oldMap.get(um.id).Approved_Through_Date__c)){
                   theIds.add(um.id);
            
            }else if(um.Med_Cat__c!='' && um.Med_Cat__c!=null&& (um.Med_Cat__c!= trigger.oldMap.get(um.id).Med_Cat__c)){
                   theIds.add(um.id);
            
            }else if(um.event_type__c!='' && um.event_type__c!=null && (um.event_type__c!= trigger.oldMap.get(um.id).event_type__c)){
                   theIds.add(um.id);
            
            }else if(um.visits__c!=null&& (um.visits__c!= trigger.oldMap.get(um.id).visits__c)){
                   theIds.add(um.id);
                  

            }
            
        }
        
      }
      
    }
    
    if(!theIds.isEmpty()){
        utilizationManagementWorkflow.sendUM(theIds);
    }

}