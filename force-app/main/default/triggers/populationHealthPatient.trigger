trigger populationHealthPatient on phm_Patient__c (before insert, before update) {
    
    populationHealthWorkFlow.validationRules(trigger.new);
    populationHealthWorkFlow.setSearchCert(trigger.new);
}