/*
 *
 *  There is a process that uploads all the claims nightly (from the warehouse), which will cause either an insert for a new claim or an update for an existing claim
 *  This will look for a matching patient case or transplant record that has service dates that fall into the fdos and tdos range, and associate the appropriate record
 *
 **Added a function to update the patient case status to Completed and the status reason to Claim paid when a claim has been marked paid
 *
 */
trigger claimsMatching on ClaimsReporting__c(before insert, before update) {
    set<string> claims_newly_paid = new set<string>();

    for (ClaimsReporting__c c :trigger.new) {
        if (Trigger.isInsert && c.pay_status__c == 'Paid') {
            claims_newly_paid.add(c.claim_number__c);
        }
        else if (!Trigger.isInsert && c.pay_status__c == 'Paid' && (c.Pay_Status__c != trigger.oldMap.get(c.id).Pay_Status__c)) {
            claims_newly_paid.add(c.claim_number__c);
        }
    }

    ClaimsMatcher.run_from_trigger(trigger.new, claims_newly_paid);
}