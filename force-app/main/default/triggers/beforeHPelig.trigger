/*
Author: Michael Martin
Date: 10/25/2013
Purpose: Add transplant id based on essn.
*/

trigger beforeHPelig on hp_eligibility__c (before insert) {

    map<string,id> pssn = new map<string,id>();
    map<string,id> essn = new map<string,id>();

    for(transplant__c t: [select employee_ssn__c, patient_ssn__c from transplant__c]){
        
        if(t.employee_ssn__c!=null && t.employee_ssn__c!=''){
        essn.put(t.employee_ssn__c, t.id);
        }
        if(t.patient_ssn__c!=null && t.patient_ssn__c!=''){
        pssn.put(t.patient_ssn__c, t.id);
        }
        
    }
    
    for(hp_eligibility__c h : trigger.new){
        if(h.GRNBR__c == 'WMT'){
            if(essn.containsKey(h.Employee_SSN__c)){
                h.transplantID__c = essn.get(h.Employee_SSN__c);
            }else if(pssn.containsKey(h.Patient_SSN__c)){
                h.transplantID__c = pssn.get(h.Patient_SSN__c);
            }
        }       
        
    }
    
    
    /*<summary> 
        Look for records that have not yet had the tranplantID assigned
    </summary>*/
     
    hp_eligibility__c[] hpList = new hp_eligibility__c[]{};
    for(hp_eligibility__c h :[select GRNBR__c,transplantID__c,Patient_SSN__c,Employee_SSN__c   from hp_eligibility__c where transplantID__c = null and GRNBR__c = 'WMT']){
    
            if(essn.containsKey(h.Employee_SSN__c)){
                h.transplantID__c = essn.get(h.Employee_SSN__c);
                 hpList.add(h); 
            }else if(pssn.containsKey(h.Patient_SSN__c)){
                h.transplantID__c = pssn.get(h.Patient_SSN__c);
                  hpList.add(h); 
            }
    
    }
    
    
    if(hpList.size()>0){ 
      
      try{
           update hpList;
       }catch (exception e){
           //Send me an e-mail
               utilities.email('mmartin@hdplus.com', null, null, 'mmartin@hdplus.com','beforeHPelig Trigger Error', 'beforeHPelig Trigger Error Line 56\n\nUser' + userInfo.getUserName() + '\n\n' + e.getMessage());
       }
    }
    
}