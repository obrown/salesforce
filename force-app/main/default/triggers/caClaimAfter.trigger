trigger caClaimAfter on caClaim__c (after insert, after update) {


    if(!system.isFuture()){
        
        set<id> theClaims = new set<id>();
        
        for(caClaim__c c :trigger.new){
            theClaims.add(c.id);    
            
        }
        
        claimAuditFuture.updateClaimAuditName(theClaims);
        
    }

}