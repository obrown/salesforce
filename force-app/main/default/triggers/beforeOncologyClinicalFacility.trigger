trigger beforeOncologyClinicalFacility on Oncology_Clinical_Facility__c (before insert, before update) {
    
    set<id> extIds = new set<id>();
    Oncology_Clinical_Facility__c[] addExternalId = new Oncology_Clinical_Facility__c[]{};
    
    if(trigger.isInsert){
        for(Oncology_Clinical_Facility__c ocf : trigger.new){
            
            if(ocf.Patient_State__c!=null && ocf.Patient_State__c.length()>2){
                ocf.Patient_State__c =oncolcogyStateFormat.stateAbbrv(ocf.Patient_State__c);
            }
            
            if(ocf.Physician_State__c!=null && ocf.Physician_State__c.length()>2){
                ocf.Physician_State__c=oncolcogyStateFormat.stateAbbrv(ocf.Physician_State__c);
            }
            
            if(ocf.parentid__c==null && ocf.facility_record_id__c!=null){
                addExternalId.add(ocf);
                extIds.add(ocf.facility_record_id__c);
            }
        }
        
        if(!extIds.isEmpty()){
            OncologyMapToExternalFacility.mapIncomingOncologyId(addExternalId, extIds);
            
        }
    
    
    }
}