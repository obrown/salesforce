trigger  beforeElectronicWorkTask on Operations_Work_Task__c (before Insert,before Update){

    
    //Create empty set of parent IDs
    Set<Id> owtIds = new Set<Id>();
    map<id, Operations_Work_Task__c[]> taskMap = new map<id, Operations_Work_Task__c[]>();
    map<id, Operations_Work_Task__c> currTaskMap = new map<id, Operations_Work_Task__c>();
    operations_work_task__c[] assignList = new operations_work_task__c[]{};
    string currentUser = userInfo.getLastName();
    // Interate TRIGGER (not selected) owt records
    for (Operations_Work_Task__c owt: Trigger.new){
       if(owt.Work_Task_Status__c=='Closed' ){
            taskMap.put(owt.id, new Operations_Work_Task__c[]{});
            currTaskMap.put(owt.id, owt);
       }
       
       if(owt.EESSN__c!=null){
           owt.EE_Last4_SSN__c ='***-**-'+owt.EESSN__c.right(4);
       }
       
       if(owt.assigned__c==null){
           assignList.add(owt);
       }
       
       if(currentUser!='System'){
           owt.hdp_last_modified_date__c=datetime.now();
           owt.hdp_last_modified_by__c  =userInfo.getUserId();
       }
         
    }
    
    /* This could be managed in a forumula field removing the need to run a select query everytime we close a case... */
    /*   This should be refactored.  */
    for(Operations_Work_Task__c owt : [select OWT_Parent__c, name,Work_Task_Status__c from Operations_Work_Task__c where OWT_Parent__c in :taskMap.keySet() and Dependent_Task__c=true and Work_Task_Status__c !='Closed']){
        Operations_Work_Task__c[]  foo = taskMap.get(owt.OWT_Parent__c);
        foo.add(owt);
        taskMap.put(owt.OWT_Parent__c, foo);
    
    }
    
    for(id i : taskMap.keyset()){
        Operations_Work_Task__c[]  foo = taskMap.get(i);
        if(!foo.isEmpty()){
            Operations_Work_Task__c o = currTaskMap.get(i);
            
            for(Operations_Work_Task__c f : foo){
                string errorMsg = 'Work Task ' + f.name +' is still in status ' +f.Work_Task_Status__c;
                o.addError(errorMsg);
                
            }
            
            
        }
        
    }
    /*  ^^^^^^   This should be refactored.   ^^^^^^  */
    
    if(assignList.size()>0){
        claimsAssignment ca = new claimsAssignment();
        ca.setEwtList(assignList);
        
    }
    
}